//
//  GermanAndChurchTax.swift
//  Input
//
//  Created by 8pitch on 13.01.2021.
//

import RxSwift

public enum TaxError : ModelError {
    case required
    
    public var errorDescription : String? {
        return "error.tax.required".localized
    }
}

public enum IDTaxError : ModelError {
    case empty
    
    public var errorDescription : String? {
        return "input.error.empty".localized
    }
}

public enum OfficeTaxError : ModelError {
    case empty
    
    public var errorDescription : String? {
        return "input.error.empty".localized
    }
}

public enum AttributeTaxError : ModelError {
    case empty
    
    public var errorDescription : String? {
        return "input.error.empty".localized
    }
}

public enum ChurchTaxAttribute: String, CaseIterable {
    case ak = "ak"
    case ap = "ap"
    case ag = "ag"
    case ba = "ba"
    case bd = "bd"
    case ca = "ca"
    case CE = "CE"
    case ev = "ev"
    case fccs = "fccs"
    case gf = "gf"
    case gk = "gk"
    case lt = "lt"
    case un = "un"
    case fr = "fr"
    case go = "go"
    case ht = "ht"
    case IS = "is"
    case jd = "jd"
    case ka = "ka"
    case me = "me"
    case mt = "mt"
    case ml = "ml"
    case na = "na"
    case ob = "ob"
    case ox = "ox"
    case pr = "pr"
    case rf = "rf"
    case rk = "rk"
    case ro = "ro"
    case so = "so"
    case wr = "wr"
    case zj = "zj"
    
    public init(rawValue: String) {
        self = Self.localized[rawValue] ?? .ak
    }
    
    private static var localized : [String : Self] {
        let values : [Self] = Self.allCases
        return values.reduce(into: [String : Self]()) { $0[$1.rawValue] = $1 }
    }
}

public class TaxInformation: Model {
    
    public let taxResidentProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public let taxIDProvider : BehaviorSubject<String?> = .init(value: nil)
    public let taxOfficeProvider : BehaviorSubject<String?> = .init(value: nil)
    public let churchTaxLabilityProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public let churchTaxAttributeProvider : BehaviorSubject<String?> = .init(value: nil)
    
    public var churchAttributeValue: String? {
        if self.churchTaxLabilityValue ?? false, let attribute = try? self.churchTaxAttributeProvider.value() {
            return attribute
        } else {
            return nil
        }
    }
    
    public var taxIDValue: String? {
        if self.taxResident, let id = try? self.taxIDProvider.value() {
            return id
        } else {
            return nil
        }
    }
    
    public var taxOfficeValue: String? {
        if self.taxResident, let office = try? self.taxOfficeProvider.value() {
            return office
        } else {
            return nil
        }
    }
    
    public var churchTaxLabilityValue: Bool? {
        if self.taxResident, let churchTaxLability = try? self.churchTaxLabilityProvider.value() {
            return churchTaxLability
        } else {
            return nil
        }
    }
    
    private var taxResident: Bool = true
    private var taxID: String = ""
    private var taxOffice: String = ""
    private var churchTaxLability: Bool = false
    private var churchTaxAttribute: ChurchTaxAttribute?
    
    public init(user: User? = nil) {
        super.init()
        let provider = try? user?.taxInformationProvider.value()
        self.taxResidentProvider.onNext((try? provider?.taxResidentProvider.value()) ?? true)
        self.taxIDProvider.onNext(try? provider?.taxIDProvider.value())
        self.taxOfficeProvider.onNext(try? provider?.taxOfficeProvider.value())
        self.churchTaxLabilityProvider.onNext((try? provider?.churchTaxLabilityProvider.value()) ?? false)
        self.churchTaxAttributeProvider.onNext(try? provider?.churchTaxAttributeProvider.value()?.localized)
    }
    
    override func setup() {
        super.setup()
        
        self.taxResidentProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.isValid.onNext(.success(false))
                self?.taxResident = $0
                self?.validate()
            }).disposed(by: self.disposables)
        
        self.taxIDProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                guard !$0.isEmpty else {
                    self?.isValid.onNext(.failure(IDTaxError.empty))
                    return
                }
                self?.taxID = $0
                self?.validate()
            }).disposed(by: self.disposables)
        
        self.taxOfficeProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                guard !$0.isEmpty else {
                    self?.isValid.onNext(.failure(OfficeTaxError.empty))
                    return
                }
                self?.taxOffice = $0
                self?.validate()
            }).disposed(by: self.disposables)
        
        self.churchTaxLabilityProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.churchTaxLability = $0
                self?.validate()
            }).disposed(by: self.disposables)
        
        self.churchTaxAttributeProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                guard !$0.isEmpty else {
                    self?.isValid.onNext(.failure(IDTaxError.empty))
                    return
                }
                self?.churchTaxAttribute = ChurchTaxAttribute(rawValue: $0)
                self?.validate()
            }).disposed(by: self.disposables)
    }
    
    public func validate() {
        let isResident = self.taxResident
        let residenceInfoCompleted = !self.taxID.isEmpty && !self.taxOffice.isEmpty
        let churchAttributeSelected = self.churchTaxAttribute != nil
        let isResidentNoChurch = !self.churchTaxLability && residenceInfoCompleted && isResident
        let isResidentWithChurch = isResident && self.churchTaxLability && residenceInfoCompleted && churchAttributeSelected
        let isValid = !isResident || isResidentNoChurch || isResidentWithChurch
        if isValid {
            self.isValid.onNext(.success(true))
        } else {
            self.isValid.onNext(.failure(TaxError.required))
        }
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.taxResidentProvider.onNext(nil)
        self.taxIDProvider.onNext(nil)
        self.taxOfficeProvider.onNext(nil)
        self.churchTaxLabilityProvider.onNext(nil)
        self.churchTaxAttributeProvider.onNext(nil)
        self.taxResident = true
        self.taxID = ""
        self.taxOffice = ""
        self.churchTaxLability = false
        self.churchTaxAttribute = nil
        self.setup()
    }
}
