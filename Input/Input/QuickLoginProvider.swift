//
//  QuickLoginProvider.swift
//  Input
//
//  Created by 8pitch on 11/26/20.
//

import Foundation

public class QuickLoginProvider: Model {
    
    public var isPinEnabled: Bool {
        get {
            BiometryManager.shared.isPINEnabled
        }
        set {
            BiometryManager.shared.pin = newValue ? self.createPin : nil
        }
    }
        
    private var currentPin: String {
        BiometryManager.shared.pin ?? ""
    }
    
    public var createPin: String = ""
    public var repeatPin: String = ""
    
    public func pinsMatched() -> Bool {
        return isPinEnabled ? createPin == currentPin : createPin == repeatPin
    }
    
    public override func cleanUp() {
        self.createPin = ""
        self.repeatPin = ""
    }
    
}
