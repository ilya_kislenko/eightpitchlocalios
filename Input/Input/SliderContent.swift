//
//  SliderContent.swift
//  Input
//
//  Created by 8pitch on 12/17/20.
//

public class SliderContent {
    public var backgroundImageFileID: String
    public var buttonText: String
    public var group: String
    public var header: String
    public var link: String
    public var title: String
    
    public init(backgroundImageFileID: String,
                buttonText: String,
                group: String,
                header: String,
                link: String,
                title: String) {
        self.backgroundImageFileID = backgroundImageFileID
        self.buttonText = buttonText
        self.group = group
        self.header = header
        self.link = link
        self.title = title
    }
}
