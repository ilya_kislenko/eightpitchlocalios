//
//  Validator.swift
//  Input
//
//  Created by 8pitch on 9/18/20.
//

enum ValidatorError : Error {
    case empty
    case format
    case pattern
}

enum ValidationType : Error {
    case name
    case age
    case city
    case zip
    case street
    case number
    case iban
    case companyName
}

public struct Validator {
    
    private let namePattern = "^[A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'’-]*+([ A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'’-]+)*"
    private let cityPattern = "^[A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'’-]*+([ A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'’-]+)*"
    private let zipPattern = "[a-zA-Z0-9]"
    private let streetPattern = "^[A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'’-]*+([ A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'’-]+)*"
    private let numberPattern = "[A-Za-z0-9]"
    private let companyNamePattern = "^[A-Za-z0-9\u{00C0}-\u{00FF}][A-Za-z0-9\u{00C0}-\u{00FF} '’.-]*+([ A-Za-z0-9\u{00C0}-\u{00FF}][A-Za-z0-9\u{00C0}-\u{00FF} '’.-]+)*"
    
    func validate(string: String, type: ValidationType) -> Result<Bool, ValidatorError> {
        if string.isEmpty {
            return .failure(.empty)
        }
        if (type != .age) && (type != .iban) && !self.isValid(string: string, type: type) {
            return .failure(.pattern)
        }
        if type == .name && !self.hasValidFirstSymbol(string: string) {
            return .failure(.format)
        }
        if type == .age && !self.isValidBirthdate(date: string) {
            return .failure(.pattern)
        }
        if type == .iban && !self.isValidIban(string) {
            return .failure(.pattern)
        }
        return .success(true)
    }
    
    private func isValid(string: String, type: ValidationType) -> Bool {
        let pattern: String
        switch type {
        case .city:
            pattern = self.cityPattern
        case .name:
            pattern = self.namePattern
        case .number:
            pattern = self.numberPattern
        case .street:
            pattern = self.streetPattern
        case .zip:
            pattern = self.zipPattern
        case .companyName:
            pattern = self.companyNamePattern
        default:
            return false
        }
        guard let regex = try? NSRegularExpression(pattern: pattern, options: []) else
        {
            return false
        }
        let searchable = NSRange(location: 0, length: string.count)
        let found = regex.rangeOfFirstMatch(in: string, options: [], range: searchable)
        return found == searchable
    }
    
    private func hasValidFirstSymbol(string: String) -> Bool {
        guard let symbol = string.first else {
            return false
        }
        let disallowedCharacters = CharacterSet.punctuationCharacters
        return symbol.unicodeScalars.first { disallowedCharacters.contains($0) } == nil
    }
    
    private func isValidBirthdate(date: String) -> Bool {
        let calendar = Calendar(identifier: .gregorian)
        let formatter = DateHelper.shared.uiFormatterShort
        guard let minimumDate = calendar.date(byAdding: .year, value: -18, to: Date()), let birthDate = formatter.date(from: date) else {
            return false
        }
        return birthDate < minimumDate
    }
    
    private func isValidIban(_ number: String) -> Bool {
        let iban = number.trimmingCharacters(in: .whitespaces).uppercased()
        if !iban.passesMod97Check() {
            return false
        }
        return true
    }
    
}

