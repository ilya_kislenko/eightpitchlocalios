//
//  UserTransfer.swift
//  Input
//
//  Created by 8pitch on 1/11/21.
//

public class UserTransfer: UserTransaction {
    
    public var from: String {
        guard let user = self.fromUser else { return "" }
        let title = Title(rawValue: user.prefixFull)
        
        return "\(title.rawValue.localized) \(user.firstName) \(user.lastName)"
    }
    
    public var to: String {
        guard let user = self.toUser else { return "" }
        let title = Title(rawValue: user.prefixFull)
        
        return "\(title.rawValue.localized) \(user.firstName) \(user.lastName)"
    }
    
    public let companyAddress: String?
    public let companyName: String?
    public let isin: String?
    public let fromUser: TransferUser?
    public let toUser: TransferUser?
    
    public init(amount: NSDecimalNumber?, companyAddress: String?, companyName: String?,
                isin: String?, transactionDate: String?, fromUser: TransferUser?,
                toUser: TransferUser?, shortCut: String?, id: String?) {
        self.companyAddress = companyAddress
        self.companyName = companyName
        self.isin = isin
        self.fromUser = fromUser
        self.toUser = toUser
        super.init(amount: amount, transactionDate: transactionDate, shortCut: shortCut, receiptID: id)
    }
}

public class TransferUser {
    public let prefixFull: String
    public let firstName: String
    public let lastName: String
    
    public init(prefixFull: String?, firstName: String?, lastName: String?) {
        self.prefixFull = prefixFull ?? ""
        self.firstName = firstName ?? ""
        self.lastName = lastName ?? ""
    }
}
