//
//  KYC.swift
//  Input
//
//  Created by 8pitch on 09.08.2020.
//

import Foundation
import RxSwift

public class KYC : Model {
    
    public var providers: [EditCellType: BehaviorSubject<String?>?] {
        [.firstName: try? self.nameAndLastNameProvider.value().firstNameProvider,
         .lastName : try? self.nameAndLastNameProvider.value().lastNameProvider,
         .birth : try? self.birthDayAndPlaceProvider.value().dateProvider,
         .nationality : try? self.personalDetailsProvider.value().nationalityProvider,
         .country : try? self.localityProvider.value().countryProvider,
         .city : try? self.localityProvider.value().cityProvider,
         .zip : try? self.localityProvider.value().zipProvider,
         .address : try? self.addressProvider.value().streetProvider,
         .companyName : try? self.companyProvider.value().companyNameProvider,
         .companyNumber : try? self.companyProvider.value().registrationNumberProvider,
         .accountOwner : try? self.financialInfoProvider.value().accountOwnerProvider,
         .iban : try? self.financialInfoProvider.value().ibanProvider,
         .place : try? self.birthDayAndPlaceProvider.value().placeProvider]
    }
    
    private var forKYC: Bool = false
    
    public var companyProvider : BehaviorSubject<Company>
    public var nameAndLastNameProvider : BehaviorSubject<NameAndLastName>
    public var birthDayAndPlaceProvider : BehaviorSubject<BirthDayAndPlace>
    public var personalDetailsProvider : BehaviorSubject<PersonalDetails>
    public var localityProvider: BehaviorSubject<Locality>
    public var addressProvider: BehaviorSubject<Address>
    public var financialInfoProvider: BehaviorSubject<FinancialInfo>
    public var germanTaxesProvider: BehaviorSubject<TaxInformation>?
    
    public let emailProvider : BehaviorSubject<String?> = .init(value: nil)
    public let phoneProvider : BehaviorSubject<String?> = .init(value: nil)
    
    public var email: String = ""
    public var phone: String = ""
    
    public var isGermanTaxResident: Bool {
        (try? self.germanTaxesProvider?.value().taxResidentProvider.value()) ?? false
    }
    
    public var taxInfo: TaxInformation? {
        try? self.germanTaxesProvider?.value()
    }
    
    public var firstName: String {
        (try? self.nameAndLastNameProvider.value().firstNameProvider.value()) ?? ""
    }
    
    public var lastName: String {
        (try? self.nameAndLastNameProvider.value().lastNameProvider.value()) ?? ""
    }
    
    public var title: Title {
        (try? self.nameAndLastNameProvider.value().title) ?? .mr
    }
    
    public var dateOfBirth: String? {
        (try? self.birthDayAndPlaceProvider.value().dateProvider.value())
    }
    
    public var placeOfBirth: String? {
        (try? self.birthDayAndPlaceProvider.value().placeProvider.value())
    }
    
    public var companyName: String? {
        (try? self.companyProvider.value().companyNameProvider.value())
    }
    
    public var companyNumber: String? {
        (try? self.companyProvider.value().registrationNumberProvider.value())
    }
    
    public var companyType: CompanyType? {
        (try? self.companyProvider.value().companyType)
    }
    
    public var accountOwner: String? {
        (try? self.financialInfoProvider.value().accountOwnerProvider.value())
    }
    
    public var iban: String? {
        (try? self.financialInfoProvider.value().ibanProvider.value())
    }
    
    public var nationality: String? {
        (try? self.personalDetailsProvider.value().nationalityProvider.value())
    }
    
    public var politicallyExposedPerson: Bool? {
        (try? self.personalDetailsProvider.value().politicallyExposedPersonProvider.value())
    }
    
    public var usTaxLiability: Bool? {
        (try? self.personalDetailsProvider.value().usTaxLiabilityProvider.value())
    }
    
    public var city: String? {
        (try? self.localityProvider.value().cityProvider.value())
    }
    
    public var country: String? {
        (try? self.localityProvider.value().countryProvider.value())
    }
    
    public var zip: String? {
        (try? self.localityProvider.value().zipProvider.value())
    }
    
    public var street: String? {
        (try? self.addressProvider.value().streetProvider.value())
    }
    
    public var streetNumber: String? {
        (try? self.addressProvider.value().streetNoProvider.value())
    }
    
    public var dateOfBirthFormatted: String? {
        guard let initialDate = self.dateOfBirth else { return nil }
        let date = DateHelper.shared.convertedInputString(dateString: initialDate )
        return date
    }
    
    private var forSecondLevel: Bool?
    private var forInstitutionalInvestor: Bool?
    
    public init(user: User?) {
        self.companyProvider = .init(value: Company(user: user))
        self.nameAndLastNameProvider = .init(value: NameAndLastName(user: user))
        self.birthDayAndPlaceProvider = .init(value: BirthDayAndPlace(user: user))
        self.personalDetailsProvider = .init(value: PersonalDetails(user: user))
        self.localityProvider = .init(value: Locality(user: user))
        self.addressProvider = .init(value: Address(user: user))
        self.financialInfoProvider = .init(value: FinancialInfo(user: user))
        self.emailProvider.onNext(try? user?.emailProvider.value())
        self.phoneProvider.onNext(try? user?.phoneProvider.value())
        self.germanTaxesProvider = .init(value: TaxInformation(user: user))
        super.init()
    }
    
    override func setup() {
        self.phoneProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.phone = $0
                self?.validate()
            }).disposed(by: self.disposables)
        
        self.emailProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.email = $0
                self?.validate()
            }).disposed(by: self.disposables)
        
        try? self.nameAndLastNameProvider.value().fullNameProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                try? self?.financialInfoProvider.value().accountOwnerProvider.onNext($0)
                try? self?.financialInfoProvider.value().fullName = $0
                self?.validate()
            }).disposed(by: self.disposables)
        
        try? self.companyProvider.value().isValid
            .subscribe(onNext: { [weak self] in
                switch $0 {
                case .failure(let error): self?.isValid.onNext(.failure(error))
                case .success(_):
                    self?.isValid.onNext(.success(false))
                }
            }).disposed(by: self.disposables)
        
        try? self.nameAndLastNameProvider.value().isValid
            .subscribe(onNext: { [weak self] in
                self?.errorTextAlignment = .center
                switch $0 {
                case .failure(let error):
                    switch error {
                    case FirstNameError.titleInvalid:
                        self?.isValid.onNext(.failure(TitleError.titleInvalid))
                    default:
                        self?.isValid.onNext(.failure(error))
                    }
                case .success(_):
                    self?.isValid.onNext(.success(false))
                }
            }).disposed(by: self.disposables)
        
        try? self.birthDayAndPlaceProvider.value().isValid
            .subscribe(onNext: { [weak self] in
                switch $0 {
                case .failure(let error): self?.isValid.onNext(.failure(error))
                case .success(_):
                    self?.isValid.onNext(.success(false))
                }
            }).disposed(by: self.disposables)
        
        try? self.personalDetailsProvider.value().isValid
            .subscribe(onNext: { [weak self] in
                switch $0 {
                case .failure(let error): self?.isValid.onNext(.failure(error))
                case .success(_):
                    self?.isValid.onNext(.success(false))
                }
            }).disposed(by: self.disposables)
        
        try? self.localityProvider.value().isValid
            .subscribe(onNext: { [weak self] in
                self?.errorTextAlignment = .center
                switch $0 {
                case .failure(let error):
                    switch error {
                    case LocalityError.zipEmpty:
                        self?.isValid.onNext(.failure(ZipError.zipEmpty))
                    case LocalityError.zipInvalid:
                        self?.isValid.onNext(.failure(ZipError.zipInvalid))
                    default:
                        self?.isValid.onNext(.failure(error))
                    }
                case .success(_):
                    self?.isValid.onNext(.success(false))
                }
            }).disposed(by: self.disposables)
        
        try? self.addressProvider.value().isValid
            .subscribe(onNext: { [weak self] in
                self?.errorTextAlignment = (try? self?.addressProvider.value().errorTextAlignment) ?? .center
                switch $0 {
                case .failure(let error): self?.isValid.onNext(.failure(error))
                case .success(_):
                    self?.isValid.onNext(.success(false))
                }
            }).disposed(by: self.disposables)
        
        try? self.financialInfoProvider.value().isValid
            .subscribe(onNext: { [weak self] in
                switch $0 {
                case .failure(let error): self?.isValid.onNext(.failure(error))
                case .success(_):
                    self?.isValid.onNext(.success(false))
                }
            }).disposed(by: self.disposables)
        
        try? self.germanTaxesProvider?.value().isValid
            .subscribe(onNext: { [weak self] in
                switch $0 {
                case .failure(let error): self?.isValid.onNext(.failure(error))
                case .success(_):
                    self?.isValid.onNext(.success(false))
                }
            }).disposed(by: self.disposables)
    }
    
    public func validate() {
        guard let level = self.forSecondLevel, let type = self.forInstitutionalInvestor else { return }
        self.validate(forSecondLevel: level, forInstitutionalInvestor: type)
    }
    
    public func validate(forSecondLevel: Bool, forInstitutionalInvestor: Bool, forKYC: Bool = false) {
        self.forKYC = forKYC
        self.forSecondLevel = forSecondLevel
        self.forInstitutionalInvestor = forInstitutionalInvestor
        if forInstitutionalInvestor && !self.validInstitutionalInvestorAccountData() {
            return
        }
        try? self.nameAndLastNameProvider.value().validate()
        switch try? self.nameAndLastNameProvider.value().isValid.value() {
        case .failure(_):
            return
        case .none,
             .some(.success(_)):
            break
        }
        if forSecondLevel && !self.validSecondLevelAccountData() {
            return
        }
        self.isValid.onNext(.success(true))
    }
    
    private func validInstitutionalInvestorAccountData() -> Bool {
        try? self.companyProvider.value().validate()
        switch try? self.companyProvider.value().isValid.value() {
        case .failure(_):
            return false
        case .none,
             .some(.success(_)):
            break
        }
        return true
    }
    
    private func validSecondLevelAccountData() -> Bool {
        try? self.birthDayAndPlaceProvider.value().validate()
        switch try? self.birthDayAndPlaceProvider.value().isValid.value() {
        case .failure(_):
            return false
        case .none,
             .some(.success(_)):
            break
        }
        try? self.personalDetailsProvider.value().validate()
        switch try? self.personalDetailsProvider.value().isValid.value() {
        case .failure(_):
            return false
        case .none,
             .some(.success(_)):
            break
        }
        try? self.localityProvider.value().validate()
        switch try? self.localityProvider.value().isValid.value() {
        case .failure(_):
            return false
        case .none,
             .some(.success(_)):
            break
        }
        try? self.addressProvider.value().validate()
        switch try? self.addressProvider.value().isValid.value() {
        case .failure(_):
            return false
        case .none,
             .some(.success(_)):
            break
        }
        try? self.financialInfoProvider.value().validate()
        switch try? self.financialInfoProvider.value().isValid.value() {
        case .failure(_):
            return false
        case .none,
             .some(.success(_)):
            break
        }
        if !forKYC {
            try? self.germanTaxesProvider?.value().validate()
            switch try? self.germanTaxesProvider?.value().isValid.value() {
            case .failure(_):
                return false
            case .none,
                 .some(.success(_)):
                break
            }
        }
        return true
    }
    
}
