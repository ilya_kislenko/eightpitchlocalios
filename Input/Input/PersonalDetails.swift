//
//  PersonalDetails.swift
//  Input
//
//  Created by 8pitch on 30.07.2020.
//

import Foundation
import RxSwift

public enum PersonalDetailsError : ModelError {
    
    case nationalityEmpty
    
    public var errorDescription : String? {
        switch self {
            case .nationalityEmpty:
                return "input.error.empty".localized
        }
    }
    
}

public enum EmptyError : ModelError {
    
}

public class PersonalDetails : Model {
    
    public let nationalityProvider : BehaviorSubject<String?> = .init(value: nil)
    public let politicallyExposedPersonProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public let usTaxLiabilityProvider : BehaviorSubject<Bool?> = .init(value: nil)
    
    private var nationality : String = ""
    private var politicallyExposedPerson : Bool = false
    private var usTaxLiability: Bool = false
    
    public init(user: User? = nil) {
        self.nationalityProvider.onNext(try? user?.nationalityProvider.value())
        self.politicallyExposedPersonProvider.onNext(try? user?.politicallyExposedPersonProvider.value())
        self.usTaxLiabilityProvider.onNext(try? user?.usTaxLiabilityProvider.value())
        super.init()
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.nationalityProvider.onNext(nil)
        self.politicallyExposedPersonProvider.onNext(false)
        self.usTaxLiabilityProvider.onNext(false)
        self.nationality = ""
        self.politicallyExposedPerson = false
        self.usTaxLiability = false
        self.setup()
    }
    
    override func setup() {
        self.nationalityProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                guard !$0.isEmpty else {
                    self?.isValid.onNext(.failure(PersonalDetailsError.nationalityEmpty))
                    return
                }
                self?.isValid.onNext(.success(false))
                self?.nationality = $0
            }).disposed(by: self.disposables)
        
        self.politicallyExposedPersonProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.politicallyExposedPerson = $0
            }).disposed(by: self.disposables)
        
        self.usTaxLiabilityProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.usTaxLiability = $0
            }).disposed(by: self.disposables)
    }
    
    
    public func validate() {
        let isNationalityValid = !(self.nationality.isEmpty)
        if !isNationalityValid {
            self.isValid.onNext(.failure(PersonalDetailsError.nationalityEmpty))
            return
        }
        if isNationalityValid {
            self.isValid.onNext(.success(true))
        }
    }
    
}

