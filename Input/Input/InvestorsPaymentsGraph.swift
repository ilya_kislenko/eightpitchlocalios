//
//  InvestorsPaymentsGraph.swift
//  Input
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

public class InvestorsPaymentsGraph {
    public var investments: [InvestorsInvestmentElement?] = []
    public var totalMonetaryAmount: Double?

    public init(investments: [InvestorsInvestmentElement?],
         totalMonetaryAmount: Double?) {
        self.investments = investments
        self.totalMonetaryAmount = totalMonetaryAmount
    }
}

public class InvestorsInvestmentElement {
    public var companyName: String?
    public var monetaryAmount: Double?
    public init(companyName: String?,
         monetaryAmount: Double?) {
        self.companyName = companyName
        self.monetaryAmount = monetaryAmount
    }
}
