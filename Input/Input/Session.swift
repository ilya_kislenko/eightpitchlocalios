//
//  Session.swift
//  Input
//
//  Created by 8pitch on 27.07.2020.
//

import UIKit
import RxSwift

public enum LoginState {
    
    case hasMetadata
    case shouldVerifyToken
    case none
    
}

public enum Registration {
    
    case individualInvestor
    case institutionalInvestor
    case none
    
}

public enum AccountGroup: String {
    case privateInvestor = "PRIVATE_INVESTOR"
    case institutionalInvestor = "INSTITUTIONAL_INVESTOR"
    case initiator = "PROJECT_INITIATOR"
}

public enum AccountLevel: String, CaseIterable {
    
    case first = "LEVEL_1"
    case emailConfirmed
    case KYCPassed = "KYC_WEB_ID"
    case WEBIDPassed = "KYC_INTERNAL"
    case second = "LEVEL_2"
    case webIDInProgress = "KYC_FINISH_CALL"
    case questionaryPassed
    case twoFAEnabled
}

public class Session : Model {
    
    public let companyProvider : BehaviorSubject<Company> = .init(value: Company())
    public let nameAndLastNameProvider : BehaviorSubject<NameAndLastName> = .init(value: NameAndLastName())
    public let passwordProvider : BehaviorSubject<Password> = .init(value: Password())
    public let emailProvider : BehaviorSubject<Email> = .init(value: Email())
    public let codeProvider : BehaviorSubject<Code> = .init(value: Code())
    public let phoneProvider : BehaviorSubject<Phone> = .init(value: Phone())
    public let loginProvider : BehaviorSubject<Login> = .init(value: Login())
    public let interactionIDProvider : BehaviorSubject<String?> = .init(value: nil)
    public let remainingAttemptsProvider : BehaviorSubject<Int?> = .init(value: nil)
    public let questionnaireProvider : BehaviorSubject<Questionnaire> = .init(value: Questionnaire())
    public var registrationEndProvider : BehaviorSubject<RegistrationEnd?> = .init(value: RegistrationEnd())
    
    public let userProvider : BehaviorSubject<User> = .init(value: User())
    public let subscribeToEmailsProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public let acceptTermsProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public var KYCProvider: BehaviorSubject<KYC> = .init(value: KYC(user: nil))
    public var personalProfileInfoProvider: BehaviorSubject<KYC> = .init(value: KYC(user: nil))
    
    public var notificationsProvider: NotificationsProvider
    public var uploadFileProvider: UploadFileProvider
    public var projectsProvider: ProjectsProvider
    public var paymentProvider: PaymentProvider
    public var twoFAProvider: TwoFAProvider
    public var verifyPasswordProvider: BehaviorSubject<Login> = .init(value: Login())
    public var qualificationProvider: QualificationProvider
    public var changePasswordProvider: BehaviorSubject<Password> = .init(value: Password(changePasswordMode: true))
    public var quickLoginProvider: QuickLoginProvider
    public var taxInformationProvider: BehaviorSubject<TaxInformation> = .init(value: TaxInformation(user: nil))
    public var projectsFilterProvider: ProjectsFilterProvider
    public var projectsSearchProvider: SearchedProjectsProvider
    public var votingProvider: VotingProvider
    public var secondaryPhone: String?
    
    public var twoFAURL: String?
    
    public func updateModelForPersonalProfile() {
        self.personalProfileInfoProvider = .init(value: KYC(user: self.user))
    }
    
    public func createChangePasswordProviders() {
        self.verifyPasswordProvider = .init(value: Login())
        self.changePasswordProvider = .init(value: Password(changePasswordMode: true))
    }
    
    public func updateProviders() {
        BiometryManager.shared.externalID = self.externalID
        self.KYCProvider = .init(value: KYC(user: self.user))
        guard let twoFactorAuthenticationType = UserVerificationType(rawValue: (try? self.user?.twoFactorAuthenticationTypeProvider.value()) ?? ""),
              let enabled = try? self.user?.twoFactorAuthenticationEnabledProvider.value(),
              enabled else {
            self.verificationTypes = []
            self.twoFAProvider = TwoFAProvider()
            return
        }
        self.verificationTypes = (twoFactorAuthenticationType == .totp) ? [.totp] : [.phone]
        self.twoFAProvider = TwoFAProvider(smsAuth: twoFactorAuthenticationType == .phone, totpAuth: twoFactorAuthenticationType == .totp)
        self.taxInformationProvider = .init(value: TaxInformation(user: self.user))
    }
    
    public var user: User? {
        let user = (try? self.userProvider.value())
        if (try? user?.externalIdProvider.value())?.isEmpty ?? true {
            return nil
        } else {
            return user
        }
    }
    
    public var updatePasswordNeeded: Bool {
        (try? self.loginProvider.value().otp) ?? false
    }
    
    public var rememberMeSelected: Bool {
        (try? self.loginProvider.value().rememberMeSelected) ?? false
    }
    
    public var codeLimitReached: Bool {
        self.remainingAttempts == 0
    }
    
    public var language: String {
        (try? self.user?.languageProvider.value()) ?? "en"
    }
    
    public var remainingAttempts: Int? {
        (try? self.remainingAttemptsProvider.value())
    }
    
    public var changePassword: String {
        (try? self.changePasswordProvider.value().passwordProvider.value()) ?? ""
    }
    
    public var verifyPassword: String {
        (try? self.verifyPasswordProvider.value().passwordProvider.value()?.passwordProvider.value()) ?? ""
    }
    
    public var accountGroup: AccountGroup? {
        guard let group = try? self.user?.groupProvider.value() else {
            return nil
        }
        return AccountGroup(rawValue: group)
    }
    
    public var userName: UserName {
        UserName(lastName: try? self.user?.lastNameProvider.value(), firstName: try? self.user?.firstNameProvider.value(), prefix: try? self.user?.prefixProvider.value())
    }
    
    public var subscribedToEmail : Bool {
        (try? self.user?.subscribeToEmailsProvider.value()) ??
            ((try? self.registrationEndProvider.value()?.subscribeToEmailProvider.value()) ?? false)
    }
    
    public var companyType : CompanyType? {
        (try? self.companyProvider.value().companyType) ?? .none
    }
    
    public var companyName : String {
        (try? self.companyProvider.value().companyName) ?? ""
    }
    
    public var companyNumber : String {
        (try? self.companyProvider.value().registrationNumber) ?? ""
    }
    
    public var phone : String {
        if let phone = self.secondaryPhone, !phone.isEmpty {
            return phone
        } else if let phone = try? self.phoneProvider.value().phone, !phone.isEmpty {
            return phone
        } else if let phone = try? self.loginProvider.value().phoneProvider.value()?.phone, !phone.isEmpty {
            return phone
        } else if let phone = try? self.user?.phoneProvider.value(), !phone.isEmpty {
            return phone
        }
        return ""
    }
    
    public var deviceID : String {
        UIDevice.current.identifierForVendor?.uuidString ?? ""
    }
    
    public var email : String {
        let email = (try? self.emailProvider.value().emailProvider.value()) ?? (try? self.user?.emailProvider.value())
        return email ?? ""
    }
    
    public var isPhoneValid : Bool = false
    public var isEmailValid : Bool = false
    
    public var firstName : String {
        (try? self.nameAndLastNameProvider.value().firstName) ?? ""
    }
    
    public var lastName : String {
        (try? self.nameAndLastNameProvider.value().lastName) ?? ""
    }
    
    public var title : Title? {
        try? self.nameAndLastNameProvider.value().title ?? .mr
    }
    
    public var code : String {
        (try? self.codeProvider.value().code) ?? ""
    }
    
    public var interactionID : String {
        (try? self.interactionIDProvider.value()) ?? ""
    }
    
    public var password : String {
        if let password = try? self.passwordProvider.value().password, !password.isEmpty {
            return password
        } else if let password = try? self.loginProvider.value().passwordProvider.value()?.password, !password.isEmpty {
            return password
        }
        return ""
    }
    
    public var questionnaire : Questionnaire {
        return (try? self.questionnaireProvider.value()) ?? Questionnaire()
    }
    
    public var isEmailConfirmed: Bool {
        (try? self.user?.emailConfirmedProvider.value()) ?? false
    }
    
    public var investorClass: InvestorClass? {
        self.user?.investorClass
    }
    
    public var isVotingOnlyAccount: Bool {
        (try? self.user?.statusProvider.value()) == "LEVEL_2_VOTING_ONLY"
    }
    
    public var level: AccountLevel {
        let level = AccountLevel(rawValue: (try? self.user?.statusProvider.value()) ?? "LEVEL_1") ?? .first
        if level == .second {
            return .second
        } else if self.questionaryPassed {
            return .questionaryPassed
        } else if level == .KYCPassed {
            return .KYCPassed
        } else if level == .WEBIDPassed || level == .webIDInProgress {
            return .WEBIDPassed
        } else if self.twoFAEnabled && isEmailConfirmed {
            return .twoFAEnabled
        } else if isEmailConfirmed {
            return .emailConfirmed
        } else {
            return level
        }
    }
    
    public var twoFAEnabled: Bool {
        guard let twoFAEnabled = try? user?.twoFactorAuthenticationEnabledProvider.value() else { return false }
        return twoFAEnabled
    }
    
    public var questionaryPassed: Bool {
        (try? user?.investmentExperienceQuestionnaireProvider.value()?.knowledgeProvider.value()) != nil
    }
    
    public func countryCode(for countryName: String?) -> String? {
        let country = PickerData(name: countryName ?? "", code: "")
        return country.iso
    }
    
    public var addressLine1: String {
        (try? self.user?.addressProvider.value()?.addressLine1Provider.value()) ?? ""
    }
    
    public var addressLine2: String {
        (try? self.user?.addressProvider.value()?.addressLine2Provider.value()) ?? ""
    }
    
    public var region: String {
        (try? self.user?.addressProvider.value()?.regionProvider.value()) ?? ""
    }
    
    public var sex: String {
        (try? self.user?.sexProvider.value()) ?? ""
    }
    
    public var registrationType : Registration
    
    public var deletionReason: String?
    
    public init(registrationType : Registration) {
        self.registrationType = registrationType
        self.projectsProvider = ProjectsProvider()
        self.notificationsProvider = NotificationsProvider()
        self.paymentProvider = PaymentProvider()
        self.twoFAProvider = TwoFAProvider()
        self.quickLoginProvider = QuickLoginProvider()
        self.projectsFilterProvider = ProjectsFilterProvider()
        self.projectsSearchProvider = SearchedProjectsProvider()
        self.qualificationProvider = QualificationProvider()
        self.uploadFileProvider = UploadFileProvider()
        self.votingProvider = VotingProvider()
        super.init()
        self.extractTokens()
    }
    
    // MARK: API Related
    
    // TODO: move to a separate entity
    public let accessTokenProvider : BehaviorSubject<String?> = .init(value: nil)
    public let refreshTokenProvider : BehaviorSubject<String?> = .init(value: nil)
    public var shouldRefreshTokens : ((Bool) -> Void)?
    // WebID SDK-related, should be moved to another entity
    public let webIDUserActionIDProvider : BehaviorSubject<String?> = .init(value: nil)
    
    public var loginState : LoginState = .none
    public var verificationTypes : [VerificationType] = []
    
    override func setup() {
        self.accessTokenProvider
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                if $0 == nil {
                    self?.shouldRefreshTokens?(true)
                }
            }).disposed(by: self.disposables)
    }
}

public struct UserName {
    
    public var fullName: String {
        "\(firstName ?? "") \(lastName ?? "")"
    }
    
    public let lastName: String?
    public let firstName: String?
    public let prefix: String?
}

public extension Session {
    
    var refreshToken : String {
        (try? self.refreshTokenProvider.value()) ?? ""
    }
    
    var accessToken : String {
        (try? self.accessTokenProvider.value()) ?? ""
    }
    
    var accountGroupValue : String {
        (try? self.user?.groupProvider.value()) ?? ""
    }
    
    var externalID : String {
        (try? self.user?.externalIdProvider.value()) ?? ""
    }
    
    var webIDUserAction : String {
        let actionID = (try? self.webIDUserActionIDProvider.value()) ?? (try? self.user?.actionIdProvider.value())
        return actionID ?? ""
    }
    
    func createTwoFAURL(_ qrURL: String?) {
        guard let qrURL = qrURL else { return }
        let strings = qrURL.split(separator: "=")
        self.twoFAURL = String(strings.last ?? "").replacingOccurrences(of: "%3F", with: "?")
        self.twoFAURL = self.twoFAURL?.replacingOccurrences(of: "%3D", with: "=").replacingOccurrences(of: " ", with: "")
    }
    
    func extractTokens() {
        self.refreshTokenProvider.onNext(BiometryManager.shared.refreshToken)
        self.accessTokenProvider.onNext(BiometryManager.shared.accessToken)
    }
    
    func saveTokens() {
        BiometryManager.shared.accessToken = self.accessToken
        BiometryManager.shared.refreshToken = self.refreshToken
    }
    
    func resetTokens() {
        self.accessTokenProvider.onNext("")
        self.refreshTokenProvider.onNext("")
        BiometryManager.shared.accessToken = ""
        BiometryManager.shared.refreshToken = ""
    }
    
}
