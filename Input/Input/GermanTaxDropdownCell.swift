//
//  GermanTaxDropdownCell.swift
//  8Pitch
//
//  Created by 8pitch on 13.01.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class GermanTaxDropdownCell: CellModel<TaxInformation> {
    
    @IBOutlet weak var textField: ChurchTaxAttributeTextField!
    
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    class override var reuseIdentifier : String {
        return "GermanTaxDropdownCell"
    }
    
    override var model : BehaviorSubject<TaxInformation>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.churchTaxAttributeProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
        }
    }
    
    public func validate() {
        guard let model = try? self.model?.value() else { return }
        
        model.isValid
            .subscribe(onNext: { [weak self] event in
                switch event {
                case .failure(_ as AttributeTaxError):
                    self?.textField.setState(.error)
                default:
                    self?.textField.setState(.success)
                }
            }).disposed(by: self.disposables)
    }
    
    override func setup() {
        super.setup()
        self.textField.titles = ChurchTaxAttribute.allCases.map { $0.rawValue.localized }
        
        self.textField.rx
            .controlEvent(.editingDidEnd)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .subscribe(onNext: { [weak self] in
                try? self?.model?.value().churchTaxAttributeProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 80
    }
}
