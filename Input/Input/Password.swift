//
//  Password.swift
//  Input
//
//  Created by 8pitch on 22.07.2020.
//

import RxSwift

public enum PasswordError : ModelError {
    case passwordWrong
    case passwordEmpty
    case passwordInvalid
    case confirmationInvalid
    
    public var errorDescription : String? {
        switch self {
        case .passwordWrong:
            return "remote.error.response.login.wrongPassword".localized
        case .passwordEmpty:
            return "input.error.empty".localized
        case .passwordInvalid:
            return "input.error.password.format".localized
        case .confirmationInvalid:
            return "input.error.password.match".localized
        }
    }
}

public enum ConfirmPasswordError: ModelError {
    case confirmationInvalid
    
    public var errorDescription : String? {
        switch self {
        case .confirmationInvalid:
            return "input.error.password.match".localized
        }
    }
    
}

public class Password : Model {
    
    public let passwordProvider : BehaviorSubject<String?> = .init(value: nil)
    public let confirmationProvider : BehaviorSubject<String?> = .init(value: nil)
    
    var singleMode : Bool
    var changePasswordMode: Bool
    
    
    var password : String
    private var confirmation : String
    
    public init(password: String = "", confirmation: String = "", singleMode: Bool = false, changePasswordMode: Bool = false) {
        self.password = password
        self.confirmation = confirmation
        self.singleMode = singleMode
        self.changePasswordMode = changePasswordMode
        super.init()
    }
    
    override func setup() {
        self.passwordProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.password = $0
                self?.validate()
            }).disposed(by: self.disposables)
        self.confirmationProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.confirmation = $0
                self?.validate()
            }).disposed(by: self.disposables)
    }
    
    override func validate(string: String) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: "^(?=.*[A-Z])(?=.*[!@#$&*À-ž])(?=.*[0-9]).{8,}$", options: []) else
        {
            return false
        }
        let searchable = NSRange(location: 0, length: string.count)
        let found = regex.rangeOfFirstMatch(in: string, options: [], range: searchable)
        return found == searchable
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.passwordProvider.onNext(nil)
        self.confirmationProvider.onNext(nil)
        self.password = ""
        self.confirmation = ""
        self.setup()
    }
    
    func validate() {
        if self.singleMode {
            self.validateSingleMode()
            return
        }
        
        let isPasswordValid = self.validate(string: self.password)
        let isConfirmationValid = self.password == self.confirmation
        if isPasswordValid && isConfirmationValid && !self.singleMode {
            self.isValid.onNext(.success(true))
        }
        if !isPasswordValid {
            self.isValid.onNext(.failure(PasswordError.passwordInvalid))
        } else if !isConfirmationValid {
            self.isValid.onNext(.failure(self.changePasswordMode ? ConfirmPasswordError.confirmationInvalid : PasswordError.confirmationInvalid))
        }
    }
    
    public func validateSingleMode() {
        if self.password.isEmpty {
            self.isValid.onNext(.failure(PasswordError.passwordEmpty))
        } else {
            self.isValid.onNext(.success(true))
        }
    }
}
