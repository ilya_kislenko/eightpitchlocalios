//
//  InvestorClass.swift
//  Input
//
//  Created by 8pitch on 2/1/21.
//

import Foundation

public enum InvestorClass: String {
    case unqualified = "UNQUALIFIED"
    case class1 = "CLASS_1"
    case class2 = "CLASS_2"
    case class3 = "CLASS_3"
    case class4 = "CLASS_4"
    case class5 = "CLASS_5"
}
