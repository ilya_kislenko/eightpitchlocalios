//
//  BirthPlaceAndDay.swift
//  Input
//
//  Created by 8pitch on 28.07.2020.
//

import Foundation
import RxSwift

public enum BirthDayError : ModelError {
    
    case dateOfBirthInvalid
    case dateOfBirthEmpty
    
    public var errorDescription : String? {
        switch self {
            case .dateOfBirthInvalid:
                return "kyc.input.error.birthdate".localized
            default:
                return "input.error.empty".localized
        }
    }
}

public enum BirthPlaceError : ModelError {
    
    case placeOfBirthInvalid
    case placeOfBirthEmpty
    
    public var errorDescription : String? {
        switch self {
            case .placeOfBirthInvalid:
                return "kyc.input.error.birthplace".localized
            default:
                return "input.error.empty".localized
        }
    }
}

public class BirthDayAndPlace: Model {
    
    public let dateProvider : BehaviorSubject<String?> = .init(value: nil)
    public let placeProvider : BehaviorSubject<String?> = .init(value: nil)
    
    internal var dateOfBirth : String = ""
    internal var placeOfBirth : String = ""
    
    private let placePattern = "[a-zA-ZÀ-ž' -]"
    
    public var valueProviders : [ KeyPath<BirthDayAndPlace, PublishSubject<String?>> : (() -> Void)? ] = [:]
    
    public init(user: User? = nil) {
        super.init()
        self.dateProvider.onNext(user?.formattedDate)
        self.placeProvider.onNext(try? user?.placeOfBirthProvider.value())
    }
    
    override func setup() {
        self.dateProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                _ = self?.validate(date: $0)
                self?.dateOfBirth = $0
            }).disposed(by: self.disposables)
        self.placeProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                _ = self?.validate(string: $0)
                self?.placeOfBirth = $0
            }).disposed(by: self.disposables)
    }
    
    public func validate() {
        let isDateOfBirthValid = self.validate(date: self.dateOfBirth)
        let isPlaceOfBirthValid = self.validate(string: self.placeOfBirth)
        if self.dateOfBirth.isEmpty {
            self.isValid.onNext(.failure(BirthDayError.dateOfBirthEmpty))
            return
        }
        if !isDateOfBirthValid {
            self.isValid.onNext(.failure(BirthDayError.dateOfBirthInvalid))
            return
        }
        if self.placeOfBirth.isEmpty {
            self.isValid.onNext(.failure(BirthPlaceError.placeOfBirthEmpty))
            return
        }
        if !isPlaceOfBirthValid {
            self.isValid.onNext(.failure(BirthPlaceError.placeOfBirthInvalid))
            return
        }
        if isDateOfBirthValid && isPlaceOfBirthValid {
            self.isValid.onNext(.success(true))
        }
    }
    
    override func validate(string: String) -> Bool {
        guard !string.isEmpty else {
            self.isValid.onNext(.failure(BirthPlaceError.placeOfBirthEmpty))
            return false
        }
        guard let regex = try? NSRegularExpression(pattern: self.placePattern, options: []) else
        {
            return false
        }
        let searchable = NSRange(location: 0, length: string.count)
        let result = regex.matches(in: string, options:[], range: searchable).count == string.count
        if result {
            self.isValid.onNext(.success(false))
            return true
        } else {
            self.isValid.onNext(.failure(BirthPlaceError.placeOfBirthInvalid))
            return false
        }
    }
    
    private func validate(date: String) -> Bool {
        guard !date.isEmpty else {
            self.isValid.onNext(.failure(BirthDayError.dateOfBirthEmpty))
            return false
        }
        let calendar = Calendar(identifier: .gregorian)
        let formatter = DateHelper.shared.uiFormatterShort
        guard let minimumDate = calendar.date(byAdding: .year, value: -18, to: Date()), let birthDate = formatter.date(from: date) else {
            return false
        }
        if birthDate < minimumDate {
            self.isValid.onNext(.success(false))
            return true
        } else {
            self.isValid.onNext(.failure(BirthDayError.dateOfBirthInvalid))
            return false
        }
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.dateProvider.onNext(nil)
        self.placeProvider.onNext(nil)
        self.dateOfBirth = ""
        self.placeOfBirth = ""
        self.valueProviders = [:]
        self.setup()
    }
}
