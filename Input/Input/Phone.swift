//
//  Phone.swift
//  Input
//
//  Created by 8pitch on 27.07.2020.
//

import Foundation
import RxSwift

public enum PhoneError : ModelError {
    
    case phoneDuplicate
    case phoneInvalid
    case phoneEmpty
    case codeInvalid
    
    public var errorDescription : String? {
        switch self {
        case .phoneDuplicate:
            return "remote.error.response.phone.duplicate".localized
        case .phoneInvalid:
            return "input.error.phone.invalid".localized
        case .phoneEmpty:
            return "input.error.empty".localized
        case .codeInvalid:
            return "input.error.empty".localized
            
        }
    }
    
}

public class Phone : Model {
    
    public let phoneProvider : BehaviorSubject<String?> = .init(value: nil)
    public let codeProvider : BehaviorSubject<PickerData?> = .init(value: DataSource.Germany)
    private let germanPrefixes: [String] = ["1521", "157", "159", "163", "176",
                                            "177", "178", "179", "1579", "151",
                                            "160", "170", "171", "175", "1529",
                                            "152", "162", "172", "173", "174"]
    
    public var phone : String {
        if self._phone.isEmpty {
            return ""
        }
        return self.code + self._phone
    }
    
    private var _phone : String
    private var code : String
    
    public init(phone: String = "", code: String = "") {
        self._phone = phone
        self.code = code
        super.init()
    }
    
    override func setup() {
        self.codeProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.code = $0.code
                self?.validateCode()
            }).disposed(by: self.disposables)
        
        self.phoneProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?._phone = $0
                self?.validatePhone()
            }).disposed(by: self.disposables)
    }
    
    override func validate(string: String) -> Bool {
        !string.isEmpty
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.phoneProvider.onNext(nil)
        self.codeProvider.onNext(DataSource.Germany)
        self._phone = ""
        self.code = ""
        self.setup()
    }
    
    private func validateCode() {
        let isCodeValid = self.validate(string: self.code)
        if !isCodeValid {
            self.isValid.onNext(.failure(PhoneError.codeInvalid))
        } else if !self._phone.isEmpty {
            self.validatePhone()
        }
    }
    
    private func validatePhone() {
        let isPhoneNotEmpty = self.validate(string: self._phone)
        if !isPhoneNotEmpty {
            self.isValid.onNext(.failure(PhoneError.phoneEmpty))
        } else if self.code == DataSource.Germany.code &&
                    (self.germanPrefixes.first(where:{self._phone.hasPrefix($0)}) == nil ||
                        self._phone.count < 10) {
            self.isValid.onNext(.failure(PhoneError.phoneInvalid))
        } else {
            self.isValid.onNext(.success(true))
        }
    }
    
    public func validate() {
        let isPhoneValid = self.validate(string: self._phone)
        let isCodeValid = self.validate(string: self.code)
        if !isPhoneValid {
            self.isValid.onNext(.failure(PhoneError.phoneEmpty))
            return
        }
        let isNotRightPrefix = self.germanPrefixes.first(where:{self._phone.hasPrefix($0)}) == nil
        if self.code == DataSource.Germany.code &&
            (isNotRightPrefix || self._phone.count < 10) {
            self.isValid.onNext(.failure(PhoneError.phoneInvalid))
            return
        }
        if !isCodeValid {
            self.isValid.onNext(.failure(PhoneError.codeInvalid))
            return
        }
        if isPhoneValid && isCodeValid {
            self.isValid.onNext(.success(true))
        }
    }
    
}
