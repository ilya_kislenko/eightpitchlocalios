//
//  Code.swift
//  Input
//
//  Created by 8pitch on 28.07.2020.
//

import Foundation
import RxSwift

public enum CodeError : ModelError {
    
    case wrongCode
    
    public var errorDescription : String? {
        switch self {
            case .wrongCode:
                return "remote.error.response.code.invalid".localized
        }
    }
    
}

public class Code : Model {
    
    public let codeProvider : BehaviorSubject<String?> = .init(value: nil)
    public let interactionIDProvider : BehaviorSubject<String?> = .init(value: nil)
    
    internal var code : String
    
    public init(code: String = "") {
        self.code = code
    }
    
    override func setup() {
        if !self.codeProvider.hasObservers {
            self.codeProvider
                .compactMap { $0 }
                .subscribe(onNext: { [weak self] in
                    guard !$0.isEmpty else {
                        self?.isValid.onNext(.success(false))
                        return
                    }
                    self?.code = $0
                    self?.validate()
                }).disposed(by: self.disposables)
        }
    }
    
    override func validate(string: String) -> Bool {
        string.count == 6
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.codeProvider.onNext(nil)
        self.interactionIDProvider.onNext(nil)
        self.code = ""
        self.setup()
    }
    
    func validate() {
        let isCodeValid = self.validate(string: self.code)
        if isCodeValid {
            self.isValid.onNext(.success(true))
        }
        if !isCodeValid {
            self.isValid.onNext(.success(false))
        }
    }
    
}
