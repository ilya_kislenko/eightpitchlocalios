//
//  OnlinePaymentSubmitInfo.swift
//  Input
//
//  Created by 8pitch on 9/8/20.
//

public enum PaymentResponseStatus: String {
    case ok = "OK"
    case paymentGatewayError = "PAYMENT_GATEWAY_ERROR"
}

public class OnlinePaymentSubmitInfo {
    public var paymentUrl: String?
    public var responseStatus: PaymentResponseStatus?

    public init(paymentUrl: String?, responseStatus: String?) {
        self.paymentUrl = paymentUrl
        if let responseStatusString = responseStatus {
            self.responseStatus = PaymentResponseStatus(rawValue: responseStatusString)
        } else {
            self.responseStatus = nil
        }
    }
}
