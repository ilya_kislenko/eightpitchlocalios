//
//  InvestmentsAmountValues.swift
//  Input
//
//  Created by 8pitch on 07.09.2020.
//

public struct InvestmentAmountValues {
    public var maxValue: NSDecimalNumber
    public var minValue: NSDecimalNumber
    public var investmentStep: NSDecimalNumber
    public var investorClassLimit: NSDecimalNumber?
    public var tokenNominalValue: NSDecimalNumber
    public var feeRate: NSDecimalNumber
    public var tokenAmount: Int
    public var feeMultiplier = NSDecimalNumber(100)
    public var shortCut: String
    
    public var minValueInEUR: NSDecimalNumber {
        self.minValue.multiplying(by: tokenNominalValue)
    }
    
    public var fee: NSDecimalNumber {
        self.feeRate.dividing(by: 100)
    }
    
    public init(max: NSDecimalNumber = 0, min: NSDecimalNumber = 0, step: NSDecimalNumber = 0, nominal: NSDecimalNumber = 0, feeRate: NSDecimalNumber = 0, tokensAmount: Int? = 0, shortCut: String,
                investorClassLimit: NSDecimalNumber?) {
        self.maxValue = max
        self.minValue = min
        self.investmentStep = step
        self.tokenNominalValue = nominal
        self.feeRate = feeRate
        self.tokenAmount = tokensAmount ?? 0
        self.shortCut = shortCut
        self.investorClassLimit = investorClassLimit
    }
}
