//
//  DataSource.swift
//  Input
//
//  Created by 8pitch on 07.08.2020.
//

public struct PickerData: Comparable {
    
    public static func < (lhs: PickerData, rhs: PickerData) -> Bool {
        lhs.name < rhs.name
    }
    
    public let name: String
    public let code: String
    
    public var iso: String? {
        Locale.current.isoCode(for: name)
    }
}

public class DataSource {
    
    static public let Germany = PickerData(name: "Germany".localized, code: "49")
    
    static public var countries: [PickerData] {
        self.countriesData.sorted()
    }
    
    static private let countriesData: [PickerData] = [PickerData(name: "Austria".localized, code: "43"),
                                              PickerData(name: "Belgium".localized, code: "32"),
                                              PickerData(name: "Bulgaria".localized, code: "359"),
                                              PickerData(name: "Croatia".localized, code: "385"),
                                              PickerData(name: "Cyprus".localized, code: "357"),
                                              PickerData(name: "Czechia".localized, code: "420"),
                                              PickerData(name: "Denmark".localized, code: "45"),
                                              PickerData(name: "Estonia".localized, code: "372"),
                                              PickerData(name: "Finland".localized, code: "358"),
                                              PickerData(name: "France".localized, code: "33"),
                                              PickerData(name: "Germany".localized, code: "49"),
                                              PickerData(name: "Greece".localized, code: "30"),
                                              PickerData(name: "Hungary".localized, code: "36"),
                                              PickerData(name: "Iceland".localized, code: "354"),
                                              PickerData(name: "Ireland".localized, code: "353"),
                                              PickerData(name: "Italy".localized, code: "39"),
                                              PickerData(name: "Latvia".localized, code: "371"),
                                              PickerData(name: "Liechtenstein".localized, code: "423"),
                                              PickerData(name: "Lithuania".localized, code: "370"),
                                              PickerData(name: "Luxembourg".localized, code: "352"),
                                              PickerData(name: "Malta".localized, code: "356"),
                                              PickerData(name: "Netherlands".localized, code: "31"),
                                              PickerData(name: "Norway".localized, code: "47"),
                                              PickerData(name: "Poland".localized, code: "48"),
                                              PickerData(name: "Portugal".localized, code: "351"),
                                              PickerData(name: "Romania".localized, code: "40"),
                                              PickerData(name: "Slovakia".localized, code: "421"),
                                              PickerData(name: "Slovenia".localized, code: "386"),
                                              PickerData(name: "Spain".localized, code: "34"),
                                              PickerData(name: "Sweden".localized, code: "46"),
                                              PickerData(name: "Switzerland".localized, code: "41")
    ]
    
    static public let deletionReasons: [PickerData] = [PickerData(name: "profile.deletion.reason.1".localized, code: ""),
                                                       PickerData(name: "profile.deletion.reason.2".localized, code: ""),
                                                       PickerData(name: "profile.deletion.reason.3".localized, code: ""),
                                                       PickerData(name: "profile.deletion.reason.4".localized, code: ""),
                                                       PickerData(name: "profile.deletion.reason.5".localized, code: "")
    ]
    
    static public let qualificationType: [PickerData] = [PickerData(name: "Unspecified", code: "UNSPECIFIED"),
                                                         PickerData(name: "Class 1", code: "CLASS_1"),
                                                         PickerData(name: "Class 2", code: "CLASS_2"),
                                                         PickerData(name: "Class 3", code: "CLASS_3"),
                                                         PickerData(name: "Class 4", code: "CLASS_4"),
                                                         PickerData(name: "Class 5", code: "CLASS_5")
    ]
}
