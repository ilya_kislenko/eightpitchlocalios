//
//  RegistrationEnd.swift
//  Input
//
//  Created by 8pitch on 02.08.2020.
//

import Foundation
import RxSwift

public enum RegistrationEndError : ModelError {
    
    case didNotAcceptTerms
    
    public var errorDescription: String? {
        switch self {
            case .didNotAcceptTerms:
                return "input.error.registrationend.mustacceptterms".localized
        }
    }
    
}

public class RegistrationEnd : Model {
    
    public let subscribeToEmailProvider : BehaviorSubject<Bool?> = .init(value: true)
    public let acceptTermsProviider : BehaviorSubject<Bool?> = .init(value: nil)
    
    internal var subscribeToEmail : Bool
    internal var acceptTerms : Bool
    
    public init(subscribeToEmail: Bool = false, acceptTerms: Bool = false) {
        self.subscribeToEmail = subscribeToEmail
        self.acceptTerms = acceptTerms
    }
    
    override func setup() {
        self.subscribeToEmailProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.subscribeToEmail = $0
                self?.validate()
            }).disposed(by: self.disposables)
        self.acceptTermsProviider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.acceptTerms = $0
                self?.validate()
            }).disposed(by: self.disposables)
    }
    
    private func validate() {
        if self.acceptTerms {
            self.isValid.onNext(.success(true))
        } else {
            self.isValid.onNext(.failure(RegistrationEndError.didNotAcceptTerms))
        }
    }
    
}
