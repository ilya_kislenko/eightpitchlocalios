//
//  ClassicBankTransferPaymentInfo.swift
//  Input
//
//  Created by 8pitch on 9/8/20.
//

import Foundation

public enum ResponseStatus: String {
    case success = "OK"
    case error = "PAYMENT_GATEWAY_ERROR"
}

public struct BankTransferPaymentInfo {
    public var bic: String?
    public var iban: String?
    public var transactionInfo: TransactionInfo?
    
    public init(bic: String?, iban: String?, transactionInfo: TransactionInfo?) {
        self.bic = bic
        self.iban = iban
        self.transactionInfo = transactionInfo
    }
}

public struct TransactionInfo {
    public var referenceNumber: String?
    public var responseStatus: ResponseStatus?
    
    public init(referenceNumber: String?, responseStatus: String?) {
        self.referenceNumber = referenceNumber
        self.responseStatus = ResponseStatus(rawValue: responseStatus ?? "")
    }
}
