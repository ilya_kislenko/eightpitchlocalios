//
//  Address.swift
//  Input
//
//  Created by 8pitch on 30.07.2020.
//

import Foundation
import RxSwift

public enum AddressError : ModelError {
    
    case streetEmpty
    case streetInvalid
    case numberEmpty
    case numberInvalid
    
    public var errorDescription : String? {
        switch self {
        case .streetInvalid:
            return "kyc.input.error.street".localized
        case .streetEmpty,
             .numberEmpty:
            return "input.error.empty".localized
        case .numberInvalid:
            return "kyc.input.error.postalCode".localized
        }
    }
}


public class Address : Model {
        
    public let addressLine1Provider : BehaviorSubject<String?> = .init(value: nil)
    public let addressLine2Provider : BehaviorSubject<String?> = .init(value: nil)
    public let cityProvider : BehaviorSubject<String?> = .init(value: nil)
    public let countryProvider : BehaviorSubject<String?> = .init(value: nil)
    public let regionProvider : BehaviorSubject<String?> = .init(value: nil)
    public let streetProvider : BehaviorSubject<String?> = .init(value: nil)
    public let streetNoProvider : BehaviorSubject<String?> = .init(value: nil)
    public let zipProvider : BehaviorSubject<String?> = .init(value: nil)
    
    private let streetPattern = "^[A-Za-zÀ-ž\u{00C0}-\u{00FF}][A-Za-zÀ-ž\u{00C0}-\u{00FF}'-]*+([ A-Za-zÀ-ž\u{00C0}-\u{00FF}][A-Za-zÀ-ž\u{00C0}-\u{00FF}'-]+)*"
    private let numberPattern = "[A-Za-z0-9]"
    
    var addressLine1: String = ""
    var addressLine2: String = ""
    var city: String = ""
    var country: String = ""
    var region: String = ""
    var street: String = ""
    var streetNo: String = ""
    var zip: String = ""
    
    public init(user: User? = nil) {
        self.streetProvider.onNext(try? user?.addressProvider.value()?.streetProvider.value())
        self.streetNoProvider.onNext(try? user?.addressProvider.value()?.streetNoProvider.value())
        super.init()
    }
    
    override func validate(string: String) -> Bool {
        guard !string.isEmpty else {
            self.errorTextAlignment = .left
            self.isValid.onNext(.failure(AddressError.streetEmpty))
            return false
        }
        guard let regex = try? NSRegularExpression(pattern: self.streetPattern, options: []) else
        {
            return false
        }
        let searchable = NSRange(location: 0, length: string.count)
        let found = regex.rangeOfFirstMatch(in: string, options: [], range: searchable)
        if found == searchable {
            self.isValid.onNext(.success(false))
            return true
        } else {
            self.errorTextAlignment = .left
            self.isValid.onNext(.failure(AddressError.streetInvalid))
            return false
        }
    }
    
    private func validate(_ number: String) -> Bool {
        if number.isEmpty {
            self.errorTextAlignment = .right
            self.isValid.onNext(.failure(AddressError.numberEmpty))
            return false
        }
        guard let regex = try? NSRegularExpression(pattern: self.numberPattern, options: []) else
        {
            return false
        }
        let searchable = NSRange(location: 0, length: number.count)
        let result = regex.matches(in: number, options:[], range: searchable).count == number.count
        if result {
            self.isValid.onNext(.success(false))
            return true
        } else {
            self.errorTextAlignment = .right
            self.isValid.onNext(.failure(AddressError.numberInvalid))
            return false
        }
    }
    
    override func setup() {
        self.streetProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                _ = self?.validate(string: $0)
                self?.street = $0
            }).disposed(by: self.disposables)
        
        self.streetNoProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                _ = self?.validate($0)
                self?.streetNo = $0
            }).disposed(by: self.disposables)
    }
    
    public func validate() {
        let isStreetValid = self.validate(string: self.street)
        if !isStreetValid {
            return
        }
        let isNumberValid = self.validate(self.streetNo)
        if !isNumberValid {
            return
        }
        if isStreetValid && isNumberValid {
            self.isValid.onNext(.success(true))
        }
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.streetProvider.onNext(nil)
        self.streetNoProvider.onNext(nil)
        self.street = ""
        self.streetNo = ""
        self.setup()
    }
    
}

