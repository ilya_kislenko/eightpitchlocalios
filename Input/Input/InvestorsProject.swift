//
//  InvestorsProjects.swift
//  Input
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

public protocol ProjectInfo: class {
    func projectIdentifier() -> Int?
    var thumbnailId: String? { get }
}

public class InvestorsProject: ProjectInfo {
    private struct Constants {
        static let imageFileType = "PROJECT_THUMBNAIL"
    }
    public var thumbnailId: String? {
        if let file = (self.projectInfo?.projectPageFiles.filter { $0?.fileType == Constants.imageFileType })?.first??.file {
            return file.identifier
        } else {
            return nil
        }
    }
    public var investmentsInfo: InvestorsInvestmentsInfo?
    public var projectFiles: [InvestorsProjectFile?]
    public var projectInfo: InvestorsProjectInfo?
    public var projectStatus: ProjectStatus?

    public init(investmentsInfo: InvestorsInvestmentsInfo?,
                projectFiles: [InvestorsProjectFile?],
                projectInfo: InvestorsProjectInfo?,
                projectStatus: String?) {
        self.investmentsInfo = investmentsInfo
        self.projectFiles = projectFiles
        self.projectInfo = projectInfo
        if let statusString = projectStatus,
           let status = ProjectStatus(rawValue: statusString) {
            self.projectStatus = status
        } else {
            self.projectStatus = nil
        }
    }

    public func projectIdentifier() -> Int? {
        return self.projectInfo?.identifier
    }
}

public class InvestorsInvestmentsInfo {
    public var investments: [InvestorsInvestment?]
    public var lastInvestmentStatus: InvestmentStatus?
    public var totalAmountOfTokens: Double?

    public init(investments: [InvestorsInvestment?],
                lastInvestmentStatus: String?,
                totalAmountOfTokens: Double?) {
        self.investments = investments
        if let lastInvestmentStatus = lastInvestmentStatus {
            self.lastInvestmentStatus = InvestmentStatus(rawValue: lastInvestmentStatus)
        }
        self.totalAmountOfTokens = totalAmountOfTokens
    }
}

public class InvestorsInvestment {
    public var amount: Double?
    public var createDateTime: Date?
    public var status: String?

    public init(amount: Double?,
                createDateTime: String?,
                status: String?) {
        self.amount = amount
        let formatter = DateHelper.shared.backendFormatter
        self.createDateTime = formatter.date(from: createDateTime ?? "")?.withCurrentTimeZoneOffset()
        self.status = status
    }
}

public class InvestorsProjectFile {
    public var file: InvestorsProjectFileFile?
    public var fileType: String?
    public var identifier: Int?

    public init(file: InvestorsProjectFileFile?,
                fileType: String?,
                identifier: Int?) {
        self.file = file
        self.fileType = fileType
        self.identifier = identifier
    }
}

public class InvestorsProjectFileFile {
    public var customFileName, identifier, originalFileName, uniqueFileName: String?

    public init(customFileName: String?,
                identifier: String?,
                originalFileName: String?,
                uniqueFileName: String?) {
        self.customFileName = customFileName
        self.identifier = identifier
        self.originalFileName = originalFileName
        self.uniqueFileName = uniqueFileName
    }
}

public class InvestorsProjectInfo {
    public var companyName, projectInfoDescription: String?
    public var financialInformation: InvestorsFinancialInformation?
    public var graph: InvestorsGraph?
    public var identifier: Int?
    public var projectPageFiles: [InvestorsProjectPageFile?]
    public var projectPageURL: String?
    public var tokenParametersDocument: InvestorsTokenParametersDocument?
    public var videoURL: String?

    public init(companyName: String?,
                projectInfoDescription: String?,
                financialInformation: InvestorsFinancialInformation?,
                graph: InvestorsGraph?,
                identifier: Int?,
                projectPageFiles: [InvestorsProjectPageFile?],
                projectPageURL: String?,
                tokenParametersDocument: InvestorsTokenParametersDocument?,
                videoURL: String?) {
        self.companyName = companyName
        self.projectInfoDescription = projectInfoDescription
        self.financialInformation = financialInformation
        self.graph = graph
        self.identifier = identifier
        self.projectPageFiles = projectPageFiles
        self.projectPageURL = projectPageURL
        self.tokenParametersDocument = tokenParametersDocument
        self.videoURL = videoURL
    }
}

public class InvestorsFinancialInformation {
    public var investmentSeries, nominalValue, typeOfSecurity: String?

    public init(investmentSeries: String?,
                nominalValue: String?,
                typeOfSecurity: String?) {
        self.investmentSeries = investmentSeries
            self.nominalValue = nominalValue
            self.typeOfSecurity = typeOfSecurity
    }
}

public class InvestorsGraph {
    public var currentFundingSum: Decimal?
    public var currentFundingSumInTokens, hardCap, hardCapInTokens: Int?
    public var softCap, softCapInTokens: Int?

    public init(currentFundingSum: Decimal?,
                currentFundingSumInTokens: Int?,
                hardCap: Int?,
                hardCapInTokens: Int?,
                softCap: Int?,
                softCapInTokens: Int?) {
        self.currentFundingSum = currentFundingSum
        self.currentFundingSumInTokens = currentFundingSumInTokens
        self.hardCap = hardCap
        self.hardCapInTokens = hardCapInTokens
        self.softCap = softCap
        self.softCapInTokens = softCapInTokens
    }
}

public class InvestorsProjectPageFile {
    public var file: InvestorsProjectPageFileFile?
    public var fileType: String?
    public var identifier: Int?

    public init(file: InvestorsProjectPageFileFile?,
                fileType: String?,
                identifier: Int?) {
        self.file = file
        self.fileType = fileType
        self.identifier = identifier
    }
}

public class InvestorsProjectPageFileFile {
    public var customFileName: String?
    public var encrypted: Bool?
    public var identifier: String?
    public var originalFileName, uniqueFileName: String?

    public init(customFileName: String?,
                encrypted: Bool?,
                identifier: String?,
                originalFileName: String?,
                uniqueFileName: String?) {
        self.customFileName = customFileName
        self.encrypted = encrypted
        self.identifier = identifier
        self.originalFileName = originalFileName
        self.uniqueFileName = uniqueFileName
    }
}

public class InvestorsTokenParametersDocument {
    public var dsoProjectFinishDate, dsoProjectStartDate: Date?
    public var hardCap, minimumInvestmentAmount, nominalValue: Int?

    public init(dsoProjectFinishDate: String?,
                dsoProjectStartDate: String?,
                hardCap: Int?,
                minimumInvestmentAmount: Int?,
                nominalValue: Int?) {
        let formatter = DateHelper.shared.backendFormatter
        self.dsoProjectFinishDate = formatter.date(from: dsoProjectFinishDate ?? "")?.withCurrentTimeZoneOffset()
        self.dsoProjectStartDate = formatter.date(from: dsoProjectStartDate ?? "")?.withCurrentTimeZoneOffset()
        self.hardCap = hardCap
        self.minimumInvestmentAmount = minimumInvestmentAmount
        self.nominalValue = nominalValue
    }
}
