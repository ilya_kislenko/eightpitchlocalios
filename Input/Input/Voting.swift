//
//  Voting.swift
//  Input
//
//  Created by 8pitch on 05.03.2021.
//

import UIKit

public class Answer {
    public let answers: [Option]
    public let question: Question
    
    public init(answers: [Option], question: Question) {
        self.answers = answers
        self.question = question
    }
}

public class Question {
    
    public enum QuestionType: String {
        case radiobutton = "RADIOBUTTON"
        case checkbox = "CHECKBOX"
    }
    
    lazy public var maxAnswer: String? = {
        self.options.map { $0.answer }.sorted { $0?.count ?? 0 > $1?.count ?? 0}.first ?? ""
    }()
    
    public let id: Int
    public let maximumAnswers: Int
    public let minimumAnswers: Int
    public let options: [Option]
    public let title: String
    public let type: QuestionType
    
    public var overLimit: Bool {
        let checked = self.options.filter { $0.selected }.count
        return checked > self.maximumAnswers && self.type != .radiobutton
    }
    
    public var answered: Bool {
        let checked = self.options.filter { $0.selected }.count
        return (checked >= self.minimumAnswers) && !self.overLimit && checked != 0
    }
    
    public init(id: Int,
                maximumAnswers: Int?,
                minimumAnswers: Int?,
                options: [Option],
                title: String?,
                type: String?) {
        self.id = id
        self.maximumAnswers = maximumAnswers ?? 0
        self.minimumAnswers = minimumAnswers ?? 0
        self.options = options
        self.title = title ?? ""
        self.type = QuestionType(rawValue: type ?? "") ?? .checkbox
    }
}

public class Option {
    public let answer: String?
    public let id: Int
    public var selected: Bool
    
    public init(answer: String?, id: Int) {
        self.answer = answer
        self.id = id
        self.selected = false
    }
}

public class SmsConfirmation {
    public let interactionId: String?
    public let token: String?
    
    public init(interactionId: String?, token: String?) {
        self.interactionId = interactionId
        self.token = token
    }
}

public class Trustee {
    public let firstName: String
    public let lastName: String
    public let prefix: Title
    
    public init(firstName: String,
                lastName: String,
                prefix: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.prefix = Title(rawTitle: prefix)
    }
}

public class VotingProjectInfo: Hashable {
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(projectId)
    }
    
    public static func == (lhs: VotingProjectInfo, rhs: VotingProjectInfo) -> Bool {
        lhs.projectId == rhs.projectId
    }
    
    public let companyName: String
    public let legalFormType: String
    public let projectId: Int
    
    public init(companyName: String,
                legalFormType: String,
                projectId: Int) {
        self.companyName = companyName
        self.legalFormType = legalFormType
        self.projectId = projectId
    }
}

public class Voting {
    
    public var projectId: Int {
        self.projectInfo.projectId
    }
    
    public var includeSurvey: Bool {
        informationCollectionPeriodEndDate != nil &&
            informationCollectionPeriodStartDate != nil
    }
    
    public var includeStream: Bool {
        streamEndDate != nil &&
            streamEndDate != nil
    }
    
    public var addTrustee: Bool {
        trustee != nil
    }
    
    public var votingPeriod: String {
        if let startDate = self.startDate,
           let endDate = self.endDate {
            let formatter = DateHelper.shared.uiFormatterFull
            let start = formatter.string(from: startDate)
            let end = formatter.string(from: endDate)
            return "\(start) - \(end)"
        } else {
            return ""
        }
    }
    
    public var conductedDate: String {
        if let date = self.userVotingDate {
            let formatter = DateHelper.shared.uiFormatterFullSeconds
            return "\(formatter.string(from: date)) CET"
        } else {
            return ""
        }
    }
    
    public var passed: Bool {
        self.questions.first(where: { !$0.answered }) == nil ? self.confirmedByUser : false
    }
    
    public enum VotingStatus: String {
        case waitingForApproval = "WAITING_FOR_APPROVAL"
        case preparations = "PREPARATIONS"
        case waitBlockChain = "WAIT_BLOCKCHAIN"
        case pending = "PENDING"
        case streaming = "STREAMING"
        case running = "RUNNING"
        case savingResultInBlockChain = "SAVING_RESULTS_IN_BLOCKCHAIN"
        case waitTrustee = "WAIT_TRUSTEE"
        case completed = "COMPLETED"
        case archived = "ARCHIVED"
        case infoCollection = "INFORMATION_COLLECTION"
        case infoCollectionRunning = "INFORMATION_COLLECTION_RUNNING"
        case stream = "STREAM"
        case streamRunning = "STREAM_RUNNING"
    }
    
    public enum UserVotingsStatus: String {
        case available = "AVAILABLE"
        case inProgress = "IN_PROGRESS"
        case finished = "FINISHED"
    }
    
    public enum Creator: String {
        case admin = "ADMIN"
        case PI = "PI"
    }
    
    public enum InvestorTrusteeDecision: String {
        case voteByHimself = "VOTE_BY_HIMSELF"
        case withObligation = "WITH_ANSWER_OBLIGATION"
        case withoutObligation = "WITHOUT_ANSWER_OBLIGATION"
    }
    
    public let agenda: String
    public let createDateTime: Date?
    public let createdBy: Creator?
    public let description: String
    public let endDate: Date?
    public let id: Int?
    public let informationCollectionPeriodEndDate: Date?
    public let informationCollectionPeriodStartDate: Date?
    public let investorTrusteeDecision: InvestorTrusteeDecision?
    public let projectInfo: VotingProjectInfo
    public let questions: [Question]
    public let sendResultsToParticipants: Bool
    public let shareResultsWithTrustee: Bool
    public let smsConfirmation: SmsConfirmation?
    public let startDate: Date?
    public let status: VotingStatus?
    public let streamAgenda: String?
    public let streamEndDate: Date?
    public let streamStartDate: Date?
    public let streamTheme: String?
    public let trustee: Trustee?
    public let trusteeEnabled: Bool
    public let trusteeExternalId: String?
    public let type: String?
    public let userVotingDate: Date?
    public let usersVotingStatus: UserVotingsStatus?
    public var confirmedByUser: Bool = false
    
    public init(agenda: String?,
                createDateTime: String?,
                createdBy: String?,
                description: String?,
                endDate: String?,
                id: Int?,
                informationCollectionPeriodEndDate: String?,
                informationCollectionPeriodStartDate: String?,
                investorTrusteeDecision: String?,
                projectInfo: VotingProjectInfo,
                questions: [Question],
                sendResultsToParticipants: Bool,
                shareResultsWithTrustee: Bool,
                smsConfirmation: SmsConfirmation?,
                startDate: String?,
                status: String?,
                streamAgenda: String?,
                streamEndDate: String?,
                streamStartDate: String?,
                streamTheme: String?,
                trustee: Trustee?,
                trusteeEnabled: Bool,
                trusteeExternalId: String?,
                type: String?,
                userVotingDate: String?,
                usersVotingStatus: String?) {
        let formatter = DateHelper.shared.backendFormatter
        self.agenda = agenda ?? ""
        self.createDateTime = formatter.date(from: createDateTime ?? "")
        self.createdBy = Creator(rawValue: createdBy ?? "")
        self.description = description ?? ""
        self.endDate = formatter.date(from: endDate ?? "")
        self.id = id
        self.informationCollectionPeriodEndDate = formatter.date(from: informationCollectionPeriodEndDate ?? "")
        self.informationCollectionPeriodStartDate = formatter.date(from: informationCollectionPeriodStartDate ?? "")
        self.investorTrusteeDecision = InvestorTrusteeDecision(rawValue: investorTrusteeDecision ?? "")
        self.projectInfo = projectInfo
        self.questions = questions
        self.sendResultsToParticipants = sendResultsToParticipants
        self.shareResultsWithTrustee = shareResultsWithTrustee
        self.smsConfirmation = smsConfirmation
        self.startDate = formatter.date(from: startDate ?? "")
        self.status = VotingStatus(rawValue: status ?? "")
        self.streamAgenda = streamAgenda
        self.streamEndDate = formatter.date(from: streamEndDate ?? "")
        self.streamStartDate = formatter.date(from: streamStartDate ?? "")
        self.streamTheme = streamTheme
        self.trustee = trustee
        self.trusteeEnabled = trusteeEnabled
        self.trusteeExternalId = trusteeExternalId
        self.type = type
        self.userVotingDate = formatter.date(from: userVotingDate ?? "")
        self.usersVotingStatus = UserVotingsStatus(rawValue: usersVotingStatus ?? "")
    }
}

public class Percent {
    public let investors: Double
    public let tokens: Double
    
    public init(investors: Double?, tokens: Double?) {
        self.investors = investors ?? 0
        self.tokens = tokens ?? 0
    }
}

public class QuestionResult {
    public let question: Int
    public let result: [Int: Percent]
    
    public init(question: Int,
                result: [Int: Percent]) {
        self.question = question
        self.result = result
    }
}

public class VotingResult {
    public let coveragePerInvestors: Double
    public let coveragePerTokens: Double
    public let questions: [QuestionResult]
    
    public init(coveragePerInvestors: Double?,
                coveragePerTokens: Double?,
                questions: [QuestionResult]) {
        self.coveragePerInvestors = coveragePerInvestors ?? 0
        self.coveragePerTokens = coveragePerTokens ?? 0
        self.questions = questions
    }
}

