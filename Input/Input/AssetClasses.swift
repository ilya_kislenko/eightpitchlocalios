//
//  AssetClasses.swift
//  Input
//
//  Created by 8pitch on 8/13/20.
//

import Foundation
import RxSwift

public enum AssetClassesError : ModelError {

    public var errorDescription : String? {
        return "AssetClassesError"
    }
}

public enum AssetClassesType: String {
    case tokenizedSecurities = "TOKENIZED_SECURITIES"
    case bonds = "BONDS"
    case stocks = "STOCKS"
    case closedEndMutualFunds = "CLOSED_END_MUTUAL_FUNDS"
    case hedgeFunds = "HEDGE_FUNDS"
    case investmentCertificates = "INVESTMENT_CERTIFICATES"
    case warrants = "WARRANTS"
    case notProvided = "NOT_PROVIDED"
}

public class AssetClasses : Model {
    public var tokenizedSecuritiesProvider : String?
    public var bondsProvider : String?
    public var stocksProvider : String?
    public var closedEndMutualFundsProvider : String?
    public var hedgeFundsProvider : String?
    public var investmentCertificatesProvider : String?
    public var warrantsProvider : String?
    public var notProvided: String?
}
