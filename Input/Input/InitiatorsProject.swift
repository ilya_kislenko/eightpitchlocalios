//
//  InitiatorsProjects.swift
//  Input
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

public class InitiatorsProject: ProjectInfo {
    private struct Constants {
        static let imageFileType = "PROJECT_THUMBNAIL"
    }
    public var thumbnailId: String? {
        if let file = self.projectFiles.filter({$0?.fileType == Constants.imageFileType}).first??.file {
            return file.identifier
        } else {
            return nil
        }
    }
    public var companyName: String?
    public var graph: InitiatorsGraph?
    public var identifier: String?
    public var projectBI: InitiatorsProjectBI?
    public var projectDescription: String?
    public var projectFiles: [InitiatorsProjectFile?]
    public var projectPage: InitiatorsProjectPage?
    public var projectStatus: ProjectStatus?
    public var tokenParametersDocument: InitiatorsTokenParametersDocument?

    public init(companyName: String?,
                graph: InitiatorsGraph?,
                identifier: String?,
                projectBI: InitiatorsProjectBI?,
                projectDescription: String?,
                projectFiles: [InitiatorsProjectFile?],
                projectPage: InitiatorsProjectPage?,
                projectStatus: String?,
                tokenParametersDocument: InitiatorsTokenParametersDocument?) {
        self.companyName = companyName
        self.graph = graph
        self.identifier = identifier
        self.projectBI = projectBI
        self.projectDescription = projectDescription
        self.projectFiles = projectFiles
        self.projectPage = projectPage
        if let statusString = projectStatus,
           let status = ProjectStatus(rawValue: statusString) {
            self.projectStatus = status
        } else {
            self.projectStatus = nil
        }
        self.tokenParametersDocument = tokenParametersDocument
    }

    public func projectIdentifier() -> Int? {
        return Int(self.identifier ?? "")
    }
}

public class InitiatorsGraph {
    public var currentFundingSumInTokens, hardCapInTokens, softCapInTokens: Int?
    public var currentFundingSum, softCap, hardCap: Decimal?

    public init(currentFundingSum: Decimal?,
                currentFundingSumInTokens: Int?,
                hardCap: Decimal?,
                hardCapInTokens: Int?,
                softCap: Decimal?,
                softCapInTokens: Int?) {
        self.currentFundingSum = currentFundingSum
        self.currentFundingSumInTokens = currentFundingSumInTokens
        self.hardCap = hardCap
        self.hardCapInTokens = hardCapInTokens
        self.softCap = softCap
        self.softCapInTokens = softCapInTokens
    }
}

public class InitiatorsProjectBI {
    public var averageInvestment, capitalInvested: Decimal?
    public var investors: InitiatorsInvestors?
    public var maxInvestment, minInvestment: Decimal?

    public init(averageInvestment: Decimal?,
                capitalInvested: Decimal?,
                investors: InitiatorsInvestors?,
                maxInvestment: Decimal?,
                minInvestment: Decimal?) {
        self.averageInvestment = averageInvestment
        self.capitalInvested = capitalInvested
        self.investors = investors
        self.maxInvestment = maxInvestment
        self.minInvestment = minInvestment
    }
}

public class InitiatorsInvestors {
    public var totalCount: Int?

    public init(totalCount: Int?) {
        self.totalCount = totalCount
    }
}

public class InitiatorsProjectFile {
    public var file: InitiatorsFile?
    public var fileType: String?
    public var identifier: Int?

    public init(file: InitiatorsFile?,
                fileType: String?,
                identifier: Int?) {
        self.file = file
        self.fileType = fileType
        self.identifier = identifier
    }
}

public class InitiatorsFile {
    public var customFileName, identifier, originalFileName, uniqueFileName: String?

    public init(customFileName: String?,
                identifier: String?,
                originalFileName: String?,
                uniqueFileName: String?) {
        self.customFileName = customFileName
        self.identifier = identifier
        self.originalFileName = originalFileName
        self.uniqueFileName = uniqueFileName
    }
}

public class InitiatorsProjectPage {
    public var promoVideo, url: String?

    public init(promoVideo: String?,
                url: String?) {
        self.promoVideo = promoVideo
        self.url = url
    }
}

public class InitiatorsTokenParametersDocument {
    public var dsoProjectFinishDate, dsoProjectStartDate: Date?
    public var nominalValue: Decimal?

    public init(dsoProjectFinishDate: String?,
                dsoProjectStartDate: String?,
                nominalValue: Decimal?) {
        let formatter = DateHelper.shared.backendFormatter
        self.dsoProjectStartDate = formatter.date(from: dsoProjectStartDate ?? "")?.withCurrentTimeZoneOffset()
        self.dsoProjectFinishDate = formatter.date(from: dsoProjectFinishDate ?? "")?.withCurrentTimeZoneOffset()
        self.nominalValue = nominalValue
    }
}
