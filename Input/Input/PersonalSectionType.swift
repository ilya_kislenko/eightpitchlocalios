//
//  PersonalSectionType.swift
//  Input
//
//  Created by 8pitch on 9/17/20.
//

public enum PersonalSectionType: Int {
    case upgrade
    case company
    case personal
    case contact
    case address
    case financial
    case other
}
