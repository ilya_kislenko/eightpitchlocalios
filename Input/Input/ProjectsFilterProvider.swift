//
//  ProjectsFilterProvider.swift
//  Input
//
//  Created by 8pitch on 19.01.21.
//

import Foundation

public class ProjectsFilterProvider {
    public var availableStatuses: [ProjectStatus] = [.comingSoon, .active, .finished, .waitBlockchain, .refunded, .manualRelease, .waitRelease, .tokensTransferred]
    public var selectedStatuses: [ProjectStatus] = []
    public var appliedStatuses: [ProjectStatus] = []
    public var availableInvestmentAmount: [String]?
    public var selectedInvesmentAmount: [String] = []
    public var appliedInvesmentAmount: [String] = []
    public var availableSecurityTypes: [String]?
    public var selectedSecurityTypes: [String] = []
    public var appliedSecurityTypes: [String] = []
    public var filteredProjects: [Project] = []
    public var filterWasApplied: Bool = false
        
    public func resetFilter() {
        selectedStatuses = []
        selectedSecurityTypes = []
        selectedInvesmentAmount = []
    }
    
    public func getSelectedSecurityTypes() -> String {
        return selectedSecurityTypes.joined(separator: ",")
    }
    
    public func getSelectedInvesmentAmount() -> String {
        return selectedInvesmentAmount.joined(separator: ",")
    }
    
    public func getSelectedStatuses() -> String {
        return selectedStatuses.isEmpty ? availableStatuses.map({ $0.rawValue }).joined(separator: ",") : selectedStatuses.contains(.finished) ? (selectedStatuses + [.manualRelease, .refunded, .waitRelease, .waitBlockchain, .tokensTransferred]).map({ $0.rawValue }).joined(separator: ",") : selectedStatuses.map({ $0.rawValue }).joined(separator: ",")
    }
    
    public func getQueryItems() -> [URLQueryItem] {
        var queryItems: [URLQueryItem] = [URLQueryItem(name: "projectStatuses", value: getSelectedStatuses()),
                                          URLQueryItem(name: "size", value: "\(Constants.size)"),
                                          URLQueryItem(name: "page", value: "\(Constants.page)")]
        if !getSelectedInvesmentAmount().isEmpty {
            queryItems.append(URLQueryItem(name: "minimumInvestmentAmounts", value: getSelectedInvesmentAmount()))
        }
        if !getSelectedSecurityTypes().isEmpty {
            queryItems.append(URLQueryItem(name: "typeOfSecurity", value: getSelectedSecurityTypes()))
        }
        return queryItems
    }
    
    public func getStatus(status: ProjectStatus) {
        if let index = selectedStatuses.firstIndex(of: status) {
            selectedStatuses.remove(at: index)
        } else {
            selectedStatuses.append(status)
        }
    }
    
    public func getInvestmentAmount(investmentAmount: String) {
        if let index = selectedInvesmentAmount.firstIndex(of: investmentAmount) {
            selectedInvesmentAmount.remove(at: index)
        } else {
            selectedInvesmentAmount.append(investmentAmount)
        }
    }
    
    public func getSecurityType(securityType: String) {
        if let index = selectedSecurityTypes.firstIndex(of: securityType) {
            selectedSecurityTypes.remove(at: index)
        } else {
            selectedSecurityTypes.append(securityType)
        }
    }
    
    public func setupAppliedFilters() {
        appliedStatuses = selectedStatuses
        appliedSecurityTypes = selectedSecurityTypes
        appliedInvesmentAmount = selectedInvesmentAmount
        filterWasApplied = !selectedStatuses.isEmpty || !selectedSecurityTypes.isEmpty || !selectedInvesmentAmount.isEmpty
    }
    
    public func setupSelectedFilters() {
        selectedStatuses = appliedStatuses
        selectedSecurityTypes = appliedSecurityTypes
        selectedInvesmentAmount = appliedInvesmentAmount
        filterWasApplied = !appliedStatuses.isEmpty || !appliedSecurityTypes.isEmpty || !appliedInvesmentAmount.isEmpty
    }
    
    public func isFiltersSelected() -> Bool {
        return !(selectedStatuses.isEmpty && selectedSecurityTypes.isEmpty && selectedInvesmentAmount.isEmpty)
    }
    
    enum Constants {
        static let size: Int = 9999
        static let page: Int = 0
    }
}
