//
//  InvestmentExperienceQuestionnaire.swift
//  Input
//
//  Created by 8pitch on 30.07.2020.
//

import Foundation
import RxSwift

public class InvestmentExperienceQuestionnaire : Model {
    
    public let experienceProvider : BehaviorSubject<String?> = .init(value: nil)
    public let experienceDurationProvider : BehaviorSubject<String?> = .init(value: nil)
    public let investingAmountProvider : BehaviorSubject<String?> = .init(value: nil)
    public let investingFrequencyProvider : BehaviorSubject<String?> = .init(value: nil)
    public let knowledgeProvider : BehaviorSubject<[String]?> = .init(value: nil)
    public let professionalExperienceSourceProvider : BehaviorSubject<String?> = .init(value: nil)
    public let virtualCurrenciesUsedProvider : BehaviorSubject<String?> = .init(value: nil)
    
     var experience : String
     var experienceDuration : String
     var investingAmount : String
     var investingFrequency : String
     var knowledge : [String]
     var professionalExperienceSource : String
     var virtualCurrenciesUsed : String
    
    public init(experience: String = "", experienceDuration: String = "", investingAmount: String = "", investingFrequency: String = "", knowledge: [String] = [], professionalExperienceSource: String = "", virtualCurrenciesUsed: String = "") {
        self.experience = experience
        self.experienceDuration = experienceDuration
        self.investingAmount = investingAmount
        self.investingFrequency = investingFrequency
        self.knowledge = knowledge
        self.professionalExperienceSource = professionalExperienceSource
        self.virtualCurrenciesUsed = virtualCurrenciesUsed
    }
    
}
