//
//  Locality.swift
//  Input
//
//  Created by 8pitch on 01.08.2020.
//

import Foundation
import RxSwift

public enum CountryError : ModelError {
    
    case countryEmpty
    
    public var errorDescription : String? {
        return "input.error.empty".localized
    }
    
}

public enum LocalityError : ModelError {
    
    case cityEmpty
    case cityInvalid
    case zipInvalid
    case zipEmpty
    
    public var errorDescription : String? {
        switch self {
        case .cityInvalid:
            return "kyc.input.error.street".localized
        case .cityEmpty, .zipEmpty:
            return "input.error.empty".localized
        case .zipInvalid:
            return "kyc.input.error.postalCode".localized
        }
    }
}

public enum ZipError : ModelError {
    
    case zipInvalid
    case zipEmpty
    
    public var errorDescription : String? {
        switch self {
        case .zipInvalid:
            return "kyc.input.error.postalCode".localized
        case .zipEmpty:
            return "input.error.empty".localized
        }
    }
}

public class Locality : Model {
    
    public let countryProvider : BehaviorSubject<String?> = .init(value: nil)
    public let cityProvider : BehaviorSubject<String?> = .init(value: nil)
    public let zipProvider : BehaviorSubject<String?> = .init(value: nil)
    
    private let cityPattern = "^[A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'-]*+([ A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'-]+)*"
    private let zipPattern = "[\u{00C0}-\u{00FF}a-zA-Z0-9]"
    
    private var country : String = ""
    private var city : String = ""
    private var zip: String = ""
    
    public init(user: User? = nil) {
        self.countryProvider.onNext(try? user?.addressProvider.value()?.countryProvider.value())
        self.cityProvider.onNext(try? user?.addressProvider.value()?.cityProvider.value())
        self.zipProvider.onNext(try? user?.addressProvider.value()?.zipProvider.value())
        super.init()
    }
    
    override func setup() {
        self.countryProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                guard !$0.isEmpty else {
                    self?.errorTextAlignment = .center
                    self?.isValid.onNext(.failure(CountryError.countryEmpty))
                    return
                }
                self?.isValid.onNext(.success(false))
                self?.country = $0
            }).disposed(by: self.disposables)
        
        self.cityProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                _ = self?.validate(string: $0)
                self?.city = $0
            }).disposed(by: self.disposables)
        
        self.zipProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                _ = self?.validateZip($0)
                self?.zip = $0
            }).disposed(by: self.disposables)
    }
    
    override func validate(string: String) -> Bool {
        guard !string.isEmpty else {
            self.errorTextAlignment = .left
            self.isValid.onNext(.failure(LocalityError.cityEmpty))
            return false
        }
        guard let regex = try? NSRegularExpression(pattern: self.cityPattern, options: []) else
        {
            return false
        }
        let searchable = NSRange(location: 0, length: string.count)
        let result = regex.rangeOfFirstMatch(in: string, options: [], range: searchable) == searchable
        if result {
            self.isValid.onNext(.success(false))
            return true
        } else {
            self.errorTextAlignment = .left
            self.isValid.onNext(.failure(LocalityError.cityInvalid))
            return false
        }
    }
    
    private func validateZip(_ zip: String) -> Bool {
        guard !zip.isEmpty else {
            self.errorTextAlignment = .right
            self.isValid.onNext(.failure(LocalityError.zipEmpty))
            return false
        }
        guard let regex = try? NSRegularExpression(pattern: self.zipPattern, options: []) else
        {
            return false
        }
        let searchable = NSRange(location: 0, length: zip.count)
        let result = regex.matches(in: zip, options:[], range: searchable).count == zip.count
        if result {
            self.isValid.onNext(.success(false))
            return true
        } else {
            self.errorTextAlignment = .right
            self.isValid.onNext(.failure(LocalityError.zipInvalid))
            return false
        }
    }
    
    public func validate() {
        let isCountryValid = !self.country.isEmpty
        if !isCountryValid {
            return
        }
        let isCityValid = self.validate(string: self.city)
        if !isCityValid {
            return
        }
        let isZipValid = self.validateZip(self.zip)
        if !isZipValid {
            return
        }
        if isCityValid && isCountryValid && isZipValid {
            self.isValid.onNext(.success(true))
        }
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.countryProvider.onNext(nil)
        self.cityProvider.onNext(nil)
        self.zipProvider.onNext(nil)
        self.country = ""
        self.city = ""
        self.zip = ""
        self.setup()
    }
    
}
