//
//  Qualification.swift
//  Input
//
//  Created by 8pitch on 6.01.21.
//

import Foundation
import RxSwift

public enum QualificationType : String, CaseIterable {
    
    case unqualified = "UNQUALIFIED"
    case firstClass = "CLASS_1"
    case secondClass = "CLASS_2"
    case thirdClass = "CLASS_3"
    case fourthClass = "CLASS_4"
    case fifthClass = "CLASS_5"
    
    public init(rawValue: String) {
        self = Self.localized[rawValue] ?? .unqualified
    }
    
    public init(rawTitle: String?) {
        switch rawTitle {
        case "UNQUALIFIED": self = .unqualified
        case "CLASS_1": self = .firstClass
        case "CLASS_2": self = .secondClass
        case "CLASS_3": self = .thirdClass
        case "CLASS_4": self = .fourthClass
        case "CLASS_5": self = .fifthClass
        default: self = .unqualified
        }
    }
    
    private static var localized : [String : Self] {
        let values : [Self] = [ .unqualified, .firstClass, .secondClass, .thirdClass, .fourthClass, .fifthClass ]
        return values.reduce(into: [String : Self]()) { $0[$1.rawValue.localized] = $1 }
    }
    
    public static func rawTitle(from type: QualificationType?) -> String {
        switch type {
        case .unqualified?: return "UNQUALIFIED"
        case .firstClass?: return "CLASS_1"
        case .secondClass?: return "CLASS_2"
        case .thirdClass?: return "CLASS_3"
        case .fourthClass?: return "CLASS_4"
        case .fifthClass?: return "CLASS_5"
        default: return "UNQUALIFIED"
        }
    }
    
}
