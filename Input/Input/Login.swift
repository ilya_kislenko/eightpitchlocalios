//
//  Login.swift
//  Input
//
//  Created by 8pitch on 28.07.2020.
//

import Foundation
import RxSwift

public class Login : Model {
    
    public let phoneProvider : BehaviorSubject<Phone?> = .init(value: Phone())
    public let passwordProvider : BehaviorSubject<Password?> = .init(value: Password(password: "", confirmation: "", singleMode: true))
    public var otp: Bool = false
    public var rememberMeSelected : Bool {
        get {
            !BiometryManager.shared.isPINEnabled && self.rememberMe
        }
        set {
            self.rememberMe = newValue
        }
    }
    
    private var rememberMe: Bool = false
    
    public init(phone: String = "", password: String = "") {
        super.init()
    }
    
    override func setup() {
        self.disposables = DisposeBag()
        try? self.phoneProvider.value()?.isValid
            .subscribe(onNext: { [weak self] in
                switch $0 {
                case .failure(let error as PhoneError) where error != .phoneInvalid:
                    self?.isValid.onNext(.failure(error))
                default:
                    self?.isValid.onNext(.success(false))
                }
            }).disposed(by: self.disposables)
        try? self.passwordProvider.value()?.isValid
            .subscribe(onNext: { [weak self] in
                switch $0 {
                case .failure(let error):
                    self?.isValid.onNext(.failure(error))
                default:
                    self?.isValid.onNext(.success(false))
                }
            }).disposed(by: self.disposables)
    }
    
    public func validate() {
        try? self.phoneProvider.value()?.validate()
        guard let isPhoneValid = try? self.phoneProvider.value()?.isValid.value(),
              let isPasswordValid = try? self.passwordProvider.value()?.isValid.value()
        else { return }
        switch isPhoneValid {
        case .failure(let error as PhoneError) where error != .phoneInvalid:
            break
        default:
            try? self.passwordProvider.value()?.validate()
        }
        if case .success(true) = isPhoneValid,
           case .success(true) = isPasswordValid {
            self.isValid.onNext(.success(true))
        }
        if case .failure(PhoneError.phoneInvalid) = isPhoneValid,
           case .success(true) = isPasswordValid {
            self.isValid.onNext(.success(true))
        }
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.phoneProvider.onNext(Phone())
        self.passwordProvider.onNext(Password(password: "", confirmation: "", singleMode: true))
        self.setup()
    }
    
}

