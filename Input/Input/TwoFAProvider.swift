//
//  TwoFAProvider.swift
//  Input
//
//  Created by 8pitch on 10/2/20.
//

import Foundation

public class TwoFAProvider: Model {
    
    public var smsAuth: Bool
    public var totpAuth: Bool
    
    public init(smsAuth: Bool = false, totpAuth: Bool = false) {
        self.smsAuth = smsAuth
        self.totpAuth = totpAuth
    }
    
    override func setup() {
        super.setup()
        self.isValid.onNext(.success(true))
    }
    
}
