//
//  ExpandedProject.swift
//  Input
//
//  Created by 8pitch on 8/27/20.
//

import Foundation
import UIKit

public protocol ProjectPageBlockPayload {}
public protocol ChartTypeData {}
public typealias LanguageMap = (current: [String?: String?], initial: [String?: String?])

public enum HeadingSizeSymbols: String {
   case h1 = "h1"
   case h2 = "h2"
   case h3 = "h3"
}

public enum ProjectPageBlockType: String, CaseIterable {
    case mainFacts = "mainFacts"
    case text = "text"
    case image = "image"
    case textAndImage = "textAndImage"
    case FAQ = "faq"
    case list = "list"
    case table = "table"
    case icons = "icons"
    case video = "video"
    case divider = "divider"
    case facts = "facts"
    case tilesImages = "tiles-images"
    case dualColumnImages = "dual-column-images"
    case imageLink = "imageLink"
    case dualColumnText = "dual-column-text"
    case button = "button_block"
    case disclaimer = "disclaimer"
    case chart = "chart"
    case linkedText = "linked_text"
    case department = "department"
    case header = "header"
    case delimiter = "line-delimiter"
    case group = "group"
}

public enum ChartType: String {
    case bar = "bar"
    case line = "line"
    case doughnut = "doughnut"
    case halfDoughnut = "half-doughnut"
}

public enum TableHeaderColor: String {
    case red = "red"
    case black = "black"
}

public class ExpandedProject {
    
    private struct Constants {
        static let imageFileType = "BACKGROUND_IMAGE"
        static let documentFileType = "CONTRACT_DOCUMENTS_DRAFT"
    }
    
    public var documents: [File?] {
        self.projectFiles?.filter { $0.fileType == Constants.documentFileType }.map { $0.file } ?? []
    }
    
    public var backgroundImageId: String? {
        if let file = (self.projectPage?.projectPageFiles?.filter {$0.fileType == Constants.imageFileType})?.first?.file {
            return file.id
        } else {
            return nil
        }
    }
    
    public class FinancialInformation {
        public var companyAddress : String?
        public var country : String?
        public var financingPurpose : String?
        public var goal : Int?
        public var guarantee : String?
        public var id : String?
        public var investmentSeries : String?
        public var isin : String?
        public var nominalValue : String?
        public var threshold : Int?
        public var typeOfSecurity : String?
        
        public init(companyAddress : String?, country : String?, financingPurpose : String?,
                    goal : Int?, guarantee : String?, id : String?,
                    investmentSeries : String?, isin : String?, nominalValue : String?,
                    threshold : Int?, typeOfSecurity : String?) {
            self.companyAddress = companyAddress
            self.country = country
            self.financingPurpose = financingPurpose
            self.goal = goal
            self.guarantee = guarantee
            self.id = id
            self.investmentSeries = investmentSeries
            self.isin = isin
            self.nominalValue = nominalValue
            self.threshold = threshold
            self.typeOfSecurity = typeOfSecurity
        }
    }
    
    public var additionalFields: AdditionalFields?
    public var companyName : String?
    public var customAdditionalFields: [CustomAdditionalField?]?
    public var domain : String?
    public var financialInformation: FinancialInformation?
    public var geoipRestrictions: [[Int:String]]?
    public var graph: Graph?
    public var id : String?
    public var legalForm : String?
    public var paymentConfiguration: PaymentConfiguration?
    public var possibilityAccept : Bool?
    public var projectDescription : String?
    public var projectExternalID : String?
    public var projectFiles: [ProjectFile]?
    public var projectInitiator: ProjectInitiator?
    public var projectInitiatorExternalID : String?
    public var projectPage: ProjectPage?
    public var projectStatus : String?
    public var tokenID : String?
    public var tokenParametersDocument: TokenParametersDocument?
    public var whitelistedUserExternalIds: [[Int: String]]?
    
    public init(additionalFields: AdditionalFields?, companyName : String?, customAdditionalFields: [CustomAdditionalField?]?,
                domain : String?, financialInformation: FinancialInformation?, geoipRestrictions: [[Int:String]]?,
                graph: Graph?, id : String?, legalForm : String?, paymentConfiguration: PaymentConfiguration?,
                possibilityAccept : Bool?, projectDescription : String?, projectExternalId : String?,
                projectFiles: [ProjectFile]?, projectInitiator: ProjectInitiator?, projectInitiatorExternalId : String?,
                projectPage: ProjectPage?, projectStatus : String?, tokenId : String?,
                tokenParametersDocument: TokenParametersDocument?,
                totalNumberOfTokens: Int?,
                whitelistedUserExternalIds: [[Int: String]]?) {
        self.additionalFields = additionalFields
        self.companyName = companyName
        self.customAdditionalFields = customAdditionalFields
        self.domain = domain
        self.financialInformation = financialInformation
        self.geoipRestrictions = geoipRestrictions
        self.graph = graph
        self.id = id
        self.legalForm = legalForm
        self.paymentConfiguration = paymentConfiguration
        self.possibilityAccept = possibilityAccept
        self.projectDescription = projectDescription
        self.projectExternalID = projectExternalId
        self.projectFiles = projectFiles
        self.projectInitiator = projectInitiator
        self.projectInitiatorExternalID = projectInitiatorExternalId
        self.projectPage = projectPage
        self.projectStatus = projectStatus
        self.tokenID = tokenId
        self.tokenParametersDocument = tokenParametersDocument
        self.whitelistedUserExternalIds = whitelistedUserExternalIds
    }
}

public class AdditionalFields {
    public var agio : String?
    public var conversionRight : String?
    public var dividend : String?
    public var futureShareNominalValue : String?
    public var highOfDividend : String?
    public var id : String?
    public var initialSalesCharge : String?
    public var interest : String?
    public var interestInterval : String?
    public var issuedShares : String?
    public var typeOfRepayment : String?
    public var wkin : String?
    
    public init(agio : String?, conversionRight : String?, dividend : String?,
                futureShareNominalValue : String?, highOfDividend : String?, id : String?,
                initialSalesCharge : String?, interest : String?, interestInterval : String?,
                issuedShares : String?, typeOfRepayment : String?, wkin : String?) {
        self.agio = agio
        self.conversionRight = conversionRight
        self.dividend = dividend
        self.futureShareNominalValue = futureShareNominalValue
        self.highOfDividend = highOfDividend
        self.id = id
        self.initialSalesCharge = initialSalesCharge
        self.interest = interest
        self.interestInterval = interestInterval
        self.issuedShares = issuedShares
        self.typeOfRepayment = typeOfRepayment
        self.wkin = wkin
    }
}

public class CustomAdditionalField {
    public var fieldName : String?
    public var fieldText : String?
    public var id : String?
    
    public init(fieldName : String?, fieldText : String?, id : String?) {
        self.fieldName = fieldName
        self.fieldText = fieldText
        self.id = fieldName
    }
}

public class PaymentConfiguration {
    public var accountHolderName : String?
    public var bic : String?
    public var fintecsystemApiKey : String?
    public var iban : String?
    public var id : Int?
    public var klarnaApiKey : String?
    public var klarnaCustomerNumber : Int?
    public var klarnaProjectID : Int?
    
    public init(accountHolderName : String?, bic : String?,
                fintecsystemApiKey : String?, iban : String?,
                id : Int?, klarnaApiKey : String?,
                klarnaCustomerNumber : Int?, klarnaProjectId : Int?) {
        self.accountHolderName = accountHolderName
        self.bic = bic
        self.fintecsystemApiKey = fintecsystemApiKey
        self.iban = iban
        self.id = id
        self.klarnaApiKey = klarnaApiKey
        self.klarnaCustomerNumber = klarnaCustomerNumber
        self.klarnaProjectID = klarnaProjectId
    }
}

public class ProjectFile {
    public var file: File?
    public var fileType : String?
    public var id : Int?
    
    public init(file: File?, fileType : String?, id : Int?) {
        self.file = file
        self.fileType = fileType
        self.id = id
    }
}

public class File {
    public var customFileName : String?
    public var id : String?
    public var originalFileName : String?
    public var uniqueFileName : String?
    
    public init(customFileName : String?, id : String?,
                originalFileName : String?, uniqueFileName : String?) {
        self.customFileName = customFileName
        self.id = id
        self.originalFileName = originalFileName
        self.uniqueFileName = uniqueFileName
    }
}

public class ProjectPage {
    public var companyName : String?
    public var externalId : String?
    public var firstInvestmentArguments : String?
    public var firstQuoteTitle : String?
    public var id : String?
    public var projectPageFiles: [ProjectFile]?
    public var projectPageRejections: [ProjectPageRejection]?
    public var projectPageStatus : String?
    public var promoVideo : String?
    public var quoteText : String?
    public var secondInvestmentArguments : String?
    public var secondQuoteTitle : String?
    public var slogan : String?
    public var thirdInvestmentArguments : String?
    public var url : String?
    public var blockConstructor: BlockConstructor?
    
    public init(companyName : String?, externalId : String?, firstInvestmentArguments : String?,
                firstQuoteTitle : String?, id : String?, projectPageFiles: [ProjectFile]?,
                projectPageRejections: [ProjectPageRejection]?, projectPageStatus : String?, promoVideo : String?,
                quoteText : String?, secondInvestmentArguments : String?, secondQuoteTitle : String?,
                slogan : String?, thirdInvestmentArguments : String?, url : String?,
                blockConstructor: BlockConstructor?) {
        self.companyName = companyName
        self.externalId = externalId
        self.firstInvestmentArguments = firstInvestmentArguments
        self.firstQuoteTitle = firstQuoteTitle
        self.id = id
        self.projectPageFiles = projectPageFiles
        self.projectPageRejections = projectPageRejections
        self.projectPageStatus = projectPageStatus
        self.promoVideo = promoVideo
        self.quoteText = quoteText
        self.secondInvestmentArguments = secondInvestmentArguments
        self.secondQuoteTitle = secondQuoteTitle
        self.slogan = slogan
        self.thirdInvestmentArguments = thirdInvestmentArguments
        self.url = url
        self.blockConstructor = blockConstructor
    }
}

public class ProjectPageRejection {
    public var comment : String?
    public var createDate : Date?
    public var id : Int?
    
    public init(comment : String?, createDate : String?, id : Int?) {
        self.comment = comment
        let formatter = DateHelper.shared.backendFormatter
        self.createDate = formatter.date(from: createDate ?? "")?.withCurrentTimeZoneOffset()
        self.id = id
    }
}

public class BlockConstructor {
    public var tabs: [ProjectPageTab?]?
    public var fileIdMap: [String?: String?]
    public var languageMap: LanguageMap?
    
    public init(tabs: [ProjectPageTab?]?, fileIdMap: [String?: String?], languageMap: LanguageMap?) {
        self.tabs = tabs ?? []
        let mainFactsTab = ProjectPageTab(id: nil, name: "projectDetails.blockConstructor.mainFacts.title".localized, blocks: [ProjectPageBlock(id: nil, type: ProjectPageBlockType.mainFacts.rawValue, payload: nil, children: [], meta: nil, fileIdMap: [:], languageMap: nil)], payload: LayoutRootIdList(layoutRootIdList: [], nameTranslationCode: ""), fileIdMap: [:], languageMap: nil)
        self.tabs?.insert(mainFactsTab, at: 0)
        self.fileIdMap = fileIdMap
        self.languageMap = languageMap
    }
}

public class ProjectPageTab {
    public var ID: String?
    public var name: String?
    public var blocks: [ProjectPageBlock?]
    public var payload: LayoutRootIdList?
    
    public init(id: String?,
                name: String?,
                blocks: [ProjectPageBlock?],
                payload: LayoutRootIdList?,
                fileIdMap: [String?: String?],
                languageMap: LanguageMap?) {
        self.ID = id
        self.name = name
        self.blocks = blocks
        if let id = payload?.nameTranslationCode, let localizedName = languageMap?.current[id] ?? languageMap?.initial[id], !(localizedName ?? "").isEmpty {
            self.name = localizedName
        }
        self.payload = payload
        guard let ids = payload?.layoutRootIdList else { return }
        self.blocks.sort {
            var firstID: Int = 0
            var secondID: Int = 0
            for (key, id) in ids.enumerated() {
                if $0?.id == id {
                    firstID = key
                } else if $1?.id == id {
                    secondID = key
                }
            }
            return firstID < secondID
        }

    }
}

public class LayoutRootIdList {
    public var layoutRootIdList: [String?]
    public var nameTranslationCode: String?
    
    public init(layoutRootIdList: [String?], nameTranslationCode: String?) {
        self.layoutRootIdList = layoutRootIdList
        self.nameTranslationCode = nameTranslationCode
    }
}

public class  ProjectPageBlock {
    public var id: String?
    public var type: ProjectPageBlockType?
    public var payload: ProjectPageBlockPayload?
    public var children: [String?]
    public var meta: ProjectPageBlockMeta?
    
    public init(id: String?,
                type: String?,
                payload: ProjectPageBlockPayload?,
                children: [String?],
                meta: ProjectPageBlockMeta?,
                fileIdMap: [String?: String?],
                languageMap: LanguageMap?) {
        self.id = id
        self.children = children
        self.meta = meta
        guard let type = type else {
            return
        }
        self.type = ProjectPageBlockType.init(rawValue: type)

        guard let map = languageMap else {
            return
        }
        switch self.type {
        case .mainFacts:
            break
        case .text:
            let textBlock = payload as? TextBlockPayload
            textBlock?.text?.localizedWith(map)
            textBlock?.header?.localizedHeaderWith(map)
            textBlock?.preHeader?.localizedHeaderWith(map)
            self.payload = textBlock
        case .image:
            let imageBlock = payload as? ImageBlockPayload
            imageBlock?.imageId?.setIDWith(fileIdMap)
            imageBlock?.text?.localizedWith(map)
            imageBlock?.header?.localizedHeaderWith(map)
            self.payload = imageBlock
        case .textAndImage:
            let block = payload as? TextAndImageBlockPayload
            self.payload = payload as? TextAndImageBlockPayload
            block?.imageId?.setIDWith(fileIdMap)
            block?.header?.localizedHeaderWith(map)
            block?.text?.localizedWith(map)
            block?.imageDescription?.localizedHeaderWith(map)
            self.payload = block
        case .FAQ:
            let block = payload as? FAQBlockPayload
            block?.header?.localizedWith(map)
            block?.items.forEach {
                $0?.answer?.localizedWith(map)
                $0?.question?.localizedWith(map)
                $0?.text?.localizedWith(map)
            }
            self.payload = block
        case .list:
            let block = payload as? ListBlockPayload
            block?.header?.localizedHeaderWith(map)
            block?.items.forEach { $0?.text?.localizedWith(map) }
            self.payload = block
        case .table:
            let block = payload as? TableBlockPayload
            block?.rows = (block?.rows ?? []).map { row -> [String?] in
                var tempRow: [String?] = row
                let a: [String?]? = tempRow.map {
                    var temp: String? = $0
                    temp?.localizedWith(map)
                    return temp
                }
                tempRow = a ?? []
                return tempRow
            }
            self.payload = block
        case .icons:
            let block = payload as? IconsBlockPayload
            block?.items.forEach { $0?.imageId?.setIDWith(fileIdMap) }
            self.payload = block
        case .video:
            let block = payload as? VideoBlockPayload
            block?.vimeoId?.setIDWith(fileIdMap)
            block?.text?.localizedWith(map)
            block?.header?.localizedHeaderWith(map)
            self.payload = block
        case .divider:
            self.payload = payload as? DividerBlockPayload
        case .facts:
            let block = payload as? FactsBlockPayload
            block?.items?.forEach {
                $0?.text?.localizedWith(map)
                $0?.imageId?.setIDWith(fileIdMap)
                $0?.header?.localizedHeaderWith(map)
            }
            self.payload = block
        case .tilesImages:
            let block = payload as? TilesImagesBlockPayload
            block?.items.forEach {
                $0?.header?.localizedHeaderWith(map)
                $0?.text?.localizedWith(map)
                $0?.imageId?.setIDWith(fileIdMap)
            }
            self.payload = block
        case .dualColumnImages:
            let block = payload as? DualColumnImagesBlockPayload
            block?.leftImageId?.setIDWith(fileIdMap)
            block?.rightImageId?.setIDWith(fileIdMap)
            block?.rightText?.localizedWith(map)
            block?.leftText?.localizedWith(map)
            block?.rightHeader?.localizedHeaderWith(map)
            block?.leftHeader?.localizedHeaderWith(map)
            self.payload = block
        case .imageLink:
            let block = payload as? ImageLinkBlockPayload
            block?.items.forEach {
                $0?.description?.localizedWith(map)
                $0?.imageUrl?.setIDWith(fileIdMap)
            }
            self.payload = block
        case .dualColumnText:
            let block = payload as? DualColumnTextBlockPayload
            block?.rightText?.localizedWith(map)
            block?.leftText?.localizedWith(map)
            block?.leftHeader?.localizedHeaderWith(map)
            block?.rightHeader?.localizedHeaderWith(map)
            self.payload = block
        case .button:
            let block = payload as? ButtonBlockPayload
            block?.text?.localizedWith(map)
            self.payload = block
        case .disclaimer:
            let block = payload as? DisclaimerBlockPayload
            block?.iconUrl?.setIDWith(fileIdMap)
            block?.text?.localizedWith(map)
            block?.background?.localizedWith(map)
            self.payload = block
        case .chart:
            self.payload = payload as? ChartBlockPayload
            let block = payload as? ChartBlockPayload
                        block?.items.forEach {
                            $0?.text?.localizedWith(map)
                            $0?.title?.localizedWith(map)
                        }
                        self.payload = payload as? ChartBlockPayload
        case .linkedText:
            self.payload = payload as? LinkedTextBlockPayload
        case .department:
            let block = payload as? DepartmentBlockPayload
            block?.items.forEach {
                $0?.imageUrl?.setIDWith(fileIdMap)
                $0?.fullName?.localizedWith(map)
                $0?.position?.localizedWith(map)
            }
            self.payload = block
        case .header:
            let block = payload as? HeaderBlockPayload
            block?.text?.localizedHeaderWith(map)
            block?.preHeader?.localizedHeaderWith(map)
            self.payload = block
        case .delimiter:
            self.payload = payload as? LineDelimiterBlockPayload
        case .group:
            self.payload = nil
        case .none:
            return
        }
    }
}

public class LineDelimiterBlockPayload: ProjectPageBlockPayload {
    public var height: CGFloat {
        CGFloat(above + below + Constants.lineHeight)
    }
    
    public var above: Int
    public var below: Int
    
    public init(above: Int?, below: Int?) {
        self.above = above ?? 0
        self.below = below ?? 0
    }
    
    enum Constants {
        static let lineHeight: Int = 1
    }
}

public class ChartBlockPayload: ProjectPageBlockPayload {
    public var perLine: Int?
    public var items: [ChartBlockPayloadItem?]
    
    public init(perLine: Int?, items: [ChartBlockPayloadItem?]) {
        self.perLine = perLine
        self.items = items
    }
}

public class ChartBlockPayloadItem {
    public var title: String?
    public var chartType: ChartType?
    public var data: [[String?]]
    public var text: String?

    public init(title: String?, chartType: String?, data: [[String?]], text: String?) {
        self.title = title
        if let chartType = chartType {
            self.chartType = ChartType(rawValue: chartType)
        }
        self.data = data
        self.text = text
    }
}

public class ProjectPageBlockMeta {
    public var header: String?
    public var previewImage: String?
    
    public init(header: String?,
                previewImage: String?) {
        self.header = header
        self.previewImage = previewImage
    }
}

public class FactsBlockPayload: ProjectPageBlockPayload {
    public var perLine: Int?
    public var items: [FactsBlockPayloadItem?]?
    
    public init(perLine: Int?,
                items: [FactsBlockPayloadItem?]) {
        self.perLine = perLine
        self.items = items
    }
}

public class FactsBlockPayloadItem {
    public var imageId: String?
    public var imageFileName: String?
    public var text: String?
    public var header: String?
    
    public init(imageId: String?,
                imageFileName: String?,
                text: String?,
                header: String?) {
        self.imageId = imageId
        self.imageFileName = imageFileName
        self.text = text
        self.header = header
    }
}

public class TilesImagesBlockPayload: ProjectPageBlockPayload {
    public var perLine: Int?
    public var bold: Bool
    public var italic: Bool
    public var underline: Bool
    public var items: [TilesImagesBlockPayloadItem?]
    
    public init(perLine: Int?,
                bold: Bool?,
                italic: Bool?,
                underline: Bool?,
                items: [TilesImagesBlockPayloadItem?]) {
        self.perLine = perLine
        self.bold = bold ?? false
        self.italic = italic ?? false
        self.underline = underline ?? false
        self.items = items
    }
}

public class TilesImagesBlockPayloadItem {
    public var imageId: String?
    public var imageFileName: String?
    public var header: String?
    public var text: String?
    
    public init(imageId: String?,
                imageFileName: String?,
                header: String?,
                text: String?) {
        self.imageId = imageId
        self.imageFileName = imageFileName
        self.header = header
        self.text = text
    }
}

public class DualColumnImagesBlockPayload: ProjectPageBlockPayload {
    public var leftImageId: String?
    public var leftImageFileName: String?
    public var leftText: String?
    public var rightImageId: String?
    public var rightImageFileName: String?
    public var rightText: String?
    public var leftHeader: String?
    public var rightHeader: String?
    
    public init(leftImageId: String?,
                leftImageFileName: String?,
                leftText: String?,
                rightImageId: String?,
                rightImageFileName: String?,
                rightText: String?,
                leftHeader: String?,
                rightHeader: String?) {
        self.leftImageId = leftImageId
        self.leftImageFileName = leftImageFileName
        self.leftText = leftText
        self.rightImageId = rightImageId
        self.rightImageFileName = rightImageFileName
        self.rightText = rightText
        self.leftHeader = leftHeader
        self.rightHeader = rightHeader
    }
}

public class ImageBlockPayload: ProjectPageBlockPayload {
    public var useText: Bool?
    public var fullWidth: Bool
    public var imageId: String?
    public var imageFileName: String?
    public var text: String?
    public var color: String?
    public var fontSize: Int?
    public var justify: NSTextAlignment
    public var bold: Bool
    public var italic: Bool
    public var underline: Bool
    public var fontFamily: String?
    public var header: String?
    public var preHeader: String?
    public var headingSize: CGFloat
    
    public init(useText: Bool?,
                imageId: String?,
                fullWidth: Bool?,
                imageFileName: String?,
                text: String?,
                color: String?,
                fontSize: Int?,
                justify: String?,
                bold: Bool?,
                italic: Bool?,
                underline: Bool?,
                fontFamily: String?,
                header: String?,
                preHeader: String?,
                headingSize: String?) {
        self.useText = useText
        self.fullWidth = fullWidth ?? false
        self.imageId = imageId
        self.imageFileName = imageFileName
        self.text = text
        self.color = color
        self.fontSize = fontSize
        self.justify = TextBlockPayload.textAlignmentFrom(justify)
        self.bold = bold ?? false
        self.italic = italic ?? false
        self.underline = underline ?? false
        self.fontFamily = fontFamily
        self.header = header
        self.preHeader = preHeader
        let hederFontSizeType = HeadingSizeSymbols(rawValue: headingSize ?? "h1")
        switch hederFontSizeType {
        case .h1, .none:
            self.headingSize = 28
        case .h2:
            self.headingSize = 22
        case .h3:
            self.headingSize = 20
        }
    }
}

public class TextAndImageBlockPayload: ProjectPageBlockPayload {
    
    public enum TextPosition: String {
        case left = "left"
        case right = "right"
    }
    
    public var imageId: String?
    public var imageFileName: String?
    public var textPosition: TextPosition
    public var header: String?
    public var imageDescription: String?
    public var text: String?
    
    
    
    public init(imageId: String?,
                imageFileName: String?,
                textPosition: String?,
                header: String?,
                imageDescription: String?,
                text: String?) {
        self.imageId = imageId
        self.imageFileName = imageFileName
        self.textPosition = TextPosition(rawValue: textPosition ?? "left") ?? .left
        self.header = header
        self.imageDescription = imageDescription
        self.text = text
    }
}

public class TextBlockPayload: ProjectPageBlockPayload {
    public var text: String?
    public var headingSize: CGFloat
    public var color: String?
    public var fontSize: Int?
    public var justify: NSTextAlignment
    public var bold: Bool
    public var italic: Bool
    public var underline: Bool
    public var fontFamily: String?
    public var header: String?
    public var preHeader: String?
    
    public init(text: String?,
                headingSize: String?,
                color: String?,
                fontSize: Int?,
                justify: String?,
                bold: Bool?,
                italic: Bool?,
                underline: Bool?,
                fontFamily: String?,
                header: String?,
                preHeader: String?) {
        self.text = text
        switch HeadingSizeSymbols(rawValue: headingSize ?? "") {
        case .h1, .none:
            self.headingSize = 28
        case .h2:
            self.headingSize = 22
        case .h3:
            self.headingSize = 20
        }
        self.color = color
        self.fontSize = fontSize
        self.justify = TextBlockPayload.textAlignmentFrom(justify)
        self.bold = bold ?? false
        self.italic = italic ?? false
        self.underline = underline ?? false
        self.fontFamily = fontFamily
        self.header = header
        self.preHeader = preHeader
    }
    
    static func textAlignmentFrom(_ value: String?) -> NSTextAlignment {
        enum ProjectPageTextAlignmentType: String, CaseIterable  {
            case left = "left"
            case center = "center"
            case right = "right"
        }
        var alignment = NSTextAlignment.natural
        
        guard let value = value,
              let alignmentType = ProjectPageTextAlignmentType(rawValue: value) else { return alignment }
        
        switch alignmentType {
        case .center:
            alignment = .center
        case .left:
            alignment = .left
        case .right:
            alignment = .right
        }
        return alignment
    }
}

public class FAQBlockPayload: ProjectPageBlockPayload {
    public var header: String?
    public var items: [FAQItem?]
    
    public init(header: String?,
                items: [FAQItem?]) {
        self.header = header
        self.items = items
    }
}

public class FAQItem {
    public var question: String?
    public var answer: String?
    public var text: String?
    
    public init(question: String?,
                answer: String?,
                text: String?) {
        self.question = question
        self.answer = answer
        self.text = text
    }
    
}

public class ListBlockPayload: ProjectPageBlockPayload {
    public var header: String?
    public var items: [ListBlockPayloadItem?]
    
    public init(header: String?,
                items: [ListBlockPayloadItem?]) {
        self.header = header
        self.items = items
    }
    
}

public class ListBlockPayloadItem {
    public var text: String?
    
    public init(text: String?) {
        self.text = text
    }
}

public class TableBlockPayload: ProjectPageBlockPayload {
    public var rowsNumber: Int?
    public var colsNumber: Int?
    public var headerColor: TableHeaderColor?
    public var rows: [[String?]]
    
    
    public init(rowsNumber: Int?,
                colsNumber: Int?,
                headerColor: String?,
                rows: [[String?]]) {
        self.rowsNumber = rowsNumber
        self.colsNumber = colsNumber
        self.headerColor = TableHeaderColor(rawValue: headerColor ?? "red")
        self.rows = rows
    }
}

public class TableBlockPayloadCellItem {
    public var text: [String?]
    
    public init(text: [String?]) {
        self.text = text
    }
}

public class IconsBlockPayload: ProjectPageBlockPayload {
    public var perLine: Int?
    public var items: [IconsBlockPayloadIconItem?]
    
    public init(perLine: Int?,
                items: [IconsBlockPayloadIconItem?]) {
        self.perLine = perLine
        self.items = items
    }
}

public class IconsBlockPayloadIconItem {
    public var imageId: String?
    public var imageFileName: String?
    public var link: String?
    
    public init(imageId: String?,
                imageFileName: String?,
                link: String?) {
        self.imageId = imageId
        self.imageFileName = imageFileName
        self.link = link
    }
}

public class VideoBlockPayload: ProjectPageBlockPayload {
    public var vimeoId: String?
    public var justify: NSTextAlignment
    public var useText: Bool?
    public var header: String?
    public var text: String?
    
    public init(vimeoId: String?,
                justify: String?,
                useText: Bool?,
                header: String?,
                text: String?) {
        self.vimeoId = vimeoId
        self.justify = TextBlockPayload.textAlignmentFrom(justify)
        self.useText = useText
        self.header = header
        self.text = text
    }
}

public class DividerBlockPayload: ProjectPageBlockPayload {
    public var height: Int?
    
    public init(height: Int?) {
        self.height = height
    }
}

public class ImageLinkBlockPayload: ProjectPageBlockPayload {
    public var perLine: Int?
    public var items: [ImageLinkBlockPayloadItem?]
    public var isLinkAdded: Bool?
    public var isDescriptionAdded: Bool?
    
    public init(perLine: Int?, items: [ImageLinkBlockPayloadItem?], isLinkAdded: Bool?, isDescriptionAdded: Bool?) {
        self.perLine = perLine
        self.items = items
        self.isLinkAdded = isLinkAdded
        self.isDescriptionAdded = isDescriptionAdded
    }
}

public class ImageLinkBlockPayloadItem {
    
    public var imageUrl: String?
    public var imageFileName: String?
    public var link: String?
    public var description: String?
    
    public init(imageUrl: String?, imageFileName: String?, link: String?, description: String?) {
        self.imageUrl = imageUrl
        self.imageFileName = imageFileName
        self.link = link
        self.description = description
    }
}

public class DualColumnTextBlockPayload: ProjectPageBlockPayload {
    
    public var leftText: String?
    public var leftHeader: String?
    public var rightText: String?
    public var rightHeader: String?
    
    public init(leftText: String?, rightText: String?,
                leftHeader: String?, rightHeader: String?) {
        self.leftText = leftText
        self.rightText = rightText
        self.leftHeader = leftHeader
        self.rightHeader = rightHeader
    }
}

public class ButtonBlockPayload: ProjectPageBlockPayload {
    public var btnColor: String?
    public var textColor: String?
    public var text: String?
    public var link: String?
    
    public init(btnColor: String?, textColor: String?, text: String?, link: String?) {
        self.btnColor = btnColor
        self.textColor = textColor
        self.text = text
        self.link = link
    }
}

public class DisclaimerBlockPayload: ProjectPageBlockPayload {
    public var iconUrl: String?
    public var iconFileName: String?
    public var text: String?
    public var background: String?
    
    public init(iconUrl: String?, iconFileName: String?, text: String?, background: String?) {
        self.iconUrl = iconUrl
        self.iconFileName = iconFileName
        self.text = text
        self.background = background
    }
}

public class ChartBarBlockPayload: ProjectPageBlockPayload {
    public init() {}
    
}

public class LineChartBlockPayload: ProjectPageBlockPayload {
    
}

public class DoughnutChartBlockPayload: ProjectPageBlockPayload {
    
}

public class LinkedTextBlockPayload: ProjectPageBlockPayload {
    public var text: String?
    public var color: String?
    public var fontSize: Int?
    public var justify: String?
    public var bold: Bool
    public var italic: Bool
    public var underline: Bool
    public var fontFamily: String?
    public var wordsLink: [LinkedTextBlockWordLink?]
    
    public init(text: String?, color: String?, fontSize: Int?,
                justify: String?, bold: Bool, italic: Bool,
                underline: Bool, fontFamily: String?, wordsLink: [LinkedTextBlockWordLink?]) {
        self.text = text
        self.color = color
        self.fontSize = fontSize
        self.justify = justify
        self.bold = bold
        self.italic = italic
        self.underline = underline
        self.fontFamily = fontFamily
        self.wordsLink = wordsLink
    }
}

public class LinkedTextBlockWordLink: ProjectPageBlockPayload {
    public var linkUrl: String?
    public var position: Int?
    public var text: String?
    public var label: String?
    
    public init(linkUrl: String?, position: Int?, text: String?, label: String?) {
        self.linkUrl = linkUrl
        self.position = position
        self.text = text
        self.label = label
    }
}

public class DepartmentBlockPayload: ProjectPageBlockPayload {
    public var header: String?
    public var items: [DepartmentBlockPayloadItem?]
    
    public init(header: String?, items: [DepartmentBlockPayloadItem?]) {
        self.header = header
        self.items = items
    }
}

public class DepartmentBlockPayloadItem {
    public var fullName: String?
    public var position: String?
    public var linkedinUrl: String?
    public var imageUrl: String?
    public var imageFileName: String?
    
    public init(fullName: String?, position: String?, linkedinUrl: String?, imageUrl: String?, imageFileName: String?) {
        self.fullName = fullName
        self.position = position
        self.linkedinUrl = linkedinUrl
        self.imageUrl = imageUrl
        self.imageFileName = imageFileName
    }
}

public class HeaderBlockPayload: ProjectPageBlockPayload {
    public var text: String?
    public var headingSize: CGFloat
    public var preHeader: String?
    
    public init(text: String?, headingSize: String?, preHeader: String?) {
        self.text = text
        let hederFontSizeType = HeadingSizeSymbols(rawValue: headingSize ?? "h1")
        switch hederFontSizeType {
        case .h1, .none:
            self.headingSize = 28
        case .h2:
            self.headingSize = 22
        case .h3:
            self.headingSize = 20
        }
        self.preHeader = preHeader
    }
}






