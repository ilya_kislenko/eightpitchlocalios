//
//  AttachedFile.swift
//  Input
//
//  Created by 8pitch on 12.01.21.
//

public class AttachedFile {
    
    public var size: Int?
    public var fileData: Data?
    public var name: String?
    public var fileExtension: String?
    
    
    public init(size: Int?, fileData: Data?, name: String?, fileExtension: String?) {
        self.size = size
        self.fileData = fileData
        self.name = name
        self.fileExtension = fileExtension
    }
}
