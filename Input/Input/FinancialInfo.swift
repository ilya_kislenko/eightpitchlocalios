//
//  FinancialInfo.swift
//  Input
//
//  Created by 8pitch on 01.08.2020.
//

import Foundation
import RxSwift

public enum AccountOwnerError : ModelError {
    
    case accountOwnerInvalid
    case accountOwnerEmpty
    
    public var errorDescription : String? {
        switch self {
            case .accountOwnerInvalid:
                return "kyc.input.error.owner".localized
            default:
                return "input.error.empty".localized
        }
    }
}

public enum IBANError : ModelError {
    
    case ibanInvalid
    case ibanEmpty
    
    public var errorDescription : String? {
        switch self {
            case .ibanInvalid:
                return "kyc.input.error.iban".localized
            default:
                return "input.error.empty".localized
        }
    }
}

public class FinancialInfo : Model {
    
    public var fullName: String?
    
    public let accountOwnerProvider : BehaviorSubject<String?> = .init(value: nil)
    public let ibanProvider : BehaviorSubject<String?> = .init(value: nil)
    
    private var accountOwner : String = ""
    private var iban : String = ""
    
    public override func cleanUp() {
        super.cleanUp()
        self.accountOwnerProvider.onNext(self.fullName)
        self.ibanProvider.onNext(nil)
        self.accountOwner = ""
        self.iban = ""
        self.setup()
    }
    
    private let namePattern = "^[A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'-]*+([ A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'-]+)*"
    
    public init(user: User? = nil) {
        super.init()
        self.fullName = user?.fullName
        self.accountOwnerProvider.onNext((try? user?.accountOwnerProvider.value()) ?? user?.fullName)
        self.ibanProvider.onNext(try? user?.ibanProvider.value())        
    }
    
    override func validate(string: String) -> Bool {
        guard !string.isEmpty else {
            self.isValid.onNext(.failure(AccountOwnerError.accountOwnerEmpty))
            return false
        }
        guard let regex = try? NSRegularExpression(pattern: self.namePattern, options: []) else
        {
            return false
        }
        let searchable = NSRange(location: 0, length: string.count)
        let found = regex.rangeOfFirstMatch(in: string, options: [], range: searchable)
        if found == searchable {
            self.isValid.onNext(.success(false))
            return true
        } else {
            self.isValid.onNext(.failure(AccountOwnerError.accountOwnerInvalid))
            return false
        }
    }
    
    private func validateIBAN(_ number: String) -> Bool {
        if number.isEmpty {
            self.isValid.onNext(.failure(IBANError.ibanEmpty))
            return false
        }
        let iban = number.replacingOccurrences(of: " ", with: "").uppercased()
        if !iban.passesMod97Check() {
            self.isValid.onNext(.failure(IBANError.ibanInvalid))
            return false
        }
        self.isValid.onNext(.success(false))
        return true
    }
    
    override func setup() {
        self.accountOwnerProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                _ = self?.validate(string: $0)
                self?.accountOwner = $0
            }).disposed(by: self.disposables)
        
        self.ibanProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                guard !$0.isEmpty else {
                    self?.isValid.onNext(.failure(IBANError.ibanEmpty))
                    return
                }
                _ = self?.validateIBAN($0)
                self?.iban = $0
            }).disposed(by: self.disposables)
    }
    
    public func validate() {
        let isAccountValid = self.validate(string: self.accountOwner)
        if !isAccountValid {
            return
        }
        let isIBANValid = self.validateIBAN(self.iban)
        if !isIBANValid {
            return
        }
        if isAccountValid && isIBANValid {
            self.isValid.onNext(.success(true))
        }
    }
    
}
