//
//  Email.swift
//  Input
//
//  Created by 8pitch on 26.07.2020.
//

import Foundation
import RxSwift

public enum EmailError : LocalizedError {
    
    case emailDuplicate
    case emailInvalid
    case emailEmpty
    
    public var errorDescription : String? {
        switch self {
        case .emailEmpty:
            return "input.error.empty".localized
        case .emailDuplicate:
            return "remote.error.response.email.duplicate".localized
        case .emailInvalid:
            return "input.error.email.format".localized
        }
    }
}

public class Email : Model {
    
    public let emailProvider : BehaviorSubject<String?> = .init(value: nil)
    
    public var email : String
    
    private let atSymbol : Character = "@"
    private let dotSymbol : Character = "."
    
    public init(email: String = "") {
        self.email = email
        super.init()
    }
    
    override func setup() {
        self.emailProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.email = $0
                self?.validate()
            }).disposed(by: self.disposables)
    }
    
    override func validate(string: String) -> Bool {
        let pattern = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
            "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
            "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        guard let regex = try? NSRegularExpression(pattern: pattern, options: []) else
        {
            return false
        }
        let searchable = NSRange(location: 0, length: string.count)
        let found = regex.rangeOfFirstMatch(in: string, options: [], range: searchable)
        return found == searchable
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.emailProvider.onNext(nil)
        self.email = ""
        self.setup()
    }
    
    public func validate() {
        let isEmailValid = self.validate(string: self.email)
        if isEmailValid {
            self.isValid.onNext(.success(true))
        }
        if !isEmailValid {
            if self.email.isEmpty {
                self.isValid.onNext(.failure(EmailError.emailEmpty))
            } else {
                self.isValid.onNext(.failure(EmailError.emailInvalid))
            }
        }
    }
    
}
