//
//  User.swift
//  Input
//
//  Created by 8pitch on 30.07.2020.
//

import Foundation
import RxSwift

public class User : Model {
    
    enum AccountGroup: String {
        case privateInvestor = "PRIVATE_INVESTOR"
        case institutionalInvestor = "INSTITUTIONAL_INVESTOR"
        case initiator = "INITIATOR"
    }
    
    public let accountOwnerProvider : BehaviorSubject<String?> = .init(value: nil)
    public let actionIdProvider : BehaviorSubject<String?> = .init(value: nil)
    public let addressProvider  : BehaviorSubject<Address?> = .init(value: Address())
    public let companyNameProvider : BehaviorSubject<String?> = .init(value: nil)
    public let companyTypeProvider : BehaviorSubject<String?> = .init(value: nil)
    public let dateOfBirthProvider : BehaviorSubject<String?> = .init(value: nil)
    public let deleteAccountProcessProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public let emailProvider : BehaviorSubject<String?> = .init(value: nil)
    public let emailConfirmedProvider : BehaviorSubject<Bool> = .init(value: false)
    public let externalIdProvider : BehaviorSubject<String?> = .init(value: nil)
    public let firstNameProvider : BehaviorSubject<String?> = .init(value: nil)
    public let groupProvider : BehaviorSubject<String?> = .init(value: nil)
    public let ibanProvider : BehaviorSubject<String?> = .init(value: nil)
    public let investmentExperienceQuestionnaireProvider : BehaviorSubject<InvestmentExperienceQuestionnaire?> = .init(value: InvestmentExperienceQuestionnaire())
    public let languageProvider : BehaviorSubject<String?> = .init(value: nil)
    public let lastNameProvider : BehaviorSubject<String?> = .init(value: nil)
    public let nationalityProvider : BehaviorSubject<String?> = .init(value: nil)
    public let newNotificationsProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public let phoneProvider : BehaviorSubject<String?> = .init(value: nil)
    public let phoneNumberConfirmedProvider : BehaviorSubject<Bool> = .init(value: false)
    public let placeOfBirthProvider : BehaviorSubject<String?> = .init(value: nil)
    public let politicallyExposedPersonProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public let prefixProvider : BehaviorSubject<String?> = .init(value: nil)
    public let sexProvider : BehaviorSubject<String?> = .init(value: nil)
    public let statusProvider : BehaviorSubject<String?> = .init(value: nil)
    public let subscribeToEmailsProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public let twoFactorAuthenticationEnabledProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public let twoFactorAuthenticationTypeProvider : BehaviorSubject<String?> = .init(value: nil)
    public let usTaxLiabilityProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public let webidUpgradeProcessBlockProvider : BehaviorSubject<Bool?> = .init(value: nil)
    public var taxInformationProvider : BehaviorSubject<TaxInformation?> = .init(value: nil)
    public let commercialRegisterNumberProvider : BehaviorSubject<String?> = .init(value: nil)
    public let investorClassProvider : BehaviorSubject<String?> = .init(value: nil)
    public var paymentsHistory: [UserPayment] = []
    public var transfersHistory: [UserTransfer] = []
    public var selectedPayment: UserPayment?
    
    var accountOwner : String?
    var actionId : String?
    var address  : Address?
    var companyName : String?
    var companyType : String?
    var dateOfBirth : String?
    var deleteAccountProcess : Bool
    var email : String?
    var emailConfirmed : Bool
    var externalId : String?
    var firstName : String?
    var group : String?
    var iban : String?
    var investmentExperienceQuestionnaire : InvestmentExperienceQuestionnaire?
    var language : String?
    var lastName : String?
    var nationality : String?
    var newNotifications : Bool?
    var phone : String?
    var phoneNumberConfirmed : Bool
    var placeOfBirth : String?
    var politicallyExposedPerson : Bool?
    var prefix : String?
    var sex : String?
    var status : String?
    var subscribeToEmails : Bool?
    var twoFactorAuthenticationEnabled : Bool?
    var twoFactorAuthenticationType : String?
    var usTaxLiability : Bool?
    var webidUpgradeProcessBlock : Bool?
    var taxInformation: TaxInformation?
    var commercialRegisterNumber : String?
    var investorClassValue : String?
    
    public init(accountOwner: String = "", actionId: String = "", address: Address? = nil, companyName: String = "", companyType: String = "", dateOfBirth: String = "", deleteAccountProcess: Bool = false, email: String = "", emailConfirmed: Bool = false, externalId: String = "", firstName: String = "", group: String = "", iban: String = "", investmentExperienceQuestionnaire: InvestmentExperienceQuestionnaire? = nil, language: String = "", lastName : String = "", nationality: String = "", newNotifications: Bool = false, phone: String = "", phoneNumberConfirmed: Bool = false, placeOfBirth: String = "", politicallyExposedPerson: Bool = false, prefix: String = "", sex: String = "", status: String = "", subscribeToEmails: Bool = false, twoFactorAuthenticationEnabled: Bool = true, usTaxLiability: Bool = true, webidUpgradeProcessBlock: Bool = true, twoFactorAuthenticationType: String = "", commercialRegisterNumber : String? = "", taxInformation: TaxInformation? = nil,
                investorClass: String? = nil) {
        self.accountOwner = accountOwner
        self.actionId = actionId
        self.address = address
        self.companyName = companyName
        self.companyType = companyType
        self.dateOfBirth = dateOfBirth
        self.deleteAccountProcess = deleteAccountProcess
        self.email = email
        self.emailConfirmed = emailConfirmed
        self.externalId = externalId
        self.firstName = firstName
        self.group = group
        self.iban = iban
        self.investmentExperienceQuestionnaire = investmentExperienceQuestionnaire
        self.language = language
        self.lastName = lastName
        self.nationality = nationality
        self.newNotifications = newNotifications
        self.phone = phone
        self.phoneNumberConfirmed = phoneNumberConfirmed
        self.placeOfBirth = placeOfBirth
        self.politicallyExposedPerson = politicallyExposedPerson
        self.prefix = prefix
        self.sex = sex
        self.status = status
        self.subscribeToEmails = subscribeToEmails
        self.twoFactorAuthenticationEnabled = twoFactorAuthenticationEnabled
        self.usTaxLiability = usTaxLiability
        self.webidUpgradeProcessBlock = webidUpgradeProcessBlock
        self.twoFactorAuthenticationType = twoFactorAuthenticationType
        self.taxInformation = taxInformation
        self.commercialRegisterNumber = commercialRegisterNumber
        self.investorClassValue = investorClass
    }
}

public extension User {
    
    var fullName: String {
        guard let firstName = try? self.firstNameProvider.value(),
            let lastName = try? self.lastNameProvider.value() else { return ""}
        return "\(firstName) \(lastName)"
    }
    
    var isInstitutionalInvestor: Bool {
        guard let group = try? self.groupProvider.value() else { return false }
        return AccountGroup(rawValue: group) == .institutionalInvestor
    }
    
    var formattedDate: String? {
        guard let date = try? self.dateOfBirthProvider.value() else { return nil }
        return DateHelper.shared.convertedBackendBirthString(dateString: date)
    }
    
    var twoFAEnabled: Bool? {
        try? self.twoFactorAuthenticationEnabledProvider.value()
    }
    
    var investorClass: InvestorClass? {
        return InvestorClass(rawValue: (try? self.investorClassProvider.value()) ?? "")
    }
}

