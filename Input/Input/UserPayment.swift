//
//  Payment.swift
//  Input
//
//  Created by 8pitch on 9/30/20.
//

public class UserPayment: UserTransaction {
    public let companyAddress: String?
    public let companyName: String?
    public let isin: String?
    public let monetaryAmount: NSDecimalNumber?
    public let paymentSystem: String?
    public let transactionID: String?
    
    public init(amount: NSDecimalNumber?, companyAddress: String?, companyName: String?,
         isin: String?, monetaryAmount: NSDecimalNumber?, paymentSystem: String?,
         transactionDate: String?, transactionID: String?, shortCut: String, investmentID: String?) {
        self.companyAddress = companyAddress
        self.companyName = companyName
        self.isin = isin
        self.monetaryAmount = monetaryAmount
        self.paymentSystem = paymentSystem
        self.transactionID = transactionID
        super.init(amount: amount, transactionDate: transactionDate, shortCut: shortCut, receiptID: investmentID)
    }
}
