//
//  EditCellType.swift
//  Input
//
//  Created by 8pitch on 9/18/20.
//

public enum EditCellType: String {
    case birth = "profile.personal.personal.birth"
    case country = "profile.personal.address.country"
    case address = "profile.personal.edit.address"
    case title = "profile.personal.personal.title"
    case firstName = "profile.personal.edit.firstName"
    case lastName = "profile.personal.edit.lastName"
    case nationality = "profile.personal.personal.nationality"
    case city = "profile.personal.address.city"
    case zip = "profile.personal.address.zip"
    case companyName = "profile.personal.company.name"
    case companyNumber = "profile.personal.company.number"
    case companyType = "profile.personal.company.type"
    case iban = "profile.personal.financial.iban"
    case accountOwner = "profile.personal.financial.owner"
    case PEP = "info.details.title"
    case USTL = "profile.personal.ustl"
    case place = "profile.personal.personal.place"
    case email
    case phone
    case germanTaxesResident
    case germanTaxesid
    case germanTaxesOffice
    case churchTaxLability
    case churchTaxAttribute
}
