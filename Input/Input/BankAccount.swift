//
//  BankAccount.swift
//  Input
//
//  Created by 8pitch on 08.09.2020.
//

import Foundation

public enum BankAccountError: ModelError {
    case name
    case bic
    case iban
    
    public var errorDescription : String? {
        switch self {
        case .name:
            return "payment.direct.error.owner".localized
        case .bic:
            return "payment.direct.error.bic".localized
        case .iban:
            return "payment.direct.error.iban".localized
        }
    }
}

public class BankAccount {
    public var bic: String
    public var iban: String
    public var owner: String?
    
    private let namePattern = "^[A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'-]*+([ A-Za-z\u{00C0}-\u{00FF}][A-Za-z\u{00C0}-\u{00FF}'-]+)*"
    private let bicPattern = "[A-Za-z0-9]"
    
    public func validateName() -> Bool {
        guard let owner = self.owner,
            let regex = try? NSRegularExpression(pattern: self.namePattern) else {
                return false
        }
        let searchable = NSRange(location: 0, length: owner.count)
        let found = regex.rangeOfFirstMatch(in: owner, options: [], range: searchable)
        return (!owner.isEmpty && 1 < owner.count && owner.count < Constants.nameCount && found == searchable)
    }
    
    public func validateIBAN() -> Bool {
        let iban = self.iban.trimmingCharacters(in: .whitespaces).uppercased()
        return iban.passesMod97Check() && !self.iban.isEmpty
    }
    
    public func validateBIC() -> Bool {
        guard let regex = try? NSRegularExpression(pattern: self.bicPattern) else {
            return false
        }
        let searchable = NSRange(location: 0, length: self.bic.count)
        let result = regex.matches(in: self.bic, range: searchable).count == self.bic.count
        return !self.bic.isEmpty && self.bic.count < Constants.bicCount && result
    }
    
    public func validationError() -> Error? {
        let isNameValid = validateName()
        let isIBANValid = validateIBAN()
        let isBICValid = validateBIC()
        if !isNameValid {
            return BankAccountError.name
        }
        if !isIBANValid {
            return BankAccountError.iban
        }
        if !isBICValid {
            return BankAccountError.bic
        }
        return nil
    }
    
    public init(bic: String = "", iban: String = "", owner: String?) {
        self.bic = bic
        self.iban = iban
        self.owner = owner
    }
    
    private enum Constants {
        static let bicCount: Int = 12
        static let nameCount: Int = 51
    }
}
