//
//  Notification.swift
//  Input
//
//  Created by 8pitch on 9/11/20.
//

import Foundation

public enum ProjectNotificationType: String, CaseIterable {
    
    case levelTwoUpgrade = "LEVEL_2_UPGRADE"
    case levelTwoReject = "LEVEL_2_REJECT"
    case projectRequestInProgress = "PROJECT_REQUEST_IN_PROGRESS"
    case projectRequestAccepted = "PROJECT_REQUEST_ACCEPTED"
    case projectRequestDeclined = "PROJECT_REQUEST_DECLINED"
    case projectRequestRequireAdditionalInfo = "PROJECT_REQUEST_REQUIRE_ADDITIONAL_INFORMATION"
    case reviewTokenParamDoc = "REVIEW_TOKEN_PARAMETERS_DOCUMENT"
    case tokenCreated = "TOKEN_CREATED"
    case tokenCreatedAdmin = "TOKEN_CREATED_ADMIN"
    case reviewProjectPage = "REVIEW_PROJECT_PAGE"
    case paymentMissingAfterThreeDays = "PAYMENT_IS_MISSING_AFTER_3_BUSINESS_DAYS"
    case paymentMissingAfterSevenDays = "PAYMENT_IS_MISSING_AFTER_7_BUSINESS_DAYS"
    case formTwoDeadlinePassed = "FORM_2_DEADLINE_PASSED"
    case projectStarted = "PROJECT_STARTED"
    case newProjectRequest = "NEW_PROJECT_REQUEST"
    case filledFormTwo = "FILLED_FORM_2"
    case tokenParametersDocumentRejected = "TOKEN_PARAMETERS_DOCUMENT_REJECTED"
    case tokenParametersDocumentApproved = "TOKEN_PARAMETERS_DOCUMENT_APPROVED"
    case projectPageDraftRejected = "PROJECT_PAGE_DRAFT_REJECTED"
    case projectPageDraftApproved = "PROJECT_PAGE_DRAFT_APPROVED"
    case projectStartedAdmin = "PROJECT_STARTED_ADMIN"
    case projectFinishedSuccess = "PROJECT_FINISHED_SUCCESSFULLY"
    case projectFinishedSuccessAdmin = "PROJECT_FINISHED_SUCCESSFULLY_ADMIN"
    case projectFinishedSuccessManualRelease = "PROJECT_FINISHED_SUCCESSFULLY_MANUAL_RELEASE"
    case projectFinishedUnsuccess = "PROJECT_FINISHED_UNSUCCESSFULLY"
    case projectFinishedUnsuccessAdmin = "PROJECT_FINISHED_UNSUCCESSFULLY_ADMIN"
    case paymentMissingTenDaysAfterInvestorAgreed = "PAYMENT_IS_MISSING_10_BUSINESS_DAYS_AFTER_INVESTOR_AGREED_TO_PAY"
    case levelTwoKYCInternal = "LEVEL_2_KYC_INTERNAL"
    case projectRequestLeftOut = "PROJECT_REQUEST_LEFT_OUT"
    case investmentReceipt = "INVESTMENT_RECEIPT"
    case updateUserRequest = "UPDATE_USER_REQUEST"
    case deleteUserRequest = "DELETE_USER_REQUEST"
    case cancelNotConfirmedInvestment = "CANCEL_NOT_CONFIRMED_INVESTMENT"
    case paymentCancelled = "PAYMENT_CANCELLED"
    case personalInfoChanged = "PERSONAL_INFO_CHANGED"
    case tokenTransfer = "TOKEN_TRANSFER"
    case investmentCancelled = "INVESTMENT_CANCELLED"
    case transferSent = "TRANSFER_SENT"
    case transferReceived = "TRANSFER_RECEIVED"
    case votingStart = "VOTING_START"
    case votingPassing = "VOTING_PASSED"
    case votingResultRecived = "VOTING_FINAL_RESULT_RECEIVED"
    case votingStartPI = "VOTING_START_PI"
    case votingCompletion = "VOTING_ENDED"
    case votingRequestForApprovalPI = "VOTING_REQUEST_FOR_APPROVAL_PI"
    case votingApprovedByAdmin = "VOTING_APPROVED_BY_ADMIN"
    case votingGreetings = "VOTING_GREETINGS"
    case votingWithStreamApproved = "VOTING_WITH_STREAM_APPROVED_BY_ADMIN"
    case votingStreamInfoChanged = "VOTING_STREAM_INFO_CHANGED"
    case votingStreamStart = "VOTING_STREAM_START"
    case trusteeAppointment = "TRUSTEE_APPOINTMENT"
    case trusteeStart = "TRUSTEE_START"
    case trusteeReminder = "TRUSTEE_REMINDER"
    case trusteeFinalResult = "TRUSTEE_FINAL_RESULTS"
    case unexpected
    
    init(value: String) {
        if let type = ProjectNotificationType(rawValue: value) {
            self = type
        } else {
            self = .unexpected
        }
    }
}

public enum ProjectNotificationStatus: String, CaseIterable {
    
    case new = "NEW"
    case read = "READ"
    
}

public class ProjectNotification {
    
    public var identifier : Int?
    public var created: Date?
    public var status: ProjectNotificationStatus?
    public var type: ProjectNotificationType?
    public var text: String?
    public var arguments: [String] = []
    public var isExpanded: Bool = false
    
    public init(id : Int?, createDateTime: String?, status: String?, type: String?, text: String?) {
        guard  let status = status,
               let parsedStatus = ProjectNotificationStatus(rawValue: status),
               let params = text?.components(separatedBy: ",") else { return }
        var type = ProjectNotificationType(value: type ?? "")
        switch type {
        case .projectRequestRequireAdditionalInfo, .reviewTokenParamDoc, .reviewProjectPage, .paymentMissingAfterThreeDays, .paymentMissingAfterSevenDays, .newProjectRequest, .projectFinishedSuccessManualRelease:
            if params.count == 1 {
                arguments = params
            } else {
                type = .unexpected
            }
        case .projectStarted, .filledFormTwo, .tokenParametersDocumentRejected,
             .tokenParametersDocumentApproved, .projectPageDraftRejected, .projectPageDraftApproved,
             .projectStartedAdmin, .projectFinishedSuccessAdmin, .projectFinishedUnsuccessAdmin,
             .votingPassing, .votingResultRecived, .votingCompletion,
             .votingApprovedByAdmin, .trusteeFinalResult, .trusteeReminder:
            if params.count == 2 {
                arguments = params
            } else {
                type = .unexpected
            }
        case .tokenCreated, .tokenCreatedAdmin, .paymentMissingTenDaysAfterInvestorAgreed,
             .projectRequestLeftOut, .votingStart, .votingRequestForApprovalPI,
             .votingStreamStart:
            if params.count == 3 {
                arguments = params
            } else {
                type = .unexpected
            }
        case .votingWithStreamApproved, .votingStreamInfoChanged:
            let formatter = DateHelper.shared.backendFormatter
            if params.count == 3,
               let _ = formatter.date(from: params[2]) {
                arguments = params
            } else {
                type = .unexpected
            }
        case .deleteUserRequest, .votingStartPI, .trusteeStart:
            if params.count == 4 {
                arguments = params
            } else {
                type = .unexpected
            }
        case .trusteeAppointment:
            if params.count == 7 {
                arguments = params
            } else {
                type = .unexpected
            }
        case .votingGreetings:
            if params.count == 10 {
                arguments = params
            } else {
                type = .unexpected
            }
        default: break
        }
        let formatter = DateHelper.shared.backendFormatter
        self.identifier = id
        self.created = formatter.date(from: createDateTime ?? "")?.withCurrentTimeZoneOffset()
        self.status = parsedStatus
        self.type = type
        self.text = text
    }
    
}

extension ProjectNotification {
    
    public func parseToMutableString(baseUrl: URL?) -> NSMutableAttributedString? {
        let formatter = DateHelper.shared.backendFormatter
        let uiFormatter = DateHelper.shared.uiFormatterFull
        guard let url = baseUrl?.absoluteString else { return nil }
        var attributedString = NSMutableAttributedString()
        
        switch type {
        case .levelTwoUpgrade:
            attributedString = NSMutableAttributedString(string: "investor.account.upgraded.to.level.2".localized)
        case .levelTwoReject:
            attributedString = NSMutableAttributedString(string: "investor.account.upgrade.declined".localized)
        case .projectRequestInProgress:
            attributedString = NSMutableAttributedString(string: "status.of.project.request.changed".localized)
        case .projectRequestAccepted:
            attributedString = NSMutableAttributedString(string: "project.request.accepted".localized)
        case .projectRequestDeclined:
            attributedString = NSMutableAttributedString(string: "project.request.declined".localized)
        case .projectRequestRequireAdditionalInfo:
            let string = String(format: "require.additional.info.for.project.request".localized, "Link")
            attributedString = NSMutableAttributedString(string: string)
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[0], range: linkRange)
        case .reviewTokenParamDoc:
            attributedString = NSMutableAttributedString(string: String(format: "review.token.parameters.document".localized, arguments[0]))
        case .tokenCreated:
            attributedString = NSMutableAttributedString(string: String(format: "request.for.token.creation".localized, arguments[0], arguments[1], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[2], range: linkRange)
        case .tokenCreatedAdmin:
            attributedString = NSMutableAttributedString(string: String(format: "request.for.token.creation.admin".localized, arguments[0], arguments[1], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[2], range: linkRange)
        case .reviewProjectPage:
            attributedString = NSMutableAttributedString(string: String(format: "review.project.page".localized, arguments[0]))
        case .paymentMissingAfterThreeDays:
            attributedString = NSMutableAttributedString(string: String(format: "payment.is.missing.after.3.business.days".localized, arguments[0]))
        case .paymentMissingAfterSevenDays:
            attributedString = NSMutableAttributedString(string: String(format: "payment.is.missing.after.7.business.days".localized, arguments[0]))
        case .formTwoDeadlinePassed:
            attributedString = NSMutableAttributedString(string: "deadline.for.additional.info.passed".localized)
        case .projectStarted:
            attributedString = NSMutableAttributedString(string: String(format: "project.started".localized, arguments[0], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[1], range: linkRange)
        case .newProjectRequest:
            attributedString = NSMutableAttributedString(string: String(format: "new.project.request".localized, arguments[0]))
        case .filledFormTwo:
            attributedString = NSMutableAttributedString(string: String(format: "filled.form.2".localized, arguments[0], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[1], range: linkRange)
        case .tokenParametersDocumentRejected:
            attributedString = NSMutableAttributedString(string: String(format: "token.parameters.document.rejected".localized, arguments[0], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[1], range: linkRange)
        case .tokenParametersDocumentApproved:
            attributedString = NSMutableAttributedString(string: String(format: "token.parameters.document.approved".localized, arguments[0], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[1], range: linkRange)
        case .projectPageDraftRejected:
            attributedString = NSMutableAttributedString(string: String(format: "project.page.draft.rejected".localized, arguments[0], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[1], range: linkRange)
        case .projectPageDraftApproved:
            attributedString = NSMutableAttributedString(string: String(format: "project.page.draft.approved".localized, arguments[0], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[1], range: linkRange)
        case .projectStartedAdmin:
            attributedString = NSMutableAttributedString(string: String(format: "project.started.admin".localized, arguments[0], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[1], range: linkRange)
        case .projectFinishedSuccess:
            attributedString = NSMutableAttributedString(string: "project.finished.successfully".localized)
        case .projectFinishedSuccessAdmin:
            attributedString = NSMutableAttributedString(string: String(format: "project.finished.successfully.admin".localized, arguments[0], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[1], range: linkRange)
        case .projectFinishedSuccessManualRelease:
            attributedString = NSMutableAttributedString(string: String(format: "project.finished.successfully.admin.manual.release".localized, "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[0], range: linkRange)
        case .projectFinishedUnsuccess:
            attributedString = NSMutableAttributedString(string: "project.finished.unsuccessfully".localized)
        case .projectFinishedUnsuccessAdmin:
            attributedString = NSMutableAttributedString(string: String(format: "project.finished.unsuccessfully.admin".localized, arguments[0], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[1], range: linkRange)
        case .paymentMissingTenDaysAfterInvestorAgreed:
            attributedString = NSMutableAttributedString(string: String(format: "payment.is.missing.after.10.business.days".localized, arguments[0], arguments[1], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[2], range: linkRange)
        case .levelTwoKYCInternal:
            attributedString = NSMutableAttributedString(string: "LEVEL_2_KYC_INTERNAL".localized)
        case .projectRequestLeftOut:
            attributedString = NSMutableAttributedString(string: String(format: "project.request.left.out".localized, arguments[0], arguments[1], "Link"))
            let linkRange = attributedString.mutableString.range(of: "Link")
            attributedString.addAttribute(.link, value: url + Constants.path + arguments[2], range: linkRange)
        case .investmentReceipt:
            attributedString = NSMutableAttributedString(string: "investment.receipt".localized)
        case .updateUserRequest:
            attributedString = NSMutableAttributedString(string: "UPDATE_USER_REQUEST".localized)
        case .deleteUserRequest:
            attributedString = NSMutableAttributedString(string: String(format: "request.to.delete.account.by.investor.level.2".localized, arguments[0], arguments[1], arguments[2], arguments[3]))
        case .cancelNotConfirmedInvestment:
            attributedString = NSMutableAttributedString(string: "investment.canceled".localized)
        case .paymentCancelled:
            attributedString = NSMutableAttributedString(string: "payment.canceled".localized)
        case .personalInfoChanged:
            attributedString = NSMutableAttributedString(string: "personal.info.changed".localized)
        case .investmentCancelled:
            attributedString = NSMutableAttributedString(string: "investment.canceled.add".localized)
        case .tokenTransfer:
            attributedString = NSMutableAttributedString(string: "token.transfer.info".localized)
        case .transferSent:
            attributedString = NSMutableAttributedString(string: "token.transfer.sent".localized)
        case .transferReceived:
            attributedString = NSMutableAttributedString(string: "token.transfer.received".localized)
        case .votingStart:
            let text = NSMutableAttributedString(string: String(format: "notification.voting.votingStart".localized, arguments[2], Constants.passLink, arguments[1]))
            attributedString = self.linkRange(for: text, of: [.pass], id: arguments[0], url: url)
        case .votingPassing:
            attributedString = NSMutableAttributedString(string: String(format: "notification.voting.votingPassing".localized, arguments[1], arguments[0]))
        case .votingResultRecived:
            attributedString = NSMutableAttributedString(string: String(format: "notification.voting.votingResultRecived".localized, arguments[1], arguments[0]))
        case .votingStartPI:
            let text = NSMutableAttributedString(string: String(format: "notification.voting.votingStartPI".localized, arguments[2], arguments[1], arguments[3], Constants.listLink))
            attributedString = self.linkRange(for: text, of: [.list], id: arguments[0], url: url)
        case .votingCompletion:
            attributedString = NSMutableAttributedString(string: String(format: "notification.voting.votingCompletion".localized, arguments[1], arguments[0]))
        case .votingRequestForApprovalPI:
            let text = NSMutableAttributedString(string: String(format: "notification.voting.votingRequestForApprovalPI".localized, arguments[2], arguments[1], Constants.approveLink))
            attributedString = self.linkRange(for: text, of: [.approve], id: arguments[0], url: url)
        case .votingApprovedByAdmin:
            attributedString = NSMutableAttributedString(string: String(format: "notification.voting.votingApprovedByAdmin".localized, arguments[1], arguments[0]))
        case .votingGreetings:
            if let text = self.setupGreetingNotification(with: arguments, url: url) {
                attributedString = text
            } else {
                return nil
            }
        case .votingWithStreamApproved:
            if let date = formatter.date(from: arguments[2]) {
                attributedString = NSMutableAttributedString(string: String(format: "notification.stream.approved".localized, arguments[0], arguments[1], uiFormatter.string(from: date)))
            } else {
                return nil
            }
        case .votingStreamInfoChanged:
            if let date = formatter.date(from: arguments[2]) {
                attributedString = NSMutableAttributedString(string: String(format: "notification.stream.edited".localized, arguments[0], arguments[1], uiFormatter.string(from: date)))
            } else {
                return nil
            }
        case .votingStreamStart:
            let text = NSMutableAttributedString(string: String(format: "notification.stream.start".localized, arguments[0], arguments[1], Constants.streamLink))
            attributedString = self.linkRange(for: text, of: [.stream], id: arguments[2], url: url)
        case .trusteeAppointment:
            attributedString = self.setupTrusteeNotification(with: arguments)
        case .trusteeStart:
            attributedString = NSMutableAttributedString(string: String(format: "notification.trustee.start".localized, arguments[1], arguments[0], arguments[2]))
        case .trusteeReminder:
            attributedString = NSMutableAttributedString(string: String(format: "notification.trustee.reminder".localized, arguments[0]))
        case .trusteeFinalResult:
            attributedString = NSMutableAttributedString(string: String(format: "notification.trustee.result".localized, arguments[0], arguments[1]))
        case .none,
             .unexpected:
            return nil
        }
        return attributedString
    }
    
    public func changeExpandedState() {
        isExpanded = !isExpanded
    }
    
    private func setupTrusteeNotification(with arguments: [String]) -> NSMutableAttributedString {
        var attributedString = NSMutableAttributedString()
        var oldTrustee: String?
        var newTrustee: String?
        if !arguments[1].isEmpty && !arguments[2].isEmpty && !arguments[3].isEmpty {
            oldTrustee = "\(Title(rawTitle: arguments[1]).rawValue.localized) \(arguments[2]) \(arguments[3])"
        }
        if !arguments[4].isEmpty && !arguments[5].isEmpty && !arguments[6].isEmpty {
            newTrustee = "\(Title(rawTitle: arguments[4]).rawValue.localized) \(arguments[5]) \(arguments[6])"
        }
        attributedString = NSMutableAttributedString(string: String(format: "notification.trustee.appointment.1".localized, arguments[0]))
        if let name = oldTrustee {
            attributedString.append(NSAttributedString(string: String(format: "notification.trustee.appointment.2".localized, name)))
        }
        if let name = newTrustee {
            attributedString.append(NSAttributedString(string: String(format: "notification.trustee.appointment.3".localized, name)))
        } else {
            attributedString.append(NSAttributedString(string: "notification.trustee.appointment.4".localized))
        }
        return attributedString
    }
    
    private func setupGreetingNotification(with arguments: [String], url: String) -> NSMutableAttributedString? {
        let formatter = DateHelper.shared.backendFormatter
        let uiFormatter = DateHelper.shared.uiFormatterFull
        let companyName = arguments[0]
        let surname = arguments[1]
        let id = arguments[9]
        let streamStart = formatter.date(from: arguments[4]) ?? Date()
        let votingStart = formatter.date(from: arguments[5]) ?? Date()
        let ICstart = formatter.date(from: arguments[2]) ?? Date()
        let ICend = formatter.date(from: arguments[3]) ?? Date()
        
        guard let isIC = Bool(arguments[6]),
              let isStream = Bool(arguments[7]),
              let isTrustee = Bool(arguments[8]) else { return nil }
        
        if !isIC && !isStream && !isTrustee {
            return nil
        }
        
        var convertedArguments = [surname, companyName]
        var textFormat = "notification.greeting"
        var types: [VotingActionType] = []
        if isIC {
            convertedArguments.append(contentsOf: [uiFormatter.string(from: ICstart), uiFormatter.string(from: ICend), Constants.surveyLink])
            textFormat += ".IC"
            types.append(.survey)
        }
        if isStream {
            convertedArguments.append(contentsOf: [uiFormatter.string(from: streamStart), Constants.streamLink])
            textFormat += ".stream"
            types.append(.stream)
        }
        convertedArguments.append(uiFormatter.string(from: votingStart))
        if isTrustee {
            convertedArguments.append(Constants.trusteeLink)
            textFormat += ".trustee"
            types.append(.trustee)
        }
        let text = NSMutableAttributedString(string: String(format: textFormat.localized, arguments: convertedArguments))
        return self.linkRange(for: text, of: types, id: id, url: url)
    }
    
    private func linkRange(for text: NSMutableAttributedString, of types: [VotingActionType], id: String, url: String) -> NSMutableAttributedString {
        for type in types {
            var link: String
            switch type {
            case .stream:
                link = Constants.streamLink
            case .survey:
                link = Constants.surveyLink
            case .trustee:
                link = Constants.trusteeLink
            case .approve:
                link = Constants.approveLink
            case .list:
                link = Constants.listLink
            case .pass:
                link = Constants.passLink
            }
            let linkRange = text.mutableString.range(of: link)
            text.addAttribute(.link, value: url + type.rawValue + id, range: linkRange)
        }
        return text
    }
    
    private enum Constants {
        static let url = URL(string: "https://8pitch.com/")
        static let path = "/dashboard/project-additional-info/"
        static let surveyLink = "notification.greeting.link.IC".localized
        static let streamLink = "notification.greeting.link.stream".localized
        static let trusteeLink = "notification.greeting.link.trustee".localized
        static let approveLink = "notification.voting.votingRequestForApprovalPI.link".localized
        static let listLink = "notification.voting.votingStartPI.link".localized
        static let passLink = "notification.voting.votingStart.link".localized
    }
}

public enum VotingActionType: String, CaseIterable {
    case approve = "/votings/approve/"
    case pass = "/votings/pass/"
    case list = "/votings/list/"
    case survey = "/votings/survey/"
    case stream = "/votings/stream/"
    case trustee = "/votings/trustee/"
}

