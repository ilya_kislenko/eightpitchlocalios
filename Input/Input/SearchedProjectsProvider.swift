//
//  SearchedProjectsProvider.swift
//  Input
//
//  Created by 8pitch on 3.02.21.
//

import Foundation

public class SearchedProjectsProvider {
    
    public var availableStatuses: [ProjectStatus] = [.comingSoon, .active, .finished, .waitBlockchain, .refunded, .manualRelease, .waitRelease, .tokensTransferred]
    public var searchedProjects: [Project] = []
    public var searchedText: String = ""
    
    public func resetFilter() {
        searchedProjects = []
        searchedText = ""
    }
    
    public func getQueryItems() -> [URLQueryItem] {
        let queryItems: [URLQueryItem] = [URLQueryItem(name: "projectStatuses", value: availableStatuses.map({ $0.rawValue }).joined(separator: ",")),
                                          URLQueryItem(name: "size", value: "\(Constants.size)"),
                                          URLQueryItem(name: "page", value: "\(Constants.page)"),
                                          URLQueryItem(name: "companyName", value: "\(searchedText)")]
        
        return queryItems
    }
    
    enum Constants {
        static let size: Int = 9999
        static let page: Int = 0
    }
}
