//
//  ClosedRange.swift
//  8Pitch
//
//  Created by 8pitch on 23.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation

public extension ClosedRange where Bound == Int {
    
    func tick(by timeInterval: Double, tickCallback: @escaping (Int?) -> Void) {
        var i: Int = self.first ?? 0
        Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true, block: { timer in
            i += 1
            if i > self.last ?? 0 {
                timer.invalidate()
                tickCallback(nil)
            } else {
                tickCallback(i)
            }
        })
    }
    
}

public extension ReversedCollection where Base == ClosedRange<Int> {
    
    func tick(by timeInterval: Double, tickCallback: @escaping (Int?) -> Void) {
        var i: Int = self.first ?? 0
        Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true, block: { timer in
            i -= 1
            if i < self.last ?? 0 {
                timer.invalidate()
                tickCallback(nil)
            } else {
                tickCallback(i)
            }
        })
    }
    
}
