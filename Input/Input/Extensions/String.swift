//
//  String.swift
//  Input
//
//  Created by 8pitch on 30.07.2020.
//

import Foundation

public extension String {
    
    var localized : String {
        NSLocalizedString(self, comment: "")
    }
    
    private func mod97() -> Int {
        let symbols: [Character] = Array(self)
        let swapped = symbols.dropFirst(4) + symbols.prefix(4)
        
        let mod: Int = swapped.reduce(0) { (previousMod, char) in
            let value = Int(String(char), radix: 36)! // "0" => 0, "A" => 10, "Z" => 35
            let factor = value < 10 ? 10 : 100
            return (factor * previousMod + value) % 97
        }
        
        return mod
    }
    
    func passesMod97Check() -> Bool {
        guard self.count >= 4 else {
            return false
        }
        
        let uppercase = self.uppercased()
        
        guard uppercase.range(of: "^[0-9A-Z]*$", options: .regularExpression) != nil else {
            return false
        }
        
        return (uppercase.mod97() == 1)
    }
    
    var countryName: String {
        DataSource.countries.first(where: { self.uppercased() == $0.iso })?.name ?? ""
    }
    
    mutating func localizedWith(_ map: LanguageMap) {
        self = ((map.current[self] ?? map.initial[self]) ?? self) ?? ""
    }
    
    mutating func localizedHeaderWith(_ map: LanguageMap) {
        self = ((map.current[self] ?? map.initial[self]) as? String) ?? ""
    }
    
    mutating func setIDWith(_ map: [String?:String?]) {
        self = (map[self] ?? self) ?? ""
    }
}
