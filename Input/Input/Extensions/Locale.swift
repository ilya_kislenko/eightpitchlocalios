//
//  Locale.swift
//  8Pitch
//
//  Created by 8pitch on 15.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation

public extension Locale {
    
    func isoCode(for countryName: String) -> String {
        if #available(iOS 13.0, *) {
        return Locale.isoRegionCodes.first(where: { (code) -> Bool in
            localizedString(forRegionCode: code)?.compare(countryName, options: [.caseInsensitive, .diacriticInsensitive]) == .orderedSame
        }) ?? "CZ"
        } else {
            let languagesThatWorkCorrect: Set = ["en", "de"]
            if languagesThatWorkCorrect.contains(String(Locale.preferredLanguages[0].prefix(2))) {
                return Locale.isoRegionCodes.first(where: { (code) -> Bool in
                    localizedString(forRegionCode: code)?.compare(countryName, options: [.caseInsensitive, .diacriticInsensitive]) == .orderedSame
                }) ?? "CZ"
            } else {
                return Locale.isoRegionCodes.first(where: { (code) -> Bool in
                    Locale(identifier: "en_US").localizedString(forRegionCode: code)?.compare(countryName, options: [.caseInsensitive, .diacriticInsensitive]) == .orderedSame
                }) ?? "CZ"
            }
        }
    }
    
}
