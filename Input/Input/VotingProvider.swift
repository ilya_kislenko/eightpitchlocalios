//
//  VotingProvider.swift
//  Input
//
//  Created by 8pitch on 05.03.2021.
//

import Foundation

public struct StreamCreds {
    public let meetingNumber: Int
    public let meetingPassword: String
    public let signature: String
    
    public init(meetingNumber: Int,
                meetingPassword: String,
                signature: String) {
        self.meetingNumber = meetingNumber
        self.meetingPassword = meetingPassword
        self.signature = signature
    }
}

public struct VotingFilter {
    public let statuses: [Voting.VotingStatus]
    public let userStatus: Voting.UserVotingsStatus?
    
    public init(statuses: [Voting.VotingStatus], userStatus: Voting.UserVotingsStatus?) {
        self.statuses = statuses
        self.userStatus = userStatus
    }
}

public class VotingProvider {
    public var votings: [Voting] = []
    public var currentVoting: Voting?
    public var answers: [Answer] = []
    public var votingResult: VotingResult?
    public var filterStatuses: [Voting.VotingStatus] = []
    public var streamCreds: StreamCreds?
    public var currentFilter: VotingFilter?
    public var trusteeDecision: Voting.InvestorTrusteeDecision?
    public var currentProject: VotingProjectInfo?
    
    public init() {}
}
