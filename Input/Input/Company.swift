//
//  Company.swift
//  Input
//
//  Created by 8pitch on 27.07.2020.
//

import Foundation
import RxSwift

public enum CompanyType : String, CaseIterable {
    
    case ag = "AG"
    case eg = "EG"
    case eingetragenerKaufmann = "EINGETRAGENER_KAUFMANN"
    case freiberuflerSelbstaendig = "FREIBERUFLER_SELBSTSTÄNDIG"
    case gbrBGBGesellschaft = "GBR_BGBGESELLSCHAFT"
    case gmbh = "GMBH"
    case gmbhCoKg = "GMBH_CO_KG"
    case kg = "KG"
    case kgaa = "KGAA"
    case ohg = "OHG"
    case ohgCoKg = "OHG_CO_KG"
    case ug = "UG"
    case limited = "LIMITED"
    case eingentragenerVerein = "EINGETRAGENER_VEREIN"
    case stiftung = "STIFTUNG"
    case other = "OTHER"
    
    public init(rawValue: String) {
        self = Self.localized[rawValue] ?? .other
    }
    
    private static var localized : [String : Self] {
        let cases : [Self] = [ .ag, .eg, .eingetragenerKaufmann, .freiberuflerSelbstaendig, .gbrBGBGesellschaft, .gmbh, .ohgCoKg, .kg, .kgaa, .ohg, .ohgCoKg, .ug, .limited, .eingentragenerVerein, .stiftung, .other ]
        return cases.reduce(into: [String : Self]()) {
            $0[$1.rawValue.localized] = $1
        }
    }
    
}

public enum CompanyTypeError : ModelError {
    case typeInvalid
    
    public var errorDescription : String? {
        switch self {
        case .typeInvalid:
            return "input.error.empty".localized
        }
    }
}

public enum CompanyNumberError : ModelError {
    case numberEmpty
    
    public var errorDescription : String? {
        switch self {
        case .numberEmpty:
            return "input.error.empty".localized
        }
    }
}

public enum CompanyError : ModelError {
    case nameInvalid
    case companyNamePatternInvalid
    
    public var errorDescription : String? {
        switch self {
        case .nameInvalid:
            return "input.error.empty".localized
        case .companyNamePatternInvalid:
            return "input.error.name.companyName".localized + " " +
                "input.error.pattern.name".localized
        }
    }
}

public class Company : Model {
    
    public let companyNameProvider : BehaviorSubject<String?> = .init(value: nil)
    public let companyTypeProvider : PublishSubject<String?> = .init()
    public let registrationNumberProvider : BehaviorSubject<String?> = .init(value: nil)
    
    public var valueProviders : [ KeyPath<Company, PublishSubject<String?>> : (() -> Void)? ] = [:]
    
    internal var companyName : String = ""
    internal var companyType : CompanyType?
    internal var registrationNumber : String = ""
    
    public init(user: User? = nil) {
        super.init()
        self.companyNameProvider.onNext(try? user?.companyNameProvider.value())
        self.companyTypeProvider.onNext(try? user?.companyTypeProvider.value())
        self.registrationNumberProvider.onNext(try? user?.commercialRegisterNumberProvider.value())
    }
    
    override func setup() {
        self.companyTypeProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                guard !$0.isEmpty else {
                    self?.isValid.onNext(.failure(CompanyTypeError.typeInvalid))
                    return
                }
                self?.companyType = CompanyType(rawValue: $0.localized)
                self?.isValid.onNext(.success(false))
            }).disposed(by: self.disposables)
        self.companyNameProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                if case .failure(let reason) = self?.validated(string: $0) {
                    switch reason {
                    case .empty: self?.isValid.onNext(.failure(CompanyError.nameInvalid))
                    case .pattern,
                         .format: self?.isValid.onNext(.failure(CompanyError.companyNamePatternInvalid))
                    }
                } else {
                    self?.isValid.onNext(.success(false))
                }
                self?.companyName = $0
            }).disposed(by: self.disposables)
        self.registrationNumberProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.registrationNumber = $0
                guard !$0.isEmpty else {
                    self?.isValid.onNext(.failure(CompanyNumberError.numberEmpty))
                    return
                }
                self?.isValid.onNext(.success(false))
            }).disposed(by: self.disposables)
    }
    
    override func validate(string: String) -> Bool {
        !string.isEmpty
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.companyNameProvider.onNext(nil)
        self.companyTypeProvider.onNext(nil)
        self.registrationNumberProvider.onNext(nil)
        self.companyName = ""
        self.companyType = .other
        self.registrationNumber = ""
        self.valueProviders = [:]
        self.setup()
    }
    
    private func validated(string: String) -> Result<Bool, ValidatorError> {
        let validator = Validator()
        return validator.validate(string: string, type: .companyName)
    }
    
    public func validate() {
        let isCompanyValid = self.validated(string: self.companyName) == .success(true)
        if !isCompanyValid {
            if case .failure(let reason) = self.validated(string: self.companyName) {
                switch reason {
                case .empty: self.isValid.onNext(.failure(CompanyError.nameInvalid))
                case .pattern,
                     .format: self.isValid.onNext(.failure(CompanyError.companyNamePatternInvalid))
                }
            }
            return
        }
        let isTypeValid = self.companyType != nil
        if !isTypeValid {
            self.isValid.onNext(.failure(CompanyTypeError.typeInvalid))
            return
        }
        let isNumberValid = !self.registrationNumber.isEmpty
        if !isNumberValid {
            self.isValid.onNext(.failure(CompanyNumberError.numberEmpty))
            return
        }
        if isCompanyValid && isTypeValid && isNumberValid {
            self.isValid.onNext(.success(true))
        }
    }
    
}
