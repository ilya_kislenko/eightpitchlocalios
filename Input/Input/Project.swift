//
//  Project.swift
//  Input
//
//  Created by 8pitch on 20.08.2020.
//

public enum ProjectStatus: String, CaseIterable {
    case notStarted = "NOT_STARTED"
    case comingSoon = "COMING_SOON"
    case active = "ACTIVE"
    case finished = "FINISHED"
    case waitBlockchain = "WAIT_BLOCKCHAIN"
    case refunded = "REFUNDED"
    case manualRelease = "MANUAL_RELEASE"
    case waitRelease = "WAIT_RELEASE"
    case tokensTransferred = "TOKENS_TRANSFERRED"
    
}

public enum ProjectPageStatus: String, CaseIterable {
    
    case draft = "DRAFT"
    case waiting = "WAITING"
    case rejected = "REJECTED"
    case approved = "APPROVED"
    case published = "PUBLISHED"
    
}

public class ProjectInitiator {
    public var email: String?
    public var firstName: String?
    public var lastName : String?
    public var phone : String?
    public var prefix : Title?
    
    public init(email: String?, firstName: String?, lastName : String?, phone : String?, prefix : Title?) {
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.phone = phone
        self.prefix = prefix
    }
}


public class Project {
    
    public var thumbnail: ProjectFile?? {
        (self.projectPageFiles.filter { $0?.fileType == "PROJECT_THUMBNAIL" }).first
    }
    
    public var companyLogo: String? {
        if let file = (self.projectPageFiles.filter { $0?.fileType == "COMPANY_LOGO_COLORED" }).first??.file {
            return file.id
        } else {
            return nil
        }
    }
        
    public var identifier : Int?
    public var companyName: String?
    public var description: String?
    public var projectPageURL: String?
    public var videoURL: String?
    public var graph: Graph?
    public var financialInformation: FinancialInformation?
    public var tokenParametersDocument: TokenParametersDocument?
    public var projectPageFiles: [ProjectFile?]
    
    public init(id : Int?, companyName: String?,
         description: String?, projectPageURL: String?,
         videoURL: String?, graph: Graph?,
         financialInformation: FinancialInformation?,
         tokenParametersDocument: TokenParametersDocument?,
         projectPageFiles: [ProjectFile?]) {
        self.identifier = id
        self.companyName = companyName
        self.description = description
        self.projectPageURL = projectPageURL
        self.videoURL = videoURL
        self.graph = graph
        self.financialInformation = financialInformation
        self.tokenParametersDocument = tokenParametersDocument
        self.projectPageFiles = projectPageFiles
    }
    
}

public class Graph {
        
    public var hardCap : Double?
    public var currentFundingSum: Double?
    public var hardCapInTokens: Int?
    public var currentFundingSumInTokens: Int?
    public var softCap: Double?
    public var softCapInTokens: Int?
    
    public init(hardCap : Double?, currentFundingSum: Double?,
         hardCapInTokens: Int?, currentFundingSumInTokens: Int?,
         softCap : Double?, softCapInTokens: Int?) {
        self.hardCap = hardCap
        self.currentFundingSum = currentFundingSum
        self.hardCapInTokens = hardCapInTokens
        self.currentFundingSumInTokens = currentFundingSumInTokens
        self.softCap = softCap
        self.softCapInTokens = softCapInTokens
    }
    
}

public class FinancialInformation {
        
    public var typeOfSecurity : String?
    public var investmentSeries: String?
    public var nominalValue: String?
    
    public init(typeOfSecurity : String?, investmentSeries: String?, nominalValue: String?) {
        self.typeOfSecurity = typeOfSecurity
        self.investmentSeries = investmentSeries
        self.nominalValue = nominalValue
    }
    
}

public class TokenParametersDocument {
        
    public var assetBasedFees: Double?
    public var assetId: String?
    public var breakable: Int?
    public var currentSupply: Int?
    public var distributorId: String?
    public var dsoProjectFinishDate: Date?
    public var dsoProjectStartDate: Date?
    public var hardCap: Int?
    public var id: Int?
    public var investmentStepSize: Double?
    public var isin: String?
    public var issuerId: String?
    public var minimumInvestmentAmount: Double?
    public var multisignRelease: Bool?
    public var nominalValue: Double?
    public var productType: String?
    public var projectId: Int?
    public var shortCut: String?
    public var singleInvestorOwnershipLimit: Double?
    public var softCap: Int?
    public var tokenId: String?
    public var totalNumberOfTokens: Int?
    public var voting: Bool?
    
    public init(assetBasedFees: Double?, assetId: String?, breakable: Int?,
                currentSupply: Int?, distributorId: String?, dsoProjectFinishDate: String?,
                dsoProjectStartDate: String?, hardCap: Int?, id: Int?,
                investmentStepSize: Double?, isin: String?, issuerId: String?,
                minimumInvestmentAmount: Double?, multisignRelease: Bool?, nominalValue: Double?,
                productType: String?, projectId: Int?, shortCut: String?,
                singleInvestorOwnershipLimit: Double?, softCap: Int?, tokenId: String?,
                totalNumberOfTokens: Int?, voting: Bool?) {
        let formatter = DateHelper.shared.backendFormatter
        self.dsoProjectStartDate = formatter.date(from: dsoProjectStartDate ?? "")
        self.dsoProjectFinishDate = formatter.date(from: dsoProjectFinishDate ?? "")
        self.assetBasedFees = assetBasedFees
        self.assetId = assetId
        self.breakable = breakable
        self.currentSupply = currentSupply
        self.distributorId = distributorId
        self.hardCap = hardCap
        self.id = id
        self.investmentStepSize = investmentStepSize
        self.isin = isin
        self.issuerId = issuerId
        self.minimumInvestmentAmount = minimumInvestmentAmount
        self.multisignRelease = multisignRelease
        self.nominalValue = nominalValue
        self.productType = productType
        self.projectId = projectId
        self.shortCut = shortCut
        self.singleInvestorOwnershipLimit = singleInvestorOwnershipLimit
        self.softCap = softCap
        self.tokenId = tokenId
        self.totalNumberOfTokens = totalNumberOfTokens
        self.voting = voting
    }

}
