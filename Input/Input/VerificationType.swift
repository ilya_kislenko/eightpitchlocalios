//
//  VerificationType.swift
//  Input
//
//  Created by 8pitch on 04.10.2020.
//

public enum VerificationType: String {
    case phone = "PHONE_VERIFICATION"
    case totp = "TWO_FACTOR_AUTH_VERIFICATION"
}

public enum UserVerificationType: String {
    case phone = "SMS"
    case totp = "TOTP"
}

