//
//  UserTransaction.swift
//  Input
//
//  Created by 8pitch on 1/11/21.
//

public class UserTransaction {
    public let amount: NSDecimalNumber?
    public let transactionDate: Date?
    public let shortCut: String
    public let receiptID: String?
    
    public init(amount: NSDecimalNumber?, transactionDate: String?, shortCut: String?, receiptID: String?) {
        self.amount = amount
        let formatter = DateHelper.shared.backendFormatter
        self.transactionDate = formatter.date(from: transactionDate ?? "")
        self.shortCut = shortCut ?? ""
        self.receiptID = receiptID
    }
}
