//
//  Model.swift
//  Input
//
//  Created by 8pitch on 27.07.2020.
//

import Foundation
import RxSwift
import UIKit

public protocol ModelError : LocalizedError {
    
}

public protocol InputSupport {
    
    var isValid : BehaviorSubject<Result<Bool, Error>> { get }
    var emptyProvider : BehaviorSubject<Bool> { get }
    var errorTextAlignment: NSTextAlignment { get set }
    
}

public class Model : InputSupport {
    
    public let emptyProvider: BehaviorSubject<Bool> = .init(value: false)
    public let isValid : BehaviorSubject<Result<Bool, Error>> = .init(value: .success(false))
    public var errorTextAlignment: NSTextAlignment = .center
    internal var disposables : DisposeBag = .init()
    
    public init() {
        self.setup()
    }
    
    func validate(string: String) -> Bool {
        return false
    }
    
    func setup() {}
    
    public func cleanUp() {
        self.isValid.onNext(.success(false))
    }
    
}
