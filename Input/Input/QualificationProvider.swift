//
//  QualificationProvider.swift
//  Input
//
//  Created by 8Pitch on 13.01.21.
//

import Foundation

public class QualificationProvider {
    public var files: [AttachedFile] = []
    public var description: String?
    public var qualificationType: String?
}
