//
//  ProjectInvestmentInfo.swift
//  Input
//
//  Created by 8pitch on 9/1/20.
//

public enum InvestmentStatus: String {
    case new = "NEW"
    case booked = "BOOKED"
    case confirmed = "CONFIRMED"
    case pending = "PENDING"
    case paid = "PAID"
    case canceldInProgress = "CANCELED_IN_PROGRESS"
    case canceled = "CANCELED"
    case received = "RECEIVED"
    case refundedInProgress = "REFUNDED_IN_PROGRESS"
    case refunded = "REFUNDED"
    case refundFailed = "REFUND_FAILED"
}

public class ProjectInvestmentInfo {
    public var assetBasedFees: Double?
    public var companyAddress: String?
    public var companyName: String?
    public var endTime: Date?
    public var financingPurpose: String?
    public var investmentSeries: String?
    public let investmentStepSize: Double?
    public var investorClassLimit: Double?
    public var isin: String?
    public var maximumInvestmentAmount: Double?
    public var minimumInvestmentAmount: Double?
    public var nominalValue: Double?
    public var shortCut: String?
    public var startTime: Date?
    public var typeOfSecurity: String?
    public var unprocessedInvestment: UnprocessedInvestment?

    public init(assetBasedFees: Double?,
                companyAddress: String?,
                companyName: String?,
                endTime: String?,
                financingPurpose: String?,
                investmentStepSize: Double?,
                investorClassLimit: Double?,
                investmentSeries: String?,
                isin: String?,
                maximumInvestmentAmount: Double?,
                minimumInvestmentAmount: Double?,
                nominalValue: Double?,
                shortCut: String?,
                startTime: String?,
                typeOfSecurity: String?,
                unprocessedInvestment: UnprocessedInvestment?) {
        let formatter = DateHelper.shared.backendFormatter
        self.assetBasedFees = assetBasedFees
        self.companyAddress = companyAddress
        self.companyName = companyName
        self.endTime = formatter.date(from: endTime ?? "")?.withCurrentTimeZoneOffset()
        self.financingPurpose = financingPurpose
        self.investmentSeries = investmentSeries
        self.investmentStepSize = investmentStepSize
        self.investorClassLimit = investorClassLimit
        self.isin = isin
        self.maximumInvestmentAmount = maximumInvestmentAmount
        self.minimumInvestmentAmount = minimumInvestmentAmount
        self.nominalValue = nominalValue
        self.shortCut = shortCut
        self.startTime = formatter.date(from: startTime ?? "")?.withCurrentTimeZoneOffset()
        self.typeOfSecurity = typeOfSecurity
        self.unprocessedInvestment = unprocessedInvestment
    }
}

public class UnprocessedInvestment {
    public var amount: Double?
    public var investmentId: String?
    public var investmentStatus: InvestmentStatus?

    public init(amount: Double?, investmentId: String?, investmentStatus: String?) {
        self.amount = amount
        self.investmentId = investmentId
        if let investmentStatus = investmentStatus {
            self.investmentStatus = InvestmentStatus.init(rawValue: investmentStatus)
        }
    }
}
