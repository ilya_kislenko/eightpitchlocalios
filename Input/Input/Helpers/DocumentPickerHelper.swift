//
//  DocumentPickerHelper.swift
//  Input
//
//  Created by 8pitch on 12.01.21.
//

import Foundation
import UIKit

public class DocumentPickerHelper: NSObject {
    weak var delegate: DocumentPickerPresentable?
    
    struct `Static` {
        fileprivate static var instance: DocumentPickerHelper?
    }
    
    static var shared: DocumentPickerHelper {
        if DocumentPickerHelper.Static.instance == nil {
            DocumentPickerHelper.Static.instance = DocumentPickerHelper()
        }
        return DocumentPickerHelper.Static.instance!
    }
    
    fileprivate func dispose() {
        DocumentPickerHelper.Static.instance = nil
    }
    
    func documenPicker(picker: UIDocumentPickerViewController, selectedFile file: AttachedFile?, errorMessage: String?) {
        picker.dismiss(animated: true, completion: nil)
        
        self.delegate?.selectedFile(file: file, errorMessage: errorMessage)
        self.delegate = nil
        self.dispose()
    }

}

extension DocumentPickerHelper: UIDocumentPickerDelegate {
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        self.documenPicker(picker: controller, selectedFile: nil, errorMessage: nil)
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        var file : AttachedFile
        
        guard let documentPath = urls.first else {
            return
        }
        
        if FileManager.default.fileExists(atPath: documentPath.path){
            if let cert = NSData(contentsOfFile: documentPath.path) {
                let data = cert as Data
                let path = documentPath.deletingPathExtension()
                let fileName = path.lastPathComponent
                let fileExtension = documentPath.pathExtension
                if Int64(data.count) > 5 * 1024 * 1024 {
                    self.documenPicker(picker: controller, selectedFile: nil, errorMessage: "file_attachment_error".localized)
                    return
                } else {
                    file = AttachedFile(size: data.count, fileData: data, name: fileName, fileExtension: fileExtension)
                    self.documenPicker(picker: controller, selectedFile: file, errorMessage: nil)
                    return
                }
                
            }
        }
    }
}
