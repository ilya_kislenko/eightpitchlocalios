//
//  DateHelper.swift
//  Input
//
//  Created by 8pitch on 8/26/20.
//

public class DateHelper {
    
    public static let shared = DateHelper()
    
    private init() {}
    

    public lazy var sessionFormatter: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =
            "yyyy-MM-dd'T'HH:mm"
            return dateFormatter
    }()

    public lazy var endingFormatter: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MMMM yyyy"
            return dateFormatter
    }()
    
    public lazy var uiFormatterShort: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            return dateFormatter
    }()
    
    public lazy var uiFormatterFullSeconds: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
            return dateFormatter
    }()
    
    public lazy var uiFormatterFull: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
            return dateFormatter
    }()
    
    public lazy var uiFormatterFullComma: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy, HH:mm"
            return dateFormatter
    }()
    
    public lazy var backendFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter
    }()
    
    public lazy var backendBirthFormatter: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return dateFormatter
    }()
    
    public lazy var infoFormatter: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return dateFormatter
    }()
    
    public func convertedInputString(dateString: String) -> String? {
        guard let date = self.uiFormatterShort.date(from: dateString) else { return nil }
        return self.sessionFormatter.string(from: date)
    }
    
    public func convertedBackendBirthString(dateString: String) -> String? {
        guard let date = self.backendBirthFormatter.date(from: dateString) else { return nil }
        return self.uiFormatterShort.string(from: date)
    }
    
    public func convertedBackendStringShort(dateString: String) -> String? {
        guard let date = self.backendFormatter.date(from: dateString) else { return nil }
        return self.uiFormatterShort.string(from: date)
    }
    
    public func convertedBackendStringFull(dateString: String) -> String? {
        guard let date = self.backendFormatter.date(from: dateString) else { return nil }
        return self.uiFormatterFullSeconds.string(from: date)
    }
}

extension Date {
    func withCurrentTimeZoneOffset() -> Date {
        self.addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
    }
}
