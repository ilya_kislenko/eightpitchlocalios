//
//  DocumentPickerPresentable.swift
//  Input
//
//  Created by 8pitch on 12.01.21.
//

import Foundation
import UIKit
import MobileCoreServices

public protocol DocumentPickerPresentable: class {
    func selectedFile(file: AttachedFile?, errorMessage: String?)
}

extension DocumentPickerPresentable where Self: UIViewController {
    
    public func showDocumentPicker() {
        DocumentPickerHelper.shared.delegate = self
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.item"], in: .import)
        documentPicker.delegate = DocumentPickerHelper.shared as UIDocumentPickerDelegate & UINavigationControllerDelegate
        documentPicker.navigationController?.navigationBar.backgroundColor = .black
        self.present(documentPicker, animated: true)
    }

}

extension DocumentPickerHelper: UINavigationControllerDelegate {
    
}
