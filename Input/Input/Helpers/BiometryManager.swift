//
//  BiometryManager.swift
//  Input
//
//  Created by 8pitch on 11/18/20.
//

import Foundation
import LocalAuthentication

public class BiometryManager {
    
    static public let shared = BiometryManager()
    public var externalID: String? {
        get {
            UserDefaults.standard.string(forKey: Constants.currentExternal)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.currentExternal)
        }
    }
    
    private init() {}
    
    private let context = LAContext()
    
    private var storage: UserDefaults {
        UserDefaults(suiteName: self.externalID) ?? UserDefaults.standard
    }
    
    private var biometryEnablingReason: String {
        var reason: String
        switch self.biometryType {
        case .touchID:
            reason = "login.setupQuick.touchID.subtitle".localized
        case .faceID:
            reason = "login.setupQuick.faceID.subtitle".localized
        case .none:
            reason = ""
        @unknown default:
            reason = ""
        }
        return reason
    }
    
    private var biometryText: String {
        var reason: String
        switch self.biometryType {
        case .touchID:
            reason = "login.biometry.touchID".localized
        case .faceID:
            reason = "login.biometry.faceID".localized
        case .none:
            reason = ""
        @unknown default:
            reason = ""
        }
        return reason
    }
    
    public var biometryType: LABiometryType {
        isBiometryEnrolled ? context.biometryType : .none
    }
    
    public var isQuickLoginSet: Bool {
        self.isPINEnabled && !(self.accessToken?.isEmpty ?? true)
    }
    
    public var isBiometryEnrolled: Bool {
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    }
    
    public var isBiometryEnabled: Bool {
        get {
            storage.bool(forKey: Constants.biometryFlagName) && self.isBiometryEnrolled && self.isPINEnabled
        }
        set {
            storage.set(newValue, forKey: Constants.biometryFlagName)
        }
    }
    
    public var pin: String? {
        get {
            self.storage.string(forKey: Constants.pinFlagName)
        }
        set {
            self.storage.setValue(newValue, forKey: Constants.pinFlagName)
        }
    }
    
    public var accessToken: String? {
        get {
            self.storage.string(forKey: Constants.accessToken)
        }
        set {
            self.storage.setValue(newValue, forKey: Constants.accessToken)
        }
    }
    
    public var refreshToken: String? {
        get {
            self.storage.string(forKey: Constants.refreshToken)
        }
        set {
            self.storage.setValue(newValue, forKey: Constants.refreshToken)
        }
    }
    
    public var isPINEnabled: Bool {
        (self.pin != nil) && (self.pin?.count == 4)
    }
    
    public func enableBiometry(completion: @escaping ErrorClosure) {
        context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: self.biometryEnablingReason, reply: { [weak self] enabled, error in
            if enabled {
                self?.isBiometryEnabled = enabled
                completion(nil)
            } else {
                completion(error)
            }
        })
    }
    
    public func checkBiometry(completion: @escaping ErrorClosure) {
        context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: self.biometryText, reply: { success, error in
            DispatchQueue.main.async {
                if success {
                    completion(nil)
                } else {
                    completion(error)
                }
            }
        })
    }
    
    enum Constants {
        static let biometryFlagName = "isBiometryEnabled"
        static let pinFlagName = "quickPinLogin"
        static let accessToken = "accessToken"
        static let refreshToken = "refreshToken"
        static let currentExternal = "currentExternal"
    }
    
}
