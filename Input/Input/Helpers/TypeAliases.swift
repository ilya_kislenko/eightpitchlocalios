//
//  TypeAliases.swift
//  Input
//
//  Created by 8pitch on 8/27/20.
//

import Foundation
import RxSwift

public typealias ProjectsHandler = ([Project?], Error?) -> Void
public typealias ProjectHandler = (ExpandedProject?, Error?) -> Void
public typealias NotificationsHandler = ([ProjectNotification?], Error?) -> Void
public typealias ErrorClosure = (Error?) -> Void
public typealias VoidClosure = () -> Void
public typealias StringClosure = (String) -> Void
public typealias ResponseClosure = (Result<Session, Error>) -> Void
public typealias ResponseDataClosure = (Result<Data, Error>) -> Void
public typealias ResponseLinkClosure = (Result<URL, Error>) -> Void
public typealias BookHandler = (String?, Error?) -> Void
public typealias SessionFinishedHandler = (URL?, URL) -> Void
public typealias ProgressHandler = (Int64, Int64) -> Void
// todo: use struct instead of typle
public typealias InvestmentSum = (inToken: NSDecimalNumber, inEUR: NSDecimalNumber)
public typealias URLHandler = (URL?, Error?) -> Void
public typealias BoolHandler = (Bool) -> Void
public typealias TransactionHandler = (UserTransaction) -> Void
public typealias DownloadFileHandler = (Error?, URL?) -> Void
