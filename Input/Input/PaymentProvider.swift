//
//  PaymentProvider.swift
//  Input
//
//  Created by 8pitch on 9/8/20.
//

public enum PaymentMethodType {
    case onlineUberweisen
    case secupayDirectDebit
    case classicBankTransfer
    case klarna
}

public class PaymentProvider {
    public var onlinePaymentSubmitInfo: OnlinePaymentSubmitInfo?
    public var bankTransferPaymentInfo: BankTransferPaymentInfo?
    public var bankAccount: BankAccount?
    public var selectedPaymentMethodType: PaymentMethodType?
}
