//
//  Questionnaire.swift
//  Input
//
//  Created by 8pitch on 8/7/20.
//

import Foundation
import RxSwift

public enum QuestionnaireStep {
    case assetExperience
    case assetClasses
    case investmentExperience
    case investmentDuration
    case investmentAmount
    case investingFrequency
    case virtualCurrency
    case virtualCurrencyShare
    case capitalMarketExperience
    case howDidGetExperience
}

public enum QuestionnaireError : ModelError {

    case questionnaireResponceInvalid

    public var errorDescription : String? {
        //TODO: update errro message
        return "QuestionnaireError"
    }
}

public enum AssetExperience: String {
    case TRUE = "TRUE"
    case notProvided = "NOT_PROVIDED"

}

public enum ExperienceDuration: String {
    case lessThanOne = "LESS_THAN_1"
    case fromOneToThree = "FROM_1_TO_3"
    case moreThanThree = "MORE_THAN_3"
}


public enum InvestmentAmount: String {
    case lessThanFive = "LESS_THAN_5K"
    case fromFiveToTwentyfive = "FROM_5K_TO_25K"
    case moreThanTwentyfive = "MORE_THAN_25K"
}

public enum InvestingFrequency: String {
    case lessThanFive = "LESS_THAN_5"
    case moreThanFive = "MORE_THAN_5"
}

public enum VirtualCurrencyShare: String {
    case lessThanFifty = "LESS_THAN_50"
    case moreThanFifty = "MORE_THAN_50"
}

public enum ProfessionalExperienceSource: String {
    case profession = "PROFESSION"
    case studies = "STUDIES"
    case professionStudies = "PROFESSION_STUDIES"
}


public class Questionnaire : Model {
    public var currentStep: QuestionnaireStep = .assetExperience
    public var assetExperienceProvider : String?
    public var assetClassesProvider : BehaviorSubject<AssetClasses?> = .init(value: AssetClasses())
    public var investingExperienceProvider : Bool?
    public var experienceDurationProvider : String?
    public var investmentAmountProvider : String?
    public var investingFrequencyProvider : String?
    public var virtualCurrencyProvider : Bool?
    public var virtualCurrencyShareProvider : String?
    public var capitalMarketExperienceProvider : Bool?
    public var howDidGetExperienceProvider : String?

    override func validate(string: String) -> Bool {
        return true
    }

    override func setup() {
        self.isValid.onNext(.success(true))
    }

    override public func cleanUp() {
        self.isValid.onNext(.success(true))
    }
}
