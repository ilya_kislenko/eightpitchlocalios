//
//  NameAndLastName.swift
//  Input
//
//  Created by 8pitch on 20.07.2020.
//

import Foundation
import RxSwift

public enum Title : String, CaseIterable {
    
    case mr = "title.Mr"
    case ms = "title.Ms"
    case mrDr = "title.Mr.Dr."
    case msDr = "title.Ms.Dr."
    case mrProfDr = "title.Mr.Prof.Dr."
    case mrsProfDr = "title.Mrs.Prof.Dr."
    
    public init(rawValue: String) {
        self = Self.localized[rawValue] ?? .mr
    }
    
    public init(rawTitle: String?) {
        switch rawTitle {
        case "MR": self = .mr
        case "MR_DR": self = .mrDr
        case "MR_PROF_DR": self = .mrProfDr
        case "MS_PROF_DR": self = .mrsProfDr
        case "MS": self = .ms
        case "MS_DR": self = .msDr
        default: self = .mr
        }
    }
    
    private static var localized : [String : Self] {
        let values : [Self] = [ .mr, .ms, .mrDr, .msDr, .mrProfDr, .mrsProfDr ]
        return values.reduce(into: [String : Self]()) { $0[$1.rawValue.localized] = $1 }
    }
    
    public static func rawTitle(from type: Title?) -> String {
        switch type {
        case .mr?: return "MR"
        case .mrDr?: return "MR_DR"
        case .mrProfDr?: return "MR_PROF_DR"
        case .mrsProfDr?: return "MS_PROF_DR"
        case .ms?: return "MS"
        case .msDr?: return "MS_DR"
        default: return "MR"
        }
    }
    
}

public enum FirstNameError : ModelError {
    
    case firstNameInvalid
    case firstNameFormatInvalid
    case firstNamePatternInvalid
    case titleInvalid
    
    public var errorDescription : String? {
        switch self {
        case .firstNameInvalid:
            return "input.error.empty".localized
        case .firstNameFormatInvalid:
            return "input.error.name.firstname".localized + " " +
                "input.error.format.name".localized
        case .firstNamePatternInvalid:
            return "input.error.name.firstname".localized + " " +
                "input.error.pattern.name".localized
        case .titleInvalid:
            return "input.error.empty".localized
        }
    }
}

public enum TitleError : ModelError {
    
    case titleInvalid
    
    public var errorDescription : String? {
        switch self {
        case .titleInvalid:
            return "input.error.empty".localized
        }
    }
}

public enum LastNameError : ModelError {
    
    case lastNameInvalid
    case lastNameFormatInvalid
    case lastNamePatternInvalid
    
    public var errorDescription : String? {
        switch self {
        case .lastNameInvalid:
            return "input.error.empty".localized
        case .lastNameFormatInvalid:
            return "input.error.name.lastname".localized + " " +
                "input.error.format.name".localized
        case .lastNamePatternInvalid:
            return "input.error.name.lastname".localized + " " +
                "input.error.pattern.name".localized
        }
    }
    
}

public class NameAndLastName : Model {
    
    public let titleProvider : PublishSubject<String?> = .init()
    public let firstNameProvider : BehaviorSubject<String?> = .init(value: nil)
    public let lastNameProvider : BehaviorSubject<String?> = .init(value: nil)
    public let fullNameProvider : BehaviorSubject<String?> = .init(value: nil)
    
    public var title : Title?
    var firstName : String = ""
    var lastName : String = ""
    
    public var valueProviders : [ KeyPath<NameAndLastName, PublishSubject<String?>> : (() -> Void)? ] = [:]
    
    public init(user: User? = nil) {
        super.init()
        self.firstNameProvider.onNext(try? user?.firstNameProvider.value())
        self.lastNameProvider.onNext(try? user?.lastNameProvider.value())
        if let title = (try? user?.prefixProvider.value()) {
            self.title = Title(rawValue: title)
        }
    }
    
    override func setup() {
        self.titleProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                if $0.isEmpty {
                    self?.errorTextAlignment = .left
                    self?.isValid.onNext(.failure(FirstNameError.titleInvalid))
                } else {
                    self?.isValid.onNext(.success(false))
                }
                self?.title = Title(rawValue: $0)
            }).disposed(by: self.disposables)
        
        self.lastNameProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                if case .failure(let reason) = self?.validated(string: $0) {
                    self?.errorTextAlignment = .center
                    switch reason {
                    case .empty: self?.isValid.onNext(.failure(LastNameError.lastNameInvalid))
                    case .format: self?.isValid.onNext(.failure(LastNameError.lastNameFormatInvalid))
                    case .pattern: self?.isValid.onNext(.failure(LastNameError.lastNamePatternInvalid))
                    }
                } else {
                    self?.isValid.onNext(.success(false))
                }
                self?.lastName = $0
                // to do: add a property full name
                self?.fullNameProvider.onNext("\(self?.firstName ?? "") \(self?.lastName ?? "")")
            }).disposed(by: self.disposables)
        self.firstNameProvider
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                if case .failure(let reason) = self?.validated(string: $0) {
                    self?.errorTextAlignment = .right
                    switch reason {
                    case .empty: self?.isValid.onNext(.failure(FirstNameError.firstNameInvalid))
                    case .format: self?.isValid.onNext(.failure(FirstNameError.firstNameFormatInvalid))
                    case .pattern: self?.isValid.onNext(.failure(FirstNameError.firstNamePatternInvalid))
                    }
                } else {
                    self?.isValid.onNext(.success(false))
                }
                self?.firstName = $0
                self?.fullNameProvider.onNext("\(self?.firstName ?? "") \(self?.lastName ?? "")")
            }).disposed(by: self.disposables)
    }
    
    private func validated(string: String) -> Result<Bool, ValidatorError> {
        let validator = Validator()
        return validator.validate(string: string, type: .name)
    }
    
    public override func cleanUp() {
        super.cleanUp()
        self.titleProvider.onNext(nil)
        self.firstNameProvider.onNext(nil)
        self.lastNameProvider.onNext(nil)
        self.title = nil
        self.firstName = ""
        self.lastName = ""
        self.valueProviders = [:]
        self.setup()
    }
    
    public func validate() {
        let isTitleValid = self.title != nil
        if !isTitleValid {
            self.errorTextAlignment = .left
            self.isValid.onNext(.failure(FirstNameError.titleInvalid))
            return
        }
        let isFirstNameValid = self.validated(string: self.firstName) == .success(true)
        if !isFirstNameValid {
            if case .failure(let reason) = self.validated(string: self.firstName) {
                self.errorTextAlignment = .right
                switch reason {
                case .empty: self.isValid.onNext(.failure(FirstNameError.firstNameInvalid))
                case .format: self.isValid.onNext(.failure(FirstNameError.firstNameFormatInvalid))
                case .pattern: self.isValid.onNext(.failure(FirstNameError.firstNamePatternInvalid))
                }
            }
            return
        }
        let isLastNameValid = self.validated(string: self.lastName) == .success(true)
        if !isLastNameValid {
            if case .failure(let reason) = self.validated(string: self.lastName) {
                self.errorTextAlignment = .center
                switch reason {
                case .empty: self.isValid.onNext(.failure(LastNameError.lastNameInvalid))
                case .format: self.isValid.onNext(.failure(LastNameError.lastNameFormatInvalid))
                case .pattern: self.isValid.onNext(.failure(LastNameError.lastNamePatternInvalid))
                }
                return
            }
        }
        if isTitleValid && isFirstNameValid && isLastNameValid {
            self.isValid.onNext(.success(true))
        }
    }
    
}
