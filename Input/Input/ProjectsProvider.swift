//
//  ProjectsProvider.swift
//  Input
//
//  Created by 8pitch on 9/2/20.
//

public class ProjectsProvider {
    
    public var currentInvestmentID: String? {
        projectInvestmentInfo?.unprocessedInvestment?.investmentId ?? investmentId
    }
    
    public var currentInvestmentSum: InvestmentSum? {
        if let amount = self.projectInvestmentInfo?.unprocessedInvestment?.amount {
            let decimalNumberAmount = NSDecimalNumber(floatLiteral: amount)
            return (inToken: decimalNumberAmount, inEUR: decimalNumberAmount.multiplying(by: self.investmentAmountValues.tokenNominalValue))
        }
        return self.currentInvestmentAmount
    }
    
    public var investmentAmountValues: InvestmentAmountValues {
        let max = NSDecimalNumber(floatLiteral: self.projectInvestmentInfo?.maximumInvestmentAmount ?? 0)
        let min = NSDecimalNumber(floatLiteral: self.expandedProject?.tokenParametersDocument?.minimumInvestmentAmount ?? 0)
        let step = NSDecimalNumber(floatLiteral: self.expandedProject?.tokenParametersDocument?.investmentStepSize ?? 0)
        let nominal = NSDecimalNumber(floatLiteral: self.expandedProject?.tokenParametersDocument?.nominalValue ?? 0)
        let feeRate = NSDecimalNumber(floatLiteral: self.expandedProject?.tokenParametersDocument?.assetBasedFees ?? 0)
        let investorClassLimit = self.projectInvestmentInfo?.investorClassLimit == nil ?
            nil : NSDecimalNumber(floatLiteral: self.projectInvestmentInfo?.investorClassLimit ?? 0)
        return InvestmentAmountValues(max: max, min: min, step: step , nominal: nominal , feeRate: feeRate, tokensAmount: self.expandedProject?.tokenParametersDocument?.totalNumberOfTokens, shortCut: self.expandedProject?.tokenParametersDocument?.shortCut ?? "", investorClassLimit: investorClassLimit)
    }
    
    public var projects: [Project] = []
    public var currentProjects: [Project] = []
    public var completedProjects: [Project] = []
    public var investedProjects: [Project] = []
    public var currentProject: Project?
    public var expandedProject: ExpandedProject?
    public var investmentId: String?
    public var projectInvestmentInfo: ProjectInvestmentInfo?
    public var currentInvestmentAmount: InvestmentSum?
    public var bankTransferPaymentInfo: BankTransferPaymentInfo?

    public var investorsProjects: [InvestorsProject] = []
    public var investorsPaymentsGraph: InvestorsPaymentsGraph?
    public var initiatorsProjects: [InitiatorsProject] = []
    public var currentInitiatorsProject: InitiatorsProject?
    public var sliderContent: [SliderContent] = []
}
