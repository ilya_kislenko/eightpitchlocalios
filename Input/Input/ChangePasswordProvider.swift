//
//  ChangePassword.swift
//  Input
//
//  Created by 8pitch on 10/9/20.
//

import Foundation

public class ChangePasswordProvider: Model {
    
    public var password: Password = Password()
    public var newPassword: Password = Password()
    
}
