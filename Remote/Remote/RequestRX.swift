//
//  Request.swift
//  Remote
//
//  Created by 8pitch on 26.07.2020.
//

import Foundation
import RxSwift
import Input

public protocol RequestError : LocalizedError {
    
}

public enum GenericRequestError : RequestError {
    
    case invalidResponse(Error?)
    case noConnection
    case accessTokenOutdated
    
    public var errorDescription : String? {
        switch self {
            // TODO: process actual error from back end, encorporate into localized string if needed
            case .accessTokenOutdated: fallthrough
            case .invalidResponse(_):
                return "remote.error.response.invalid".localized
        case .noConnection:
            return "remote.error.noConnection".localized
        }
    }
    
}

class RequestRx {
    
    var disposables : DisposeBag = .init()
    var session : Session
    var remote = Remote()
    
    var path : String {
        fatalError("this needs to be overridden!")
    }
    
    var config : APIConfig.Type {
        return BGHAPIConfig.self
    }
    
    var url : URL {
        var root = self.config.root
        root.path += self.path
        return root.url!
    }
    
    var isRefreshTokenRequest : Bool {
        false
    }
    
    var method : String {
        fatalError("this needs to be overridden!")
    }
    
    var body : Data? { nil }
    
    var headers : [ String : String ]? { nil }
    
    private var QABasicAuth: String {
        Data("mobile:password".utf8).base64EncodedString()
    }
    
    private var demoBasicAuth: String {
        Data("SMS-dev-user:11BoeL_1o7t!iH>Tl_nfxhNA=D".utf8).base64EncodedString()
    }
    
    private var prodBasicAuth: String {
        Data("SMS-prod-user:hFsq-G=TUqGShB!Z@IPghNzgN".utf8).base64EncodedString()
    }
    
    var authHeader: String {
        switch Enviroment.current {
        case .qa:
            return self.QABasicAuth
        case .demo:
            return self.demoBasicAuth
        case .dev, .perf:
            return self.QABasicAuth
        case .prod:
            return self.prodBasicAuth
        }
    }
    
    public let response : BehaviorSubject<Result<Session, Error>?> = .init(value: nil)
    
    init(session: Session) {
        self.session = session
    }
    
    func parse(_ data: Data) throws {
        // no-op
    }
    
    func send() {
        var request = URLRequest(url: self.url)
        request.httpMethod = self.method
        request.httpBody = self.body
        request.addValue("iOS", forHTTPHeaderField: "User-Agent")
        for header in self.headers ?? [:] {
            request.addValue(header.value, forHTTPHeaderField: header.key)
        }
        URLSession.shared.rx.response(request: request)
            .subscribe(onNext: { [weak self] (response: HTTPURLResponse, data: Data) in
                self?.processResponse(data: data, response: response)
            }, onError: { [weak self] in
                self?.processResponse(error: $0)
            }).disposed(by: self.disposables)
    }
    
    func processResponse(data: Data? = nil, response: HTTPURLResponse? = nil, error: Error? = nil) {
        guard error == nil else {
            let errorCode = (error as NSError?)?.code
            self.response.onNext(.failure(errorCode ?? 0 < 0 ?
                                            GenericRequestError.noConnection :
                                            GenericRequestError.invalidResponse(error)))
            return
        }
        do {
            try self.processResponse(response: response)
            if let data = data {
                try self.parse(data)
            } else {
                throw GenericRequestError.invalidResponse(nil)
            }
            self.response.onNext(.success(self.session))
        } catch GenericRequestError.accessTokenOutdated {
            // no-op, we are trying to refresh the accessToken
            return
        } catch let error where error is RequestError {
            self.response.onNext(.failure(error))
        } catch {
            self.response.onNext(.failure(GenericRequestError.invalidResponse(error)))
            return
        }
    }
    
    func processResponse(response: HTTPURLResponse?) throws {
        if response?.statusCode == 401 && !self.isRefreshTokenRequest {
            self.refreshToken()
            throw GenericRequestError.accessTokenOutdated
        }
    }
    
    func saveTokens() {
        BiometryManager.shared.accessToken = self.session.accessToken
        BiometryManager.shared.refreshToken = self.session.refreshToken
    }
    
}

extension RequestRx {
    
    private func refreshToken() {
        self.remote.refreshToken(self.session)
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                switch $0 {
                    case .success(_):
                        self?.send()
                    case .failure(_):
                        // to trigger the view controller to attempt manually refresh token.
                        // if it fails, controller is responsible of showing an error message
                        // that the log in is necessary, and for presenting the login screen
                        self?.session.accessTokenProvider.onNext(nil)
                        self?.saveTokens()
                        self?.response.onNext(.failure(GenericRequestError.accessTokenOutdated))
                }
            }).disposed(by: self.disposables)
    }
    
}
