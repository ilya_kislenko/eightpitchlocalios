//
//  ApproveVotingRequest.swift
//  Remote
//
//  Created by 8pitch on 05.03.2021.
//

import Input

struct ApproveVotingResponse: Codable {
    var id: Int
    var smsConfirmationResult: String
    
    var result: ValidationResult? {
        ValidationResult(rawValue: smsConfirmationResult)
    }
}

class ApproveVotingRequest: Request {
    
    private var id: Int
    
    init(session: Session, id: Int) {
        self.id = id
        super.init(session: session)
    }
    
    override var path : String {
        "/voting-service/voting/\(self.id)/approve"
    }
    
    override var method: String {
        "PUT"
    }
    
    override var body: Data? {
        let body = VerifyInvestmentTokenRequestBody(interactionID: self.session.interactionID, token: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(VerifyEmailTokenResponse.self, from: data)
        switch ValidationResult(rawValue: parsed.result) ?? .invalid {
        case .expired:
            throw VerifyTokenError.expired
        case .invalid:
            throw VerifyTokenError.invalidCode
        case .valid:
            break
        }
    }
}

