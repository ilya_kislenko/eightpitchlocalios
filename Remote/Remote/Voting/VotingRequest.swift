//
//  VotingRequest.swift
//  Remote
//
//  Created by 8pitch on 05.03.2021.
//

import Input

public enum VotingRequestError : RequestError {
    case prohibited
}

class VotingRequest: Request {
    
    private var id: Int
    
    init(session: Session, id: Int) {
        self.id = id
        super.init(session: session)
    }
    
    override var path : String {
        "/voting-service/voting/\(self.id)"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func processResponse(response: HTTPURLResponse?) throws {
        try super.processResponse(response: response)
        if response?.statusCode == 403 || response?.statusCode == 404 {
            throw VotingRequestError.prohibited
        }
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(VotingResponse.self, from: data)
        self.session.votingProvider.currentVoting = Input.Voting(response: parsed)
    }
}
