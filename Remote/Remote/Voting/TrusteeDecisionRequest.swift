//
//  TrusteeDecision.swift
//  Remote
//
//  Created by 8pitch on 4/5/21.
//

import Input

struct TrusteeDecisionRequestBody: Codable {
    var answers: [VotingAnswer]
    var decisionType: String
    var smsConfirmation: VerifyInvestmentTokenRequestBody
}

class TrusteeDecisionRequest: Request {
    
    private var id: Int
    
    init(session: Session, id: Int) {
        self.id = id
        super.init(session: session)
    }
    
    override var path : String {
        "/voting-service/voting/\(self.id)/trustee-decision"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        var answers: [VotingAnswer] = []
        let type = self.session.votingProvider.trusteeDecision
        switch type {
        case .voteByHimself,
             .withoutObligation,
             .none:
            answers = []
        case .withObligation:
            self.session.votingProvider.currentVoting?.questions.forEach {
                let choosedOptionsIDs: [Int] = $0.options.compactMap { option in
                    option.selected ? option.id : nil
                }
                let answer = VotingAnswer(answersIds: choosedOptionsIDs, questionId: $0.id)
                answers.append(answer)
            }
        }
        let confirmation = VerifyInvestmentTokenRequestBody(interactionID: self.session.interactionID, token: self.session.code)
        let body = TrusteeDecisionRequestBody(answers: answers,
                                              decisionType: type?.rawValue ?? "VOTE_BY_HIMSELF",
                                              smsConfirmation: confirmation)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(VerifyEmailTokenResponse.self, from: data)
        switch ValidationResult(rawValue: parsed.result) ?? .invalid {
        case .expired:
            throw VerifyTokenError.expired
        case .invalid:
            throw VerifyTokenError.invalidCode
        case .valid:
            break
        }
    }
}

