//
//  ResultPDFRequest.swift
//  Remote
//
//  Created by 8pitch on 05.03.2021.
//

import Foundation
import Input

class ResultPDFRequest : Request {
    
    private var id: Int
    private var completionHandler: ResponseDataClosure
    override var path : String {
        "/voting-service/voting/\(self.id)/result-pdf"
    }
    
    override var method: String {
        "GET"
    }
    
    init(session: Session, id: Int, completionHandler: @escaping ResponseDataClosure) {
        self.id = id
        self.completionHandler = completionHandler
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        DispatchQueue.main.async {
            self.completionHandler(.success(data))
        }
    }
}
