//
//  RejectVotingRequest.swift
//  Remote
//
//  Created by 8pitch on 05.03.2021.
//

import Input

struct RejectVotingResponse: Codable {
    var id: Int
    var smsConfirmationResult: String
    
    var result: ValidationResult? {
        ValidationResult(rawValue: smsConfirmationResult)
    }
}

class RejectVotingRequest: Request {
    
    private var id: Int
    
    init(session: Session, id: Int) {
        self.id = id
        super.init(session: session)
    }
    
    override var path : String {
        "/voting-service/voting/\(self.id)"
    }
    
    override var method: String {
        "DELETE"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
}
