//
//  JoinMeetingRequest.swift
//  Remote
//
//  Created by 8pitch on 07.04.2021.
//

import Input

struct JoinMeetingResponse: Codable {
    var meetingNumber: Int
    var meetingPassword: String
    var signature: String
}

class JoinMeetingRequest: Request {
    
    private var id: Int
    
    init(session: Session, id: Int) {
        self.id = id
        super.init(session: session)
    }
    
    override var path : String {
        "/voting-service/voting/\(self.id)/join-meeting-inv"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(JoinMeetingResponse.self, from: data)
        self.session.votingProvider.streamCreds = StreamCreds(meetingNumber: parsed.meetingNumber, meetingPassword: parsed.meetingPassword, signature: parsed.signature)
    }
}

