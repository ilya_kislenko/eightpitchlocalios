//
//  VotingResultRequest.swift
//  Remote
//
//  Created by 8pitch on 05.03.2021.
//

import Input

struct Percent: Codable {
    var investors: Double
    var tokens: Double
}

struct VotingResultResponse: Codable {
    var coveragePerInvestors: Double
    var coveragePerTokens: Double
    var questions: [Int: [Int: Percent]]
}

class VotingResultRequest: Request {
    
    private var id: Int
    
    init(session: Session, id: Int) {
        self.id = id
        super.init(session: session)
    }
    
    override var path : String {
        "/voting-service/voting/\(self.id)/result"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(VotingResultResponse.self, from: data)
        self.session.votingProvider.votingResult = Input.VotingResult(response: parsed)
    }
}

