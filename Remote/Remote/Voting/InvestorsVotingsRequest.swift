//
//  InvestorsVotingsRequest.swift
//  Remote
//
//  Created by 8pitch on 05.03.2021.
//

import Input

class InvestorsVotingsRequest: Request {
    
    override var path : String {
        "/voting-service/voting/investor"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode([VotingResponse].self, from: data)
        self.session.votingProvider.votings = parsed.compactMap { Input.Voting(response: $0) }
    }
}


