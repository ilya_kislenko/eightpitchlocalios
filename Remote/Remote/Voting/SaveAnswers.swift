//
//  SaveAnswers.swift
//  Remote
//
//  Created by 8pitch on 05.03.2021.
//

import Input

struct VotingAnswer: Codable {
    var answersIds: [Int]
    var questionId: Int
}

struct SaveAnswersRequestBody: Codable {
    var confirmationCodeRequest: VerifyInvestmentTokenRequestBody
    var votingAnswers: [VotingAnswer]
}

class SaveAnswersRequest: Request {
    
    private var id: Int
    
    init(session: Session, id: Int) {
        self.id = id
        super.init(session: session)
    }
    
    override var path : String {
        "/voting-service/voting/\(self.id)/answers"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        var answers: [VotingAnswer] = []
        self.session.votingProvider.currentVoting?.questions.forEach {
            let choosedOptionsIDs: [Int] = $0.options.compactMap { option in
                option.selected ? option.id : nil
            }
            let answer = VotingAnswer(answersIds: choosedOptionsIDs, questionId: $0.id)
            answers.append(answer)
        }
        let body = SaveAnswersRequestBody(confirmationCodeRequest: VerifyInvestmentTokenRequestBody(interactionID: self.session.interactionID, token: self.session.code),
                                          votingAnswers: answers)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(VerifyEmailTokenResponse.self, from: data)
        switch ValidationResult(rawValue: parsed.result) ?? .invalid {
        case .expired:
            throw VerifyTokenError.expired
        case .invalid:
            throw VerifyTokenError.invalidCode
        case .valid:
            break
        }
    }
}
