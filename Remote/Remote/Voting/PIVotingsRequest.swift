//
//  VotingsRequest.swift
//  Remote
//
//  Created by 8pitch on 05.03.2021.
//

import Input

struct Question: Codable {
    var id: Int
    var maximumAnswers: Int?
    var minimumAnswers: Int?
    var options: [Option?]
    var title: String?
    var type: String?
}

struct Option: Codable {
    var answer: String?
    var id: Int
}

struct SmsConfirmation: Codable {
    var interactionId: String?
    var token: String?
}

struct Trustee: Codable {
    var firstName: String
    var lastName: String
    var prefix: String
}

struct StreamInfo: Codable {
    var streamAgenda: String?
    var streamEndDate: String?
    var streamStartDate: String?
    var streamTheme: String?
}

struct ProjectInfo: Codable {
    var companyName: String
    var legalFormType: String
    var projectId: Int
}

struct VotingResponse: Codable {
    var agenda: String?
    var createDateTime: String?
    var createdBy: String?
    var description: String?
    var endDate: String?
    var id: Int?
    var informationCollectionPeriodEndDate: String?
    var informationCollectionPeriodStartDate: String?
    var investorTrusteeDecision: String?
    var projectInfo: ProjectInfo
    var questions: [Question?]
    var sendResultsToParticipants: Bool?
    var shareResultsWithTrustee: Bool?
    var smsConfirmation: SmsConfirmation?
    var startDate: String?
    var status: String?
    var streamInfo: StreamInfo?
    var trustee: Trustee?
    var trusteeEnabled: Bool?
    var trusteeExternalId: String?
    var type: String?
    var userVotingDate: String?
    var usersVotingStatus: String?
}

class PIVotingsRequest: Request {
    
    override var path : String {
        "/voting-service/voting"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode([VotingResponse].self, from: data)
        self.session.votingProvider.votings = parsed.compactMap { Input.Voting(response: $0) }
    }
}

