//
//  AnswersRequest.swift
//  Remote
//
//  Created by 8pitch on 05.03.2021.
//

import Input

struct AnswerResponse: Codable {
    var answers: [Option]
    var question: Question
}

class AnswersRequest: Request {
    
    private var id: Int
    
    init(session: Session, id: Int) {
        self.id = id
        super.init(session: session)
    }
    
    override var path : String {
        "/voting-service/voting/\(self.id)/answers"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode([AnswerResponse].self, from: data)
        self.session.votingProvider.answers = parsed.compactMap { Input.Answer(response: $0) }
    }
}

