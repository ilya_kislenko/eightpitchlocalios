//
//  RefreshTokenRequest.swift
//  Remote
//
//  Created by 8pitch on 29.07.2020.
//

import Foundation
import Input
import RxSwift
import RxCocoa

struct RefreshTokenRequestBody : Codable {
    
    var refreshToken : String
    
}

class RefreshTokenRequest : RequestRx {
    
    override var path : String {
        "/user-service/refresh-token"
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json" ]
    }
    
    override var isRefreshTokenRequest: Bool {
        true
    }
    
    override var body: Data? {
        let body = RefreshTokenRequestBody(refreshToken: self.session.refreshToken)
        return try? JSONEncoder().encode(body)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(Token.self, from: data)
        self.session.refreshTokenProvider.onNext(parsed.refreshToken)
        self.session.accessTokenProvider.onNext(parsed.accessToken)
        self.saveTokens()
    }
    
}
