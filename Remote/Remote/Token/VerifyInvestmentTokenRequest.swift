//
//  VerifyInvestmentTokenRequest.swift
//  Remote
//
//  Created by 8pitch on 9/4/20.
//

import Foundation
import RxSwift
import RxCocoa
import Input

enum VerifyInvestmentStatus: String {
    case success = "SUCCESS"
    case waiting = "WAITING"
    case invalid = "INVALID_CODE"
    case canceled = "CANCELED"
    case expired = "EXPIRED_CODE"
}

struct VerifyInvestmentTokenRequestBody : Codable {
    enum CodingKeys : String, CodingKey {
        case interactionID = "interactionId"
        case token = "token"
    }
    
    var interactionID : String
    var token : String
}

struct VerifyInvestmentTokenResponse : Codable {
    var status : String
}

class VerifyInvestmentTokenRequest : RequestRx {
    
    private let projectID: Int
    private let investmentID: String
    
    override var path : String {
        "/project-service/investments/\(self.projectID)/confirm-investment/\(self.investmentID)"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override var body: Data? {
        let body = VerifyInvestmentTokenRequestBody(interactionID: self.session.interactionID, token: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override var method: String {
        "PUT"
    }
    
    override init(session: Session) {
        self.projectID = session.projectsProvider.currentProject?.identifier ?? 0
        self.investmentID = session.projectsProvider.currentInvestmentID ?? ""
        super.init(session: session)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(VerifyInvestmentTokenResponse.self, from: data)
        let status = VerifyInvestmentStatus(rawValue: parsed.status)
        switch status {
        case .invalid:
            throw VerifyTokenError.invalidCode
        case .waiting:
            throw VerifyTokenError.waiting
        case .expired:
            throw VerifyTokenError.expired
        case .canceled:
            throw VerifyTokenError.canceled
        default:
            break
        }
    }
    
}

