//
//  VerifyTokenRequest.swift
//  Remote
//
//  Created by 8pitch on 27.07.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

public enum VerifyTokenError : RequestError {
    
    case invalidCode
    case waiting
    case canceled
    case expired
    
    public var errorDescription : String? {
        switch self {
        case .invalidCode:
            return "remote.error.response.code.invalid".localized
        case .waiting:
            return "remote.error.response.code.waiting".localized
        case .canceled:
            return "remote.error.response.code.canceled".localized
        case .expired:
            return "remote.error.response.code.expired".localized
        }
    }
    
}

struct VerifyTokenRequestBody : Codable {
    
    enum CodingKeys : String, CodingKey {
        case interactionID = "interactionId"
        case token = "token"
    }
    
    var interactionID : String
    var token : String
    
}

struct VerifyTokenResponse : Codable {
    
    var tokenValidationResult: ValidationResult {
        ValidationResult(rawValue: self.result) ?? .invalid
    }
    
    var isValid: Bool {
        tokenValidationResult == .valid
    }
    
    var result : String
    
    enum CodingKeys : String, CodingKey {
        
        case result = "result"
        
    }
    
}

class VerifyTokenRequest : RequestRx {
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json" ]
    }
    
    override var path : String {
        "/user-service/registration/phones/\(self.session.phone)/verify-token"
    }
    
    override var body: Data? {
        let body = VerifyTokenRequestBody(interactionID: self.session.interactionID, token: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override var method: String {
        "POST"
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(VerifyTokenResponse.self, from: data)
        switch parsed.tokenValidationResult {
        case .expired:
            throw VerifyTokenError.expired
        case .invalid:
            throw VerifyTokenError.invalidCode
        case .valid:
            break
        }
    }
    
}
