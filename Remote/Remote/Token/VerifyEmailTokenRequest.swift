//
//  VerifyEmailTokenRequest.swift
//  Remote
//
//  Created by 8pitch on 05.08.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

public enum VerifyEmailTokenError : RequestError {
    
    case invalidCode
    case expired
    
    public var errorDescription : String? {
        switch self {
        case .invalidCode:
            return "remote.error.response.code.invalid".localized
        case .expired:
            return "remote.error.response.code.expired".localized
        }
    }
    
}

struct VerifyEmailTokenRequestBody : Codable {
    
    var token : String
    
}

struct VerifyEmailTokenResponse : Codable {
    
    var result : String
    
    
    
}

class VerifyEmailTokenRequest : RequestRx {
    
    override var path : String {
        "/user-service/email/verify-token"
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(self.session.accessToken)",
          "Content-Type" : "application/json"
        ]
    }
    
    override var body: Data? {
        let body = VerifyEmailTokenRequestBody(token: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(VerifyEmailTokenResponse.self, from: data)
        switch ValidationResult(rawValue: parsed.result) ?? .invalid {
        case .expired:
            throw VerifyEmailTokenError.expired
        case .invalid:
            throw VerifyEmailTokenError.invalidCode
        case .valid:
            break
        }
    }
    
}

