//
//  VerifyLoginTokenRequest.swift
//  Remote
//
//  Created by 8pitch on 29.07.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

enum ValidationResult: String {
    case expired = "EXPIRED"
    case invalid = "INVALID"
    case valid = "VALID"
}

enum VerifyTokenResult: String {
    case success = "SUCCESS"
    case unsuccess = "UNSUCCESS"
    case inactive = "INACTIVE"
}

struct VerifyLoginTokenRequestBody : Codable {
    
    enum CodingKeys : String, CodingKey {
        case interactionID = "interactionId"
        case token = "token"
    }
    
    var interactionID : String
    var token : String
    
}

struct VerifyLoginTokenResponse : Codable {
    
    var verifyTokenResult: VerifyTokenResult {
        VerifyTokenResult(rawValue: self.result ?? "") ?? .unsuccess
    }
    
    var tokenValidationResult: ValidationResult {
        ValidationResult(rawValue: self.validationResult ?? "") ?? .invalid
    }
    
    var isValid: Bool {
        verifyTokenResult == .success
    }
    
    var valid : Bool?
    var attempts : Int?
    var interactionID : String?
    var result: String?
    var tokenResponse : Token?
    var validationResult : String?
    
    enum CodingKeys : String, CodingKey {
        
        case valid = "valid"
        case interactionID = "interactionId"
        case tokenResponse = "tokenResponse"
        case result = "result"
        case attempts = "remainingAttempts"
        case validationResult = "validationResult"
    }
    
}

class VerifyLoginTokenRequest : RequestRx {
    
    override var path : String {
        "/user-service/login/phones/\(self.session.phone)/verify-token"
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json" ]
    }
    
    override var body: Data? {
        let body = VerifyTokenRequestBody(interactionID: self.session.interactionID, token: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(VerifyLoginTokenResponse.self, from: data)
        self.session.remainingAttemptsProvider.onNext(parsed.attempts)
        if self.session.codeLimitReached && parsed.interactionID?.isEmpty ?? true {
            throw SendCodeError.limit
        }
        switch parsed.tokenValidationResult {
        case .expired:
            throw VerifyTokenError.expired
        case .invalid:
            throw VerifyTokenError.invalidCode
        case .valid:
            break
        }
        self.session.interactionIDProvider.onNext(parsed.interactionID)
        if let accessToken = parsed.tokenResponse?.accessToken,
           let refreshToken = parsed.tokenResponse?.refreshToken
        {
            self.session.accessTokenProvider.onNext(accessToken)
            self.session.refreshTokenProvider.onNext(refreshToken)
            self.saveTokens()
            self.session.loginState = .hasMetadata
        }
    }
    
}
