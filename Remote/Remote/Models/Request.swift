//
//  Request.swift
//  Remote
//
//  Created by 8pitch on 8/21/20.
//

import Input
import RxSwift

class Request: NSObject {
    
    var session : Session
    var disposables : DisposeBag = .init()
    var remote = Remote()
    
    init(session: Session) {
        self.session = session
    }
    
    var path : String {
        fatalError("this needs to be overridden!")
    }
    
    var config : APIConfig.Type {
        return BGHAPIConfig.self
    }
    
    var url : URL {
        var root = self.config.root
        root.path += self.path
        return root.url!
    }
    
    var method : String {
        fatalError("this needs to be overridden!")
    }
    
    var completion : ErrorClosure?
    var body : Data? { nil }
    
    var headers: [String : String]? { nil }
    var queryItems: [URLQueryItem]? { nil }
    
    var request: URLRequest {
        var components = URLComponents(url: self.url, resolvingAgainstBaseURL: false)
        components?.queryItems = queryItems
        guard let urlWithQuery = components?.url else {
            fatalError("unable to configure request url: \(String(describing: components))")
        }
        var request = URLRequest(url: urlWithQuery)
        request.addValue("iOS", forHTTPHeaderField: "User-Agent")
        for header in self.headers ?? [:] {
            request.addValue(header.value, forHTTPHeaderField: header.key)
        }
        request.httpMethod = self.method
        request.httpBody = self.body
        return request
    }
    
    func send(completionHandler: @escaping ErrorClosure) {
        self.completion = completionHandler
        URLSession.shared.dataTask(with: self.request) { data, response, error in
            self.processResponse(data: data, response: response as? HTTPURLResponse, error: error)
        }.resume()
    }
    
    func processResponse(data: Data? = nil, response: HTTPURLResponse? = nil, error: Error? = nil) {
        guard error == nil else {
            let errorCode = (error as NSError?)?.code
            self.completion?(errorCode ?? 0 < 0 ? GenericRequestError.noConnection : GenericRequestError.invalidResponse(error))
            return
        }
        do {
            try self.processResponse(response: response)
            if let data = data {
                try self.parse(data)
            } else {
                throw GenericRequestError.invalidResponse(nil)
            }
            self.completion?(nil)
        } catch GenericRequestError.accessTokenOutdated {
            // no-op, we are trying to refresh the accessToken
            return
        } catch let error where error is RequestError {
            self.completion?(error)
            return
        } catch {
            self.completion?(GenericRequestError.invalidResponse(error))
            return
        }
    }
    
    func processResponse(response: HTTPURLResponse?) throws {
        if response?.statusCode == 401 {
            self.refreshToken()
            throw GenericRequestError.accessTokenOutdated
        }
    }
    
    func parse(_ data: Data) throws {
        // no-op
    }
    
    func saveTokens() {
        BiometryManager.shared.accessToken = self.session.accessToken
        BiometryManager.shared.refreshToken = self.session.refreshToken
    }
    
}

extension Request {
    
    func refreshToken() {
        self.remote.refreshToken(self.session)
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                switch $0 {
                    case .success(_):
                        guard let completion = self?.completion else {
                            self?.completion?(GenericRequestError.accessTokenOutdated)
                            return
                        }
                        self?.send(completionHandler: completion)
                    case .failure(_):
                        // to trigger the view controller to attempt manually refresh token.
                        // if it fails, controller is responsible of showing an error message
                        // that the log in is necessary, and for presenting the login screen
                        self?.session.accessTokenProvider.onNext(nil)
                        self?.saveTokens()
                        self?.completion?(GenericRequestError.accessTokenOutdated)
                }
            }).disposed(by: self.disposables)
    }
    
}
