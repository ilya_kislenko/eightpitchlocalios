//
//  User+Remote.swift
//  Remote
//
//  Created by 8pitch on 30.07.2020.
//

import Input
import RxSwift

extension BehaviorSubject where Element == User {
    
    func onNext(_ response: UserResponse) {
        guard let user = try? self.value() else {
            return
        }
        user.addressProvider.onNext(response.address)
        user.investmentExperienceQuestionnaireProvider.onNext(response.investmentExperienceQuestionnaire)
        user.accountOwnerProvider.onNext(response.accountOwner)
        user.actionIdProvider.onNext(response.actionId)
        user.companyNameProvider.onNext(response.companyName)
        user.companyTypeProvider.onNext(response.companyType)
        user.dateOfBirthProvider.onNext(response.dateOfBirth)
        user.deleteAccountProcessProvider.onNext(response.deleteAccountProcess)
        user.emailConfirmedProvider.onNext(response.emailConfirmed)
        user.emailProvider.onNext(response.email)
        user.externalIdProvider.onNext(response.externalId)
        user.firstNameProvider.onNext(response.firstName)
        user.groupProvider.onNext(response.group)
        user.ibanProvider.onNext(response.iban)
        user.languageProvider.onNext(response.language)
        user.lastNameProvider.onNext(response.lastName)
        let nationality = response.nationality?.countryName
        let country = response.address?.country.countryName
        if (nationality?.isEmpty ?? true) && !(country?.isEmpty ?? true) {
            user.nationalityProvider.onNext(country)
        } else {
            user.nationalityProvider.onNext(nationality)
        }
        user.newNotificationsProvider.onNext(response.newNotifications)
        user.phoneNumberConfirmedProvider.onNext(response.phoneNumberConfirmed)
        user.phoneProvider.onNext(response.phone)
        user.placeOfBirthProvider.onNext(response.placeOfBirth)
        user.politicallyExposedPersonProvider.onNext(response.politicallyExposedPerson)
        user.prefixProvider.onNext(Title(rawTitle: response.prefix).rawValue.localized)
        user.sexProvider.onNext(response.sex)
        user.statusProvider.onNext(response.status)
        user.subscribeToEmailsProvider.onNext(response.subscribeToEmails)
        user.twoFactorAuthenticationEnabledProvider.onNext(response.twoFactorAuthenticationEnabled)
        user.twoFactorAuthenticationTypeProvider.onNext(response.twoFactorAuthenticationType)
        user.usTaxLiabilityProvider.onNext(response.usTaxLiability)
        user.webidUpgradeProcessBlockProvider.onNext(response.webidUpgradeProcessBlock)
        if let taxResponse = response.taxInformation {
            user.taxInformationProvider = .init(value: TaxInformation())
            user.taxInformationProvider.onNext(taxResponse)
        }
        user.commercialRegisterNumberProvider.onNext(response.commercialRegisterNumber)
        user.investorClassProvider.onNext(response.investorClass)
    }
}

extension BehaviorSubject where Element == Address? {
    
    func onNext(_ response: UserResponse.Address?) {
        guard let address = try? self.value() else {
            return
        }
        address.addressLine1Provider.onNext(response?.addressLine1)
        address.addressLine2Provider.onNext(response?.addressLine2)
        address.cityProvider.onNext(response?.city)
        address.countryProvider.onNext(response?.country.countryName)
        address.regionProvider.onNext(response?.region)
        address.streetProvider.onNext(response?.street)
        address.streetNoProvider.onNext(response?.streetNo)
        address.zipProvider.onNext(response?.zip)
        
    }
}

extension BehaviorSubject where Element == InvestmentExperienceQuestionnaire? {
    
    func onNext(_ response: UserResponse.InvestmentExperienceQuestionnaire?) {
        guard let questionnaire = try? self.value() else {
            return
        }
        questionnaire.experienceProvider.onNext(response?.experience)
        questionnaire.experienceDurationProvider.onNext(response?.experienceDuration)
        questionnaire.investingAmountProvider.onNext(response?.investingAmount)
        questionnaire.investingFrequencyProvider.onNext(response?.investingFrequency)
        questionnaire.knowledgeProvider.onNext(response?.knowledge)
        questionnaire.professionalExperienceSourceProvider.onNext(response?.professionalExperienceSource)
        questionnaire.virtualCurrenciesUsedProvider.onNext(response?.virtualCurrenciesUsed)
    }
}

extension BehaviorSubject where Element == TaxInformation? {
    
    func onNext(_ response: UserResponse.TaxInformation?) {
        guard let taxInformation = try? self.value() else {
            return
        }
        taxInformation.churchTaxAttributeProvider.onNext(response?.churchTaxAttribute)
        taxInformation.churchTaxLabilityProvider.onNext(response?.churchTaxLiability)
        taxInformation.taxIDProvider.onNext(response?.taxNumber)
        taxInformation.taxOfficeProvider.onNext(response?.responsibleTaxOffice)
        taxInformation.taxResidentProvider.onNext(response?.isTaxResidentInGermany)
    }
}
