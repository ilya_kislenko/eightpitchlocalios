//
//  VerifyOldPasswordRequest.swift
//  Remote
//
//  Created by 8pitch on 10/9/20.
//

import Input

public enum ConfirmPasswordError : RequestError {
    
    case invalidPassword
    
    public var errorDescription : String? {
        switch self {
        case .invalidPassword:
            return "changePassword.error.incorrectPassword".localized
        }
    }
}

struct VerifyOldPasswordResponse: Codable {
    
    enum Status: String {
        case success = "SUCCESS"
        case invalidPassword = "INVALID_PASSWORD"
    }
    
    var isValid: Bool {
        Status(rawValue: status) == .success
    }
    
    let interactionId: String?
    let status: String
}

struct VerifyOldPasswordRequestBody: Codable {
    let password: String
    let userExternalId: String
}

class VerifyOldPasswordRequest : Request {
    
    override var path : String {
        "/user-service/change-password/verify-old-password"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = VerifyOldPasswordRequestBody(password: self.session.verifyPassword, userExternalId: self.session.externalID)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(VerifyOldPasswordResponse.self, from: data)
        if response.isValid {
            self.session.interactionIDProvider.onNext(response.interactionId)
        } else {
            throw ConfirmPasswordError.invalidPassword
        }
    }
}

