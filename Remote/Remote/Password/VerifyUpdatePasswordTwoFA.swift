//
//  VerifyUpdatePasswordTwoFA.swift
//  Remote
//
//  Created by 8pitch on 09.02.2021.
//

import Foundation
import RxSwift
import RxCocoa
import Input

class VerifyUpdatePasswordTwoFARequest : Request {
    
    override var path : String {
        "/user-service/password/qrcode/verify-code"
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json" ]
    }
    
    override var body: Data? {
        let body = TwoFAVerifyCodeRequestBody(code: self.session.code, interactionId: self.session.interactionID)
        return try? JSONEncoder().encode(body)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(LoginResponseBody.self, from: data)
        let verificationTypes = parsed.verificationTypes?.compactMap { VerificationType(rawValue: $0) }
        self.session.verificationTypes = verificationTypes ?? []
        self.session.remainingAttemptsProvider.onNext(parsed.attempts)
        if self.session.codeLimitReached && parsed.interactionID?.isEmpty ?? true {
            throw SendCodeError.limit
        }
        if !parsed.isValid  {
            throw VerifyTokenError.invalidCode
        }
        self.session.interactionIDProvider.onNext(parsed.interactionID)
        if let accessToken = parsed.token?.accessToken,
           let refreshToken = parsed.token?.refreshToken {
            self.session.accessTokenProvider.onNext(accessToken)
            self.session.refreshTokenProvider.onNext(refreshToken)
            self.saveTokens()
            self.session.loginState = .hasMetadata
        }
    }
    
}

