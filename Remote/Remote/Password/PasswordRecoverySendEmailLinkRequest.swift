//
//  PasswordRecoverySendEmailLinkRequest.swift
//  Remote
//
//  Created by 8pitch on 10/19/20.
//

import Input

public enum PasswordRecoverySendEmailLinkError : RequestError {
    
    case invalidCode
    
    public var errorDescription : String? {
        switch self {
            case .invalidCode:
                return "remote.error.response.code.invalid".localized
        }
    }
}

struct PasswordRecoverySendEmailLinkResponse: Codable {
    
    var tokenValidationResult: ValidationResult {
        ValidationResult(rawValue: self.result) ?? .invalid
    }
    
    let result: String
    
}

struct PasswordRecoverySendEmailLinkRequestBody: Codable {
    let interactionId: String
    let token: String
}

class PasswordRecoverySendEmailLinkRequest : Request {
    
    override var path : String {
        "/user-service/password-recovery/send-email-link"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = PasswordRecoverySendEmailLinkRequestBody(interactionId: self.session.interactionID, token: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
          ["Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(PasswordRecoverySendEmailLinkResponse.self, from: data)
        switch response.tokenValidationResult {
        case .expired:
            throw VerifyTokenError.expired
        case .invalid:
            throw VerifyTokenError.invalidCode
        case .valid:
            break
        }
    }
}


