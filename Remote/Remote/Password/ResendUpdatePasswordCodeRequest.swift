//
//  ResendUpdatePasswordCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 09.02.2021.
//

import Foundation
import RxSwift
import RxCocoa
import Input


class ResendUpdatePasswordCodeRequest : RequestRx {
    
    override var path : String {
        "/user-service/password/sms/resend-limit-code"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = ResendCodeRequestBody(interactionID: self.session.interactionID)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        ["Content-Type" : "application/json",
         "Authorization" : "Basic \(self.authHeader)"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(SendLimitCodeResponse.self, from: data)
        self.session.remainingAttemptsProvider.onNext(parsed.remainingAttempts)
        if self.session.codeLimitReached && parsed.interactionID?.isEmpty ?? true {
            throw SendCodeError.limit
        }
        self.session.interactionIDProvider.onNext(parsed.interactionID)
    }
    
}

