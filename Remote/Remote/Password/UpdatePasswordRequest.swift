//
//  UpdatePasswordRequest.swift
//  Remote
//
//  Created by 8pitch on 09.02.2021.
//


import Input

struct UpdatePasswordRequestBody: Codable {
    let confirmPassword: String
    let interactionId: String
    let password: String
}

class UpdatePasswordRequest: Request {
    
    override var path : String {
        "/user-service/password/update"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = UpdatePasswordRequestBody(confirmPassword: session.changePassword, interactionId: self.session.interactionID, password: session.changePassword)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        ["Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(LoginResponseBody.self, from: data)
        let verificationTypes = parsed.verificationTypes?.compactMap { VerificationType(rawValue: $0) }
        self.session.verificationTypes = verificationTypes ?? []
        self.session.remainingAttemptsProvider.onNext(parsed.attempts)
        if self.session.codeLimitReached && parsed.interactionID?.isEmpty ?? true {
            throw SendCodeError.limit
        }
        self.session.interactionIDProvider.onNext(parsed.interactionID)
        if let accessToken = parsed.token?.accessToken,
           let refreshToken = parsed.token?.refreshToken {
            self.session.accessTokenProvider.onNext(accessToken)
            self.session.refreshTokenProvider.onNext(refreshToken)
            self.saveTokens()
            self.session.loginState = .hasMetadata
        }
    }
}

