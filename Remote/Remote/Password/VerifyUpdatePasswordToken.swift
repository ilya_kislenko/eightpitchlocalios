//
//  VerifyUpdatePasswordToken.swift
//  Remote
//
//  Created by 8pitch on 09.02.2021.
//

import Foundation
import RxSwift
import RxCocoa
import Input

class VerifyUpdatePasswordTokenRequest : RequestRx {
    
    override var path : String {
        "/user-service/password/sms/verify-code"
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json" ]
    }
    
    override var body: Data? {
        let body = VerifyTokenRequestBody(interactionID: self.session.interactionID, token: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(LoginResponseBody.self, from: data)
        let verificationTypes = parsed.verificationTypes?.compactMap { VerificationType(rawValue: $0) }
        self.session.verificationTypes = verificationTypes ?? []
        self.session.remainingAttemptsProvider.onNext(parsed.attempts)
        if self.session.codeLimitReached && parsed.interactionID?.isEmpty ?? true {
            throw SendCodeError.limit
        }
        if !parsed.isValid  {
            throw VerifyTokenError.invalidCode
        }
        self.session.interactionIDProvider.onNext(parsed.interactionID)
        if let accessToken = parsed.token?.accessToken,
           let refreshToken = parsed.token?.refreshToken {
            self.session.accessTokenProvider.onNext(accessToken)
            self.session.refreshTokenProvider.onNext(refreshToken)
            self.saveTokens()
        }
    }
    
}
