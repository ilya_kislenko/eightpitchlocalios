//
//  ChangePasswordRequest.swift
//  Remote
//
//  Created by 8pitch on 10/9/20.
//

import Input

struct ChangePasswordRequestBody: Codable {
    let confirmPassword: String
    let interactionId: String
    let password: String
    let userExternalId: String
}

class ChangePasswordRequest: Request {
    
    override var path : String {
        "/user-service/change-password/change-password"
    }
    
    override var method: String {
        "PUT"
    }
    
    override var body: Data? {
        let body = ChangePasswordRequestBody(confirmPassword: session.changePassword, interactionId: self.session.interactionID, password: session.changePassword, userExternalId: self.session.externalID)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
}
