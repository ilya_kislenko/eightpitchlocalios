//
//  PasswordRecoveryInitialCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 10/19/20.
//

import Input

public enum PasswordRecoveryInitialCodeError : RequestError {
    
    case invalidEmail
    
    public var errorDescription : String? {
        switch self {
        case .invalidEmail:
            return "restorePassword.error.incorrectEmail".localized
        }
    }
}

struct PasswordRecoveryInitialCodeResponse: Codable {
    
    enum Status: String {
        case success = "SUCCESS"
        case unsuccess = "UNSUCCESS"
    }
    
    var isValid: Bool {
        Status(rawValue: status) == .success
    }
    
    let interactionId: String?
    let status: String
}

struct PasswordRecoveryInitialCodeRequestBody: Codable {
    let email: String
}

class PasswordRecoveryInitialCodeRequest : Request {
    
    override var path : String {
        "/user-service/password-recovery/send-initial-sms-code"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = PasswordRecoveryInitialCodeRequestBody(email: self.session.email)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
          ["Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(PasswordRecoveryInitialCodeResponse.self, from: data)
        if response.isValid {
            self.session.interactionIDProvider.onNext(response.interactionId)
        } else {
            self.completion?(PasswordRecoveryInitialCodeError.invalidEmail)
            return
        }
    }
}
