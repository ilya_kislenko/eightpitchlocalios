//
//  ProjectRequest.swift
//  Remote
//
//  Created by 8pitch on 8/26/20.
//

import Foundation
import Input

struct ProjectResponse : Codable {
    
    struct AdditionalFields: Codable {
        var agio : String?
        var conversionRight : String?
        var dividend : String?
        var futureShareNominalValue : String?
        var highOfDividend : String?
        var id : String?
        var initialSalesCharge : String?
        var interest : String?
        var interestInterval : String?
        var issuedShares : String?
        var typeOfRepayment : String?
        var wkin : String?
    }
    
    struct CustomAdditionalField: Codable {
        var fieldName : String?
        var fieldText : String?
        var id : String?
    }
    
    struct PaymentConfiguration: Codable {
        var accountHolderName : String?
        var bic : String?
        var fintecsystemApiKey : String?
        var iban : String?
        var id : Int?
        var klarnaApiKey : String?
        var klarnaCustomerNumber : Int?
        var klarnaProjectId : Int?
    }
    
    struct ProjectFile: Codable {
        var file: File?
        var fileType : String?
        var id : Int?
    }
    
    struct File: Codable {
        var customFileName : String?
        var id : String?
        var originalFileName : String?
        var uniqueFileName : String?
    }
    
    struct ProjectPage: Codable {
        var companyName : String?
        var externalId : String?
        var firstInvestmentArguments : String?
        var firstQuoteTitle : String?
        var id : String?
        var projectPageFiles: [ProjectFile?]
        var projectPageRejections: [ProjectPageRejection?]
        var projectPageStatus : String?
        var promoVideo : String?
        var quoteText : String?
        var secondInvestmentArguments : String?
        var secondQuoteTitle : String?
        var slogan : String?
        var thirdInvestmentArguments : String?
        var url : String?
        var blockConstructor: BlockConstructor?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            companyName = try values.decode(String.self, forKey: .companyName)
            externalId = try values.decode(String.self, forKey: .externalId)
            firstInvestmentArguments = try values.decode(String.self, forKey: .firstInvestmentArguments)
            firstQuoteTitle = try values.decode(String.self, forKey: .firstQuoteTitle)
            id = try values.decode(String.self, forKey: .id)
            projectPageFiles = try values.decode([ProjectFile?].self, forKey: .projectPageFiles)
            projectPageRejections = try values.decode([ProjectPageRejection?].self, forKey: .projectPageRejections)
            projectPageStatus = try values.decode(String.self, forKey: .projectPageStatus)
            promoVideo = try values.decode(String.self, forKey: .promoVideo)
            quoteText = try values.decode(String.self, forKey: .quoteText)
            secondInvestmentArguments = try values.decode(String.self, forKey: .secondInvestmentArguments)
            secondQuoteTitle = try values.decode(String.self, forKey: .secondQuoteTitle)
            slogan = try values.decode(String.self, forKey: .slogan)
            thirdInvestmentArguments = try values.decode(String.self, forKey: .thirdInvestmentArguments)
            url = try values.decode(String.self, forKey: .url)
            do {
                if let blockConstructorData = try values.decode(String.self, forKey: .blockConstructor).data(using: .utf8) {
                    blockConstructor = try JSONDecoder().decode(BlockConstructor.self, from: blockConstructorData)
                }
            } catch {
                blockConstructor = nil
            }
            
            if blockConstructor == nil {
                do {
                    let blockConstructorData = try values.decode(Data.self, forKey: .blockConstructor)
                    blockConstructor = try JSONDecoder().decode(BlockConstructor.self, from: blockConstructorData)
                } catch {
                    blockConstructor = nil
                }
            }
            if blockConstructor == nil {
                do {
                    let blockConstructorData = try values.decode(Data.self, forKey: .blockConstructor)
                    if let decodedData = String(data: blockConstructorData, encoding: .ascii)?.data(using: .utf8) {
                        blockConstructor = try JSONDecoder().decode(BlockConstructor.self, from: decodedData)
                    }
                } catch {
                    blockConstructor = nil
                }
            }
        }
    }
    
    struct BlockConstructor: Codable {
        var tabs: [ProjectPageTab?]
        var fileIdMap: [String: String]
        var i18nData: LanguageData?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            tabs = try values.decode([ProjectPageTab?].self, forKey: .tabs)
            fileIdMap = try values.decode([String: String].self, forKey: .fileIdMap)
            i18nData = try values.decode(LanguageData?.self, forKey: .i18nData)
        }
    }
    
    struct LanguageData: Codable {
        var defaultLanguage: String?
        var i18nMaps: LanguageMap?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            defaultLanguage = try values.decode(String?.self, forKey: .defaultLanguage)
            i18nMaps = try values.decode(LanguageMap?.self, forKey: .i18nMaps)
        }
    }
    
    struct LanguageMap: Codable {
        var en: [String: String?]
        var de: [String: String?]
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            en = try values.decode([String: String?].self, forKey: .en)
            de = try values.decode([String: String?].self, forKey: .de)
        }
    }
    
    struct ProjectPageTab: Codable {
        var id: String?
        var name: String?
        var blocks: [ProjectPageBlock?]
        var payload: LayoutRootIdList?
        
        enum CodingKeys: String, CodingKey {
            case id = "id"
            case name = "name"
            case blocks = "blocks"
            case payload = "payload"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            id = try values.decode(String.self, forKey: .id)
            name = try values.decode(String.self, forKey: .name)
            blocks = try values.decode([ProjectPageBlock?].self, forKey: .blocks)
            if let payloadData = try values.decode(String.self, forKey: .payload).data(using: .utf8) {
                payload = try JSONDecoder().decode(LayoutRootIdList.self, from: payloadData)
            }
            blocks.forEach { block in
                if block?.type == ProjectPageBlockType.group.rawValue {
                    guard var layout = self.payload?.layoutRootIdList, let childs = block?.children else { return }
                    for (key, id) in layout.enumerated() {
                        if id == block?.id {
                            layout.remove(at: key)
                            childs.enumerated().forEach { child in
                                guard let childValue = child.element else { return }
                                layout.insert(childValue, at: key + child.offset)
                            }
                        }
                    }
                }
            }
        }
    }
    
    struct LayoutRootIdList: Codable {
        var layoutRootIdList: [String]
        var nameTranslationCode: String?
    }
    
    struct ProjectPageBlock: Codable {
        var id: String?
        var type: String?
        var payload: ProjectPageBlockPayload?
        var children: [String?]?
        var meta: ProjectPageBlockMeta?
        
        enum CodingKeys: String, CodingKey {
            case type = "type"
            case payload = "payload"
            case id = "id"
            case children = "children"
            case meta = "meta"
        }
        
        func encode(to encoder: Encoder) throws {}
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decode(String.self, forKey: .id)
            type = try values.decode(String.self, forKey: .type)
            children = try? values.decode([String?].self, forKey: .children)
            meta = try? values.decode(ProjectPageBlockMeta?.self, forKey: .meta)
            do {
                if let payloadData = try values.decode(String.self, forKey: .payload).data(using: .utf8) {
                    switch type {
                    case ProjectPageBlockType.text.rawValue:
                        payload = try JSONDecoder().decode(TextBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.image.rawValue:
                        payload = try JSONDecoder().decode(ImageBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.textAndImage.rawValue:
                        payload = try JSONDecoder().decode(TextAndImageBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.FAQ.rawValue:
                        payload = try JSONDecoder().decode(FAQBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.list.rawValue:
                        payload = try JSONDecoder().decode(ListBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.table.rawValue:
                        payload = try JSONDecoder().decode(TableBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.icons.rawValue:
                        payload = try JSONDecoder().decode(IconsBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.video.rawValue:
                        payload = try JSONDecoder().decode(VideoBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.divider.rawValue:
                        payload = try JSONDecoder().decode(DividerBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.facts.rawValue:
                        payload = try JSONDecoder().decode(FactsBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.tilesImages.rawValue:
                        payload = try JSONDecoder().decode(TilesImagesBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.dualColumnImages.rawValue:
                        payload = try JSONDecoder().decode(DualColumnImagesBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.imageLink.rawValue:
                        payload = try JSONDecoder().decode(ImageLinkBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.dualColumnText.rawValue:
                        payload = try JSONDecoder().decode(DualColumnTextBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.button.rawValue:
                        payload = try JSONDecoder().decode(ButtonBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.disclaimer.rawValue:
                        payload = try JSONDecoder().decode(DisclaimerBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.chart.rawValue:
                        payload = try JSONDecoder().decode(ChartBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.linkedText.rawValue:
                        payload = try JSONDecoder().decode(LinkedTextBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.department.rawValue:
                        payload = try JSONDecoder().decode(DepartmentBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.header.rawValue:
                        payload = try JSONDecoder().decode(HeaderBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.delimiter.rawValue:
                        payload = try JSONDecoder().decode(LineDelimiterBlockPayload.self, from: payloadData)
                    case ProjectPageBlockType.group.rawValue:
                        payload = nil
                    default:
                        break
                    }
                }
            } catch {
                payload = nil
            }
        }
    }
    
    struct LineDelimiterBlockPayload: ProjectPageBlockPayload, Codable {
        var above: Int?
        var below: Int?
    }
    
    struct ChartBlockPayload: ProjectPageBlockPayload, Codable {
        var perLine: Int?
        var items: [ChartBlockPayloadItem?]
    }
    
    struct ChartBlockPayloadItem: Codable {
        var title: String?
        var chartType: String?
        var data: [[String?]]
        var text: String?
        
        enum CodingKeys: String, CodingKey {
            case title = "title"
            case chartType = "chartType"
            case data = "data"
            case text = "text"
        }
        
        func encode(to encoder: Encoder) throws {}
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            title = try values.decode(String.self, forKey: .title)
            chartType = try values.decode(String.self, forKey: .chartType)
            data = try values.decode([[String?]].self, forKey: .data)
            text = try? values.decode(String.self, forKey: .text)
            data.removeFirst()
        }
    }
    
    struct ProjectPageBlockMeta: Codable {
        var header: String?
        var previewImage: String?
    }
    
    struct FactsBlockPayload: ProjectPageBlockPayload, Codable {
        var perLine: Int?
        var items: [FactsBlockPayloadItem?]
    }
    
    struct FactsBlockPayloadItem: Codable {
        var imageId: String?
        var imageFileName: String?
        var text: String?
        var header: String?
    }
    
    struct TilesImagesBlockPayload: ProjectPageBlockPayload, Codable {
        var perLine: Int?
        var bold: Bool?
        var italic: Bool?
        var underline: Bool?
        var items: [TilesImagesBlockPayloadItem?]
    }
    
    struct TilesImagesBlockPayloadItem: Codable {
        var imageId: String?
        var imageFileName: String?
        var header: String?
        var text: String?
    }
    
    struct DualColumnImagesBlockPayload:  ProjectPageBlockPayload, Codable {
        var leftImageId: String?
        var leftImageFileName: String?
        var leftText: String?
        var leftHeader: String?
        var rightImageId: String?
        var rightImageFileName: String?
        var rightText: String?
        var rightHeader: String?
    }
    
    struct ImageBlockPayload:  ProjectPageBlockPayload, Codable {
        var fullWidth: Bool?
        var useText: Bool?
        var imageId: String?
        var imageFileName: String?
        var text: String?
        var color: String?
        var fontSize: Int?
        var justify: String?
        var bold: Bool?
        var italic: Bool?
        var underline: Bool?
        var fontFamily: String?
        var header: String?
        var preHeader: String?
        var headingSize: String?
    }
    
    struct TextBlockPayload:  ProjectPageBlockPayload, Codable {
        var text: String?
        var headingSize: String?
        var color: String?
        var fontSize: Int?
        var justify: String?
        var bold: Bool?
        var italic: Bool?
        var underline: Bool?
        var fontFamily: String?
        var header: String?
        var preHeader: String?
    }
    
    struct TextAndImageBlockPayload:  ProjectPageBlockPayload, Codable {
        var imageId: String?
        var imageFileName: String?
        var textPosition: String?
        var header: String?
        var imageDescription: String?
        var text: String?
    }
    
    struct FAQBlockPayload:  ProjectPageBlockPayload, Codable {
        var header: String?
        var items: [FaqItem?]
        var language: String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            header = try values.decode(String.self, forKey: .header)
            items = try values.decode([FaqItem?].self, forKey: .items)
            language = try values.decode(String?.self, forKey: .language)
        }
    }
    
    struct FaqItem: Codable {
        var question: String?
        var answer: String?
        var text: String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            question = try values.decode(String.self, forKey: .question)
            answer = try values.decode(String.self, forKey: .answer)
            text = try values.decodeIfPresent(String.self, forKey: .text)
        }
    }
    
    struct ListBlockPayload:  ProjectPageBlockPayload, Codable {
        var header: String?
        var items: [ListBlockPayloadItem?]
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            header = try values.decode(String.self, forKey: .header)
            items = try values.decode([ListBlockPayloadItem?].self, forKey: .items)
        }
    }
    
    struct ListBlockPayloadItem: Codable {
        var text: String?
    }
    
    struct TableBlockPayload:  ProjectPageBlockPayload, Codable {
        var rowsNumber: Int?
        var colsNumber: Int?
        var headerColor: String?
        var rows = [[String?]]()
        
        enum RowTextCodingKeys: String, CodingKey {
            case text
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            rowsNumber = try values.decode(Int.self, forKey: .rowsNumber)
            colsNumber = try values.decode(Int.self, forKey: .colsNumber)
            var rowsContainer = try values.nestedUnkeyedContainer(forKey: .rows)
            while !rowsContainer.isAtEnd {
                var payloadContainer = try rowsContainer.nestedUnkeyedContainer()
                var titles = [String?]()
                while !payloadContainer.isAtEnd {
                    let container = try payloadContainer.nestedContainer(keyedBy: RowTextCodingKeys.self)
                    titles.append(try container.decode(String.self, forKey: .text))
                }
                rows.append(titles)
            }
        }
    }
    
    struct TableBlockPayloadCellItem: Codable {
        var text: [String?]
    }
    
    struct IconsBlockPayload:  ProjectPageBlockPayload, Codable {
        var perLine: Int?
        var items: [IconsBlockPayloadIconItem?]
    }
    
    struct IconsBlockPayloadIconItem: Codable {
        var imageId: String?
        var imageFileName: String?
        var link: String?
        
        enum CodingKeys: String, CodingKey {
            case imageId,
                 imageFileName,
                 link
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            imageId = try values.decode(String.self, forKey: .imageId)
            imageFileName = try values.decode(String.self, forKey: .imageFileName)
            link = try values.decode(String.self, forKey: .link)
        }
    }
    
    struct VideoBlockPayload:  ProjectPageBlockPayload, Codable {
        var vimeoId: String?
        var justify: String?
        var useText: Bool?
        var header: String?
        var text: String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            vimeoId = try values.decode(String.self, forKey: .vimeoId)
            justify = try values.decodeIfPresent(String.self, forKey: .justify)
            useText = try values.decodeIfPresent(Bool.self, forKey: .useText)
            header = try values.decodeIfPresent(String.self, forKey: .header)
            text = try values.decodeIfPresent(String.self, forKey: .text)
        }
    }
    
    struct DividerBlockPayload: ProjectPageBlockPayload, Codable {
        var height: Int?
    }
    
    struct ImageLinkBlockPayload: ProjectPageBlockPayload, Codable {
        var perLine: Int?
        var items: [ImageLinkBlockPayloadItem?]
        var isLinkAdded: Bool?
        var isDescriptionAdded: Bool?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            perLine = try values.decode(Int.self, forKey: .perLine)
            items = try values.decodeIfPresent([ImageLinkBlockPayloadItem?].self, forKey: .items) ?? []
            isLinkAdded = try values.decodeIfPresent(Bool.self, forKey: .isLinkAdded)
            isDescriptionAdded = try values.decodeIfPresent(Bool.self, forKey: .isDescriptionAdded)
        }
    }
    
    struct ImageLinkBlockPayloadItem: ProjectPageBlockPayload, Codable {
        var imageUrl: String?
        var imageFileName: String?
        var link: String?
        var description: String?
    }
    
    struct DualColumnTextBlockPayload: ProjectPageBlockPayload, Codable {
        var leftText: String?
        var leftHeader: String?
        var rightText: String?
        var rightHeader: String?
    }
    
    struct ButtonBlockPayload: ProjectPageBlockPayload, Codable {
        var btnColor: String?
        var textColor: String?
        var text: String?
        var link: String?
    }
    
    struct DisclaimerBlockPayload: ProjectPageBlockPayload, Codable {
        var iconUrl: String?
        var iconFileName: String?
        var text: String?
        var background: String?
    }
    
    struct LinkedTextBlockPayload: ProjectPageBlockPayload, Codable {
        var text: String?
        var color: String?
        var fontSize: Int?
        var justify: String?
        var bold: Bool
        var italic: Bool
        var underline: Bool
        var fontFamily: String?
        var wordsLink: [LinkedTextBlockWordLink?]
        var language: String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            text = try values.decode(String?.self, forKey: .text)
            color = try values.decode(String?.self, forKey: .color)
            fontSize = try values.decode(Int?.self, forKey: .fontSize)
            justify = try values.decode(String?.self, forKey: .justify)
            bold = try values.decode(Bool.self, forKey: .bold)
            italic = try values.decode(Bool.self, forKey: .italic)
            underline = try values.decode(Bool.self, forKey: .underline)
            fontFamily = try values.decode(String?.self, forKey: .fontFamily)
            wordsLink = try values.decode([LinkedTextBlockWordLink?].self, forKey: .wordsLink)
            language = try values.decode(String?.self, forKey: .language)
        }
    }
    
    struct LinkedTextBlockWordLink: Codable, ProjectPageBlockPayload {
        var linkUrl: String?
        var position: Int?
        var text: String?
        var label: String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            linkUrl = try values.decode(String?.self, forKey: .linkUrl)
            position = try values.decode(Int?.self, forKey: .position)
            text = try values.decode(String?.self, forKey: .text)
            label = try values.decode(String?.self, forKey: .label)
        }
    }
    
    struct DepartmentBlockPayload: ProjectPageBlockPayload, Codable {
        var header: String?
        var items: [DepartmentBlockPayloadItem?]
        var language: String?
    }
    
    struct DepartmentBlockPayloadItem: ProjectPageBlockPayload, Codable {
        var fullName: String?
        var position: String?
        var linkedinUrl: String?
        var imageUrl: String?
        var imageFileName: String?
    }
    
    struct HeaderBlockPayload: ProjectPageBlockPayload, Codable {
        var text: String?
        var headingSize: String?
        var preHeader: String?
    }
    
    struct ProjectPageRejection: Codable {
        var comment : String?
        var createDate : String?
        var id : Int?
    }
    
    struct ProjectInitiator: Codable {
        var email : String?
        var firstName : String?
        var lastName : String?
        var phone : String?
        var prefix : String?
    }
    
    struct FinancialInformation: Codable {
        var companyAddress : String?
        var country : String?
        var financingPurpose : String?
        var goal : Int?
        var guarantee : String?
        var id : String?
        var investmentSeries : String?
        var isin : String?
        var nominalValue : String?
        var threshold : Int?
        var typeOfSecurity : String?
    }
    
    var additionalFields: AdditionalFields?
    var companyName : String?
    var customAdditionalFields: [CustomAdditionalField?]
    var domain : String?
    var financialInformation: ProjectResponse.FinancialInformation?
    var geoipRestrictions: [[Int:String]]?
    var graph: ProjectsResponse.Graph?
    var id : String?
    var legalForm : String?
    var paymentConfiguration: PaymentConfiguration?
    var possibilityAccept : Bool?
    var projectDescription : String?
    var projectExternalId : String?
    var projectFiles: [ProjectFile?]
    var projectInitiator: ProjectInitiator?
    var projectInitiatorExternalId : String?
    var projectPage: ProjectPage?
    var projectStatus : String?
    var tokenId : String?
    var tokenParametersDocument: ProjectsResponse.TokenParametersDocument?
    var totalNumberOfTokens: Int?
    var whitelistedUserExternalIds: [[Int: String]]?
}

class ProjectRequest : Request {
    
    private var projectID: Int
    override var path : String {
        "/project-service/projects/\(self.projectID)"
    }
    
    override var method: String {
        "GET"
    }
    
    init(session: Session, projectID: Int) {
        self.projectID = projectID
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let project = try JSONDecoder().decode(ProjectResponse.self, from: data)
        self.session.projectsProvider.expandedProject = ExpandedProject(project: project, currentLanguage: (Language(rawValue: Locale.current.languageCode ?? "de") ?? .de).rawValue)
    }
}
