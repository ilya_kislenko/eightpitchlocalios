//
//  InvestorsProjectsRequest.swift
//  Remote
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

// MARK: - InvestorsProjectElement
struct InvestorsProjectResponse: Codable {
    let investmentsInfo: InvestorsInvestmentsInfo?
    let projectFiles: [InvestorsProjectFile?]
    let projectInfo: InvestorsProjectInfo?
    let projectStatus: String?
}

// MARK: - InvestorsInvestmentsInfo
struct InvestorsInvestmentsInfo: Codable {
    let investments: [InvestorsInvestment?]
    let lastInvestmentStatus: String?
    let totalAmountOfTokens: Double?
}

// MARK: - InvestorsInvestment
struct InvestorsInvestment: Codable {
    let amount: Double?
    let createDateTime, status: String?
}

// MARK: - InvestorsProjectFile
struct InvestorsProjectFile: Codable {
    let file: InvestorsProjectFileFile?
    let fileType: String?
    let id: Int?
}

// MARK: - InvestorsProjectFileFile
struct InvestorsProjectFileFile: Codable {
    let customFileName, id, originalFileName, uniqueFileName: String?
}

// MARK: - InvestorsProjectInfo
struct InvestorsProjectInfo: Codable {
    let companyName, projectInfoDescription: String?
    let financialInformation: InvestorsFinancialInformation?
    let graph: InvestorsGraph?
    let id: Int?
    let projectPageFiles: [InvestorsProjectPageFile?]
    let projectPageURL: String?
    let tokenParametersDocument: InvestorsTokenParametersDocument?
    let videoURL: String?

    enum CodingKeys: String, CodingKey {
        case companyName
        case projectInfoDescription = "description"
        case financialInformation, graph, id, projectPageFiles
        case projectPageURL = "projectPageUrl"
        case tokenParametersDocument
        case videoURL = "videoUrl"
    }
}

// MARK: - InvestorsFinancialInformation
struct InvestorsFinancialInformation: Codable {
    let investmentSeries, nominalValue, typeOfSecurity: String?
}

// MARK: - InvestorsGraph
struct InvestorsGraph: Codable {
    let currentFundingSum: Decimal?
    let currentFundingSumInTokens, hardCap, hardCapInTokens: Int?
    let softCap, softCapInTokens: Int?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        currentFundingSum = try? values.decodeIfPresent(Decimal.self, forKey: .currentFundingSum)
        currentFundingSumInTokens = try? values.decodeIfPresent(Int.self, forKey: .currentFundingSumInTokens)
        hardCap = try? values.decodeIfPresent(Int.self, forKey: .hardCap)
        hardCapInTokens = try? values.decodeIfPresent(Int.self, forKey: .hardCapInTokens)
        softCap = try? values.decodeIfPresent(Int.self, forKey: .softCap)
        softCapInTokens = try? values.decodeIfPresent(Int.self, forKey: .softCapInTokens)
    }
}

// MARK: - InvestorsProjectPageFile
struct InvestorsProjectPageFile: Codable {
    let file: InvestorsProjectPageFileFile?
    let fileType: String?
    let id: Int?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        file = try? values.decodeIfPresent(InvestorsProjectPageFileFile.self, forKey: .file)
        fileType = try? values.decodeIfPresent(String.self, forKey: .fileType)
        id = try? values.decodeIfPresent(Int.self, forKey: .id)
    }
}

// MARK: - InvestorsProjectPageFileFile
struct InvestorsProjectPageFileFile: Codable {
    let customFileName: String?
    let encrypted: Bool?
    let id: String?
    let originalFileName, uniqueFileName: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        customFileName = try? values.decodeIfPresent(String.self, forKey: .customFileName)
        encrypted = try? values.decodeIfPresent(Bool.self, forKey: .encrypted)
        id = try? values.decodeIfPresent(String.self, forKey: .id)
        originalFileName = try? values.decodeIfPresent(String.self, forKey: .originalFileName)
        uniqueFileName = try? values.decodeIfPresent(String.self, forKey: .uniqueFileName)
    }
}

// MARK: - InvestorsTokenParametersDocument
struct InvestorsTokenParametersDocument: Codable {
    let dsoProjectFinishDate, dsoProjectStartDate: String?
    let hardCap, minimumInvestmentAmount, nominalValue: Int?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        dsoProjectFinishDate = try? values.decodeIfPresent(String.self, forKey: .dsoProjectFinishDate)
        dsoProjectStartDate = try? values.decodeIfPresent(String.self, forKey: .dsoProjectStartDate)
        hardCap = try? values.decodeIfPresent(Int.self, forKey: .hardCap)
        minimumInvestmentAmount = try? values.decodeIfPresent(Int.self, forKey: .minimumInvestmentAmount)
        nominalValue = try? values.decodeIfPresent(Int.self, forKey: .nominalValue)
    }
}

typealias InvestorProjects = [InvestorsProjectResponse]

class InvestorsProjectsRequest: Request {

    override var path : String {
        "/project-service/investors/projects"
    }

    override var method: String {
        "GET"
    }

    override var headers: [String : String]? {
        ["Authorization" : "Bearer \(self.session.accessToken)",
         "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(InvestorProjects.self, from: data)
        self.session.projectsProvider.investorsProjects = response.map { Input.InvestorsProject(project: $0) }
    }
    
}
