//
//  FilteredProjectsRequest.swift
//  Remote
//
//  Created by 8pitch on 19.01.21.
//

import Foundation
import Input

class FilteredProjectsRequest: Request {
    
    private let currentQueryItems: [URLQueryItem]
    
    override var path: String {
        "/project-service/project-overview/paged"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json",
          "Authorization" : "Bearer \(session.accessToken)" ]
    }
    
    override var queryItems: [URLQueryItem]? {
        self.currentQueryItems
    }
    
    init(session: Session, queryItems: [URLQueryItem]?) {
        self.currentQueryItems = queryItems ?? []
        super.init(session: session)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(ProjectsResponse.self, from: data).content
        self.session.projectsFilterProvider.filteredProjects = parsed.map { Input.Project($0) }
    }
}
