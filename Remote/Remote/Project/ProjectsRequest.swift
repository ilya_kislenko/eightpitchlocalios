//
//  GetProjectsRequest.swift
//  Remote
//
//  Created by 8pitch on 20.08.2020.
//

import Foundation
import Input
import RxSwift

public struct ProjectsResponse : Codable {
    
    struct Project: Codable {
        public var id : Int?
        public var companyName: String?
        public var description: String?
        public var projectPageURL: String?
        public var videoURL: String?
        public var graph: Graph?
        public var financialInformation: FinancialInformation?
        public var tokenParametersDocument: TokenParametersDocument?
        public var projectPageFiles: [ProjectFile?]
    }
    
    struct Graph: Codable {
        public var hardCap : Double?
        public var currentFundingSum: Double?
        public var hardCapInTokens: Int?
        public var currentFundingSumInTokens: Int?
        public var softCap: Double?
        public var softCapInTokens: Int?
    }
    
    struct ProjectFile: Codable {
        var file: File?
        var fileType : String?
        var id : Int?
    }
    
    struct File: Codable {
        var customFileName : String?
        var id : String?
        var originalFileName : String?
        var uniqueFileName : String?
    }
    
    struct FinancialInformation: Codable {
        
        public var typeOfSecurity : String?
        public var investmentSeries: String?
        public var nominalValue: String?
        
    }
    
    struct TokenParametersDocument: Codable {
        public var assetBasedFees: Double?
        public var assetId: String?
        public var breakable: Int?
        public var currentSupply: Int?
        public var distributorId: String?
        public var dsoProjectFinishDate: String?
        public var dsoProjectStartDate: String?
        public var hardCap: Int?
        public var id: Int?
        public var investmentStepSize: Double?
        public var isin: String?
        public var issuerId: String?
        public var minimumInvestmentAmount: Double?
        public var multisignRelease: Bool?
        public var nominalValue: Double?
        public var productType: String?
        public var projectId: Int?
        public var shortCut: String?
        public var singleInvestorOwnershipLimit: Double?
        public var softCap: Int?
        public var tokenId: String?
        public var totalNumberOfTokens: Int?
        public var voting: Bool?
    }
    
    var content : [Project]
    var empty : Bool
    var first : Bool
    var last : Bool
    var number : Int
    var numberOfElements : Int
    var pageable : Pageable
    var size : Int
    var sort : Sort
    var totalElements : Int
    var totalPages : Int
    
    struct Pageable: Codable {
        var pageSize : Int
        var pageNumber : Int
        var sort : Sort
        var offset: Int
        var paged: Bool
        var unpaged: Bool
    }
    
    struct Sort: Codable {
        var empty : Bool
        var sorted : Bool
        var unsorted : Bool
    }
    
}

class ProjectsRequest: RequestRx {
    
    override var path : String {
        "/project-service/projects/paged"
    }
    
    override var url : URL {
        let url = super.url
        return URL(string: url.absoluteString + "?page=\(Constants.page)&size=\(Constants.size)")!
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(self.session.accessToken)",
            "Content-Type" : "application/json"
        ]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(ProjectsResponse.self, from: data).content
        self.session.projectsProvider.projects = parsed.map { Input.Project($0) }
    }
    
    enum Constants {
        static let size: Int = 9999
        static let page: Int = 0
    }
    
}

extension Input.Project {
    convenience init(_ project: ProjectsResponse.Project?) {
        self.init(id : project?.id, companyName: project?.companyName,
                  description: project?.description, projectPageURL: project?.projectPageURL,
                  videoURL: project?.videoURL, graph: Graph(project?.graph),
                  financialInformation: FinancialInformation(project?.financialInformation),
                  tokenParametersDocument: TokenParametersDocument(project?.tokenParametersDocument),
                  projectPageFiles: project?.projectPageFiles.map { ProjectFile(file: $0) } ?? [])
    }
}

extension Input.Graph {
    
    convenience init(_ graph: ProjectsResponse.Graph?) {
        self.init(hardCap : graph?.hardCap,
                  currentFundingSum: graph?.currentFundingSum,
                  hardCapInTokens: graph?.hardCapInTokens,
                  currentFundingSumInTokens: graph?.currentFundingSumInTokens,
                  softCap : graph?.softCap, softCapInTokens: graph?.softCapInTokens)
    }
}

extension Input.ProjectFile {
    convenience init(file: ProjectsResponse.ProjectFile?) {
        self.init(file: File(file: file?.file), fileType : file?.fileType, id : file?.id)
    }
}

extension Input.File {
    convenience init(file: ProjectsResponse.File?) {
        self.init(customFileName : file?.customFileName, id : file?.id,
                  originalFileName : file?.originalFileName, uniqueFileName : file?.uniqueFileName)
    }
}

extension Input.FinancialInformation {
    
    convenience init(_ info: ProjectsResponse.FinancialInformation?) {
        self.init(typeOfSecurity : info?.typeOfSecurity,
                  investmentSeries: info?.investmentSeries,
                  nominalValue: info?.nominalValue)
    }
}

extension Input.TokenParametersDocument {
    
    convenience init(_ doc: ProjectsResponse.TokenParametersDocument?) {
        self.init(assetBasedFees: doc?.assetBasedFees, assetId: doc?.assetId, breakable: doc?.breakable,
                  currentSupply: doc?.currentSupply, distributorId: doc?.distributorId, dsoProjectFinishDate: doc?.dsoProjectFinishDate,
                  dsoProjectStartDate: doc?.dsoProjectStartDate, hardCap: doc?.hardCap, id: doc?.id,
                  investmentStepSize: doc?.investmentStepSize, isin: doc?.isin, issuerId: doc?.issuerId,
                  minimumInvestmentAmount: doc?.minimumInvestmentAmount, multisignRelease: doc?.multisignRelease, nominalValue: doc?.nominalValue,
                  productType: doc?.productType, projectId: doc?.projectId, shortCut: doc?.shortCut,
                  singleInvestorOwnershipLimit: doc?.singleInvestorOwnershipLimit, softCap: doc?.softCap, tokenId: doc?.tokenId,
                  totalNumberOfTokens: doc?.totalNumberOfTokens, voting: doc?.voting)
    }
    
}
