//
//  ProjectsWithStatusRequest.swift
//  Remote
//
//  Created by 8pitch on 2/1/21.
//

import Foundation
import Input

public enum ProjectsType {
    case current
    case completed
}

class ProjectsWithStatusRequest: Request {
    
    private let type: ProjectsType
    
    override var path: String {
        "/project-service/project-overview/paged"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json",
          "Authorization" : "Bearer \(session.accessToken)" ]
    }
    
    override var queryItems: [URLQueryItem]? {
        let value = self.type == .current ? Constants.currentStatusQueryItemValues : Constants.completedStatusQueryItemValues
        return [URLQueryItem(name: "projectStatuses", value:value),
                URLQueryItem(name: "size", value: "\(Constants.size)"),
                URLQueryItem(name: "page", value: "\(Constants.page)")]
    }
    
    init(session: Session, type: ProjectsType) {
        self.type = type
        super.init(session: session)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(ProjectsResponse.self, from: data).content
        switch type {
        case .current:
            self.session.projectsProvider.currentProjects = parsed.map { Input.Project($0) }
        case .completed:
            self.session.projectsProvider.completedProjects = parsed.map { Input.Project($0) }
        }
    }
    
    enum Constants {
        static var currentStatusQueryItemValues: String = {
            let statuses: [ProjectStatus] = [.comingSoon, .active]
            return statuses.map { $0.rawValue }.joined(separator: ",")
        }()
        static let completedStatusQueryItemValues: String = {
            let statuses: [ProjectStatus] = [.finished, .waitBlockchain, .refunded, .manualRelease, .waitRelease, .tokensTransferred]
            return statuses.map { $0.rawValue }.joined(separator: ",")
        }()
        static let size: Int = 9999
        static let page: Int = 0
    }
}

