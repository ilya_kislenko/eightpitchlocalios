//
//  InvestorsGraphRequest.swift
//  Remote
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

struct InvestorsGraphResponse: Codable {
    let investments: [InvestorsInvestmentElement?]
    let totalMonetaryAmount: Double?
}

// MARK: - InvestorsInvestmentElement
struct InvestorsInvestmentElement: Codable {
    let companyName: String?
    let monetaryAmount: Double?
}

class InvestorsGraphRequest: Request {

    override var path : String {
        "/project-service/investors/graph"
    }

    override var method: String {
        "GET"
    }

    override var headers: [String : String]? {
        ["Authorization" : "Bearer \(self.session.accessToken)",
         "Content-Type" : "application/json"]
    }

    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(InvestorsGraphResponse.self, from: data)
        self.session.projectsProvider.investorsPaymentsGraph = InvestorsPaymentsGraph(item: response)
    }
    
}

