//
//  InvestedProjectsResponse.swift
//  Remote
//
//  Created by 8pitch on 10/1/20.
//

import Foundation

public struct InvestedProjectsResponse : Codable {
    
    struct Project: Codable {
        public var id : Int?
        public var companyName: String?
        public var description: String?
        public var projectPageURL: String?
        public var videoURL: String?
        public var graph: Graph?
        public var financialInformation: FinancialInformation?
        public var tokenParametersDocument: TokenParametersDocument?
    }
    
    struct Graph: Codable {
        public var hardCap : Double?
        public var currentFundingSum: Double?
        public var hardCapInTokens: Int?
        public var currentFundingSumInTokens: Int?
        public var softCap: Double?
        public var softCapInTokens: Int?
    }
    
    struct FinancialInformation: Codable {
        
        public var typeOfSecurity : String?
        public var investmentSeries: String?
        public var nominalValue: String?
        
    }
    
    struct TokenParametersDocument: Codable {
        public var assetBasedFees: Double?
        public var assetId: String?
        public var breakable: Int?
        public var currentSupply: Int?
        public var distributorId: String?
        public var dsoProjectFinishDate: String?
        public var dsoProjectStartDate: String?
        public var hardCap: Int?
        public var id: Int?
        public var investmentStepSize: Double?
        public var isin: String?
        public var issuerId: String?
        public var minimumInvestmentAmount: Double?
        public var multisignRelease: Bool?
        public var nominalValue: Double?
        public var productType: String?
        public var projectId: Int?
        public var shortCut: String?
        public var singleInvestorOwnershipLimit: Double?
        public var softCap: Int?
        public var tokenId: String?
        public var totalNumberOfTokens: Int?
        public var voting: Bool?
    }
    
    var projectInfo: ProjectsResponse.Project?
    
}
