//
//  VimeoVideoURLRequest.swift
//  Remote
//
//

import Foundation
import Input

// MARK: - VimeoWelcome
struct VimeoWelcome: Codable {
    let request: VimeoRequest?
}

// MARK: - VimeoRequest
struct VimeoRequest: Codable {
    let files: VimeoFiles?
}

// MARK: - VimeoFiles
struct VimeoFiles: Codable {
    let progressive: [VimeoProgressive]
}

// MARK: - VimeoProgressive
struct VimeoProgressive: Codable {
    let url: String?
}

class VimeoVideoURLRequest: NSObject {

    func urlString(videoId: String) -> String {
        return "\(WebURLConstants.vimeoBaseURLString)\(videoId)\(WebURLConstants.vimeoBaseURLastPathComponentString)"
    }

    func send(_ videoID: String,completionHandler: @escaping ResponseLinkClosure) {
        guard let url = URL(string: self.urlString(videoId: videoID)) else {
            completionHandler(.failure(GenericRequestError.invalidResponse(nil)))
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completionHandler(.failure(error))
                } else if let jsonData = data {
                    let vimeo = try? JSONDecoder().decode(VimeoWelcome.self, from: jsonData)
                    guard let linkString = vimeo?.request?.files?.progressive.last?.url,
                          let link = URL(string: linkString) else {
                        completionHandler(.failure(GenericRequestError.invalidResponse(nil)))
                        return
                    }
                    completionHandler(.success(link))
                }
            }
        }.resume()
    }
}
