//
//  InitiatorsProjectsRequest.swift
//  Remote
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

struct InitiatorsProjectsResponse: Codable {
    let content: [InitiatorsContent?]
}

// MARK: - InitiatorsContent
struct InitiatorsContent: Codable {
    let companyName: String?
    let graph: InitiatorsGraph?
    let id: String?
    let projectBI: InitiatorsProjectBI?
    let projectDescription: String?
    let projectFiles: [InitiatorsProjectFile?]
    let projectPage: InitiatorsProjectPage?
    let projectStatus: String?
    let tokenParametersDocument: InitiatorsTokenParametersDocument?
    
    enum CodingKeys: String, CodingKey {
        case companyName, graph, id
        case projectBI = "projectBi"
        case projectDescription, projectFiles, projectPage, projectStatus, tokenParametersDocument
    }
}

// MARK: - InitiatorsGraph
struct InitiatorsGraph: Codable {
    let currentFundingSum, hardCap: Decimal?
    let currentFundingSumInTokens, hardCapInTokens: Int?
    let softCap: Decimal?
    let softCapInTokens: Int?
}

// MARK: - InitiatorsProjectBI
struct InitiatorsProjectBI: Codable {
    let averageInvestment, capitalInvested: Decimal?
    let investors: InitiatorsInvestors?
    let maxInvestment, minInvestment: Decimal?
}

// MARK: - InitiatorsInvestors
struct InitiatorsInvestors: Codable {
    let totalCount: Int?
}

// MARK: - InitiatorsProjectFile
struct InitiatorsProjectFile: Codable {
    let file: InitiatorsFile?
    let fileType: String?
    let id: Int?
}

// MARK: - InitiatorsFile
struct InitiatorsFile: Codable {
    let customFileName, id, originalFileName, uniqueFileName: String?
}

// MARK: - InitiatorsProjectPage
struct InitiatorsProjectPage: Codable {
    let promoVideo, url: String?
}

// MARK: - InitiatorsTokenParametersDocument
struct InitiatorsTokenParametersDocument: Codable {
    let dsoProjectFinishDate, dsoProjectStartDate: String?
    let nominalValue: Decimal?
}

class InitiatorsProjectsRequest: Request {

    private enum Constants {
        static let page: Int = 0
        static let size: Int = 9999
        static let sort: String = "asc"
    }
    
    override var path : String {
        "/project-service/initiators/projects"
    }
    
    override var method: String {
        "GET"
    }

    override var request: URLRequest {
        var components = URLComponents(url: self.url, resolvingAgainstBaseURL: false)
        components?.queryItems = [
            URLQueryItem(name: "page", value: "\(Constants.page)"),
            URLQueryItem(name: "size", value: "\(Constants.size)"),
            URLQueryItem(name: "status", value: "\(ProjectStatus.notStarted.rawValue)"),
            URLQueryItem(name: "status", value: "\(ProjectStatus.comingSoon.rawValue)"),
            URLQueryItem(name: "status", value: "\(ProjectStatus.active.rawValue)"),
            URLQueryItem(name: "status", value: "\(ProjectStatus.finished.rawValue)"),
            URLQueryItem(name: "status", value: "\(ProjectStatus.waitBlockchain.rawValue)"),
            URLQueryItem(name: "status", value: "\(ProjectStatus.refunded.rawValue)"),
            URLQueryItem(name: "status", value: "\(ProjectStatus.manualRelease.rawValue)"),
            URLQueryItem(name: "status", value: "\(ProjectStatus.waitRelease.rawValue)"),
            URLQueryItem(name: "status", value: "\(ProjectStatus.tokensTransferred.rawValue)")
        ]
        guard let urlWithQuery = components?.url else {
            fatalError("unable to configure request url: \(String(describing: components))")
        }
        var request = URLRequest(url: urlWithQuery)
        request.httpMethod = self.method
        request.addValue("iOS", forHTTPHeaderField: "User-Agent")
        for header in self.headers ?? [:] {
            request.addValue(header.value, forHTTPHeaderField: header.key)
        }
        return request
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json",
          "Authorization" : "Bearer \(session.accessToken)" ]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(InitiatorsProjectsResponse.self, from: data)
        self.session.projectsProvider.initiatorsProjects = response.content.map{ InitiatorsProject(item: $0)}
    }
    
}
