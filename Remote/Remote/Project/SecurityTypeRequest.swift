//
//  SecurityTypeRequest.swift
//  Remote
//
//  Created by 8pitch on 19.01.21.
//

import Foundation
import Input

class SecurityTypeRequest: Request {
    
    override var path: String {
        "/project-service/project-overview/security-type"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json",
          "Authorization" : "Bearer \(session.accessToken)" ]
    }
    
    override var queryItems: [URLQueryItem]? {
        let statuses: [ProjectStatus] = [.comingSoon,
                                         .active,
                                         .finished,
                                         .waitBlockchain,
                                         .refunded,
                                         .manualRelease,
                                         .waitRelease,
                                         .tokensTransferred]
        return [ URLQueryItem(name: "projectStatuses", value: statuses.map { $0.rawValue }.joined(separator: ",")) ]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let types = try JSONDecoder().decode([String].self, from: data)
        self.session.projectsFilterProvider.availableSecurityTypes = types
    }
    
}
