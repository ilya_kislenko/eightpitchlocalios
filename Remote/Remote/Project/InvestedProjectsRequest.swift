//
//  InvestedProjectsRequest.swift
//  Remote
//
//  Created by 8pitch on 10/2/20.
//

import Foundation
import Input

class InvestedProjectRequest: Request {
    override var path: String {
        "/project-service/investors/projects"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        ["Authorization" : "Bearer \(self.session.accessToken)",
         "Content-Type" : "application/json"]
    }
    
    override init(session: Session) {
        super.init(session: session)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let projects = try JSONDecoder().decode([InvestedProjectsResponse].self, from: data)
        self.session.projectsProvider.investedProjects = projects.map { Input.Project($0.projectInfo) }
    }
    
}
