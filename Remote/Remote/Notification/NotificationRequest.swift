//
//  NotificationRequest.swift
//  Remote
//
//  Created by 8pitch on 9/11/20.
//

import Foundation
import Input

public struct NotificationResponse : Codable {
    
    struct ProjectNotification: Codable {
        public var id : Int?
        public var createDateTime : String?
        public var notificationStatus : String?
        public var notificationType: String?
        public var notificationText: String?
    }
    
    var notifications : [ProjectNotification]
    var totalNotifications : Int
    
}

class NotificationRequest: Request {
    
    override var path: String {
        "/user-service/users/notifications"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json",
          "Authorization" : "Bearer \(session.accessToken)" ]
    }
    
    override var queryItems: [URLQueryItem]? {
        [ URLQueryItem(name: "pageSize", value: "\(Constants.size)") ]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        if data.isEmpty {
            return // no-op
        }
        let notification = try JSONDecoder().decode(NotificationResponse.self, from: data)
        self.session.notificationsProvider.notifications = notification.notifications.map { Input.ProjectNotification($0) }.filter( {
            
            return $0.identifier != nil && $0.type != .unexpected
        } )
    }
    
    enum Constants {
        static let size: Int = 999
    }
}

extension Input.ProjectNotification {
    convenience init(_ notification: NotificationResponse.ProjectNotification?) {
        self.init(id: notification?.id, createDateTime: notification?.createDateTime, status: notification?.notificationStatus, type: notification?.notificationType, text: notification?.notificationText)
    }
}
