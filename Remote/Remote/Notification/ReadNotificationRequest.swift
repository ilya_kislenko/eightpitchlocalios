//
//  ReadNotificationRequest.swift
//  Remote
//
//  Created by 8pitch on 9/14/20.
//

import Foundation
import Input

class ReadNotificationRequest : Request {
    
    private var notificationID : Int
    
    override var path: String {
        "/user-service/users/notifications/\(notificationID)"
    }
    
    override var method: String {
        "PUT"
    }
    
    init(session: Session, notificationID: Int) {
        self.notificationID = notificationID
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }

}
