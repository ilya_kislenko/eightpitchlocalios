//
//  LoginRequest.swift
//  Remote
//
//  Created by 8pitch on 28.07.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

enum LoginResult: String {
    case success = "SUCCESS"
    case failed = "UNSUCCESS"
    case inactive = "INACTIVE"
}

public enum LoginError : RequestError {
    
    case wrongPassword
    case inactive
    
    public var errorDescription : String? {
        switch self {
        case .wrongPassword:
            return "remote.error.response.login.wrongPassword".localized
        case .inactive:
            return "remote.error.response.login.inactive".localized
        }
    }
    
}

struct LoginReguestBody : Codable {
    
    var deviceID : String
    var password : String
    var phone: String
    var twoFactorAuthCode: String
    
    enum CodingKeys : String, CodingKey {
        case deviceID = "deviceId"
        case password
        case phone
        case twoFactorAuthCode
    }
    
}

struct Token : Codable {
    
    var accessToken : String
    var refreshToken : String
}

struct LoginResponseBody : Codable {
    
    var tokenValidationResult: ValidationResult {
        ValidationResult(rawValue: self.validationResult ?? "") ?? .invalid
    }
    
    var isSuccess: Bool {
        LoginResult(rawValue: self.result ?? "") == .success
    }
    
    var loginResult: LoginResult {
        LoginResult(rawValue: self.result ?? "UNSUCCESS") ?? .failed
    }
    
    var isValid: Bool {
        self.tokenValidationResult == .valid
    }
    
    var verificationTypes : [String]?
    var valid : Bool
    var attempts : Int?
    var interactionID : String?
    var result: String?
    var token : Token?
    var validationResult : String?
    var otp : Bool?
    var secondaryPhone : String?
    
    enum CodingKeys : String, CodingKey {
        case interactionID = "interactionId"
        case verificationTypes = "verificationTypes"
        case valid = "valid"
        case token = "tokenResponse"
        case result = "result"
        case attempts = "remainingAttempts"
        case validationResult = "validationResult"
        case otp = "otp"
        case secondaryPhone = "secondaryPhone"
    }
    
}

class LoginRequest : RequestRx {
    
    override var path : String {
        "/user-service/mobile/login"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = LoginReguestBody(deviceID: self.session.deviceID,
                                    password: self.session.password,
                                    phone: self.session.phone,
                                    twoFactorAuthCode: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json",
          "Authorization" : "Basic \(self.authHeader)" ]
    }
    
    override func processResponse(response: HTTPURLResponse?) throws {
        guard let response = response else {
            return
        }
        if response.statusCode == 401 {
            throw LoginError.wrongPassword
        }
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(LoginResponseBody.self, from: data)
        let verificationTypes = parsed.verificationTypes?.compactMap { VerificationType(rawValue: $0) }
        self.session.secondaryPhone = parsed.secondaryPhone
        self.session.verificationTypes = verificationTypes ?? []
        self.session.remainingAttemptsProvider.onNext(parsed.attempts)
        try? self.session.loginProvider.value().otp = parsed.otp ?? false
        if self.session.codeLimitReached && parsed.interactionID?.isEmpty ?? true {
            throw SendCodeError.limit
        }
        if parsed.loginResult == .inactive {
            throw LoginError.inactive
        }
        if !parsed.valid {
            throw LoginError.wrongPassword
        }
        self.session.interactionIDProvider.onNext(parsed.interactionID)
        if let accessToken = parsed.token?.accessToken,
           let refreshToken = parsed.token?.refreshToken {
            self.session.accessTokenProvider.onNext(accessToken)
            self.session.refreshTokenProvider.onNext(refreshToken)
            self.saveTokens()
            self.session.loginState = .hasMetadata
        }
    }
    
}
