//
//  LogoutRequest.swift
//  Remote
//
//  Created by 8pitch on 29.07.2020.
//

import Foundation
import Input
import RxSwift
import RxCocoa

struct LogoutResponseBody : Codable {
    
    var body: Data
    var statusCode: String
    var statusCodeValue: Int
    
}

class LogoutRequest : RequestRx {
    
    override var path : String {
        "/user-service/logout"
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"
        ]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        self.session.loginState = .none
    }
    
}
