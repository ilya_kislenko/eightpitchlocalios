//
//  Voting.swift
//  Remote
//
//  Created by 8pitch on 05.03.2021.
//

import Input

extension Input.Voting {
    convenience init(response: VotingResponse) {
        let trustee = response.trustee == nil ? nil : Input.Trustee(response: response.trustee!)
        self.init(agenda: response.agenda,
                  createDateTime: response.createDateTime,
                  createdBy: response.createdBy,
                  description: response.description,
                  endDate: response.endDate,
                  id: response.id,
                  informationCollectionPeriodEndDate: response.informationCollectionPeriodEndDate,
                  informationCollectionPeriodStartDate: response.informationCollectionPeriodStartDate,
                  investorTrusteeDecision: response.investorTrusteeDecision,
                  projectInfo: Input.VotingProjectInfo(response: response.projectInfo),
                  questions: response.questions.compactMap {
                    guard let question = $0 else { return nil }
                    return Input.Question(response: question)
                  },
                  sendResultsToParticipants: response.sendResultsToParticipants ?? false,
                  shareResultsWithTrustee: response.shareResultsWithTrustee ?? false,
                  smsConfirmation: Input.SmsConfirmation(response: response.smsConfirmation),
                  startDate: response.startDate,
                  status: response.status,
                  streamAgenda: response.streamInfo?.streamAgenda,
                  streamEndDate: response.streamInfo?.streamEndDate,
                  streamStartDate: response.streamInfo?.streamStartDate,
                  streamTheme: response.streamInfo?.streamTheme,
                  trustee: trustee,
                  trusteeEnabled: response.trusteeEnabled ?? false,
                  trusteeExternalId: response.trusteeExternalId,
                  type: response.type,
                  userVotingDate: response.userVotingDate,
                  usersVotingStatus: response.usersVotingStatus)
    }
}

extension Input.Question {
    convenience init(response: Question) {
        self.init(id: response.id,
                  maximumAnswers: response.maximumAnswers,
                  minimumAnswers: response.minimumAnswers,
                  options: response.options.compactMap {
                    guard let option = $0 else { return nil }
                    return Input.Option(response: option)
                  },
                  title: response.title,
                  type: response.type)
    }
}

extension Input.Option {
    convenience init(response: Option) {
        self.init(answer: response.answer, id: response.id)
    }
}

extension Input.SmsConfirmation {
    convenience init(response: SmsConfirmation?) {
        self.init(interactionId: response?.interactionId, token: response?.token)
    }
}

extension Input.Trustee {
    convenience init(response: Trustee) {
        self.init(firstName: response.firstName,
                  lastName: response.lastName,
                  prefix: response.prefix)
    }
}

extension Input.Answer {
    convenience init(response: AnswerResponse) {
        self.init(answers: response.answers.compactMap { Input.Option(response: $0) },
                  question: Input.Question(response: response.question))
    }
}

extension Input.Percent {
    convenience init(response: Percent?) {
        self.init(investors: response?.investors, tokens: response?.tokens)
    }
}

extension Input.VotingResult {
    convenience init(response: VotingResultResponse?) {
        self.init(coveragePerInvestors: response?.coveragePerInvestors,
                  coveragePerTokens: response?.coveragePerTokens,
                  questions: response?.questions.compactMap {
                    Input.QuestionResult(question: $0.key, result: $0.value.compactMapValues { percent in
                        return Input.Percent(response: percent)
                    })
                  } ?? [])
    }
}

extension Input.VotingProjectInfo {
    convenience init(response: ProjectInfo) {
        self.init(companyName: response.companyName,
                  legalFormType: response.legalFormType,
                  projectId: response.projectId)
    }
}
