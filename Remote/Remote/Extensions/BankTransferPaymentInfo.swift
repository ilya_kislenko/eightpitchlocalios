//
//  BankTransferPaymentInfo.swift
//  Remote
//
//  Created by 8pitch on 9/9/20.
//

import Input

extension Input.BankTransferPaymentInfo {
    init(response: BankTransferInfoResponse?) {
        self.init(bic: response?.bic, iban: response?.iban, transactionInfo: Input.TransactionInfo(info: response?.transactionInfo))
    }
}

extension Input.BankTransferPaymentInfo {
    init(response: SecupayBankTransferInfoResponse?) {
        self.init(bic: response?.bic, iban: response?.iban, transactionInfo: Input.TransactionInfo(info: response?.transactionInfo))
    }
}
