//
//  ProjectInvestmentInfo.swift
//  Remote
//
//  Created by 8pitch on 9/2/20.
//

import Input

extension Input.ProjectInvestmentInfo {
    convenience init(projectInfo: ProjectInvestmentInfoResponse?) {
        self.init(assetBasedFees: projectInfo?.assetBasedFees,
                  companyAddress: projectInfo?.companyAddress,
                  companyName: projectInfo?.companyName,
                  endTime: projectInfo?.endTime,
                  financingPurpose: projectInfo?.financingPurpose,
                  investmentStepSize: projectInfo?.investmentStepSize,
                  investorClassLimit: projectInfo?.investorClassLimit,
                  investmentSeries: projectInfo?.investmentSeries,
                  isin: projectInfo?.isin,
                  maximumInvestmentAmount: projectInfo?.maximumInvestmentAmount,
                  minimumInvestmentAmount: projectInfo?.minimumInvestmentAmount,
                  nominalValue: projectInfo?.nominalValue,
                  shortCut: projectInfo?.shortCut,
                  startTime: projectInfo?.startTime,
                  typeOfSecurity: projectInfo?.typeOfSecurity,
                  unprocessedInvestment: UnprocessedInvestment(unprocessedInvestment: projectInfo?.unprocessedInvestment))
    }
}
