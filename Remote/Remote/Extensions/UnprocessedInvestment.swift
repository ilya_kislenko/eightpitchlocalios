//
//  UnprocessedInvestment.swift
//  Remote
//
//  Created by 8pitch on 9/2/20.
//

import Input

extension Input.UnprocessedInvestment {
    convenience init(unprocessedInvestment: UnprocessedInvestmentResponse?) {
        self.init(amount: unprocessedInvestment?.amount,
                  investmentId: unprocessedInvestment?.investmentId,
                  investmentStatus: unprocessedInvestment?.investmentStatus)
    }
}
