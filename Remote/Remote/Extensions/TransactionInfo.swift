//
//  TransactionInfo.swift
//  Remote
//
//  Created by 8pitch on 9/9/20.
//

import Input

extension Input.TransactionInfo {
    init(info: BankTransferInfoResponse.TransactionInfo?) {
        self.init(referenceNumber: info?.referenceNumber, responseStatus: info?.responseStatus)
    }
}

extension Input.TransactionInfo {
    init(info: SecupayBankTransferInfoResponse.TransactionInfo?) {
        self.init(referenceNumber: info?.referenceNumber, responseStatus: info?.responseStatus)
    }
}

