//
//  UserTransfer.swift
//  Remote
//
//  Created by 8pitch on 1/11/21.
//

import Input

extension Input.UserTransfer {
    convenience init(_ response: TransferResponse?) {
        self.init(amount: NSDecimalNumber(floatLiteral: response?.amount ?? 0), companyAddress: response?.companyAddress, companyName: response?.companyName,
                  isin: response?.isin, transactionDate: response?.transferDate, fromUser: Input.TransferUser(response?.fromUser), toUser: Input.TransferUser(response?.toUser), shortCut: response?.shortCut, id: response?.id)
    }
}

extension Input.TransferUser {
    convenience init(_ response: TransferUser?) {
        self.init(prefixFull: response?.prefixFull, firstName: response?.firstName, lastName: response?.lastName)
    }
}

