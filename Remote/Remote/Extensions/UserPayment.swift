//
//  UserPayment.swift
//  Remote
//
//  Created by 8pitch on 9/30/20.
//

import Input

extension Input.UserPayment {
    convenience init(_ response: PaymentResponse?) {
        self.init(amount: NSDecimalNumber(floatLiteral: response?.amount ?? 0), companyAddress: response?.companyAddress,
                  companyName: response?.companyName, isin: response?.isin,
                  monetaryAmount: NSDecimalNumber(floatLiteral: response?.monetaryAmount ?? 0), paymentSystem: response?.paymentSystem,
                  transactionDate: response?.transactionDate, transactionID: response?.transactionId, shortCut: response?.shortCut ?? "", investmentID: response?.investmentId)
    }
}
