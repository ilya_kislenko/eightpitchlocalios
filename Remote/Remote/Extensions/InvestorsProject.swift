//
//  InvestorsProject.swift
//  Remote
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

extension Input.InvestorsProject {
    convenience init(project: InvestorsProjectResponse?) {
        self.init(investmentsInfo: Input.InvestorsInvestmentsInfo(item: project?.investmentsInfo),
                  projectFiles: project?.projectFiles.map{ Input.InvestorsProjectFile(item: $0) } ?? [],
                  projectInfo: Input.InvestorsProjectInfo(item: project?.projectInfo),
                  projectStatus: project?.projectStatus)
    }
}

extension Input.InvestorsInvestmentsInfo {
    convenience init(item: InvestorsInvestmentsInfo?) {
        self.init(investments: item?.investments.map{ Input.InvestorsInvestment(item: $0) } ?? [],
                  lastInvestmentStatus: item?.lastInvestmentStatus,
                  totalAmountOfTokens: item?.totalAmountOfTokens)
    }
}

extension Input.InvestorsInvestment {
    convenience init(item: InvestorsInvestment?) {
        self.init(amount: item?.amount,
                  createDateTime: item?.createDateTime,
                  status: item?.status)
    }
}

extension Input.InvestorsProjectFile {
    convenience init(item: InvestorsProjectFile?) {
        self.init(file: Input.InvestorsProjectFileFile(item: item?.file),
                  fileType: item?.fileType,
                  identifier: item?.id)
    }
}

extension Input.InvestorsProjectFileFile {
    convenience init(item: InvestorsProjectFileFile?) {

        self.init(customFileName: item?.customFileName,
                  identifier: item?.id,
                  originalFileName: item?.originalFileName,
                  uniqueFileName: item?.uniqueFileName)
    }
}

extension Input.InvestorsProjectInfo {
    convenience init(item: InvestorsProjectInfo?) {
        self.init(companyName: item?.companyName,
                  projectInfoDescription: item?.projectInfoDescription,
                  financialInformation: Input.InvestorsFinancialInformation(item: item?.financialInformation),
                  graph: Input.InvestorsGraph(item: item?.graph),
                  identifier: item?.id,
                  projectPageFiles: item?.projectPageFiles.map{ Input.InvestorsProjectPageFile(item: $0)} ?? [],
                  projectPageURL: item?.projectPageURL,
                  tokenParametersDocument: Input.InvestorsTokenParametersDocument(item: item?.tokenParametersDocument),
                  videoURL: item?.videoURL)
    }

}

extension Input.InvestorsFinancialInformation {
    convenience init(item: InvestorsFinancialInformation?) {
        self.init(investmentSeries: item?.investmentSeries,
                  nominalValue: item?.nominalValue,
                  typeOfSecurity: item?.typeOfSecurity)
    }
}

extension Input.InvestorsGraph {
    convenience init(item: InvestorsGraph?) {
        self.init(currentFundingSum: item?.currentFundingSum,
                  currentFundingSumInTokens: item?.currentFundingSumInTokens,
                  hardCap: item?.hardCap,
                  hardCapInTokens: item?.hardCapInTokens,
                  softCap: item?.softCap,
                  softCapInTokens: item?.softCapInTokens)
    }
}

extension Input.InvestorsProjectPageFile {
    convenience init(item: InvestorsProjectPageFile?) {

        self.init(file: Input.InvestorsProjectPageFileFile(item: item?.file),
                  fileType: item?.fileType,
                  identifier: item?.id)
    }
}

extension Input.InvestorsProjectPageFileFile {
    convenience init(item: InvestorsProjectPageFileFile?) {
        self.init(customFileName: item?.customFileName,
                  encrypted: item?.encrypted,
                  identifier: item?.id,
                  originalFileName: item?.originalFileName,
                  uniqueFileName: item?.uniqueFileName)
    }
}

extension Input.InvestorsTokenParametersDocument {
    convenience init(item: InvestorsTokenParametersDocument?) {
        self.init(dsoProjectFinishDate: item?.dsoProjectFinishDate,
                  dsoProjectStartDate: item?.dsoProjectStartDate,
                  hardCap: item?.hardCap,
                  minimumInvestmentAmount: item?.minimumInvestmentAmount,
                  nominalValue: item?.nominalValue)
    }
}
