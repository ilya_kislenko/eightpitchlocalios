//
//  InvestorsPaymentsGraph.swift
//  Remote
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

extension Input.InvestorsPaymentsGraph {
    convenience init(item: InvestorsGraphResponse?) {
        self.init(investments: item?.investments.compactMap { Input.InvestorsInvestmentElement(item: $0)}.sorted() { $0.monetaryAmount ?? 0 > $1.monetaryAmount ?? 0 } ?? [],
                  totalMonetaryAmount: item?.totalMonetaryAmount)
    }
}

extension Input.InvestorsInvestmentElement {
    convenience init(item: InvestorsInvestmentElement?) {
        self.init(companyName: item?.companyName,
                  monetaryAmount: item?.monetaryAmount)
    }
}
