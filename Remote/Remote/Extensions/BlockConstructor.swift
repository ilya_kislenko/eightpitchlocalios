//
//  BlockConstructor.swift
//  Remote
//
//  Created by 8pitch on 9/21/20.
//

import Input

extension Input.ExpandedProject {
    convenience init(project: ProjectResponse?, currentLanguage: String?) {
        self.init(additionalFields: AdditionalFields(fields: project?.additionalFields), companyName : project?.companyName, customAdditionalFields: project?.customAdditionalFields.map { CustomAdditionalField(field: $0) },
                  domain : project?.domain, financialInformation: Input.ExpandedProject.FinancialInformation(info: project?.financialInformation), geoipRestrictions: project?.geoipRestrictions,
                  graph: Graph(project?.graph), id : project?.id, legalForm : project?.legalForm, paymentConfiguration: PaymentConfiguration(configuration: project?.paymentConfiguration),
                  possibilityAccept : project?.possibilityAccept, projectDescription : project?.projectDescription, projectExternalId : project?.projectExternalId,
                  projectFiles: project?.projectFiles.map { ProjectFile(file: $0) }, projectInitiator: ProjectInitiator(initiator: project?.projectInitiator), projectInitiatorExternalId : project?.projectInitiatorExternalId,
                  projectPage: ProjectPage(page: project?.projectPage, currentLanguage: currentLanguage), projectStatus : project?.projectStatus, tokenId : project?.tokenId,
                  tokenParametersDocument: TokenParametersDocument(project?.tokenParametersDocument),
                  totalNumberOfTokens: project?.totalNumberOfTokens,
                  whitelistedUserExternalIds: project?.whitelistedUserExternalIds)
    }
}

extension Input.ExpandedProject.FinancialInformation {
    convenience init(info: ProjectResponse.FinancialInformation?) {
        self.init(companyAddress : info?.companyAddress, country : info?.country, financingPurpose : info?.financingPurpose,
                  goal : info?.goal, guarantee : info?.guarantee, id : info?.id,
                  investmentSeries : info?.investmentSeries, isin : info?.isin, nominalValue : info?.nominalValue,
                  threshold : info?.threshold, typeOfSecurity : info?.typeOfSecurity)
    }
}

extension Input.AdditionalFields {
    convenience init(fields: ProjectResponse.AdditionalFields?) {
        self.init(agio : fields?.agio, conversionRight : fields?.conversionRight, dividend : fields?.dividend,
                  futureShareNominalValue : fields?.futureShareNominalValue, highOfDividend : fields?.highOfDividend, id : fields?.id,
                  initialSalesCharge : fields?.initialSalesCharge, interest : fields?.interest, interestInterval : fields?.interestInterval,
                  issuedShares : fields?.issuedShares, typeOfRepayment : fields?.typeOfRepayment, wkin : fields?.wkin)
    }
}

extension Input.CustomAdditionalField {
    convenience init(field: ProjectResponse.CustomAdditionalField?) {
        self.init(fieldName : field?.fieldName, fieldText : field?.fieldText, id : field?.id)
    }
}

extension Input.PaymentConfiguration {
    convenience init(configuration: ProjectResponse.PaymentConfiguration?) {
        self.init(accountHolderName : configuration?.accountHolderName, bic : configuration?.bic,
                  fintecsystemApiKey : configuration?.fintecsystemApiKey, iban : configuration?.iban,
                  id : configuration?.id, klarnaApiKey : configuration?.klarnaApiKey,
                  klarnaCustomerNumber : configuration?.klarnaCustomerNumber, klarnaProjectId : configuration?.klarnaProjectId)
    }
}

extension Input.ProjectFile {
    convenience init(file: ProjectResponse.ProjectFile?) {
        self.init(file: File(file: file?.file), fileType : file?.fileType, id : file?.id)
    }
}

extension Input.File {
    convenience init(file: ProjectResponse.File?) {
        self.init(customFileName : file?.customFileName, id : file?.id,
                  originalFileName : file?.originalFileName, uniqueFileName : file?.uniqueFileName)
    }
}

extension Input.ProjectPage {
    convenience init(page: ProjectResponse.ProjectPage?, currentLanguage: String?) {
        self.init(companyName : page?.companyName, externalId : page?.externalId,
                  firstInvestmentArguments : page?.firstInvestmentArguments,
                  firstQuoteTitle : page?.firstQuoteTitle, id : page?.id,
                  projectPageFiles: page?.projectPageFiles.map { ProjectFile(file: $0) },
                  projectPageRejections: page?.projectPageRejections.map { ProjectPageRejection(rejection: $0) },
                  projectPageStatus : page?.projectPageStatus,
                  promoVideo : page?.promoVideo,
                  quoteText : page?.quoteText,
                  secondInvestmentArguments : page?.secondInvestmentArguments,
                  secondQuoteTitle : page?.secondQuoteTitle,
                  slogan : page?.slogan,
                  thirdInvestmentArguments : page?.thirdInvestmentArguments,
                  url : page?.url,
                  blockConstructor: Input.BlockConstructor.init(tabs: page?.blockConstructor?.tabs, fileIdMap: page?.blockConstructor?.fileIdMap ?? [:], i18nData: page?.blockConstructor?.i18nData, currentLanguage: currentLanguage))
    }
}

extension Input.ProjectPageRejection {
    convenience init(rejection: ProjectResponse.ProjectPageRejection?) {
        self.init(comment : rejection?.comment, createDate : rejection?.createDate, id : rejection?.id)
    }
}

extension Input.ProjectInitiator {
    convenience init(initiator: ProjectResponse.ProjectInitiator?) {
        self.init(email: initiator?.email, firstName: initiator?.firstName, lastName : initiator?.lastName,
                  phone : initiator?.phone, prefix : Title(rawValue: initiator?.prefix ?? ""))
    }
}


extension Input.BlockConstructor {
    convenience init(tabs: [ProjectResponse.ProjectPageTab?]?, fileIdMap: [String?: String?], i18nData: ProjectResponse.LanguageData?, currentLanguage: String?) {
        let currentMap = currentLanguage == "en" ? i18nData?.i18nMaps?.en : i18nData?.i18nMaps?.de
        let initialMap = currentLanguage == "en" ? i18nData?.i18nMaps?.de : i18nData?.i18nMaps?.en
        let map = (current: currentMap ?? [:], initial: initialMap ?? [:])
        self.init(tabs: tabs?.map { Input.ProjectPageTab(tab: $0, fileIdMap: fileIdMap, i18nData: map) }, fileIdMap: fileIdMap, languageMap: map)
    }
}

extension Input.ProjectPageTab {
    convenience init(tab: ProjectResponse.ProjectPageTab?, fileIdMap: [String?: String?], i18nData: Input.LanguageMap?) {
        let blocks = tab?.blocks.map { Input.ProjectPageBlock(block: $0, fileIdMap: fileIdMap, i18nData: i18nData) } ?? []
        self.init(id: tab?.id,
                  name: tab?.name,
                  blocks: blocks.filter { $0.payload != nil },
                  payload: Input.LayoutRootIdList(layoutRootIdList: tab?.payload?.layoutRootIdList ?? [], nameTranslationCode: tab?.payload?.nameTranslationCode),
                  fileIdMap: fileIdMap,
                  languageMap: i18nData
        )
    }
}

extension Input.ProjectPageBlock {
    convenience init(block: ProjectResponse.ProjectPageBlock?, fileIdMap: [String?: String?], i18nData: Input.LanguageMap?) {
        var payload: ProjectPageBlockPayload?
        switch block?.type {
        case ProjectPageBlockType.text.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.TextBlockPayload
            payload = Input.TextBlockPayload.init(text: blockPayload?.text, headingSize: blockPayload?.headingSize, color: blockPayload?.color, fontSize: blockPayload?.fontSize, justify: blockPayload?.justify, bold: blockPayload?.bold, italic: blockPayload?.italic, underline: blockPayload?.underline, fontFamily: blockPayload?.fontFamily, header: blockPayload?.header, preHeader: blockPayload?.preHeader)
        case ProjectPageBlockType.image.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.ImageBlockPayload
            payload = Input.ImageBlockPayload.init(useText: blockPayload?.useText, imageId: blockPayload?.imageId, fullWidth: blockPayload?.fullWidth, imageFileName: blockPayload?.imageFileName, text: blockPayload?.text, color: blockPayload?.color, fontSize: blockPayload?.fontSize, justify: blockPayload?.justify, bold: blockPayload?.bold, italic: blockPayload?.italic, underline: blockPayload?.underline, fontFamily: blockPayload?.fontFamily, header: blockPayload?.header, preHeader: blockPayload?.preHeader, headingSize: blockPayload?.headingSize)
        case ProjectPageBlockType.textAndImage.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.TextAndImageBlockPayload
            payload = Input.TextAndImageBlockPayload.init(imageId: blockPayload?.imageId, imageFileName: blockPayload?.imageFileName, textPosition: blockPayload?.textPosition, header: blockPayload?.header, imageDescription: blockPayload?.imageDescription, text: blockPayload?.text)
        case ProjectPageBlockType.FAQ.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.FAQBlockPayload
            if blockPayload?.language == Locale.current.languageCode {
                payload = Input.FAQBlockPayload.init(header: blockPayload?.header, items: (blockPayload?.items.map{(Input.FAQItem.init(question: $0?.question, answer: $0?.answer, text: $0?.text))}) ?? [])
            } else {
                break
            }
        case ProjectPageBlockType.list.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.ListBlockPayload
            payload = Input.ListBlockPayload.init(header: blockPayload?.header, items: blockPayload?.items.map{(Input.ListBlockPayloadItem(text: $0?.text))} ?? [])
        case ProjectPageBlockType.table.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.TableBlockPayload
            payload = Input.TableBlockPayload.init(rowsNumber: blockPayload?.rowsNumber, colsNumber: blockPayload?.colsNumber, headerColor: blockPayload?.headerColor, rows: blockPayload?.rows ?? [[]])
        case ProjectPageBlockType.icons.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.IconsBlockPayload
            payload = Input.IconsBlockPayload.init(perLine: blockPayload?.perLine, items: blockPayload?.items.map{(Input.IconsBlockPayloadIconItem.init(imageId: $0?.imageId, imageFileName: $0?.imageFileName, link: $0?.link))} ?? [])
        case ProjectPageBlockType.video.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.VideoBlockPayload
            payload = Input.VideoBlockPayload.init(vimeoId: blockPayload?.vimeoId, justify: blockPayload?.justify, useText: blockPayload?.useText, header: blockPayload?.header, text: blockPayload?.text)
        case ProjectPageBlockType.divider.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.DividerBlockPayload
            payload = Input.DividerBlockPayload.init(height: blockPayload?.height)
        case ProjectPageBlockType.facts.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.FactsBlockPayload
            payload = Input.FactsBlockPayload.init(perLine: blockPayload?.perLine, items: blockPayload?.items.map{(Input.FactsBlockPayloadItem.init(imageId: $0?.imageId, imageFileName: $0?.imageFileName, text: $0?.text, header: $0?.header))} ?? [])
        case ProjectPageBlockType.tilesImages.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.TilesImagesBlockPayload
            payload = Input.TilesImagesBlockPayload.init(perLine: blockPayload?.perLine, bold: blockPayload?.bold, italic: blockPayload?.italic, underline: blockPayload?.underline, items: blockPayload?.items.map { Input.TilesImagesBlockPayloadItem(imageId: $0?.imageId, imageFileName: $0?.imageFileName, header: $0?.header, text: $0?.text) } ?? [])
        case ProjectPageBlockType.dualColumnImages.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.DualColumnImagesBlockPayload
            payload = Input.DualColumnImagesBlockPayload.init(leftImageId: blockPayload?.leftImageId, leftImageFileName: blockPayload?.leftImageFileName, leftText: blockPayload?.leftText, rightImageId: blockPayload?.rightImageId, rightImageFileName: blockPayload?.rightImageFileName, rightText: blockPayload?.rightText, leftHeader: blockPayload?.leftHeader, rightHeader: blockPayload?.rightHeader)
        case ProjectPageBlockType.imageLink.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.ImageLinkBlockPayload
            payload = Input.ImageLinkBlockPayload.init(perLine: blockPayload?.perLine, items: blockPayload?.items.map { Input.ImageLinkBlockPayloadItem(imageUrl: $0?.imageUrl, imageFileName: $0?.imageFileName, link: $0?.link, description: $0?.description) } ?? [], isLinkAdded: blockPayload?.isLinkAdded, isDescriptionAdded: blockPayload?.isDescriptionAdded)
        case ProjectPageBlockType.dualColumnText.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.DualColumnTextBlockPayload
            payload = Input.DualColumnTextBlockPayload.init(leftText: blockPayload?.leftText, rightText: blockPayload?.rightText, leftHeader: blockPayload?.leftHeader, rightHeader: blockPayload?.rightHeader)
        case ProjectPageBlockType.button.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.ButtonBlockPayload
            payload = Input.ButtonBlockPayload(btnColor: blockPayload?.btnColor, textColor: blockPayload?.textColor, text: blockPayload?.text, link: blockPayload?.link)
        case ProjectPageBlockType.disclaimer.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.DisclaimerBlockPayload
            payload = Input.DisclaimerBlockPayload(iconUrl: blockPayload?.iconUrl, iconFileName: blockPayload?.iconFileName, text: blockPayload?.text, background: blockPayload?.background)
        case ProjectPageBlockType.linkedText.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.LinkedTextBlockPayload
            if blockPayload?.language == Locale.current.languageCode {
                payload = Input.LinkedTextBlockPayload(text: blockPayload?.text, color: blockPayload?.color,
                                                       fontSize: blockPayload?.fontSize, justify: blockPayload?.justify,
                                                       bold: blockPayload?.bold ?? false, italic: blockPayload?.italic ?? false,
                                                       underline: blockPayload?.underline ?? false, fontFamily: blockPayload?.fontFamily,
                                                       wordsLink: blockPayload?.wordsLink.map { Input.LinkedTextBlockWordLink(linkUrl: $0?.linkUrl, position: $0?.position, text: $0?.text, label: $0?.label) } ?? [])
            } else {
                break
            }
        case ProjectPageBlockType.department.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.DepartmentBlockPayload
            if blockPayload?.language == Locale.current.languageCode {
                payload = Input.DepartmentBlockPayload(header: blockPayload?.header, items: blockPayload?.items.map { Input.DepartmentBlockPayloadItem(fullName: $0?.fullName, position: $0?.position, linkedinUrl: $0?.linkedinUrl, imageUrl: $0?.imageUrl, imageFileName: $0?.imageFileName) } ?? [])
            } else {
                break
            }
        case ProjectPageBlockType.header.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.HeaderBlockPayload
            payload = Input.HeaderBlockPayload(text: blockPayload?.text, headingSize: blockPayload?.headingSize, preHeader: blockPayload?.preHeader)
        case ProjectPageBlockType.chart.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.ChartBlockPayload
            payload = Input.ChartBlockPayload(perLine: blockPayload?.perLine, items: blockPayload?.items.map { Input.ChartBlockPayloadItem(title: $0?.title, chartType: $0?.chartType, data: $0?.data ?? [], text: $0?.text)} ?? [])
        case ProjectPageBlockType.delimiter.rawValue:
            let blockPayload = block?.payload as? ProjectResponse.LineDelimiterBlockPayload
            payload = Input.LineDelimiterBlockPayload(above: blockPayload?.above, below: blockPayload?.below)
        default:
            break
        }
        
        self.init(id: block?.id,
                  type: block?.type,
                  payload: payload,
                  children: block?.children ?? [],
                  meta: Input.ProjectPageBlockMeta(header: block?.meta?.header, previewImage: block?.meta?.previewImage),
                  fileIdMap: fileIdMap,
                  languageMap: i18nData)
    }
}
