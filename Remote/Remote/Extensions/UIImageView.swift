//
//  UIImageView.swift
//  Remote
//
//  Created by 8pitch on 9/27/20.
//

import SDWebImage
import Input

extension UIImageView {
    open func sd_setImage(with imageID: String,
                          session: Session,
                          placeholderImage placeholder: UIImage?,
                          completed completedBlock: SDExternalCompletionBlock? = nil) {
        let urlConfigurator = ImageRequestURLConfigurator(imageID: imageID)
        self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        SDWebImageDownloader.shared.setValue("image/png" , forHTTPHeaderField: "Content-Type")
        SDWebImageDownloader.shared.setValue("Bearer \(session.accessToken)", forHTTPHeaderField: "Authorization")
        self.sd_setImage(with: urlConfigurator.url, placeholderImage: placeholder, completed: completedBlock)
    }
}
