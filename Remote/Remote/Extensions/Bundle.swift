//
//  Bundle.swift
//  Remote
//
//  Created by 8pitch on 21.08.2020.
//

import Foundation

extension Bundle {
    
    var API_BASE_URL : String? {
        return self.object(forInfoDictionaryKey: #function) as? String
    }
    
    var WEB_BASE_URL : String? {
        return self.object(forInfoDictionaryKey: #function) as? String
    }
    
}
