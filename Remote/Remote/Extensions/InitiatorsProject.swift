//
//  InitiatorsProject.swift
//  Remote
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

extension Input.InitiatorsProject {
    convenience init(item: InitiatorsContent?) {
        self.init(companyName: item?.companyName,
                  graph: Input.InitiatorsGraph(item: item?.graph),
                  identifier: item?.id,
                  projectBI: Input.InitiatorsProjectBI(item: item?.projectBI),
                  projectDescription: item?.projectDescription,
                  projectFiles: item?.projectFiles.map{ Input.InitiatorsProjectFile(item: $0) } ?? [],
                  projectPage: Input.InitiatorsProjectPage(item: item?.projectPage),
                  projectStatus: item?.projectStatus,
                  tokenParametersDocument: Input.InitiatorsTokenParametersDocument(item: item?.tokenParametersDocument))
    }
}

extension Input.InitiatorsGraph {
    convenience init(item: InitiatorsGraph?) {
        self.init(currentFundingSum: item?.currentFundingSum,
                  currentFundingSumInTokens: item?.currentFundingSumInTokens,
                  hardCap: item?.hardCap,
                  hardCapInTokens: item?.hardCapInTokens,
                  softCap: item?.softCap,
                  softCapInTokens: item?.softCapInTokens)
    }
}

extension Input.InitiatorsProjectBI {

    convenience init(item: InitiatorsProjectBI?) {
        self.init(averageInvestment: item?.averageInvestment,
                  capitalInvested: item?.capitalInvested,
                  investors: Input.InitiatorsInvestors(item: item?.investors),
                  maxInvestment: item?.maxInvestment,
                  minInvestment: item?.minInvestment)
    }
}

extension Input.InitiatorsInvestors {

    convenience init(item: InitiatorsInvestors?) {
        self.init(totalCount: item?.totalCount)
    }
}

extension Input.InitiatorsProjectFile {

    convenience init(item: InitiatorsProjectFile?) {
        self.init(file: Input.InitiatorsFile(item: item?.file),
                  fileType: item?.fileType,
                  identifier: item?.id)
    }
}

extension Input.InitiatorsFile {

    convenience init(item: InitiatorsFile?) {
        self.init(customFileName: item?.customFileName,
                  identifier: item?.id,
                  originalFileName: item?.originalFileName,
                  uniqueFileName: item?.uniqueFileName)
    }
}

extension Input.InitiatorsProjectPage {
    convenience init(item: InitiatorsProjectPage?) {
        self.init(promoVideo: item?.promoVideo,
                  url: item?.url)
    }
}

extension Input.InitiatorsTokenParametersDocument {

    convenience init(item: InitiatorsTokenParametersDocument?) {
        self.init(dsoProjectFinishDate: item?.dsoProjectFinishDate,
                  dsoProjectStartDate: item?.dsoProjectStartDate,
                  nominalValue: item?.nominalValue)
    }
}
