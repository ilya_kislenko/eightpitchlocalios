//
//  SecupaySubmitPaymentRequest.swift
//  Remote
//
//  Created by 8pitch on 08.09.2020.
//

import Foundation
import Input

struct SecupaySubmitPaymentResponse: Codable {
    var success: Bool
}

struct SecupaySubmitPaymentBody: Codable {
    var bankAccount: BankAccount
}

struct BankAccount: Codable {
    public var bic: String
    public var iban: String
    public var owner: String?
    
    init(_ bankAccount: Input.BankAccount) {
        self.bic = bankAccount.bic
        self.iban = bankAccount.iban
        self.owner = bankAccount.owner
    }
}

class SecupaySubmitPaymentRequest : Request {
    
    private var investmentID: String
    private var bankAccount: Input.BankAccount
    override var path : String {
        "/project-service/secupay/payments/submit/\(self.investmentID)"
    }
    
    override var body: Data? {
        let body = SecupaySubmitPaymentBody(bankAccount: BankAccount(self.bankAccount))
        return try? JSONEncoder().encode(body)
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    init(session: Session, investmentID: String, bankAccount: Input.BankAccount) {
        self.investmentID = investmentID
        self.bankAccount = bankAccount
        super.init(session: session)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(SecupaySubmitPaymentResponse.self, from: data)
        if !response.success {
            throw GenericRequestError.invalidResponse(nil)
        }
    }
    
}
