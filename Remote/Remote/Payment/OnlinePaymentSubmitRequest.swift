//
//  OnlinePaymentsSubmitRequest.swift
//  Remote
//
//  Created by 8pitch on 9/8/20.
//

import Foundation
import Input

public enum OnlinePaymentsSubmitRedirectionURL: String {
    case confirm = "/payment/confirm"
    case abort = "/payment/abort"
}

struct OnlinePaymentsSubmitResponse: Codable {
    public var paymentUrl: String?
    public var responseStatus: String?
}

struct RedirectParamsSubmitRequestBody: Codable {
    var redirectParams: OnlinePaymentsSubmitRequestBody
}

struct OnlinePaymentsSubmitRequestBody: Codable {
    var abortUrl: String
    var successUrl: String
}

class OnlinePaymentsSubmitRequest : Request {
    
    private var currentInvestmentID: String
    override var path : String {
        "/project-service/fintecsystems/payments/submit/\(currentInvestmentID)"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let paymentBody = OnlinePaymentsSubmitRequestBody(abortUrl: OnlinePaymentsSubmitRedirectionURL.abort.rawValue, successUrl: OnlinePaymentsSubmitRedirectionURL.confirm.rawValue)
        let body = RedirectParamsSubmitRequestBody(redirectParams: paymentBody)
        return try? JSONEncoder().encode(body)
    }
    
    init(session: Session, currentInvestmentID: String) {
        self.currentInvestmentID = currentInvestmentID
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let submitResponse = try? JSONDecoder().decode(OnlinePaymentsSubmitResponse.self, from: data)
        let info = OnlinePaymentSubmitInfo(paymentUrl: submitResponse?.paymentUrl,
                                           responseStatus: submitResponse?.responseStatus)
        self.session.paymentProvider.onlinePaymentSubmitInfo = info
    }
    
}
