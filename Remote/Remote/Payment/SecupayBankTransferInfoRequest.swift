//
//  SecupayBankTransferInfoRequest.swift
//  Remote
//
//  Created by 8pitch on 10.09.2020.
//

import Foundation
import Input

struct SecupayBankTransferInfoResponse: Codable {
    
    
    var bic: String?
    var iban: String?
    var transactionInfo: TransactionInfo?
    
    struct TransactionInfo: Codable {
        var referenceNumber: String?
        var responseStatus: String?
    }
}

class SecupayBankTransferInfoRequest : Request {
    
    private var investmentID: String
    override var path : String {
        "/project-service/classic-bank-transfer/payments/secupay-bank-transfer-info/\(self.investmentID)"
    }
    
    override var method: String {
        "GET"
    }
    
    init(session: Session, investmentID: String) {
        self.investmentID = investmentID
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let secupayResponse = try JSONDecoder().decode(SecupayBankTransferInfoResponse.self, from: data)
        self.session.paymentProvider.bankTransferPaymentInfo = BankTransferPaymentInfo(response: secupayResponse)
    }
    
}
