//
//  ManualBankTransferInfoRequest.swift
//  Remote
//
//  Created by 8pitch on 9/8/20.
//

import Foundation
import Input

enum ResponseStatus: String {
    case success = "OK"
    case error = "PAYMENT_GATEWAY_ERROR"
}

struct BankTransferInfoResponse: Codable {
    
    var status: ResponseStatus {
        ResponseStatus(rawValue: self.transactionInfo?.responseStatus ?? "") ?? .error
    }
    
    var bic: String?
    var iban: String?
    var transactionInfo: TransactionInfo?
    
    struct TransactionInfo: Codable {
        var referenceNumber: String?
        var responseStatus: String?
    }
}

class ManualBankTransferInfoRequest : Request {
    
    private var investmentID: String
    override var path : String {
        "/project-service/classic-bank-transfer/payments/manual-bank-transfer-info/\(self.investmentID)"
    }
    
    override var method: String {
        "GET"
    }
    
    init(session: Session, investmentID: String) {
        self.investmentID = investmentID
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(BankTransferInfoResponse.self, from: data)
        self.session.paymentProvider.bankTransferPaymentInfo = BankTransferPaymentInfo(response: response)
        if response.status == .error {
            throw GenericRequestError.invalidResponse(nil)
        }
    }
    
}
