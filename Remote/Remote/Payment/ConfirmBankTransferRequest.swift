//
//  ConfirmBankTransferRequest.swift
//  Remote
//
//  Created by 8pitch on 9/8/20.
//

import Foundation
import Input

class ConfirmBankTransferRequest : Request {
    
    private var investmentID: String
    override var path : String {
        "/project-service/classic-bank-transfer/payments/confirm-bank-transfer/\(self.investmentID)"
    }
    
    override var method: String {
        "PUT"
    }
    
    init(session: Session, investmentID: String) {
        self.investmentID = investmentID
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
}
