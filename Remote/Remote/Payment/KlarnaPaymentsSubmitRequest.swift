//
//  KlarnaPaymentsSubmitRequest.swift
//  Remote
//
//  Created by 8pitch on 9/8/20.
//

import Foundation
import Input

class KlarnaPaymentsSubmitRequest : Request {
    
    private var currentInvestmentID: String
    override var path : String {
        "/project-service/klarna/payments/submit/\(currentInvestmentID)"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = OnlinePaymentsSubmitRequestBody(abortUrl: OnlinePaymentsSubmitRedirectionURL.abort.rawValue, successUrl: OnlinePaymentsSubmitRedirectionURL.confirm.rawValue)
        return try? JSONEncoder().encode(body)
    }
    
    init(session: Session, currentInvestmentID: String) {
        self.currentInvestmentID = currentInvestmentID
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let submitResponse = try? JSONDecoder().decode(OnlinePaymentsSubmitResponse.self, from: data)
        let info = OnlinePaymentSubmitInfo(paymentUrl: submitResponse?.paymentUrl,
                                           responseStatus: submitResponse?.responseStatus)
        self.session.paymentProvider.onlinePaymentSubmitInfo = info
    }
    
}
