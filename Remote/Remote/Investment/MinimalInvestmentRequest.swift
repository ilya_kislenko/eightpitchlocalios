//
//  MinimalInvestmentRequest.swift
//  Remote
//
//  Created by 8pitch on 19.01.21.
//

import Foundation
import Input

class MinimalInvestmentRequest: Request {
    
    override var path: String {
        "/project-service/project-overview/minimal-investment"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json",
          "Authorization" : "Bearer \(session.accessToken)" ]
    }
    
    override var queryItems: [URLQueryItem]? {
        [ URLQueryItem(name: "projectStatuses", value: [ProjectStatus.comingSoon.rawValue, ProjectStatus.active.rawValue, ProjectStatus.finished.rawValue, ProjectStatus.waitBlockchain.rawValue].joined(separator: ",")) ]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode([String: Bool].self, from: data)
        let minimalInvestments = response.filter({ $0.value }).map({$0.key}).sorted(by: < )
        self.session.projectsFilterProvider.availableInvestmentAmount = minimalInvestments
    }
    
}
