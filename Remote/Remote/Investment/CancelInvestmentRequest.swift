//
//  CancelInvestmentRequest.swift
//  Remote
//
//  Created by 8pitch on 9/2/20.
//

import Foundation
import Input

class CancelInvestmentRequest : Request {
    
    private var projectID: Int
    private var investmentID: String
    override var path : String {
        "/project-service/investments/\(self.projectID)/cancel/\(self.investmentID)"
    }
    
    override var method: String {
        "PUT"
    }
    
    init(session: Session, projectID: Int, investmentID: String) {
        self.projectID = projectID
        self.investmentID = investmentID
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
}

