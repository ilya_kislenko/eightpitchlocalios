//
//  DocumentsOnEmailRequest.swift
//  Remote
//
//  Created by 8pitch on 8/31/20.
//

import Foundation
import Input

class DocumentsOnEmailRequest : Request {
    
    private var projectID: Int
    private var investmentID: String
    override var path : String {
        "/project-service/investments/\(self.projectID)/send-project-documents-draft/\(self.investmentID)"
    }
    
    override var method: String {
        "POST"
    }
    
    init(session: Session, projectID: Int, investmentID: String) {
        self.projectID = projectID
        self.investmentID = investmentID
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
}
