//
//  ProjectInvestmentInfoRequest.swift
//  Remote
//
//  Created by 8pitch on 9/1/20.
//

import Input

struct ProjectInvestmentInfoResponse: Codable {
    public var assetBasedFees: Double?
    public var companyAddress: String?
    public var companyName: String?
    public var endTime: String?
    public var financingPurpose: String?
    public var investmentSeries: String?
    public var investmentStepSize: Double?
    public var investorClassLimit: Double?
    public var isin: String?
    public var maximumInvestmentAmount: Double?
    public var minimumInvestmentAmount: Double?
    public var nominalValue: Double?
    public var shortCut: String?
    public var startTime: String?
    public var typeOfSecurity: String?
    public var unprocessedInvestment: UnprocessedInvestmentResponse?
}

struct UnprocessedInvestmentResponse: Codable {
    public var amount: Double?
    public var investmentId: String?
    public var investmentStatus: String?
}

class ProjectInvestmentInfoRequest: Request {
    
    private var projectID: Int
    override var path : String {
        "/project-service/investments/\(self.projectID)"
    }
    
    override var method: String {
        "GET"
    }
    
    init(session: Session, projectID: Int) {
        self.projectID = projectID
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let investmentInfo = try? JSONDecoder().decode(ProjectInvestmentInfoResponse.self, from: data)
        self.session.projectsProvider.projectInvestmentInfo = ProjectInvestmentInfo(projectInfo: investmentInfo)
    }
    
}
