//
//  IsInvestmentAllowedRequest.swift
//  Remote
//
//  Created by 8pitch on 1/18/21.
//

import Input

public enum IsInvestmentAllowedError : RequestError {
    
    case notAllowed
    
    public var errorDescription : String? {
        switch self {
        case .notAllowed:
            return "error.remote.investment.notAllowed".localized
        }
    }
}

struct IsInvestmentAllowedResponse: Codable {
    public var status: String
}

class IsInvestmentAllowedRequest: Request {
    
    enum Constants {
        static let success: String = "TRUE"
    }
    
    private var projectID: Int
    
    init(session: Session, projectID: Int) {
        self.projectID = projectID
        super.init(session: session)
    }
    
    override var path : String {
        "/project-service/investments/\(self.projectID)/is-allowed"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        ["Authorization" : "Bearer \(self.session.accessToken)",
         "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(IsInvestmentAllowedResponse.self, from: data)
        if response.status != Constants.success {
            throw IsInvestmentAllowedError.notAllowed
        }
    }
}


