//
//  TaxInformationRequest.swift
//  Remote
//
//  Created by 8pitch on 1/14/21.
//

import Input

struct TaxInformationRequestBody: Codable {
    let churchTaxAttribute: String?
    let churchTaxLiability: Bool?
    let isTaxResidentInGermany: Bool
    let responsibleTaxOffice: String?
    let taxNumber: String?
}

class TaxInformationRequest: Request {
    
    override var path : String {
        "/user-service/tax-information"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        guard let taxInfo = try? self.session.taxInformationProvider.value(),
              let isTaxResidentInGermany = try? taxInfo.taxResidentProvider.value() else { return nil }
        let body = TaxInformationRequestBody(churchTaxAttribute: taxInfo.churchAttributeValue, churchTaxLiability: taxInfo.churchTaxLabilityValue, isTaxResidentInGermany: isTaxResidentInGermany, responsibleTaxOffice: taxInfo.taxOfficeValue, taxNumber: taxInfo.taxIDValue)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json",
          "group" : "\(session.accountGroupValue)",
          "userExternalId" : "\(session.externalID)"]
    }
}

