//
//  BookRequest.swift
//  Remote
//
//  Created by 8pitch on 8/28/20.
//

import Foundation
import Input

struct BookResponse: Codable {
    var availableTokens: Int?
    var confirmedInvestmentId: String?
    var investmentId: String?
    var status: String?
}

struct BookRequestBody: Codable {
    var amount:  Int
    var riskAwareness: Bool
}

class BookRequest : Request {
    
    private var projectID: Int
    override var path : String {
        "/project-service/investments/\(self.projectID)/book"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = BookRequestBody(amount: self.session.projectsProvider.currentInvestmentAmount?.inToken.intValue ?? 0, riskAwareness: true)
        return try? JSONEncoder().encode(body)
    }
    
    init(session: Session, projectId: Int) {
        self.projectID = projectId
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let bookResponse = try JSONDecoder().decode(BookResponse.self, from: data)
        self.session.projectsProvider.investmentId = bookResponse.investmentId
    }
    
}
