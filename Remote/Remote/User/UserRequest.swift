//
//  UserRequest.swift
//  Remote
//
//  Created by 8pitch on 30.07.2020.
//

import Foundation
import Input
import RxSwift
import RxCocoa

struct UserResponse : Codable {
    
    struct Address : Codable {
        var addressLine1 : String?
        var addressLine2 : String?
        var city : String
        var country : String
        var region : String?
        var street : String
        var streetNo : String
        var zip : String
    }
    
    struct InvestmentExperienceQuestionnaire : Codable {
        var experience : String?
        var experienceDuration : String?
        var investingAmount : String?
        var investingFrequency : String?
        var knowledge : [String]?
        var professionalExperienceSource : String?
        var virtualCurrenciesUsed : String?
    }
    
    struct InvestorQualificationForm : Codable {
        var investorClass: String?
        var description: String?
        var fileIds: [String]?
    }
    
    struct TaxInformation : Codable {
        var churchTaxAttribute : String?
        var churchTaxLiability : Bool?
        var isTaxResidentInGermany : Bool
        var responsibleTaxOffice : String?
        var taxNumber : String?
    }
    
    var address : Address?
    var investmentExperienceQuestionnaire : InvestmentExperienceQuestionnaire?
    var investorQualificationForm : InvestorQualificationForm?
    
    var accountOwner : String?
    var actionId : String?
    var companyName : String?
    var companyType : String?
    var dateOfBirth : String?
    var deleteAccountProcess : Bool
    var email : String?
    var emailConfirmed : Bool
    var externalId : String?
    var firstName : String?
    var group : String?
    var iban : String?
    var language : String?
    var lastName : String?
    var nationality : String?
    var newNotifications : Bool?
    var phone : String?
    var phoneNumberConfirmed : Bool
    var placeOfBirth : String?
    var politicallyExposedPerson : Bool?
    var prefix : String?
    var sex : String?
    var status : String?
    var subscribeToEmails : Bool?
    var twoFactorAuthenticationEnabled : Bool?
    var twoFactorAuthenticationType : String?
    var usTaxLiability : Bool?
    var webidUpgradeProcessBlock : Bool?
    var taxInformation : TaxInformation?
    var commercialRegisterNumber : String?
    var investorClass : String?
}

class UserRequest : RequestRx {
    
    override var path : String {
        "/user-service/users/me"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(self.session.accessToken)" ]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(UserResponse.self, from: data)
        self.session.userProvider.onNext(parsed)
        self.session.qualificationProvider.qualificationType = parsed.investorQualificationForm?.investorClass
        self.session.updateProviders()
    }
}
