//
//  UpdateUserDataRequest.swift
//  Remote
//
//  Created by 8pitch on 9/17/20.
//

import Foundation
import RxSwift
import RxCocoa
import Input

struct UpdateUserDataRequestBody : Codable {
    
    struct Address: Codable {
        var city: String?
        var country: String?
        var street: String?
        var streetNo: String?
        var zip: String?
    }
    
    struct TaxInformation: Codable {
        var churchTaxAttribute: String?
        var churchTaxLiability: Bool?
        var isTaxResidentInGermany: Bool
        var responsibleTaxOffice: String?
        var taxNumber: String?
    }
    
    var nationality : String?
    var accountOwner : String?
    var address : Address
    var companyName : String?
    var companyType : String?
    var commercialRegisterNumber: String?
    var dateOfBirth : String?
    var firstName : String
    var iban : String?
    var lastName : String
    var placeOfBirth : String?
    var prefix: String
    var politicallyExposedPerson : Bool?
    var usTaxLiability : Bool?
    var taxInformation: TaxInformation?
    
}

class UpdateUserDataRequest : RequestRx {
    
    override var path : String {
        "/user-service/users/personal"
    }
    
    override var method: String {
        "PUT"
    }
    
    override var body : Data? {
        guard let info = try? self.session.personalProfileInfoProvider.value() else { return nil }
        var company: String? = nil
        if let companyType = info.companyType {
            company = CompanyType.rawType(from: companyType)
        }
        var taxInformation: UpdateUserDataRequestBody.TaxInformation? = nil
        if let taxInfo = try? info.germanTaxesProvider?.value(),
           let isResident = try? taxInfo.taxResidentProvider.value() {
            taxInformation = UpdateUserDataRequestBody.TaxInformation(churchTaxAttribute: taxInfo.churchAttributeValue,
                                                                      churchTaxLiability: taxInfo.churchTaxLabilityValue,
                                                                      isTaxResidentInGermany: isResident,
                                                                      responsibleTaxOffice: taxInfo.taxOfficeValue,
                                                                      taxNumber: taxInfo.taxIDValue)
        }
        let body = UpdateUserDataRequestBody(nationality: self.session.countryCode(for: info.nationality),
                                             accountOwner: info.accountOwner,
                                             address: UpdateUserDataRequestBody.Address(
                                                city: info.city,
                                                country: self.session.countryCode(for: info.country),
                                                street: info.street,
                                                streetNo: info.streetNumber,
                                                zip: info.zip),
                                             companyName: info.companyName,
                                             companyType: company,
                                             commercialRegisterNumber: info.companyNumber,
                                             dateOfBirth: info.dateOfBirthFormatted,
                                             firstName: info.firstName,
                                             iban: info.iban,
                                             lastName: info.lastName,
                                             placeOfBirth: info.placeOfBirth,
                                             prefix: Title.rawTitle(from: info.title),
                                             politicallyExposedPerson: info.politicallyExposedPerson,
                                             usTaxLiability: info.usTaxLiability,
                                             taxInformation: taxInformation)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(self.session.accessToken)",
          "Content-Type" : "application/json" ]
    }
    
}
