//
//  AccountDeletionRequest.swift
//  Remote
//
//  Created by 8pitch on 20.09.2020.
//

import Foundation
import Input
import RxSwift
import RxCocoa

struct AccountDeletionRequestBody: Codable {
    var reason: String
}

class AccountDeletionRequest : RequestRx {
    
    private var deletionReason: String

    override var path : String {
        "/user-service/users"
    }

    override var method: String {
        "DELETE"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override var body: Data? {
        let body = AccountDeletionRequestBody(reason: self.deletionReason)
        return try? JSONEncoder().encode(body)
    }
    
    required init(session: Session, deletionReason: String) {
        self.deletionReason = deletionReason
        super.init(session: session)
    }

}

