//
//  UpdateUserSettingsDataRequest.swift
//  Remote
//
//  Created by 8pitch on 28.09.2020.
//

import Input

struct UpdateUserSettingsDataRequestBody: Codable {
    let subscribeToEmails: Bool
}

class UpdateUserSettingsDataRequest: Request {
    
    override var path : String {
        "/user-service/users/settings"
    }
    
    override var method: String {
        "PUT"
    }
    
    override var body: Data? {
        let subscribed = (try? self.session.subscribeToEmailsProvider.value()) ?? false
        let body = UpdateUserSettingsDataRequestBody(subscribeToEmails: subscribed)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
}

