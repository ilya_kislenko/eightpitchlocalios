//
//  UpdateEmailRequest.swift
//  Remote
//
//  Created by 8pitch on 30.09.2020.
//
import Input

struct UpdateEmailRequestBody: Codable {
    let email: String
}

class UpdateEmailRequest: RequestRx {
    
    override var body: Data? {
        let email = try? self.session.emailProvider.value().email
        let body = UpdateEmailRequestBody(email: email ?? "")
        return try? JSONEncoder().encode(body)
    }
    
    override var path : String {
        "/user-service/email/update"
    }
    
    override var method: String {
        "PUT"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(self.session.accessToken)",
        "Content-Type" : "application/json"]
    }
    
}
