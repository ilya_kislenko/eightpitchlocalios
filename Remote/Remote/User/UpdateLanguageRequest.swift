//
//  UpdateLanguageRequest.swift
//  Remote
//
//  Created by 8pitch on 16.11.2020.
//

import Input

enum Language: String {
    case en = "en"
    case de = "de"
}

struct UpdateLanguageRequestBody: Codable {
    let language: String
}

class UpdateLanguageRequest: Request {
    
    override var path : String {
        "/user-service/users/language"
    }
    
    override var method: String {
        "PUT"
    }
    
    override var body: Data? {
        let lang = Language(rawValue: Locale.current.languageCode ?? "de") ?? .de
        let body = UpdateLanguageRequestBody(language: lang.rawValue)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
}
