//
//  TransfersHistoryRequest.swift
//  Remote
//
//  Created by 8pitch on 1/11/21.
//

import Input

struct TransfersHistoryResponse: Codable {
    public var content: [TransferResponse?]
}

struct TransferResponse: Codable {
    public var amount: Double?
    public var companyAddress: String?
    public var companyName: String?
    public var id: String?
    public var isin: String?
    public var transferDate: String?
    public var fromUser: TransferUser?
    public var toUser: TransferUser?
    public var shortCut: String?
}

struct TransferUser: Codable {
    public var prefixFull: String?
    public var firstName: String?
    public var lastName: String?
}

class TransfersHistoryRequest: Request {
    
    private enum Constants {
        static let page: Int = 0
        static let size: Int = 9999
        static let sort = "created,asc"
    }
    
    override var queryItems: [URLQueryItem]? {
        [ URLQueryItem(name: "page", value: "\(Constants.page)"),
          URLQueryItem(name: "size", value: "\(Constants.size)")]
    }
    
    override var path : String {
        "/project-service/investors/transfers"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        ["Authorization" : "Bearer \(self.session.accessToken)",
            "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(TransfersHistoryResponse.self, from: data)
        self.session.user?.transfersHistory = response.content.map { Input.UserTransfer($0) }.sorted { $0.transactionDate ?? Date() > $1.transactionDate ?? Date() }
    }
}

