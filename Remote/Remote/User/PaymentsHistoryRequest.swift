//
//  PaymentsHistoryRequest.swift
//  Remote
//
//  Created by 8pitch on 9/30/20.
//

import Input

struct PaymentsHistoryResponse: Codable {
    public var content: [PaymentResponse?]
    public var pageable: NextPageable?
    public var number: Int?
    public var hasPrevious: Bool?
    public var sort: Sort?
    public var hasNext: Bool?
    public var size: Int?
    public var nextPageable: String?
    public var totalElements: Int?
    public var numberOfElements: Int?
    public var totalPages: Int?
    public var previousPageable: String?
    public var hasContent: Bool?
    public var empty: Bool?
    public var first: Bool?
    public var last: Bool?
}

struct PaymentResponse: Codable {
    public var amount: Double?
    public var companyAddress: String?
    public var companyName: String?
    public var investmentId: String?
    public var isin: String?
    public var monetaryAmount: Double?
    public var paymentSystem: String?
    public var transactionDate: String?
    public var transactionId: String?
    public var shortCut: String
}

struct NextPageable: Codable {
    public var page: Int?
    public var size: Int?
    public var sort: Sort?
    public var offset: Int?
    public var pageNumber: Int?
    public var pageSize: Int?
    public var unpaged: Bool?
    public var paged: Bool?
    
}

struct Sort: Codable {
    public var empty: Bool?
    public var sorted: Bool?
    public var unsorted: Bool?
}

class PaymentsHistoryRequest: Request {
    
    private enum Constants {
        static let page: Int = 0
        static let size: Int = 9999
    }
    
    override var queryItems: [URLQueryItem]? {
        [ URLQueryItem(name: "page", value: "\(Constants.page)"),
          URLQueryItem(name: "size", value: "\(Constants.size)")]
    }
    
    override var path : String {
        "/project-service/investors/payments"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        ["Authorization" : "Bearer \(self.session.accessToken)",
            "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(PaymentsHistoryResponse.self, from: data)
        self.session.user?.paymentsHistory = response.content.map { Input.UserPayment($0) }.sorted { $0.transactionDate ?? Date() > $1.transactionDate ?? Date() }
    }
    
}


