//
//  ChangePhoneRequest.swift
//  Remote
//
//  Created by 8pitch on 16.11.2020.
//

import Input

struct ChangePhoneRequestBody: Codable {
    let interactionId: String
    let phone: String
    let token: String
}

class ChangePhoneRequest: Request {
    
    override var path : String {
        "/user-service/users/phone"
    }
    
    override var method: String {
        "PUT"
    }
    
    override var body: Data? {
        let body = ChangePhoneRequestBody(interactionId: self.session.interactionID, phone: self.session.phone, token: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(VerifyEmailTokenResponse.self, from: data)
        switch ValidationResult(rawValue: parsed.result) ?? .invalid {
        case .expired:
            throw VerifyEmailTokenError.expired
        case .invalid:
            throw VerifyEmailTokenError.invalidCode
        case .valid:
            break
        }
    }
}

