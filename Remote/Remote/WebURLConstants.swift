//
//  WebURLConstants.swift
//  Remote
//
//  Created by 8pitch on 9/25/20.
//

public struct WebURLConstants {
    public static let vimeoBaseURLString = "https://player.vimeo.com/video/"
    public static let vimeoBaseURLastPathComponentString = "/config"
    public static let policiesDE = "\(BGHAPIConfig.webRoot)/policies_de_app/"
    public static let policiesEN = "\(BGHAPIConfig.webRoot)/policies_eng_app/"
    public static let privacyPolicy = "\(BGHAPIConfig.webRoot)/static/privacy"
    public static let privacyPathEng = "\(BGHAPIConfig.webRoot)/privacy-english-app/"
    public static let privacyPathDe = "\(BGHAPIConfig.webRoot)/privacy-german-app/"
    public static let filePath = "\(BGHAPIConfig.root)/project-service/files/"
    public static let faqPathPI = "\(BGHAPIConfig.webRoot)faq/issuers"
    public static let faqPathInvestors = "\(BGHAPIConfig.webRoot)faq/investors"
    public static let login = "\(BGHAPIConfig.webRoot)/login"
}
