//
//  WebIDRequest.swift
//  Remote
//
//  Created by 8pitch on 05.08.2020.
//

import Foundation
import Input
import RxSwift
import RxCocoa

struct WebIDAddress : Codable {
    var street : String
    var streetNo : String
    var zip : String
    var city : String
    var country : String
}

struct WebIDRequestBody : Codable {
    
    var prefix : String
    var firstName : String
    var lastName : String
    var dateOfBirth : String
    var address : WebIDAddress
    var internalInfo : InternalInfo
    
}

struct InternalInfo : Codable {
    
    var accountOwner : String
    var iban : String
    var nationality : String
    var placeOfBirth : String
    var politicallyExposedPerson : Bool
    var usTaxLiability : Bool
    
}

struct WebIDResponseBody : Codable {
    
    var actionID : String
    
    enum CodingKeys : String, CodingKey {
        case actionID = "actionId"
    }
    
}

class WebIDRequest : RequestRx {
    
    override var path : String {
        "/user-service/webid/form-complete"
    }
    
    override var config: APIConfig.Type {
        return BGHAPIConfig.self
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers : [ String : String ] {
        let authorizationValue = "\(self.session.accessToken)"
        return [ "Authorization" : "Bearer " + authorizationValue, "Content-Type" : "application/json" ]
    }
    
    override var body : Data? {
        guard let kyc = try? self.session.KYCProvider.value(),
              let countryCode = self.session.countryCode(for: kyc.country),
              let nationalityCode = self.session.countryCode(for: kyc.nationality),
              let street = kyc.street,
              let streetNumber = kyc.streetNumber,
              let zip = kyc.zip,
              let city = kyc.city,
              let accountOwner = kyc.accountOwner,
              let iban = kyc.iban,
              let placeOfBirth = kyc.placeOfBirth,
              let dateOfBirth = kyc.dateOfBirthFormatted
            else { return nil }
        let address = WebIDAddress(street: street, streetNo: streetNumber, zip: zip, city: city, country: countryCode)
        let info = InternalInfo(accountOwner: accountOwner, iban: iban, nationality: nationalityCode, placeOfBirth: placeOfBirth, politicallyExposedPerson: kyc.politicallyExposedPerson ?? false, usTaxLiability: kyc.usTaxLiability ?? false)
        let user = WebIDRequestBody(prefix: Title.rawTitle(from: kyc.title), firstName: kyc.firstName, lastName: kyc.lastName, dateOfBirth: dateOfBirth, address: address, internalInfo: info)
        let data = try? JSONEncoder().encode(user)
        return data
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(WebIDResponseBody.self, from: data)
        self.session.webIDUserActionIDProvider.onNext(parsed.actionID)
    }
    
}
