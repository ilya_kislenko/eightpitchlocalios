//
//  InvestmentExperienceQuestionnaireRequest.swift
//  Remote
//
//  Created by 8pitch on 8/13/20.
//

import Foundation
import Input
import RxSwift
import RxCocoa

struct InvestmentExperienceQuestionnaireBody: Codable {
    var experience: String?
    var experienceDuration: String?
    var investingAmount: String?
    var investingFrequency: String?
    var knowledge: [String] = [AssetClassesType.notProvided.rawValue]
    var professionalExperienceSource: String?
    var virtualCurrenciesUsed: String?

    init(questionnaire: Questionnaire) {
        experience = questionnaire.assetExperienceProvider
        experienceDuration = questionnaire.experienceDurationProvider
        investingAmount = questionnaire.investmentAmountProvider
        investingFrequency = questionnaire.investingFrequencyProvider
        if let assetClasses = try? questionnaire.assetClassesProvider.value() {
            let tempKnowledges = [assetClasses.bondsProvider,
                                  assetClasses.closedEndMutualFundsProvider,
                                  assetClasses.hedgeFundsProvider,
                                  assetClasses.investmentCertificatesProvider,
                                  assetClasses.stocksProvider,
                                  assetClasses.tokenizedSecuritiesProvider,
                                  assetClasses.warrantsProvider].compactMap( { $0 } )

            if !tempKnowledges.isEmpty {
                knowledge = tempKnowledges
            }
        }
        professionalExperienceSource = questionnaire.howDidGetExperienceProvider
        virtualCurrenciesUsed = questionnaire.virtualCurrencyShareProvider
    }
}

class InvestmentExperienceQuestionnaireRequest : RequestRx {

    override var path : String {
        "/user-service/investors/send-questionnaire"
    }

    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"
        ]
    }
    
    override var body: Data? {
        let body = InvestmentExperienceQuestionnaireBody(questionnaire: self.session.questionnaire)
        return try? JSONEncoder().encode(body)
    }

}
