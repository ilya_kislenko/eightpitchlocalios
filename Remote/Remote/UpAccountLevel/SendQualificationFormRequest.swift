//
//  SendQualificationFormRequest.swift
//  Remote
//
//  Created by 8pitch on 13.01.21.
//

import Input
import RxSwift

struct QualificationFormRequestBody: Codable {
    var description: String
    var fileIds: [String]
    var investorClass: String
}

class SendQualificationFormRequest: Request {
    
    private var qualificationDescription: String
    private var qualificationType: QualificationType
    private var attachedFileIds: [String]
    
    override var path: String {
        "/user-service/investors/send-qualification-form"
    }
    
    override var method: String {
        "POST"
    }
    
    init(qualificationDescription: String, qualificationType: QualificationType, attachedFileIds: [String], session: Session) {
        self.qualificationDescription = qualificationDescription
        self.qualificationType = qualificationType
        self.attachedFileIds = attachedFileIds
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override var body: Data? {
        let body = QualificationFormRequestBody(description: qualificationDescription, fileIds: attachedFileIds, investorClass: qualificationType.rawValue)
        return try? JSONEncoder().encode(body)
    }
    
}
