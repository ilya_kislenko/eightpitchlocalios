//
//  InvestmentExperienceQuestionnaireUpdateRequest.swift
//  Remote
//
//  Created by 8pitch on 26.01.21.
//

import Foundation
class InvestmentExperienceQuestionnaireUpdateRequest : RequestRx {

    override var path : String {
        "/user-service/investors/send-questionnaire"
    }

    override var method: String {
        "PUT"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"
        ]
    }
    
    override var body: Data? {
        let body = InvestmentExperienceQuestionnaireBody(questionnaire: self.session.questionnaire)
        return try? JSONEncoder().encode(body)
    }

}
