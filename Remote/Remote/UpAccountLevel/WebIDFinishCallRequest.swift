//
//  WebIDFinishCallRequest.swift
//  Remote
//
//  Created by 8pitch on 10/21/20.
//

class WebIDFinishCallRequest: Request {

    override var path : String {
        "/user-service/webid/finish-call"
    }

    override var method: String {
        "PUT"
    }

    override var headers: [String : String]? {
        ["Authorization" : "Bearer \(self.session.accessToken)",
         "Content-Type" : "application/json"]
    }
    
}
