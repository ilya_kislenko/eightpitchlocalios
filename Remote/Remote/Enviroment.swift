//
//  Enviroment.swift
//  Remote
//
//  Created by 8pitch on 2/17/21.
//

import Foundation

public enum Enviroment {
    case qa
    case dev
    case demo
    case prod
    case perf
    
    static public var current: Enviroment {
        let url = Bundle.main.API_BASE_URL
        switch url {
        case "backend.qa.bgh-dev.xyz":
            return .qa
        case "backend.dev.bgh-dev.xyz":
            return .dev
        case "backend.prev.8pitch.com":
            return .demo
        case "backend.prod.8pitch.com":
            return .prod
        case "backend.perf.bgh-dev.xyz":
            return .perf
        default:
            fatalError("host setup is wrong")
        }
    }
}
