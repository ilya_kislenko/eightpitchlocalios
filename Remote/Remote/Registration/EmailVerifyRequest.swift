//
//  EmailVerifyRequest.swift
//  Remote
//
//  Created by 8pitch on 26.07.2020.
//

import Foundation
import RxSwift
import RxCocoa

public enum EmailVerifyError : RequestError {
    
    case duplicateEmail
    
    public var errorDescription : String? {
        switch self {
            case .duplicateEmail:
                return "remote.error.response.email.duplicate".localized
        }
    }
    
}

struct EmailVerifyResponse : Codable {
    
    var isValid : Bool
    
    enum CodingKeys : String, CodingKey {
        case isValid = "valid"
    }
    
}

class EmailVerifyRequest : RequestRx {
    
    override var path : String {
        "/user-service/emails/\(self.session.email)/free"
    }
    
    override var method: String {
        "GET"
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(EmailVerifyResponse.self, from: data)
        self.session.isEmailValid = parsed.isValid
        if !parsed.isValid {
            throw EmailVerifyError.duplicateEmail
        }
    }
    
}
