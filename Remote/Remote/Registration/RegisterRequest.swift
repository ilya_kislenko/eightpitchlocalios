//
//  RegisterRequest.swift
//  Remote
//
//  Created by 8pitch on 27.07.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

struct CompanyData : Codable {
    
    var legalForm : String
    var name : String
    
    init(legalForm: CompanyType?, name: String) {
        self.name = name
        self.legalForm = CompanyType.rawType(from: legalForm)
    }
    
}

extension CompanyType {
    
    static func rawType(from type: CompanyType?) -> String {
        switch type {
        case .ag?: return "AG"
        case .eg?: return "EG"
        case .eingetragenerKaufmann?: return "EINGETRAGENER_KAUFMANN"
        case .freiberuflerSelbstaendig?: return "FREIBERUFLER_SELBSTSTÄNDIG"
        case .gbrBGBGesellschaft?: return "GBR_BGBGESELLSCHAFT"
        case .gmbh?: return "GMBH"
        case .gmbhCoKg?: return "GMBH_CO_KG"
        case .kg?: return "KG"
        case .kgaa?: return "KGAA"
        case .ohg?: return "OHG"
        case .ohgCoKg?: return "OHG_CO_KG"
        case .ug?: return "UG"
        case .limited?: return "LIMITED"
        case .eingentragenerVerein?: return "EINGETRAGENER_VEREIN"
        case .stiftung?: return "STIFTUNG"
        case .other?: return "OTHER"
        default: return "AG"
        }
    }
    
}

struct RegisterRequestPrivateBody : Codable {
    var deviceID : String
    var email : String
    var firstName : String
    var group : String = "PRIVATE_INVESTOR" // TODO: hardcoded for now
    var interactionID : String
    var lastName : String
    var password : String
    var phone : String
    var prefix : String
    var subscribeToEmails : Bool
    var language : String
    
    enum CodingKeys : String, CodingKey {
        case deviceID = "deviceId"
        case email
        case firstName
        case group
        case interactionID = "interactionId"
        case lastName
        case password
        case phone
        case prefix
        case subscribeToEmails
        case language
    }
}

struct RegisterRequestInsitutionalBody : Codable {
    var companyData : CompanyData
    var commercialRegisterNumber: String
    var deviceID : String
    var email : String
    var firstName : String
    var group : String = "INSTITUTIONAL_INVESTOR" // TODO: hardcoded for now
    var interactionID : String
    var lastName : String
    var password : String
    var phone : String
    var prefix : String
    var subscribeToEmails : Bool
    var language : String
    
    enum CodingKeys : String, CodingKey {
        case companyData
        case commercialRegisterNumber
        case deviceID = "deviceId"
        case email
        case firstName
        case group
        case interactionID = "interactionId"
        case lastName
        case password
        case phone
        case prefix
        case subscribeToEmails
        case language
    }
}

struct RegisterResponse : Codable {
    
    var tokenResponse : Token
    
}

class RegisterRequest : RequestRx {
    
    override var path : String {
        "/user-service/users"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body : Data? {
        let lang = Language(rawValue: Locale.current.languageCode ?? "de") ?? .de
        if self.session.registrationType == .individualInvestor {
            let body = RegisterRequestPrivateBody(deviceID: session.deviceID, email: session.email, firstName: session.firstName, interactionID: session.interactionID, lastName: session.lastName, password: session.password, phone: session.phone, prefix: Title.rawTitle(from: session.title), subscribeToEmails: session.subscribedToEmail, language: lang.rawValue)
            return try? JSONEncoder().encode(body)
        } else {
            let body = RegisterRequestInsitutionalBody(companyData: .init(legalForm: session.companyType, name: session.companyName), commercialRegisterNumber: session.companyNumber, deviceID: session.deviceID, email: session.email, firstName: session.firstName, interactionID: session.interactionID, lastName: session.lastName, password: session.password, phone: session.phone, prefix: Title.rawTitle(from: session.title), subscribeToEmails: session.subscribedToEmail, language: lang.rawValue)
            return try? JSONEncoder().encode(body)
        }
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json" ]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(RegisterResponse.self, from: data)
        self.session.accessTokenProvider.onNext(parsed.tokenResponse.accessToken)
        self.session.refreshTokenProvider.onNext(parsed.tokenResponse.refreshToken)
        self.saveTokens()
    }
}
