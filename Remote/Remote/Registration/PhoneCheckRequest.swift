//
//  PhoneCheckRequest.swift
//  Remote
//
//  Created by 8pitch on 26.07.2020.
//

import Foundation
import RxSwift
import RxCocoa

public enum PhoneCheckError : RequestError {
    
    case duplicatePhone
    
    public var errorDescription : String? {
        switch self {
            case .duplicatePhone:
                return "remote.error.response.phone.duplicate".localized
        }
    }
    
}

struct PhoneCheckResponse : Codable {
    
    var isValid : Bool
    
    enum CodingKeys : String, CodingKey {
        
        case isValid = "valid"
        
    }
    
}

class PhoneCheckRequest : RequestRx {

    override var path : String {
        "/user-service/phones/\(self.session.phone)/free"
    }
    
    override var method: String {
        "GET"
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(PhoneCheckResponse.self, from: data)
        self.session.isPhoneValid = parsed.isValid
        if !parsed.isValid {
            throw PhoneCheckError.duplicatePhone
        }
    }
    
}
