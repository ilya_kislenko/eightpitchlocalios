//
//  Remote.swift
//  Remote
//
//  Created by 8pitch on 26.07.2020.
//

import Foundation
import RxSwift
import Input

public class Remote {
    
    private var config : APIConfig.Type {
        return BGHAPIConfig.self
    }
    
    public var webURL: URL? {
        self.config.webRoot.url
    }
    
    public var url : URL? {
        self.config.root.url
    }
    
    private var emailVerifyRequest : EmailVerifyRequest?
    private var phoneCheckRequest : PhoneCheckRequest?
    private var sendCodeRequest : SendCodeRequest?
    private var resendCodeRequest : ResendCodeRequest?
    private var verifyTokenRequest : VerifyTokenRequest?
    private var registerRequest : RegisterRequest?
    private var loginRequest : LoginRequest?
    private var verifyLoginTokenRequest : VerifyLoginTokenRequest?
    private var refreshTokenRequest : RefreshTokenRequest?
    private var logoutRequest : LogoutRequest?
    private var userRequest : UserRequest?
    private var sendLoginCodeRequest : SendLoginCodeRequest?
    private var investmentExperienceQuestionnaireRequest : InvestmentExperienceQuestionnaireRequest?
    private var investmentExperienceQuestionnaireUpdateRequest : InvestmentExperienceQuestionnaireUpdateRequest?
    
    private var sendEmailCodeRequest : SendEmailCodeRequest?
    private var verifyEmailTokenRequest : VerifyEmailTokenRequest?
    private var resendEmailCodeRequest : ResendEmailCodeRequest?
    private var webIDRequest : WebIDRequest?
    private var projectsRequest: ProjectsRequest?
    private var projectRequest: ProjectRequest?
    private var investedProjectsRequest: InvestedProjectRequest?
    private var bookRequest: BookRequest?
    private var documentsRequest: DocumentsRequest?
    private var documentsonEmailRequest: DocumentsOnEmailRequest?
    private var cancelInvestmentRequest: CancelInvestmentRequest?
    private var projectInvestmentInfoRequest: ProjectInvestmentInfoRequest?
    private var verifyInvestmentTokenRequest: VerifyInvestmentTokenRequest?
    private var sendInvestmentCodeRequest : SendInvestmentCodeRequest?
    private var confirmBankTransferRequest : ConfirmBankTransferRequest?
    private var manualBankTransferInfoRequest : ManualBankTransferInfoRequest?
    private var secupaySubmitPaymentRequest : SecupaySubmitPaymentRequest?
    private var downloadInvoiceFileRequest : DownloadInvoiceFileRequest?
    private var klarnaPaymentsSubmitRequest: KlarnaPaymentsSubmitRequest?
    private var onlinePaymentsSubmitRequest: OnlinePaymentsSubmitRequest?
    private var notificationRequest: NotificationRequest?
    private var readNotificationRequest: ReadNotificationRequest?
    private var secupayBankTransferInfoRequest: SecupayBankTransferInfoRequest?
    private var accountDeletionRequest: AccountDeletionRequest?
    private var vimeoVideoURLRequest: VimeoVideoURLRequest?
    private var updateUserDataRequest: UpdateUserDataRequest?
    private var updateUserSettingsDataRequest: UpdateUserSettingsDataRequest?
    private var paymentsHistoryRequest: PaymentsHistoryRequest?
    private var updateEmailRequest: UpdateEmailRequest?
    private var enableSMSAuthRequest: EnableSMSAuthRequest?
    private var disableSMSAuthRequest: DisableSMSAuthRequest?
    private var getQRCodeRequest: GetQRCodeRequest?
    private var enableTwoFARequest: EnableTwoFARequest?
    private var disableTwoFARequest: DisableTwoFARequest?
    private var twoFAVerifyCodeRequest: TwoFAVerifyCodeRequest?
    private var initiatorsProjectsRequest: InitiatorsProjectsRequest?
    private var investorsGraphRequest: InvestorsGraphRequest?
    private var investorsProjectsRequest: InvestorsProjectsRequest?
    private var changePasswordRequest: ChangePasswordRequest?
    private var verifyOldPasswordRequest: VerifyOldPasswordRequest?
    private var passwordRecoveryInitialCodeRequest: PasswordRecoveryInitialCodeRequest?
    private var passwordRecoverySendEmailLinkRequest: PasswordRecoverySendEmailLinkRequest?
    private var webIDFinishCallRequest: WebIDFinishCallRequest?
    private var resendLimitCodeRequest: ResendLimitCodeRequest?
    private var sendLimitCodeRequest: SendLimitCodeRequest?
    private var changePhoneRequest: ChangePhoneRequest?
    private var updateLanguageRequest: UpdateLanguageRequest?
    private var linkSliderRequest: LinkSliderRequest?
    private var uploadRequest: UploadRequest?
    private var sendQualificationFormRequest: SendQualificationFormRequest?
    private var sendUnqualifiedForm: SendQualificationFormRequest?
    private var transfersHistoryRequest: TransfersHistoryRequest?
    private var taxInformationRequest: TaxInformationRequest?
    private var securityTypesRequest: SecurityTypeRequest?
    private var minimalInvestmentRequest: MinimalInvestmentRequest?
    private var filteredProjectsRequest: FilteredProjectsRequest?
    private var searchedProjectsRequest: SearchedProjectsRequest?
    private var isInvestmentAllowedRequest: IsInvestmentAllowedRequest?
    private var documentRequest: DocumentRequest?
    private var documentRequests: [DocumentRequest] = []
    private var downloadFileRequest: DownloadFileRequest?
    private var transferReceiptRequest: TransferReceiptRequest?
    private var projectsWithStatusRequest: ProjectsWithStatusRequest?
    private var updatePasswordRequest: UpdatePasswordRequest?
    private var verifyUpdatePasswordTokenRequest: VerifyUpdatePasswordTokenRequest?
    private var verifyUpdatePasswordTwoFARequest: VerifyUpdatePasswordTwoFARequest?
    private var resendUpdatePasswordCodeRequest: ResendUpdatePasswordCodeRequest?
    private var votingsRequest: PIVotingsRequest?
    private var votingRequest: VotingRequest?
    private var rejectVotingRequest: RejectVotingRequest?
    private var approveVotingRequest: ApproveVotingRequest?
    private var investorsVotingsRequest: InvestorsVotingsRequest?
    private var answersRequest: AnswersRequest?
    private var saveAnswersRequest: SaveAnswersRequest?
    private var answersPDFRequest: AnswersPDFRequest?
    private var votingResultRequest: VotingResultRequest?
    private var resultPDFRequest: ResultPDFRequest?
    private var joinMeetingRequest: JoinMeetingRequest?
    private var trusteeDecisionRequest: TrusteeDecisionRequest?
    
    public init() {}
    
    public func emailVerify(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = EmailVerifyRequest(session: session)
        request.send()
        self.emailVerifyRequest = request
        return request.response
    }
    
    public func phoneCheck(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = PhoneCheckRequest(session: session)
        request.send()
        self.phoneCheckRequest = request
        return request.response
    }
    
    public func sendCode(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = SendCodeRequest(session: session)
        request.send()
        self.sendCodeRequest = request
        return request.response
    }
    
    public func sendLimitCode(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = SendLimitCodeRequest(session: session)
        request.send()
        self.sendLimitCodeRequest = request
        return request.response
    }
    
    public func resendEmailCode(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = ResendEmailCodeRequest(session: session)
        request.send()
        self.resendEmailCodeRequest = request
        return request.response
    }
    
    public func sendQuestionnaire(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = InvestmentExperienceQuestionnaireRequest(session: session)
        request.send()
        self.investmentExperienceQuestionnaireRequest = request
        return request.response
    }
    
    public func updateQuestionnaire(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = InvestmentExperienceQuestionnaireUpdateRequest(session: session)
        request.send()
        self.investmentExperienceQuestionnaireUpdateRequest = request
        return request.response
    }
    
    public func resendCode(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = ResendCodeRequest(session: session)
        request.send()
        self.resendCodeRequest = request
        return request.response
    }
    
    public func resendLimitCode(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = ResendLimitCodeRequest(session: session)
        request.send()
        self.resendLimitCodeRequest = request
        return request.response
    }
    
    public func resendUpdatePasswordCode(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = ResendUpdatePasswordCodeRequest(session: session)
        request.send()
        self.resendUpdatePasswordCodeRequest = request
        return request.response
    }
    
    public func verifyToken(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = VerifyTokenRequest(session: session)
        request.send()
        self.verifyTokenRequest = request
        return request.response
    }
    
    public func register(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = RegisterRequest(session: session)
        request.send()
        self.registerRequest = request
        return request.response
    }
    
    public func login(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = LoginRequest(session: session)
        request.send()
        self.loginRequest = request
        return request.response
    }
    
    public func verifyLoginToken(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = VerifyLoginTokenRequest(session: session)
        request.send()
        self.verifyLoginTokenRequest = request
        return request.response
    }
    
    public func verifyUpdatePasswordToken(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = VerifyUpdatePasswordTokenRequest(session: session)
        request.send()
        self.verifyUpdatePasswordTokenRequest = request
        return request.response
    }
    
    public func refreshToken(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = RefreshTokenRequest(session: session)
        request.send()
        self.refreshTokenRequest = request
        return request.response
    }
    
    public func logout(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = LogoutRequest(session: session)
        request.send()
        self.logoutRequest = request
        return request.response
    }
    
    public func user(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = UserRequest(session: session)
        request.send()
        self.userRequest = request
        return request.response
    }
    
    public func sendLoginCode(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = SendLoginCodeRequest(session: session)
        request.send()
        self.sendLoginCodeRequest = request
        return request.response
    }
    
    public func sendEmailCode(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = SendEmailCodeRequest(session: session)
        request.send()
        self.sendEmailCodeRequest = request
        return request.response
    }
    
    public func verifyEmailToken(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = VerifyEmailTokenRequest(session: session)
        request.send()
        self.verifyEmailTokenRequest = request
        return request.response
    }
    
}

public extension Remote {
    
    func webID(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = WebIDRequest(session: session)
        request.send()
        self.webIDRequest = request
        return request.response
    }
    
}

public extension Remote {
    
    func notifications(_ session: Session, _ completionHandler: @escaping NotificationsHandler) {
        let request = NotificationRequest(session: session)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(session.notificationsProvider.notifications, error)
            }
        }
        self.notificationRequest = request
    }
    
    func readNotification(_ session: Session, _ notificationID: Int, _ completionHandler: @escaping ErrorClosure) {
        let request = ReadNotificationRequest(session: session, notificationID: notificationID)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.readNotificationRequest = request
    }
    
    func sendFile(_ session: Session, _ file: AttachedFile, _ completionHandler: @escaping ErrorClosure) {
        let request = UploadRequest(session: session, file: file)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.uploadRequest = request
    }
    
    func sendQualificationForm(_ session: Session, _ description: String, _ qualificationType: QualificationType, _ attachedFileIds: [String] , _ completionHandler: @escaping ErrorClosure) {
        let request = SendQualificationFormRequest(qualificationDescription: description, qualificationType: qualificationType, attachedFileIds: attachedFileIds, session: session)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.sendQualificationFormRequest = request
    }
    
    func projects(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = ProjectsRequest(session: session)
        request.send()
        self.projectsRequest = request
        return request.response
    }
    
    func project(_ session: Session, _ projectID: Int,  _ completionHandler: @escaping ProjectHandler) {
        let request = ProjectRequest(session: session, projectID: projectID)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(session.projectsProvider.expandedProject, error)
            }
        }
        self.projectRequest = request
    }
    
    func investedProjects(_ session: Session, completionHandler: @escaping ProjectsHandler) {
        let request = InvestedProjectRequest(session: session)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(session.projectsProvider.investedProjects, error)
            }
        }
        self.investedProjectsRequest = request
    }
    
    func bookInvestment(_ session: Session, _ projectId: Int, _ completionHandler: @escaping BookHandler) {
        let request = BookRequest(session: session, projectId: projectId)
        request.send { error in
            completionHandler(session.projectsProvider.investmentId, error)
        }
        self.bookRequest = request
    }
    
    func documents(_ session: Session,
                   id: String,
                   completionHandler: @escaping ResponseDataClosure) {
        let request = DocumentRequest(session: session, id: id, completionHandler: completionHandler)
        request.send(completionHandler: { error in
            DispatchQueue.main.async {
                guard let error = error else { return }
                completionHandler(.failure(error))
            }
        })
        self.documentRequests.append(request)
    }
    
    func document(_ session: Session,
                  id: String,
                  completionHandler: @escaping ResponseDataClosure) {
        let request = DocumentRequest(session: session, id: id, completionHandler: completionHandler)
        request.send(completionHandler: { error in
            DispatchQueue.main.async {
                guard let error = error else { return }
                completionHandler(.failure(error))
            }
        })
        self.documentRequest = request
    }
    
    func vimeoVideoURL(_ videoID: String, _ completionHandler: @escaping ResponseLinkClosure) {
        
        if self.vimeoVideoURLRequest == nil {
            self.vimeoVideoURLRequest = VimeoVideoURLRequest()
        }
        self.vimeoVideoURLRequest?.send(videoID) { (response) in
            switch response {
            case .success(let link):
                completionHandler(.success(link))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
    
    
    func documentsOnEmail(_ session: Session, projectId: Int, investmentID: String, _ completionHandler: @escaping ErrorClosure) {
        let request = DocumentsOnEmailRequest(session: session, projectID: projectId, investmentID: investmentID)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.documentsonEmailRequest = request
    }
    
    func projectInvestmentInfo(_ session: Session, projectId: Int, _ completionHandler: @escaping ResponseClosure) {
        
        let request = ProjectInvestmentInfoRequest(session: session, projectID: projectId)
        request.send { error in
            if let error = error {
                completionHandler(.failure(error))
                return
            }
            self.project(session, projectId) { (project, error) in
                if let error = error {
                    completionHandler(.failure(error))
                } else {
                    completionHandler(.success(session))
                }
            }
        }
        self.projectInvestmentInfoRequest = request
    }
    
    func cancelInvestment(_ session: Session, _ projectId: Int, _ investmentId: String, _ completionHandler: @escaping ErrorClosure) {
        let request = CancelInvestmentRequest(session: session, projectID: projectId, investmentID: investmentId)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.cancelInvestmentRequest = request
    }
    
    func verifyInvestmentToken(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = VerifyInvestmentTokenRequest(session: session)
        request.send()
        self.verifyInvestmentTokenRequest = request
        return request.response
    }
    
    func sendInvestmentCode(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = SendInvestmentCodeRequest(session: session)
        request.send()
        self.sendInvestmentCodeRequest = request
        return request.response
    }
    
    func confirmBankTransfer(_ session: Session, _ investmentID: String, _ completionHandler: @escaping ErrorClosure) {
        let request = ConfirmBankTransferRequest(session: session, investmentID: investmentID)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.confirmBankTransferRequest = request
    }
    
    func manualBankTransferInfo(_ session: Session, _ investmentID: String, _ completionHandler: @escaping ErrorClosure) {
        let request = ManualBankTransferInfoRequest(session: session, investmentID: investmentID)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.manualBankTransferInfoRequest = request
    }
    
    func secupayBankTransferInfo(_ session: Session, _ investmentID: String, _ completionHandler: @escaping ErrorClosure) {
        let request = SecupayBankTransferInfoRequest(session: session, investmentID: investmentID)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.secupayBankTransferInfoRequest = request
    }
    
    func secupayPaymentSubmit(_ session: Session, _ investmentID: String, _ bankAccount: Input.BankAccount, _ completionHandler: @escaping ErrorClosure) {
        let request = SecupaySubmitPaymentRequest(session: session, investmentID: investmentID, bankAccount: bankAccount)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.secupaySubmitPaymentRequest = request
    }
    
    func downloadInvoiceFile(_ session: Session, _ investmentID: String, _ completionHandler: @escaping ResponseDataClosure) {
        let request = DownloadInvoiceFileRequest(session: session, investmentID: investmentID, completionHandler: completionHandler)
        request.send(completionHandler: { error in
            DispatchQueue.main.async {
                guard let error = error else { return }
                completionHandler(.failure(error))
            }
        })
        self.downloadInvoiceFileRequest = request
    }
    
    func klarnaPaymentsSubmitRequest(_ session: Session, currentInvestmentID: String, _ completionHandler: @escaping ResponseClosure) {
        let request = KlarnaPaymentsSubmitRequest(session: session, currentInvestmentID: currentInvestmentID)
        request.send { error in
            DispatchQueue.main.async {
                if let error = error {
                    completionHandler(.failure(error))
                    return
                }
                completionHandler(.success(session))
            }
        }
        self.klarnaPaymentsSubmitRequest = request
    }
    
    func onlinePaymentsSubmitRequest(_ session: Session, currentInvestmentID: String, _ completionHandler: @escaping ResponseClosure) {
        let request = OnlinePaymentsSubmitRequest(session: session, currentInvestmentID: currentInvestmentID)
        request.send { error in
            DispatchQueue.main.async {
                if let error = error {
                    completionHandler(.failure(error))
                    return
                }
                completionHandler(.success(session))
            }
        }
        self.onlinePaymentsSubmitRequest = request
    }
    
    func deleteAccount(_ session: Session, deletionReason: String) -> BehaviorSubject<Result<Session, Error>?> {
        let request = AccountDeletionRequest(session: session, deletionReason: deletionReason)
        request.send()
        self.accountDeletionRequest = request
        return request.response
    }
    
    func updateUserData(_ session: Session) -> BehaviorSubject<Result<Session, Error>?> {
        let request = UpdateUserDataRequest(session: session)
        request.send()
        self.updateUserDataRequest = request
        return request.response
    }
    
    func updateUserSettingsData(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = UpdateUserSettingsDataRequest(session: session)
        request.send(completionHandler: completionHandler)
        self.updateUserSettingsDataRequest = request
    }
    
    func payments(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = PaymentsHistoryRequest(session: session)
        request.send(completionHandler: { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        })
        self.paymentsHistoryRequest = request
    }
    
    func changeEmail(_ session: Session)  -> BehaviorSubject<Result<Session, Error>?> {
        let request = UpdateEmailRequest(session: session)
        request.send()
        self.updateEmailRequest = request
        return request.response
    }
    
    func enableSMSAuth(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = EnableSMSAuthRequest(session: session)
        request.send(completionHandler: completionHandler)
        self.enableSMSAuthRequest = request
    }
    
    func disableSMSAuth(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = DisableSMSAuthRequest(session: session)
        request.send(completionHandler: completionHandler)
        self.disableSMSAuthRequest = request
    }
    
    func QRCode(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = GetQRCodeRequest(session: session)
        request.send(completionHandler: completionHandler)
        self.getQRCodeRequest = request
    }
    
    func enableTwoFA(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = EnableTwoFARequest(session: session)
        request.send(completionHandler: completionHandler)
        self.enableTwoFARequest = request
    }
    
    func disableTwoFA(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = DisableTwoFARequest(session: session)
        request.send(completionHandler: completionHandler)
        self.disableTwoFARequest = request
    }
    
    func verifyTwoFACode(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = TwoFAVerifyCodeRequest(session: session)
        request.send(completionHandler: completionHandler)
        self.twoFAVerifyCodeRequest = request
    }
    
    func verifyTwoFAUpdatePasswordCode(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = VerifyUpdatePasswordTwoFARequest(session: session)
        request.send(completionHandler: completionHandler)
        self.verifyUpdatePasswordTwoFARequest = request
    }
    
    func initiatorsProjects(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = InitiatorsProjectsRequest(session: session)
        request.send(completionHandler: completionHandler)
        self.initiatorsProjectsRequest = request
    }
    
    func investorsGraph(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = InvestorsGraphRequest(session: session)
        request.send(completionHandler: completionHandler)
        self.investorsGraphRequest = request
    }
    
    func investorsProjects(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = InvestorsProjectsRequest(session: session)
        request.send { (error) in
            if let error = error {
                completionHandler(error)
            } else {
                self.investorsGraph(session, completionHandler: completionHandler)
            }
        }
        self.investorsProjectsRequest = request
    }
    
    func changePassword(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = ChangePasswordRequest(session: session)
        request.send() { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.changePasswordRequest = request
    }
    
    func updatePassword(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = UpdatePasswordRequest(session: session)
        request.send() { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.updatePasswordRequest = request
    }
    
    func verifyPassword(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = VerifyOldPasswordRequest(session: session)
        request.send() { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.verifyOldPasswordRequest = request
    }
    
    func passwordRecoveryInitial(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = PasswordRecoveryInitialCodeRequest(session: session)
        request.send() { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.passwordRecoveryInitialCodeRequest = request
    }
    
    func passwordRecoveryLink(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = PasswordRecoverySendEmailLinkRequest(session: session)
        request.send() { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.passwordRecoverySendEmailLinkRequest = request
    }
    
    func finishCall(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = WebIDFinishCallRequest(session: session)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.webIDFinishCallRequest = request
    }
    
    func changePhone(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = ChangePhoneRequest(session: session)
        request.send() { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.changePhoneRequest = request
    }
    
    func updateLanguage(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = UpdateLanguageRequest(session: session)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.updateLanguageRequest = request
    }
    
    func linkSlider(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = LinkSliderRequest(session: session)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.linkSliderRequest = request
    }
    
    func transfersHistory(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = TransfersHistoryRequest(session: session)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.transfersHistoryRequest = request
    }
    
    func taxInformation(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = TaxInformationRequest(session: session)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.taxInformationRequest = request
    }
    
    func getSecurityTypes(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = SecurityTypeRequest(session: session)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.securityTypesRequest = request
    }
    
    func isInvestmentAllowed(_ session: Session, projectID: Int, completionHandler: @escaping ErrorClosure) {
        let request = IsInvestmentAllowedRequest(session: session, projectID: projectID)
        request.send { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.isInvestmentAllowedRequest = request
    }
    
    func downloadFile(_ session: Session, _ id: String, _ completionHandler: @escaping ResponseDataClosure) {
        let request = DownloadFileRequest(session: session, id: id, completionHandler: completionHandler)
        request.send(completionHandler: { error in
            DispatchQueue.main.async {
                guard let error = error else { return }
                completionHandler(.failure(error))
            }
        })
        self.downloadFileRequest = request
    }
    
    func downloadTransferFile(_ session: Session, _ id: String, _ completionHandler: @escaping ResponseDataClosure) {
        let request = TransferReceiptRequest(session: session, id: id, completionHandler: completionHandler)
        request.send(completionHandler: { error in
            DispatchQueue.main.async {
                guard let error = error else { return }
                completionHandler(.failure(error))
            }
        })
        self.transferReceiptRequest = request
    }
    
    func getMinimalInvestment(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = MinimalInvestmentRequest(session: session)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.minimalInvestmentRequest = request
    }
    
    func getProjectsFiltered(_ session: Session, queryItems: [URLQueryItem]?, completionHandler: @escaping ErrorClosure) {
        let request = FilteredProjectsRequest(session: session, queryItems: queryItems)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.filteredProjectsRequest = request
    }
    
    func getProjectsSearched(_ session: Session, queryItems: [URLQueryItem]?, completionHandler: @escaping ErrorClosure) {
        let request = SearchedProjectsRequest(session: session, queryItems: queryItems)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.searchedProjectsRequest = request
    }
    
    func projectsWithStatus(_ session: Session, type: ProjectsType, completionHandler: @escaping ErrorClosure) {
        let request = ProjectsWithStatusRequest(session: session, type: type)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.projectsWithStatusRequest = request
    }
    
    func PIvotings(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = PIVotingsRequest(session: session)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.votingsRequest = request
    }
    
    func voting(_ session: Session, id: Int, completionHandler: @escaping ErrorClosure) {
        let request = VotingRequest(session: session, id: id)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.votingRequest = request
    }
    
    func rejectVoting(_ session: Session, id: Int, completionHandler: @escaping ErrorClosure) {
        let request = RejectVotingRequest(session: session, id: id)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.rejectVotingRequest = request
    }
    
    func approveVoting(_ session: Session, id: Int, completionHandler: @escaping ErrorClosure) {
        let request = ApproveVotingRequest(session: session, id: id)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.approveVotingRequest = request
    }
    
    func investorsVotings(_ session: Session, completionHandler: @escaping ErrorClosure) {
        let request = InvestorsVotingsRequest(session: session)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.investorsVotingsRequest = request
    }
    
    func answers(_ session: Session, id: Int, completionHandler: @escaping ErrorClosure) {
        let request = AnswersRequest(session: session, id: id)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.answersRequest = request
    }
    
    func saveAnswers(_ session: Session, id: Int, completionHandler: @escaping ErrorClosure) {
        let request = SaveAnswersRequest(session: session, id: id)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.saveAnswersRequest = request
    }
    
    func votingAnswersFile(_ session: Session, _ id: Int, _ completionHandler: @escaping ResponseDataClosure) {
        let request = AnswersPDFRequest(session: session, id: id, completionHandler: completionHandler)
        request.send(completionHandler: { error in
            DispatchQueue.main.async {
                guard let error = error else { return }
                completionHandler(.failure(error))
            }
        })
        self.answersPDFRequest = request
    }
    
    func votingResult(_ session: Session, id: Int, completionHandler: @escaping ErrorClosure) {
        let request = VotingResultRequest(session: session, id: id)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.votingResultRequest = request
    }
    
    func votingResultFile(_ session: Session, _ id: Int, _ completionHandler: @escaping ResponseDataClosure) {
        let request = ResultPDFRequest(session: session, id: id, completionHandler: completionHandler)
        request.send(completionHandler: { error in
            DispatchQueue.main.async {
                guard let error = error else { return }
                completionHandler(.failure(error))
            }
        })
        self.resultPDFRequest = request
    }
    
    func joinMeeting(_ session: Session, id: Int, completionHandler: @escaping ErrorClosure) {
        let request = JoinMeetingRequest(session: session, id: id)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.joinMeetingRequest = request
    }
    
    func trusteeDecision(_ session: Session, id: Int, completionHandler: @escaping ErrorClosure) {
        let request = TrusteeDecisionRequest(session: session, id: id)
        request.send { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
        self.trusteeDecisionRequest = request
    }
}

