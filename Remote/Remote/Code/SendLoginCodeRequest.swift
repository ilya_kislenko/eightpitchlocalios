//
//  SendLoginCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 27.07.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

struct SendLoginCodeResponse : Codable {
        
    var interactionID : String

    enum CodingKeys: String, CodingKey {
        case interactionID = "interactionId"
    }
    
}

class SendLoginCodeRequest : RequestRx {
    
    override var path : String {
        "/user-service/mobile/phones/\(self.session.phone)/send-code-without-recaptcha"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Basic \(self.authHeader)" ]
    }
    
    override var method: String {
        "POST"
    }
    
}
