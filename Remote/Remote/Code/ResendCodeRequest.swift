//
//  ResendCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 29.07.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

struct ResendCodeRequestBody : Codable {
    
    enum CodingKeys : String, CodingKey {
        case interactionID = "interactionId"
    }
    
    var interactionID : String
    var userResponse = ""
    
    init(interactionID: String) {
        self.interactionID = interactionID
    }
    
}

class ResendCodeRequest : RequestRx {
    
    override var path : String {
        "/user-service/mobile/phones/resend-code-without-recaptcha"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = ResendCodeRequestBody(interactionID: self.session.interactionID)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Content-Type" : "application/json",
          "Authorization" : "Basic \(self.authHeader)" ]
    }
    
}
