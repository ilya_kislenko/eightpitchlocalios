//
//  SendEmailCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 05.08.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input


class SendEmailCodeRequest : RequestRx {
    
    override var path : String {
        "/user-service/email/send-code"
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(self.session.accessToken)" ]
    }
    
}

