//
//  SendCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 27.07.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

struct SendCodeResponse : Codable {
        
    var interactionID : String

    enum CodingKeys: String, CodingKey {
        case interactionID = "interactionId"
    }
    
}

class SendCodeRequest : RequestRx {
    
    override var path : String {
        "/user-service/mobile/phones/\(self.session.phone)/send-code-without-recaptcha"
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Basic \(self.authHeader)" ]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(SendCodeResponse.self, from: data)
        self.session.interactionIDProvider.onNext(parsed.interactionID)
    }

}
