//
//  ResendLimitCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 26.10.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

public enum SendCodeError : RequestError {
    
    case limit
    
    public var errorDescription : String? {
        switch self {
        case .limit:
            return "remote.error.response.code.limit".localized
        }
    }
    
}

struct ResendLimitCodeRequestBody : Codable {
    enum CodingKeys : String, CodingKey {
        case interactionID = "interactionId"
    }
    
    var interactionID : String
}

class ResendLimitCodeRequest : RequestRx {
    
    override var path : String {
        "/user-service/mobile/phones/resend-limit-code"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = ResendCodeRequestBody(interactionID: self.session.interactionID)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
            ["Content-Type" : "application/json",
             "Authorization" : "Basic \(self.authHeader)"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(SendLimitCodeResponse.self, from: data)
        self.session.remainingAttemptsProvider.onNext(parsed.remainingAttempts)
        if self.session.codeLimitReached && parsed.interactionID?.isEmpty ?? true {
            throw SendCodeError.limit
        }
    }
    
}
