//
//  ResendEmailCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 05.08.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

struct ResendEmailCodeRequestBody : Codable {
    
    enum CodingKeys : String, CodingKey {
        case interactionID = "interactionId"
    }
    
    var interactionID : String
    var userResponse = ""
    
    init(interactionID: String) {
        self.interactionID = interactionID
    }
    
}

class ResendEmailCodeRequest : RequestRx {
    
    override var path : String {
        "/user-service/email/resend-code"
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(self.session.accessToken)",
            "Content-Type" : "application/json" ]
    }
    
}

