//
//  SendInvestmentCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 9/4/20.
//

import Foundation
import RxSwift
import RxCocoa
import Input

struct SendInvestmentCodeResponse : Codable {
        
    var interactionID : String

    enum CodingKeys: String, CodingKey {
        case interactionID = "interactionId"
    }
    
}

class SendInvestmentCodeRequest : RequestRx {
    
    private let projectID: Int
    private let investmentID: String
    
    override var path : String {
        "/project-service/investments/\(self.projectID)/send-confirmation-code-without-recaptcha/\(self.investmentID)"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override var method: String {
        "POST"
    }
    
    override init(session: Session) {
        self.projectID = session.projectsProvider.currentProject?.identifier ?? 0
        self.investmentID = session.projectsProvider.currentInvestmentID ?? ""
        super.init(session: session)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(SendInvestmentCodeResponse.self, from: data)
        self.session.interactionIDProvider.onNext(parsed.interactionID)
    }

}

