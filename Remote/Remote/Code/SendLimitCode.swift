//
//  SendLimitCode.swift
//  Remote
//
//  Created by 8pitch on 26.10.2020.
//

import Foundation
import RxSwift
import RxCocoa
import Input

struct SendLimitCodeResponse : Codable {
        
    var interactionID : String?
    var remainingAttempts : Int

    enum CodingKeys: String, CodingKey {
        case interactionID = "interactionId"
        case remainingAttempts
    }
    
}

class SendLimitCodeRequest : RequestRx {
    
    override var path : String {
        "/user-service/mobile/phones/\(self.session.phone)/send-limit-code"
    }
    
    override var method: String {
        "POST"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Basic \(self.authHeader)" ]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode(SendLimitCodeResponse.self, from: data)
        self.session.remainingAttemptsProvider.onNext(parsed.remainingAttempts)
        if self.session.codeLimitReached && parsed.interactionID?.isEmpty ?? true {
            throw SendCodeError.limit
        }
        self.session.interactionIDProvider.onNext(parsed.interactionID)
    }

}

