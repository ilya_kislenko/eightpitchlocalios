//
//  Request.swift
//  Remote
//
//  Created by 8pitch on 12/16/20.
//

import Input

enum UserGroup: String {
    case topInitiatorSlider = "topInitiatorSlider"
    case topInvestorSlider = "topInvestorSlider"
}

struct LinkSliderRequestBody: Codable {
    var group: String
}

struct LinkSliderResponse: Codable {
    var backgroundImageFileId: String
    var buttonText: String
    var group: String
    var header: String
    var link: String
    var title: String
}

class LinkSliderRequest: Request {
    
    override var path : String {
        "/project-service/link-slider"
    }
    
    override var method: String {
        "GET"
    }
    
    override var queryItems: [URLQueryItem]? {
        let group = self.session.accountGroup == .initiator ?
            UserGroup.topInitiatorSlider.rawValue : UserGroup.topInvestorSlider.rawValue
        return [ URLQueryItem(name: "group", value: "\(group)") ]
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let parsed = try JSONDecoder().decode([LinkSliderResponse].self, from: data)
        self.session.projectsProvider.sliderContent = parsed.map { SliderContent(backgroundImageFileID: $0.backgroundImageFileId, buttonText: $0.buttonText, group: $0.group, header: $0.header, link: $0.link, title: $0.title)}
    }
    
}
