//
//  UploadRequest.swift
//  Remote
//
//  Created by 8pitch on 12.01.21.
//

import Input
import RxSwift
import MobileCoreServices

public struct UploadResponse : Codable {
    
    var id : String
    
}

class UploadRequest: Request {
    private var attachmentFile: AttachedFile
    
    private let boundary = "Boundary-\(UUID().uuidString)"
    
    override var path: String {
        "/project-service/files/upload"
    }
    
    override var method: String {
        "POST"
    }
    
    init(session: Session, file: AttachedFile) {
        self.attachmentFile = file
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "multipart/form-data; boundary=\(boundary)"]
    }
    
    override var body: Data? {
        guard let fileName = attachmentFile.name,
              let fileData = attachmentFile.fileData,
              let fileExtension = attachmentFile.fileExtension,
              let extUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension as CFString, nil)?.takeUnretainedValue(),
              let mimeUTI = UTTypeCopyPreferredTagWithClass(extUTI, kUTTagClassMIMEType)?.takeRetainedValue() as String? else { return nil }
        
        return convertFileData(fieldName: "file", fileName: fileName, mimeType: mimeUTI, fileData: fileData, using: boundary)
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        if data.isEmpty {
            return // no-op
        }
        let fileId = try JSONDecoder().decode(UploadResponse.self, from: data)
        self.session.uploadFileProvider.uploadFileIDs.append(fileId.id)
    }
    
    
    func convertFileData(fieldName: String, fileName: String, mimeType: String, fileData: Data, using boundary: String) -> Data {
        let data = NSMutableData()

        data.appendString("--\(boundary)\r\n")
        
        data.appendString("Content-Type: \(mimeType)\r\n")
        data.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n\r\n")
        data.append(fileData)
        data.appendString("\r\n--\(boundary)--\r\n")

        return data as Data
    }
    
}

extension NSMutableData {
    func appendString(_ string: String) {
        if let data = string.data(using: .utf8) {
            self.append(data)
        }
    }
}
