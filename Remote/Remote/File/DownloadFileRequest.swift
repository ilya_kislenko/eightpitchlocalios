//
//  DownloadFileRequest.swift
//  Remote
//
//  Created by 8pitch on 1/27/21.
//

import Foundation
import Input

class DownloadFileRequest : Request {
    
    private var id: String
    private var completionHandler: ResponseDataClosure
    override var path : String {
        "/project-service/documents/receipts/\(self.id)"
    }
    
    override var method: String {
        "GET"
    }
    
    init(session: Session, id: String, completionHandler: @escaping ResponseDataClosure) {
        self.id = id
        self.completionHandler = completionHandler
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        DispatchQueue.main.async {
            self.completionHandler(.success(data))
        }
    }
}
