//
//  ImageRequestURLConfigurator.swift
//  Remote
//
//  Created by 8pitch on 9/27/20.
//

import Input

class ImageRequestURLConfigurator {
    
    var config : APIConfig.Type {
        return BGHAPIConfig.self
    }
    
    var url : URL {
        var root = self.config.root
        root.path += self.path
        return root.url!
    }
    
    var path : String {
        "/project-service/files/\(self.imageID)"
    }

    private var imageID: String
    var method: String {
        "GET"
    }

    init(imageID: String) {
        self.imageID = imageID
    }
}
