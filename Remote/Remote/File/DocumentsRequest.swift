//
//  DocumentsRequest.swift
//  Remote
//
//  Created by 8pitch on 8/28/20.
//

import Input

class DocumentsRequest : Request {
    
    private var urlString: String?
    override var path : String {
        "/project-service/files/"
    }
    private var finishHandler: SessionFinishedHandler?
    private var errorHandler: ErrorClosure?
    private var progressHandler: ProgressHandler?
    
    override var method: String {
        "GET"
    }
    
    func send(url: URL) {
        let queue = OperationQueue()
        //TODO: check if move session to classProperty, to not spawn new one on every call
        //pay attention to progress counter(possibly will be broken after it)
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: queue)
        let task = session.downloadTask(with: url)
        task.resume()
    }
    
    func loadDocuments(finishHandler: @escaping SessionFinishedHandler,
                       errorHandler: @escaping ErrorClosure,
                       progressHandler: @escaping ProgressHandler) {
        self.finishHandler = finishHandler
        self.errorHandler = errorHandler
        self.progressHandler = progressHandler
        let ids = self.session.projectsProvider.expandedProject?.documents.map { $0?.uniqueFileName }
        ids?.forEach {
            guard let id = $0, let fileURL = URL(string: self.url.absoluteString + id) else { return }
            self.send(url: fileURL)
        }
    }
}

extension DocumentsRequest: URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        self.finishHandler?(downloadTask.originalRequest?.url, location)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard let error = error else { return }
        self.errorHandler?(GenericRequestError.invalidResponse(error))
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        self.progressHandler?(totalBytesExpectedToWrite, totalBytesWritten)
    }
}

