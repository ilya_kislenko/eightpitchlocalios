//
//  DocumentRequest.swift
//  Remote
//
//  Created by 8pitch on 29.01.2021.
//

import Input
import Foundation

class DocumentRequest : Request {
    
    private var id: String
    private var completionHandler: ResponseDataClosure
    override var path : String {
        "/project-service/files/\(self.id)"
    }
    
    override var method: String {
        "GET"
    }
    
    init(session: Session, id: String, completionHandler: @escaping ResponseDataClosure) {
        self.id = id
        self.completionHandler = completionHandler
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        DispatchQueue.main.async {
            self.completionHandler(.success(data))
        }
    }
}

