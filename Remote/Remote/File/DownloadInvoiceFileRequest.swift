//
//  DownloadInvoiceFileRequest.swift
//  Remote
//
//  Created by 8pitch on 9/9/20.
//

import Foundation
import Input

class DownloadInvoiceFileRequest : Request {
    
    private var investmentId: String
    private var completionHandler: ResponseDataClosure
    override var path : String {
        "/project-service/documents/invoices/manual-transfers/\(self.investmentId)"
    }
    
    override var method: String {
        "GET"
    }
    
    init(session: Session, investmentID: String, completionHandler: @escaping ResponseDataClosure) {
        self.investmentId = investmentID
        self.completionHandler = completionHandler
        super.init(session: session)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        DispatchQueue.main.async {
            self.completionHandler(.success(data))
        }
    }
}

