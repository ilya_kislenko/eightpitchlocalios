//
//  APIConfig.swift
//  Remote
//
//  Created by 8pitch on 26.07.2020.
//

import Foundation

protocol APIConfig {
    
    static var root : URLComponents { get }
    static var webRoot : URLComponents { get }
    
}

struct BGHAPIConfig : APIConfig {
    
    private static let hostname = Bundle.main.API_BASE_URL
    private static let webHostname = Bundle.main.WEB_BASE_URL
    private static let scheme = "https"
    private static let port = 8443
    
    static var root : URLComponents {
        var retVal = URLComponents()
        retVal.host = self.hostname
        retVal.port = self.port
        retVal.scheme = self.scheme
        return retVal
    }
    
    static var webRoot : URLComponents {
        var retVal = URLComponents()
        retVal.host = self.webHostname
        retVal.scheme = self.scheme
        return retVal
    }
    
}
