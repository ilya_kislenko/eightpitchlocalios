//
//  TwoFAVerifyCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 10/5/20.
//

import Input

struct TwoFAVerifyCodeRequestBody: Codable {
    let code: String
    let interactionId: String
}

class TwoFAVerifyCodeRequest: Request {
    
    override var path : String {
        "/user-service/2fa/verify-code"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = TwoFAVerifyCodeRequestBody(code: self.session.code, interactionId: self.session.interactionID)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        ["Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(VerifyLoginTokenResponse.self, from: data)
        switch response.tokenValidationResult {
        case .expired:
            throw VerifyTokenError.expired
        case .invalid:
            throw VerifyTokenError.invalidCode
        case .valid:
            break
        }
        self.session.accessTokenProvider.onNext(response.tokenResponse?.accessToken)
        self.session.refreshTokenProvider.onNext(response.tokenResponse?.refreshToken)
        self.saveTokens()
    }
    
}
