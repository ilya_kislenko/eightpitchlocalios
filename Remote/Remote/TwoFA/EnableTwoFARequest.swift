//
//  EnableTwoFARequest.swift
//  Remote
//
//  Created by 8pitch on 10/5/20.
//

import Input

struct EnableTwoFARequestBody: Codable {
    let group: String
    let twoFactorAuthCode: String
}

class EnableTwoFARequest: Request {
    
    override var path : String {
        "/user-service/2fa/enable-with-access-token"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = EnableTwoFARequestBody(group: self.session.accountGroup?.rawValue ?? "", twoFactorAuthCode: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
          "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(TwoFARequestResponse.self, from: data)
        let status = Status(rawValue: response.status ?? "")
        switch status {
        case .success:
            return // no-op
        case .invalidCode:
            throw VerifyTokenError.invalidCode
        case .expired:
            throw VerifyTokenError.expired
        default:
            throw GenericRequestError.invalidResponse(nil)
        }
    }
    
}
