//
//  GetQRCodeRequest.swift
//  Remote
//
//  Created by 8pitch on 10/5/20.
//

import Input

struct GetQRCodeRequestResponse: Codable {
    let qrCodeImageUrl: String?
}

class GetQRCodeRequest: Request {
    
    override var path : String {
        "/user-service/2fa/qrcode"
    }
    
    override var method: String {
        "GET"
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(self.session.accessToken)",
        "Content-Type" : "application/json"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(GetQRCodeRequestResponse.self, from: data)
        self.session.createTwoFAURL(response.qrCodeImageUrl)
    }
    
}
