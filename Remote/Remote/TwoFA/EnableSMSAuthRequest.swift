//
//  EnableSMSAuthRequest.swift
//  Remote
//
//  Created by 8pitch on 10/1/20.
//

import UIKit
import Input

struct EnableSMSAuthRequestBody: Codable {
    let interactionId: String
    let token: String
}

enum Status: String {
    case success =  "SUCCESS"
    case invalidPassword = "INVALID_PASSWORD"
    case invalidCode = "INVALID_CODE"
    case twoFAAlreadyEnabled = "TWO_FACTOR_AUTH_ALREADY_ENABLED"
    case expired = "EXPIRED"
}

struct TwoFARequestResponse: Codable {
    public var status: String?
}


class EnableSMSAuthRequest: Request {
    
    override var path : String {
        "/user-service/sms-2fa/enable"
    }
    
    override var method: String {
        "POST"
    }
    
    override var body: Data? {
        let body = EnableSMSAuthRequestBody(interactionId: self.session.interactionID, token: self.session.code)
        return try? JSONEncoder().encode(body)
    }
    
    override var headers: [String : String]? {
        [ "Authorization" : "Bearer \(session.accessToken)",
            "Content-Type" : "application/json",
            "group" : "\(session.accountGroupValue)",
            "userExternalId" : "\(session.externalID)"]
    }
    
    override func parse(_ data: Data) throws {
        try super.parse(data)
        let response = try JSONDecoder().decode(TwoFARequestResponse.self, from: data)
        let status = Status(rawValue: response.status ?? "")
        switch status {
        case .success:
            return // no-op
        case .invalidCode:
            throw VerifyTokenError.invalidCode
        case .expired:
            throw VerifyTokenError.expired
        default:
            throw GenericRequestError.invalidResponse(nil)
        }
    }
    
}
