//
//  Date.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation
import Input

fileprivate struct Constants {
    static let endsToday = "dates.ending.today".localized
    static let ended = "dates.ending.ended".localized
    static let daysLeft = "dates.ending.daysLeft".localized
    static let dayLeft = "dates.ending.dayLeft".localized
    static let endsOn = "dates.ending.endsOn".localized
}

extension Date {
    func endDateString() -> String? {
        var formatter = DateHelper.shared.backendFormatter
            guard let daysLeft = Calendar.current.dateComponents([.day], from: Date(), to: self).day
            else { return nil }
        formatter = DateHelper.shared.endingFormatter
        if daysLeft < 0 {
            return Constants.ended
        }
        if daysLeft == 0 {
            return Constants.endsToday
        }
        if daysLeft == 1 {
            return "\(daysLeft) \(Constants.dayLeft)"
        }
        if daysLeft < 10 {
            return "\(daysLeft) \(Constants.daysLeft)"
        }
        return  "\(Constants.endsOn) \(formatter.string(from: self))"
    }
    
    func withCurrentTimeZoneOffset() -> Date {
        self.addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
    }
}
