//
//  FontFamily.swift
//  8Pitch
//
//  Created by 8pitch on 19.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//
import UIKit

extension UIFont {

    var errorSize : UIFont {
        self.withSize(9)
    }

    var markSize : UIFont {
        self.withSize(10)
    }

    var selectableBarSize: UIFont {
           self.withSize(11)
       }

    var textSize: UIFont {
        self.withSize(12)
    }

    var titleSize: UIFont {
        self.withSize(13)
    }

    var questionarrieAssetsTextSize: UIFont {
        self.withSize(14)
    }

    var labelSize: UIFont {
        self.withSize(15)
    }

    var placeholderSize: UIFont {
        self.withSize(16)
    }
    
    var imageSize: UIFont {
        self.withSize(20)
    }
    
    var projectNameSize: UIFont {
        self.withSize(22)
    }

    var infoTitleSize : UIFont {
        self.withSize(15)
    }
    
    var infoPageTitleSize : UIFont {
        self.withSize(22)
    }
    
    var statisticSize : UIFont {
        self.withSize(24)
    }

    var currencyInputSize: UIFont {
        self.withSize(28)
    }
    
    var totalAmoutCellSize: UIFont {
        self.withSize(22)
    }

    var largeTitleSize: UIFont {
        self.withSize(34)
    }
    
    var OTPSize : UIFont {
        self.withSize(38)
    }
    
    var numberPadSize: UIFont {
        self.withSize(40)
    }
    
    var amountSize: UIFont {
        self.withSize(42)
    }

    static let barlow = Barlow()
    static let roboto = Roboto()
    
    convenience init(named: String, size: CGFloat) {
        guard let _ = UIFont(name: named, size: size) else {
            self.init(named: UIFont.systemFont(ofSize: size).fontName, size: 10)
            return
        }
        self.init(name: named, size: size)!
    }
    
    struct Barlow {
        var regular = UIFont(named: "Barlow-Regular", size: 1.0)
        var bold = UIFont(named: "Barlow-Bold", size: 1.0)
        var semibold = UIFont(named: "Barlow-SemiBold", size: 1.0)
        var medium = UIFont(named: "Barlow-Medium", size: 1.0)
        var boldItalic = UIFont(named: "Barlow-BoldItalic", size: 1.0)
        var italic = UIFont(named: "Barlow-Italic", size: 1.0)
    }

    struct Roboto {
        var regular = UIFont(named: "Roboto-Regular", size: 1.0)
        var bold = UIFont(named: "Roboto-Bold", size: 1.0)
        var medium = UIFont(named: "Roboto-Medium", size: 1.0)
        var light = UIFont(named: "Roboto-Light", size: 1.0)
        var boldItalic = UIFont(named: "Roboto-BoldItalic", size: 1.0)
        var italic = UIFont(named: "Roboto-Italic", size: 1.0)
    }
    
}
