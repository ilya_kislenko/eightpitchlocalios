//
//  UIColor+Pallete.swift
//  8Pitch_LOG
//
//  Created by 8pitch on 14.07.2020.
//  Copyright © 2020 8pitch. All rights reserved.
//

import UIKit

enum Color: String {
    case borderWhite
    case darkShadow
    case lightShadow
    case white
    case gray
    case lightGray
    case answerText
    case red
    case textGray
    case lightRed
    case backgroundRed
    case placeholderGray
    case separatorGray
    case backgroundGray
    case backgroundDark
    case redText
    case backGroundCurrencyDark
    case green
    case viewGray
    case tapGray
    case grayShadow
    case grayCurrecy
    case answerBackground
    case switchGray
    case statisticsColor1
    case statisticsColor2
    case statisticsColor3
    case statisticsColor4
    case statisticsColor5
    case statisticsColor6
    case statisticsColor7
    case statisticsColor8
    case statisticsColor9
    case statisticsColor10
    case statisticsColor11
    case statisticsColor12
    case statisticsColor13
    case statisticsColor14
    case link
    case stateButtonGrayColor
    case halfWhite
    case tabBarWhite
    case lightGraphRed
    case projectGreen
    case projectGray
    case white80
    case projectParametrGray
    case halfGrayText
    case votingGray
    case separatorVotingGray
    case questionGray
    case questionHintRed
}

extension UIColor {
    convenience init(_ name: Color) {
        guard let _ = UIColor(named: name.rawValue) else {
            self.init(white: 1.0, alpha: 1.0)
            return
        }
        self.init(named: name.rawValue)!
    }
}

extension UIColor {
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        let hexString: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
