//
//  UITableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 20.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

// MARK: Identifier

@objc public extension UITableViewCell {
    
    static var reuseIdentifier: String { String(describing: self) }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
}

