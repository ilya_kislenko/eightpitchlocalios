//
//  UIView.swift
//  8Pitch
//
//  Created by 8pitch on 8/26/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//
import UIKit

extension UIView {
    
    class func fromNib<T : UIView>() -> T {
        guard let view = Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)?[0] as? T else {
            fatalError("No \(String(describing: T.self)) found in bundle")
        }
        return view
    }
    
    func roundView(radius: CGFloat, on corners: UIRectCorner) {
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height:  radius))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    func scrollToTop(in view: UIView) {
        view.subviews.forEach{
            scrollToTop(in: $0)
        }
        
        if let scrollView = view as? UIScrollView {
            scrollView.scrollToTop()
        }
    }
}
