//
//  ScaledHeightImageView.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ScaledHeightImageView: UIImageView {
    public override var intrinsicContentSize: CGSize {
        previousLayoutWidth = bounds.width

        guard let image = self.image else {
            return super.intrinsicContentSize
        }
        
        return CGSize(
            width: bounds.width,
            height: bounds.width / image.size.aspectRatio
        )
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        if previousLayoutWidth != bounds.width {
            invalidateIntrinsicContentSize()
        }
    }

    private var previousLayoutWidth: CGFloat = 0
}

extension CGSize {
    public var aspectRatio: CGFloat {
        return width / height
    }
}
