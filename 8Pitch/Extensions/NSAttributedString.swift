//
//  NSAttributedString.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation
import UIKit

fileprivate struct Constants {
    static let defaultFontSize = 14
}
public extension NSAttributedString {
    static func withParameters(text: String?,
                      color: String?,
                      font: String?,
                      fontSize: Int?,
                      isBold: Bool,
                      isUnderlined: Bool,
                      isItelic: Bool) -> NSAttributedString? {
        guard let text = text else { return nil }
        var attributes = [NSAttributedString.Key: Any]()
        enum ColorsString: String {
            case black
            case red
            case green
        }
        if let color = color {
            switch ColorsString(rawValue: color) {
            case .black:
                attributes[.foregroundColor] = UIColor(.backGroundCurrencyDark)
            case .green:
                attributes[.foregroundColor] = UIColor(.green)
            case .red:
                attributes[.foregroundColor] = UIColor(.redText)
            default:
                attributes[.foregroundColor] = UIColor(.textGray)
            }
        }

        if isUnderlined {
            attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
        }
        var fontNormalized = UIFont.roboto.regular
        if let _ = font?.contains("Roboto") {
            if isBold && isItelic {
                fontNormalized = UIFont.roboto.boldItalic
            } else if isBold {
                fontNormalized = UIFont.roboto.bold
            } else if isItelic {
                fontNormalized = UIFont.roboto.italic
            } else {
                fontNormalized = UIFont.roboto.regular
            }
        }

        if let _ = font?.contains("Barlow") {
            if isBold && isItelic {
                fontNormalized = UIFont.barlow.boldItalic
            } else if isBold {
                fontNormalized = UIFont.barlow.bold
            } else if isItelic {
                fontNormalized = UIFont.barlow.italic
            } else {
                fontNormalized = UIFont.barlow.regular
            }
        }
        attributes[.font] = fontNormalized.withSize(CGFloat(fontSize ?? Constants.defaultFontSize))

        return NSAttributedString(string: text, attributes: attributes)
    }
}


