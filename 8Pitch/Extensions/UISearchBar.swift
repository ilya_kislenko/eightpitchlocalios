//
//  UISearchBar.swift
//  8Pitch
//
//  Created by 8Pitch on 3.02.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

extension UISearchBar {
    func enableCancelButton(in view: UIView) {

        view.subviews.forEach {
            enableCancelButton(in: $0)
        }

        if let cancelButton = view as? UIButton {
            cancelButton.isEnabled = true
            cancelButton.isUserInteractionEnabled = true
        }
    }
}
