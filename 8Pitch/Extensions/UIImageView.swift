//
//  UIImageView.swift
//  8Pitch
//
//  Created by 8pitch on 2/10/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

extension UIImageView {
    
    public func setSize(id: String, offset: CGFloat) {
        self.snp.makeConstraints {
            let width = UIScreen.main.bounds.width - offset
            $0.width.equalTo(width)
        }
        self.layoutIfNeeded()
    }
}
