//
//  Numeric.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation

extension Formatter {
    static let number = NumberFormatter()
}

extension Numeric {
    func formatted(with groupingSeparator: String? = nil, style: NumberFormatter.Style = .decimal, locale: Locale = .init(identifier: "de_DE")) -> String {
        Formatter.number.locale = .current
        Formatter.number.numberStyle = style
        if let groupingSeparator = groupingSeparator {
            Formatter.number.groupingSeparator = groupingSeparator
        }
        return Formatter.number.string(for: self) ?? ""
    }
    
    var currency: String { formatted() }
}
