//
//  RxSwift+Disposable.swift
//  8Pitch
//
//  Created by 8pitch on 30.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import RxSwift

extension Disposable {
    
    func disposed(by bag: DisposeBag?) {
        guard let bag = bag else { return }
        self.disposed(by: bag)
    }
    
}

extension DisposeBag {
    
    func insert(_ disposable: Disposable?) {
        guard let disposable = disposable else {
            return
        }
        self.insert(disposable)
    }
    
}
