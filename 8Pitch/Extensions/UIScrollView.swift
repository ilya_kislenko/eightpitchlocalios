//
//  UIScrollView.swift
//  8Pitch
//
//  Created by 8Pitch on 25.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
   }
}
