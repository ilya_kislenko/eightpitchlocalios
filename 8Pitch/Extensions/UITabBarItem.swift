//
//  UITabBarItem.swift
//  8Pitch
//
//  Created by 8pitch on 8/25/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

extension UITabBarItem {
    func setupAppearance() {
        setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(.tabBarWhite), NSAttributedString.Key.font: UIFont.roboto.regular.markSize], for: .normal)
        setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(Color.red), NSAttributedString.Key.font: UIFont.roboto.regular.markSize], for: .selected)
    }
}
