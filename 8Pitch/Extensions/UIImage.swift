//
//  UIImage.swift
//  8Pitch
//
//  Created by 8pitch on 19.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

enum Image: String {
    
    case mail
    case arrowBack
    case info
    case closeButton
    case investorInstitutional
    case investorPrivate
    case line
    case select
    case splash
    case background
    case logo
    case resend
    case normal
    case disabled
    case pressed
    case focused
    case visibilityOn
    case visibilityOff
    case progress
    case checkOff
    case checkOn
    case search
    case calendar
    case close
    case whiteClose
    case edit
    case KYCcalendar
    case KYCflag
    case KYCiban
    case KYCnationality
    case KYCnumber
    case KYCowner
    case KYCpin
    case KYCuser
    case KYCzip
    case camera
    case waitingForAgent
    case successKYC
    case projectPlaceholder
    case projects
    case projectsSelected
    case dashboard
    case dashboardSelected
    case notifications
    case notificationsSelected
    case profile
    case profileSelected
    case checkOnWhite
    case checkOffWhite
    case Close
    case load
    case copy
    case klarna
    case manualTransfer
    case onlinePayment
    case secupay
    case arrowUp
    case arrowDown
    case profileCard
    case profileHelp
    case profileLegal
    case profileLogout
    case profileSecurity
    case profileSettings
    case prefix
    case company
    case companyType
    case downloadImagePlaceholder
    case placeholderImage
    case profilePhone
    case profileMail
    case profileDelete
    case profileLaw
    case profileTaxes
    case profilePin
    case dashboardInitiatorBackground
    case dashboardInvestorBackground
    case dashboardInfo
    case initiatorPreview
    case initiatorPreview2
    case initiatorPreview3
    case investorPreview
    case investorPreview2
    case investorPreview3
    case firstLevel = "1lvl"
    case secondLevel = "2lvl"
    case newsletters
    case isin
    case amountOfTokens
    case transactionID
    case projectsPlaceholderImage
    case boldEuro
    case profilePayments
    case twoFA
    case profilePassword
    case averageInvestment
    case euro
    case clock
    case numberOfInvestors
    case hight
    case low
    case typeOfSecurity
    case minimumInvestmentAmount
    case dsoProjectStartDate
    case dsoProjectFinishDate
    case goal
    case euroIcon
    case softCap
    case factsIsin
    case financingPurpose
    case investmentSeries
    case country
    case guarantee
    case wkin
    case conversionRight
    case issuedShares
    case futureShareNominalValue
    case inverestInterval
    case dividend
    case highOfDividend
    case initialSalesCharge
    case typeOfRepayment
    case pinCode
    case touchID
    case faceID
    case touchIDSuccess
    case faceIDSuccess
    case backspace
    case pinCodeSuccess
    case faceIDLogin
    case touchIDLogin
    case KYCStreet
    case agentAnswered
    case faceIDButton
    case touchIDButton
    case userRecieved
    case userSent
    case amountInEuro
    case amountInToken
    case noPayments
    case invest
    case taxID
    case taxOffice
    case church
    case churchTax
    case registrationNumber
    case glyph
    case glyphFilled
    case searchIcon
    case security
    case briefcase
    case tokenNew
    case votingButton
    case reject
    case approved
    case radioOn
    case radioOff
    case checkboxOff
    case checkboxOn
    case chevronUp
    case chevronDown
    case votingInfo
    case whiteLoad
    case checkboxResult
    case radioResult
    case votingLoad
    case votingStream
    case streamErrorTime
    case streamErrorState
    case errorRepeate
    case errorTerm
    case votingPendingCellStart
    case votingPendingCellEnd
    case votingPeriod
    case votingDate
    case votingUser

    static func splashStep(for index: Int) -> String {
        guard let _ = UIImage(named: "splash\(index)") else {
            // show info image wherever there's a missing step image
            return info.rawValue
        }
        return "splash\(index)"
    }
    
}

extension UIImage {
    
    convenience init(_ name: Image) {
        guard let _ = UIImage(named: name.rawValue) else {
            self.init(named: Image.Close.rawValue)!
            return
        }
        self.init(named: name.rawValue)!
    }
}

extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
