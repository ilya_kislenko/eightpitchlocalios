//
//  String+Localizable.swift
//  8Pitch
//
//  Created by 8pitch on 16.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    enum Constants {
        static let flagValue: UInt32 = 127397
    }
    
    func flag() -> String? {
        guard Locale.isoRegionCodes.firstIndex(where: { self == $0 }) != nil else {
            return nil
        }
        return String(String.UnicodeScalarView(self.unicodeScalars.compactMap(
                                                { UnicodeScalar(Constants.flagValue + $0.value) })))
    }
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, attributes: [NSAttributedString.Key : Any]?) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        return ceil(boundingBox.height)
    }
    
    func width(constrainedBy height: CGFloat, with font: UIFont) -> CGFloat {
        let constrainedRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constrainedRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return boundingBox.width
    }
}

extension String {
    func localizedRange() -> String? {
        switch self {
        case "RANGE_0_1000_EUR":
            return "\(0.currency)-\(1000.currency) EUR"
        case "RANGE_1001_5000_EUR":
            return "\(1001.currency)-\(5000.currency) EUR"
        case "RANGE_5001_10000_EUR":
            return "\(5001.currency)-\(10000.currency) EUR"
        case "RANGE_10001_25000_EUR":
            return "\(10001.currency)-\(25000.currency) EUR"
        case "RANGE_25001_50000_EUR":
            return "\(25001.currency)-\(50000.currency) EUR"
        case "RANGE_50001_100000_EUR":
            return "\(50001.currency)-\(100000.currency) EUR"
        case "RANGE_MORE_THAN_100000_EUR":
            return "RANGE_MORE_THAN_100000_EUR".localized + "\(100000.currency) EUR"
        default:
            return nil
        }
    }
}

extension String {
    var numeric: NSNumber? { self.formatted() }
    
    func formatted() -> NSNumber? {
        Formatter.number.locale = .current
        Formatter.number.numberStyle = .decimal
        return Formatter.number.number(from: self)
    }
}

