//
//  UINavigationItem.swift
//  8Pitch
//
//  Created by 8pitch on 22.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

extension UINavigationItem {
    
    func customizeForWhite() {
        self.leftBarButtonItem?.tintColor = UIColor(.gray)
        self.rightBarButtonItem?.tintColor = UIColor(.gray)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        self.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor(.gray), NSAttributedString.Key.font: UIFont.barlow.semibold.titleSize, .kern: 0.3, .paragraphStyle: paragraphStyle], for: .normal)
        self.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor(.gray), NSAttributedString.Key.font: UIFont.barlow.semibold.titleSize, .kern: 0.3, .paragraphStyle: paragraphStyle], for: .selected)
    }
    
    func customizeForFilters() {
        self.leftBarButtonItem?.tintColor = UIColor(.gray)
        self.rightBarButtonItem?.tintColor = UIColor(.red)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        self.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor(.red), NSAttributedString.Key.font: UIFont.barlow.semibold.titleSize, .kern: 0.3, .paragraphStyle: paragraphStyle], for: .normal)
        self.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor(.red), NSAttributedString.Key.font: UIFont.barlow.semibold.titleSize, .kern: 0.3, .paragraphStyle: paragraphStyle], for: .highlighted)
    }
    
    func customizeForColored() {
        self.leftBarButtonItem?.tintColor = UIColor(.white)
    }
    
}
