//
//  AppDelegate.swift
//  8Pitch
//
//  Created by 8pitch on 14.07.2020.
//

import UIKit
import Input
import FirebaseCore
import MobileRTC
import MobileCoreServices

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.setupSplashScreen()
        self.setupFirebase()
        self.setupZoom()
        return true
    }
}

extension AppDelegate {
    func setupSplashScreen() {
        guard let initialNavigationController = BiometryManager.shared.isQuickLoginSet ?
                ViewControllerProvider.setupLoginPinViewController() :
                self.window?.rootViewController as? UINavigationController,
              let rootViewController = initialNavigationController.topViewController as? GenericViewController else {
            return
        }
        rootViewController.splashController = ViewControllerProvider.setupSplashViewController(rootViewController: rootViewController)
    }
}

extension AppDelegate {
    func setupFirebase() {
        FirebaseApp.configure()
    }
}

extension AppDelegate: MobileRTCAuthDelegate {
    func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
        if (returnValue != .success) {
            let msg = "SDK authentication failed, error code: \(returnValue)"
            print(msg)
        }
    }
    
    func setupZoom() {
        let mainSDK = MobileRTCSDKInitContext()
        mainSDK.domain = "zoom.us"
        MobileRTC.shared().initialize(mainSDK)
        let authService = MobileRTC.shared().getAuthService()
        authService?.delegate = self
        authService?.clientKey = "bbTbhmNoLBbXJdTOZBMYRf36i57WiXizLodi"
        authService?.clientSecret = "TaO4EaGFfw33bwxHpQgvHz0l8kReXFAv5VOa"
        authService?.sdkAuth()
    }
}

