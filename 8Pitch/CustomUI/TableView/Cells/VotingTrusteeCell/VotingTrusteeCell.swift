//
//  VotingTrusteeCell.swift
//  8Pitch
//
//  Created by 8pitch on 4/6/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class VotingTrusteeCell: UITableViewCell {
    
    @IBOutlet weak var startHintLabel: VotingHintLabel!
    @IBOutlet weak var endHintLabel: VotingHintLabel!
    @IBOutlet weak var startLabel: PersonalHeaderLabel!
    @IBOutlet weak var endLabel: PersonalHeaderLabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.startLabel.text = nil
        self.endLabel.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.startHintLabel.text = "voting.approval.hint.start".localized
        self.endHintLabel.text = "voting.approval.hint.end".localized
    }
    
    func setup(with voting: Voting?) {
        if let voting = voting,
           let start = voting.startDate,
           let end = voting.endDate {
            self.startLabel.text = DateHelper.shared.uiFormatterFull.string(from: start)
            self.endLabel.text = DateHelper.shared.uiFormatterFull.string(from: end)
        }
    }
}
