//
//  PaymentHistoryCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/30/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class PaymentHistoryCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: InfoTitleLabel!
    @IBOutlet weak var valueLabel: InfoLabel!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.trailingConstraint.constant = Constants.separatorOffset
        self.leadingConstraint.constant = Constants.separatorOffset
    }

    func setupFor(payment: UserTransaction) {
        guard let date = payment.transactionDate,
            let amount = payment.amount else {
            return
        }
        self.dateLabel.text = DateHelper.shared.uiFormatterShort.string(from: date)
        self.valueLabel.text = "\(amount.doubleValue.currency) \(payment.shortCut)"
    }
    
    func setupLastSeparator(_ last: Bool) {
        guard last else { return }
        self.trailingConstraint.constant = 0
        self.leadingConstraint.constant = 0
    }
    
    private enum Constants {
        static let separatorOffset: CGFloat = 16
    }
    
}
