//
//  OpenVotingCell.swift
//  8Pitch
//
//  Created by 8pitch on 24.03.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class OpenVotingCell: UITableViewCell {
    
    @IBOutlet weak var title: InfoTitleLabel!
    @IBOutlet weak var picture: UIImageView!
    
    func setup(for type: ProjectVotingsViewController.VotingCell) {
        switch type {
        case .loadAnswers, .loadResults, .popUp:
            self.picture.image = UIImage(.votingLoad)
            self.title.text = "votings.open.answer.title".localized
        case .stream:
            self.picture.image = UIImage(.votingStream)
            self.title.text = "voting.load.title.stream".localized
        case .openPass,
             .openApprove,
             .plug:
            return
        }
    }
}
