//
//  VotingOptionsCell.swift
//  8Pitch
//
//  Created by 8pitch on 12.03.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

enum VotingQuestionState {
    case unchecked
    case checked
}

class VotingQuestionsContainerCell: UITableViewCell {
    weak var delegate: PassVotingViewProtocol?
    @IBOutlet weak var tableView: SelfSizingTableView!
    private var questions: [Question] = []
    private var isEditable: Bool = false
    
    func setup(with questions: [Question], editable: Bool) {
        self.isEditable = editable
        self.questions = questions
        self.setupLayout(with: questions)
        self.tableView.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.register(VotingQuestionOptionCell.nibForRegistration, forCellReuseIdentifier: VotingQuestionOptionCell.reuseIdentifier)
        tableView.register(VotingQuestionHeader.nibForRegistration, forHeaderFooterViewReuseIdentifier: VotingQuestionHeader.reuseIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isScrollEnabled = false
    }
    
    private func setupLayout(with questions: [Question]) {
        guard !questions.isEmpty else { return }
        var size: CGFloat = 0
        var cellSize: CGFloat = 0
        var cellCount: Int = 0
        questions.forEach {
            size += VotingLabel().height(for: $0.title, with: Constants.headerLabelWidth)
            size += ($0.type == .checkbox ? Constants.headerHeight : Constants.headerSmallHeight)
            $0.options.forEach { option in
                cellSize += InfoCurrencyLabel().height(for: option.answer, with: Constants.rowLabelWidth)
                cellSize += Constants.rowInset
            }
            cellCount += $0.options.count
        }
        self.tableView.estimatedRowHeight = cellSize/CGFloat(cellCount)
        self.tableView.estimatedSectionHeaderHeight = size/CGFloat(questions.count)
    }
    
    private enum Constants {
        static let rowInset: CGFloat = 24
        static let rowLabelWidth = UIScreen.main.bounds.width - 86
        static let headerLabelWidth = UIScreen.main.bounds.width - 48
        static let headerHeight: CGFloat = 71
        static let headerSmallHeight: CGFloat = 31
        static let separatorHeight: CGFloat = 1
        static let separatorInset: CGFloat = 24
    }
}

extension VotingQuestionsContainerCell: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let question = questions[section]
        return question.options.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: VotingQuestionHeader.reuseIdentifier) as? VotingQuestionHeader
        view?.contentView.backgroundColor = .white
        view?.setup(for: questions[section], filled: self.isEditable)
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor(.white)
        let separator = UIView()
        separator.backgroundColor = UIColor(.separatorVotingGray)
        view.addSubview(separator)
        separator.snp.makeConstraints {
            $0.height.equalTo(Constants.separatorHeight)
            $0.leading.trailing.equalToSuperview().inset(Constants.separatorInset)
            $0.centerY.equalToSuperview()
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: VotingQuestionOptionCell.reuseIdentifier) as? VotingQuestionOptionCell {
            let question = questions[indexPath.section]
            cell.setupWith(model: question.options[indexPath.row], type: question.type)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard isEditable else { return }
        let options = questions[indexPath.section].options
        let option = options[indexPath.row]
        option.selected = !option.selected
        if option.selected && questions[indexPath.section].type == .radiobutton {
            options.forEach {
                $0.selected = $0.id == option.id ? true : false
            }
        }
        self.tableView.reloadSections(IndexSet(integer: indexPath.section), with: .automatic)
        self.delegate?.reloadFooter()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        Constants.separatorHeight
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let question = questions[section]
        let text = questions[section].title
        let height = VotingLabel().height(for: text, with: Constants.headerLabelWidth)
        let inset = question.type == .checkbox ? Constants.headerHeight : Constants.headerSmallHeight
        return height + inset
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let text = questions[indexPath.section].options[indexPath.row].answer
        let rowHeight = InfoCurrencyLabel().height(for: text, with: Constants.rowLabelWidth)
        return rowHeight + Constants.rowInset
    }
}
