//
//  VotingQuestionOptionCell.swift
//  scrollableLabelsLine
//
//  Created by 8pitch on 11.03.21.
//

import UIKit
import Input

class VotingQuestionOptionCell: UITableViewCell {
        
    @IBOutlet weak var typeIcon: UIImageView!
    @IBOutlet weak var optionText: InfoCurrencyLabel!
    
    override func awakeFromNib() {
        self.selectionStyle = .none
    }
    
    func setupWith(model: Option, type: Question.QuestionType) {
        self.optionText.text = model.answer
        switch type {
        case .checkbox:
            typeIcon.image = model.selected ? UIImage(.checkboxOn) : UIImage(.checkboxOff)
        case .radiobutton:
            typeIcon.image = model.selected ? UIImage(.radioOn) : UIImage(.radioOff)
        }
    }
}
