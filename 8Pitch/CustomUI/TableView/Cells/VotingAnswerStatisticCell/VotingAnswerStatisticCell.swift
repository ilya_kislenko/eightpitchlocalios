//
//  VotingAnswerStatisticCell.swift
//  8Pitch
//
//  Created by 8pitch on 3/17/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class VotingAnswerStatisticCell: UITableViewCell {
    @IBOutlet weak var titleLabel: SumLabel!
    @IBOutlet weak var valueLabel: PercentLabel!
    @IBOutlet weak var separator: UIView!
    
    func setup(for title: String, value: Double, last: Bool) {
        self.titleLabel.text = title
        self.valueLabel.text = "\(Int(value)) %"
        self.separator.isHidden = last
    }
}
