//
//  SummarizeCell.swift
//  8Pitch
//
//  Created by 8pitch on 09.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

public struct SummaryData {
    let image: UIImage?
    let key: String
    public let value: String?
}

import UIKit

class SummarizeCell: UITableViewCell {
    
    // MARK: UI Components
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var keyLabel: InfoTitleLabel!
    @IBOutlet weak var valueLabel: InfoLabel!
    @IBOutlet weak var separator: SeparatorView!
    
    func setupWith(_ data: SummaryData) {
        self.icon.image = data.image
        self.keyLabel.text = data.key
        self.valueLabel.text = data.value
        let keyWidth = self.keyLabel.intrinsicContentSize.width
        let valueWidth = self.keyLabel.intrinsicContentSize.width
        self.keyLabel.setContentCompressionResistancePriority(keyWidth < valueWidth ? UILayoutPriority(rawValue: 750) : UILayoutPriority(rawValue: 752), for: .horizontal)
        self.keyLabel.sizeThatFits(self.keyLabel.intrinsicContentSize)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.separator.isHidden = false
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func hideSeparator() {
        self.separator.isHidden = true
    }
    
    func updateSeparatorForLastRow(_ last: Bool) {
        guard last else { return }
        self.separator.snp.makeConstraints {
            $0.trailing.leading.equalToSuperview()
        }
    }
}
