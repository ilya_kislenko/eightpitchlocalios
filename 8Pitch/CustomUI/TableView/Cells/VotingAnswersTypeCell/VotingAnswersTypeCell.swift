//
//  VotingAnswersTypeCell.swift
//  8Pitch
//
//  Created by 8pitch on 3/17/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class VotingAnswersTypeCell: UITableViewCell {
    
    enum CellType: String {
        case perParticipant = "voting.result.answer.participant"
        case effectiveVotes = "voting.result.answer.vote"
    }
    
    @IBOutlet weak var titleLabel: VotingAnswerLabel!
    @IBOutlet weak var questionTypeImageView: UIImageView!
    
    func setup(for cellType: CellType, questionType: Question.QuestionType) {
        self.titleLabel.text = cellType.rawValue.localized
        self.questionTypeImageView.isHidden = cellType == .effectiveVotes
        self.questionTypeImageView.image = questionType == .radiobutton ? UIImage(.radioResult) : UIImage(.checkboxResult)
    }
}
