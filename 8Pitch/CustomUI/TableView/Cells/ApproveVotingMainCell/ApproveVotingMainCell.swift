//
//  ApproveVotingMainCell.swift
//  8Pitch
//
//  Created by 8pitch on 3/9/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class ApproveVotingMainCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: CustomAssetTitleLabel!
    
    func setup(description: String?) {
        self.descriptionLabel.text = description
        self.descriptionLabel.setNeedsLayout()
        self.descriptionLabel.layoutIfNeeded()
        self.descriptionLabel.sizeThatFits(self.descriptionLabel.intrinsicContentSize)
    }
    
}
