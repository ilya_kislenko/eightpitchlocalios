//
//  ProjectDetailsMainFactsPadTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 12/21/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ProjectDetailsMainFactsPadTableViewCell: ProjectDetailsMainFactsItemTableViewCell {
    @IBOutlet weak var leftIcon: UIImageView!
    @IBOutlet weak var rightIcon: UIImageView!
    @IBOutlet weak var leftTitle: StatisticsTitleLabel!
    @IBOutlet weak var rightTitle: StatisticsTitleLabel!
    @IBOutlet weak var leftValue: ProjectParameterLabel!
    @IBOutlet weak var rightValue: ProjectParameterLabel!
    
    override func setupForPad(items: [MainFactItem?]) {
        super.setupForPad(items: items)
        self.leftIcon.image = items.first??.icon ?? nil
        self.rightIcon.image = items.last??.icon ?? nil
        self.leftTitle.text = items.first??.title ?? nil
        self.rightTitle.text = items.last??.title ?? nil
        self.leftValue.text = items.first??.content ?? nil
        self.rightValue.text = items.last??.content ?? nil
    }
}
