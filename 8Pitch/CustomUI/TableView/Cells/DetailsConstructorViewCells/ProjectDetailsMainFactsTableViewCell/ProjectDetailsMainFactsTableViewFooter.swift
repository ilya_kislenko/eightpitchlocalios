//
//  ProjectDetailsMainFactsTableViewFooter.swift
//  8Pitch
//
//  Created by 8pitch on 11/12/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

fileprivate struct Constants {
    static let viewMore = "blockConstructor.mainFacts.viewMore".localized
    static let viewLess = "blockConstructor.mainFacts.viewLess".localized
    static let borderWidth = CGFloat(1)
    static let cornerRadius = CGFloat(4)
    static let insetShare = CGFloat(0.15)
    static let padTopInset = CGFloat(40)
    static let topInset = CGFloat(24)
    static let arrowWidth = CGFloat(12)
    static let arrowHeight = CGFloat(6)
}

protocol ProjectDetailsMainFactsTableViewFooterDelegate: class {
    func updateExpandableCell()
}

class ProjectDetailsMainFactsTableViewFooter: UITableViewHeaderFooterView {
    @IBOutlet var viewMoreButton: RoundedButton!
    @IBOutlet weak var trailingInset: NSLayoutConstraint!
    @IBOutlet weak var leadingInset: NSLayoutConstraint!
    @IBOutlet weak var topInset: NSLayoutConstraint!
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ProjectDetailsMainFactsTableViewFooter"
    }
    
    private var isCollapsed: Bool = true {
        didSet {
            let arrowImage = isCollapsed ? UIImage(.arrowDown) : UIImage(.arrowUp)
            let text = isCollapsed ? Constants.viewMore : Constants.viewLess
            let title = self.createAttributedTitle(for: text, image: arrowImage)
            self.viewMoreButton.setAttributedTitle(title, for: .normal)
        }
    }
    
    weak var delegate: ProjectDetailsMainFactsTableViewFooterDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setCollapsed()
        self.viewMoreButton.layer.borderWidth = Constants.borderWidth
        self.viewMoreButton.layer.borderColor = UIColor(.backGroundCurrencyDark).cgColor
        self.viewMoreButton.layer.cornerRadius = Constants.cornerRadius
        let newInset = UIDevice.current.userInterfaceIdiom == .pad ?
            UIScreen.main.bounds.width * Constants.insetShare : self.trailingInset.constant
        self.trailingInset.constant = newInset
        self.leadingInset.constant = newInset
        self.topInset.constant = UIDevice.current.userInterfaceIdiom == .pad ? Constants.padTopInset : Constants.topInset
    }
    
    private func setCollapsed() {
        let title = self.createAttributedTitle(for: Constants.viewMore, image: UIImage(.arrowDown))
        self.viewMoreButton.setAttributedTitle(title, for: .normal)
    }
    
    private func createAttributedTitle(for text: String, image: UIImage) -> NSMutableAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = image.withRenderingMode(.alwaysTemplate)
        attachment.bounds = CGRect(x: 0,
                                   y: ((UIFont.barlow.semibold.placeholderSize.capHeight).rounded() - Constants.arrowHeight) / 2,
                                   width: Constants.arrowWidth,
                                   height: Constants.arrowHeight)
        let attachmentString = NSAttributedString(attachment: attachment)
        let text = NSMutableAttributedString(string: text)
        text.append(attachmentString)
        text.addAttribute(.foregroundColor, value: UIColor(.backGroundCurrencyDark), range: NSRange(location: 0, length: text.length))
        return text
    }
    
    @IBAction func expandAction(_ sender: Any) {
        self.isCollapsed.toggle()
        self.delegate?.updateExpandableCell()
    }
}
