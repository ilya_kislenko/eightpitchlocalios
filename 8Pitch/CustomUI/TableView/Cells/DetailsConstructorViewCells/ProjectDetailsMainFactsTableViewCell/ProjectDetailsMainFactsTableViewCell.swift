//
//  ProjectDetailsMainFactsTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 11/12/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

struct MainFactItem {
    var icon: UIImage?
    var title: String?
    var content: String?
}

fileprivate struct Constants {
    static let defaultNumberOfItemsShown = 6
    static let typeOfSecurity = "blockConstructor.mainFacts.typeOfSecurity".localized.uppercased()
    static let minimumInvestmentAmount = "blockConstructor.mainFacts.minimumInvestmentAmount".localized.uppercased()
    static let dsoProjectStartDate = "blockConstructor.mainFacts.dsoProjectStartDate".localized.uppercased()
    static let dsoProjectFinishDate = "blockConstructor.mainFacts.dsoProjectFinishDate".localized.uppercased()
    static let goal = "blockConstructor.mainFacts.goal".localized.uppercased()
    static let softcap = "blockConstructor.mainFacts.softCap".localized.uppercased()
    static let isin = "blockConstructor.mainFacts.isin".localized.uppercased()
    static let financingPurpose = "blockConstructor.mainFacts.financingPurpose".localized.uppercased()
    static let investmentSeries = "blockConstructor.mainFacts.investmentSeries".localized.uppercased()
    static let country = "blockConstructor.mainFacts.country".localized.uppercased()
    static let guarantee = "blockConstructor.mainFacts.guarantee".localized.uppercased()
    static let wkin = "blockConstructor.mainFacts.wkin".localized.uppercased()
    static let agio = "blockConstructor.mainFacts.agio".localized.uppercased()
    static let conversionRight = "blockConstructor.mainFacts.conversionRight".localized.uppercased()
    static let issuedShares = "blockConstructor.mainFacts.issuedShares".localized.uppercased()
    static let futureShareNominalValue = "blockConstructor.mainFacts.futureShareNominalValue".localized.uppercased()
    static let interest = "blockConstructor.mainFacts.interest".localized.uppercased()
    static let inverestInterval = "blockConstructor.mainFacts.inverestInterval".localized.uppercased()
    static let dividend = "blockConstructor.mainFacts.dividend".localized.uppercased()
    static let highOfDividend = "blockConstructor.mainFacts.highOfDividend".localized.uppercased()
    static let initialSalesCharge = "blockConstructor.mainFacts.initialSalesCharge".localized.uppercased()
    static let typeOfRepayment = "blockConstructor.mainFacts.typeOfRepayment".localized.uppercased()
    static let euro = "projects.nominal.currency.euro".localized.uppercased()
    static let viewMore = "blockConstructor.mainFacts.viewMore".localized.uppercased()
    static let padTopInset = CGFloat(48)
    static let topInset = CGFloat(26)
    static let zeroString = "0"
}

class ProjectDetailsMainFactsTableViewCell: UITableViewCell {
    @IBOutlet var mainFactsTableView: DynamicHeightTableView!
    @IBOutlet weak var topInset: NSLayoutConstraint!
    private var mainFactsItems: [MainFactItem] = []
    fileprivate var isExpanded: Bool = false
    weak var delegate: ProjectDetailsExpandableCellDelegate?
    
    private var allItems: Int {
        let padItemsCount = mainFactsItems.count % 2 == 0 ? mainFactsItems.count/2 : (mainFactsItems.count + 1)/2
        return UIDevice.current.userInterfaceIdiom == .pad ? padItemsCount : mainFactsItems.count
    }
    
    private var shortItems: Int {
        UIDevice.current.userInterfaceIdiom == .pad ? Constants.defaultNumberOfItemsShown/2 : Constants.defaultNumberOfItemsShown
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.topInset.constant = UIDevice.current.userInterfaceIdiom == .pad ? Constants.padTopInset : Constants.topInset
    }
    
    func setup(project: Project?, projectInfo: ExpandedProject?) {
        self.setupMainFactsItems(project: project, projectInfo: projectInfo)
        self.mainFactsTableView.delegate = self
        self.mainFactsTableView.dataSource = self
        self.mainFactsTableView.register(ProjectDetailsMainFactsPadTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsMainFactsPadTableViewCell.reuseIdentifier)
        self.mainFactsTableView.register(ProjectDetailsMainFactsItemTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsMainFactsItemTableViewCell.reuseIdentifier)
        self.mainFactsTableView.register(ProjectDetailsMainFactsTableViewFooter.nibForRegistration, forHeaderFooterViewReuseIdentifier: ProjectDetailsMainFactsTableViewFooter.reuseIdentifier)
        self.mainFactsTableView.reloadData()
    }
    
    private func setupMainFactsItems(project: Project?, projectInfo: ExpandedProject?) {
        let formatter = DateHelper.shared.uiFormatterShort
        let finInfo = projectInfo?.financialInformation
        let document = project?.tokenParametersDocument
        let fields = projectInfo?.additionalFields
        
        let typeOfSecurity = MainFactItem(icon: UIImage(.typeOfSecurity), title: Constants.typeOfSecurity,
                                          content: finInfo?.typeOfSecurity)
        
        var amount: String = ""
        if let minAmount = document?.minimumInvestmentAmount,
           let nominalValue = document?.nominalValue {
            amount = "\((minAmount * nominalValue).currency) \(Constants.euro)"
        }
        let minimumInvestmentAmount = MainFactItem(icon: UIImage(.euroIcon), title: Constants.minimumInvestmentAmount, content: amount)
        
        let dsoProjectStartDate = MainFactItem(icon: UIImage(.dsoProjectStartDate), title: Constants.dsoProjectStartDate,
                                               content: document?.dsoProjectStartDate == nil ? nil : formatter.string(from: document?.dsoProjectStartDate ?? Date()))
        let dsoProjectFinishDate = MainFactItem(icon: UIImage(.dsoProjectFinishDate), title: Constants.dsoProjectFinishDate,
                                                content: document?.dsoProjectFinishDate == nil ? nil : formatter.string(from: document?.dsoProjectFinishDate ?? Date()))
        
        self.mainFactsItems = UIDevice.current.userInterfaceIdiom == .pad ?
            [typeOfSecurity, dsoProjectStartDate, minimumInvestmentAmount, dsoProjectFinishDate] :
            [typeOfSecurity, minimumInvestmentAmount, dsoProjectStartDate, dsoProjectFinishDate]
        self.mainFactsItems = self.mainFactsItems.filter { !($0.content?.isEmpty ?? true) }
        
        if let goal = finInfo?.goal,
           let nominalValue = document?.nominalValue {
            let goal = MainFactItem(icon: UIImage(.goal), title: Constants.goal, content: "\((goal * Int(nominalValue)).currency) \(Constants.euro)")
            self.mainFactsItems.append(goal)
            
            if let softcapValue = projectInfo?.graph?.softCap {
                let softCapAmount = softcapValue * (document?.nominalValue ?? 0)
                let softcap = MainFactItem(icon: UIImage(.softCap), title: Constants.softcap, content: "\(softCapAmount.currency) \(Constants.euro)")
                self.mainFactsItems.append(softcap)
            }
            
            let isin = MainFactItem(icon: UIImage(.factsIsin), title: Constants.isin, content: finInfo?.isin)
            let purpose = MainFactItem(icon: UIImage(.financingPurpose), title: Constants.financingPurpose, content: finInfo?.financingPurpose)
            let series = MainFactItem(icon: UIImage(.investmentSeries), title: Constants.investmentSeries, content: finInfo?.investmentSeries)
            let countryCode = finInfo?.country
            let country = MainFactItem(icon: UIImage(.country), title: Constants.country, content: countryCode?.countryName )
            let guarantee = MainFactItem(icon: UIImage(.guarantee), title: Constants.guarantee, content: finInfo?.guarantee)
            let wkin = MainFactItem(icon: UIImage(.wkin), title: Constants.wkin, content: fields?.wkin)
            let agio = MainFactItem(icon: UIImage(.dsoProjectStartDate), title: Constants.agio, content: fields?.agio?.isEmpty ?? true ? nil : "\(fields?.agio ?? "")%")
            let conversionRight = MainFactItem(icon: UIImage(.conversionRight), title: Constants.conversionRight, content: fields?.conversionRight)
            let issuedShares = MainFactItem(icon: UIImage(.issuedShares), title: Constants.issuedShares, content: fields?.issuedShares)
            let futureShareNominalValue = MainFactItem(icon: UIImage(.futureShareNominalValue), title: Constants.futureShareNominalValue, content: fields?.futureShareNominalValue?.isEmpty ?? true ? nil : "\(fields?.futureShareNominalValue ?? Constants.zeroString) \(Constants.euro)")
            let interest = MainFactItem(icon: UIImage(.dsoProjectFinishDate), title: Constants.interest, content: fields?.interest?.isEmpty ?? true ? nil : "\(fields?.interest ?? "")%")
            let inverestInterval = MainFactItem(icon: UIImage(.inverestInterval), title: Constants.inverestInterval, content: fields?.interestInterval)
            let dividend = MainFactItem(icon: UIImage(.dividend), title: Constants.dividend, content: fields?.dividend)
            let highOfDividend = MainFactItem(icon: UIImage(.highOfDividend), title: Constants.highOfDividend, content: fields?.highOfDividend)
            let initialSalesCharge = MainFactItem(icon: UIImage(.initialSalesCharge), title: Constants.initialSalesCharge, content: fields?.initialSalesCharge)
            let typeOfRepayment = MainFactItem(icon: UIImage(.typeOfRepayment), title: Constants.typeOfRepayment, content: fields?.typeOfRepayment)
            let items = [isin,
                         purpose,
                         series,
                         country,
                         guarantee,
                         wkin,
                         agio,
                         conversionRight,
                         issuedShares,
                         futureShareNominalValue,
                         interest,
                         inverestInterval,
                         dividend,
                         highOfDividend,
                         initialSalesCharge,
                         typeOfRepayment]
            for field in projectInfo?.customAdditionalFields ?? [] {
                let fact = MainFactItem(icon: UIImage(.country), title: field?.fieldName, content: field?.fieldText)
                self.mainFactsItems.append(fact)
            }
            self.mainFactsItems.append(contentsOf: items.filter {
                $0.content != nil && !$0.content!.isEmpty
            } )
        }
    }
}

extension ProjectDetailsMainFactsTableViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.isExpanded ? min(self.allItems, self.mainFactsItems.count) : min(self.shortItems, self.mainFactsItems.count/2)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProjectDetailsMainFactsItemTableViewCell
        var index = indexPath.row
        if UIDevice.current.userInterfaceIdiom == .pad {
            cell = self.mainFactsTableView.dequeueReusableCell(withIdentifier: ProjectDetailsMainFactsPadTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsMainFactsPadTableViewCell
            index *= 2
            let nextIndex = index + 1
            let firstFact = self.mainFactsItems[index]
            let secondFact = nextIndex < self.mainFactsItems.count ? self.mainFactsItems[nextIndex] : nil
            cell.setupForPad(items: [firstFact, secondFact])
        } else {
            cell = self.mainFactsTableView.dequeueReusableCell(withIdentifier: ProjectDetailsMainFactsItemTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsMainFactsItemTableViewCell
            cell.setup(item: self.mainFactsItems[index])
        }
        return cell
    }
}

extension ProjectDetailsMainFactsTableViewCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = self.mainFactsTableView.dequeueReusableHeaderFooterView(withIdentifier: ProjectDetailsMainFactsTableViewFooter.reuseIdentifier) as! ProjectDetailsMainFactsTableViewFooter
        footer.delegate = self
        footer.viewMoreButton.isHidden = self.allItems == self.shortItems
        return footer
    }
}

extension ProjectDetailsMainFactsTableViewCell: ProjectDetailsMainFactsTableViewFooterDelegate {
    func updateExpandableCell() {
        self.isExpanded.toggle()
        self.mainFactsTableView.reloadData()
        self.delegate?.updateExpandableCell()
        self.delegate?.scrollToTop()
    }
}
