//
//  ProjectDetailsMainFactsItemTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 11/12/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ProjectDetailsMainFactsItemTableViewCell: UITableViewCell {
    @IBOutlet var typeIcon: UIImageView!
    @IBOutlet var titleLabel: StatisticsTitleLabel!
    @IBOutlet var contentLabel: CustomCurrencyLabel!

    func setup(item: MainFactItem?) {
        self.typeIcon.image = item?.icon
        self.titleLabel.text = item?.title
        self.contentLabel.text = item?.content
    }
    
    func setupForPad(items: [MainFactItem?]) {}
}
