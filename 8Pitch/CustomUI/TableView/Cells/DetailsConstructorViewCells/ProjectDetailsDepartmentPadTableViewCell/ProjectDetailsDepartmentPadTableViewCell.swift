//
//  ProjectDetailsDepartmentPadTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 12/21/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsDepartmentPadTableViewCell: ProjectDetailsDepartmentTableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var items: [DepartmentBlockPayloadItem?] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: Constants.reuseIdentifier)
    }
    
    override func setupForPad(_ payload: DepartmentBlockPayload?) {
        super.setupForPad(payload)
        self.items = payload?.items ?? []
        self.collectionView.reloadData()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.items = []
        self.collectionView.reloadData()
    }
}

extension ProjectDetailsDepartmentPadTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: Constants.cellWidth, height: Constants.cellHeight )
    }
}

extension ProjectDetailsDepartmentPadTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.reuseIdentifier, for: indexPath)
        guard let departmentView = DepartmentView.nib(owner: self) else { return cell }
        departmentView.delegate = self.delegate
        departmentView.setup(self.items[indexPath.row])
        cell.contentView.addSubview(departmentView)
        departmentView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview()
            $0.trailing.leading.equalToSuperview().inset(Constants.inset)
        }
        return cell
    }
    
    enum Constants {
        static let reuseIdentifier = "UICollectionViewCell"
        static let inset: CGFloat = 12
        static let cellHeight: CGFloat = 416
        static let cellWidth: CGFloat = 295
    }
}
