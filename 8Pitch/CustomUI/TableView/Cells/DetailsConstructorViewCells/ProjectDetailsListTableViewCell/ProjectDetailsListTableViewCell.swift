//
//  ProjectDetailsListTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsListTableViewCell: UITableViewCell {
    @IBOutlet var containerStackView: UIStackView!
    @IBOutlet var headerLabel: InfoTitleSemiboldLabel!

    func setup(_ payload: ListBlockPayload?) {
        guard let payload = payload else { return }
        self.headerLabel.text = "\(payload.header ?? "")\n"
        if containerStackView.arrangedSubviews.count == 0 {
            containerStackView.addArrangedSubview(self.headerLabel)
        }
        payload.items.compactMap({$0}).forEach { (item) in
            let listItemLabel = DescriptionLabel()
            listItemLabel.numberOfLines = 0
            listItemLabel.text = "•  \(item.text ?? "")"
            self.containerStackView.addArrangedSubview(listItemLabel)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.containerStackView.arrangedSubviews.forEach( {
            self.containerStackView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        } )
    }
}
