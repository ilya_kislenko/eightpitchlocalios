//
//  ProjectDetailsDepartmentTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 11/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsDepartmentTableViewCell: UITableViewCell {
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    @IBOutlet var headerLabel: ProjectsTitleLabel!
    @IBOutlet weak var stackView: UIStackView!
    
    
    func setup(_ payload: DepartmentBlockPayload?) {
        guard let items = payload?.items else { return }
        if let headerText = payload?.header {
            self.headerLabel.isHidden = false
            self.headerLabel.text = headerText
        }

        for item in items {
            guard let departmentView = DepartmentView.nib(owner: self) else { return }
            departmentView.delegate = self.delegate
            departmentView.setup(item)
            self.stackView.addArrangedSubview(departmentView)
        }
    }
    
    func setupForPad(_ payload: DepartmentBlockPayload?) {}
    
    func clear() {
        guard let label = self.headerLabel else { return }
        label.isHidden = true
        self.stackView.arrangedSubviews.forEach {
            self.stackView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.clear()
    }
    
}
