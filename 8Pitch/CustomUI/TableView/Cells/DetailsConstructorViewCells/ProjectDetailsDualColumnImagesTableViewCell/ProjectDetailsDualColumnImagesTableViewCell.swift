//
//  ProjectDetailsDualColumnImagesTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsDualColumnImagesTableViewCell: UITableViewCell {
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    @IBOutlet weak var stackView: UIStackView!
    
    func setup(_ payload: DualColumnImagesBlockPayload?) {
        self.stackView.axis =
            UIDevice.current.userInterfaceIdiom == .pad ? .horizontal : .vertical
        guard let leftView = DualColomnImagesView.nib(owner: self) else { return }
        leftView.delegate = self.delegate
        leftView.setup(payload?.leftHeader, payload?.leftText, id: payload?.leftImageId)
        self.stackView.addArrangedSubview(leftView)
        guard let rightView = DualColomnImagesView.nib(owner: self) else { return }
        rightView.delegate = self.delegate
        rightView.setup(payload?.rightHeader, payload?.rightText, id: payload?.rightImageId)
        self.stackView.addArrangedSubview(rightView)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.stackView.arrangedSubviews.forEach {
            self.stackView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
    }
    
}
