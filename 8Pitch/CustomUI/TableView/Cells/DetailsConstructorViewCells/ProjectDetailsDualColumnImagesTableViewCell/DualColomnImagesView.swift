//
//  DualColomnImagesView.swift
//  8Pitch
//
//  Created by 8pitch on 11/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class DualColomnImagesView: UIView {
    
    @IBOutlet weak var picture: ScaledHeightImageView!
    @IBOutlet weak var textLabel: DualTextLabel!
    weak var delegate: LoadProjectDetailsImageDelegate?
    @IBOutlet weak var headerLabel: TotalSumLabel!
    
    private func loadImage(_ ID: String?) {
        guard let imageID = ID else { return }
        self.delegate?.loadProjectDetailsImage(imageID, self.picture)
    }
    
    func setup(_ header: String?, _ text: String?, id: String?) {
        self.textLabel.text = text
        self.headerLabel.text = header
        if let id = id {
            self.loadImage(id)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.picture.clipsToBounds = true
        self.picture.layer.masksToBounds = true
        self.picture.layer.cornerRadius = Constants.radius
        self.picture.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
        
    static func nib(owner: UIView) -> DualColomnImagesView? {
        UINib(nibName: "DualColomnImagesView", bundle: nil).instantiate(withOwner: owner, options: nil).first as? DualColomnImagesView
    }
    
    private enum Constants {
        static let radius: CGFloat = 8
        static let offset: CGFloat = 32
    }
    
}
