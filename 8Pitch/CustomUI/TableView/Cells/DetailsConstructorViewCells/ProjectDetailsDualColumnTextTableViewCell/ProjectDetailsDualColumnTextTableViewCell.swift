//
//  ProjectDetailsDualColumnTextTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 11/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsDualColumnTextTableViewCell: UITableViewCell {

    @IBOutlet weak var topHeaderLabel: ProjectsTitleLabel!
    @IBOutlet weak var topLabel: PositionLabel!
    @IBOutlet weak var bottomLabel: PositionLabel!
    @IBOutlet weak var bottomHeaderLabel: ProjectsTitleLabel!
    
    func setup(_ payload: DualColumnTextBlockPayload?) {
        self.topHeaderLabel.text = payload?.leftHeader
        self.topLabel.text = payload?.leftText
        self.bottomHeaderLabel.text = payload?.rightHeader
        self.bottomLabel.text = payload?.rightText
    }
}
