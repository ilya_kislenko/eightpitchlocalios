//
//  ProjectDetailsFactsTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsFactsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var factsStackView: UIStackView!
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    func setup(_ payload: FactsBlockPayload?) {
        guard let items = payload?.items,
              let perLine = payload?.perLine else { return }
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.createStackOfStacks(items: items, perLine: perLine)
        } else {
            items.forEach { self.addFactViewTo(stack: self.factsStackView, item: $0) }
        }
    }
    
    private func createStack() -> UIStackView {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = Constants.spacingForStack
        return stack
    }
    
    private func createStackOfStacks(items: [FactsBlockPayloadItem?], perLine: Int) {
        let stacksCount = Int((Double(items.count)/Double(perLine)).rounded(.up))
        Array(0..<stacksCount).forEach { stackIndex in
            let stack = createStack()
            let firstIndex = perLine * stackIndex
            let lastIndex = firstIndex + perLine
            Array(firstIndex..<lastIndex).forEach { viewIndex in
                if viewIndex < items.count {
                    self.addFactViewTo(stack: stack, item: items[viewIndex])
                }
            }
            self.factsStackView.addArrangedSubview(stack)
        }
    }
    
    private func addFactViewTo(stack: UIStackView, item: FactsBlockPayloadItem?) {
        guard let view = FactView.nib(owner: self) else { return }
        view.delegate = self.delegate
        view.setup(item)
        stack.addArrangedSubview(view)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.factsStackView.arrangedSubviews.forEach {
            self.factsStackView.removeArrangedSubview($0)
        }
    }
    
    enum Constants {
        static let spacingForStack: CGFloat = 20
    }
}
