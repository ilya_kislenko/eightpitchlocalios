//
//  FactView.swift
//  8Pitch
//
//  Created by 8pitch on 11/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class FactView: UIView {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var textLabel: StatisticsTitleLabel!
    @IBOutlet weak var headerLabel: TotalSumLabel!
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    private func loadImage(_ item: FactsBlockPayloadItem?) {
        guard let imageID = item?.imageId else { return }
        self.delegate?.loadProjectDetailsImage(imageID, self.iconImageView)
    }
    
    func setup(_ item: FactsBlockPayloadItem?) {
        self.headerLabel.text = item?.header
        self.textLabel.text = item?.text
        self.loadImage(item)
    }
    
    static func nib(owner: UIView) -> FactView? {
        UINib(nibName: "FactView", bundle: nil).instantiate(withOwner: owner, options: nil).first as? FactView
    }
    
    
}
