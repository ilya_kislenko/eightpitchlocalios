//
//  ProjectDetailsDualColumnTextPadTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 12/22/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsDualColumnTextPadTableViewCell: UITableViewCell {
    
    @IBOutlet weak var topLabel: PositionLabel!
    @IBOutlet weak var bottomLabel: PositionLabel!
    
    func setup(_ payload: DualColumnTextBlockPayload?) {
        self.topLabel.text = payload?.leftText
        self.bottomLabel.text = payload?.rightText
    }
}
