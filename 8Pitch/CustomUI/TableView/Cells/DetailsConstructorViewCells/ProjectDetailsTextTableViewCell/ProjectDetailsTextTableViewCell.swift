//
//  ProjectDetailsTextTableViewCell.swift.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsTextTableViewCell: UITableViewCell {
    @IBOutlet var textContentLabel: DonutDescriptionLabel!
    @IBOutlet weak var headerLabel: ProjectsTitleLabel!
    @IBOutlet weak var leadingTextInset: NSLayoutConstraint!
    @IBOutlet weak var trailingTextInset: NSLayoutConstraint!
    @IBOutlet weak var topInset: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.current.userInterfaceIdiom == .pad {
            let inset = UIScreen.main.bounds.width * Constants.sideInsetShare
            self.leadingTextInset.constant = inset
            self.trailingTextInset.constant = inset
        }
    }
    
    private enum Constants {
        static let cornerRadius: CGFloat = 1
        static let sideInsetShare: CGFloat = 0.15
    }
    
    func setup(_ payload: TextBlockPayload?) {
        guard let payload = payload else { return }
        self.topInset.constant = (payload.header?.isEmpty ?? true) ? 0 : self.topInset.constant
        self.headerLabel.text = payload.header
        self.headerLabel.font = UIFont.barlow.semibold.withSize(payload.headingSize)
        self.textContentLabel.textAlignment = payload.justify
        self.textContentLabel.attributedText = NSAttributedString.withParameters(text: payload.text,
                                                                                 color: payload.color,
                                                                                 font: payload.fontFamily ?? "Roboto",
                                                                                 fontSize: payload.fontSize,
                                                                                 isBold: payload.bold,
                                                                                 isUnderlined: payload.underline,
                                                                                 isItelic: payload.italic)
    }
}
