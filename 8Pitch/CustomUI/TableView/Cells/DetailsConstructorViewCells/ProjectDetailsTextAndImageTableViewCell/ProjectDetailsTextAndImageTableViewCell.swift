//
//  ProjectDetailsTextAndImageTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let cornerRadius = CGFloat(8)
}

class ProjectDetailsTextAndImageTableViewCell: UITableViewCell {
    @IBOutlet var headerLabel: InfoTitleSemiboldLabel!
    @IBOutlet var picture: ScaledHeightImageView!
    @IBOutlet var descriptionLabel: ProjectDescriptionLabel!
    @IBOutlet var imageTextLabel: FAQLabel!

    @IBOutlet var containerStackView: UIStackView!
    weak var delegate: LoadProjectDetailsImageDelegate?

    func setup(_ payload: TextAndImageBlockPayload?) {
        guard let payload = payload else { return }

        if let headerText = payload.header {
            self.headerLabel.text = headerText
        } else {
            self.headerLabel.isHidden = true
        }
        if let descriptionText = payload.imageDescription {
            self.descriptionLabel.text = descriptionText
        } else {
            self.descriptionLabel.isHidden = true
        }
        if let imageText = payload.text {
            self.imageTextLabel.text = imageText
        } else {
            self.imageTextLabel.isHidden = true
        }
        self.loadImage(payload)
        self.picture.clipsToBounds = true
        self.picture.layer.masksToBounds = true
        self.picture.layer.cornerRadius = Constants.cornerRadius
        self.picture.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }

    private func loadImage(_ payload: TextAndImageBlockPayload) {
        guard let imageID = payload.imageId else { return }
        self.delegate?.loadProjectDetailsImage(imageID, self.picture, completionHandler: {
            self.picture.snp.makeConstraints {
                $0.height.equalTo(self.picture.intrinsicContentSize.height)
                $0.width.equalTo(self.picture.intrinsicContentSize.width)
            }
            self.picture.layoutSubviews()
            self.picture.layoutIfNeeded()
        })
    }
}
