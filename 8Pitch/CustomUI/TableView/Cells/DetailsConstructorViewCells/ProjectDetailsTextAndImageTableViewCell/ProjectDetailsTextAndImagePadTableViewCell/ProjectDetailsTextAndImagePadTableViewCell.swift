//
//  ProjectDetailsTextAndImagePadTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 12/22/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsTextAndImagePadTableViewCell: ProjectDetailsTextAndImageTableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    override func setup(_ payload: TextAndImageBlockPayload?) {
        super.setup(payload)
        switch payload?.textPosition {
        case .left:
            self.containerStackView.removeArrangedSubview(self.picture)
            self.containerStackView.addArrangedSubview(self.picture)
        case .right:
            self.containerStackView.removeArrangedSubview(self.containerView)
            self.containerStackView.addArrangedSubview(self.containerView)
        default:
            return
        }
    }
    
}
