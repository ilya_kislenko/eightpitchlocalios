//
//  ProjectDetailsDisclaimerTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 11/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsDisclaimerTableViewCell: UITableViewCell {
    
    weak var delegate: LoadProjectDetailsImageDelegate?

    private func loadImage(_ payload: DisclaimerBlockPayload?) {
        guard let imageID = payload?.iconUrl else { return }
        self.delegate?.loadProjectDetailsImage(imageID, self.iconImageView)
    }

    @IBOutlet weak var coloredView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var disclaimerTextLabel: DisclaimerLabel!
    
    func setup(_ payload: DisclaimerBlockPayload?) {
        if let color = payload?.background {
        self.coloredView.backgroundColor = UIColor(hex: color)
        }
        self.disclaimerTextLabel.text = payload?.text
        self.loadImage(payload)
    }
    
}
