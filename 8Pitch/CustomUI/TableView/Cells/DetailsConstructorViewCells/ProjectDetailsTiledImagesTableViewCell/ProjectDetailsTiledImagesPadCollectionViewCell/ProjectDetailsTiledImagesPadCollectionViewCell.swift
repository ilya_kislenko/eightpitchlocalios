//
//  ProjectDetailsTiledImagesPadCollectionViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 12/23/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let cornerRadius = CGFloat(8)
    static let insetSum = CGFloat(60)
    static let numberOfPictureOnItem = CGFloat(2)
}

class ProjectDetailsTiledImagesPadCollectionViewCell: UICollectionViewCell {
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ProjectDetailsTiledImagesPadCollectionViewCell"
    }
    
    @IBOutlet weak var leftNumber: InfoTitleSemiboldWhiteLabel!
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var leftLabel: ImageInfoLabel!
    @IBOutlet weak var rightNumber: InfoTitleSemiboldWhiteLabel!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var rightLabel: ImageInfoLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let imageWidth = (UIScreen.main.bounds.width - Constants.insetSum)/Constants.numberOfPictureOnItem
        self.leftImageView.snp.makeConstraints {
            $0.width.equalTo(imageWidth)
        }
        self.rightImageView.snp.makeConstraints {
            $0.width.equalTo(imageWidth)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.leftNumber.text = nil
        self.rightNumber.text = nil
        self.leftLabel.text = nil
        self.rightLabel.text = nil
        self.rightImageView.image = nil
        self.leftImageView.image = nil
    }
    
    func setupWith(_ items: [TilesImagesBlockPayloadItem?], indexes: [Int]) {
        if let leftId = items.first??.imageId, let index = indexes.first {
            self.delegate?.loadProjectDetailsImage(leftId, self.leftImageView)
            self.leftNumber.text = "\(index + 1)."
            self.leftLabel.text = items.first??.text
            self.setupImageView(self.leftImageView)
        }
        if let rightId = items.last??.imageId, let index = indexes.last {
            self.delegate?.loadProjectDetailsImage(rightId, self.rightImageView)
            self.rightNumber.text = "\(index + 1)."
            self.rightLabel.text = items.last??.text
            self.setupImageView(self.rightImageView)
        }
    }
    
    private func setupImageView(_ view: UIImageView) {
        view.layer.masksToBounds = true
        view.layer.cornerRadius = Constants.cornerRadius
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
}
