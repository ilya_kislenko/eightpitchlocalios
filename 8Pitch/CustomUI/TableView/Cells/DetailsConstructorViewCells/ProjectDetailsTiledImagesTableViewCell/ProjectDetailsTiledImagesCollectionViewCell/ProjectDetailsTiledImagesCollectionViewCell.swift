//
//  ProjectDetailsTiledImagesCollectionViewCell.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let cornerRadius = CGFloat(8)
    static let insetSum = CGFloat(40)
}

class ProjectDetailsTiledImagesCollectionViewCell: UICollectionViewCell {
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    var completion: VoidClosure?
    
    @IBOutlet weak var number: InfoTitleSemiboldWhiteLabel!
    @IBOutlet weak var imageView: ScaledHeightImageView!
    @IBOutlet weak var label: ImageInfoLabel!
    
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ProjectDetailsTiledImagesCollectionViewCell"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.completion = nil
    }
    
    func setupWith(_ item: TilesImagesBlockPayloadItem?, index: Int) {
        guard let id = item?.imageId else { return }
        self.imageView.setSize(id: id, offset: Constants.insetSum)
        self.delegate?.loadProjectDetailsImage(id, self.imageView, completionHandler: completion)
        self.number.text = "\(index + 1)."
        self.label.text = item?.text
        self.imageView.clipsToBounds = true
        self.imageView.layer.masksToBounds = true
        self.imageView.layer.cornerRadius = Constants.cornerRadius
        self.imageView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
}
