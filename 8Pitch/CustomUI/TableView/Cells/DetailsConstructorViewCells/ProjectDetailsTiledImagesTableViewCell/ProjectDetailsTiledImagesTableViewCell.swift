//
//  ProjectDetailsTiledImagesTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 05.11.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let numberOfPictureOnItem = 2
    static let separatorWidth: CGFloat = 20
}

class ProjectDetailsTiledImagesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: DynamicHeightCollectionView!
    @IBOutlet weak var swipeView: UIStackView!
    var completion: VoidClosure?
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    private var items: [TilesImagesBlockPayloadItem?] = []
    
    func setup(_ payload: TilesImagesBlockPayload?) {
        self.items = payload?.items ?? []
        self.setupSwipeViewFor(index: 0, count: self.items.count)
        self.collectionView.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.completion = { [weak self] in
            self?.collectionView.reloadData()
        }
        self.collectionView.contentInsetAdjustmentBehavior = .never
        self.collectionView.register(ProjectDetailsTiledImagesCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectDetailsTiledImagesCollectionViewCell.reuseIdentifier)
        self.collectionView.register(ProjectDetailsTiledImagesPadCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectDetailsTiledImagesPadCollectionViewCell.reuseIdentifier)
    }
    
    func setupSwipeViewFor(index: Int, count: Int) {
        guard count > 0 else { return }
        let max = UIDevice.current.userInterfaceIdiom == .pad ? Int((Double(count)/Double(Constants.numberOfPictureOnItem)).rounded(.up)) : count
        for i in 0...max - 1 {
            let view = SeparatorView()
            view.snp.makeConstraints {
                $0.width.equalTo(Constants.separatorWidth)
            }
            view.backgroundColor = index == i ? UIColor(.red) : UIColor(.stateButtonGrayColor)
            self.swipeView.addArrangedSubview(view)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.swipeView.arrangedSubviews.forEach {
            self.swipeView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
    }
}

extension ProjectDetailsTiledImagesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = collectionView.contentOffset
        visibleRect.size   = collectionView.bounds.size
        let visiblePoint   = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let visibleIndexPath: IndexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
        self.swipeView.arrangedSubviews.enumerated().forEach { (index, view ) in
            view.backgroundColor = index == visibleIndexPath.row ?
                UIColor(.red) : UIColor(.stateButtonGrayColor)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        UIDevice.current.userInterfaceIdiom == .pad ?
            Int((Double(self.items.count)/Double(Constants.numberOfPictureOnItem)).rounded(.up)) :
            self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if UIDevice.current.userInterfaceIdiom == .pad {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectDetailsTiledImagesPadCollectionViewCell.reuseIdentifier, for: indexPath) as? ProjectDetailsTiledImagesPadCollectionViewCell else { return UICollectionViewCell() }
            let firstIndex = indexPath.row * Constants.numberOfPictureOnItem
            let lastIndex = firstIndex + 1
            cell.delegate = self.delegate
            cell.setupWith([self.items[firstIndex], lastIndex >= self.items.count ? nil : self.items[lastIndex]], indexes: [firstIndex, lastIndex])
            return cell
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectDetailsTiledImagesCollectionViewCell.reuseIdentifier, for: indexPath) as? ProjectDetailsTiledImagesCollectionViewCell else { return UICollectionViewCell() }
        cell.delegate = self.delegate
        cell.setupWith(self.items[indexPath.row], index: indexPath.row)
        return cell
    }
}
