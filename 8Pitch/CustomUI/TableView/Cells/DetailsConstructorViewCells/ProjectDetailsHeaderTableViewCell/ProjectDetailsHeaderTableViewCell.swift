//
//  ProjectDetailsHeaderTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 11/3/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel: ProjectsTitleLabel!
    @IBOutlet weak var leftInset: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.leftInset.constant = UIDevice.current.userInterfaceIdiom == .pad ?
            UIScreen.main.bounds.width * Constants.sideInsetShare : self.leftInset.constant
    }
    
    func setup(_ payload: HeaderBlockPayload?) {
        self.headerLabel.text = payload?.text
        if let fontSize = payload?.headingSize {
            self.headerLabel.font = UIFont.barlow.semibold.withSize(fontSize)
        }
    }
    
    enum Constants {
        static let sideInsetShare: CGFloat = 0.15
    }
}
