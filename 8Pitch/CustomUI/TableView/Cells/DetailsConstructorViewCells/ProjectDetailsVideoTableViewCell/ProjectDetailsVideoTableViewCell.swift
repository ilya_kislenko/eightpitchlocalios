//
//  ProjectDetailsVideoTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

protocol ProjectDetailsVideoDelegate: class {
    func playVideoWithID(_ videoID: String)
}

class ProjectDetailsVideoTableViewCell: UITableViewCell {

    weak var delegate: ProjectDetailsVideoDelegate?
    private var videoID: String?
    @IBOutlet var headerLabel: InfoTitleSemiboldLabel!
    @IBOutlet var descriptionLabel: DonutDescriptionLabel!

    func setup(_ payload: VideoBlockPayload?) {
        guard let payload = payload else { return }
        if let useText = payload.useText, useText {
            self.headerLabel.isHidden = false
            self.descriptionLabel.isHidden = false
            self.headerLabel.textAlignment = payload.justify
            self.descriptionLabel.textAlignment = payload.justify
            self.headerLabel.text = payload.header
            self.descriptionLabel.text = payload.text
        }
        self.videoID = payload.vimeoId
    }

    @IBAction func playVideoAction(_ sender: Any) {
        guard let identifier = self.videoID else {
            return
        }
        self.delegate?.playVideoWithID(identifier)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.headerLabel.isHidden = true
        self.descriptionLabel.isHidden = true
    }
}
