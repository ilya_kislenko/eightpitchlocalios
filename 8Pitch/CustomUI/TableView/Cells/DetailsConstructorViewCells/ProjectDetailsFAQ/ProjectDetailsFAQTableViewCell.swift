//
//  ProjectDetailsFAQTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsFAQTableViewCell: UITableViewCell {

    @IBOutlet var FAQTableView: DynamicHeightTableView!
    private var payload: FAQBlockPayload?

    weak var delegate: ProjectDetailsExpandableCellDelegate?

    func setup(_ payload: FAQBlockPayload?) {
        self.payload = payload
        self.FAQTableView.delegate = self
        self.FAQTableView.dataSource = self
        self.FAQTableView.register(ProjectDetailsFAQItemTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsFAQItemTableViewCell.reuseIdentifier)
        self.FAQTableView.reloadData()
    }
}

extension ProjectDetailsFAQTableViewCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: ProjectDetailsFAQHeaderTableView = .fromNib()
        headerView.titleLabel.text = self.payload?.header
        return headerView
    }
}

extension ProjectDetailsFAQTableViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.payload?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = self.payload?.items[indexPath.row] else {
            return UITableViewCell() }
        let cell = self.FAQTableView.dequeueReusableCell(withIdentifier: ProjectDetailsFAQItemTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsFAQItemTableViewCell
        cell.delegate = self
        cell.setup(item)
        return cell
    }
}

extension ProjectDetailsFAQTableViewCell: ProjectDetailsExpandableCellDelegate {
    func scrollToTop() {}
    
    func updateExpandableCell() {
        self.FAQTableView.beginUpdates()
        self.FAQTableView.endUpdates()
        self.delegate?.updateExpandableCell()
    }
}
