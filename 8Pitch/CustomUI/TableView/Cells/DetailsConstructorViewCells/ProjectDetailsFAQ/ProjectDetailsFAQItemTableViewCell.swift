//
//  ProjectDetailsFAQItemTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/27/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static var bottomCornerRadius = CGFloat(8)
}

protocol ProjectDetailsExpandableCellDelegate: class {
    func updateExpandableCell()
    func scrollToTop()
}

class ProjectDetailsFAQItemTableViewCell: UITableViewCell {
    @IBOutlet var containerView: UIView!
    @IBOutlet var expandButton: UIButton!

    @IBOutlet var questionTextLabel: TotalSumLabel!

    @IBOutlet var answerContainerView: UIView!
    @IBOutlet var answerTextLabel: FAQLabel!
    @IBOutlet var separatorView: UIView!

    weak var delegate: ProjectDetailsExpandableCellDelegate?

    func setup(_ item: FAQItem) {
        self.questionTextLabel.text = item.question
        self.questionTextLabel.textAlignment = .left
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = answerTextLabel.textAlignment
        paragraphStyle.lineBreakMode = .byClipping
        paragraphStyle.lineHeightMultiple = 1.47
        paragraphStyle.lineBreakMode = .byClipping
        let attributes: [NSAttributedString.Key : Any] = [.kern: answerTextLabel.kern, .paragraphStyle: paragraphStyle, .font: answerTextLabel.font!]
        let attributedText = NSMutableAttributedString(string: item.answer ?? "", attributes: attributes)
        answerTextLabel.attributedText = attributedText
        self.containerView.layer.cornerRadius = Constants.bottomCornerRadius
        self.containerView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }

    @IBAction func expandAction(_ sender: Any) {
        if  self.answerContainerView.isHidden {
            self.answerContainerView.isHidden = false
            self.containerView.backgroundColor = UIColor(.answerBackground)
            self.expandButton.setImage(UIImage(.arrowUp), for: .normal)
            self.separatorView.isHidden = true
        } else {
            self.answerContainerView.isHidden = true
            self.containerView.backgroundColor = UIColor.clear
            self.expandButton.setImage(UIImage(.arrowDown), for: .normal)
            self.separatorView.isHidden = false
        }
        self.delegate?.updateExpandableCell()
    }

    override func prepareForReuse() {
        self.expandButton.setImage(UIImage(.arrowDown), for: .normal)
        self.answerContainerView.isHidden = true
        self.containerView.backgroundColor = UIColor.clear
        self.separatorView.isHidden = false
        super.prepareForReuse()
    }
}
