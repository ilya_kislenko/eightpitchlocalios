//
//  c.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsIconsTableViewCell: UITableViewCell {

    weak var delegate: LoadProjectDetailsImageDelegate?
    
    @IBOutlet weak var collectionView: UICollectionView!
    var items: [IconsBlockPayloadIconItem?] = []

    func setup(_ payload: IconsBlockPayload?) {
        self.items = payload?.items ?? []
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(ProjectDetailsIconCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectDetailsIconCollectionViewCell.reuseIdentifier)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.items = []
        self.collectionView.reloadData()
    }

}

extension ProjectDetailsIconsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectDetailsIconCollectionViewCell.reuseIdentifier, for: indexPath) as? ProjectDetailsIconCollectionViewCell else { return UICollectionViewCell() }
        cell.delegate = self.delegate
        cell.setupWith(self.items[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let urlString = self.items[indexPath.row]?.link, let url = URL(string: urlString) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
