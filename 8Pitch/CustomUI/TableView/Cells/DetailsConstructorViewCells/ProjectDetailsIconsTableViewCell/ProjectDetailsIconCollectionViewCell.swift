//
//  ProjectDetailsIconCollectionViewCell.swift
//  8Pitch
//
//  Created by 8Pitch on 11/10/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation
import UIKit
import Input

class ProjectDetailsIconCollectionViewCell: UICollectionViewCell {

    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }

    class var reuseIdentifier : String {
        return "ProjectDetailsIconCollectionViewCell"
    }

    @IBOutlet var iconImageView: UIImageView!
    weak var delegate: LoadProjectDetailsImageDelegate?

    func setupWith(_ item: IconsBlockPayloadIconItem?) {
        guard let id = item?.imageId else { return }
        self.delegate?.loadProjectDetailsImage(id, self.iconImageView)
    }
}
