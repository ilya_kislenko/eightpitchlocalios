//
//  ProjectDetailsIconsPadTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 12/22/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsIconsPadTableViewCell: ProjectDetailsIconsTableViewCell {
    
    @IBOutlet weak var stack: UIStackView!
    private var perLine = 0
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.stack.arrangedSubviews.forEach {
            if $0 != self.collectionView {
                $0.removeFromSuperview()
            }
        }
    }
    
    override func setup(_ payload: IconsBlockPayload?) {
        self.perLine = payload?.perLine ?? 0
        super.setup(payload)
        let numberOfCollections = Int((Double(self.items.count)/Double(self.perLine)).rounded(.up))
        if numberOfCollections > 1 {
            for i in 1...numberOfCollections - 1 {
                self.addCollectionViewWithTag(i)
            }
        }
    }
    
    private func addCollectionViewWithTag(_ tag: Int) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(ProjectDetailsIconCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectDetailsIconCollectionViewCell.reuseIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.tag = tag
        self.stack.addArrangedSubview(collectionView)
        self.collectionView.reloadData()
        collectionView.reloadData()
    }
        
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let allItemsCount = super.collectionView(collectionView, numberOfItemsInSection: section)
        let lastItemsCount = allItemsCount - (collectionView.tag * self.perLine)
        return min(lastItemsCount, self.perLine)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as? ProjectDetailsIconCollectionViewCell else { return UICollectionViewCell() }
        let index = collectionView.tag * self.perLine + indexPath.row
        cell.setupWith(self.items[index])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = collectionView.tag * self.perLine + indexPath.row
        super.collectionView(collectionView, didSelectItemAt: IndexPath(item: index, section: indexPath.section))
    }
    
}

