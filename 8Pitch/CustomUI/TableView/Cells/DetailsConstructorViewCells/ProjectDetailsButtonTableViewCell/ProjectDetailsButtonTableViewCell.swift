//
//  ProjectDetailsButtonTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 11/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsButtonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var button: RoundedButton!
    @IBOutlet weak var leftInset: NSLayoutConstraint!
    @IBOutlet weak var rightInset: NSLayoutConstraint!
    
    private var link: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let inset = UIScreen.main.bounds.width * Constants.insetShare
        self.leftInset.constant = inset
        self.rightInset.constant = inset
    }
    
    func setup(_ payload: ButtonBlockPayload?) {
        if let color = payload?.btnColor, let textColor = payload?.textColor {
            self.button.backgroundColor = UIColor(hex: color)
            self.button.setTitleColor(UIColor(hex: textColor), for: .normal)
        }
        self.button.setTitle(payload?.text, for: .normal)
        self.link = payload?.link
        self.button.addTarget(self, action: #selector(self.openLink), for: .touchUpInside)
    }
    
    @objc private func openLink() {
        guard let urlString = self.link, let url = URL(string: urlString) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    enum Constants {
        static let insetShare: CGFloat = 0.15
    }
    
}
