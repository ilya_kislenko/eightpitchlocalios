//
//  ProjectDetailsTableTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let leftRightContentInset = CGFloat(16)
    static let tableItemHeight = CGFloat(48)
    static let topBottomContentInset = CGFloat(15)
}

class ProjectDetailsTableTableViewCell: UITableViewCell {
    static func cellHeight(numberOfRows: Int) -> CGFloat {
        return CGFloat(numberOfRows) * Constants.tableItemHeight + Constants.topBottomContentInset * 2
    }

    @IBOutlet var tableCollectionView: UICollectionView!

    fileprivate var payload: TableBlockPayload?

    func setup(_ payload: TableBlockPayload?) {
        self.tableCollectionView.register(ProjectDetailsTableCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectDetailsTableCollectionViewCell.reuseIdentifier)
        self.payload = payload
        self.configureCollectionView()
    }

    private func configureCollectionView() {
        self.tableCollectionView.delegate = self
        self.tableCollectionView.dataSource = self
        self.tableCollectionView.contentInset = UIEdgeInsets(top: .zero, left: Constants.leftRightContentInset, bottom: .zero, right: Constants.leftRightContentInset)
        self.tableCollectionView.reloadData()
    }
}

extension ProjectDetailsTableTableViewCell: UICollectionViewDelegate {}

extension ProjectDetailsTableTableViewCell: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return payload?.colsNumber ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return payload?.rowsNumber ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let payload = self.payload else { return UICollectionViewCell() }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectDetailsTableCollectionViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsTableCollectionViewCell
        let backgroundSyle = self.itemBackgroundStyle(indexPath)
        let borderStyle = self.itemBorderStyle(indexPath, payload.rowsNumber ?? 1)

        cell.setup(payload.rows[indexPath.row][indexPath.section], payload.headerColor, borderStyle, backgroundSyle)
        return cell
    }

    private func itemBorderStyle(_ indexPath: IndexPath, _ numberOfColumns: Int) -> ProjectDetailsTableItemBorderStyle {
        if indexPath.row == 0,
           indexPath.section == 0 {
            return .leading
        } else if indexPath.row == 0,
                  indexPath.section == (numberOfColumns - 1) {
            return .trailing
        } else {
            return .bordered
        }
    }

    private func itemBackgroundStyle(_ indexPath: IndexPath) -> ProjectDetailsTableItemBackgroundStyle {
        if indexPath.row == 0 {
            return .title
        } else if (indexPath.row % 2 == 0) {
            return .even
        } else {
            return .odd
        }
    }
}
