//
//  ProjectDetailsTableCollectionViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/25/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Foundation
import Input

enum ProjectDetailsTableItemBorderStyle {
    case leading
    case trailing
    case bordered
}

enum ProjectDetailsTableItemBackgroundStyle {
    case title
    case even
    case odd
}

fileprivate struct Constants {
    static let titleFontSize = CGFloat(14)
    static let regularFontSize = CGFloat(12)
}

class ProjectDetailsTableCollectionViewCell: UICollectionViewCell {
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }

    class var reuseIdentifier : String {
        return "ProjectDetailsTableCollectionViewCell"
    }

    @IBOutlet var topBorder: UIView!
    @IBOutlet var leadingBorder: UIView!
    @IBOutlet var trailingBorder: UIView!
    @IBOutlet var bottomBorder: UIView!
    @IBOutlet var contentInfoLabel: TableLabel!

    func setup(_ text: String?,
               _ headerColor: TableHeaderColor?,
               _ borderStyle: ProjectDetailsTableItemBorderStyle,
               _ backgroundStyle: ProjectDetailsTableItemBackgroundStyle) {
        self.contentInfoLabel.text = text
        switch backgroundStyle {
        case .title:
            switch headerColor {
            case .black:
                self.contentView.backgroundColor = UIColor(.backGroundCurrencyDark)
            case .red, .none:
                self.contentView.backgroundColor = UIColor(.red)
            }
            self.contentInfoLabel.textColor =  UIColor(.white)
            self.contentInfoLabel.font = self.contentInfoLabel.font.withSize(Constants.titleFontSize)
            switch borderStyle {
            case .leading:
                self.trailingBorder.backgroundColor = UIColor(.white)
            case .trailing:
                self.leadingBorder.backgroundColor = UIColor(.white)
            default:
                self.leadingBorder.backgroundColor = UIColor(.white)
                self.trailingBorder.backgroundColor = UIColor(.white)
            }
        case .even:
            self.contentView.backgroundColor = UIColor(.grayShadow)
            self.contentInfoLabel.textColor =  UIColor(.textGray)
            self.contentInfoLabel.font = self.contentInfoLabel.font.withSize(Constants.regularFontSize)
            self.setupRegularCellBorder()
        case .odd:
            self.contentView.backgroundColor = UIColor(.white)
            self.contentInfoLabel.textColor =  UIColor(.textGray)
            self.contentInfoLabel.font = self.contentInfoLabel.font.withSize(Constants.regularFontSize)
            self.setupRegularCellBorder()
        }
    }

    private func setupRegularCellBorder() {
        self.leadingBorder.backgroundColor = UIColor(.textGray)
        self.trailingBorder.backgroundColor = UIColor(.textGray)
        self.topBorder.backgroundColor = UIColor(.textGray)
        self.bottomBorder.backgroundColor = UIColor(.textGray)
    }

    override func prepareForReuse() {
        self.topBorder.backgroundColor = .clear
        self.leadingBorder.backgroundColor = .clear
        self.trailingBorder.backgroundColor = .clear
        self.bottomBorder.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        super.prepareForReuse()
    }

}
