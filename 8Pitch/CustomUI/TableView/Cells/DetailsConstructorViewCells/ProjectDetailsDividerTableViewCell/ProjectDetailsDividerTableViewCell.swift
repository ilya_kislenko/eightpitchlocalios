//
//  ProjectDetailsDividerTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsDividerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var aboveInset: NSLayoutConstraint!
    @IBOutlet weak var belowInset: NSLayoutConstraint!
    
    public func setup(_ above: Int? = nil, _ below: Int? = nil) {
        guard let above = above, let below = below else {
            self.separator.isHidden = true
            return
        }
        self.aboveInset.constant = CGFloat(above)
        self.belowInset.constant = CGFloat(below)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.separator.isHidden = false
    }
}
