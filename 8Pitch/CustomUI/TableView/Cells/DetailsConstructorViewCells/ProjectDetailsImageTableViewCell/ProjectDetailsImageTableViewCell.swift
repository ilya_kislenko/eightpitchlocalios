//
//  ProjectDetailsImageTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let cornerRadius = CGFloat(8)
    static let imageHorizontalOffset = CGFloat(16)
}

class ProjectDetailsImageTableViewCell: UITableViewCell {
    @IBOutlet var textOverlapLabel: DonutDescriptionLabel!
    @IBOutlet var imageConstraint: NSLayoutConstraint!
    @IBOutlet var picture: ScaledHeightImageView!
    @IBOutlet weak var headerLabel: ProjectsTitleLabel!
    
    weak var delegate: LoadProjectDetailsImageDelegate?

    func setup(_ payload: ImageBlockPayload?) {
        guard let payload = payload else { return }
        self.headerLabel.font = UIFont.barlow.semibold.withSize(payload.headingSize)
        self.headerLabel.text = payload.header
        self.textOverlapLabel.isHidden = payload.useText ?? false ? false : true
        self.imageConstraint.constant = payload.fullWidth ? 0 : Constants.imageHorizontalOffset
        self.textOverlapLabel.textAlignment = payload.justify
        self.textOverlapLabel.attributedText = NSAttributedString.withParameters(text: payload.text,
                                                                                 color: payload.color,
                                                                                 font: payload.fontFamily,
                                                                                 fontSize: payload.fontSize,
                                                                                 isBold: payload.bold,
                                                                                 isUnderlined: payload.underline,
                                                                                 isItelic: payload.italic)
        self.loadImage(payload)
        self.picture.clipsToBounds = true
        self.picture.layer.masksToBounds = true
        self.picture.layer.cornerRadius = Constants.cornerRadius
        self.picture.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }

    private func loadImage(_ payload: ImageBlockPayload) {
        guard let imageID = payload.imageId else { return }
        self.delegate?.loadProjectDetailsImage(imageID, self.picture)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.textOverlapLabel.isHidden = true
    }
}
