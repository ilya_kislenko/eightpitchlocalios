//
//  ProjectDetailsImageLinkTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 10/30/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsImageLinkTableViewCell: UITableViewCell {
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    @IBOutlet weak var collectionView: DynamicHeightCollectionView!
    private var items: [ImageLinkBlockPayloadItem?] = []
    @IBOutlet weak var swipeView: UIStackView!
    
    func setup(_ payload: ImageLinkBlockPayload?) {
        self.items = payload?.items ?? []
        self.setupSwipeViewFor(index: 0, count: items.count)
        self.collectionView.reloadData()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.swipeView.arrangedSubviews.forEach {
            self.swipeView.removeArrangedSubview($0)
            $0.removeFromSuperview()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(ProjectDetailsImageLinkCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectDetailsImageLinkCollectionViewCell.reuseIdentifier)
        self.collectionView.register(ProjectDetailsImageLinkPadCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectDetailsImageLinkPadCollectionViewCell.reuseIdentifier)
    }
    
    private func setupSwipeViewFor(index: Int, count: Int) {
        guard count > 0 else { return }
        let max = UIDevice.current.userInterfaceIdiom == .pad ? Int((Double(count)/Double(Constants.numberOfImageOnItem)).rounded(.up)) : count
        for i in 0...max - 1 {
            let view = SeparatorView()
            view.snp.makeConstraints {
                $0.width.equalTo(Constants.swipiIndicatorWidth)
            }
            view.backgroundColor = index == i ? UIColor(.red) : UIColor(.stateButtonGrayColor)
            self.swipeView.addArrangedSubview(view)
        }
    }
    
    enum Constants {
        static let swipiIndicatorWidth = CGFloat(20)
        static let numberOfImageOnItem: Int = 2
        static let swipeViewHeight = CGFloat(55)
    }
    
}

extension ProjectDetailsImageLinkTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = collectionView.contentOffset
        visibleRect.size   = collectionView.bounds.size
        let visiblePoint   = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let visibleIndexPath: IndexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
        self.swipeView.arrangedSubviews.enumerated().forEach { (index, view ) in
            view.backgroundColor = index == visibleIndexPath.row ?
                UIColor(.red) : UIColor(.stateButtonGrayColor)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        UIDevice.current.userInterfaceIdiom == .pad ?
            Int((Double(self.items.count)/Double(Constants.numberOfImageOnItem)).rounded(.up)) :
            self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if UIDevice.current.userInterfaceIdiom == .pad {
            let firstIndex = indexPath.row * Constants.numberOfImageOnItem
            let lastIndex = firstIndex + 1
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectDetailsImageLinkPadCollectionViewCell.reuseIdentifier, for: indexPath) as? ProjectDetailsImageLinkPadCollectionViewCell else { return UICollectionViewCell() }
            cell.delegate = self.delegate
            cell.setupWith([self.items[firstIndex], lastIndex >= self.items.count ? nil : self.items[lastIndex]])
            return cell
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectDetailsImageLinkCollectionViewCell.reuseIdentifier, for: indexPath) as? ProjectDetailsImageLinkCollectionViewCell else { return UICollectionViewCell() }
        cell.delegate = self.delegate
        cell.setupWith(self.items[indexPath.row], index: indexPath.row, allItemsCount: self.items.count)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let urlString = self.items[indexPath.row]?.link, let url = URL(string: urlString) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

}
