//
//  ProjectDetailsLineChartTableViewCell.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Charts
import Input

class ProjectDetailsLineChartTableViewCell: UITableViewCell {

    @IBOutlet var titleContainerView: UIView!
    @IBOutlet var titleLabel: ProjectsTitleLabel!
    @IBOutlet var lineChartView: LineChartView!
    @IBOutlet var chartTextContainer: UIView!
    @IBOutlet var charTextLabel: FAQLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(_ payload: ChartBlockPayloadItem?) {
        self.titleContainerView.isHidden = payload?.title == nil ? true : false
        self.chartTextContainer.isHidden = payload?.text == nil ? true : false
        self.titleLabel.text = payload?.title
        self.charTextLabel.text = payload?.text
        guard let items = payload?.data else { return }

        let xAxis: [Double] = items.compactMap {
            guard let value = $0[0] else { return nil }
            return Double(value)
        }

        let dataSets = items.compactMap({ (item) -> LineChartDataSet? in
            guard let name = item[1], let color = item[3] else { return nil }
            let yAxis: [Double] = item.dropFirst(4).compactMap {
                guard let value = $0 else { return nil }
                return Double(value)
            }
            let entries = zip(xAxis, yAxis).map { ChartDataEntry(x: $0, y: $1) }
            let dataSet = LineChartDataSet(entries: entries, label: name)
            dataSet.setColor(NSUIColor(hex: color ))
            dataSet.circleRadius = 0
            dataSet.lineWidth = 2
            return dataSet
        })
        let chartData = LineChartData(dataSets: dataSets)
        chartData.setDrawValues(false)
        self.lineChartView.rightAxis.enabled = false
        self.lineChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.lineChartView.legend.form = .circle
        self.lineChartView.legend.font = UIFont.roboto.light.textSize
        self.lineChartView.data = chartData
    }
}

