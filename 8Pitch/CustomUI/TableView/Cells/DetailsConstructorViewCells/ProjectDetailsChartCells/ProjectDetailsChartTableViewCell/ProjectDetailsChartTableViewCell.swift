//
//  ProjectDetailsChartTableViewCell.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Charts
import Input

class ProjectDetailsChartTableViewCell: UITableViewCell {
    @IBOutlet var chartsTableView: DynamicHeightTableView!
    
    private var payload: ChartBlockPayload?
    
    func setup(_ payload: ChartBlockPayload?) {
        self.payload = payload
        self.chartsTableView.delegate = self
        self.chartsTableView.dataSource = self
        self.chartsTableView.register(ProjectDetailsDonutChartTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsDonutChartTableViewCell.reuseIdentifier)
        self.chartsTableView.register(ProjectDetailsLineChartTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsLineChartTableViewCell.reuseIdentifier)
        self.chartsTableView.register(ProjectDetailsBarChartTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsBarChartTableViewCell.reuseIdentifier)
        self.chartsTableView.reloadData()
    }
}

extension ProjectDetailsChartTableViewCell: UITableViewDelegate {}

extension ProjectDetailsChartTableViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.payload?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = self.payload?.items[indexPath.row] else {
            return UITableViewCell() }
        
        switch item.chartType {
        case .doughnut, .halfDoughnut:
            let cell = self.chartsTableView.dequeueReusableCell(withIdentifier: ProjectDetailsDonutChartTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsDonutChartTableViewCell
            cell.setup(item)
            return cell
        case .line:
            let cell = self.chartsTableView.dequeueReusableCell(withIdentifier: ProjectDetailsLineChartTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsLineChartTableViewCell
            cell.setup(item)
            return cell
        case .bar:
            let cell = self.chartsTableView.dequeueReusableCell(withIdentifier: ProjectDetailsBarChartTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsBarChartTableViewCell
            cell.setup(item)
            return cell
        case .none:
            break
        }
        return UITableViewCell()
    }
}
