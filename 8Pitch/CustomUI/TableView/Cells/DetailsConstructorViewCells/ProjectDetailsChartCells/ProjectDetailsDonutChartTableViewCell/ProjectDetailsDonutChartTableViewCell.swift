//
//  ProjectDetailsDonutChartTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 05.11.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Charts
import Input

class ProjectDetailsDonutChartTableViewCell: UITableViewCell {

    @IBOutlet var titleContainerView: UIView!
    @IBOutlet var titleLabel: ProjectsTitleLabel!
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet var chartTextContainer: UIView!
    @IBOutlet var charTextLabel: FAQLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(_ payload: ChartBlockPayloadItem?) {
        self.titleContainerView.isHidden = payload?.title == nil ? true : false
        self.chartTextContainer.isHidden = payload?.text == nil ? true : false
        guard let items = payload?.data else { return }
        self.titleLabel.text = payload?.title
        self.charTextLabel.text = payload?.text
        let dataEntries: [ChartDataEntry] = items.compactMap {
            guard let name = $0[0], let value = $0[1] else { return nil }
            return PieChartDataEntry(value: Double(value) ?? 0, label: name, data: name )
        }
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
        pieChartDataSet.colors = items.map {
            NSUIColor(hex: $0[2] ?? "#FFFFF")
        }
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartData.setValueFormatter(formatter)
        pieChartView.legend.form = .circle
        self.pieChartView.data = pieChartData
    }
    
}
