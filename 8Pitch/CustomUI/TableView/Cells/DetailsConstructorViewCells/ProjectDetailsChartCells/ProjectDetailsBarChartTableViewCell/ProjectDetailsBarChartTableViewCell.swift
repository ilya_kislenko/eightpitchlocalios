//
//  ProjectDetailsBarChartTableViewCell.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Charts
import Input

fileprivate struct Constants {
    static let barWidth: Double = 7
    static let groupSpace: Double = 20
}

class ProjectDetailsBarChartTableViewCell: UITableViewCell {

    @IBOutlet var titleContainerView: UIView!
    @IBOutlet var titleLabel: ProjectsTitleLabel!
    @IBOutlet var barChartView: BarChartView!
    @IBOutlet var chartTextContainer: UIView!
    @IBOutlet var charTextLabel: FAQLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(_ payload: ChartBlockPayloadItem?) {
        self.titleContainerView.isHidden = payload?.title == nil ? true : false
        self.chartTextContainer.isHidden = payload?.text == nil ? true : false
        self.titleLabel.text = payload?.title
        self.charTextLabel.text = payload?.text
        guard let items = payload?.data else { return }

        let xAxis: [Double] = items.compactMap {
            guard let value = $0[0] else { return nil }
            return Double(value)
        }

        let dataSets = items.compactMap({ (item) -> BarChartDataSet? in
            guard let name = item[1], let color = item[2] else { return nil }
            let yAxis: [Double] = item.dropFirst(3).compactMap {
                guard let value = $0 else { return nil }
                return Double(value)
            }
            let entries = zip(xAxis, yAxis).map { BarChartDataEntry(x: $0, y: $1) }
            let dataSet = BarChartDataSet(entries: entries, label: name)
            dataSet.setColor(NSUIColor(hex: color ))
            return dataSet
        })
        let chartData = BarChartData(dataSets: dataSets)
        chartData.barWidth = Constants.barWidth
        self.barChartView.data = chartData
        self.barChartView.notifyDataSetChanged()
        guard let firstX = xAxis.first else { return }
        self.barChartView.fitBars = true
        self.barChartView.xAxis.drawGridLinesEnabled = false
        self.barChartView.leftAxis.drawGridLinesEnabled = false
        self.barChartView.rightAxis.drawGridLinesEnabled = false
        self.barChartView.rightAxis.drawAxisLineEnabled = false
        self.barChartView.rightAxis.enabled = false
        self.barChartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        self.barChartView.legend.form = .circle
        self.barChartView.legend.font = UIFont.roboto.light.textSize
        self.barChartView.groupBars(fromX: firstX, groupSpace: Constants.groupSpace, barSpace: 0)
    }
}
