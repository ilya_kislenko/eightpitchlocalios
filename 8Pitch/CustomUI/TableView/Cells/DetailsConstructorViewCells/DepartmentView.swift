//
//  DepartmentView.swift
//  8Pitch
//
//  Created by 8pitch on 11/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class DepartmentView: UIView {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: DepartmentLabel!
    @IBOutlet weak var positionLabel: PositionLabel!
    @IBOutlet weak var button: UIButton!
    
    private var link: String?
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    private func loadImage(_ item: DepartmentBlockPayloadItem?) {
        guard let imageID = item?.imageUrl else { return }
        self.delegate?.loadProjectDetailsImage(imageID, self.photoImageView)
    }
    
    func setup(_ item: DepartmentBlockPayloadItem?) {
        self.photoImageView.layer.masksToBounds = true
        self.photoImageView.layer.cornerRadius = self.photoImageView.frame.height/2
        self.nameLabel.text = item?.fullName
        self.positionLabel.text = item?.position
        self.loadImage(item)
        self.link = item?.linkedinUrl
        self.button.addTarget(self, action: #selector(self.openLinkedinPage), for: .touchUpInside)
    }
    
    @objc private func openLinkedinPage() {
        guard let urlString = self.link, let url = URL(string: urlString) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    static func nib(owner: UIView) -> DepartmentView? {
        UINib(nibName: "DepartmentView", bundle: nil).instantiate(withOwner: owner, options: nil).first as? DepartmentView
    }
    
}
