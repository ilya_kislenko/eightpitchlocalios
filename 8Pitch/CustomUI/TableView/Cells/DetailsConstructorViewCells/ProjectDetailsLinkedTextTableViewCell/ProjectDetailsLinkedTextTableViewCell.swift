//
//  ProjectDetailsLinkedTextTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 11/5/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsLinkedTextTableViewCell: UITableViewCell {
    
    @IBOutlet weak var linkedTextView: UITextView!
    @IBOutlet weak var leftInset: NSLayoutConstraint!
    @IBOutlet weak var rightInset: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.current.userInterfaceIdiom == .pad {
        let inset = UIScreen.main.bounds.width * Constants.insetShare
        self.leftInset.constant = inset
        self.rightInset.constant = inset
        }
    }
    
    func setup(_ payload: LinkedTextBlockPayload?) {
        guard let payload = payload else { return }
        let links = payload.wordsLink.filter { !($0?.linkUrl?.isEmpty ?? true) }
        var text = payload.wordsLink.reduce(into: "") { $0 += " \($1?.text ?? "")" }
        text.removeFirst()
        guard let attributedText = NSAttributedString.withParameters(text: text,
                                                                     color: payload.color,
                                                                     font: payload.fontFamily,
                                                                     fontSize: payload.fontSize,
                                                                     isBold: payload.bold,
                                                                     isUnderlined: payload.underline,
                                                                     isItelic: payload.italic) else { return }
        let resultText = NSMutableAttributedString(attributedString: attributedText)
        links.forEach {
            guard let text = $0?.text, let link = $0?.linkUrl else { return }
            let linkRange = resultText.mutableString.range(of: text)
            resultText.addAttributes([.link: link, .foregroundColor: UIColor(.link)], range: linkRange)
        }
        self.linkedTextView.attributedText = resultText
        self.linkedTextView.snp.makeConstraints {
            $0.height.equalTo(self.linkedTextView.contentSize.height)
        }
    }
    
    enum Constants {
        static let insetShare: CGFloat = 0.15
    }
}

