//
//  VotingRateCell.swift
//  8Pitch
//
//  Created by 8pitch on 3/17/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class VotingRateCell: UITableViewCell {
    
    @IBOutlet weak var investorTitleLabel: SumLabel!
    @IBOutlet weak var tokenTitleLabel: SumLabel!
    @IBOutlet weak var investorValueLabel: PercentLabel!
    @IBOutlet weak var tokenValueLabel: PercentLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.investorTitleLabel.text = "voting.result.rate.investor".localized
        self.tokenTitleLabel.text = "voting.result.rate.token".localized
    }
    
    func setValues(for investor: Double, token: Double) {
        self.investorValueLabel.text = "\(Int(investor)) %"
        self.tokenValueLabel.text = "\(Int(token)) %"
    }
}
