//
//  ApproveVotingAgendaCell.swift
//  8Pitch
//
//  Created by 8pitch on 3/9/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class ApproveVotingAgendaCell: UITableViewCell {

    @IBOutlet weak var agendaLabel: PositionLabel!
    
    func setText(_ text: String?) {
        self.agendaLabel.text = text
        self.agendaLabel.setNeedsLayout()
        self.agendaLabel.layoutIfNeeded()
        self.agendaLabel.sizeThatFits(self.agendaLabel.intrinsicContentSize)
    }
    
}
