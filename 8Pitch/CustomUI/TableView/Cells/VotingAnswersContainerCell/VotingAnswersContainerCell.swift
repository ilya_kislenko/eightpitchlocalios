//
//  VotingAnswersContainerCell.swift
//  8Pitch
//
//  Created by 8pitch on 3/17/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class VotingAnswersContainerCell: UITableViewCell {
    
    @IBOutlet weak var tableView: SelfSizingTableView!
    private var questionsResults: [QuestionResult] = []
    private var questions: [Question] = []
    
    func setup(with questionsResults: [QuestionResult], questions: [Question]) {
        self.questionsResults = questionsResults
        self.questions = questions
        self.setupLayout(with: questions)
        self.tableView.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.register(VotingAnswersTypeCell.nibForRegistration, forCellReuseIdentifier: VotingAnswersTypeCell.reuseIdentifier)
        tableView.register(VotingAnswerStatisticCell.nibForRegistration, forCellReuseIdentifier: VotingAnswerStatisticCell.reuseIdentifier)
        tableView.register(VotingQuestionHeader.nibForRegistration, forHeaderFooterViewReuseIdentifier: VotingQuestionHeader.reuseIdentifier)
        tableView.isScrollEnabled = false
    }
    
    private func setupLayout(with questions: [Question]) {
        var headerSizes: CGFloat = 0
        var cellSizes: CGFloat = 0
        var cellCount: Int = 0
        questions.forEach {
            headerSizes += VotingLabel().height(for: $0.title, with: Constants.headerLabelWidth)
            headerSizes += Constants.headerHeight
            cellCount += $0.options.count * 2 + 2
            cellSizes += Constants.typeRowHeight * 2
            $0.options.forEach { option in
                cellSizes += (SumLabel().height(for: option.answer, with: Constants.rowLabelWidth) + Constants.rowInset) * 2
            }
        }
        self.tableView.estimatedRowHeight = cellSizes/CGFloat(cellCount)
        self.tableView.estimatedSectionHeaderHeight = headerSizes/CGFloat(questions.count)
    }
    
    private enum Constants {
        static let typeRowHeight: CGFloat = 58
        static let rowInset: CGFloat = 20
        static let rowLabelWidth = UIScreen.main.bounds.width - 106
        static let headerLabelWidth = UIScreen.main.bounds.width - 48
        static let headerHeight: CGFloat = 31
    }
}

extension VotingAnswersContainerCell: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        questions.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let questionID = self.questionsResults[section].question
        guard let question = self.questions.first(where: { $0.id == questionID }) else { return 0 }
        return questionsResults.isEmpty || questions.isEmpty ? 0 : question.options.count * 2 + 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: VotingQuestionHeader.reuseIdentifier) as? VotingQuestionHeader
        view?.contentView.backgroundColor = .white
        let index = self.questionsResults[section].question
        if let question = self.questions.first(where: { $0.id == index }) {
            view?.setup(for: question, filled: false, result: true)
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let questionID = self.questionsResults[indexPath.section].question
        guard let question = self.questions.first(where: { $0.id == questionID }) else { return UITableViewCell() }
        switch indexPath.row {
        case 0, 1 + question.options.count:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingAnswersTypeCell.reuseIdentifier, for: indexPath) as! VotingAnswersTypeCell
            let type: VotingAnswersTypeCell.CellType = indexPath.row == 0 ? .perParticipant : .effectiveVotes
            cell.setup(for: type, questionType: question.type)
            return cell
        default:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingAnswerStatisticCell.reuseIdentifier, for: indexPath) as! VotingAnswerStatisticCell
            let optionsCount = question.options.count
            let index = indexPath.row > optionsCount ? indexPath.row - optionsCount - 2 : indexPath.row - 1
            let option = question.options[index]
            if let result = self.questionsResults[indexPath.section].result.enumerated().first(where: { $0.element.key == option.id } )?.element {
                let value = indexPath.row > questions.count ? result.value.tokens : result.value.investors
                let isLastCell = index == question.options.count - 1
                cell.setup(for: option.answer ?? "", value: value, last: isLastCell)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let questionID = self.questionsResults[indexPath.section].question
        guard let question = self.questions.first(where: { $0.id == questionID }) else { return UITableView.automaticDimension }
        switch indexPath.row {
        case 0, 1 + question.options.count:
            return UITableView.automaticDimension
        default:
            let optionsCount = question.options.count
            let index = indexPath.row > optionsCount ? indexPath.row - optionsCount - 2 : indexPath.row - 1
            let option = question.options[index].answer
            let rowHeight = SumLabel().height(for: option, with: Constants.rowLabelWidth)
            return rowHeight + Constants.rowInset
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let text = questions[section].title
        let height = VotingLabel().height(for: text, with: Constants.headerLabelWidth)
        return height + Constants.headerHeight
    }
}
