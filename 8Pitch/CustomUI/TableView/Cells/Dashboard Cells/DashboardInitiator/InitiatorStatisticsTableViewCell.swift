//
//  InitiatorStatisticsTableViewCell.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class InitiatorStatisticsTableViewCell: UITableViewCell {
    
    @IBOutlet var typeIcon: UIImageView!
    @IBOutlet var titleLabel: StatisticsTitleLabel!
    @IBOutlet var contentLabel: StatisticsContentLabel!

    func setup(icon: UIImage?, title: String?, content: String?) {
        self.typeIcon.image = icon
        self.titleLabel.text = title
        self.contentLabel.text = content
    }
    
    func setupForPad(icons: [UIImage?], titles: [String?], content: [String?]) {}
}
