//
//  InitiatorStatisticsPadTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 12/21/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class InitiatorStatisticsPadTableViewCell: InitiatorStatisticsTableViewCell {
    @IBOutlet weak var leftIcon: UIImageView!
    @IBOutlet weak var rightIcon: UIImageView!
    @IBOutlet weak var leftTitle: StatisticsTitleLabel!
    @IBOutlet weak var rightTitle: StatisticsTitleLabel!
    @IBOutlet weak var leftValue: ProjectParameterLabel!
    @IBOutlet weak var rightValue: ProjectParameterLabel!
    
    override func setupForPad(icons: [UIImage?], titles: [String?], content: [String?]) {
        super.setupForPad(icons: icons, titles: titles, content: content)
        self.leftIcon.image = icons.first ?? nil
        self.rightIcon.image = icons.last ?? nil
        self.leftTitle.text = titles.first ?? nil
        self.rightTitle.text = titles.last ?? nil
        self.leftValue.text = content.first ?? nil
        self.rightValue.text = content.last ?? nil
    }
}
