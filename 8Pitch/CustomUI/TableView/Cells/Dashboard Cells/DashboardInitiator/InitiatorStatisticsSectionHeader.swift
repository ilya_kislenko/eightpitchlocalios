//
//  InitiatorStatisticsSectionHeader.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let statusTitle = "dashboard.initiators.statistics.status".localized
    static let title = "dashboard.initiators.statistics.title".localized
    static let padInset = CGFloat(20)
}

class InitiatorStatisticsSectionHeader: UIView {
    @IBOutlet var statusTitleLabel: InfoTitleLabel!
    @IBOutlet var statusLabel: StatisticsHeaderStatusLabel!
    @IBOutlet var statusBackgrounView: UIView!
    @IBOutlet var titleLabel: InfoTitleSemiboldLabel!
    @IBOutlet var progressView: PercentView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.statusTitleLabel.text = Constants.statusTitle
        self.titleLabel.text = Constants.title
    }

    func setup(project: InitiatorsProject?) {
        var statusColor: UIColor
        switch project?.projectStatus {
        case .notStarted, .comingSoon:
            statusColor = UIColor(.textGray)
        case .active:
            statusColor = UIColor(.green)
        default:
            statusColor = UIColor(.red)
        }
        self.statusBackgrounView.backgroundColor = statusColor
        self.statusLabel.text = project?.projectStatus?.rawValue.replacingOccurrences(of: "_", with: " ")
        let currentFundingSum = NSDecimalNumber(decimal:project?.graph?.currentFundingSum ?? 0).doubleValue
        let softCap = NSDecimalNumber(decimal:project?.graph?.softCap ?? 0).doubleValue
        let hardCap = NSDecimalNumber(decimal:project?.graph?.hardCap ?? 0).doubleValue
        self.progressView.setupFor(currentFundingSum, softCap: softCap, hardCap: hardCap)
    }
}
