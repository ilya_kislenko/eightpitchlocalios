//
//  InitiatorProjectsTitleTableViewCell.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class InitiatorProjectsTitleTableViewCell: UITableViewCell {
    @IBOutlet var titleTextLabel: ProjectsTitleLabel!
}
