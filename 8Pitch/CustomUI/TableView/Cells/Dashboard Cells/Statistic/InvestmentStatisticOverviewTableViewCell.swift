//
//  InvestmentStatisticOverviewTableViewCell.swift
//  8Pitch
//
//  Created by 8Pitch on 10/2/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Foundation

fileprivate struct Constants {
    static let statisticTitle  = "dashboard.investor.statistic".localized
    static let amountDesccription = "dashboard.investor.amountDesccription".localized
    static let projectsTitle = "dashboard.investor.projecctsTitle".localized
    static let eurSymbol = "currency.eur.symbol".localized
    static let contentOffset = CGFloat(32)
    static let hundredCap = Double(100)
    static let thousandCap = Double(1000)
    static let minimalStep = Double(250)
    static let minimalStepUnder100 = CGFloat(0.125)
    static let minimalStepOver100 = CGFloat(13)
    static let minimalStepOver1000 = CGFloat(125)
    static let itemWidth = CGFloat(140)
    static let itemWidthPad = CGFloat(200)
    static let itemHeight = CGFloat(160)
    static let colorsPalette: [UIColor?] = [UIColor(.statisticsColor1),
                                            UIColor(.statisticsColor2),
                                            UIColor(.statisticsColor3),
                                            UIColor(.statisticsColor4),
                                            UIColor(.statisticsColor5),
                                            UIColor(.statisticsColor6),
                                            UIColor(.statisticsColor7),
                                            UIColor(.statisticsColor8),
                                            UIColor(.statisticsColor8),
                                            UIColor(.statisticsColor10),
                                            UIColor(.statisticsColor11),
                                            UIColor(.statisticsColor12),
                                            UIColor(.statisticsColor13),
                                            UIColor(.statisticsColor14)]
}

class InvestmentStatisticOverviewTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: ProjectsTitleLabel!
    @IBOutlet var totalAmountLabel: AmountLabel!
    @IBOutlet var totalAmountDescriptionLabel: TotalAmountLabel!
    @IBOutlet var statisticView: UIView!
    @IBOutlet var statisticColoredView: UIView!
    @IBOutlet var detailedCollectionView: UICollectionView!
    @IBOutlet var investedProjecctsTitle: ProjectsTitleLabel!
    @IBOutlet var amountLabels: [TitleSumLabel]!
    @IBOutlet var amountSegmentViews: [UIView]!
    @IBOutlet weak var leftInset: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rightInset: NSLayoutConstraint!
    private var paymentGraph: InvestorsPaymentsGraph?
    private var colorsArray: [UIColor?] = Constants.colorsPalette
    @IBOutlet weak var fullStaticticView: UIView!
        
    func setup(paymentGraph: InvestorsPaymentsGraph?) {
        self.paymentGraph = paymentGraph
        if UIDevice.current.userInterfaceIdiom == .pad && self.paymentGraph?.investments.count ?? 0 < 4 {
            let itemsWidth = Constants.itemWidthPad * CGFloat(self.paymentGraph?.investments.count ?? 0) * 1.1
            let inset = (UIScreen.main.bounds.width - itemsWidth)/2
            self.leftInset.constant = inset
            self.rightInset.constant = inset
        }
        while self.colorsArray.count < paymentGraph?.investments.count ?? 0 {
            self.colorsArray.append(contentsOf: Constants.colorsPalette)
        }
        self.configureCollectionView()
        self.setupContent(paymentGraph: paymentGraph)
    }
    
    private func configureCollectionView() {
        self.detailedCollectionView.register(InvestmentStatisticItemCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: InvestmentStatisticItemCollectionViewCell.reuseIdentifier)
        self.detailedCollectionView.delegate = self
        self.detailedCollectionView.dataSource = self
        self.detailedCollectionView.reloadData()
    }
    
    private func setupContent(paymentGraph: InvestorsPaymentsGraph?) {
        self.titleLabel.text = Constants.statisticTitle
        self.totalAmountDescriptionLabel.text = Constants.amountDesccription
        self.totalAmountLabel.text = "\(Constants.eurSymbol)\(self.paymentGraph?.totalMonetaryAmount?.currency ?? "0")"
        self.investedProjecctsTitle.text = Constants.projectsTitle
        guard self.paymentGraph?.investments.count ?? 0 > 1 else {
            self.fullStaticticView.isHidden = true
            self.fullStaticticView.snp.makeConstraints {
                $0.height.equalTo(30)
            }
            return
        }
        self.setupStatisticOverviewStackView()
        self.collectionViewHeight.constant =
            paymentGraph?.totalMonetaryAmount == 0 ?
            0 : self.collectionViewHeight.constant
    }
    
    private func setupStatisticOverviewStackView() {
        guard let allAmount = self.paymentGraph?.totalMonetaryAmount else { return }
        let round = Int(allAmount) % Int(Constants.thousandCap) == 0 ? 0 : 1
        let amount = allAmount < Constants.thousandCap ? allAmount : Double(Int(allAmount/Constants.thousandCap) + round) * 1000
        var indexOfInvestment = 0
        var offset = CGFloat(0)
        self.paymentGraph?.investments.forEach({ (inmestment) in
            if let monetaryAmount = inmestment?.monetaryAmount {
                let availableWidth = UIScreen.main.bounds.width - Constants.contentOffset
                let width = availableWidth * CGFloat((monetaryAmount / amount))
                let view = UIView.init(frame: CGRect(x: offset, y: 0, width: width, height: self.statisticColoredView.frame.height))
                view.backgroundColor = colorsArray[indexOfInvestment]
                self.statisticColoredView.addSubview(view)
                indexOfInvestment += 1
                offset += width
            }
        })
        
        let step = self.stepCalculation(allAmount: allAmount)
        var indexOfLabel = 0
        self.amountLabels.forEach { (label) in
            let value = step * Double(indexOfLabel)
            label.text = value < Constants.thousandCap ? value.currency : "\((value/Constants.thousandCap).rounded().currency) K"
            indexOfLabel += 1
        }
    }
    
    private func stepCalculation(allAmount: Double) -> Double {
        var step: Double
        if allAmount < Constants.thousandCap {
            step = (allAmount/Double(self.amountLabels.count)).rounded()
        } else {
            let allAmountInThousand = (allAmount/Constants.thousandCap).rounded(.up)
            step = allAmountInThousand * Double(Constants.thousandCap) / Double(self.amountLabels.count)
        }
        return step
    }
    
    private func share(value: Double?, part: Double?) -> Double {
        guard let value = value,
              let part = part
        else {
            return 0
        }
        return part / (value / 100)
    }
}

extension InvestmentStatisticOverviewTableViewCell: UICollectionViewDelegate {}

extension InvestmentStatisticOverviewTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.paymentGraph?.investments.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.detailedCollectionView.dequeueReusableCell(withReuseIdentifier: InvestmentStatisticItemCollectionViewCell.reuseIdentifier, for: indexPath) as! InvestmentStatisticItemCollectionViewCell
        if self.paymentGraph?.investments.count ?? indexPath.row > indexPath.row,
           let paymentElement = self.paymentGraph?.investments[indexPath.row] {
            let share = self.share(value: self.paymentGraph?.totalMonetaryAmount, part: paymentElement.monetaryAmount)
            cell.setup(color: self.colorsArray[indexPath.row] ?? UIColor(.red), title: paymentElement.companyName, share: share, amount: paymentElement.monetaryAmount)
        }
        return cell
    }
}

extension InvestmentStatisticOverviewTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: UIDevice.current.userInterfaceIdiom == .pad ? Constants.itemWidthPad : Constants.itemWidth, height: Constants.itemHeight)
    }
}
