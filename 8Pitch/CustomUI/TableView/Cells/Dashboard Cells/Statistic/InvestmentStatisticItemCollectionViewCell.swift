//
//  InvestmentStatisticItemCollectionViewCell.swift
//  8Pitch
//
//  Created by 8Pitch on 10/2/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let eurSymbol = "currency.eur.symbol".localized
    static let cornerRadius: CGFloat = 8
    static let shadowWidth: CGFloat = 0
    static let shadowHeight: CGFloat = 6
    static let shadowRadius: CGFloat = 6
    static let shadowOpacity: Float = 1
}

class InvestmentStatisticItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet var projectColorView: UIView!
    @IBOutlet var colorOverlay: UIView!
    @IBOutlet var progressView: CircularProgressView!
    @IBOutlet var percentageLabel: TitleSumLabel!
    @IBOutlet var nameLabel: TitleSumLightLabel!
    @IBOutlet var amountLabel: TotalAmountLabel!
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }

    class var reuseIdentifier : String {
        return "InvestmentStatisticItemCollectionViewCell"
    }

    func setup(color: UIColor, title: String?, share: Double, amount: Double?) {
        self.addShadow()
        self.setupCorners()
        self.projectColorView.backgroundColor = color
        self.progressView.progressColor = color
        self.progressView.progressEnd = share
        self.percentageLabel.text = share > 1 ? "\(share.rounded())%" : ">0%"
        self.nameLabel.text = title
        self.amountLabel.text = "\(Constants.eurSymbol)\(amount?.currency ?? "0")"
    }

    private func setupCorners() {
        self.projectColorView.clipsToBounds = true
        self.projectColorView.layer.masksToBounds = true
        self.projectColorView.layer.cornerRadius = Constants.cornerRadius
        self.projectColorView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

        self.colorOverlay.clipsToBounds = true
        self.colorOverlay.layer.masksToBounds = true
        self.colorOverlay.layer.cornerRadius = Constants.cornerRadius
        self.colorOverlay.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    func addShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: Constants.shadowWidth, height: Constants.shadowHeight)
        self.layer.shadowColor = UIColor(.darkShadow).cgColor
        self.layer.shadowRadius = Constants.shadowRadius
        self.layer.shadowOpacity = Constants.shadowOpacity
    }
}
