//
//  VotingButtonCell.swift
//  8Pitch
//
//  Created by 8pitch on 3/3/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class VotingButtonCell: UITableViewCell {

    private var votingButtonView: VotingButtonView!
    
    func setup(for user: AccountGroup, delegate: OpenVotingFlowDelegate) {
        self.votingButtonView.setup(for: user)
        self.votingButtonView.delegate = delegate
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addVotingButtonView()
    }
    
    private func addVotingButtonView() {
        guard let view = UINib(nibName: "VotingButtonView", bundle: nil).instantiate(withOwner: self, options: nil).first as? VotingButtonView else {
            return
        }
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        self.votingButtonView = view
    }
}
