//
//  VotingButtonView.swift
//  8Pitch
//
//  Created by 8pitch on 3/3/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

protocol OpenVotingFlowDelegate: class {
    func openVoting()
}

class VotingButtonView: UIView {
    
    weak var delegate: OpenVotingFlowDelegate?
    
    @IBOutlet weak var titleLabel: VotingLabel!
    @IBOutlet weak var buttonTitleLabel: InfoLabel!
    @IBOutlet weak var buttonArrow: UIImageView!
    @IBOutlet weak var tappableView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.openVoting))
        self.tappableView.addGestureRecognizer(tap)
    }
    
    func setup(for user: AccountGroup) {
        switch user {
        case .initiator:
            self.titleLabel.text = "dashboard.voting.title.initiator".localized
        default:
            self.titleLabel.text = "dashboard.voting.title.investor".localized
        }
        self.buttonTitleLabel.text = "dashboard.voting.button.title".localized
    }
    
    @objc private func openVoting() {
        self.backgroundColor = UIColor(.votingGray)
        self.buttonArrow.tintColor = UIColor(.gray)
        self.buttonTitleLabel.textColor = UIColor(.gray)
        self.delegate?.openVoting()
    }
}
