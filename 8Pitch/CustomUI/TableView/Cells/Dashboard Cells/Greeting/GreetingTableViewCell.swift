//
//  GreetingTableViewCell.swift
//  8Pitch
//
//  Created by 8Pitch on 10/2/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let autoScrollInterval = Double(8)
    static let greetingTableViewCellAspectRatio = CGFloat(25 / 16)
}

class GreetingTableViewCell: UITableViewCell {
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    @IBOutlet var greetingCollectionView: DynamicHeightCollectionView!
    @IBOutlet var visibleItemIndicatorStackView: UIStackView!
    
    private var images: [UIImage] = []
    private var imagesID: [String?] = []
    private var greeting: [String?] = []
    private var urls: [String?] = []
    private var currentItemIndex: Int = 0
    
    private var timer = Timer()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.images = []
        self.imagesID = []
        self.greeting = []
        self.urls = []
        self.visibleItemIndicatorStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        self.timer.invalidate()
    }
    
    func setup(sliderContent: [SliderContent], isInitiator: Bool, greeting: String) {
        self.greetingCollectionView.contentInsetAdjustmentBehavior = .never
        self.greetingCollectionView.register(GreetingItemCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: GreetingItemCollectionViewCell.reuseIdentifier)
        self.images = isInitiator ? [UIImage(.initiatorPreview)] : [UIImage(.investorPreview)]
        self.greeting = [greeting]
        self.visibleItemIndicatorStackView.addArrangedSubview(UIView())
        self.setupSlider(content: sliderContent)
        self.visibleItemIndicatorStackView.isHidden = sliderContent.isEmpty
        self.greetingCollectionView.reloadData()
    }
    
    deinit {
        self.timer.invalidate()
    }
    
    private func setupSlider(content: [SliderContent]) {
        guard !content.isEmpty else { return }
        content.forEach {
            self.imagesID.append($0.backgroundImageFileID)
            self.greeting.append($0.header)
            self.urls.append($0.link)
            self.visibleItemIndicatorStackView.addArrangedSubview(UIView())
        }
        self.updateContentOffset(self.greetingCollectionView)
        self.timer = Timer.scheduledTimer(timeInterval: Constants.autoScrollInterval, target: self, selector: #selector(scrollToNext), userInfo: nil, repeats: false)
    }
}

extension GreetingTableViewCell: UICollectionViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let currentCell = self.greetingCollectionView.visibleCells.first else { return }
        self.currentItemIndex = self.greetingCollectionView.indexPath(for: currentCell)?.row ?? 0
        self.updateContentOffset(scrollView)
    }
    
    @objc private func scrollToNext() {
        let nextIndex = self.currentItemIndex + 1
        self.currentItemIndex = nextIndex >= self.greeting.count ? nextIndex - self.greeting.count : nextIndex
        let cellSize = CGSize(width: self.frame.width, height: self.frame.height)
        let contentOffset = self.greetingCollectionView.contentOffset
        let x = self.currentItemIndex > self.greeting.count ? contentOffset.x - cellSize.width : contentOffset.x + cellSize.width
        self.greetingCollectionView.scrollToItem(at: IndexPath(item: self.currentItemIndex, section: 0), at: .left, animated: true)
        self.greetingCollectionView.scrollRectToVisible(CGRect(x: x, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        self.updateContentOffset(self.greetingCollectionView)
    }
    
    private func updateContentOffset(_ scrollView: UIScrollView) {
        self.timer.invalidate()
        for (key, value) in self.visibleItemIndicatorStackView.arrangedSubviews.enumerated() {
            value.backgroundColor = key == self.currentItemIndex ? UIColor(.red) : UIColor(.white)
        }
        self.timer = Timer.scheduledTimer(timeInterval: Constants.autoScrollInterval, target: self, selector: #selector(scrollToNext), userInfo: nil, repeats: false)
    }
}

extension GreetingTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.frame.width, height: collectionView.frame.width / (Constants.greetingTableViewCellAspectRatio) )
    }
    
    func loadProjectDetailsImageData(_ ID: String, _ imageView: UIImageView) {
        self.delegate?.loadProjectDetailsImage(ID, imageView)
    }
}

extension GreetingTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.greeting.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.greetingCollectionView.dequeueReusableCell(withReuseIdentifier: GreetingItemCollectionViewCell.reuseIdentifier, for: indexPath) as! GreetingItemCollectionViewCell
        cell.greetingLabel.text = self.greeting[indexPath.row]
        if (indexPath.row == 0) {
            cell.imageView.image = self.images.first
        } else {
            if let id = self.imagesID[indexPath.row - 1] {
                self.loadProjectDetailsImageData(id, cell.imageView)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            guard let urlString = urls[indexPath.row - 1],
                  let url = URL(string: urlString),
                  UIApplication.shared.canOpenURL(url) else {
                return
            }
            UIApplication.shared.open(url)
        }
    }
}
