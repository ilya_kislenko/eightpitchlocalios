//
//  ProjectsListTableViewCell.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let investorCellHeight = CGFloat(500)
    static let initiatorCellHeight = CGFloat(580)
    static let investorPadCellHeight = CGFloat(408)
    static let initiatorPadCellHeight = CGFloat(456)
    static let widthOffset = CGFloat(48)
}

class ProjectsListTableViewCell: UITableViewCell {
    @IBOutlet var projectItemCollectionView: DynamicHeightCollectionView!
    private var projects: [ProjectInfo?] = []
    fileprivate var isInitiator: Bool = false
    private var investorCellHeight: CGFloat = Constants.investorCellHeight
    private var initiatorCellHeight: CGFloat = Constants.initiatorCellHeight
    
    weak var delegate: ProjectCollectionViewCellDelegate?
    
    func setup(isInitiator: Bool, projects: [ProjectInfo?]) {
        self.projectItemCollectionView.delegate = self
        self.projectItemCollectionView.dataSource = self
        self.projectItemCollectionView.register(ProjectCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectCollectionViewCell.reuseIdentifier)
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.projectItemCollectionView.register(ProjectPadCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectPadCollectionViewCell.reuseIdentifier)
            self.investorCellHeight = Constants.investorPadCellHeight
            self.initiatorCellHeight = Constants.initiatorPadCellHeight
        }
        self.isInitiator = isInitiator
        self.projects = projects
        self.projectItemCollectionView.reloadData()
    }
}

extension ProjectsListTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelectProject(identifier: self.projects[indexPath.row]?.projectIdentifier())
    }
}

extension ProjectsListTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.projects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProjectCollectionViewCell
        if UIDevice.current.userInterfaceIdiom == .pad {
            cell = self.projectItemCollectionView.dequeueReusableCell(withReuseIdentifier: ProjectPadCollectionViewCell.reuseIdentifier, for: indexPath) as! ProjectPadCollectionViewCell
        } else {
            cell = self.projectItemCollectionView.dequeueReusableCell(withReuseIdentifier: ProjectCollectionViewCell.reuseIdentifier, for: indexPath) as! ProjectCollectionViewCell
        }
        cell.setup(isInitiator: self.isInitiator, project: self.projects[indexPath.item])
        cell.delegate = self
        return cell
    }
}

extension ProjectsListTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: UIScreen.main.bounds.width - Constants.widthOffset, height: self.isInitiator ? self.initiatorCellHeight : self.investorCellHeight)
    }
}

extension ProjectsListTableViewCell: ProjectCollectionViewCellDelegate {
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView, completionHandler: VoidClosure?) {
        self.delegate?.loadProjectDetailsImage(ID, imageView)
    }
    
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView) {
        self.delegate?.loadProjectDetailsImage(ID, imageView)
    }
    
    func didSelectProject(identifier: Int?) {}
    
    func showMoreInfo(identifier: String?) {
        self.delegate?.showMoreInfo(identifier: identifier)
    }
}
