//
//  ProjectsListSectionHeader.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

protocol ProjectsListSectionHeaderDelegate: class {
    func didSelect(type: ProjectStatusType)
}

fileprivate struct Constants {
    static let indicatorOffset: CGFloat = -Constants.indicatorHeight
    static let indicatorHeight: CGFloat = 2
    static let runningSegmentTitle = "dashboard.tabs.running".localized
    static let finishedSegmentTitle = "dashboard.tabs.finished".localized
}

class ProjectsListSectionHeader: UIView {
    @IBOutlet var segmentedControl: ProjectsSegmentedControl!

    weak var delegate: ProjectsListSectionHeaderDelegate?
    
    private let indicator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(.red)
        return view
    }()

    @IBAction func tap(_ sender: UISegmentedControl) {
        self.handleAppearanceOnTouch(sender, indicator: indicator)
        self.delegate?.didSelect(type: ProjectStatusType.init(rawValue: sender.selectedSegmentIndex) ?? .running)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.segmentedControl.apportionsSegmentWidthsByContent = true
        self.addSubview(indicator)
        self.setupSegmentedControl(segmentedControl: segmentedControl, with: indicator)
    }

    func setupSegmentedControl(segmentedControl: ProjectsSegmentedControl, with indicator: UIView) {
        self.segmentedControl.setupFor(titles: [Constants.runningSegmentTitle, Constants.finishedSegmentTitle])
        self.fixBackgroundSegmentControl(segmentedControl)
        self.setDefaultConstraints(for: indicator, dependsOn: segmentedControl)
        indicator.snp.makeConstraints {
            $0.leading.equalTo(self.segmentedControl.snp.leading)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.handleAppearanceOnTouch(self.segmentedControl, indicator: self.indicator)
    }

    func handleAppearanceOnTouch(_ sender: UISegmentedControl, indicator: UIView) {
        let width = self.segmentedControl.segmentWidthByContent(for: segmentedControl.selectedSegmentIndex)
        let offset = self.segmentedControl.segmentOffsetByContent(for: segmentedControl.selectedSegmentIndex)
        indicator.snp.remakeConstraints {
            $0.leading.equalTo(segmentedControl.snp.leading).offset(offset)
            $0.top.equalTo(segmentedControl.snp.bottom).offset(Constants.indicatorOffset)
            $0.height.equalTo(Constants.indicatorHeight)
            $0.width.equalTo(width)
        }
    }

    func setDefaultConstraints(for indicator: UIView, dependsOn segmentedControl: UISegmentedControl) {
        let width = self.segmentedControl.segmentWidthByContent(for: segmentedControl.selectedSegmentIndex)
        indicator.snp.makeConstraints {
            $0.leading.equalTo(segmentedControl.snp.leading)
            $0.top.equalTo(segmentedControl.snp.bottom).offset(Constants.indicatorOffset)
            $0.height.equalTo(Constants.indicatorHeight)
            $0.width.equalTo(width)
        }
    }

    func fixBackgroundSegmentControl( _ segmentedControl: UISegmentedControl) {
        if #available(iOS 13.0, *) {
            //TODO: chrck it be possible to set an empty/opaque background image via a public API instead of hiding subviews like in the old days?
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                for i in 0...(segmentedControl.numberOfSegments - 1)  {
                    let backgroundSegmentView = segmentedControl.subviews[i]
                    backgroundSegmentView.isHidden = true
                }
            }
        }
    }
}
