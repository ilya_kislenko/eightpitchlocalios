//
//  VotingSurveyCell.swift
//  8Pitch
//
//  Created by 8pitch on 4/1/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class VotingSurveyCell: UITableViewCell {
    
    @IBOutlet weak var startHintLabel: VotingHintLabel!
    @IBOutlet weak var endHintLabel: VotingHintLabel!
    @IBOutlet weak var startLabel: PersonalHeaderLabel!
    @IBOutlet weak var endLabel: PersonalHeaderLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.startHintLabel.text = "voting.details.survey.start".localized.uppercased()
        self.endHintLabel.text = "voting.details.survey.end".localized.uppercased()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.startLabel.text = nil
        self.endLabel.text = nil
    }
    
    func setup(for voting: Voting?) {
        if let start = voting?.informationCollectionPeriodStartDate,
           let end = voting?.informationCollectionPeriodEndDate {
            self.startLabel.text = DateHelper.shared.uiFormatterFull.string(from: start)
            self.endLabel.text = DateHelper.shared.uiFormatterFull.string(from: end)
        }
    }
}
