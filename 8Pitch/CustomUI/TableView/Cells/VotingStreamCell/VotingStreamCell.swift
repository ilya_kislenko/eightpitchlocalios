//
//  VotingStreamCell.swift
//  8Pitch
//
//  Created by 8pitch on 4/1/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class VotingStreamCell: UITableViewCell {
    
    @IBOutlet weak var startHintLabel: VotingHintLabel!
    @IBOutlet weak var endHintLabel: VotingHintLabel!
    @IBOutlet weak var startLabel: PersonalHeaderLabel!
    @IBOutlet weak var endLabel: PersonalHeaderLabel!
    @IBOutlet weak var descriptionHintLabel: VotingHintLabel!
    @IBOutlet weak var descriptionLabel: PersonalHeaderLabel!
    @IBOutlet weak var agendaHintLabel: VotingHintLabel!
    @IBOutlet weak var agendaLabel: FAQLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.startHintLabel.text = "voting.approve.settings.streamStart".localized.uppercased()
        self.endHintLabel.text = "voting.approve.settings.endStart".localized.uppercased()
        self.descriptionHintLabel.text = "voting.approve.settings.streamDescription".localized.uppercased()
        self.agendaHintLabel.text = "voting.approve.settings.streamAgenda".localized.uppercased()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.startLabel.text = nil
        self.endLabel.text = nil
    }
    
    func setup(for voting: Voting?) {
        if let start = voting?.streamStartDate,
           let end = voting?.streamEndDate {
            self.startLabel.text = DateHelper.shared.uiFormatterFull.string(from: start)
            self.endLabel.text = DateHelper.shared.uiFormatterFull.string(from: end)
        }
        self.descriptionLabel.text = voting?.streamTheme
        self.agendaLabel.text = voting?.streamAgenda
    }
}
