//
//  NotificationTableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import TTTAttributedLabel

class NotificationTableViewCell: UITableViewCell {
    
    weak var delegate: OpenVotingPageDelegate?

    @IBOutlet weak var readView: UIView!
    @IBOutlet weak var timeLabel: TimeLabel!
    @IBOutlet weak var expandButton: UIButton!
    @IBOutlet weak var descriptionLabel: TTTAttributedLabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.timeLabel.text = nil
        self.descriptionLabel.text = nil
    }
    
    var onExpand: VoidClosure?
    
    @IBAction func expandButtonClicked(_ sender: Any) {
        onExpand?()
    }
    
    func changeButtonState(isExpanded: Bool?) {
        guard let isExpanded = isExpanded else { return }
        descriptionLabel.numberOfLines = isExpanded ? 0 : 3
        expandButton.setImage(isExpanded ? UIImage(.arrowUp) : UIImage(.arrowDown), for: .normal)
    }
    
    func setup(_ notification: ProjectNotification) {
        guard let text = notification.parseToMutableString(baseUrl: Remote().webURL) else {
            return
        }
        self.selectionStyle = .none
        descriptionLabel.delegate = self
        let activeLinkAttributes = NSMutableDictionary(dictionary: descriptionLabel.activeLinkAttributes)
        activeLinkAttributes[NSAttributedString.Key.foregroundColor] = UIColor(.redText)
        activeLinkAttributes[NSAttributedString.Key.font] = UIFont.roboto.regular.labelSize
        descriptionLabel.linkAttributes = activeLinkAttributes as NSDictionary as? [AnyHashable: Any]
        descriptionLabel.activeLinkAttributes = activeLinkAttributes as NSDictionary as? [AnyHashable: Any]
        let range = NSRange(location: 0, length: text.length)
        text.addAttribute(.font, value: UIFont.roboto.regular.labelSize,
                          range: NSRange(location: 0, length: text.length))
        text.addAttribute(.kern, value: -0.24,
                          range: NSRange(location: 0, length: text.length))
        let style = NSMutableParagraphStyle()
        style.lineHeightMultiple = 1.37
        text.addAttribute(.paragraphStyle, value: style,
                          range: NSRange(location: 0, length: text.length))
        text.addAttribute(.backgroundColor, value: UIColor.clear, range: range)
        text.addAttribute(.foregroundColor, value: notification.status == .read ? UIColor(.answerText) : UIColor(.gray), range: range)
        timeLabel.text = transformDate(notification.created)
        descriptionLabel.setText(text)
        readView.isHidden = notification.status == .read
        expandButton.isHidden = isExpandButtonHidden()
        changeButtonState(isExpanded: notification.isExpanded)
    }
    
    private func isExpandButtonHidden() -> Bool {
        descriptionLabel.numberOfLines = 0
        let newSize = descriptionLabel.sizeThatFits(CGSize(width: Constants.fixedWidthOfTextView, height: CGFloat.greatestFiniteMagnitude))
        return newSize.height < Constants.textViewMaximumUnexpandedHeight
    }
}

private extension NotificationTableViewCell {
    
    private enum Constants {
        static let fixedWidthOfTextView: CGFloat = UIScreen.main.bounds.width - 92
        static let textViewMaximumUnexpandedHeight: CGFloat = 56
    }
    
    //TO DO
    func transformDate(_ date: Date?) -> String? {
        guard let date = date else { return nil }
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "dd.MM.yy HH:mm"
        return formatter.string(from: date)
    }
    
    func handleDeepLink(type: VotingActionType, id: String) {
        switch type {
        case .approve:
            self.delegate?.approveVoting(with: id)
        case .pass:
            self.delegate?.passVoting(with: id)
        case .list:
            self.delegate?.votingsList(with: id)
        case .stream:
            self.delegate?.getVoting(with: id)
        case .survey:
            self.delegate?.votingSurvey()
        case .trustee:
            self.delegate?.trustee(with: id)
        }
    }
}

extension NotificationTableViewCell: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        if let votingActionType = VotingActionType.allCases.first(where: { url.absoluteString.contains($0.rawValue) }) {
            self.handleDeepLink(type: votingActionType, id: url.lastPathComponent)
        } else {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
