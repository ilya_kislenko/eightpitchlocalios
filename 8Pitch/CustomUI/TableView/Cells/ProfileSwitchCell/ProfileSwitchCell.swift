//
//  ProfileSwitchCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/18/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProfileSwitchCell: UITableViewCell {
    
    enum CellType: String {
        case newsletters
        case twoFA
        case pinCode
        case touchID
        case faceID
    }
    
    var switchToggled: BoolHandler?
    
    @IBOutlet weak private var profileSwitch: UISwitch!
    @IBOutlet weak var label: InfoTitleLabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var separator: SeparatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        profileSwitch.layer.cornerRadius = Constants.switchRadius
    }
    
    func setupFor(_ type: CellType) {
        self.cellImage.image = UIImage(named: type.rawValue)
        let text = Constants.title + type.rawValue
        self.label.text = text.localized
        let isBottomSeparatorShouldBeHidden = type == .touchID || type == .faceID
        self.separator.isHidden = isBottomSeparatorShouldBeHidden
        if type == .pinCode {
            let separator = SeparatorView()
            self.contentView.addSubview(separator)
            separator.snp.makeConstraints {
                $0.trailing.leading.top.equalToSuperview()
            }
        }
    }
    
    func hideSeparator() {
        self.separator.isHidden = true
    }
    
    func setState(_ state: Bool) {
        self.profileSwitch.isOn = state
    }
    @IBAction func switchToggled(_ sender: UISwitch) {
        self.switchToggled?(sender.isOn)
    }
    
    private enum Constants {
        static let title = "profile.switchCell."
        static let switchRadius: CGFloat = 16
    }
    
}
