//
//  VotingSettingsCell.swift
//  8Pitch
//
//  Created by 8pitch on 3/9/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class VotingSettingsCell: UITableViewCell {
    
    @IBOutlet weak var noTrusteeConstraint: NSLayoutConstraint!
    @IBOutlet weak var startHintLabel: VotingHintLabel!
    @IBOutlet weak var endHintLabel: VotingHintLabel!
    @IBOutlet weak var startLabel: PersonalHeaderLabel!
    @IBOutlet weak var endLabel: PersonalHeaderLabel!
    @IBOutlet weak var sendResultButton: CheckmarkBlackButton!
    @IBOutlet weak var sendResultLabel: ApproveLabel!
    @IBOutlet weak var addTrusteeLabel: ApproveLabel!
    @IBOutlet weak var addTrusteeButton: CheckmarkBlackButton!
    @IBOutlet weak var trusteeLabel: TrusteeLabel!
    @IBOutlet weak var shareResultButton: CheckmarkBlackButton!
    @IBOutlet weak var shareResultLabel: ApproveLabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.startLabel.text = nil
        self.endLabel.text = nil
        self.trusteeLabel.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.sendResultLabel.text = "voting.approval.checkmark.sendResult".localized
        self.shareResultLabel.text = "voting.approve.checkbox.shareResult".localized
        self.addTrusteeLabel.text = "voting.approve.checkbox.addTrustee".localized
        self.startHintLabel.text = "voting.approval.hint.start".localized
        self.endHintLabel.text = "voting.approval.hint.end".localized
    }
    
    func setup(with voting: Voting?) {
        guard let voting = voting else { return }
        self.sendResultButton.isSelected = voting.sendResultsToParticipants
        self.addTrusteeButton.isSelected = voting.addTrustee
        self.shareResultButton.isSelected = voting.shareResultsWithTrustee
        self.trusteeLabel.isHidden = !voting.addTrustee
        self.noTrusteeConstraint.priority = voting.addTrustee ? .defaultLow : .required
        
        if let start = voting.startDate, let end = voting.endDate {
            self.startLabel.text = DateHelper.shared.uiFormatterFull.string(from: start)
            self.endLabel.text = DateHelper.shared.uiFormatterFull.string(from: end)
        }
        
        if let trustee = voting.trustee {
            let title = trustee.prefix.rawValue.localized
            let name = trustee.firstName
            let surname = trustee.lastName
            self.trusteeLabel.text = "voting.approve.checkbox.trustee".localized + "\(title) \(name) \(surname)"
        }
    }
}
