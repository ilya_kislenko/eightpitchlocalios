//
//  VotingInfoCell.swift
//  8Pitch
//
//  Created by 8pitch on 12.03.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class VotingInfoCell: UITableViewCell {
    
    enum CellType: String {
        case name = "voting.approval.hint.companyName"
        case date = "voting.passing.hint.period"
    }
    
    enum Constants {
        static let topInset = CGFloat(28)
        static let bottomInset = CGFloat(8)
    }
    
    @IBOutlet weak var topInset: NSLayoutConstraint!
    @IBOutlet weak var bottomInset: NSLayoutConstraint!
    @IBOutlet weak var hintLabel: VotingHintLabel!
    @IBOutlet weak var nameLabel: PersonalHeaderLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.topInset.constant = Constants.topInset
        self.bottomInset.constant = Constants.bottomInset
    }
    
    func setup(with text: String?, type: CellType) {
        self.hintLabel.text = type.rawValue.localized
        self.nameLabel.text = text?.uppercased()
        if type == .date {
            self.topInset.constant = Constants.bottomInset
            self.bottomInset.constant = Constants.topInset
        }
    }
}
