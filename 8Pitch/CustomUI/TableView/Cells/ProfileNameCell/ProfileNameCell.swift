//
//  ProfileNameCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/15/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ProfileNameCell: UITableViewCell {
    
    @IBOutlet weak var label: ProjectsTitleLabel!
    
    public func setupFor(name: String?) {
        self.label.text = name
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
}
