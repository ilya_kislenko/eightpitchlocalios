//
//  VotingProjectDetailsCell.swift
//  8Pitch
//
//  Created by 8pitch on 16.03.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class VotingProjectDetailsCell: UITableViewCell {
    
    @IBOutlet weak var icon1: UIImageView!
    @IBOutlet weak var icon2: UIImageView!
    @IBOutlet weak var title1: StatisticsTitleLabel!
    @IBOutlet weak var title2: StatisticsTitleLabel!
    @IBOutlet weak var data1: UILabel!
    @IBOutlet weak var data2: UILabel!
    @IBOutlet weak var separator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.data1.text = nil
        self.data2.text = nil
        self.separator.snp.makeConstraints {
            $0.height.equalTo(0.5)
            $0.leading.trailing.equalToSuperview().inset(16)
            $0.centerY.equalToSuperview()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.data1.text = nil
        self.data2.text = nil
    }
    
    func setupWith(voting: Voting, for account: AccountGroup) {
        switch voting.status {
        case .waitingForApproval:
            icon1.image = UIImage(.votingDate)
            icon2.image = UIImage(.votingUser)
            title1.text = "voting.details.submissionDate".localized.uppercased()
            title2.text = "voting.details.approvalReqBy".localized.uppercased()
            if let startDate = voting.startDate {
                data1.text = DateHelper.shared.uiFormatterFull.string(from: startDate)
            }
            if voting.createdBy == Voting.Creator.admin {
                data2.text = "issuer".localized.uppercased()
            } else if voting.createdBy == Voting.Creator.PI {
                data2.text = "8pitch".localized.uppercased()
            }
        case .pending:
            switch account {
            case .initiator:
                icon1.image = UIImage(.votingPendingCellStart)
                icon2.image = UIImage(.votingPendingCellEnd)
                title1.text = "voting.details.startOfVoting".localized.uppercased()
                title2.text = "voting.details.remainingTime".localized.uppercased()
                if let startDate = voting.startDate, let endDate = voting.endDate,
                   let remaining = Calendar.current.dateComponents([.day], from: startDate, to: endDate).day {
                    let endString = DateHelper.shared.uiFormatterFullComma.string(from: endDate)
                    data1.text = DateHelper.shared.uiFormatterFull.string(from: startDate)
                    data2.text = remaining == 1 ? String(format: "voting.remainingTime.pi.hint.one".localized, String(remaining), endString) :
                        String(format: "voting.remainingTime.pi.hint.some".localized, String(remaining), endString)
                    data2.text = startDate > Date() ? "voting.remainingTime.pi.hint.notStarted".localized : data2.text
                }
            case .institutionalInvestor, .privateInvestor:
                icon1.image = UIImage(.votingPendingCellStart)
                icon2.image = UIImage(.votingPendingCellEnd)
                title1.text = "voting.details.startOfVoting".localized.uppercased()
                title2.text = "voting.details.remainingTime".localized.uppercased()
                if let startDate = voting.startDate, let endDate = voting.endDate {
                    data1.text = DateHelper.shared.uiFormatterFull.string(from: startDate)
                    data2.text = DateHelper.shared.uiFormatterFull.string(from: endDate)
                }
            }
        case .stream,
             .streamRunning:
            icon1.image = UIImage(.votingPendingCellStart)
            icon2.image = UIImage(.votingPendingCellEnd)
            title1.text = "voting.approve.settings.streamStart".localized.uppercased()
            title2.text = "voting.approve.settings.endStart".localized.uppercased()
            if let startDate = voting.streamStartDate, let endDate = voting.streamEndDate {
                data1.text = DateHelper.shared.uiFormatterFull.string(from: startDate)
                data2.text = DateHelper.shared.uiFormatterFull.string(from: endDate)
            }
        case .infoCollection,
             .infoCollectionRunning:
            icon1.image = UIImage(.votingPendingCellStart)
            icon2.image = UIImage(.votingPendingCellEnd)
            title1.text = "voting.details.survey.start".localized.uppercased()
            title2.text = "voting.details.survey.end".localized.uppercased()
            if let startDate = voting.informationCollectionPeriodStartDate, let endDate = voting.informationCollectionPeriodEndDate {
                data1.text = DateHelper.shared.uiFormatterFull.string(from: startDate)
                data2.text = DateHelper.shared.uiFormatterFull.string(from: endDate)
            }
        case .waitTrustee:
            icon1.image = UIImage(.votingPendingCellEnd)
            icon2.image = UIImage(.votingUser)
            title1.text = "voting.trustee.details.period".localized.uppercased()
            title2.text = "voting.trustee.details.trustee".localized.uppercased()
            if let trustee = voting.trustee {
                data2.text = "\(trustee.prefix.rawValue.localized) \(trustee.firstName) \(trustee.lastName)"
            }
            if let endDate = voting.endDate {
                data1.text = DateHelper.shared.uiFormatterFull.string(from: endDate)
            }
        case .completed:
            switch account {
            case .initiator:
                icon1.image = UIImage(.votingPendingCellStart)
                icon2.image = UIImage(.votingPendingCellEnd)
                title1.text = "voting.approval.hint.start".localized.uppercased()
                title2.text = "voting.approval.hint.completed".localized.uppercased()
                if let startDate = voting.startDate, let endDate = voting.endDate {
                    data1.text = DateHelper.shared.uiFormatterFull.string(from: startDate)
                    data2.text = DateHelper.shared.uiFormatterFull.string(from: endDate)
                }
            case .institutionalInvestor, .privateInvestor:
                icon1.image = UIImage(.votingPeriod)
                icon2.image = UIImage(.votingDate)
                title1.text = "voting.details.votingPeriod".localized.uppercased()
                title2.text = "voting.details.votingDate".localized.uppercased()
                if let startDate = voting.startDate, let endDate = voting.endDate {
                    let startEndDate = "\(DateHelper.shared.uiFormatterFull.string(from: startDate)) - \(DateHelper.shared.uiFormatterFull.string(from: endDate))"
                    data1.text = startEndDate
                    data2.text = DateHelper.shared.uiFormatterFull.string(from: startDate)
                }
            }
        case .running: //part
            icon1.image = UIImage(.votingPendingCellStart)
            icon2.image = UIImage(.votingPendingCellEnd)
            title1.text = "voting.details.startOfVoting".localized.uppercased()
            title2.text = "voting.details.remainingTime".localized.uppercased()
            if let startDate = voting.startDate, let endDate = voting.endDate,
               let remaining = Calendar.current.dateComponents([.day], from: startDate, to: endDate).day {
                let endString = DateHelper.shared.uiFormatterFullComma.string(from: endDate)
                data1.text = DateHelper.shared.uiFormatterFull.string(from: startDate)
                data2.text = remaining == 1 ? String(format: "voting.remainingTime.pi.hint.one".localized, String(remaining), endString) :
                    String(format: "voting.remainingTime.pi.hint.some".localized, String(remaining), endString)
                data2.text = startDate > Date() ? "voting.remainingTime.pi.hint.notStarted".localized : data2.text
            }
        default:
            break
        }
    }
}
