//
//  VotingProjectsCell.swift
//  8Pitch
//
//  Created by 8pitch on 15.03.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class VotingProjectsCell: UITableViewCell {
    
    @IBOutlet weak var name: InfoTitleLabel!
    @IBOutlet weak var separator: UIView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.separator.isHidden = false
    }
}
