//
//  ProfileCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/15/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    enum Parameter: String {
        case personal
        case payments
        case security
        case settings
        case help
        case legal
        case logout
        case delete
        case password
        case login
    }

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var label: InfoTitleLabel!
    
    public func setupFor(parameter: Parameter) {
        let imageName = Constants.profile + parameter.rawValue.capitalized
        self.profileImage.image = UIImage(named: imageName)
        let text = Constants.localizeProfilePath + parameter.rawValue
        self.label.text = text.localized
        switch parameter {
        case .settings:
            return
        case .delete, .logout, .password, .login:
            self.addSeparator(inset: 0)
        default:
            self.addSeparator(inset: Constants.separatorInset)
        }
    }
    
    private enum Constants {
        static let profile = "profile"
        static let localizeProfilePath = "profile.cell."
        static let separatorInset: CGFloat = 16
    }
    
    private func addSeparator(inset: CGFloat) {
        let separator = SeparatorView()
        self.contentView.addSubview(separator)
        separator.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.trailing.leading.equalToSuperview().inset(inset)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.contentView.subviews.first(where: { $0 is SeparatorView })?.removeFromSuperview()
    }
    
}
