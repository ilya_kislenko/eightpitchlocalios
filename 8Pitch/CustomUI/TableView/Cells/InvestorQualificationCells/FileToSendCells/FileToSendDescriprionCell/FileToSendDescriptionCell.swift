//
//  FileToSendDescriptionCell.swift
//  8Pitch
//
//  Created by 8Pitch on 9.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class FileToSendDescriptionCell: UITableViewCell {
    
    private enum Constant {
        static let titleTopConstant: CGFloat = 16
        static let titleBottomConstant: CGFloat = 12
        static let documentConstant: CGFloat = 4
        static let multipleHeightConstant: CGFloat = 1.47
    }

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    func setupForTitlePreview(title: String?) {
        guard let title = title else { return }
        titleLabel.font = UIFont.roboto.medium.questionarrieAssetsTextSize
        titleLabel.text = String(format: "investor.qualification.needed.file.title".localized, title.localized)
        titleLabel.setLineSpacing(lineHeightMultiple: Constant.multipleHeightConstant)
        topConstraint.constant = Constant.titleTopConstant
        bottomConstraint.constant = Constant.titleBottomConstant
    }
    
    func setupForDocument(documentName: String?) {
        guard let documentName = documentName else { return }
        titleLabel.font = UIFont.roboto.light.questionarrieAssetsTextSize
        titleLabel.text = "• " + documentName
        titleLabel.setLineSpacing(lineHeightMultiple: Constant.multipleHeightConstant)
        topConstraint.constant = Constant.documentConstant
        bottomConstraint.constant = Constant.documentConstant
    }
    
}
