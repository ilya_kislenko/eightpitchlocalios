//
//  NextButtonCellCell.swift
//  8Pitch
//
//  Created by 8Pitch on 13.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class NextButtonCellCell: UITableViewCell {

    @IBOutlet weak var nextButton: NavigationButton!
    
    var nextButtonAction: (()->())?
    
    @IBAction func nextButtonClick(_ sender: Any) {
        nextButtonAction?()
    }
    
}
