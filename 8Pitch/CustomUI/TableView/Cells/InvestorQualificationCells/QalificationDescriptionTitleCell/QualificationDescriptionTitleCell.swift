//
//  QualificationDescriptionTitleCell.swift
//  8Pitch
//
//  Created by 8Pitch on 14.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class QualificationDescriptionTitleCell: UITableViewCell {
    
    struct Constants {
        static let descriptionLabelLineHeightMultipleConstant: CGFloat = 1.11
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.text = "investor.qualification.page.description".localized
        titleLabel.setLineSpacing(lineHeightMultiple: Constants.descriptionLabelLineHeightMultipleConstant)
    }
    
}
