//
//  AttachedFileDescriptionCell.swift
//  8Pitch
//
//  Created by 8Pitch on 13.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class AttachedFileDescriptionCell: UITableViewCell {
    
    @IBOutlet weak var checkmarkIcon: UIImageView!
    @IBOutlet weak var fileNameLabel: UILabel!
    @IBOutlet weak var fileSizeLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var mainBackgroundView: UIView!
    
    var removeButtonAction: (()->())?
    
    @IBAction func removeButtonAction(_ sender: Any) {
        removeButtonAction?()
    }
    
    func setup(attachedFileInfo: AttachedFile?) {
        guard let attachedFileInfo = attachedFileInfo, let fileExtension = attachedFileInfo.fileExtension, let fileName =  attachedFileInfo.name, let size = attachedFileInfo.size else { return }
        fileNameLabel.text = "\(fileName).\(fileExtension)"
        fileSizeLabel.text = Units(bytes: Int64(size)).getReadableUnit()
    }
    
}
