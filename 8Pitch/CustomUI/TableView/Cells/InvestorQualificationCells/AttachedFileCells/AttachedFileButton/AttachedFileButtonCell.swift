//
//  AttachedFileButtonCell.swift
//  8Pitch
//
//  Created by 8Pitch on 13.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class AttachedFileButtonCell: UITableViewCell {
    
    private enum Constants {
        static let cornerRadius: CGFloat = 8
    }

    @IBOutlet weak var addFileButton: UIButton!
    @IBOutlet weak var mainBackroundView: UIView!
    
    var addFileButtonAction: (()->())?
    
    @IBAction func addFileButtonAction(_ sender: Any) {
        addFileButtonAction?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.mainBackroundView.roundView(radius: Constants.cornerRadius, on: [.bottomLeft, .bottomRight])
        }
    }
    
}
