//
//  AttachedFileHeaderCellCell.swift
//  8Pitch
//
//  Created by 8Pitch on 13.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class AttachedFileHeaderCellCell: UITableViewCell {

    private enum Constants {
        static let cornerRadius: CGFloat = 8
    }
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainBackgroundView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = "investor.qualification.attach.file.title".localized
        DispatchQueue.main.async {
            self.mainBackgroundView.roundView(radius: Constants.cornerRadius, on: [.topLeft, .topRight])
        }
    }
    
}
