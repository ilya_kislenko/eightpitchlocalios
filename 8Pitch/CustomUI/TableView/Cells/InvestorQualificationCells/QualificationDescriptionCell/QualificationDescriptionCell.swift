//
//  QualificationDescriptionCell.swift
//  8Pitch
//
//  Created by 8Pitch on 13.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class QualificationDescriptionCell: UITableViewCell {

    @IBOutlet weak var textField: TextField!
    
    var textFieldText: ((String?)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.addTarget(self, action: #selector(qualificationTypeChosen), for: .allEditingEvents)
        self.textField.placeholder = "Description"
    }
    
    @objc private func qualificationTypeChosen() {
        textFieldText?(textField.text)
    }
    
}
