//
//  QualificatonCheckMarkCell.swift
//  8Pitch
//
//  Created by 8Pitch on 13.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import Remote

class QualificatonCheckMarkCell: UITableViewCell {
    
    private enum Constants {
        static let languageCodeDE = "de"
    }

    @IBOutlet weak var checkMarkButton: CheckmarkBlackButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var toggleChanged: ((Bool)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let lineHeightMultiple = NSMutableParagraphStyle()
        lineHeightMultiple.lineHeightMultiple = 1.16
        let policyRange = Locale.current.languageCode == Constants.languageCodeDE ? NSRange(location: 140, length: 20) : NSRange(location: 137, length: 15)
        let attributedString = NSMutableAttributedString(string: "investor.qualification.privacy.policy".localized, attributes:[.foregroundColor: UIColor(.textGray), .font: UIFont.roboto.regular.textSize, .paragraphStyle:lineHeightMultiple])

        
        attributedString.addAttribute(.link, value: WebURLConstants.privacyPolicy, range: policyRange)

        attributedString.addAttributes([.foregroundColor:UIColor(.link)], range: policyRange)
        
        self.descriptionTextView.attributedText = attributedString
    }
    
    @IBAction func checkMarkButtonClick(_ sender: CheckmarkBlackButton) {
        sender.isSelected.toggle()
        toggleChanged?(checkMarkButton.isSelected)
    }
    
}

extension QualificatonCheckMarkCell: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
