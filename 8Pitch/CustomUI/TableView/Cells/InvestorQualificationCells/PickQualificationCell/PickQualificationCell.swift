//
//  PickQualificationCell.swift
//  8Pitch
//
//  Created by 8Pitch on 13.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class PickQualificationCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleField : QualificationTypeTextField!
    
    var qualificationTypeChoosed : ((QualificationType) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel.text = "investor.qualification.picker.title".localized
        self.titleField.placeholder = "investor.qualification.picker.placeholder".localized
        self.titleField.addTarget(self, action: #selector(qualificationTypeChosen), for: .allEditingEvents)
    }
    
    @objc private func qualificationTypeChosen() {
        titleField.rowSelected = { [weak self] (string) in
            self?.qualificationTypeChoosed?(QualificationType(rawTitle: string))
        }
    }
    
}
