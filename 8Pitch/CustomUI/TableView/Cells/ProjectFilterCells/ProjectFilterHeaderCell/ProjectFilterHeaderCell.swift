//
//  ProjectFilterHeaderCell.swift
//  8Pitch
//
//  Created by 8pitch on 18.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class ProjectFilterHeaderCell: UITableViewCell {

    @IBOutlet weak var mainBackgroundView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    func setup(type: ProjectFilterViewModel.TabType, hideSeparator: Bool) {
        bottomConstraint.constant = hideSeparator ? Constants.ConstraintHeightWithSeparator : Constants.ConstraintHeightWithoutSeparator
        separatorView.isHidden = hideSeparator
        setupImageAndTitleFor(type: type)
        mainBackgroundView.layer.cornerRadius = Constants.CornerRadius
    }
    
    private func setupImageAndTitleFor(type: ProjectFilterViewModel.TabType) {
        switch type {
        case .status:
            mainImageView.image = UIImage(.clock)
            titleLabel.text = "project.filter.summary.project.status.title".localized
        case .investmentAmount:
            mainImageView.image = UIImage(.euroIcon)
            titleLabel.text = "project.filter.summary.invest.amount.title".localized
        case .typesOfSecurity:
            mainImageView.image = UIImage(.security)
            titleLabel.text = "project.filter.summary.financial.instrument.title".localized
        }
    }
    
    private enum Constants {
        static let CornerRadius: CGFloat = 4
        static let ConstraintHeightWithSeparator : CGFloat = 8
        static let ConstraintHeightWithoutSeparator : CGFloat = 0
    }
    
}
