//
//  ProjectFilterElementCell.swift
//  8Pitch
//
//  Created by 8pitch on 18.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectFilterElementCell: UITableViewCell {

    @IBOutlet weak var mainBackgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectedImageView: UIImageView!

    func setup(title: String, type: ProjectFilterViewModel.TabType, isItemSelected: Bool) {
        setupTitleLabelFor(type: type, with: title)
        self.titleLabel.textColor = isItemSelected ? UIColor(.white) : UIColor(.gray)
        self.mainBackgroundView.backgroundColor = isItemSelected ? UIColor(.backgroundDark) : UIColor(.switchGray)
        self.selectedImageView.isHidden = !isItemSelected
        self.mainBackgroundView.layer.cornerRadius = Constants.CornerRadius
    }
    
    private func setupTitleLabelFor(type: ProjectFilterViewModel.TabType, with title: String) {
        switch type {
        case .status:
            titleLabel.text = ("projects.filter.status." + title).localized
        case .typesOfSecurity:
            titleLabel.text = title
        case .investmentAmount:
            titleLabel.text = title.localizedRange()
        }
    }
    
    private enum Constants {
        static let CornerRadius: CGFloat = 4
    }
}
