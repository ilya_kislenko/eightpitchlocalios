//
//  ProjectFilterSelectedStateCell.swift
//  8Pitch
//
//  Created by 8pitch on 18.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectFilterSelectedStateCell: UITableViewCell {

    @IBOutlet weak var mainBackgroundView: UIView!
    @IBOutlet weak var additionalBachgroundView: UIView!
    @IBOutlet weak var mainTitleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var additionalBotomContraint: NSLayoutConstraint!
    
    var closeButtonAction: (()->())?
    
    func setup(item: ProjectStatus, isLastItem: Bool) {
        self.additionalBachgroundView.layer.cornerRadius = Constants.roundRadius
        self.mainBackgroundView.layer.cornerRadius = isLastItem ? Constants.roundRadius : 0
        self.bottomConstraint.constant = isLastItem ? Constants.lastItemBottomConstraing : Constants.normalItemBottomConstraing
        self.additionalBotomContraint.constant = isLastItem ? Constants.lastItemAdditionalBottomViewConstraint : Constants.normalItemBottomConstraing
        mainTitleLabel.text = "projects.filter.status.\(item.rawValue)".localized
        
    }
    
    func setup(item: String, type: ProjectFilterViewModel.TabType, isLastItem: Bool) {
        self.additionalBachgroundView.layer.cornerRadius = Constants.roundRadius
        self.mainBackgroundView.layer.cornerRadius = isLastItem ? Constants.roundRadius : 0
        self.bottomConstraint.constant = isLastItem ? Constants.lastItemBottomConstraing : Constants.normalItemBottomConstraing
        self.additionalBotomContraint.constant = isLastItem ? Constants.lastItemAdditionalBottomViewConstraint : Constants.normalItemBottomConstraing
        switch  type {
        case .investmentAmount:
            mainTitleLabel.text = item.localizedRange()
        case .status, .typesOfSecurity:
            mainTitleLabel.text = item
        }
    }

    @IBAction func closeButtonClick(_ sender: Any) {
        closeButtonAction?()
    }
    
    private enum Constants {
        static let lastItemBottomConstraing: CGFloat = 8
        static let lastItemAdditionalBottomViewConstraint: CGFloat = 16
        static let normalItemBottomConstraing: CGFloat = 0
        static let roundRadius: CGFloat = 4
    }
}
