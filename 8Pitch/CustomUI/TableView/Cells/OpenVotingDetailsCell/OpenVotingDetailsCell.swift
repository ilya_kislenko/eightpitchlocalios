//
//  OpenVotingDetailsCell.swift
//  8Pitch
//
//  Created by 8pitch on 24.03.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class OpenVotingDetailsCell: UITableViewCell {
    
    @IBOutlet weak var title: InfoTitleLabel!
    @IBOutlet weak var hint: DescriptionLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.hint.text = "votings.open.details.hint".localized
    }
    
    func setup(for type: ProjectVotingsViewController.VotingCell) {
        switch type {
        case .plug:
            self.title.text = "voting.open.title.survey".localized
        case .openPass, .openApprove:
            self.title.text = "votings.open.details.title".localized
        case .loadAnswers,
             .loadResults,
             .stream,
             .popUp:
            return
        }
    }
}
