//
//  SettingsHeaderView.swift
//  8Pitch
//
//  Created by 8pitch on 9/18/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class SettingsHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var label: ProjectDescriptionLabel!
    @IBOutlet weak var separator: SeparatorView!

    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "SettingsHeaderView"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.separator.isHidden = false
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor(.white)
        self.separator.backgroundColor = UIColor(.separatorGray)
    }
    
    func setText(_ text: String) {
        self.label.text = text
    }
    
    func hideSeparator() {
        self.separator.isHidden = true
    }

}
