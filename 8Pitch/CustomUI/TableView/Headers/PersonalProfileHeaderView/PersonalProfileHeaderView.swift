//
//  PersonalProfileHeaderView.swift
//  8Pitch
//
//  Created by 8pitch on 9/16/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PersonalProfileHeaderView: UITableViewHeaderFooterView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor(.white)
    }
    
    @IBOutlet weak var titleLabel: PersonalHeaderLabel!
    
    func setTitle(_ title: String?) {
        self.titleLabel.text = title
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "PersonalProfileHeaderView"
    }
    
}
