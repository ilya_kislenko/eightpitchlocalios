//
//  ExpandableHeader.swift
//  8Pitch
//
//  Created by 8pitch on 12.03.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class ExpandableHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: VotingLabel!
    @IBOutlet weak var chevronView: UIImageView!

    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ExpandableHeader"
    }
    
    func setText(_ text: String, expanded: Bool) {
        self.titleLabel.text = text
        self.chevronView.image = expanded ? UIImage(.chevronUp) : UIImage(.chevronDown)
    }
    
    func setupForTrustee() {
        self.chevronView.isHidden = true
    }
}
