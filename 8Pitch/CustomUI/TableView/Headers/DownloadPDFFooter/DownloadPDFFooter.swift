//
//  DownloadPDFFooter.swift
//  8Pitch
//
//  Created by 8pitch on 3/16/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class DownloadPDFFooter: UITableViewHeaderFooterView {
    weak var delegate: LoadFileDelegate?
    @IBOutlet weak var downloadButton: RoundedButton!
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "DownloadPDFFooter"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let attachment = NSTextAttachment()
        let image = UIImage(.whiteLoad)
        attachment.image = image
        attachment.bounds = CGRect(x: 0,
                                   y: ((UIFont.barlow.medium.placeholderSize.capHeight).rounded() - 24) / 2,
                                   width: 24,
                                   height: 24)
        let attachmentString = NSAttributedString(attachment: attachment)
        let text = NSMutableAttributedString(string: "voting.answers.button".localized)
        text.append(attachmentString)
        text.addAttribute(.foregroundColor, value: UIColor(.white), range: NSRange(location: 0, length: text.length))
        self.downloadButton.setAttributedTitle(text, for: .normal)
    }
    
    @IBAction func download(_ sender: UIButton) {
        self.delegate?.loadFile()
    }
}
