//
//  VotingQuestionHeader.swift
//  8Pitch
//
//  Created by 8pitch on 12.03.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class VotingQuestionHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var hintLabel: QuestionLabel!
    @IBOutlet weak var titleLabel: VotingLabel!
    @IBOutlet weak var hintView: UIView!
    @IBOutlet weak var hintInset: NSLayoutConstraint!
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "VotingQuestionHeader"
    }
    
    func setup(for question: Question, filled: Bool, result: Bool = false) {
        let min = String(question.minimumAnswers)
        let max = String(question.maximumAnswers)
        let all = String(question.options.count)
        var title: String = ""
        if min == max && min == "1" {
            let format = filled ? "voting.answer.hint.one".localized : "voting.passing.question.hint.one".localized
            title = String(format: format, min, all)
        } else if min == max {
            let format = filled ? "voting.answer.hint.some".localized : "voting.passing.question.hint.some".localized
            title = String(format: format, min, all)
        } else {
            let format = filled ? "voting.answer.hint".localized : "voting.passing.question.hint".localized
            title = String(format: format, min, max, all)
        }
        self.hintLabel.text = title
        let removeHint = question.type == .radiobutton || result
        self.hintView.isHidden = removeHint
        self.hintInset.priority = removeHint ? .defaultLow : .defaultHigh
        self.hintView.backgroundColor = question.overLimit ? UIColor(.questionHintRed) : UIColor(.questionGray)
        self.hintLabel.textColor = question.overLimit ? UIColor(.red) : UIColor(.textGray)
        self.titleLabel.text = question.title
        self.hintView.roundView(radius: 4, on: .allCorners)
    }
}
