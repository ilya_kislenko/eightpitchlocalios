//
//  UpdateQuestionaryHeaderView.swift
//  8Pitch
//
//  Created by 8pitch on 1/26/21.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class UpdateQuestionaryHeaderView: UITableViewHeaderFooterView {
    
    public var upgradeAction: VoidClosure?
    
    @IBOutlet weak var separator: SeparatorView!
    
    @IBAction func upgradeAccountTapped(_ sender: RoundedButton) {
        upgradeAction?()
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "UpdateQuestionaryHeaderView"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor(.white)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.separator?.isHidden = false
    }
    
    func hideSeparator() {
        self.separator.isHidden = true
    }
}
