//
//  KYCSummaryHeaderView.swift
//  8Pitch
//
//  Created by 8pitch on 28.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class KYCSummaryHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var progressView: ProgressView!
    @IBOutlet weak var titleLabel: UILabel!

    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "KYCSummaryHeaderView"
    }
    
    func setup(stepsCount: Int) {
        tintColor = UIColor(.white)
        contentView.backgroundColor = UIColor(.white)
        self.progressView.numberOfItems = stepsCount
        self.progressView.currentIndex = 3
        let text = NSMutableAttributedString()
        text.append(NSAttributedString(string: "summarize.info.1".localized, attributes: [.foregroundColor : UIColor(.gray), .font : UIFont.roboto.regular.labelSize]))
        let attributedText = NSAttributedString(string: "summarize.info.2".localized, attributes: [.foregroundColor: UIColor(.red), .strokeColor: UIColor(.red), .font : UIFont.roboto.bold.labelSize])
        text.append(attributedText)
        self.titleLabel.attributedText = text
    }
    
}
