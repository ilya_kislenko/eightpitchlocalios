//
//  TrusteeDecisionFooter.swift
//  8Pitch
//
//  Created by 8pitch on 4/5/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class TrusteeDecisionFooter: UITableViewHeaderFooterView {
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "TrusteeDecisionFooter"
    }
    
    weak var delegate: TrusteeDecisionDelegate?
    
    @IBOutlet weak var withoutInstructionButton: BorderedButton!
    @IBOutlet weak var withInstructionButton: BorderedButton!
    @IBOutlet weak var hintLabel: PositionLabel!
    @IBOutlet weak var voteYourselfButton: RoundedButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.withoutInstructionButton.layer.borderColor = UIColor(.gray).cgColor
        self.withInstructionButton.layer.borderColor = UIColor(.gray).cgColor
        let noInstructionTitle = self.attributedTitle("voting.trustee.button.transferWithout".localized, "voting.trustee.button.without".localized)
        self.withoutInstructionButton.setAttributedTitle(noInstructionTitle, for: .normal)
        let instructionTitle = self.attributedTitle("voting.trustee.button.transferWith".localized, "voting.trustee.button.with".localized)
        self.withInstructionButton.setAttributedTitle(instructionTitle, for: .normal)
        self.voteYourselfButton.setTitle("voting.trustee.button.vote".localized.uppercased(), for: .normal)
        self.hintLabel.text = "voting.trustee.hint.transferWith".localized
    }
    
    @IBAction func noInstructionSelected(_ sender: UIButton) {
        self.delegate?.noInstructionSelected()
    }
    
    @IBAction func withInstructionSelected(_ sender: UIButton) {
        self.delegate?.instructionSelected()
    }
    
    @IBAction func voteSelected(_ sender: UIButton) {
        self.delegate?.vote()
    }
    
    private func attributedTitle(_ text: String, _ boldText: String) -> NSAttributedString {
        let text = String(format: text, boldText) as NSString
        let boldedRange = text.range(of: boldText)
        let style = NSMutableParagraphStyle()
        style.lineHeightMultiple = 1.02
        style.alignment = .center
        let attributedText = NSMutableAttributedString(string: text as String,
                                                       attributes: [.foregroundColor: UIColor(.gray),
                                                                    .font: UIFont.barlow.regular.placeholderSize,
                                                                    .paragraphStyle: style])
        attributedText.addAttribute(.font, value: UIFont.barlow.bold.placeholderSize, range: boldedRange)
        return attributedText
    }
}
