//
//  ApproveVotingFooter.swift
//  8Pitch
//
//  Created by 8pitch on 3/9/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class ApproveVotingFooter: UITableViewHeaderFooterView {
    weak var delegate: ApproveVotingViewProtocol?

    @IBOutlet weak var approveButton: RoundedButton!
    @IBOutlet weak var rejectButton: BorderedButton!
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ApproveVotingFooter"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.rejectButton.layer.borderColor = UIColor(.gray).cgColor
        self.approveButton.setTitle("voting.approval.button.approve".localized, for: .normal)
        self.rejectButton.setTitle("voting.approval.button.reject".localized, for: .normal)
    }
    
    @IBAction func approveTapped(_ sender: UIButton) {
        self.delegate?.approve()
    }
    
    @IBAction func rejectTapped(_ sender: UIButton) {
        self.delegate?.confirmReject()
    }
}
