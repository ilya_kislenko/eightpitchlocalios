//
//  ApproveVotingHeader.swift
//  8Pitch
//
//  Created by 8pitch on 3/9/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class ApproveVotingHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var titleLabel: InfoTitleSemiboldLabel!
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ApproveVotingHeader"
    }
    
    func setText(_ text: String) {
        self.titleLabel.text = text
    }
}
