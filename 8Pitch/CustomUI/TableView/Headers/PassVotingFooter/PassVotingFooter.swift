//
//  PassVotingFooter.swift
//  8Pitch
//
//  Created by 8pitch on 12.03.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//RoundedButton

import UIKit
import Input

class PassVotingFooter: UITableViewHeaderFooterView {
    var voting: Voting? {
        didSet {
            self.submitButton.isEnabled = self.voting?.passed ?? false
            self.checkmarkButton.isSelected = self.voting?.confirmedByUser ?? false
        }
    }
    weak var delegate: PassVotingViewProtocol?
    
    @IBOutlet weak var infoLabel: ApproveLabel!
    @IBOutlet weak var submitButton: StateButton!
    @IBOutlet weak var checkmarkButton: CheckmarkBlackButton!
    @IBOutlet weak var hintLabel: PositionLabel!
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "PassVotingFooter"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.submitButton.setTitle("voting.passing.button".localized, for: .normal)
        self.infoLabel.text = "voting.passing.checkbox.text".localized
        self.infoLabel.setNeedsLayout()
        self.infoLabel.layoutIfNeeded()
        self.infoLabel.sizeThatFits(self.infoLabel.intrinsicContentSize)
        self.hintLabel.text = "voting.trustee.hint.transferWith".localized
    }
    
    func setupForTrustee() {
        self.hintLabel.isHidden = false
        self.infoLabel.isHidden = true
        self.checkmarkButton.isHidden = true
        self.voting?.confirmedByUser = true
    }
    
    @IBAction func submit(_ sender: UIButton) {
        self.delegate?.submit()
    }
    
    @IBAction func confirm(_ sender: CheckmarkBlackButton) {
        sender.isSelected.toggle()
        self.voting?.confirmedByUser = sender.isSelected
        self.submitButton.isEnabled = self.voting?.passed ?? false
    }
}
