//
//  TableViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 15.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.accessoryType = isSelected ? .checkmark : .none
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.accessoryType = .none
        self.textLabel?.text = nil
    }

}
