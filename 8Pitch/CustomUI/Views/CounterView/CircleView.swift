//
//  CircleView.swift
//  anim
//
//  Created by 8pitch on 10.07.2020.
//  Copyright © 2020 8pitch. All rights reserved.
//

import UIKit

final class CircleView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.snp.makeConstraints {
            $0.height.equalTo(self.snp.width)
        }
        self.layer.cornerRadius = frame.height / 2
    }
    
}
