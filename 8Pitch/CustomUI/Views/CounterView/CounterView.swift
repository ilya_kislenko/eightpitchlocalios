//
//  CounterView.swift
//  anim
//
//  Created by 8pitch on 10.07.2020.
//  Copyright © 2020 8pitch. All rights reserved.
//

import UIKit
import SnapKit

class CounterView: View {
    
    // MARK: UI Components
    
    private var digitImageView = UIImageView(image: UIImage(named: Image.splashStep(for: 1)))
    private let internalView = CircleView()
    private let middleView = CircleView()
    private let externalView = CircleView()
    
    // MARK: Overrides
    
    override func setup() {
        super.setup()
        self.setupContent()
    }
    
    // MARK: Public
    
    public func startAnimating(completion: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            self?.animateViews()
        }
        (1...8).tick(by: Constants.pulseDuration) { [weak self] in
            guard let currentTick = $0 else {
                completion()
                return
            }
            self?.digitImageView.image = UIImage(named: Image.splashStep(for: currentTick))
        }
    }
}

// MARK: Private

private extension CounterView {
    
    func setupContent() {
        self.digitImageView.tintColor = UIColor(.white)
        let uiComponents = [self.externalView, self.middleView, self.internalView]
        uiComponents.forEach {
            $0.backgroundColor = UIColor.white.withAlphaComponent(0.1)
        }
        addSubview(externalView)
        self.externalView.addSubview(self.middleView)
        self.middleView.addSubview(self.internalView)
        addSubview(self.digitImageView)
        self.setConstraints()
    }
    
    func setConstraints() {
        self.snp.makeConstraints {
            $0.height.equalTo(self.snp.width)
        }
        self.externalView.snp.makeConstraints {
            $0.height.equalToSuperview()
            $0.center.equalToSuperview()
        }
        self.middleView.snp.makeConstraints {
            $0.height.equalTo(self.snp.height).multipliedBy(Constants.middleCircleHeightFactor)
            $0.center.equalToSuperview()
        }
        self.internalView.snp.makeConstraints {
            $0.height.equalTo(self.snp.height).multipliedBy(Constants.internalCircleHeightFactor)
            $0.center.equalToSuperview()
        }
        self.digitImageView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    private func animation(duration: Double) -> CASpringAnimation {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = duration
        pulse.fromValue = 1.0
        pulse.toValue = 1.02
        pulse.autoreverses = true
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 0.8
        return pulse
    }
    
    func animateViews() {
        let externalAnimation = self.animation(duration: Constants.externalAnimationDuration)
        let middleAnimation = self.animation(duration: Constants.middleAnimationDuration)
        let internalAnimation = self.animation(duration: Constants.internalAnimationDuration)
        
        self.externalView.layer.add(externalAnimation, forKey: "pulse")
        self.middleView.layer.add(middleAnimation, forKey: "pulse")
        self.internalView.layer.add(internalAnimation, forKey: "pulse")
    }
    
    // MARK: Constants
    
    enum Constants {
        static let numbersCount: Int = 8
        
        static let pulseDuration: Double = 0.2
        
        static let externalAnimationDuration: Double = 0.4
        static let middleAnimationDuration: Double = 0.6
        static let internalAnimationDuration: Double = 0.8
        
        static let middleCircleHeightFactor: Double = 0.75
        static let internalCircleHeightFactor: Double = 0.5
    }
    
}
