//
//  KeyboardView.swift
//  8Pitch
//
//  Created by 8pitch on 12/14/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class KeyboardView: View {
    private let nibName = "KeyboardView"
    private var contentView: UIView?
    public var checkBiometry: VoidClosure?
    public var savePin: StringClosure?
    private var text: String = "" {
        didSet {
            self.savePinAndSetupButtons()
        }
    }
    @IBOutlet var numberButtons: [UIButton]!
    @IBOutlet weak var funcButton: UIButton!
    
    override func setup() {
        super.setup()
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        self.numberButtons.forEach { $0.addTarget(self, action: #selector(self.writeNumber), for: .touchUpInside) }
    }
    
    private func loadViewFromNib() -> UIView? {
        let nib = UINib(nibName: nibName, bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    @IBAction func deleteLast(_ sender: UIButton) {
        self.text.removeLast()
    }
    
    @IBAction func checkBiometry(_ sender: UIButton) {
        self.checkBiometry?()
    }
    
    @objc private func writeNumber(_ sender: UIButton) {
        self.text = self.text + String(sender.tag)
    }
    
    private func savePinAndSetupButtons() {
        self.savePin?(self.text)
        if self.text.count < 4 {
            self.enableButtons()
        } else {
            self.disableButtons()
        }
    }
    
    private func disableButtons() {
        self.numberButtons.forEach { $0.isUserInteractionEnabled = false }
    }
    
    private func enableButtons() {
        self.numberButtons.forEach { $0.isUserInteractionEnabled = true }
    }
    
    public func clearPin() {
        self.text = ""
    }
}
