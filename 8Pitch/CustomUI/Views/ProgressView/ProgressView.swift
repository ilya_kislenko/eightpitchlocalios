//
//  ProgressView.swift
//  8Pitch
//
//  Created by 8pitch on 16.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

@IBDesignable class ProgressView : View {
    
    lazy private var view : UIView = {
        let retVal = UIView()
        self.addSubview(retVal)
        retVal.snp.makeConstraints {
            $0.width.equalTo(self.snp.width)
            $0.height.equalTo(self.snp.height)
            $0.center.equalTo(self.snp.center)
        }
        return retVal
    }()
    
    @IBInspectable var currentIndex : Int = 1 {
        didSet {
            guard (1 ... self.numberOfItems) ~= self.currentIndex else {
                fatalError("invalid current index set!")
            }
            self.reducedCurrentIndex = self.currentIndex - 1
        }
    }
    private var reducedCurrentIndex : Int = 0 {
        didSet {
            self.redraw()
        }
    }
    
    @IBInspectable var numberOfItems : Int = 0 {
        didSet {
            guard self.numberOfItems > 0 else {
                fatalError("invalid number of items set!")
            }
            self.reducedNumberOfItems = self.numberOfItems - 1
        }
    }
    private var reducedNumberOfItems : Int = 0 {
        didSet {
            self.redraw()
        }
    }
    private var progressViews : [UIView] = []
    
    override func setup() {
        super.setup()
        self.redraw()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.redraw()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.redraw()
    }
    
    private func redraw() {
        guard self.numberOfItems > 0 else {
            return
        }
        guard self.view.frame.width > 0 else {
            return
        }
        self.progressViews.forEach { $0.removeFromSuperview() }
        self.progressViews.removeAll()
        (0 ... (self.reducedNumberOfItems)).forEach { i in
            self.insertImageView(at: i)
            if i < self.reducedNumberOfItems {
                self.insertLineView(at: i)
            }
        }
    }
    
    private func insertImageView(at i: Int) {
        let image = UIImage(named: "splash\(i + 1)")
        let imageView = UIImageView(image: image)
        if i <= self.reducedCurrentIndex {
            imageView.tintColor = UIColor(.red)
        } else {
            imageView.tintColor = UIColor(.placeholderGray)
        }
        imageView.backgroundColor = UIColor(.white)
        self.view.addSubview(imageView)
        imageView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        imageView.snp.makeConstraints {
            $0.width.equalTo(Constants.digitWidth)
            $0.height.equalToSuperview()
            $0.centerY.equalToSuperview()
            if let previous = self.progressViews.last {
                $0.leading.equalTo(previous.snp.trailing)
            } else {
                $0.leading.equalToSuperview()
            }
        }
        self.progressViews.append(imageView)
    }
 
    private func insertLineView(at i: Int) {
        let line = UIView()
        if self.reducedCurrentIndex > 0,
           i < self.reducedCurrentIndex
        {
            line.backgroundColor = UIColor(.red)
        } else {
            line.backgroundColor = UIColor(.placeholderGray)
        }
        self.view.addSubview(line)
        let numberOfLines = CGFloat(self.reducedNumberOfItems)
        let numberOfItems = CGFloat(self.numberOfItems)
        let lineWidth = floor((self.view.frame.width - (Constants.digitWidth * numberOfItems)) / numberOfLines)
        line.setContentHuggingPriority(.defaultLow, for: .horizontal)
        line.snp.makeConstraints {
            $0.width.equalTo(lineWidth)
            $0.height.equalTo(Constants.lineHeight)
            $0.centerY.equalToSuperview()
            if let previous = self.progressViews.last {
                $0.leading.equalTo(previous.snp.trailing)
            } else {
                $0.leading.equalToSuperview()
            }
        }
        self.progressViews.append(line)
    }
    
    // MARK: Constants
    
    enum Constants {
        
        static let digitWidth : CGFloat = 38.0
        static let lineHeight : CGFloat = 1.0
        
    }
    
}
