//
//  CircularProgressView.swift
//  8Pitch
//
//  Created by 8Pitch on 10/2/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

@IBDesignable class CircularProgressView: UIView {
    private var progressLayer = CAShapeLayer()
    private var trackLayer = CAShapeLayer()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.makeCircularPath()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.makeCircularPath()
    }

    @IBInspectable var progressColor = UIColor(.white) {
        didSet {
            self.progressLayer.strokeColor = progressColor.cgColor
        }
    }

    @IBInspectable var progressEnd = Double(0) {
        didSet {
            self.progressLayer.strokeEnd = CGFloat(CGFloat(progressEnd)/100)
        }
    }

    @IBInspectable var trackColor = UIColor(.grayCurrecy){
        didSet {
            self.trackLayer.strokeColor = trackColor.cgColor
        }
    }

    func makeCircularPath() {
        self.backgroundColor = UIColor.clear
        self.layer.cornerRadius = self.frame.size.width/2
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width/2, y: frame.size.height/2), radius: (frame.size.width - 1.5)/2, startAngle: CGFloat(-0.5 * .pi), endAngle: CGFloat(1.5 * .pi), clockwise: true)
        self.trackLayer.path = circlePath.cgPath
        self.trackLayer.fillColor = UIColor.clear.cgColor
        self.trackLayer.strokeColor = self.trackColor.cgColor
        self.trackLayer.lineWidth = 4.0
        self.trackLayer.strokeEnd = 1.0
        self.layer.addSublayer(self.trackLayer)
        self.progressLayer.path = circlePath.cgPath
        self.progressLayer.fillColor = UIColor.clear.cgColor
        self.progressLayer.strokeColor = self.progressColor.cgColor
        self.progressLayer.lineWidth = 4.0
        self.progressLayer.strokeEnd = 0.0
        self.layer.addSublayer(self.progressLayer)
    }
}
