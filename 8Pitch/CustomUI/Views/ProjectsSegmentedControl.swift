//
//  SegmentedControl.swift
//  8Pitch
//
//  Created by 8pitch on 20.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ProjectsSegmentedControl: UISegmentedControl {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAppearance()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupAppearance()
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return !gestureRecognizer.isKind(of: UITapGestureRecognizer.self)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let previousIndex = selectedSegmentIndex
        super.touchesEnded(touches, with: event)
        if previousIndex == selectedSegmentIndex, let touchLocation = touches.first?.location(in: self) {
            if bounds.contains(touchLocation) {
                sendActions(for: .valueChanged)
            }
        }
    }
    
    func segmentWidth(for index: Int) -> CGFloat {
        self.widthOfTitle(index: index) + Constants.labelOffset
    }
    
    func segmentWidthByContent(for index: Int) -> CGFloat {
        var width: CGFloat = 0
        let fullWidth = self.frame.width
        for i in 0..<self.numberOfSegments {
            width += self.widthOfTitle(index: i)
        }
        let offset = (fullWidth - width)/CGFloat(self.numberOfSegments)
        return self.widthOfTitle(index: index) + offset
    }
    
    private func widthOfTitle(index: Int) -> CGFloat {
        let title = self.titleForSegment(at: index)
        let attributes = self.titleTextAttributes(for: .normal)
        let label = UILabel()
        label.textAlignment = .center
        label.attributedText = NSAttributedString(string: title ?? "", attributes: attributes)
        label.sizeToFit()
        return label.frame.width
    }
    
    func segmentOffsetByContent(for index: Int) -> CGFloat {
        var offset: CGFloat = 0
        (0..<index).forEach {
            offset += self.segmentWidthByContent(for: $0)
        }
        return offset
    }
    
    func segmentOffset(for index: Int) -> CGFloat {
        var offset: CGFloat = 0
        (0..<index).forEach {
            offset += self.segmentWidth(for: $0)
        }
        return offset
    }
    
    func setupFor(titles: [String]) {
        for (index, title) in titles.enumerated() {
            self.setTitle(title, forSegmentAt: index)
        }
    }
    
    func customizeForPad() {
        self.setTitleTextAttributes([.font : UIFont.barlow.semibold.labelSize, .foregroundColor: UIColor(.textGray)], for: .normal)
        self.setTitleTextAttributes([.font : UIFont.barlow.semibold.labelSize, .foregroundColor: UIColor(.red)], for: .selected)
    }
    
}

private extension ProjectsSegmentedControl {
    enum Constants {
        static let padSegmentWidth = CGFloat(150)
        static let labelOffset = CGFloat(19.5)
    }
    
    func setupAppearance() {
        self.backgroundColor = .clear
        self.tintColor = .clear
        self.setTitleTextAttributes([.font : UIFont.barlow.semibold.titleSize, .foregroundColor: UIColor(.textGray), .kern : 0.3], for: .normal)
        self.setTitleTextAttributes([.font : UIFont.barlow.semibold.titleSize, .foregroundColor: UIColor(.red), .kern : 0.3], for: .selected)
    }
}
