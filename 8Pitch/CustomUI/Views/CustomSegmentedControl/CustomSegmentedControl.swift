//
//  CustomSegmentedControl.swift
//  8Pitch
//
//  Created by 8pitch on 13.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift
import RxCocoa

class CustomSegmentedControl: UISegmentedControl {
    
    public var delegateAction: BoolHandler?
    public var index: Int = 1
    
    
    @IBOutlet weak var button: RoundedButton!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    @IBAction func buttonTapped(_ sender: RoundedButton) {
        self.toggleState()
    }
    
    private enum Constants {
        static let cornerRadius: CGFloat = 4
        static let controlHeight: CGFloat = 44
        static let controlWidth: CGFloat = 128
    }
    
    @objc private func toggleState() {
        self.button.isSelected = !self.button.isSelected
        self.setupUIForState(self.button.isSelected, animated: true)
        self.index = self.button.isSelected ? 0 : 1
        self.delegateAction?(self.button.isSelected)
    }
    
    func setupUIForState(_ selected: Bool, animated: Bool = false) {
        self.button.isSelected = selected
        self.button.setTitle(selected ? "questionnaire.yes".localized : "questionnaire.no".localized, for: .normal)
        self.leadingConstraint.priority = selected ? UILayoutPriority(999) : .defaultHigh
        self.trailingConstraint.priority = selected ? UILayoutPriority(999) : .defaultHigh
        if animated {
            UIView.animate(withDuration: 0.5) { [weak self] in
                self?.layoutIfNeeded()
            }
        } else {
            self.layoutIfNeeded()
        }
    }
    
    func initialSetup() {
        self.isMomentary = true
        self.layer.cornerRadius = Constants.cornerRadius
        button.titleLabel?.font = UIFont.barlow.medium.textSize
        self.snp.makeConstraints {
            $0.height.equalTo(Constants.controlHeight)
            $0.width.equalTo(Constants.controlWidth)
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(toggleState))
        self.addGestureRecognizer(tap)
    }
    
    static func nib(owner: UIView) -> CustomSegmentedControl? {
        let segmentedControl = UINib(nibName: "CustomSegmentedControl", bundle: nil).instantiate(withOwner: owner, options: nil).first as? CustomSegmentedControl
        segmentedControl?.initialSetup()
        return segmentedControl
    }
}
