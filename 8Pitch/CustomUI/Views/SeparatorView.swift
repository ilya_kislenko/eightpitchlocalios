//
//  SeparatorView.swift
//  8Pitch
//
//  Created by 8pitch on 8/25/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class SeparatorView: UIView {

    // MARK: Init
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor(.separatorGray)
        self.snp.makeConstraints {
            $0.height.equalTo(1)
        }
    }

}
