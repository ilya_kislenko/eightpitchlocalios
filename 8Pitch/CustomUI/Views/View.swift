//
//  View.swift
//  8Pitch
//
//  Created by 8pitch on 8/28/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class View: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    func setup() {}
}
