//
//  PickerViewController.swift
//  8Pitch
//
//  Created by 8pitch on 23.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PickerViewController : GenericViewController, SheetDetentPresentationSupport {
    
    var selection : PublishSubject<String?> = .init()
    
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet var picker : UIPickerView!
    var dataSource : [String] = [] {
        didSet {
            guard !self.dataSource.isEmpty,
                  let first = self.dataSource.first else
            {
                return
            }
            self.selection.onNext(first)
        }
    }
    
    @IBOutlet weak var pickerHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var toolbarHeightConstraint: NSLayoutConstraint!
    
    var contentHeight: CGFloat {
        self.pickerHeightConstraint.constant + self.toolbarHeightConstraint.constant * 1.5
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
    }
    
    @IBAction func setSelected(_ sender: UIBarButtonItem) {
        self.selection.onCompleted()
        self.dismiss(animated: true, completion: nil)
    }
    
    func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension PickerViewController : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selection.onNext(self.dataSource[row])
    }
    
}

extension PickerViewController : UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        self.dataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.dataSource[row]
    }
    
}

extension PickerViewController : UIViewControllerTransitioningDelegate {
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return SheetDetentPresentationController(presentedViewController: presented, presenting: presenting)
    }
    
}

