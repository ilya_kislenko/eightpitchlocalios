//
//  ConfirmationView.swift
//  8Pitch
//
//  Created by 8pitch on 8/28/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

public protocol InvestmentDocumentsDelegate {
    func nextButtonAction()
}

class ConfirmationView: View {
    
    // MARK: UI Components
    
    private let statementsTitleLabel = InvestmentInfoTitleLabel()
    private let documentsTitleLabel = InvestmentInfoTitleLabel()
    private let statementsLabel: InvestmentInfoLabel = {
        let label = InvestmentInfoLabel()
        label.font = UIFont.barlow.semibold.titleSize
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    private let documentsLabel: InvestmentInfoLabel = {
        let label = InvestmentInfoLabel()
        label.font = UIFont.barlow.semibold.titleSize
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    private let statementsCheckmarkButton = CheckmarkButton()
    private let documentsCheckmarkButton = CheckmarkButton()
    private let nextButton = StateButton()
    
    public var delegate: InvestmentDocumentsDelegate?
    
    // MARK: Overrides
    
    override func setup() {
        super.setup()
        self.setupAppearance()
        self.setConstraints()
        self.addTargets()
    }
    
    func setupWith( _ documents: [String?]) {
        self.documentsLabel.setupStrings(documents)
    }
}

private extension ConfirmationView {
    
    enum Constants {
        static let labelSideOffset: CGFloat = 12
        static let labelTopOffset: CGFloat = 5
        static let buttonTopOffset: CGFloat = 30
        static let checkmarkButtonTopOffset: CGFloat = 37
    }
    
    func setupAppearance() {
        self.statementsTitleLabel.numberOfLines = 0
        self.documentsTitleLabel.numberOfLines = 0
        self.backgroundColor = .clear
        self.statementsTitleLabel.text = "investment.directDownloader.statements.title".localized
        self.documentsTitleLabel.text = "investment.directDownloader.documents.title".localized
        self.statementsLabel.setupStrings(["investment.directDownloader.statements.firstParagraph".localized, "investment.directDownloader.statements.secondParagraph".localized])
        self.nextButton.setTitle("investment.directDownloader.next".localized, for: .normal)
        self.nextButton.setTitleColor(UIColor(.white), for: .normal)
    }
    
    func setConstraints() {
        let views = [self.statementsTitleLabel, self.documentsTitleLabel,
                     self.statementsLabel, self.documentsLabel,
                     self.statementsCheckmarkButton, self.documentsCheckmarkButton,
                     self.nextButton]
        views.forEach { self.addSubview($0) }
        
        self.statementsTitleLabel.snp.makeConstraints {
            $0.top.trailing.equalToSuperview()
            $0.leading.equalTo(self.statementsCheckmarkButton.snp.trailing).offset(Constants.labelSideOffset)
        }
        
        self.statementsCheckmarkButton.snp.makeConstraints {
            $0.leading.equalToSuperview()
            $0.centerY.equalTo(self.statementsTitleLabel)
        }
        
        self.statementsLabel.snp.makeConstraints {
            $0.trailing.equalToSuperview()
            $0.leading.equalTo(self.statementsCheckmarkButton.snp.trailing).offset(Constants.labelSideOffset)
            $0.top.equalTo(self.statementsTitleLabel.snp.bottom).offset(Constants.labelTopOffset)
        }
        
        
        self.documentsTitleLabel.snp.makeConstraints {
            $0.top.equalTo(self.statementsLabel.snp.bottom).offset(Constants.checkmarkButtonTopOffset)
            $0.trailing.equalToSuperview()
            $0.leading.equalTo(self.documentsCheckmarkButton.snp.trailing).offset(Constants.labelSideOffset)
        }
        
        self.documentsCheckmarkButton.snp.makeConstraints {
            $0.centerY.equalTo(self.documentsTitleLabel)
            $0.leading.equalToSuperview()
        }
        
        self.documentsLabel.snp.makeConstraints {
            $0.leading.trailing.equalTo(self.documentsTitleLabel)
            $0.top.equalTo(self.documentsTitleLabel.snp.bottom).offset(Constants.labelTopOffset)
        }
        
        self.nextButton.snp.makeConstraints {
            $0.top.equalTo(self.documentsLabel.snp.bottom).offset(Constants.buttonTopOffset)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func addTargets() {
        self.documentsCheckmarkButton.addTarget(self, action: #selector(self.checkmarkButtonTapped), for: .touchUpInside)
        self.statementsCheckmarkButton.addTarget(self, action: #selector(self.checkmarkButtonTapped), for: .touchUpInside)
        self.nextButton.addTarget(self, action: #selector(self.nextButtonTapped), for: .touchUpInside)
    }
    
    @objc func checkmarkButtonTapped(_ sender: CheckmarkButton) {
        sender.isSelected.toggle()
        self.nextButton.isEnabled = documentsCheckmarkButton.isSelected && statementsCheckmarkButton.isSelected
    }
    
    @objc func nextButtonTapped(_ sender: StateButton) {
        if documentsCheckmarkButton.isSelected && statementsCheckmarkButton.isSelected {
            self.delegate?.nextButtonAction()
        }
    }
    
}
