//
//  BlurView.swift
//  8Pitch
//
//  Created by 8pitch on 21.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class BlurView: UIVisualEffectView {

    // MARK: Init
    
    override init(effect: UIVisualEffect?) {
        super.init(effect: effect)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }

}

// MARK: Private

private extension BlurView {
    
    func setup() {
        self.alpha = 0.8
        self.backgroundColor = UIColor(.gray).withAlphaComponent(0.8)
    }
    
}
