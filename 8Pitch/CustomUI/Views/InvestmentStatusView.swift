//
//  InvestmentStatusView.swift
//  8Pitch
//
//  Created by 8pitch on 1/18/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class InvestmentStatusView: View {
    
    override func setup() {
        super.setup()
        self.backgroundColor = UIColor(.grayCurrecy)
        self.subviews.forEach { $0.backgroundColor = UIColor(.placeholderGray) }
    }
    
    func setupFor(_ status: InvestmentStatus?) {
        switch status {
        case .confirmed,
             .pending:
            self.backgroundColor = UIColor(.red).withAlphaComponent(0.2)
            self.subviews.forEach { $0.backgroundColor = UIColor(.red).withAlphaComponent(0.7) }
        case .paid:
            self.backgroundColor = UIColor(.answerBackground)
            self.subviews.forEach { $0.backgroundColor = UIColor(.projectGray) }
        case .received:
            self.backgroundColor = UIColor(.projectGreen).withAlphaComponent(0.2)
            self.subviews.forEach { $0.backgroundColor = UIColor(.projectGreen).withAlphaComponent(0.7) }
        case .new,
             .booked,
             .canceldInProgress,
             .canceled,
             .refundedInProgress,
             .refunded,
             .refundFailed,
             .none:
            self.backgroundColor = UIColor(.grayCurrecy)
            self.subviews.forEach { $0.backgroundColor = UIColor(.placeholderGray) }
        }
    }
}
