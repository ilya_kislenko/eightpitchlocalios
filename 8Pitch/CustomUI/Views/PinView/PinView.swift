//
//  PinView.swift
//  8Pitch
//
//  Created by 8pitch on 11/25/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PinView: View {
    
    @IBOutlet weak var stack: UIStackView!
    
    let nibName = "PinView"
    var contentView: UIView?
    
    override func setup() {
        super.setup()
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        self.stack.arrangedSubviews.forEach {
            $0.roundView(radius: $0.frame.height/2, on: .allCorners)
        }
    }
    
    func setupFor(_ index: Int) {
        self.stack.arrangedSubviews.forEach {
            $0.backgroundColor = $0.tag < index ? UIColor(.white) :
                UIColor(white: 1, alpha: 0.3)
        }
    }
    
    func loadViewFromNib() -> UIView? {
        let nib = UINib(nibName: nibName, bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
}
