//
//  TotalAmountView.swift
//  8Pitch
//
//  Created by 8pitch on 07.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class TotalAmountView: UIView {
    
    // MARK: UI Components
    
    @IBOutlet weak var nominalLabel: TitleSumLabel!
    @IBOutlet weak var minimalAmountLabel: TitleSumLabel!
    @IBOutlet weak var feeLabel: TitleSumLabel!
    @IBOutlet weak var nominalValueLabel: SumLabel!
    @IBOutlet weak var minimumAmountValueLabel: SumLabel!
    @IBOutlet weak var feeValueLabel: SumLabel!
    @IBOutlet weak var EURAmountLabel: EuroLabel!
    @IBOutlet weak var tokenAmountLabel: InvestmentErrorInfoLabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var feeView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundView(radius: Constants.radius, on: [.bottomLeft, .bottomRight])
    }
 
    public func setupLabels(values: InvestmentAmountValues?, sum: InvestmentSum?) {
        guard let values = values, let sum = sum else { return }
        self.feeView.isHidden = values.feeRate == 0
        
        let feeInfo = String(format: Constants.feeInfo, "\(values.feeRate.decimalValue.currency) %")
        let labelText: [String] = Constants.labelText + [feeInfo]
        let labels = [self.nominalLabel, self.minimalAmountLabel, self.feeLabel]
        for (index, label) in labels.enumerated() {
            label?.text = labelText[index]
        }
        let infoAmounts = [values.tokenNominalValue.decimalValue.currency, values.minValueInEUR.decimalValue.currency, sum.inEUR.multiplying(by: values.fee).decimalValue.currency]
        let valueLabels = [self.nominalValueLabel, self.minimumAmountValueLabel, self.feeValueLabel]
        for (index, label) in valueLabels.enumerated() {
            label?.text = infoAmounts[index] + " " + Constants.euro
        }
        self.EURAmountLabel.text = "\((sum.inEUR.decimalValue + sum.inEUR.multiplying(by: values.fee).decimalValue).currency) " + "currency.eur.symbol".localized
        self.tokenAmountLabel.text = "\(sum.inToken.intValue.currency) " + values.shortCut
    }
   
}

private extension TotalAmountView {
    
    enum Constants {
        static var labelText = ["payment.transfer.nominal.bank".localized, "payment.transfer.minimal".localized]
        static let feeInfo = "payment.transfer.fee".localized
        static let radius: CGFloat = 8
        static let euro = "currency.eur".localized
    }

}
