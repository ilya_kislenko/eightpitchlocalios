//
//  CustomNavigationBar.swift
//  8Pitch
//
//  Created by 8pitch on 17.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class CustomNavigationBar: UINavigationBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAppearance()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupAppearance()
    }
    
    func setupAppearance() {
        barTintColor = UIColor(.white)
        isTranslucent = false
        setBackgroundImage(UIImage(), for: .default)
        shadowImage = UIImage()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        titleTextAttributes = [
            .font: UIFont.barlow.semibold.titleSize,
            .foregroundColor: UIColor(.gray),
            .kern: 0.3, .paragraphStyle: paragraphStyle]
        items?.forEach {
            $0.rightBarButtonItem?.setTitleTextAttributes(titleTextAttributes, for: .normal)
        }
    }
    
    func setupForDark() {
        self.backgroundColor = UIColor(.backgroundDark)
        self.barTintColor = UIColor(.backgroundDark)
    }
    
    func setupForRed() {
        self.backgroundColor = UIColor(.red)
        self.barTintColor = UIColor(.red)
    }
    
    func setupForWhite() {
        self.backgroundColor = UIColor(.white)
        self.barTintColor = UIColor(.white)
    }
}
