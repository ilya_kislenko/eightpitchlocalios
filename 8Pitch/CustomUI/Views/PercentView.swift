//
//  PercentView.swift
//  8Pitch
//
//  Created by 8pitch on 18.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PercentView: UIView {
    
    struct Content {
        let part: Int
        let label: UILabel
        let view: UIView
    }
    
    // MARK: Colors
    
    private var projectsScreen: Bool = false
    private var detailsScreen: Bool = false
    
    private var investedColor: UIColor {
        let typeColor = self.softCap == 0 ? UIColor(.red) : UIColor(.lightGraphRed)
        return self.softCap >= self.invested ? UIColor(.red) : typeColor
    }
    
    private var softColor: UIColor {
        let typeColor = detailsScreen ? UIColor(.white) : UIColor.black
        return self.softCap > self.invested ? typeColor : UIColor(.red)
    }
    
    // MARK: UI Components
    
    private var investedView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var softCapView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var hardCapView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(.lightGray)
        let path = UIBezierPath(roundedRect:view.bounds,
                                byRoundingCorners:[.topRight, .bottomRight],
                                cornerRadii: CGSize(width: 20, height:  20))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        view.layer.mask = maskLayer
        return view
    }()
    
    private var investedLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(.red)
        label.font = UIFont.roboto.regular.markSize
        label.textAlignment = .right
        return label
    }()
    
    private var softCapLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(.gray)
        label.font = UIFont.roboto.regular.markSize
        label.textAlignment = .right
        label.text = "project.explore.softCap".localized
        return label
    }()
    
    private var hardCapLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(.gray)
        label.font = UIFont.roboto.regular.markSize
        label.textAlignment = .right
        label.text = "project.explore.hardCap".localized
        return label
    }()
    
    private var viewsInOrder: [Content] = []
    
    // MARK: Properties
    
    private var softCap: Double = 0
    private var invested: Double = 0
    
    private var investedPart: Int = 0
    private var softPart: Int = 0
    private var hardPart: Int = 0
    
    // Setup
    
    public func setupFor(_ invested: Double,
                         softCap: Double,
                         hardCap: Double,
                         details: Bool = false,
                         projectsScreen: Bool = false) {
        self.softCap = softCap
        self.invested = invested
        self.projectsScreen = projectsScreen
        self.detailsScreen = details
        if hardCap > 0 {
            self.investedPart = Int((invested/hardCap) * Double(Constants.fullShare))
            self.investedLabel.text = String(self.investedPart) + Constants.percent
            let remainderOfSoftCapPart = Int((softCap/hardCap) * Double(Constants.fullShare)) - self.investedPart
            self.softPart = remainderOfSoftCapPart < 0 ? Int((softCap/hardCap) * Double(Constants.fullShare)) : remainderOfSoftCapPart
            self.investedPart = remainderOfSoftCapPart < 0 ? self.investedPart - self.softPart : self.investedPart
            let remainderOfHardCapPart = (Constants.fullShare - investedPart - softPart)
            self.hardPart = remainderOfHardCapPart < 0 ? 0 : remainderOfHardCapPart
        }
        if details {
            self.hardCapLabel.text = nil
        }
        self.setViewsInOrder()
        self.setup()
    }
    
    public func discardSetup() {
        self.subviews.forEach { $0.removeFromSuperview() }
        self.investedPart = 0
        self.softPart = 0
        self.hardPart = 0
        self.investedLabel.text = nil
        self.viewsInOrder = []
        snp.removeConstraints()
    }
    
    // Overrides
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.redraw()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.redraw()
    }
}

private extension PercentView {
    
    func setColor() {
        self.investedView.backgroundColor = self.investedColor
        self.softCapView.backgroundColor = self.softColor
    }
    
    func setViewsInOrder() {
        self.setColor()
        let content = [
            Content(part: self.softPart, label: self.softCapLabel, view: self.softCapView),
            Content(part: self.investedPart, label: self.investedLabel, view: self.investedView)]
        let filteredContent = content.filter { $0.part != 0 }
        var contentInOrder = filteredContent.sorted {
            if $0.part == $1.part {
                return self.invested < self.softCap
            }
            if self.invested < self.softCap {
            return $0.view == self.investedView
            } else {
                return $0.view == self.softCapView
            }
        }
        if self.hardPart != 0 {
        contentInOrder.append(Content(part: self.hardPart, label: self.hardCapLabel, view: self.hardCapView))
        }
        self.viewsInOrder = contentInOrder

    }
    
    enum Constants {
        static let labelOffset: CGFloat = 8
        static let viewHeight: CGFloat = 8
        static let percent: String = " %"
        static let viewCornerRadius: CGFloat = 4
        static let percentLabelLimit = 20
        static let fullShare: Int = 100
    }
    
    func redraw() {
        let width = self.bounds.width
        if !self.viewsInOrder.isEmpty {
            if viewsInOrder.count == 1 {
                viewsInOrder.first?.view.roundView(radius: Constants.viewCornerRadius, on: [.allCorners])
            } else {
                viewsInOrder.first!.view.roundView(radius: Constants.viewCornerRadius, on: [.bottomLeft, .topLeft])
                viewsInOrder.last!.view.roundView(radius: Constants.viewCornerRadius, on: [.bottomRight, .topRight])
                if viewsInOrder.count > 2 {
                    self.viewsInOrder[1].view.roundView(radius: 0, on: [.allCorners])
                }
            }
        }
        let parts = self.viewsInOrder.map { $0.part }
        let sizes = parts.map { CGFloat($0)/CGFloat(Constants.fullShare) * width }
        for (index, value) in viewsInOrder.enumerated() {
            let view = value.view
            view.snp.updateConstraints {
                $0.width.equalTo(sizes[index])
            }
        }
    }
    
    func setup() {
        self.viewsInOrder.enumerated().forEach { index, viewInOrder in
            self.addSubview(viewInOrder.view)
            self.addSubview(viewInOrder.label)
            viewInOrder.view.snp.removeConstraints()
            viewInOrder.view.snp.makeConstraints { make in
                make.centerY.equalToSuperview()
                make.height.equalTo(Constants.viewHeight)
                make.width.equalTo(0)
            }
            viewInOrder.label.snp.makeConstraints { make in
                if viewInOrder.part < Constants.percentLabelLimit && index == 0 {
                    make.leading.equalTo(viewInOrder.view.snp.leading)
                } else {
                    make.trailing.equalTo(viewInOrder.view.snp.trailing)
                }
                if viewInOrder.label == investedLabel {
                    make.bottom.equalTo(viewInOrder.view.snp.top)
                } else {
                    make.top.equalTo(viewInOrder.view.snp.bottom)
                }
            }
            if !self.viewsInOrder.contains(where: { $0.label == investedLabel }) {
                self.addSubview(investedLabel)
                investedLabel.snp.makeConstraints { make in
                    make.left.equalToSuperview()
                    make.bottom.equalTo(viewInOrder.view.snp.top)
                }
            }
        }

        self.viewsInOrder.first?.view.snp.makeConstraints {
            $0.leading.equalToSuperview()
            if self.viewsInOrder.count == 2, let nextView =
                self.viewsInOrder.last?.view {
                $0.trailing.equalTo(nextView.snp.leading)
            }
            if self.viewsInOrder.count == 3 {
                let nextView = self.viewsInOrder[1].view
                $0.trailing.equalTo(nextView.snp.leading)
            }
        }
        self.viewsInOrder.last?.view.snp.makeConstraints {
            $0.trailing.equalToSuperview()
            if self.viewsInOrder.count == 3 {
                let previousView = self.viewsInOrder[1].view
                $0.leading.equalTo(previousView.snp.trailing)
            }
        }
        
        if self.hardPart == 0 {
            self.addSubview(self.hardCapLabel)
            self.hardCapLabel.snp.makeConstraints {
                $0.trailing.equalToSuperview()
                $0.top.equalTo(self.snp.centerY).offset(Constants.viewHeight/2)
            }
        }
        if self.softPart == 0 && self.softCap != 0 {
            self.addSubview(self.softCapLabel)
            self.softCapLabel.snp.makeConstraints {
                $0.centerX.equalTo(self.investedView.snp.trailing)
                $0.top.equalTo(self.snp.centerY).offset(Constants.viewHeight/2)
            }
        }
        redraw()
    }
    
}
