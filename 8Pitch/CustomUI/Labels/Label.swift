//
//  Label.swift
//  8Pitch
//
//  Created by 8pitch on 18.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class Label: UILabel {
    
    var lineSpacing: CGFloat = 0.0
    var lineHeightMultiple: CGFloat = 0.83
    var kern: CGFloat = 0
    var heightInset: CGFloat = 10
    
    override var text: String? {
        didSet {
            self.setAttributes()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        let superContentSize = super.intrinsicContentSize
        let height = superContentSize.height + self.heightInset
        return CGSize(width: superContentSize.width, height: height)
    }
    
    // MARK: Init
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.customizeFont()
    }
    
    func setup() {
        self.setAttributes()
    }
    
    func height(for text: String?, with width: CGFloat) -> CGFloat {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = self.lineHeightMultiple
        let attributes: [NSAttributedString.Key : Any] = [.kern: self.kern, .paragraphStyle: paragraphStyle, .font: self.font!]
        return (text)?.height(withConstrainedWidth: width, attributes: attributes) ?? 0
    }
    
    private func setAttributes() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = self.textAlignment
        paragraphStyle.lineBreakMode = .byClipping
        let attributes: [NSAttributedString.Key : Any] = [.kern: self.kern, .paragraphStyle: paragraphStyle, .font: self.font!]
        if !self.isOneLineLong(with: attributes) {
            (attributes[.paragraphStyle] as? NSMutableParagraphStyle)?.lineHeightMultiple = self.lineHeightMultiple
            (attributes[.paragraphStyle] as? NSMutableParagraphStyle)?.lineSpacing = self.lineSpacing
        }
        let attributedText = NSMutableAttributedString(string: text ?? "", attributes: attributes)
        self.attributedText = attributedText
    }
    
    private func customizeFont() {
        let words = self.attributedText?.string.components(separatedBy: .whitespacesAndNewlines).filter { !$0.isEmpty } ?? []
        let numberOfWords = words.count
        self.numberOfLines = numberOfWords
        switch numberOfWords {
        case 1:
            self.adjustsFontSizeToFitWidth = true
            self.minimumScaleFactor = 0
        default:
            self.setupFont(for: words)
        }
    }
    
    private func setupFont(for words: [String]) {
        let longestWord = words.sorted { return $0.count > $1.count }.first ?? ""
        let wordWidth = longestWord.width(constrainedBy: self.font.pointSize, with: self.font)
        let labelWidth = self.frame.size.width
        if wordWidth > labelWidth {
            let k = labelWidth/wordWidth
            let newSize = Int(self.font.pointSize * k)
            self.font = self.font.withSize(CGFloat(newSize))
            self.textAlignment = .center
        }
    }
    
    private func isOneLineLong(with attributes: [NSAttributedString.Key : Any]) -> Bool {
        guard let text = text as NSString? else {
            return true
        }
        return text.size(withAttributes: attributes).width <= self.intrinsicContentSize.width
    }
}
