//
//  Subtitles.swift
//  8Pitch
//
//  Created by 8pitch on 1/22/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

// Helper

class TextFieldHeaderLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.regular.selectableBarSize
        self.textColor = UIColor(.halfGrayText)
        self.lineHeightMultiple = 1.28
    }
}

class QuestionLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.regular.selectableBarSize
        self.textColor = UIColor(.answerText)
        self.lineHeightMultiple = 1.28
    }
}

// Subtitle

class StatisticsTitleLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.regular.questionarrieAssetsTextSize
        self.textColor = UIColor(.answerText)
        self.lineHeightMultiple = 0.96
        self.kern = 0.02
    }
}

class StatisticsContentLabel: StatisticsTitleLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.gray)
    }
}

class StatisticsHeaderStatusLabel: StatisticsTitleLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.white)
    }
}

// Small subtitle

class ProjectParameterTitleLabel: Label {
    override func setup() {
        self.font = UIFont.barlow.regular.markSize
        self.textColor = UIColor(.projectParametrGray)
        self.kern = 0.02
        super.setup()
    }
}

