//
//  Headings.swift
//  8Pitch
//
//  Created by 8pitch on 8/5/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

// Heading H1

class CustomLabel: Label {
    
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.semibold.largeTitleSize
        self.textColor = UIColor(.gray)
        self.textAlignment = .center
        self.numberOfLines = 0
        self.kern = -0.5
    }
}

class CustomSubtitleLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.semibold.largeTitleSize
        self.textColor = UIColor(.gray)
        self.kern = -0.5
    }
}

class CustomSubtitleDarkLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.semibold.largeTitleSize
        self.textColor = UIColor(.white)
        self.kern = -0.5
    }
}

// Heading H2

class ProjectsTitleLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.semibold.currencyInputSize
        self.textColor = UIColor(.gray)
        self.kern = -0.5
    }
}

class EuroLabel: ProjectsTitleLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.white)
    }
}

// Heading H3 Semibold

class InfoTitleSemiboldLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.semibold.projectNameSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 0.92
        self.kern = -0.05
    }
}

class InfoPageTitleSemiboldLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.semibold.infoPageTitleSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 0.92
        self.kern = -0.05
    }
}

class InfoTitleSemiboldWhiteLabel: InfoTitleSemiboldLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.white)
    }
}

// Heading H3 Regular

class HintLabel: Label {
    override func setup() {
        self.font = UIFont.roboto.regular.infoTitleSize
        self.textColor = UIColor(.white)
        self.lineHeightMultiple = 0.92
        self.lineSpacing = 8.0
        super.setup()
    }
}

class HintLargeLabel: Label {
    override func setup() {
        self.font = UIFont.barlow.regular.projectNameSize
        self.textColor = UIColor(.white)
        self.lineHeightMultiple = 0.92
        super.setup()
    }
}

class HintDarkLargeLabel: Label {
    override func setup() {
        self.font = UIFont.barlow.regular.projectNameSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 0.92
        super.setup()
    }
}

class HintDarkLabel: HintLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.gray)
    }
}

// Heading H4 Medium

class ImageInfoLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.medium.imageSize
        self.textColor = UIColor(.white)
        self.lineHeightMultiple = 1.07
        self.kern = -0.3
    }
}

class TotalSumLabel: ImageInfoLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.gray)
    }
}

// Heading H4 Regular - ???
