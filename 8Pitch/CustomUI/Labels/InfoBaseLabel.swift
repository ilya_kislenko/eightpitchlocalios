//
//  InfoBaseLabel.swift
//  8Pitch
//
//  Created by 8pitch on 10/8/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation
import TTTAttributedLabel

class InfoBaseLabel: TTTAttributedLabel {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    func setup() {}

}
