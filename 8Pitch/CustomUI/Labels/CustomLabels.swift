//
//  CustomLabels.swift
//  8Pitch
//
//  Created by 8pitch on 8/5/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class UppercasedWhiteLabel: Label {
    override func setup() {
        super.setup()
        self.text = self.text?.uppercased()
        self.font = UIFont.barlow.regular.labelSize
        self.textColor = UIColor(.white)
    }
}

class ErrorLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.medium.textSize
        self.textColor = UIColor(.red)
        self.textAlignment = .center
        self.backgroundColor = UIColor(.white)
    }
}

class DepartmentLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.bold.statisticSize
        self.textColor = UIColor(.gray)
    }
}

class TitleSumLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.regular.selectableBarSize
        self.textColor = UIColor(.gray)
    }
}

class TitleSumLightLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.regular.selectableBarSize
        self.textColor = UIColor(.backgroundGray)
        self.textAlignment = .left
        self.backgroundColor = .clear
        self.numberOfLines = 0
    }
}

class TotalAmountLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.semibold.totalAmoutCellSize
        self.textColor = UIColor(.backGroundCurrencyDark)
    }
}

class InvestmentErrorLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.regular.selectableBarSize
        self.textColor = UIColor(.white80)
    }
}

class InvestmentErrorInfoLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.regular.selectableBarSize
        self.textColor = UIColor(.white)
    }
}

class DisclaimerLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.light.markSize
        self.textColor = UIColor(.white80)
    }
}

class StatusLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.light.markSize
        self.textColor = UIColor(.gray)
    }
}

class ConvertedLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.regular.placeholderSize
        self.textColor = UIColor(.white)
    }
}

class AmountLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.medium.amountSize
        self.textColor = UIColor(.gray)
        self.textAlignment = .center
        self.lineHeightMultiple = 0.83
        self.kern = -0.5
    }
}





