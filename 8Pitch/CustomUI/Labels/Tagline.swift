//
//  Tagline.swift
//  8Pitch
//
//  Created by 8pitch on 8/28/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

// Tagline 15 Semibold

class InvestmentInfoTitleLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIDevice.current.userInterfaceIdiom == .pad ?
            UIFont.barlow.semibold.labelSize : UIFont.barlow.semibold.titleSize
        self.textColor = UIColor(.white)
        self.lineHeightMultiple = 1.11
        self.kern = 0.3
    }
}

class PersonalHeaderLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.semibold.labelSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 1.11
        self.kern = 0.3
    }
}

// Tagline 15 Medium

class RadioLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.medium.labelSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 1.11
        self.kern = 0.3
        self.heightInset = 0
    }
}

// Tagline 13 Semibold

class ResendLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.semibold.titleSize
        self.textColor = UIColor(.red)
        self.lineHeightMultiple = 1
        self.kern = 0.3
    }
}

// Tagline 13 Regular

class CustomCurrencyLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.regular.titleSize
        self.textColor = UIColor(.gray)
        self.kern = 0.3
    }
}

// Tagline 11 Semibold

class VotingHintLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.barlow.semibold.selectableBarSize
        self.textColor = UIColor(.textGray)
        self.lineHeightMultiple = 1.17
    }
}
