//
//  Caption.swift
//  8Pitch
//
//  Created by 8pitch on 1/24/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

// Caption 13 medium

class SumLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.medium.titleSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 1.24
        self.kern = -0.08
    }
}

// Caption 13 regular

class PercentLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.regular.titleSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 1.24
        self.kern = -0.08
    }
}
