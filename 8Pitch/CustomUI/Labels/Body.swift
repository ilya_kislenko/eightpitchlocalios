//
//  Body.swift
//  8Pitch
//
//  Created by 8pitch on 1/22/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

// Body 16 Light

class ImageLinkLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.light.placeholderSize
        self.textColor = UIColor(.textGray)
        self.lineHeightMultiple = 1.28
    }
}

class SwitchCellLabel: ImageLinkLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.gray)
    }
}

// Body 16 Regular

class InfoTitleLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.regular.placeholderSize
        self.textColor = UIColor(.gray)
    }
}

class DescriptionLabel: InfoTitleLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.textGray)
    }
}

// Body 16 Medium

class VotingLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.medium.placeholderSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 1.28
    }
}

// Body 15 roboto Regular

class InfoLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.regular.labelSize
        self.textColor = UIColor(.textGray)
        self.lineHeightMultiple = 1.37
        self.kern = -0.24
    }
}

class InfoWhiteLabel: InfoLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.white)
    }
}

class InfoCurrencyLabel: Label {
    override func setup() {
        self.font = UIFont.roboto.regular.labelSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 1.37
        self.kern = -0.24
        super.setup()
    }
}

class InfoAttributedLabel: InfoBaseLabel {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.regular.labelSize
        self.textColor = UIColor(.answerText)
        self.textAlignment = .left
        self.numberOfLines = 0
        let activeLinkAttributes = NSMutableDictionary(dictionary: self.activeLinkAttributes)
        activeLinkAttributes[NSAttributedString.Key.foregroundColor] = UIColor(.redText)
        activeLinkAttributes[NSAttributedString.Key.font] = UIFont.roboto.regular.labelSize
        self.linkAttributes = activeLinkAttributes as NSDictionary as? [AnyHashable: Any]
        self.activeLinkAttributes = activeLinkAttributes as NSDictionary as? [AnyHashable: Any]
    }
}

// Body 15 roboto medium

class VotingAnswerLabel: Label {
    override func setup() {
        self.font = UIFont.roboto.medium.labelSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 1.28
        self.kern = -0.24
        super.setup()
    }
}

// Body 14 light

class PositionLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.light.questionarrieAssetsTextSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 1.47
    }
}

class FAQLabel: PositionLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.answerText)
    }
}

// Body 14 regular

class CustomAssetTitleLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.regular.questionarrieAssetsTextSize
        self.textColor = UIColor(.gray)
        self.textAlignment = .left
        self.lineHeightMultiple = 1.47
    }
}

class DonutDescriptionLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.regular.questionarrieAssetsTextSize
        self.textColor = UIColor(.answerText)
        self.lineHeightMultiple = 1.47
    }
}

class TextAndImageLabel: InfoBaseLabel {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.regular.questionarrieAssetsTextSize
        self.textColor = UIColor(.answerText)
    }
}

class ProjectParameterLabel: Label {
    override func setup() {
        self.font = UIDevice.current.userInterfaceIdiom == .pad ?
            UIFont.roboto.regular.questionarrieAssetsTextSize :
            UIFont.roboto.regular.titleSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = UIDevice.current.userInterfaceIdiom == .pad ? 1.47 : 1.24
        self.kern = -0.08
        super.setup()
    }
}

// Body 14 roboto medium - ??

// Body 12 roboto light

class TableLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.light.textSize
        self.lineHeightMultiple = 1.11
        self.textColor = UIColor(.textGray)
    }
}

class ProjectDescriptionLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.light.textSize
        self.textColor = UIColor(.textGray)
        self.lineHeightMultiple = 1.11
    }
}

class InvestReasonLabel: ProjectDescriptionLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.white)
    }
}

class DualTextLabel: ProjectDescriptionLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.answerText)
    }
}

// Body 12 roboto regular

class InvestmentInfoLabel: Label {
    override func setup() {
        super.setup()
        self.font = UIFont.roboto.regular.textSize
        self.textColor = UIColor(.white)
        self.lineHeightMultiple = 1.11
    }
    
    func setupStrings(_ elements: [String?]) {
        let strings = elements.map { return Constants.bulletSign + ($0 ?? "")}
        var attributes = [NSAttributedString.Key: Any]()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.headIndent = (Constants.bulletSign as NSString).size(withAttributes: attributes).width
        attributes[.paragraphStyle] = paragraphStyle
        let string = strings.joined(separator: Constants.newStringSign)
        attributedText = NSAttributedString(string: string, attributes: attributes)
    }
    
    private enum Constants {
        static let bulletSign = "•  "
        static let newStringSign = "\n\n"
    }
}

class TimeLabel: Label {
    override func setup() {
        self.font = UIFont.roboto.regular.titleSize
        self.textColor = UIColor(.answerText)
        self.kern = -0.08
        super.setup()
    }
}

class ApproveLabel: InvestmentInfoLabel {
    override func setup() {
        super.setup()
        self.textColor = UIColor(.textGray)
    }
}

class TrusteeLabel: Label {
    override func setup() {
        self.font = UIFont.roboto.medium.textSize
        self.textColor = UIColor(.gray)
        self.lineHeightMultiple = 1.11
        super.setup()
    }
}
