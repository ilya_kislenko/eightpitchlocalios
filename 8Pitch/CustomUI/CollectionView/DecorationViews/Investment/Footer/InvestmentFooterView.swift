//
//  InvestmentFooterView.swift
//  8Pitch
//
//  Created by 8pitch on 8/25/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote

protocol InvestmentFooterViewDelegate: class {
    func confirmButtonTapped()
}

class InvestmentFooterView: UICollectionReusableView {
    
    class var reuseIdentifier : String {
        return "InvestmentFooterView"
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    public var confirmAction: VoidClosure?
    
    public var contentHeight: CGFloat {
        Constants.mainContentHeight + documentsViewHeight.constant
    }
    
    private let remote = Remote()
    private var stackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Constants.spacing
        stack.alignment = .fill
        stack.axis = .vertical
        stack.distribution = .fill
        return stack
    }()
    
    // MARK: Outlets
    
    @IBOutlet weak var awareButton: UIButton!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var dataProtectionView: UITextView!
    @IBOutlet weak var documentsView: UIView!
    @IBOutlet weak var documentsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmButton: StateButton!
    
    // MARK: Actions
    
    @IBAction func agreeButtonTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        self.confirmButton.isEnabled = awareButton.isSelected && agreeButton.isSelected
    }
    
    @IBAction func awareButtonTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        self.confirmButton.isEnabled = awareButton.isSelected && agreeButton.isSelected
    }
    
    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        if awareButton.isSelected && agreeButton.isSelected {
            self.confirmAction?()
        }
    }
    
    // MARK: Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupStaticLabels()
        self.setupDocumentsView()
        self.confirmButton.isEnabled = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.stackView.arrangedSubviews.forEach { self.stackView.removeArrangedSubview($0) }
        self.documentsViewHeight.constant = 0
    }
    
    private func setupStaticLabels() {
        let attributedString = NSMutableAttributedString(string: "investment.preconstructual.protection".localized,
                                                         attributes: [.foregroundColor: UIColor(.gray), .font: UIFont.roboto.regular.textSize])
        let range = Locale.current.languageCode == Constants.languageCodeDE ? NSMakeRange(43, 24) : NSMakeRange(22, 27)
        let url = Locale.current.languageCode == Constants.languageCodeDE ? Constants.privacyPathDe : Constants.privacyPathEng
            attributedString.setAttributes([.link: url], range: range)
        self.dataProtectionView.attributedText = attributedString
        self.dataProtectionView.textContainer.maximumNumberOfLines = 5
        self.dataProtectionView.linkTextAttributes = [
            .foregroundColor: UIColor(.red)
        ]
    }
    
    private func setupDocumentsView() {
        self.documentsView.addSubview(self.stackView)
        self.stackView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func setupWithFiles(_ files: [File?], delegate: UITextViewDelegate) {
        for file in files {
            self.createTextViewFor(file, delegate: delegate)
        }
    }
    
    private func createTextViewFor(_ document: File?, delegate: UITextViewDelegate) {
        guard let document = document, let name = document.originalFileName?.uppercased(),
            let id = document.uniqueFileName else { return }
        let textView = UITextView()
        textView.snp.makeConstraints {
            $0.height.equalTo(Constants.textViewHeight)
        }
        textView.isEditable = false
        textView.isSelectable = true
        textView.delegate = delegate
        textView.textContainer.maximumNumberOfLines = 1
        textView.textContainer.lineBreakMode = .byTruncatingTail
        var attributes: [NSAttributedString.Key : Any] = [:]
        if let link = self.remote.url?.absoluteString {
        attributes = [.link: link + Constants.path + id ]
        }
        textView.attributedText = NSAttributedString(string: name, attributes: attributes)
        textView.linkTextAttributes = [.foregroundColor: UIColor(.red),
                                       .font: UIFont.barlow.semibold.titleSize]
        self.documentsViewHeight.constant += Constants.textViewHeight + Constants.spacing
        self.stackView.addArrangedSubview(textView)
    }
    
    // MARK: Constants
    
    private enum Constants {
        static let languageCodeDE = "de"
        static let path: String = "/project-service/files/"
        static let privacyPathEng = WebURLConstants.privacyPathEng
        static let privacyPathDe = WebURLConstants.privacyPathDe
        static let textViewHeight: CGFloat = 32
        static let spacing: CGFloat = 16
        static let mainContentHeight: CGFloat = 320
    }
    
}

