//
//  InvestmentView.swift
//  8Pitch
//
//  Created by 8pitch on 8/25/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class InvestmentHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var titleLabel: PersonalHeaderLabel!
    private var bottomSeparator: SeparatorView = {
        SeparatorView()
    }()
    private var topSeparator: SeparatorView = {
        SeparatorView()
    }()
    
    class var reuseIdentifier : String {
        return "InvestmentHeaderView"
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    func setTitle(_ text: String) {
        self.titleLabel.text = text
    }
    
    func addSeparator(_ isTopNeeded: Bool) {
        self.topSeparator.removeFromSuperview()
        self.bottomSeparator.removeFromSuperview()
        if isTopNeeded {
            let view = self.topSeparator
            self.addSubview(view)
            view.snp.makeConstraints {
                $0.top.trailing.leading.equalToSuperview()
            }
        }
        let view = self.bottomSeparator
        self.addSubview(view)
        view.snp.makeConstraints {
            $0.bottom.trailing.leading.equalToSuperview()
        }
    }
    
}
