//
//  BankTransferView.swift
//  8Pitch
//
//  Created by 8pitch on 07.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class BankTransferView: UICollectionReusableView {
    
    @IBOutlet weak var checkOutButton: RoundedButton!
    private var totalAmountView: TotalAmountView?
    
    class var reuseIdentifier : String {
        return "BankTransferView"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addTotalAmountView()
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
        
    public var buttonAction: VoidClosure?

    @IBAction func checkOutTapped(_ sender: UIButton) {
        self.buttonAction?()
    }
    
    public func setupWith(values: InvestmentAmountValues?, sum: InvestmentSum?) {
        guard let values = values, let sum = sum else { return }
        self.totalAmountView?.snp.makeConstraints {
            $0.height.equalTo(values.feeRate == 0 ? Constants.emptyFeeHeight : Constants.height)
        }
        self.totalAmountView?.setupLabels(values: values, sum: sum)
    }
    
    public func setButtonTitle(_ title: String) {
        checkOutButton.setTitle(title, for: .normal)
    }
    
    public enum Constants {
        static let topInset: CGFloat = 32
        static let height: CGFloat = 248
        static let emptyFeeHeight: CGFloat = 200
        static let bottomInset: CGFloat = -15
    }
    
    private func addTotalAmountView() {
        guard let view = UINib(nibName: "TotalAmountView", bundle: nil).instantiate(withOwner: self, options: nil).first as? TotalAmountView else {
            return
        }
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.snp.makeConstraints {
            $0.trailing.leading.equalToSuperview()
            $0.top.equalToSuperview().inset(Constants.topInset)
            $0.bottom.equalTo(checkOutButton.snp.top).inset(Constants.bottomInset)
        }
        self.totalAmountView = view
    }
    
}
