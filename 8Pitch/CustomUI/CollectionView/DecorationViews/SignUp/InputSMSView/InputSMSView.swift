//
//  InputSMSView.swift
//  8Pitch
//
//  Created by 8pitch on 21.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class InputSMSView: DecorationViewModel<Model> {
    
    enum ViewType {
        case twoFA
        case standart
        case deletionFirst
        case deletionSecond
        case changePhone
    }
    
    private(set) var footerViewType : PublishSubject<ViewType?> = .init()
    private var footerType : ViewType = .standart
    
    class override var reuseIdentifier : String {
        return "InputSMSView"
    }
    
    @IBOutlet private weak var confirmButton: StateButton!
    @IBOutlet private weak var resendButton: ResendButton!
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? DecoratableLayoutAttributes {
            attributes.confirmationFooterViewType.onNext(self.footerViewType)
            attributes.model?.isValid
                .subscribe(onNext: { [weak self] in
                    switch $0 {
                    case .success(let valid):
                        self?.confirmButton.isEnabled = valid
                    default:
                        self?.confirmButton.isEnabled = false
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let attributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        if let attributes = attributes as? DecoratableLayoutAttributes {
            attributes.confirmationFooterViewType.onNext(self.footerViewType)
            var height = self.confirmButton.intrinsicContentSize.height
            height += self.resendButton.intrinsicContentSize.height
            height += Constants.heightPadding
            attributes.frame.size.height = height
        }
        return attributes
    }
    
    func setup(for type: ViewType) {
        let title: String
        switch type {
        case .standart:
            title = "summarize.button.title"
        case .deletionFirst:
            title = "profile.deletion.button.first"
        case .deletionSecond:
            title = "profile.deletion.button.second"
        case .twoFA:
            title = "summarize.button.title"
            self.resendButton.isHidden = true
        case .changePhone:
            title = "profile.changePhone.button"
        }
        self.confirmButton.setTitle(title.localized.uppercased(), for: .normal)
    }
    
    override func setup() {
        self.footerViewType
            .compactMap { $0 }
            .subscribe(onNext: {
                self.setup(for: $0)
            }).disposed(by: self.disposables)
        self.confirmButton.isEnabled = false
        self.confirmButton.addTarget(self, action: #selector(nextButonPressed), for: .touchUpInside)
        self.resendButton.addTarget(self, action: #selector(resendButtonPressed), for: .touchUpInside)
        self.resendButton.setupForType(.sms)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.confirmButton.isEnabled = false
    }

    
    @objc private func resendButtonPressed() {
        self.resendButton.startCountdown()
        self.additionalButtonAction.onNext(.loading)
        self.additionalButtonAction.onNext(nil)
    }
    
    @objc private func nextButonPressed() {
        self.nextButtonAction.onNext(.loading)
        self.nextButtonAction.onNext(nil)
    }
    
    enum Constants {
        
        static let heightPadding : CGFloat = 150.0
        
    }
    
}
