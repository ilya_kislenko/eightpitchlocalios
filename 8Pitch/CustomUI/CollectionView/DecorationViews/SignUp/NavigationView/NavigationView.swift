//
//  PhoneCollectionReusableView.swift
//  8Pitch
//
//  Created by 8pitch on 20.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

enum NextState {
    
    case loading
    case error
    case next
    
}

class NavigationView: DecorationViewModel<Model> {
    
    @IBOutlet weak var buttonWidth: NSLayoutConstraint!
    
    private var infoLabel: Label? = {
        let label = Label()
        label.textColor = UIColor(.gray)
        label.font = UIFont.roboto.regular.titleSize
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.lineHeightMultiple = 1.45
        label.kern = -0.08
        return label
    }()
    
    var resendButton: ResendButton = {
        ResendButton()
    }()
    
    var forgotPasswordButton: UIButton = {
        let button = UIButton()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        paragraphStyle.alignment = .left
        let attributedText = NSMutableAttributedString(string: "forgotPassword.button".localized,
                                                       attributes: [.paragraphStyle: paragraphStyle,
                                                                    .font: UIFont.barlow.semibold.titleSize,
                                                                    .foregroundColor: UIColor(.red)])
        button.setAttributedTitle(attributedText, for: .normal)
        return button
    }()
    
    var rememberMeButton: UIButton = {
        let button = UIButton()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        paragraphStyle.alignment = .left
        let attributedText = NSMutableAttributedString(string: "login.remeberMeButton.title".localized,
                                                       attributes: [.paragraphStyle: paragraphStyle,
                                                                    .font: UIFont.roboto.regular.textSize,
                                                                    .foregroundColor: UIColor(.textGray)])
        button.setAttributedTitle(attributedText, for: .normal)
        button.setImage(UIImage(.checkOff), for: .normal)
        button.setImage(UIImage(.checkOn), for: .selected)
        button.tintColor = UIColor(.red)
        return button
    }()
    
    enum ViewType {
        case standart
        case info
        case infoEmail
        case resendSMS
        case resendEmail
        case password
    }
    
    class override var reuseIdentifier : String {
        return "NavigationView"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.footerViewType = .init()
    }
    
    private(set) var footerViewType : PublishSubject<ViewType?> = .init()
    private var footerType : ViewType = .standart {
        didSet {
            switch footerType {
            case .standart,
                 .info,
                 .resendSMS,
                 .password,
                 .resendEmail:
                self.infoLabel?.text = "inputSMS.infoLabel".localized
            case .infoEmail:
                self.infoLabel?.text = "inputEmail.infoLabel".localized
            }
        }
    }
    
    @IBOutlet private weak var nextButton: UIButton!
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? DecoratableLayoutAttributes {
            attributes.navigationFooterViewType.onNext(self.footerViewType)
        }
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let attributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        if let attributes = attributes as? DecoratableLayoutAttributes {
            attributes.navigationFooterViewType.onNext(self.footerViewType)
            var height = self.infoLabel?.intrinsicContentSize.height ?? 0
            height += self.nextButton.intrinsicContentSize.height
            height += self.resendButton.intrinsicContentSize.height
            height += self.forgotPasswordButton.intrinsicContentSize.height
            if self.footerType == .info || self.footerType == .infoEmail {
                height += Constants.nextButtonBottomInset
            }
            if self.footerType == .password {
                height += Constants.nextButtonInset
            }
            attributes.frame.size.height = height
        }
        return attributes
    }
    
    override func setup() {
        self.footerViewType
            .compactMap { $0 }
            .subscribe(onNext: {
                self.setup(for: $0)
            }).disposed(by: self.disposables)
        self.buttonWidth.constant = self.nextButton.currentTitle == Constants.next ? Constants.nextWidth : self.buttonWidth.constant
    }
    
    func setup(for type: ViewType) {
        self.footerType = type
        // TODO: connect via IB instead
        self.nextButton.addTarget(self, action: #selector(nextButonPressed), for: .touchUpInside)
        self.forgotPasswordButton.addTarget(self, action: #selector(forgotButtonPressed), for: .touchUpInside)
        self.rememberMeButton.addTarget(self, action: #selector(rememberButtonPressed), for: .touchUpInside)
        self.resendButton.addTarget(self, action: #selector(additionalButtonPressed), for: .touchUpInside)
        
        switch type {
        case .standart://1
            self.nextButton.snp.remakeConstraints {
                $0.top.equalToSuperview().inset(Constants.nextButtonTopInset)
                $0.trailing.equalToSuperview()
            }
            self.infoLabel?.removeFromSuperview()
            return
        case .info://3
            guard let label = self.infoLabel else { return }
            self.addSubview(label)
            label.snp.makeConstraints {
                $0.top.equalTo(self.safeAreaLayoutGuide).inset(Constants.infoLabelInset)
                $0.leading.trailing.equalToSuperview()
                $0.bottom.equalTo(nextButton.snp.top).offset(-Constants.nextButtonBottomInset)
            }
        case .infoEmail://2
            guard let label = self.infoLabel else { return }
            self.addSubview(label)
            label.snp.makeConstraints {
                $0.top.equalTo(self.safeAreaLayoutGuide).inset(Constants.infoLabelInset)
                $0.leading.trailing.equalToSuperview()
                $0.bottom.equalTo(nextButton.snp.top).offset(-Constants.nextButtonBottomInset)
            }
        case .resendSMS:
            self.addSubview(self.resendButton)
            self.resendButton.snp.makeConstraints {
                $0.centerY.equalTo(self.nextButton.snp.centerY)
                $0.leading.equalToSuperview()
                $0.top.equalToSuperview().inset(Constants.resenButtonInset)
            }
            self.resendButton.setupForType(.sms, accConfirmation: true)
            return
        case .resendEmail:
            self.addSubview(self.resendButton)
            self.resendButton.snp.makeConstraints {
                $0.centerY.equalTo(nextButton.snp.centerY)
                $0.leading.equalToSuperview()
            }
            self.resendButton.setupForType(.email)
            return
        case .password:
            self.addSubview(self.forgotPasswordButton)
            self.forgotPasswordButton.snp.makeConstraints {
                $0.leading.equalToSuperview()
                $0.top.equalTo(self.safeAreaLayoutGuide).inset(Constants.nextButtonInset)
                $0.bottom.equalTo(nextButton.snp.top).offset(-Constants.nextButtonInset)
            }
            if !BiometryManager.shared.isQuickLoginSet {
                self.addSubview(self.rememberMeButton)
                self.rememberMeButton.snp.makeConstraints {
                    $0.trailing.equalToSuperview()
                    $0.centerY.equalTo(self.forgotPasswordButton.snp.centerY)
                }
            }
            return
        }
    }
    
    @objc private func additionalButtonPressed() {
        self.additionalButtonAction.onNext(.loading)
        self.additionalButtonAction.onNext(nil)
        self.resendButton.startCountdown()
    }
    
    @objc private func forgotButtonPressed() {
        self.forgotButtonAction.onNext(.loading)
        self.forgotButtonAction.onNext(nil)
    }
    
    @objc private func nextButonPressed() {
        self.nextButtonAction.onNext(.loading)
        self.nextButtonAction.onNext(nil)
    }
    
    @objc private func rememberButtonPressed() {
        self.rememberMeButton.isSelected.toggle()
        self.additionalButtonAction.onNext(self.rememberMeButton.isSelected ? .next : .error)
    }
    
}

private extension NavigationView {
    
    enum Constants {
        static let resenButtonInset: CGFloat = 53
        static let nextButtonInset : CGFloat = 32.5
        static let nextButtonBottomInset : CGFloat = 54.5
        static let infoLabelInset : CGFloat = 24
        static let nextButtonTopInset : CGFloat = 42
        static let infoLabelWidth : CGFloat = 329
        static let next: String = "next"
        static let nextWidth: CGFloat = 78
    }
    
}
