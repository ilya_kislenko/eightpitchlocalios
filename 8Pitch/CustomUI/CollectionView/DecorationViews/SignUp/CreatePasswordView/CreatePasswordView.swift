//
//  CreatePasswordView.swift
//  8Pitch
//
//  Created by 8pitch on 22.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import RxSwift

class CreatePasswordView: DecorationViewModel<Password> {
    
    private var isModelValid: Bool = false
    private var isTermsAgreed: Bool = false
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var createButton: StateButton!
    
    private var metadata : BehaviorSubject<RegistrationEnd?>?
    
    class override var reuseIdentifier : String {
        return "CreatePasswordView"
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? DecoratableLayoutAttributes {
            attributes.model?.isValid
                .subscribe(onNext: { [weak self] in
                    switch $0 {
                    case .success(let valid):
                        self?.isModelValid = valid
                        self?.setButtonState()
                    default:
                        self?.isModelValid = false
                        self?.setButtonState()
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.createButton.isEnabled = false
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let attributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        var height = self.createButton.frame.height
        height += self.textView.intrinsicContentSize.height
        height += Constants.footerPadding
        attributes.frame.size.height = height
        return attributes
    }
    
    override func setup() {
        self.createButton.isEnabled = false
        self.metadata = .init(value: RegistrationEnd())
        self.createButton.addTarget(self, action: #selector(createButtonPressed), for: .touchUpInside)
        
        self.textView.linkTextAttributes = [:]
        self.textView.textContainer.maximumNumberOfLines = 3
        let agreementRange = Locale.current.languageCode == Constants.languageCodeDE ? NSRange(location: 19, length: 32) : NSRange(location: 9, length: 9)
        let policyRange = Locale.current.languageCode == Constants.languageCodeDE ? NSRange(location: 60, length: 23) : NSRange(location: 23, length: 14)
        let attributedString = NSMutableAttributedString(string: "createPassword.acceptPolicy".localized, attributes:[.foregroundColor: UIColor(.textGray), .font: UIFont.roboto.regular.textSize])
        // TODO: don't rely on hardcoded locations. Construct final string out of preconfigured attributed strings
        attributedString.addAttribute(.link, value: Locale.current.languageCode == Constants.languageCodeDE ? Constants.privacyDe : Constants.privacyEng, range: policyRange)
        attributedString.addAttribute(.link, value: Locale.current.languageCode == Constants.languageCodeDE ? WebURLConstants.policiesDE : WebURLConstants.policiesEN, range: agreementRange)

        attributedString.addAttributes([.foregroundColor:UIColor(.red)], range: agreementRange)
        attributedString.addAttributes([.foregroundColor:UIColor(.red)], range: policyRange)
        self.textView.attributedText = attributedString
        self.textView.linkTextAttributes = [.foregroundColor:UIColor(.red)]
    }
    
    @IBAction func agreeToTermsButtonTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        try? self.metadata?.value()?.acceptTermsProviider.onNext(sender.isSelected)
        self.metadataProvider?.onNext(try? self.metadata?.value())
        self.isTermsAgreed = sender.isSelected
        self.setButtonState()
    }
    
    @IBAction func subscribeButtonTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        try? self.metadata?.value()?.subscribeToEmailProvider.onNext(sender.isSelected)
        self.metadataProvider?.onNext(try? self.metadata?.value())
    }
    
    @objc private func createButtonPressed() {
        self.nextButtonAction.onNext(.loading)
        self.nextButtonAction.onNext(nil)
    }
    
    private func setButtonState() {
        self.createButton.isEnabled = self.isModelValid && self.isTermsAgreed
    }
    
    enum Constants {
        static let footerPadding : CGFloat = 150.0
        static let languageCodeDE = "de"
        static let privacyDe = WebURLConstants.privacyPathDe
        static let privacyEng = WebURLConstants.privacyPathEng
    }
    
}
