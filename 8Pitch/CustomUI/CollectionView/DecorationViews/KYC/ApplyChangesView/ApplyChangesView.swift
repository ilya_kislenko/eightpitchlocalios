//
//  ApplyChangesView.swift
//  8Pitch
//
//  Created by 8pitch on 11.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class ApplyChangesView: DecorationViewModel<Model> {
    
    enum ViewType: String {
        case applyChanges = "personal.edit.button.applyChanges"
        case sendRequest = "personal.edit.button.sendRequest"
        case getStarted = "TwoFA.getStartedButton.title"
        case getStartedFromProfile = "TwoFA.getStartedButton.title.fromProfile"
        case installApp = "TwoFA.TOTP.installButton.title"
        case logIn = "login.2fa.button"
        case changePassword = "changePassword.setNew.button.change"
        case GermanTaxInfo = "investment.tax.button.title"
    }
    
    private(set) var footerViewType : PublishSubject<ViewType?> = .init()
    
    @IBOutlet weak var applyChangesButton: StateButton!
    @IBOutlet weak var skipButton: BorderedButton!
    
    class override var reuseIdentifier : String {
        return "ApplyChangesView"
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? DecoratableLayoutAttributes {
            attributes.editFooterViewType.onNext(self.footerViewType)
            attributes.model?.isValid
                .subscribe(onNext: { [weak self] in
                    switch $0 {
                    case .success(let valid):
                        self?.applyChangesButton.isEnabled = valid
                    default:
                        self?.applyChangesButton.isEnabled = false
                    }
                    if (attributes.model is KYC) || (attributes.model is TwoFAProvider) || ((attributes.model is TaxInformation)) {
                        self?.applyChangesButton.isEnabled = true
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.skipButton.isHidden = true
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let attributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        if let attributes = attributes as? DecoratableLayoutAttributes {
            attributes.editFooterViewType.onNext(self.footerViewType)
            var height = self.applyChangesButton.intrinsicContentSize.height
            let skipButtonHeight = self.skipButton.intrinsicContentSize.height
            height += Constants.footerPadding + Constants.skipButtonInsetSum + skipButtonHeight
            attributes.frame.size.height = height
        }
        return attributes
    }
    
    override func setup() {
        self.footerViewType
            .compactMap { $0 }
            .subscribe(onNext: {
                self.skipButton.isHidden = !($0 == .getStarted)
                self.setTitle($0.rawValue.localized)
                self.applyChangesButton.isEnabled = ($0 == .applyChanges || $0 == .sendRequest)
            }).disposed(by: self.disposables)
        self.applyChangesButton.addTarget(self, action: #selector(applyChangesButtonPressed), for: .touchUpInside)
        self.skipButton.addTarget(self, action: #selector(skipButtonPressed), for: .touchUpInside)
    }
    
    @objc private func applyChangesButtonPressed() {
        self.nextButtonAction.onNext(.loading)
        self.nextButtonAction.onNext(nil)
    }
    
    @objc private func skipButtonPressed() {
        self.additionalButtonAction.onNext(.loading)
        self.additionalButtonAction.onNext(nil)
    }
    
    private func setTitle(_ title: String?) {
        self.applyChangesButton.setTitle(title, for: .normal)
    }
    
    enum Constants {
        static let footerPadding : CGFloat = 100.0
        static let skipButtonInsetSum: CGFloat = 32
    }
    
}
