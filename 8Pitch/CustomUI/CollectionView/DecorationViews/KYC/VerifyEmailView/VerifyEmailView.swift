//
//  VerifyEmailView.swift
//  8Pitch
//
//  Created by 8pitch on 29.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class VerifyEmailView: DecorationViewModel<Model> {
    
    enum ViewType {
        case confirm
        case change
    }
    
    private(set) var footerViewType : PublishSubject<ViewType?> = .init()
    private var footerType : ViewType = .confirm
    
    @IBOutlet weak var resendButton: ResendButton!
    @IBOutlet weak var nextButton: StateButton!
    
    class override var reuseIdentifier : String {
        return "VerifyEmailView"
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? DecoratableLayoutAttributes {
            attributes.emailConfirmationFooterViewType.onNext(self.footerViewType)
            attributes.model?.isValid
                .subscribe(onNext: { [weak self] in
                    switch $0 {
                    case .success(let valid):
                        self?.nextButton.isEnabled = valid
                    default:
                        self?.nextButton.isEnabled = false
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.nextButton.isEnabled = false
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let attributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        if let attributes = attributes as? DecoratableLayoutAttributes {
            attributes.emailConfirmationFooterViewType.onNext(self.footerViewType)
            var height = self.resendButton.intrinsicContentSize.height
            height += self.nextButton.intrinsicContentSize.height
            height += Constants.footerPadding
            attributes.frame.size.height = height
        }
        return attributes
    }
    
    override func setup() {
        self.footerViewType
            .compactMap { $0 }
            .subscribe(onNext: {
                self.setup(for: $0)
            }).disposed(by: self.disposables)
        self.resendButton.setupForType(.email)
        self.nextButton.isEnabled = false
        self.nextButton.addTarget(self, action: #selector(nextButonPressed), for: .touchUpInside)
        self.resendButton.addTarget(self, action: #selector(resendButtonPressed), for: .touchUpInside)
    }
    
    private func setup(for type: ViewType) {
        let title = (type == .confirm) ? "verifyEmail.button".localized : "profile.updateEmail.button".localized
        self.nextButton.setTitle(title, for: .normal)
    }
    
    @objc private func resendButtonPressed() {
        self.additionalButtonAction.onNext(.loading)
        self.additionalButtonAction.onNext(nil)
        self.resendButton.startCountdown()
    }
    
    @objc private func nextButonPressed() {
        self.nextButtonAction.onNext(.loading)
        self.nextButtonAction.onNext(nil)
    }
    
    enum Constants {
        static let footerPadding : CGFloat = 95.0
    }
    
}
