//
//  EmailCell.swift
//  8Pitch
//
//  Created by 8pitch on 24.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import RxSwift

class EmailCell : CellModel<Email> {
    
    override var model: BehaviorSubject<Email>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.emailProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
            .subscribe(onNext: { event in
                switch event {
                    case .failure(let error) where error is EmailError:
                        self.textField.setState(.error)
                    default:
                        self.textField.setState(.success)
                }
            }).disposed(by: self.disposables)
        }
    }
    
    class override var reuseIdentifier : String {
        return "EmailCell"
    }
    
    override func setup() {
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .map { [weak self] in $0.prefix(self?.maxNumberOfSymbols ?? 0) }
            .subscribe(onNext: { [weak self] in
                try? self?.model?.value().emailProvider.onNext(String($0))
            }).disposed(by: self.disposables)
    }
    
    @IBOutlet var textField : EmailTextField!
    @IBOutlet var imageView : UIImageView!
    
}
