//
//  CompanyNameCell.swift
//  8Pitch
//
//  Created by 8pitch on 27.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class CompanyNameCell : CellModel<Company> {
 
    override var model : BehaviorSubject<Company>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.companyNameProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { result in
                    switch result {
                        case .failure(_ as CompanyError):
                            self.textField.setState(.error)
                        default:
                            self.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override class var reuseIdentifier: String {
        "CompanyNameCell"
    }
    
    @IBOutlet weak var textField : CompanyNameTextField!
    
    override func setup() {
        self.textField.autocapitalizationType = .sentences
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .map { [weak self] in $0.prefix(self?.maxNumberOfSymbols ?? 0) }
            .subscribe(onNext: { [weak self] in
                try? self?.model?.value().companyNameProvider.onNext(String($0))
            }).disposed(by: self.disposables)
    }
}
