//
//  CreatePasswordCell.swift
//  8Pitch
//
//  Created by 8pitch on 22.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

fileprivate struct Constants {
    static let maxPassLength = 64
}

class CreatePasswordCell: CellModel<Password> {

    override var model: BehaviorSubject<Password>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.passwordProvider
                .bind(to: self.passwordField.rx.text)
                .disposed(by: self.disposables)
            model.confirmationProvider
                .bind(to: self.confirmationField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                        case .failure(let error as PasswordError) where error == .passwordInvalid:
                            self.passwordField.setState(.error)
                        case .failure(let error as PasswordError) where error == .confirmationInvalid:
                            self.confirmationField.setState(.error)
                        default:
                            self.passwordField.setState(.success)
                            self.confirmationField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    class override var reuseIdentifier : String {
        return "CreatePasswordCell"
    }
    
    override func setup() {
        self.passwordField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.passwordField.rx.text.orEmpty)
            .map { $0.prefix(Constants.maxPassLength) }
            .subscribe(onNext: { [weak self] in
                try? self?.model?.value().passwordProvider.onNext(String($0))
            }).disposed(by: self.disposables)
        
        self.confirmationField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.confirmationField.rx.text.orEmpty)
            .subscribe(onNext: {
                if let text = self.confirmationField.text {
                    self.confirmationField.text = String(text.prefix(Constants.maxPassLength))
                }
                try? self.model?.value().confirmationProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    @IBOutlet weak var passwordField: PasswordTextField!
    @IBOutlet weak var confirmationField: PasswordTextField!
    
}
