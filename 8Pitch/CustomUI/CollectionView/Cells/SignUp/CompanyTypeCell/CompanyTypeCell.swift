//
//  CompanyTypeCell.swift
//  8Pitch
//
//  Created by 8pitch on 27.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class CompanyTypeCell : CellModel<Company> {
    
    override var model : BehaviorSubject<Company>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            model.companyTypeProvider
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.textField.text = $0
                })
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { result in
                    switch result {
                    case .failure(_ as CompanyTypeError):
                        self.textField.setState(.error)
                    default:
                        self.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override class var reuseIdentifier: String {
        "CompanyTypeCell"
    }
    
    @IBOutlet weak var textField : PickerTextField!
    
    override func setup() {
        self.textField.tapProvider.subscribe(onNext: {
            guard $0 else { return }
            let valueProvider = try? self.model?.value().valueProviders[\.companyTypeProvider]
            valueProvider??()
        }).disposed(by: self.disposables)
        
    }
    
}

