//
//  CodeTextField.swift
//  8Pitch
//
//  Created by 8pitch on 04.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift

// (c) https://neilsultimatelab.github.io/posts/OneTimeCodeField/
class OTPField: UIControl {
    
    private var text: String? {
        self.labels.reduce(into: "") { $0 += $1.text ?? "" }
    }
    
    var inputProvider : BehaviorSubject<String?> = .init(value: nil)
    
    enum State {
        case empty
        case filled
        case responding
    }
    
    private var digits : Int = 6
    private var spacing : CGFloat = 9.0
    
    var keyboardType : UIKeyboardType = .numberPad
    
    var textContentType : UITextContentType {
        if #available(iOS 12.0, *) {
            return .oneTimeCode
        } else {
            return .password
        }
    }
    
    private lazy var stackView : UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = self.spacing
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        self.addSubview(stackView)
        return stackView
    }()
    
    private var labels : [UILabel] = []
    private var layers : [CAShapeLayer] = []
    
    private var currentIndex : Int = 0
    
    private var yPosition : CGFloat {
        return self.bounds.height - 2
    }
    
    private var individualWidth : CGFloat {
        self.bounds.width - (self.spacing * CGFloat(self.digits - 1)) / CGFloat(self.digits)
    }
    
    private lazy var tapGesture : UITapGestureRecognizer = {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapped))
        return tapGesture
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func label() -> UILabel {
        let label = UILabel()
        label.font = UIFont.roboto.regular.OTPSize
        label.textColor = UIColor(.red)
        label.textAlignment = .center
        label.backgroundColor = UIColor(.lightRed)
        return label
    }
    
    public func clearField() {
        self.labels.enumerated().forEach { (index, label) in
            label.text = nil
            self.updateState(.empty, at: index)
        }
        self.inputProvider.onNext(self.text)
        self.currentIndex = 0
    }
    
    private func setup() {
        self.addGestureRecognizer(self.tapGesture)
        self.setupStackView()
        self.setupLabels()
    }
    
    private func setupLabels() {
        for _ in 0 ..< self.digits {
            let label = self.label()
            self.stackView.addArrangedSubview(label)
            self.labels.append(label)
        }
    }
    
    private func setupStackView() {
        self.stackView.snp.makeConstraints {
            $0.bottom.top.left.right.equalToSuperview()
        }
    }
    
    private func position(for index: Int) -> CGPoint {
        let xPosition = CGFloat(index) * (self.individualWidth + self.spacing)
        return .init(x: xPosition, y: self.yPosition)
    }
    
    override var canBecomeFirstResponder: Bool {
        true
    }
    
    override func becomeFirstResponder() -> Bool {
        return super.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        let previousFieldEmpty = self.labels[currentIndex >= digits ? digits - 1: self.currentIndex].text?.isEmpty ?? true
        self.updateState(previousFieldEmpty ? .empty : .filled, at: self.currentIndex)
        return super.resignFirstResponder()
    }
    
    private func updateState(_ state: State, at index: Int) {
        guard let layer = self.layers[safe: index] else { return }
        switch state {
        case .filled:
            layer.backgroundColor = UIColor(.lightRed).cgColor
            layer.borderWidth = 0
            layer.frame.size.height = 2
            layer.frame.origin.y = self.bounds.height - 2
            layer.cornerRadius = 1
        case .empty:
            layer.backgroundColor = UIColor(.lightGray).cgColor
            layer.borderWidth = 0
            layer.frame.size.height = 2
            layer.frame.origin.y = self.bounds.height - 2
            layer.cornerRadius = 1
        case .responding:
            layer.backgroundColor = UIColor(.red).cgColor
            layer.frame.origin.y = self.bounds.height - 5
            layer.frame.size.height = 5
            layer.cornerRadius = 2.5
        }
    }
    
}

extension OTPField {
    
    @objc func tapped(_ sender: UITapGestureRecognizer) {
        for label in labels.enumerated() {
            if let text = label.element.text, text.isEmpty {
                currentIndex = label.offset
            }
        }
        self.updateState(.responding, at: currentIndex)
        _ = self.becomeFirstResponder()
    }
    
}

extension OTPField : UIKeyInput {
    
    private var textCount : Int {
        self.labels.reduce(into: Int()) { $0 += $1.text?.count ?? 0 }
    }
    
    private func resignFirstResponderIfNeeded() -> Bool {
        if self.currentIndex >= (self.digits - 1), let text = text, text.isEmpty {
            self.inputProvider.onNext(text)
            _ = self.resignFirstResponder()
            return true
        }
        return false
    }
    
    func insertText(_ text: String) {
        if text.count == self.digits {
            for (index, character) in text.enumerated() {
                self.labels[index].text = String(character)
                self.inputProvider.onNext(self.text)
                self.currentIndex = index
                self.updateState(.filled, at: currentIndex)
            }
            if self.resignFirstResponderIfNeeded() {
                return
            }
        } else {
            guard let label = self.labels[safe: self.currentIndex] else { return }
            label.text = text
            self.inputProvider.onNext(self.text)
            self.updateState(.filled, at: self.currentIndex)
            if self.resignFirstResponderIfNeeded() {
                return
            }
            self.currentIndex += 1
            self.updateState(.responding, at: self.currentIndex)
            return
        }
    }
    
    func deleteBackward() {
        if self.labels[safe: self.currentIndex]?.text == nil && self.currentIndex != 0 {
            currentIndex -= 1
            let label = self.labels[safe: self.currentIndex]
            label?.text = nil
            self.inputProvider.onNext(self.text)
            self.updateState(.empty, at: self.currentIndex)
            return
        }
        let label = self.labels[safe: self.currentIndex]
        label?.text = nil
        self.inputProvider.onNext(self.text)
        self.updateState(.empty, at: self.currentIndex)
        if self.currentIndex <= 0 {
            self.currentIndex = 0
            self.updateState(.responding, at: self.currentIndex)
            return
        }
        self.updateState(.responding, at: self.currentIndex)
    }
    
    var hasText: Bool {
        self.textCount > 0
    }
    
}

private extension CGPoint {
    func offsetBy(dx: CGFloat, dy: CGFloat) -> CGPoint {
        return CGPoint(x: x + dx, y: y + dy)
    }
}

extension Array {
    
    subscript(safe index: Array.Index) -> Element? {
        if index < 0 || index >= self.count { return nil }
        return self[index]
    }
    
}

extension OTPField: UITextInput {
    func replace(_ range: UITextRange, withText text: String) {}
    
    var tokenizer: UITextInputTokenizer { UITextInputStringTokenizer() }
    
    var selectedTextRange: UITextRange? {
        get { nil }
        set(selectedTextRange) {}
    }
    
    var markedTextRange: UITextRange? { nil }
    
    var markedTextStyle: [NSAttributedString.Key : Any]? {
        get { nil }
        set(markedTextStyle) {}
    }
    
    var inputDelegate: UITextInputDelegate? {
        get { nil }
        set(inputDelegate) {}
    }
    
    func setMarkedText(_ markedText: String?, selectedRange: NSRange) {}
    
    func unmarkText() {}
    
    var beginningOfDocument: UITextPosition { .init() }
    
    var endOfDocument: UITextPosition { .init() }
    
    func textRange(from fromPosition: UITextPosition, to toPosition: UITextPosition) -> UITextRange? { nil }
    
    func position(from position: UITextPosition, offset: Int) -> UITextPosition? { nil }
    
    func position(from position: UITextPosition, in direction: UITextLayoutDirection, offset: Int) -> UITextPosition? { nil }
    
    func compare(_ position: UITextPosition, to other: UITextPosition) -> ComparisonResult { .orderedSame }
    
    func offset(from: UITextPosition, to toPosition: UITextPosition) -> Int { 0 }
    
    func position(within range: UITextRange, farthestIn direction: UITextLayoutDirection) -> UITextPosition? { nil }
    
    func characterRange(byExtending position: UITextPosition, in direction: UITextLayoutDirection) -> UITextRange? { nil }
    
    func baseWritingDirection(for position: UITextPosition, in direction: UITextStorageDirection) -> NSWritingDirection { .natural }
    
    func setBaseWritingDirection(_ writingDirection: NSWritingDirection, for range: UITextRange) {}
    
    func firstRect(for range: UITextRange) -> CGRect { .zero }
    
    func caretRect(for position: UITextPosition) -> CGRect { .zero }
    
    func selectionRects(for range: UITextRange) -> [UITextSelectionRect] { []}
    
    func closestPosition(to point: CGPoint) -> UITextPosition? { nil }
    
    func closestPosition(to point: CGPoint, within range: UITextRange) -> UITextPosition? { nil }
    
    func characterRange(at point: CGPoint) -> UITextRange? { nil }
    
    func text(in range: UITextRange) -> String? { text }
}

