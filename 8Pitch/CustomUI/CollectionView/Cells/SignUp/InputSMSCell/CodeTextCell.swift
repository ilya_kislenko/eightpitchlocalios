//
//  CodeTextCell.swift
//  8Pitch
//
//  Created by 8pitch on 04.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation
import Input
import RxSwift
import RxCocoa
import UIKit

class CodeTextCell: CellModel<Code> {
    
    class override var reuseIdentifier : String {
        return "CodeTextCell"
    }
    
    override var cellHeight : CGFloat {
        Constants.height
    }
    
    @IBOutlet weak var input : OTPField!
    
    override func setup() {
        self.input.inputProvider
            .subscribe(onNext: {
                try? self.model?.value().codeProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    enum Constants {
        static let height: CGFloat = 72
    }
}
