//
//  LastNameCell.swift
//  8Pitch
//
//  Created by 8pitch on 19.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class LastNameCell : CellModel<NameAndLastName> {
    
    override var model : BehaviorSubject<NameAndLastName>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.lastNameProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
            .subscribe(onNext: { event in
                switch event {
                    case .failure(let error) where error is LastNameError:
                        self.textField.setState(.error)
                    default:
                        self.textField.setState(.success)
                }
            }).disposed(by: self.disposables)
        }
    }
    
    override var cellHeight : CGFloat {
        85
    }
    
    class override var reuseIdentifier : String {
        return "LastNameCell"
    }
    
    @IBOutlet weak var textField : TextField!
    
    override func setup() {
        self.textField.autocapitalizationType = .sentences
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .map { [weak self] in  $0.prefix(self?.maxNumberOfSymbols ?? 0) }
            .subscribe(onNext: { [weak self] in
                try? self?.model?.value().lastNameProvider.onNext(String($0))
            }).disposed(by: self.disposables)
    }
    
}
