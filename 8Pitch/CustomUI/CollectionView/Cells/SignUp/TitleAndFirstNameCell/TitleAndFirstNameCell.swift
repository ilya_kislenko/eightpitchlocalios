//
//  TitleAndFirstNameCell.swift
//  8Pitch
//
//  Created by 8pitch on 19.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Input

class TitleAndFirstNameCell : CellModel<NameAndLastName> {
    
    override var model : BehaviorSubject<NameAndLastName>? {
        didSet {
            
            guard let model = try? self.model?.value() else { return }
            
            model.titleProvider
                .bind(to: self.titleField.rx.text)
                .disposed(by: self.disposables)
            
            model.firstNameProvider
                .bind(to: self.firstNameField.rx.text)
                .disposed(by: self.disposables)
            
            self.titleField.text = model.title?.rawValue.localized
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as FirstNameError):
                        switch error {
                        case .titleInvalid:
                            self.titleField.setState(.error)
                        default:
                            self.firstNameField.setState(.error)
                        }
                    case .success(_),
                         .failure(_):
                        self.firstNameField.setState(.success)
                        self.titleField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    class override var reuseIdentifier : String {
        return "TitleAndFirstNameCell"
    }
    
    @IBOutlet weak var titleField : PickerTextField!
    @IBOutlet weak var firstNameField: TextField!
    
    override func setup() {
        self.firstNameField.autocapitalizationType = .sentences
        self.firstNameField.rx.controlEvent(.editingChanged)
            .withLatestFrom(self.firstNameField.rx.text.orEmpty)
            .map { [weak self] in  $0.prefix(self?.maxNumberOfSymbols ?? 0) }
            .subscribe(onNext: { [weak self] in
                try? self?.model?.value().firstNameProvider.onNext(String($0))
            }).disposed(by: self.disposables)
        
        self.titleField.tapProvider.subscribe(onNext: { [weak self] in
            guard $0 else { return }
            let valueProvider = try? self?.model?.value().valueProviders[\.titleProvider]
            valueProvider??()
        }).disposed(by: self.disposables)
    }
    
}
