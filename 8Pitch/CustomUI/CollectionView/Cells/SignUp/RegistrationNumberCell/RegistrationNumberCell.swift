//
//  RegistrationNumberCell.swift
//  8Pitch
//
//  Created by 8pitch on 12.01.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class RegistrationNumberCell: CellModel<Company> {

    override var model : BehaviorSubject<Company>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.registrationNumberProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { [weak self] result in
                    switch result {
                        case .failure(_ as CompanyNumberError):
                            self?.textField.setState(.error)
                        default:
                            self?.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override class var reuseIdentifier: String {
        "RegistrationNumberCell"
    }
    
    @IBOutlet weak var textField : TextField!
    
    override func setup() {
        self.textField.autocapitalizationType = .sentences
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .map { [weak self] in $0.prefix(self?.maxNumberOfSymbols ?? 0) }
            .subscribe(onNext: { [weak self] in
                try? self?.model?.value().registrationNumberProvider.onNext(String($0))
            }).disposed(by: self.disposables)
    }

}
