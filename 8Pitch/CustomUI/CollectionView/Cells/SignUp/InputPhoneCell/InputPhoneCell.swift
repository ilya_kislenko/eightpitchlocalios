//
//  PhoneCollectionViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 20.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

fileprivate struct Constants {
    static let maxPhoneLength = 64
}

class InputPhoneCell: CellModel<Phone> {
    
    var delegate: CodeListDelegate?
    
    override var model: BehaviorSubject<Phone>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            self.codeField.setupFor(country: try? model.codeProvider.value())
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as PhoneError) where error == .codeInvalid: self.codeField.setState(.error)
                    case .failure(let error as PhoneError) where error == .phoneInvalid: self.textField.setState(.error)
                    case .failure(let error as PhoneError) where error == .phoneEmpty: self.textField.setState(.error)
                    default:
                        self.codeField.setState(.success)
                        self.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    class override var reuseIdentifier : String {
        return "InputPhoneCell"
    }
    
    override func setup() {
        let characterSet = CharacterSet.decimalDigits.inverted
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .map { $0.prefix(Constants.maxPhoneLength) }
            .compactMap { string -> String in
                let retVal = (string as NSString).trimmingCharacters(in: characterSet)
                self.textField.text = retVal
                return retVal
            }
            .subscribe(onNext: { [weak self] in
                try? self?.model?.value().phoneProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    @IBOutlet weak var codeField: PhoneCodeTextField!
    @IBOutlet weak var textField : PhoneTextField!
    
}

extension InputPhoneCell: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.delegate?.openCodeList()
        return false
    }
    
}

