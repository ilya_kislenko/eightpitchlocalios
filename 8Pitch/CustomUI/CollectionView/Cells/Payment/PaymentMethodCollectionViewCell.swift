//
//  PaymentMethodCollectionViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/8/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PaymentMethodCollectionViewCell: UICollectionViewCell {

    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var typeNameLabel: InfoTitleSemiboldLabel!
    @IBOutlet var separatorView: UIView!
    
    var model: PaymentMethodCollectionViewCellModel?

    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }

    class var reuseIdentifier : String {
        return "PaymentMethodCollectionViewCell"
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.separatorView.isHidden = false
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.logoImageView.layer.cornerRadius = 7
        self.logoImageView.clipsToBounds = true
    }

    func setup(model: PaymentMethodCollectionViewCellModel) {
        switch model.type {
        case .onlineUberweisen:
            self.setupOnlineUberweisen()
        case .secupayDirectDebit:
            self.setupSecupayDirectDebit()
        case .classicBankTransfer:
            self.setuClassicBankTransfer()
        case .klarna:
            self.setupKlarna()
        }
    }

    private func setupOnlineUberweisen() {
        self.logoImageView.image = UIImage(Image.onlinePayment)
        self.typeNameLabel.text = "payment.softcap.methods.online".localized
    }

    private func setupSecupayDirectDebit() {
        self.logoImageView.image = UIImage(Image.secupay)
        self.typeNameLabel.text = "payment.softcap.methods.directDebit".localized
    }

    private func setuClassicBankTransfer() {
        self.logoImageView.image = UIImage(Image.manualTransfer)
        self.typeNameLabel.text = "payment.softcap.methods.classicBank".localized
    }

    private func setupKlarna() {
        self.logoImageView.image = UIImage(Image.klarna)
        self.typeNameLabel.text = "payment.softcap.methods.online.klarna".localized
    }
}
