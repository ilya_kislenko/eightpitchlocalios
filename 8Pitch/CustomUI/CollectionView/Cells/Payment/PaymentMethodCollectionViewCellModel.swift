//
//  PaymentMethodCollectionViewCellModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/8/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//
import Input

struct PaymentMethodCollectionViewCellModel {
    var type: PaymentMethodType
    var isHardCapOnly: Bool
}
