//
//  ProjectCollectionViewCell.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let cornerRadius: CGFloat = 8
    static let borderWidth: CGFloat = 0
    static let shadowOffset: CGSize = CGSize(width: 0, height: 6)
    static let shadowRadius: CGFloat = 20
    static let shadowOpacity: Float = 0.2
    static let eurSymbol = "currency.eur.symbol".localized
    static let euroText = "projects.nominal.currency.euro".localized
    static let emptyValue = "0"
}

protocol ProjectCollectionViewCellDelegate: LoadProjectDetailsImageDelegate {
    func showMoreInfo(identifier: String?)
    func didSelectProject(identifier: Int?)
}

class ProjectCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var projectImage: UIImageView!
    @IBOutlet weak var nameLabel: InfoTitleSemiboldLabel!
    @IBOutlet weak var descriptionLabel: ProjectDescriptionLabel!
    @IBOutlet weak var percentView: PercentView!

    @IBOutlet var investmentAmountContainer: UIView!
    @IBOutlet var seccurityContainer: UIView!
    @IBOutlet var nominalContainer: UIView!
    @IBOutlet var amountOfTokensContainer: UIView!

    @IBOutlet var capitalInvestedContainer: UIView!
    @IBOutlet var timeLeftContainer: UIView!
    @IBOutlet var averageinvestmentContainer: UIView!
    @IBOutlet var numberOfInvestors: UIView!
    @IBOutlet var highestInvestment: UIView!
    @IBOutlet var lowestInvestment: UIView!

    @IBOutlet var investmentAmountLabel: ProjectParameterLabel!
    @IBOutlet var lastInvestmentStatus: StatusLabel!
    @IBOutlet var statusView: InvestmentStatusView!
    @IBOutlet var securitylabel: ProjectParameterLabel!
    @IBOutlet var nominalValueLabel: ProjectParameterLabel!
    @IBOutlet var amountOfTokensLabel: ProjectParameterLabel!

    @IBOutlet var capitalInvestedLabel: ProjectParameterLabel!
    @IBOutlet var timeLeftLabel: ProjectParameterLabel!
    @IBOutlet var averageInvestmentLabel: ProjectParameterLabel!
    @IBOutlet var numberOfInvestorsLabel: ProjectParameterLabel!
    @IBOutlet var highestInvestmentLabel: ProjectParameterLabel!
    @IBOutlet var lowestinvestmentLabel: ProjectParameterLabel!

    weak var delegate: ProjectCollectionViewCellDelegate?

    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }

    class var reuseIdentifier : String {
        return "ProjectCollectionViewCell"
    }

    private var initiatorProjectIdentifier: String?

    func setup(isInitiator: Bool, project: ProjectInfo?) {
        self.setupCornersAndShadows()
        if isInitiator,
           let project = project as? InitiatorsProject {
            self.initiatorProjectIdentifier = project.identifier
            self.setupInitiatorProject(project: project)
        } else if let project = project as? InvestorsProject {
            self.setupInvestorProject(project: project)
        }
        if let id = project?.thumbnailId {
            self.delegate?.loadProjectDetailsImage(id, self.projectImage)
        }
    }

    private func setupInitiatorProject(project: InitiatorsProject) {
        self.investorsGroup(isHidden: true)
        self.initiatorsGroup(isHidden: false)
        let currentFundingSum = NSDecimalNumber(decimal:project.graph?.currentFundingSum ?? 0).doubleValue
        let softCap = NSDecimalNumber(decimal:project.graph?.softCap ?? 0).doubleValue
        let hardCap = NSDecimalNumber(decimal:project.graph?.hardCap ?? 0).doubleValue

        self.percentView.setupFor(currentFundingSum, softCap: softCap, hardCap: hardCap)
        self.nameLabel.text = project.companyName
        self.descriptionLabel.text = project.projectDescription

        self.capitalInvestedLabel.text = "\(Constants.eurSymbol)\(project.projectBI?.capitalInvested?.currency ?? Constants.emptyValue)"
        self.timeLeftLabel.text = project.tokenParametersDocument?.dsoProjectFinishDate?.endDateString()
        self.averageInvestmentLabel.text = "\(Constants.eurSymbol)\(project.projectBI?.averageInvestment?.currency ?? Constants.emptyValue)"
        self.numberOfInvestorsLabel.text = "\(project.projectBI?.investors?.totalCount ?? 0)"
        self.highestInvestmentLabel.text = "\(Constants.eurSymbol)\(project.projectBI?.maxInvestment?.currency ?? Constants.emptyValue)"
        self.lowestinvestmentLabel.text = "\(Constants.eurSymbol)\(project.projectBI?.minInvestment?.currency ?? Constants.emptyValue)"
    }

    private func investorsGroup(isHidden hidden: Bool) {
        self.investmentAmountContainer.isHidden = hidden
        self.seccurityContainer.isHidden = hidden
        self.nominalContainer.isHidden = hidden
        self.amountOfTokensContainer.isHidden = hidden
    }

    private func initiatorsGroup(isHidden hidden: Bool) {
        self.capitalInvestedContainer.isHidden = hidden
        self.timeLeftContainer.isHidden = hidden
        self.averageinvestmentContainer.isHidden = hidden
        self.numberOfInvestors.isHidden = hidden
        self.highestInvestment.isHidden = hidden
        self.lowestInvestment.isHidden = hidden
    }

    @IBAction func moreDetailsAction(_ sender: Any) {
        self.delegate?.showMoreInfo(identifier: self.initiatorProjectIdentifier)
    }

    private func setupInvestorProject(project: InvestorsProject) {
        let currentFundingSum = NSDecimalNumber(decimal:project.projectInfo?.graph?.currentFundingSum ?? .zero).doubleValue
        self.percentView.setupFor(currentFundingSum, softCap: Double(project.projectInfo?.graph?.softCap ?? 0), hardCap: Double(project.projectInfo?.graph?.hardCap ?? 0))
        self.nameLabel.text = project.projectInfo?.companyName
        self.descriptionLabel.text = project.projectInfo?.projectInfoDescription
        let investmetAmount = (project.investmentsInfo?.totalAmountOfTokens ?? 0) * (Double(project.projectInfo?.tokenParametersDocument?.nominalValue ?? 0) )
        self.investmentAmountLabel.text = "\(investmetAmount.currency) \(Constants.euroText)"
        if let status = project.investmentsInfo?.lastInvestmentStatus {
            switch status {
            case .confirmed, .pending:
                self.lastInvestmentStatus.text = "payment.status.awaiting".localized
            case .paid:
                self.lastInvestmentStatus.text = "payment.status.processing".localized
            case .received:
                self.lastInvestmentStatus.text = "payment.status.received".localized
            case .booked, .canceldInProgress, .new, .canceled, .refundFailed, .refunded, .refundedInProgress:
                 self.lastInvestmentStatus.text = project.investmentsInfo?.lastInvestmentStatus?.rawValue.replacingOccurrences(of: "_", with: " ").lowercased()
            }
        } else {
            self.lastInvestmentStatus.isHidden = true
            self.statusView.isHidden = true
        }
        self.statusView.setupFor(project.investmentsInfo?.lastInvestmentStatus)
        self.securitylabel.text = project.projectInfo?.financialInformation?.typeOfSecurity
        self.nominalValueLabel.text = "\(project.projectInfo?.tokenParametersDocument?.nominalValue?.currency ?? Constants.emptyValue) \(Constants.euroText)"
        self.amountOfTokensLabel.text = "\(project.investmentsInfo?.totalAmountOfTokens?.currency ?? Constants.emptyValue)"
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.percentView.discardSetup()
        self.projectImage.image = nil
    }
}

private extension ProjectCollectionViewCell {
    func setupCornersAndShadows() {
        self.projectImage.roundView(radius: Constants.cornerRadius, on: .allCorners)
        
        self.contentView.layer.masksToBounds = true
        self.contentView.layer.cornerRadius = Constants.cornerRadius
        self.contentView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        self.contentView.layer.borderWidth = Constants.borderWidth
        self.contentView.layer.borderColor = UIColor.lightGray.cgColor
        self.contentView.layer.masksToBounds = true

        self.layer.cornerRadius = Constants.cornerRadius
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = Constants.shadowOffset
        self.layer.shadowRadius = Constants.shadowRadius
        self.layer.shadowOpacity = Constants.shadowOpacity
        self.layer.masksToBounds = false
    }
}
