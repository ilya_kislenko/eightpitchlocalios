//
//  ProjectDetailsImageLinkPadCollectionViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 12/23/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let cornerRadius = CGFloat(8)
}

class ProjectDetailsImageLinkPadCollectionViewCell: UICollectionViewCell {
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    class var reuseIdentifier : String {
        "ProjectDetailsImageLinkPadCollectionViewCell"
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var rightImage: UIImageView!
    @IBOutlet weak var leftLabel: ImageLinkLabel!
    @IBOutlet weak var rightLabel: ImageLinkLabel!
    @IBOutlet weak var leftLink: UIImageView!
    @IBOutlet weak var rightLink: UIImageView!
    
    func setupWith(_ items: [ImageLinkBlockPayloadItem?]) {
        if let leftID = items.first??.imageUrl {
        self.delegate?.loadProjectDetailsImage(leftID, self.leftImage)
        self.leftLabel.text = items.first??.description
        self.leftLink.isHidden = URL(string: items.first??.link ?? "") == nil
        self.setupImage(self.leftImage)
        }
        if let rightID = items.last??.imageUrl {
        self.delegate?.loadProjectDetailsImage(rightID, self.rightImage)
        self.rightLabel.text = items.last??.description
        self.rightLink.isHidden = URL(string: items.last??.link ?? "") == nil
        self.setupImage(self.rightImage)
        }
    }
        
    private func setupImage(_ imageView: UIImageView) {
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = Constants.cornerRadius
        imageView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.leftLink.isHidden = true
        self.rightLink.isHidden = true
        self.leftImage.image = nil
        self.rightImage.image = nil
        self.leftLabel.text = nil
        self.rightLabel.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.leftLink.isHidden = true
        self.rightLink.isHidden = true
    }
}
