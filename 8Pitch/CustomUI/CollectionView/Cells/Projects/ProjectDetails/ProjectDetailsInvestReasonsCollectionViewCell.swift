//
//  ProjectDetailsInvestReasonsCollectionViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/27/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsInvestReasonsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainTitleLabel: InvestmentInfoTitleLabel!
    @IBOutlet weak var firstArgumentImage: UIImageView!
    @IBOutlet weak var firstArgumentLabel: InvestReasonLabel!
    @IBOutlet weak var secondArgumentImage: UIImageView!
    @IBOutlet weak var secondArgumentLabel: InvestReasonLabel!
    @IBOutlet weak var thirdArgumentImage: UIImageView!
    @IBOutlet weak var thirdArgumentLabel: InvestReasonLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.main.bounds.width)
            make.edges.equalToSuperview()
        }
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ProjectDetailsInvestReasonsCollectionViewCell"
    }
    
    func setup(firstArgument: String?, secondArgument: String?, thirdArgument: String?) {
        firstArgumentLabel.text = firstArgument
        secondArgumentLabel.text = secondArgument
        thirdArgumentLabel.text = thirdArgument
    }

}
