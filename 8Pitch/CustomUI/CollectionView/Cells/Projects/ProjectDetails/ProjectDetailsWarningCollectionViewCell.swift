//
//  ProjectDetailsWarningCollectionViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/27/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ProjectDetailsWarningCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var warningImage: UIImageView!
    @IBOutlet weak var warningMessageLabel: DisclaimerLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.main.bounds.width)
            make.edges.equalToSuperview()
        }
        self.warningMessageLabel.text = "project.details.investment.warning".localized
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ProjectDetailsWarningCollectionViewCell"
    }

}
