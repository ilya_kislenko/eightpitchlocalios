//
//  ProjectDetailsOverViewCollectionViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/27/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectDetailsOverViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var projectTitleLabel: CustomSubtitleDarkLabel!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var progressView: PercentView!
    @IBOutlet weak var investButton: RoundedButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var rightButtonInset: NSLayoutConstraint!
    @IBOutlet weak var leftButtonInset: NSLayoutConstraint!
    
    public var buttonAction: VoidClosure?
    public var playButtonAction: VoidClosure?
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.snp.makeConstraints { make in
            make.width.equalTo(UIScreen.main.bounds.width)
            make.edges.equalToSuperview()
        }
        let inset = UIDevice.current.userInterfaceIdiom == .pad ?
            UIScreen.main.bounds.width * Constants.insetShare : self.rightButtonInset.constant
        self.rightButtonInset.constant = inset
        self.leftButtonInset.constant = inset
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ProjectDetailsOverViewCollectionViewCell"
    }
    
    func setupFor(project: ExpandedProject?, for lvl: AccountLevel?) {
        guard let project = project, let lvl = lvl else { return }
        self.progressView.setupFor(project.graph?.currentFundingSum ?? 0, softCap: project.graph?.softCap ?? 0, hardCap: project.graph?.hardCap ?? 0, details: true)
        self.projectTitleLabel.text = project.companyName
        self.investButton.isHidden = checkInvestButton(project: project, lvl: lvl)
        if let backgroundImageId = project.backgroundImageId {
            self.delegate?.loadProjectDetailsImage(backgroundImageId, self.backgroundImageView)
        }
    }
    
    func checkInvestButton(project: ExpandedProject, lvl: AccountLevel) -> Bool {
        guard let statusString = project.projectStatus, let status = ProjectStatus(rawValue: statusString), let pageStatusString = project.projectPage?.projectPageStatus, let pageStatus = ProjectPageStatus(rawValue: pageStatusString) else { return true }
        switch pageStatus {
        case .published:
            switch status {
            case .active, .comingSoon:
                switch lvl {
                case .second:
                    return false
                default:
                    return true
                }
            default:
                return true
            }
        default:
            return true
        }
    }
    
    @IBAction func playButtonClicked(_ sender: Any) {
        playButtonAction?()
    }
    
    @IBAction func investClicked(_ sender: Any) {
        buttonAction?()
    }
    
    enum Constants {
        static let insetShare: CGFloat = 0.15
    }
    
}
