//
//  ProjectDetailsImageLinkCollectionViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 10/30/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let cornerRadius = CGFloat(8)
    static let horizontalOffsets = CGFloat(32)
    static let maximumImage = CGFloat(220)
}

class ProjectDetailsImageLinkCollectionViewCell: UICollectionViewCell {
    
    static func height(with textHeight: CGFloat) -> CGFloat {
        textHeight + Constants.horizontalOffsets + Constants.maximumImage
    }
    
    @IBOutlet weak var width: NSLayoutConstraint!
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    @IBOutlet weak var link: UIImageView!
    @IBOutlet weak var imageView: ScaledHeightImageView!
    @IBOutlet weak var textLabel: FAQLabel!
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ProjectDetailsImageLinkCollectionViewCell"
    }
    
    func setupWith(_ item: ImageLinkBlockPayloadItem?, index: Int, allItemsCount: Int) {
        guard let id = item?.imageUrl else { return }
        self.imageView.setSize(id: id, offset: Constants.horizontalOffsets)
        self.delegate?.loadProjectDetailsImage(id, self.imageView)
        self.textLabel.text = item?.description
        self.link.isHidden = URL(string: item?.link ?? "") == nil
        self.imageView.clipsToBounds = true
        self.imageView.layer.masksToBounds = true
        self.imageView.layer.cornerRadius = Constants.cornerRadius
        self.imageView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.link.isHidden = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.link.isHidden = true
        self.imageView.snp.makeConstraints {
            $0.height.lessThanOrEqualTo(Constants.maximumImage)
        }
    }

}
