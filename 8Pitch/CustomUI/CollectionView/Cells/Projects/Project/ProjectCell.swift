//
//  ProjectCell.swift
//  8Pitch
//
//  Created by 8pitch on 20.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ProjectCell: UICollectionViewCell {
    
    weak var delegate: LoadProjectDetailsImageDelegate?
    
    // MARK: UI Components
    
    @IBOutlet weak var projectImage: UIImageView!
    @IBOutlet weak var nameLabel: InfoTitleSemiboldLabel!
    @IBOutlet weak var descriptionLabel: ProjectDescriptionLabel!
    @IBOutlet weak var percentView: PercentView!
    @IBOutlet weak var securityLabel: ProjectParameterLabel!
    @IBOutlet weak var nominalLabel: ProjectParameterLabel!
    @IBOutlet weak var timeLabel: ProjectParameterLabel!
    @IBOutlet weak var seriesLabel: ProjectParameterLabel!
    
    // MARK: Properties
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        "ProjectCell"
    }
    
    // MARK: Methods
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setup()
    }
    
    func setupFor(_ project: Project) {
        nameLabel.text = project.companyName ?? ""
        descriptionLabel.text = project.description ?? ""
        securityLabel.text = project.financialInformation?.typeOfSecurity ?? ""
        nominalLabel.text = (project.tokenParametersDocument?.minimumInvestmentAmount?.currency ?? "") + "projects.nominal.currency.euro".localized
        seriesLabel.text = (project.tokenParametersDocument?.hardCap?.currency ?? "") + "projects.nominal.currency.euro".localized
        timeLabel.text = project.tokenParametersDocument?.dsoProjectFinishDate?.endDateString()
        percentView.setupFor(project.graph?.currentFundingSum ?? 0, softCap: project.graph?.softCap ?? 0, hardCap: project.graph?.hardCap ?? 0, projectsScreen: true)
        if let id = project.thumbnail??.file?.id {
        self.delegate?.loadProjectDetailsImage(id, self.projectImage)
        }
        self.contentView.layoutIfNeeded()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        descriptionLabel.text = nil
        securityLabel.text = nil
        nominalLabel.text = nil
        seriesLabel.text = nil
        timeLabel.text = nil
        projectImage.image = nil
        percentView.discardSetup()
    }
    
}

// MARK: Private

private extension ProjectCell {

    func setup() {
        self.contentView.roundView(radius: Constants.cornerRadius, on: [.bottomLeft, .bottomRight])
        self.projectImage.roundView(radius: Constants.cornerRadius, on: .allCorners)
        
        self.contentView.layer.cornerRadius = Constants.cornerRadius
        self.contentView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        self.contentView.layer.borderWidth = Constants.borderWidth
        self.contentView.layer.borderColor = UIColor.lightGray.cgColor
        self.contentView.layer.masksToBounds = true

        self.layer.cornerRadius = Constants.cornerRadius
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = Constants.shadowOffset
        self.layer.shadowRadius = Constants.shadowRadius
        self.layer.shadowOpacity = Constants.shadowOpacity
        self.layer.masksToBounds = false
    }
    
    enum Constants {
        static let cornerRadius: CGFloat = 8
        static let borderWidth: CGFloat = 0
        static let shadowOffset: CGSize = CGSize(width: 0, height: 6)
        static let shadowRadius: CGFloat = 20
        static let shadowOpacity: Float = 0.2
    }
    
}
