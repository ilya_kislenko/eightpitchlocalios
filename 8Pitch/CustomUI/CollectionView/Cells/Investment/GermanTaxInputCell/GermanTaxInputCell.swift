//
//  GermanTaxInputCell.swift
//  8Pitch
//
//  Created by 8pitch on 13.01.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class GermanTaxInputCell: CellModel<TaxInformation> {
    
    enum GermanTaxInputCellType: String {
        case id = "investment.tax.key.number"
        case office = "investment.tax.key.office"
    }
    
    private var type: GermanTaxInputCellType?
    
    @IBOutlet weak var textField: TextField!
    
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    class override var reuseIdentifier : String {
        return "GermanTaxInputCell"
    }
    
    func setup(for type: GermanTaxInputCellType) {
        self.textField.placeholder = type.rawValue.localized
        self.type = type
        
        guard let model = try? self.model?.value() else { return }
        let provider: BehaviorSubject<String?>
        
        switch type {
        case .id:
            provider = model.taxIDProvider
            self.textField.rx
                .controlEvent(.editingChanged)
                .withLatestFrom(self.textField.rx.text.orEmpty)
                .subscribe(onNext: {
                    model.taxIDProvider.onNext($0)
                }).disposed(by: self.disposables)
        case .office:
            provider = model.taxOfficeProvider
            self.textField.rx
                .controlEvent(.editingChanged)
                .withLatestFrom(self.textField.rx.text.orEmpty)
                .subscribe(onNext: {
                    model.taxOfficeProvider.onNext($0)
                }).disposed(by: self.disposables)
        }
        
        provider
            .bind(to: self.textField.rx.text)
            .disposed(by: self.disposables)
    }
    
    public func validate() {
        guard let model = try? self.model?.value() else { return }
        
        model.isValid
            .subscribe(onNext: { [weak self] event in
                switch event {
                case .failure(let error):
                    switch error {
                    case is IDTaxError:
                        self?.textField.setState(self?.type == .id ? .error : .success)
                    case is OfficeTaxError:
                        self?.textField.setState(self?.type == .office ? .error : .success)
                    default:
                        self?.textField.setState(.success)
                    }
                default:
                    self?.textField.setState(.success)
                }
            }).disposed(by: self.disposables)
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 80
    }
}
