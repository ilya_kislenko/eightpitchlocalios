//
//  GermanTaxSwitcherCell.swift
//  8Pitch
//
//  Created by 8pitch on 13.01.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class GermanTaxSwitcherCell: CellModel<TaxInformation> {
    
    enum GermanTaxSwitcherCellType: String {
        case tax = "investment.tax.key.resident"
        case churchTax = "investment.tax.key.churchTax"
    }
    
    private var type: GermanTaxSwitcherCellType?
    @IBOutlet weak var infoLabel: CustomAssetTitleLabel!
    private var segmentedControl: CustomSegmentedControl!
    public var switchHandler: VoidClosure?
        
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    class override var reuseIdentifier : String {
        return "GermanTaxSwitcherCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.segmentedControl = CustomSegmentedControl.nib(owner: self)
        self.contentView.addSubview(self.segmentedControl)
        self.segmentedControl.snp.makeConstraints {
            $0.trailing.centerY.equalToSuperview()
            $0.leading.equalTo(self.infoLabel.snp.trailing).offset(Constants.minimumLabelInset)
        }
    }
    
    func setup(for type: GermanTaxSwitcherCellType) {
        self.infoLabel.text = type.rawValue.localized
        
        guard let model = try? self.model?.value() else { return }
        
        switch type {
        case .tax:
            self.segmentedControl.setupUIForState((try? model.taxResidentProvider.value()) ?? true)
            self.segmentedControl.delegateAction = { [weak self] bool in
                model.taxResidentProvider.onNext(bool)
                self?.switchHandler?()
            }
        case .churchTax:
            self.segmentedControl.setupUIForState((try? model.churchTaxLabilityProvider.value()) ?? false)
            self.segmentedControl.delegateAction = { [weak self] bool in
                model.churchTaxLabilityProvider.onNext(bool)
                self?.switchHandler?()
            }
        }
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 80
        static let minimumLabelInset = CGFloat(43)
    }
}
