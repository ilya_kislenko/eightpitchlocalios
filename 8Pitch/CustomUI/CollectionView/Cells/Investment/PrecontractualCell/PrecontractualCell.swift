//
//  PreconstructualCell.swift
//  8Pitch
//
//  Created by 8pitch on 8/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PrecontractualCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: InfoTitleLabel!
    @IBOutlet weak var label: InfoLabel!
    @IBOutlet weak var cellWidth: NSLayoutConstraint!
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.containerView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        cellWidth.constant = screenWidth
        self.titleLabel.numberOfLines = 1
    }
    
    class var reuseIdentifier : String {
        return "PrecontractualCell"
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    func setupFor(info: PrecontractualInfo) {
        titleLabel.text = info.title
        label.text = info.text
        titleLabel.sizeToFit()
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    func clearSeparator() {
        self.separatorView.backgroundColor = .clear
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.separatorView.backgroundColor = UIColor(.separatorGray)
    }

}

public struct PrecontractualInfo {
    public let title: String
    public let text: String?
}
