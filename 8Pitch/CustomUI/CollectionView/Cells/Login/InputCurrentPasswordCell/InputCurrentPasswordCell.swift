//
//  InputPasswordCell.swift
//  8Pitch
//
//  Created by 8pitch on 24.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class InputCurrentPasswordCell: CellModel<Login> {
    
    override var model: BehaviorSubject<Login>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            setupModel(model: model)
        }
    }
    
    class override var reuseIdentifier : String {
        return "InputCurrentPasswordCell"
    }
    
    override func setup() {
        super.setup()
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .subscribe(onNext: { [weak self] in
                try? self?.model?.value().passwordProvider.value()?.passwordProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    func setupModel(model: Login) {
        try? model.passwordProvider
            .value()?
            .passwordProvider
            .bind(to: self.textField.rx.text)
            .disposed(by: self.disposables)
        
        model.isValid
        .subscribe(onNext: { [weak self] event in
            switch event {
                case .failure(let error as PasswordError) where error == .passwordEmpty:
                    self?.textField.setState(.error)
                default:
                    self?.textField.setState(.success)
            }
        }).disposed(by: self.disposables)
    }
    
    @IBOutlet weak var textField : PasswordTextField!

}

extension InputCurrentPasswordCell {
    enum Constants {
        static let passwordMaxLenght = 64
    }
}

extension InputCurrentPasswordCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= Constants.passwordMaxLenght
    }
    
}
