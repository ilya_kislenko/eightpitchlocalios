//
//  ChangePasswordCell.swift
//  8Pitch
//
//  Created by 8pitch on 10/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class ChangePasswordCell: CellModel <Password> {
    override var model: BehaviorSubject<Password>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.passwordProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as PasswordError) where error == .passwordEmpty:
                        self.textField.setState(.error)
                    default:
                        self.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    class override var reuseIdentifier : String {
        return "ChangePasswordCell"
    }
    
    override func setup() {
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().passwordProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    @IBOutlet weak var textField : PasswordTextField!
}

extension ChangePasswordCell {
    enum Constants {
        static let passwordMaxLenght = 64
    }
}

extension ChangePasswordCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= Constants.passwordMaxLenght
    }
}
