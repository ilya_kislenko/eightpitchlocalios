//
//  ConfirmPasswordCell.swift
//  8Pitch
//
//  Created by 8pitch on 10/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class ConfirmPasswordCell: CellModel<Password> {
    override var model: BehaviorSubject<Password>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.confirmationProvider
                .bind(to: self.confirmationField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                        case .failure(let error as ConfirmPasswordError) where error == .confirmationInvalid:
                            self.confirmationField.setState(.error)
                        default:
                            self.confirmationField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    class override var reuseIdentifier : String {
        return "ConfirmPasswordCell"
    }
    
    override func setup() {
        self.confirmationField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.confirmationField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().confirmationProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    @IBOutlet weak var confirmationField: PasswordTextField!

}
