//
//  LoginInputPhoneCell.swift
//  8Pitch
//
//  Created by 8pitch on 29.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation
import UIKit
import Input
import RxSwift
import RxCocoa

protocol CodeListDelegate {
    func openCodeList()
}

// TODO: this class is duplicate of InputPhoneCell. There should be a way to reuse considering generics
class LoginInputPhoneCell : CellModel<Login> {
    
    var delegate: CodeListDelegate?
    
    override var model: BehaviorSubject<Login>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            self.codeField.setupFor(country: try? model.phoneProvider.value()?.codeProvider.value())
            
            try? model.phoneProvider.value()?.phoneProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as PhoneError) where error == .codeInvalid: self.codeField.setState(.error)
                    case .failure(let error as PhoneError) where error == .phoneInvalid: self.textField.setState(.error)
                    case .failure(let error as PhoneError) where error == .phoneEmpty: self.textField.setState(.error)
                    default:
                        self.codeField.setState(.success)
                        self.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
            
        }
    }
    
    class override var reuseIdentifier : String {
        return "LoginInputPhoneCell"
    }
    
    override func setup() {
        let characterSet = CharacterSet.decimalDigits.inverted
        
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .compactMap { string -> String in
                let retVal = (string as NSString).trimmingCharacters(in: characterSet)
                self.textField.text = retVal
                return retVal
            }
            .subscribe(onNext: { [weak self] in
                //            if let code = self?.codeField.digits {
                //                try? self?.model?.value().phoneProvider.value()?.codeProvider.onNext(code)
                //            }
                try? self?.model?.value().phoneProvider.value()?.phoneProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    @IBOutlet weak var codeField: PhoneCodeTextField!
    @IBOutlet weak var textField : PhoneTextField!
    
}

extension LoginInputPhoneCell {
    enum Constants {
        static let phoneMaxLenght = 64
    }
}

extension LoginInputPhoneCell: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == codeField {
            self.delegate?.openCodeList()
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= Constants.phoneMaxLenght
    }
    
}
