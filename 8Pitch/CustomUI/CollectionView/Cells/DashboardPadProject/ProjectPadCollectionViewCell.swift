//
//  ProjectPadCollectionViewCell.swift
//  8Pitch
//
//  Created by 8pitch on 12/21/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ProjectPadCollectionViewCell: ProjectCollectionViewCell {
    override class var reuseIdentifier : String {
        return "ProjectPadCollectionViewCell"
    }
}
