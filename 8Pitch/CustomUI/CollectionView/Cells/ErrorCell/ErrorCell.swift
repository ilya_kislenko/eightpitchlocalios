//
//  ErrorCell.swift
//  8Pitch
//
//  Created by 8pitch on 30.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class ErrorCell : CellModel<Model> {
    
    var textAlignment: NSTextAlignment = .center {
        didSet {
            self.textLabel.textAlignment = self.textAlignment
        }
    }
    
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let size = self.textLabel.sizeThatFits(self.textLabel.frame.size)
        layoutAttributes.frame.size.height = size.height + self.cellHeight
        return layoutAttributes
    }
    
    class override var reuseIdentifier : String {
        return "ErrorCell"
    }
    
    @IBOutlet weak var textLabel: ErrorLabel!
    
    enum Constants {
        static let cellHeight: CGFloat = 15
    }
    
}
