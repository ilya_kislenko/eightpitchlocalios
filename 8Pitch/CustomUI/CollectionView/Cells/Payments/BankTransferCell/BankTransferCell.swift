//
//  BankTransferCell.swift
//  8Pitch
//
//  Created by 8pitch on 07.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class BankTransferCell: UICollectionViewCell, UITextFieldDelegate {

    @IBOutlet weak var label: TextFieldHeaderLabel!
    @IBOutlet weak var textField: PaymentsTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
    }
    
    class var reuseIdentifier : String {
        return "BankTransferCell"
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    public func setText(forTitle labelText: String, forValue fieldText: String?) {
        self.label.text = labelText
        self.textField.text = fieldText
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        false
    }
    
}
