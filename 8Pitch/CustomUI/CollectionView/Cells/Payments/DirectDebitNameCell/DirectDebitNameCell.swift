//
//  DirectDebitCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/8/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class DirectDebitNameCell: UICollectionViewCell {
    
    public var model: BankAccount? {
        didSet {
            let name = self.model?.owner
            self.textField.text = name?.uppercased()
        }
    }
    
    class var reuseIdentifier : String {
        return "DirectDebitNameCell"
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.addTarget(self, action: #selector(validateModel), for: .editingChanged)
        self.textField.addTarget(self, action: #selector(endEditing(_:)), for: .editingDidEndOnExit)
    }

    @IBOutlet weak var textField: TextField!
    @IBOutlet weak var errorLabel: ErrorLabel!
    
    @objc private func validateModel() {
        let text = self.textField.text
        self.model?.owner = text
        let isValid = self.model?.validateName() ?? false
        self.errorLabel.text = isValid ? nil : "payment.direct.error.owner".localized
    }

}
