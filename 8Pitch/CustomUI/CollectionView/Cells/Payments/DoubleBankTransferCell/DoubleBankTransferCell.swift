//
//  DoubleBankTransferCell.swift
//  8Pitch
//
//  Created by 8pitch on 07.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class DoubleBankTransferCell: UICollectionViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var bicLabel: TextFieldHeaderLabel!
    @IBOutlet weak var bicTextField: PaymentsTextField!
    @IBOutlet weak var numberLabel: TextFieldHeaderLabel!
    @IBOutlet weak var numberTextField: PaymentsTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bicTextField.delegate = self
        self.numberTextField.delegate = self
    }
    
    class var reuseIdentifier : String {
        return "DoubleBankTransferCell"
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    public func setText(forTitles labelsText: [String], forValues fieldsText: [String?]) {
        self.bicLabel.text = labelsText.first
        self.bicTextField.text = fieldsText.first ?? ""
        self.numberLabel.text = labelsText.last
        self.numberTextField.text = fieldsText.last ?? ""
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        false
    }
    
}
