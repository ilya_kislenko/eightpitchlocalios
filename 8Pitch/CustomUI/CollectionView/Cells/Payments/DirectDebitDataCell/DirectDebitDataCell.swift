//
//  DirectDebitDataCell.swift
//  8Pitch
//
//  Created by 8pitch on 9/8/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class DirectDebitDataCell: UICollectionViewCell {
    
    enum CellType: Int {
        case iban
        case bic
    }
    
    private var type: CellType = .iban
    private let textFor: [String] = ["payment.direct.iban".localized, "payment.direct.bic".localized]
    
    public var model: BankAccount?
    
    class var reuseIdentifier : String {
        return "DirectDebitDataCell"
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.addTarget(self, action: #selector(validateModel), for: .editingChanged)
        self.textField.addTarget(self, action: #selector(endEditing(_:)), for: .editingDidEndOnExit)
    }
    
    @IBOutlet weak var titleLabel: TextFieldHeaderLabel!
    @IBOutlet weak var textField: DirectPaymentTextField!
    @IBOutlet weak var errorLabel: ErrorLabel!
    
    
    public func setType(_ type: CellType) {
        self.type = type
        self.titleLabel.text = self.textFor[type.rawValue]
    }
    
    @objc private func validateModel() {
        guard let text = self.textField.text else { return }
        let isValid: Bool
        switch type {
        case .bic:
            self.model?.bic = text
            isValid = self.model?.validateBIC() ?? false
        case .iban:
            self.model?.iban = text
            isValid = self.model?.validateIBAN() ?? false
        }
        let error = type == .bic ? "payment.direct.error.bic".localized : "payment.direct.error.iban".localized
        self.errorLabel.text = isValid ? nil : error
    }
    
}
