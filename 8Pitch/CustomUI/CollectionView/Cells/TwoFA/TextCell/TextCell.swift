//
//  TextCell.swift
//  8Pitch
//
//  Created by 8pitch on 10/1/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class TextCell: CellModel<Model> {
    
    enum CellType: String {
        case addSecurity = "TwoFA.add.info"
        case installApp = "TwoFA.TOTP.info"
        case getCode = "TwoFA.getCode.info"
    }
    
    class override var reuseIdentifier : String {
        return "TextCell"
    }
    
    override var cellHeight : CGFloat {
        let labelHeight = self.textLabel.frame.height
        return self.textLabel.frame.origin.y + labelHeight + Constants.inset
    }

    @IBOutlet weak var textLabel: InfoCurrencyLabel!
    
    func setupForType(_ type: CellType) {
        self.textLabel.text = type.rawValue.localized
    }
    
    private enum Constants {
        static let inset: CGFloat = 20
    }
}
