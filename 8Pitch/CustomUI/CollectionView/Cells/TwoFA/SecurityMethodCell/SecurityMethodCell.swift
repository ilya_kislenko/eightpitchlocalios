//
//  SecurityMethodCell.swift
//  8Pitch
//
//  Created by 8pitch on 10/1/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class SecurityMethodCell: CellModel<TwoFAProvider> {
    
    class override var reuseIdentifier : String {
        return "SecurityMethodCell"
    }
    
    enum CellType {
        case sms
        case totp
    }
    
    struct CellData {
        let image: UIImage
        let title: String
        let info: String
    }
    
    enum Constants {
        static let sms = CellData(image: UIImage(.profileMail), title: "TwoFA.choose.method.sms".localized, info: "TwoFA.choose.info.sms".localized)
        static let totp = CellData(image: UIImage(.newsletters), title: "TwoFA.choose.method.TwoFA".localized, info: "TwoFA.choose.info.TwoFA".localized)
        static let methods: [CellType : CellData] = [.sms: sms, .totp: totp]
        static let cellHeight: CGFloat = 101
    }
    
    var switchHandler: VoidClosure?
    
    @IBOutlet weak var methodSwitch: Switch!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var titleLabel: InfoTitleLabel!
    @IBOutlet weak var infoLabel: ProjectDescriptionLabel!
    
    @IBAction func switchToggled(_ sender: UISwitch) {
        switchHandler?()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.methodSwitch.setOn(false, animated: false)
    }
    
    func setupForType(_ type: CellType) {
        guard let model = try? self.model?.value() else { return }
        let data = Constants.methods[type]
        self.cellImage.image = data?.image
        self.titleLabel.text = data?.title
        self.titleLabel.sizeToFit()
        self.infoLabel.text = data?.info
        self.methodSwitch.isOn = (type == .sms) ? model.smsAuth : model.totpAuth
    }
    
    override var cellHeight: CGFloat {
        Constants.cellHeight
    }
    
}
