//
//  CountryCell.swift
//  8Pitch
//
//  Created by 8pitch on 01.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class CountryCell: CellModel<Locality> {
    
    // MARK: UI Components
    
    @IBOutlet weak var textField: PickerTextField!
    
    // MARK: Overrides
    
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    class override var reuseIdentifier : String {
        return "CountryCell"
    }
    
    override var model : BehaviorSubject<Locality>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.countryProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as CountryError) where error == .countryEmpty:
                        self.textField.setState(.error)
                    default:
                        self.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override func setup() {
        self.textField.rx
            .controlEvent(.editingDidEnd)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().countryProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 64
    }
    
}
