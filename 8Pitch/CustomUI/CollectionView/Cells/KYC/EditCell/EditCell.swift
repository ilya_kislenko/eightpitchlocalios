//
//  EditCell.swift
//  8Pitch
//
//  Created by 8pitch on 12.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift
import RxCocoa

class EditCell: CellModel<KYC> {
    
    private var type: EditCellType = .firstName
    
    private var label: UILabel = {
        TextFieldHeaderLabel()
    }()
    
    private let birthTextField = BirthdayTextField()
    private let countryTextField = CountryTextField()
    private let inputTextField = TextField()
    private let pickerTextField = PickerTextField()
    private let companyTextField = CompanyNameTextField()
    
    private var textField: TextField {
        switch type {
        case .birth:
            return self.birthTextField
        case .country,
             .nationality:
            return self.countryTextField
        case .title,
             .companyType:
            return self.pickerTextField
        case .companyName:
            return self.companyTextField
        default:
            return self.inputTextField
        }
    }
    
    private var additionalTextField: TextField = TextField()
    
    override class var reuseIdentifier: String {
        "EditCell"
    }
    
    override var cellHeight: CGFloat {
        Constants.cellHeight
    }
    
    override var model : BehaviorSubject<KYC>? {
        didSet {
            self.subscribeOnValidationResult()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.textField.text = nil
        self.label.text = nil
        self.additionalTextField.text = nil
        self.textField.setState(.success)
        self.additionalTextField.setState(.success)
        self.contentView.subviews.forEach { $0.removeFromSuperview() }
        self.disposables = DisposeBag()
    }
    
    public func setup(for type: EditCellType) {
        self.type = type
        self.label.text = self.type.rawValue.localized
        switch type {
        case .address:
            self.setupForAddress()
        default:
            self.setupCell()
        }
        self.setup()
    }
    
    override func setup() {
        super.setup()
        let isPickerType = (self.type == .companyType) || (self.type == .title)
        if isPickerType {
            self.setupModelForPicker()
            return
        }
        
        if self.type == .address {
            self.setupModelForAddress()
        }
        
        self.setupModel()
        self.subscribeOnValidationResult()
    }
    
}

private extension EditCell {
    
    func setupModel() {
        guard let model = try? self.model?.value(),
              let provider = model.providers[self.type] as? BehaviorSubject<String?> else { return }
        
            provider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            switch self.type {
            case .firstName:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.nameAndLastNameProvider.value().firstNameProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .lastName:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.nameAndLastNameProvider.value().lastNameProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .birth:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.birthDayAndPlaceProvider.value().dateProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .nationality:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.personalDetailsProvider.value().nationalityProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .country:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.localityProvider.value().countryProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .city:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.localityProvider.value().cityProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .zip:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.localityProvider.value().zipProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .address:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.addressProvider.value().streetProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .companyName:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.companyProvider.value().companyNameProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .companyNumber:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.companyProvider.value().registrationNumberProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .accountOwner:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.financialInfoProvider.value().accountOwnerProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .iban:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.financialInfoProvider.value().ibanProvider.onNext($0)
                    }).disposed(by: self.disposables)
            case .place:
                self.textField.rx
                    .controlEvent(.allEditingEvents)
                    .withLatestFrom(self.textField.rx.text.orEmpty)
                    .subscribe(onNext: {
                        try? model.birthDayAndPlaceProvider.value().placeProvider.onNext($0)
                    }).disposed(by: self.disposables)
            default:
                break
            }
    }
    
    func setupModelForPicker() {
        let isCompanyType = (self.type == .companyType)
        guard let model = try? self.model?.value(),
              let publishProvider = isCompanyType ? try? model.companyProvider.value().companyTypeProvider : try? model.nameAndLastNameProvider.value().titleProvider,
              let textField = self.textField as? PickerTextField else { return }
        self.textField.text = isCompanyType ? model.companyType?.rawValue.localized : model.title.rawValue.localized
        publishProvider
            .compactMap { $0 }
            .subscribe(onNext: {
                self.textField.text = $0.localized
            })
            .disposed(by: self.disposables)
        
        textField.tapProvider.subscribe(onNext: {
            guard $0 else { return }
            let valueProvider = isCompanyType ? try? model.companyProvider.value().valueProviders[\.companyTypeProvider] : try? model.nameAndLastNameProvider.value().valueProviders[\.titleProvider]
            valueProvider??()
        }).disposed(by: self.disposables)
    }
    
    func setupModelForAddress() {
        guard let model = try? self.model?.value() else { return }
        
        try? model.addressProvider.value().streetNoProvider
            .bind(to: self.additionalTextField.rx.text)
            .disposed(by: self.disposables)
        
        self.additionalTextField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.additionalTextField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? model.addressProvider.value().streetNoProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    func subscribeOnValidationResult() {
        guard let model = try? self.model?.value() else { return }
        let currentModel: Model?
        switch type {
        case .firstName,
             .lastName,
             .title:
            currentModel = try? model.nameAndLastNameProvider.value()
        case .accountOwner, .iban:
            currentModel = try? model.financialInfoProvider.value()
        case .address:
            currentModel = try? model.addressProvider.value()
        case .country,
             .city,
             .zip:
            currentModel = try? model.localityProvider.value()
        case .birth, .place:
            currentModel = try? model.birthDayAndPlaceProvider.value()
        case .nationality, .PEP, .USTL:
            currentModel = try? model.personalDetailsProvider.value()
        case .companyName, .companyType, .companyNumber:
            currentModel = try? model.companyProvider.value()
        case .phone, .email,
             .germanTaxesid, .germanTaxesOffice,
             .germanTaxesResident, .churchTaxLability,
             .churchTaxAttribute:
            return
        }
        currentModel?.isValid
            .subscribe(onNext: { event in
                switch event {
                case .failure(_ as FirstNameError):
                    self.textField.setState((self.type == .firstName) ? .error : .success)
                    return
                case .failure(_ as LastNameError):
                    self.textField.setState((self.type == .lastName) ? .error : .success)
                    return
                case .failure(_ as TitleError):
                    self.textField.setState((self.type == .title) ? .error : .success)
                    return
                case .failure(_ as AccountOwnerError):
                    self.textField.setState((self.type == .accountOwner) ? .error : .success)
                    return
                case .failure(_ as IBANError):
                    self.textField.setState((self.type == .iban) ? .error : .success)
                    return
                case .failure(let error as AddressError):
                    switch error {
                    case .numberEmpty, .numberInvalid:
                        self.additionalTextField.setState((self.type == .address) ? .error : .success)
                    case .streetEmpty, .streetInvalid:
                        self.textField.setState((self.type == .address) ? .error : .success)
                    }
                    return
                case .failure(_ as CountryError):
                    self.textField.setState((self.type == .country) ? .error : .success)
                    return
                case .failure(let error as LocalityError):
                    switch error {
                    case .zipInvalid, .zipEmpty:
                        self.textField.setState((self.type == .zip) ? .error : .success)
                    case .cityEmpty, .cityInvalid:
                        self.textField.setState((self.type == .city) ? .error : .success)
                    }
                    return
                case .failure(_ as BirthDayError):
                    self.textField.setState((self.type == .birth) ? .error : .success)
                    return
                case .failure(_ as BirthPlaceError):
                    self.textField.setState((self.type == .place) ? .error : .success)
                    return
                case .failure(_ as PersonalDetailsError):
                    self.textField.setState((self.type == .nationality) ? .error : .success)
                    return
                case .failure(_ as CompanyError):
                    self.textField.setState((self.type == .companyName) ? .error : .success)
                    return
                case .failure(_ as CompanyTypeError):
                    self.textField.setState((self.type == .companyType) ? .error : .success)
                    return
                case .failure(_ as CompanyNumberError):
                    self.textField.setState((self.type == .companyNumber) ? .error : .success)
                    return
                default:
                    self.textField.setState(.success)
                    self.additionalTextField.setState(.success)
                }
            }).disposed(by: self.disposables)
    }
    
    func setupCell() {
        let field = self.textField
        self.textField.delegate = self
        self.contentView.addSubview(field)
        self.contentView.addSubview(self.label)
        field.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(Constants.fieldHeight)
        }
        self.label.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Constants.labelInset)
            $0.leading.equalToSuperview()
        }
    }
    
    func setupForAddress() {
        let field = self.textField
        let additionalField = self.additionalTextField
        additionalField.delegate = self
        let numberLabel = TextFieldHeaderLabel()
        numberLabel.text = "summarize.header.address.number".localized
        self.contentView.addSubview(field)
        self.contentView.addSubview(additionalField)
        self.contentView.addSubview(self.label)
        self.contentView.addSubview(numberLabel)
        field.snp.makeConstraints {
            $0.leading.bottom.equalToSuperview()
            $0.height.equalTo(Constants.fieldHeight)
        }
        additionalField.snp.makeConstraints {
            $0.leading.equalTo(field.snp.trailing).offset(Constants.fieldInset)
            $0.bottom.trailing.equalToSuperview()
            $0.width.equalTo(Constants.fieldWidth)
            $0.height.equalTo(Constants.fieldHeight)
        }
        
        self.label.snp.makeConstraints {
            $0.top.equalToSuperview().inset(Constants.labelInset)
            $0.leading.equalToSuperview()
        }
        
        numberLabel.snp.makeConstraints {
            $0.leading.equalTo(additionalField.snp.leading)
            $0.top.equalToSuperview().inset(Constants.labelInset)
        }
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 80
        static let fieldHeight: CGFloat = 44
        static let fieldInset: CGFloat = 16
        static let fieldWidth: CGFloat = 97
        static let labelInset: CGFloat = 22
        static let maxLenght = 255
        static let zipMaxLenght = 6
    }
    
}

extension EditCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        let maxLength = (type == .zip) ? Constants.zipMaxLenght : Constants.maxLenght
        return newString.length <= maxLength
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        try? self.model?.value().validate()
    }
    
}


