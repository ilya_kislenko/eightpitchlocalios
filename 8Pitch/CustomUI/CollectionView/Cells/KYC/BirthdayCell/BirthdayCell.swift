//
//  BirthdayCell.swift
//  8Pitch
//
//  Created by 8pitch on 28.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class BirthdayCell: CellModel<BirthDayAndPlace> {
    
    @IBOutlet weak var textField: BirthdayTextField!
    
    // MARK: Overrides
    
    override class var reuseIdentifier: String {
        "BirthdayCell"
    }
    
    override var cellHeight : CGFloat {
        64.0
    }
    
    override var model : BehaviorSubject<BirthDayAndPlace>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.dateProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as BirthDayError):
                        switch error {
                        case .dateOfBirthEmpty:
                            self.textField.setState(.error)
                        case .dateOfBirthInvalid:
                            self.textField.setState(.error)
                        }
                    default:
                        self.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override func setup() {
        self.textField.rx
            .controlEvent(.editingDidEnd)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().dateProvider.onNext($0)
            }).disposed(by: self.disposables)
    }

}
