//
//  TaxCell.swift
//  8Pitch
//
//  Created by 8pitch on 30.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class TaxCell: CellModel<PersonalDetails> {    
    
    private var segmentedControl: CustomSegmentedControl?
    
    // MARK: Overrides
    
    override var model: BehaviorSubject<PersonalDetails>? {
        didSet {
            self.setupState()
        }
    }
    
    class override var reuseIdentifier : String {
        return "TaxCell"
    }
    
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addSegmentedControl()
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 88
    }
    
    private func addSegmentedControl() {
        guard let segmentedControl = CustomSegmentedControl.nib(owner: self) else {
            return
        }
        self.contentView.addSubview(segmentedControl)
        segmentedControl.snp.makeConstraints {
            $0.trailing.centerY.equalToSuperview()
        }
        self.segmentedControl = segmentedControl
        segmentedControl.delegateAction = { [weak self] bool in
            try? self?.model?.value().usTaxLiabilityProvider.onNext(bool)
        }
    }
    
    private func setupState() {
        self.segmentedControl?.setupUIForState((try? self.model?.value().usTaxLiabilityProvider.value()) ?? false)
    }
    
}
