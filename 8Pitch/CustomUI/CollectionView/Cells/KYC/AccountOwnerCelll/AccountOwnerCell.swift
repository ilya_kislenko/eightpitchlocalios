//
//  AccountOwnerCell.swift
//  8Pitch
//
//  Created by 8pitch on 01.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class AccountOwnerCell: CellModel<FinancialInfo> {
    
    @IBOutlet weak var textField: TextField!
    @IBOutlet weak var addNameButton: UIButton!
    
    // MARK: Overrides
    
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    class override var reuseIdentifier : String {
        return "AccountOwnerCell"
    }
    
    override var model : BehaviorSubject<FinancialInfo>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
//            model.userName?.bind(to: self.textField.rx.text).disposed(by: self.disposables)
            
            model.accountOwnerProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as AccountOwnerError) where error == .accountOwnerEmpty:
                        self.textField.setState(.error)
                    case .failure(let error as AccountOwnerError) where error == .accountOwnerInvalid:
                        self.textField.setState(.error)
                    default: self.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override func setup() {
        self.addNameButton.isSelected = true
        self.textField.isUserInteractionEnabled = false
        textField.autocapitalizationType = .sentences
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().accountOwnerProvider.onNext($0)
            }).disposed(by: self.disposables)
                
        self.addNameButton.rx
            .controlEvent(.valueChanged)
        .withLatestFrom(self.textField.rx.text.orEmpty)
        .subscribe(onNext: {
            try? self.model?.value().accountOwnerProvider.onNext($0)
        }).disposed(by: self.disposables)
    }
    
    // MARK: Actions
    
    @IBAction func addNameTapped(_ sender: UIButton) {
        guard let model = try? self.model?.value() else { return }
        sender.isSelected = !sender.isSelected
        self.textField.isUserInteractionEnabled = !sender.isSelected
        if sender.isSelected {
            self.textField.text = model.fullName
            model.accountOwnerProvider.onNext(model.fullName)
        }
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 104
    }
    
}
