//
//  PlaceOfBirthCell.swift
//  8Pitch
//
//  Created by 8pitch on 28.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class PlaceOfBirthCell: CellModel<BirthDayAndPlace> {
    
    @IBOutlet weak var textField: TextField!
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    // MARK: Overrides
    
    override class var reuseIdentifier: String {
        "PlaceOfBirthCell"
    }
    
    override var model : BehaviorSubject<BirthDayAndPlace>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.placeProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as BirthPlaceError) where error == .placeOfBirthInvalid:
                        self.textField.setState(.error)
                    case .failure(let error as BirthPlaceError) where error == .placeOfBirthEmpty:
                        self.textField.setState(.error)
                    default: self.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override func setup() {
        textField.delegate = self
        textField.autocapitalizationType = .sentences
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().placeProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 84
    }
    
}

// MARK: UITextFieldDelegate

extension PlaceOfBirthCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let resultedText = textField.text ?? "" + string
        if textField == self.textField && resultedText.count > 255 {
            return false
        }
        return true
    }
    
}
