//
//  NationalityCell.swift
//  8Pitch
//
//  Created by 8pitch on 30.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class NationalityCell: CellModel<PersonalDetails> {
    
    // MARK: UI Components
    
    @IBOutlet weak var textField: CountryTextField!
    
    // MARK: Overrides
    
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    class override var reuseIdentifier : String {
        return "NationalityCell"
    }
    
    override var model : BehaviorSubject<PersonalDetails>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.nationalityProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
        }
    }
    
    override func setup() {
        self.textField.rx
            .controlEvent(.editingDidEnd)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().nationalityProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 62
    }
    
}

