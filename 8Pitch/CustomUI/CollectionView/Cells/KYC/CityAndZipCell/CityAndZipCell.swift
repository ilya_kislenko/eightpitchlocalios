//
//  CityAndZipCell.swift
//  8Pitch
//
//  Created by 8pitch on 01.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class CityAndZipCell: CellModel<Locality> {
    
    // MARK: Outlets
    
    @IBOutlet weak var cityTextField: TextField!
    @IBOutlet weak var zipTextField: TextField!
    
    // MARK: Overrides
    
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    class override var reuseIdentifier : String {
        return "CityAndZipCell"
    }
    
    override var model : BehaviorSubject<Locality>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.cityProvider
                .bind(to: self.cityTextField.rx.text)
                .disposed(by: self.disposables)
            
            model.zipProvider
                .bind(to: self.zipTextField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as LocalityError) where error == .cityEmpty:
                        self.cityTextField.setState(.error)
                    case .failure(let error as LocalityError) where error == .cityInvalid:
                        self.cityTextField.setState(.error)
                    case .failure(let error as LocalityError) where error == .zipEmpty:
                        self.zipTextField.setState(.error)
                    case .failure(let error as LocalityError) where error == .zipInvalid:
                        self.zipTextField.setState(.error)
                    default:
                        self.cityTextField.setState(.success)
                        self.zipTextField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override func setup() {
        self.zipTextField.delegate = self
        self.cityTextField.autocapitalizationType = .sentences
        self.zipTextField.autocapitalizationType = .sentences
        self.cityTextField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.cityTextField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().cityProvider.onNext($0)
            }).disposed(by: self.disposables)
        
        self.zipTextField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.zipTextField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().zipProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 82
        static let cityMaxLenght = 255
        static let zipMaxLenght = 6
    }
    
}

// MARK: UITextFieldDelegate

extension CityAndZipCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if textField == cityTextField {
            return newString.length <= Constants.cityMaxLenght
        } else if textField == zipTextField {
            return newString.length <= Constants.zipMaxLenght
        }
        return true
    }
    
}

