//
//  EditHeaderView.swift
//  8Pitch
//
//  Created by 8pitch on 11.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class EditHeaderCell: CellModel<KYC> {
    
    @IBOutlet weak var textLabel: PersonalHeaderLabel!
    
    class override var reuseIdentifier : String {
        return "EditHeaderCell"
    }
    
    public func setTitle(_ title: String?) {
        self.textLabel.text = title
    }
    
    override var cellHeight: CGFloat {
        Constants.cellHeight
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 80
    }
    
}
