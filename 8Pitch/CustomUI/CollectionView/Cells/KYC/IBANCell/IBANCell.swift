//
//  IBANCell.swift
//  8Pitch
//
//  Created by 8pitch on 01.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class IBANCell: CellModel<FinancialInfo> {
    
    @IBOutlet weak var textField: TextField!
    
    // MARK: Overrides
    
    override var cellHeight : CGFloat {
        Constants.cellHeight
    }
    
    class override var reuseIdentifier : String {
        return "IBANCell"
    }
    
    override var model : BehaviorSubject<FinancialInfo>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.ibanProvider
                .bind(to: self.textField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as IBANError):
                        switch error {
                        case .ibanEmpty:
                            self.textField.setState(.error)
                        case .ibanInvalid:
                            self.textField.setState(.error)
                        }
                    default:
                        self.textField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override func setup() {
        self.textField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.textField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().ibanProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 90
    }
    
}
