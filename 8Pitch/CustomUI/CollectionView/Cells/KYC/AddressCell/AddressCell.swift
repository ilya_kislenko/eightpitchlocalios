//
//  AddressCell.swift
//  8Pitch
//
//  Created by 8pitch on 01.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class AddressCell: CellModel<Address> {
    
    //MARK: Outlets
    
    @IBOutlet weak var streetTextField: TextField!
    @IBOutlet weak var numberTextField: TextField!
    
    //MARK: Overrides
    
    class override var reuseIdentifier : String {
        return "AddressCell"
    }
    
    override var model : BehaviorSubject<Address>? {
        didSet {
            guard let model = try? self.model?.value() else { return }
            
            model.streetProvider
                .bind(to: self.streetTextField.rx.text)
                .disposed(by: self.disposables)
            
            model.streetNoProvider
                .bind(to: self.numberTextField.rx.text)
                .disposed(by: self.disposables)
            
            model.isValid
                .subscribe(onNext: { event in
                    switch event {
                    case .failure(let error as AddressError) where error == .streetEmpty:
                        self.streetTextField.setState(.error)
                    case .failure(let error as AddressError) where error == .streetInvalid:
                        self.streetTextField.setState(.error)
                    case .failure(let error as AddressError) where error == .numberEmpty:
                        self.numberTextField.setState(.error)
                    case .failure(let error as AddressError) where error == .numberInvalid:
                        self.numberTextField.setState(.error)
                    default: self.numberTextField.setState(.success)
                    self.streetTextField.setState(.success)
                    }
                }).disposed(by: self.disposables)
        }
    }
    
    override var cellHeight : CGFloat {
        68.0
    }
    
    override func setup() {
        streetTextField.delegate = self
        numberTextField.delegate = self
        streetTextField.autocapitalizationType = .sentences
        numberTextField.autocapitalizationType = .sentences
        self.streetTextField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.streetTextField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().streetProvider.onNext($0)
            }).disposed(by: self.disposables)
        
        self.numberTextField.rx
            .controlEvent(.editingChanged)
            .withLatestFrom(self.numberTextField.rx.text.orEmpty)
            .subscribe(onNext: {
                try? self.model?.value().streetNoProvider.onNext($0)
            }).disposed(by: self.disposables)
    }
    
}

// MARK: UITextFieldDelegate

extension AddressCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let resultedText = textField.text ?? "" + string
        if textField == streetTextField && resultedText.count > 255  {
            return false
        }
        if textField == numberTextField && resultedText.count > 255 {
            return false
        }
        return true
    }
    
}
