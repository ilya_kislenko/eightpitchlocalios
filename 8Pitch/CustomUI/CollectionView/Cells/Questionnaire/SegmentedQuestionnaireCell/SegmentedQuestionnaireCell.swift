//
//  SegmentedQuestionnaireCell.swift
//  8Pitch
//
//  Created by 8pitch on 8/5/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class SegmentedQuestionnaireCell: CellModel<Questionnaire> {
    @IBOutlet var textValueLabel: CustomAssetTitleLabel!
    private var segmentedControl: CustomSegmentedControl?
    
    private func setupModel(assetClasses: AssetClasses, selectedIndex: Int) {
        switch indexPath?.section {
        case 0:
            if selectedIndex == 0 {
                assetClasses.tokenizedSecuritiesProvider = AssetClassesType.tokenizedSecurities.rawValue
            } else {
                assetClasses.tokenizedSecuritiesProvider = nil
            }
        case 1:
            if selectedIndex == 0 {
                assetClasses.stocksProvider = AssetClassesType.stocks.rawValue
            } else {
                assetClasses.stocksProvider = nil
            }
        case 2:
            if selectedIndex == 0 {
                assetClasses.hedgeFundsProvider = AssetClassesType.hedgeFunds.rawValue
            } else {
                assetClasses.hedgeFundsProvider = nil
            }
        case 3:
            if selectedIndex == 0 {
                assetClasses.warrantsProvider = AssetClassesType.warrants.rawValue
            } else {
                assetClasses.warrantsProvider = nil
            }
        case 4:
            if selectedIndex == 0 {
                assetClasses.bondsProvider = AssetClassesType.bonds.rawValue
            } else {
                assetClasses.bondsProvider = nil
            }
        case 5:
            if selectedIndex == 0 {
                assetClasses.closedEndMutualFundsProvider = AssetClassesType.closedEndMutualFunds.rawValue
            } else {
                assetClasses.closedEndMutualFundsProvider = nil
            }
        case 6:
            if selectedIndex == 0 {
                assetClasses.investmentCertificatesProvider = AssetClassesType.investmentCertificates.rawValue
            } else {
                assetClasses.investmentCertificatesProvider = nil
            }
        default:
            break
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addSegmentedControl()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        textValueLabel.text = content as? String
    }
    
    class override var reuseIdentifier : String {
        return "SegmentedQuestionnaireCell"
    }
    
    private func addSegmentedControl() {
        guard let segmentedControl = CustomSegmentedControl.nib(owner: self) else {
            return
        }
        self.contentView.addSubview(segmentedControl)
        segmentedControl.snp.makeConstraints {
            $0.trailing.centerY.equalToSuperview()
            $0.leading.equalTo(textValueLabel.snp.trailing)
        }
        self.segmentedControl = segmentedControl
        segmentedControl.delegateAction = { [weak self] _ in
            guard let model = try? self?.model?.value().assetClassesProvider.value() else { return }
            self?.setupModel(assetClasses: model, selectedIndex: self?.segmentedControl?.index ?? 1)
        }
        self.segmentedControl?.setupUIForState(false)
    }
}
