//
//  SquareCheckMarkCell.swift
//  8Pitch
//
//  Created by 8pitch on 8/7/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

fileprivate struct Constants {
    static let squareCheckMarkCellHeight = CGFloat(88)
}

class SquareCheckMarkCell: CellModel<Questionnaire> {
    @IBOutlet var checkImageView: UIImageView!
    @IBOutlet var textValueLabel: RadioLabel!
    
    @IBOutlet var containerView: UIView!

    override var cellHeight: CGFloat {
        Constants.squareCheckMarkCellHeight
    }

    override var isSelected: Bool {
        didSet {
            self.containerView.backgroundColor = isSelected ? UIColor(Color.backgroundRed) : UIColor(Color.borderWhite)
            let checkImage = isSelected ? UIImage(.checkOn) : UIImage(.checkOff)
            self.checkImageView.image = checkImage.withRenderingMode(.alwaysTemplate)
            self.checkImageView.tintColor = isSelected ? UIColor(Color.red) : UIColor(Color.gray)
        }
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.textValueLabel.text = (content as? String)?.uppercased()
    }

    class override var reuseIdentifier : String {
        return "SquareCheckMarkCell"
    }
}
