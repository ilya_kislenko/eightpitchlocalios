//
//  CheckMarkCell.swift
//  8Pitch
//
//  Created by 8pitch on 8/5/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class CheckMarkCell: CellModel<Questionnaire> {
    @IBOutlet var containerView: UIView!
    @IBOutlet var radioButton: UIImageView!
    @IBOutlet var textValueLabel: RadioLabel!

    override var cellHeight: CGFloat {
        78
    }

    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.containerView.backgroundColor = UIColor(Color.backgroundRed)
            } else {
                self.containerView.backgroundColor = UIColor(Color.borderWhite)
            }
        }
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.textValueLabel.text = self.content as? String
    }

      class override var reuseIdentifier : String {
          return "CheckMarkCell"
      }
}
