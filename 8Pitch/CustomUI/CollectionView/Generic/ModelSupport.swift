//
//  ModelSupport.swift
//  8Pitch
//
//  Created by 8pitch on 27.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import RxSwift

protocol ModelSupport {
    
    associatedtype T
    
    var model : BehaviorSubject<T>? { get set }
    var disposables : DisposeBag { get set }
    
}

class CellModel<T : Model> : UICollectionViewCell, ModelSupport {
    
    // MARK: Constants
    
    var cellHeight : CGFloat {
        60.0
    }

    var maxNumberOfSymbols: Int {
        255
    }
    
    var disposables : DisposeBag = .init()
    var model : BehaviorSubject<T>?
    var content : Any?
    var indexPath: IndexPath?
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        layoutAttributes.size.height = self.cellHeight
        return layoutAttributes
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    class var reuseIdentifier : String {
        fatalError("this var needs to be overridden!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposables = DisposeBag()
        self.setup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {}
    
}

class DecorationViewModel<T : Model> : UICollectionReusableView, ModelSupport {
    
    var disposables : DisposeBag = .init()
    var model : BehaviorSubject<T?>? = BehaviorSubject.init(value: nil)
    private(set) var nextButtonAction : BehaviorSubject<NextState?> = .init(value: nil)
    private(set) var additionalButtonAction : BehaviorSubject<NextState?> = .init(value: nil)
    private(set) var forgotButtonAction : BehaviorSubject<NextState?> = .init(value: nil)
    private(set) var metadataProvider : BehaviorSubject<Model?>? = .init(value: nil)
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        if let attributes = layoutAttributes as? DecoratableLayoutAttributes, let model = attributes.model {
            self.model?.onNext(model as? BehaviorSubject<T>.Element)
            attributes.nextButtonAction.onNext(self.nextButtonAction)
            attributes.additionalButtonAction.onNext(self.additionalButtonAction)
            attributes.forgotButtonAction.onNext(self.forgotButtonAction)
            attributes.metadataProvider.onNext(self.metadataProvider)
        }
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        var attributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        if let decoratableAttributes = layoutAttributes as? DecoratableLayoutAttributes {
            decoratableAttributes.frame.size.width = decoratableAttributes.collectionWidth ?? Constants.footerWidth
            attributes = decoratableAttributes
        }
        return attributes
    }
    
    class var reuseIdentifier : String {
        fatalError("this var needs to be overridden!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.nextButtonAction.onNext(nil)
        self.additionalButtonAction.onNext(nil)
        self.forgotButtonAction.onNext(nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {}
    
}

fileprivate struct Constants {
    static let footerWidth : CGFloat = UIScreen.main.bounds.width - 48
}
