//
//  DecoratableCollectionViewLayout.swift
//  8Pitch
//
//  Created by 8pitch on 31.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class DecoratableLayoutAttributes<T : Model> : UICollectionViewLayoutAttributes {
    
    var model: T?
    var nextButtonAction : BehaviorSubject<BehaviorSubject<NextState?>?> = .init(value: nil)
    var additionalButtonAction : BehaviorSubject<BehaviorSubject<NextState?>?> = .init(value: nil)
    var forgotButtonAction : BehaviorSubject<BehaviorSubject<NextState?>?> = .init(value: nil)
    var navigationFooterViewType : PublishSubject<PublishSubject<NavigationView.ViewType?>?> = .init()
    var confirmationFooterViewType : PublishSubject<PublishSubject<InputSMSView.ViewType?>?> = .init()
    var emailConfirmationFooterViewType : PublishSubject<PublishSubject<VerifyEmailView.ViewType?>?> = .init()
    var editFooterViewType : PublishSubject<PublishSubject<ApplyChangesView.ViewType?>?> = .init()
    var metadataProvider : BehaviorSubject<BehaviorSubject<Model?>?> = .init(value: nil)
    var collectionWidth : CGFloat?
    
}

class DecoratableCollectionViewLayout<T : Model> : UICollectionViewLayout {
    
    var model : BehaviorSubject<T>
    var bottomFooterReuseIdentifier : String?
    var nextButtonAction : BehaviorSubject<BehaviorSubject<NextState?>?> = .init(value: nil)
    var additionalButtonAction : BehaviorSubject<BehaviorSubject<NextState?>?> = .init(value: nil)
    var forgotButtonAction : BehaviorSubject<BehaviorSubject<NextState?>?> = .init(value: nil)
    var navigationFooterViewType : PublishSubject<PublishSubject<NavigationView.ViewType?>?> = .init()
    var confirmationFooterViewType : PublishSubject<PublishSubject<InputSMSView.ViewType?>?> = .init()
    var emailConfirmationFooterViewType : PublishSubject<PublishSubject<VerifyEmailView.ViewType?>?> = .init()
    var editFooterViewType : PublishSubject<PublishSubject<ApplyChangesView.ViewType?>?> = .init()
    var metadataProvider : BehaviorSubject<BehaviorSubject<Model?>?> = .init(value: nil)
    private var disposables : DisposeBag = .init()
    
    init(model: BehaviorSubject<T>) {
        self.model = model
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("unimplemented!")
    }
    
    override func layoutAttributesForDecorationView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.cachedDecorationAttributes[indexPath]
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.cachedCellAttributes[indexPath]
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var allRects : [UICollectionViewLayoutAttributes : CGRect] = [:]
        self.cachedCellAttributes.forEach { allRects[$0.value] = $0.value.frame }
        self.cachedDecorationAttributes.forEach { allRects[$0.value] = $0.value.frame }
        let filtered = allRects.filter { rect.intersects($0.value) }
        return Array(filtered.keys)
    }
    
    var contentWidth : CGFloat = 0.0
    var cachedCellAttributes : [IndexPath : UICollectionViewLayoutAttributes] = [:]
    var cachedDecorationAttributes : [IndexPath : UICollectionViewLayoutAttributes] = [:]
    
    private let defaultCellHeight : CGFloat = 0.9
    private let minimumCellHeight : CGFloat = 1.0
    private let footerInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    var contentInsets : UIEdgeInsets = .zero
    
    private func layoutAttributesForCell(at indexPath: IndexPath, originY: CGFloat) -> UICollectionViewLayoutAttributes {
        let itemHeight : CGFloat = self.defaultCellHeight
        let itemWidth = self.contentWidth
        let itemY = originY
        
        let frame = CGRect(x: self.contentInsets.left, y: itemY, width: itemWidth, height: itemHeight)
        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attributes.frame = frame
        
        return attributes
    }
    
    override func shouldInvalidateLayout(forPreferredLayoutAttributes preferredAttributes: UICollectionViewLayoutAttributes, withOriginalAttributes originalAttributes: UICollectionViewLayoutAttributes) -> Bool {
        let indexPath = originalAttributes.indexPath
        var frame = preferredAttributes.frame
        frame.origin.x = self.contentInsets.right
        if let _ = originalAttributes.representedElementKind {
            // TODO: distinguish between different elementKind if needed
            frame.origin.y = self.footerInsets.top
            preferredAttributes.frame = frame
            self.cachedDecorationAttributes[indexPath] = preferredAttributes
        } else {
            preferredAttributes.frame = frame
            self.cachedCellAttributes[indexPath] = preferredAttributes
        }
        return true
    }
    
    // override for fix of the issue when View Debugger would draw the cell but the actual layout won't
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        self.cachedCellAttributes[itemIndexPath]
    }
    
    // override for fix of the animation of the footer when layout is invalidated
    override func initialLayoutAttributesForAppearingDecorationElement(ofKind elementKind: String, at decorationIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        self.cachedDecorationAttributes[decorationIndexPath]
    }
    
    private func layoutAttributesForBottomFooterDecoration(at indexPath: IndexPath, originY: CGFloat) -> UICollectionViewLayoutAttributes? {
        guard let reuseIdentifier = self.bottomFooterReuseIdentifier else {
            return nil
        }
        let attributes = DecoratableLayoutAttributes(forDecorationViewOfKind: reuseIdentifier, with: indexPath)
        attributes.collectionWidth = self.collectionView?.frame.width ?? 0.0
        var frame = CGRect(x: self.contentInsets.left, y: originY, width: self.contentWidth, height: self.defaultCellHeight)
        frame.origin.y = self.footerInsets.top
        attributes.frame = frame
        attributes.model = try! self.model.value()
        
        attributes.nextButtonAction
            .compactMap { $0 }
            .subscribe(onNext: {
                self.nextButtonAction.onNext($0)
            }).disposed(by: self.disposables)
        attributes.confirmationFooterViewType
            .compactMap { $0 }
            .subscribe(onNext: {
                self.confirmationFooterViewType.onNext($0)
            }).disposed(by: self.disposables)
        attributes.emailConfirmationFooterViewType
            .compactMap { $0 }
            .subscribe(onNext: {
                self.emailConfirmationFooterViewType.onNext($0)
            }).disposed(by: self.disposables)
        attributes.navigationFooterViewType
            .compactMap { $0 }
            .subscribe(onNext: {
                self.navigationFooterViewType.onNext($0)
            }).disposed(by: self.disposables)
        attributes.editFooterViewType
        .compactMap { $0 }
        .subscribe(onNext: {
            self.editFooterViewType.onNext($0)
        }).disposed(by: self.disposables)
        attributes.additionalButtonAction
            .compactMap { $0 }
            .subscribe(onNext: {
                self.additionalButtonAction.onNext($0)
            }).disposed(by: self.disposables)
        attributes.forgotButtonAction
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                self?.forgotButtonAction.onNext($0)
            }).disposed(by: self.disposables)
        attributes.metadataProvider
            .compactMap { $0 }
            .subscribe(onNext: {
                self.metadataProvider.onNext($0)
            }).disposed(by: self.disposables)
        return attributes
    }
    
    override func invalidateLayout() {
        super.invalidateLayout()
    }
    
    private func smartCleanCaches() {
        guard let collectionView = self.collectionView else {
            return
        }
        let sectionCount = collectionView.numberOfSections
        var existingIndexPaths : [IndexPath] = []
        for section in 0 ..< sectionCount {
            let itemCount = collectionView.numberOfItems(inSection: section)
            for item in 0 ..< itemCount {
                let indexPath = IndexPath(item: item, section: section)
                existingIndexPaths.append(indexPath)
            }
        }
        let cachedCellAttrs : [IndexPath] = Array(self.cachedCellAttributes.keys)
        for key in cachedCellAttrs {
            if !existingIndexPaths.contains(key) {
                self.cachedCellAttributes[key] = nil
                self.cachedDecorationAttributes[key] = nil
            }
        }
    }
    
    private func recalculateLayout() {
        guard let collectionView = self.collectionView else {
            return
        }
        let numberOfSections = collectionView.numberOfSections
        var currentOriginY : CGFloat = 0.0
        
        let cacheNewAttributes : (CGFloat, IndexPath) -> Void = { originY, indexPath in
            let cellAttributes = self.layoutAttributesForCell(at: indexPath, originY: originY)
            self.cachedCellAttributes[indexPath] = cellAttributes
        }
        
        for section in 0 ..< numberOfSections {
            let numberOfRows = collectionView.numberOfItems(inSection: section)
            for item in 0 ..< numberOfRows {
                let indexPath = IndexPath(item: item, section: section)
                if let existingAttrs = self.cachedCellAttributes[indexPath] {
                    if existingAttrs.frame.height < self.minimumCellHeight {
                        cacheNewAttributes(currentOriginY, indexPath)
                    } else {
                        existingAttrs.frame.origin.y = currentOriginY
                        self.cachedCellAttributes[indexPath] = existingAttrs
                        currentOriginY = existingAttrs.frame.maxY
                    }
                } else {
                    cacheNewAttributes(currentOriginY, indexPath)
                }
            }
        }
        // draw footer only for the last item, no matter if there's additional cell in its section
        let footerIndexPath = IndexPath(item: 0, section: max(0, numberOfSections - 1))
        if let existingAttributes = self.cachedDecorationAttributes[footerIndexPath] {
            existingAttributes.frame.origin.y = currentOriginY + self.footerInsets.top
            self.cachedDecorationAttributes[footerIndexPath] = existingAttributes
        } else {
            let footerAttributes = self.layoutAttributesForBottomFooterDecoration(at: footerIndexPath, originY: currentOriginY)
            self.cachedDecorationAttributes[footerIndexPath] = footerAttributes
        }
    }
    
    override func prepare() {
        super.prepare()
        guard let collectionView = self.collectionView else {
            return
        }
        
        self.contentWidth = collectionView.frame.width - (self.contentInsets.left + self.contentInsets.right)
        
        self.smartCleanCaches()
        self.recalculateLayout()
        super.prepare()
    }
    
    override var collectionViewContentSize: CGSize {
        var attributes : [UICollectionViewLayoutAttributes] = []
        attributes += self.cachedCellAttributes.values
        attributes += self.cachedDecorationAttributes.values
        let height = attributes.reduce(into: CGFloat()) { $0 += $1.frame.height }
        return CGSize(width: self.contentWidth, height: height)
    }
    
}
