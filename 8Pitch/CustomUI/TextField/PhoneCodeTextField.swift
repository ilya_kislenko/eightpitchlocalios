//
//  PhoneCodeTextField.swift
//  8Pitch
//
//  Created by 8pitch on 17.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class PhoneCodeTextField: TextField {
        
    override var customRightView : UIView? {
        UIImageView(image: UIImage(.select))
    }
    
    public func setupFor(country: PickerData?) {
        if let flag = country?.iso?.flag(), let code = country?.code {
            self.text = " \(flag) +\(code)"
        }
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        let initialRect = super.rightViewRect(forBounds: bounds)
        return CGRect(x: initialRect.origin.x - 7, y: initialRect.origin.y, width: initialRect.width, height: initialRect.height)
    }
    
}
