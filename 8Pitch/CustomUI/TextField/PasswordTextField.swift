//
//  PasswordTextField.swift
//  8Pitch
//
//  Created by 8pitch on 22.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PasswordTextField: TextField {
    
    var normalImage : UIImage {
        UIImage(.visibilityOff)
    }
    
    var selectedImage : UIImage {
        UIImage(.visibilityOn)
    }
    
    var action : Selector? {
        #selector(toggleSecureTextEntry)
    }
    
    override var customRightView : UIView? {
        let button  = UIButton()
        button.setImage(self.normalImage, for: .normal)
        button.setImage(self.selectedImage, for: .selected)
        if let action = self.action {
            button.addTarget(self, action: action, for: .touchUpInside)
        }
        button.snp.makeConstraints {
            $0.height.width.equalTo(Constants.viewSideSize)
        }
        return button
    }
    
    @objc private func toggleSecureTextEntry(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.isSecureTextEntry.toggle()
        self.becomeFirstResponder()
    }

}
