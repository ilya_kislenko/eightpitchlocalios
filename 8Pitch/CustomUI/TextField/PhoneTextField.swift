//
//  PhoneTextField.swift
//  8Pitch
//
//  Created by 8pitch on 17.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PhoneTextField: TextField {
    
    // MARK: Init
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupContent()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupContent()
    }
}

// MARK: Private

private extension PhoneTextField {
    
    func setupContent() {
        self.placeholder = "phoneTextField.placeholder".localized
    }
    
}
