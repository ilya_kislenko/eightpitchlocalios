//
//  TapTextField.swift
//  8Pitch
//
//  Created by 8pitch on 10.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift

class TapTextField : TextField, UITextFieldDelegate {
    
    override var customRightView : UIView? {
        nil
    }
    
    let tapProvider : PublishSubject<Bool> = .init()
    
    override func setup() {
        super.setup()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapped))
        self.addGestureRecognizer(tap)
    }
    
    @objc func tapped() {
        self.tapProvider.onNext(true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        false
    }
    
}
