//
//  DirectPaymentTextField.swift
//  8Pitch
//
//  Created by 8pitch on 9/8/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class DirectPaymentTextField: TextField {

    override func setup() {
        super.setup()
        self.underlineColor.onNext(UIColor(.gray))
    }

}
