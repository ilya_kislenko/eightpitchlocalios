//
//  DeletionTextField.swift
//  8Pitch
//
//  Created by 8pitch on 19.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class DeletionTextField: CountryTextField {
    
    override var titles: [Any] {
        DataSource.deletionReasons
    }
    
    override func setup() {
        super.setup()
        self.underlineColor.onNext(UIColor(.gray))
    }
    
    enum Constants {
        static let cellHeight: CGFloat = 60
        static let labelHeight: CGFloat = 44
        static let labelWidth: CGFloat = UIScreen.main.bounds.width - 32
    }
}

extension DeletionTextField {
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: Constants.labelWidth, height: Constants.labelHeight));
        label.lineBreakMode = .byWordWrapping;
        label.numberOfLines = 0;
        label.text =  (self.titles[row] as? PickerData)?.name
        label.sizeToFit()
        return label;
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return Constants.cellHeight
    }
}
