//
//  QualificationTypeTextField.swift
//  8Pitch
//
//  Created by 8pitch on 19.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class QualificationTypeTextField: CountryTextField {
    
    override var titles: [Any] {
        DataSource.qualificationType
    }
    
    override func setup() {
        super.setup()
        self.underlineColor.onNext(UIColor(.placeholderGray))
    }
    
}
