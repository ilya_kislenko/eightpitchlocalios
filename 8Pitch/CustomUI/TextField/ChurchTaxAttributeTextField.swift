//
//  ChurchTaxAttributeTextField.swift
//  8Pitch
//
//  Created by 8pitch on 13.01.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class ChurchTaxAttributeTextField: BirthdayTextField {

    private let picker = UIPickerView()
    
    override var normalImage : UIImage {
        UIImage(.select)
    }
    
    override var selectedImage : UIImage {
        UIImage(.select)
    }
    
    public var titles: [Any] = []
    
    override func setup() {
        super.setup()
        self.picker.dataSource = self
        self.picker.delegate = self
        self.inputView = picker
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.pickerTapped(sender:)))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        self.picker.addGestureRecognizer(tap)
    }
}

// MARK: UIPickerViewDataSource, UIPickerViewDelegate

extension ChurchTaxAttributeTextField: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        self.titles.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.titles[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.text = self.titles[row] as? String
    }
}

extension ChurchTaxAttributeTextField: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
    
    @objc func pickerTapped(sender: UITapGestureRecognizer) {
        if (sender.state == .ended) {
            let rowHeight : CGFloat  = self.picker.rowSize(forComponent: 0).height
            let selectedRowFrame: CGRect = self.picker.bounds.insetBy(dx: 0.0, dy: (self.picker.frame.height - rowHeight) / 2.0 )
            let userTappedOnSelectedRow = (selectedRowFrame.contains(sender.location(in: picker)))
            if (userTappedOnSelectedRow) {
                let selectedRow = self.picker.selectedRow(inComponent: 0)
                self.text = self.titles[selectedRow] as? String
            }
        }
    }
}
