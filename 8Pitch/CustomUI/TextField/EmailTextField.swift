//
//  EmailTextField.swift
//  8Pitch
//
//  Created by 8pitch on 16.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class EmailTextField : TextField {
    
    override var customLeftView: UIView? {
        let imageView = UIImageView(image: UIImage(.mail))
        imageView.contentMode = .scaleAspectFit
        let view = UIView()
        view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        view.snp.makeConstraints {
            $0.height.width.equalTo(Constants.viewSideSize + 10)
        }
        
        return view
    }
    
}
