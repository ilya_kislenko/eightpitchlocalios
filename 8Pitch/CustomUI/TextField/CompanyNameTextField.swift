//
//  CompanyNameTextField.swift
//  8Pitch
//
//  Created by 8pitch on 27.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class CompanyNameTextField : PasswordTextField {
    
    override var normalImage : UIImage {
        UIImage(.search)
    }
    
    override var selectedImage : UIImage {
        UIImage(.search)
    }
    
    override var action : Selector? {
        nil
    }
    
}
