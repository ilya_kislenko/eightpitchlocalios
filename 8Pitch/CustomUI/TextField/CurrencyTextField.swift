//
//  CurrencyTextField.swift
//  8Pitch
//
//  Created by 8pitch on 8/27/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class CurrencyTextField: UITextField {

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }

    func setup() {
        self.textColor = UIColor(.white)
        self.font = UIFont.barlow.regular.currencyInputSize
    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
            return false
    }
}
