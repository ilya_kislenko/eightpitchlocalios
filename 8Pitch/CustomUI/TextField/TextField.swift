//
//  TextField.swift
//  8Pitch
//
//  Created by 8pitch on 20.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift

class TextField : UITextField {
    
    enum State {
        case error
        case success
        case `default`
    }
    
    private var disposables : DisposeBag = .init()
    
    var underlineColor : BehaviorSubject<UIColor> = .init(value: .init(.separatorGray))
    
    lazy private var underline : UIView = {
        let retVal = UIView()
        self.addSubview(retVal)
        retVal.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.height.equalTo(Constants.underlineHeight)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
        return retVal
    }()
    
    override var placeholder: String? {
        didSet {
            self.setupPlaceholderFont()
        }
    }
    
    var customRightView : UIView? {
        return nil
    }
    
    var customLeftView : UIView? {
        return nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    func setup() {
        self.textContentType = .none
        self.textColor = UIColor(.gray)
        self.font = UIFont.roboto.light.placeholderSize
        self.underlineColor.subscribe(onNext: {
            self.underline.backgroundColor = $0
        }).disposed(by: self.disposables)
        self.rightViewMode = .always
        self.leftViewMode = .always
        self.rightView = self.customRightView
        self.leftView = self.customLeftView
        self.setupPlaceholderFont()
    }
    
    func setState(_ state: State) {
        switch state {
            case .success: fallthrough
            case .default: self.underlineColor.onNext(.init(.separatorGray))
            case .error: self.underlineColor.onNext(.init(.red))
        }
    }
    
    func setupPlaceholderFont() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byTruncatingTail
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor(.textGray),
                          NSAttributedString.Key.font : UIFont.roboto.light.placeholderSize,
                          .paragraphStyle: paragraphStyle]
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: attributes)
    }
    
    // MARK: Constants
    
    enum Constants {
        static let underlineHeight = 1.0
        static let viewSideSize: CGFloat = 24
    }
    
}
