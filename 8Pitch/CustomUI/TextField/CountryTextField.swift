//
//  CountryTextField.swift
//  8Pitch
//
//  Created by 8pitch on 07.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class CountryTextField: BirthdayTextField {
    
    private let picker = UIPickerView()
    
    override var normalImage : UIImage {
        UIImage(.select)
    }
    
    override var selectedImage : UIImage {
        UIImage(.select)
    }
    
    var titles: [Any] {
        DataSource.countries
    }
    
    override func setup() {
        super.setup()
        self.picker.dataSource = self
        self.picker.delegate = self
        self.inputView = picker
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.pickerTapped(sender:)))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        self.picker.addGestureRecognizer(tap)
    }
    
    var rowSelected: ((String) -> Void)?
    
}

// MARK: UIPickerViewDataSource, UIPickerViewDelegate

extension CountryTextField: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        self.titles.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        (self.titles[row] as? PickerData)?.name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.text = (self.titles[row] as? PickerData)?.name
        self.rowSelected?((self.titles[row] as? PickerData)?.code ?? "")
    }
    
}

extension CountryTextField: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
    
    @objc func pickerTapped(sender: UITapGestureRecognizer) {
        if (sender.state == .ended) {
            let rowHeight : CGFloat  = self.picker.rowSize(forComponent: 0).height
            let selectedRowFrame: CGRect = self.picker.bounds.insetBy(dx: 0.0, dy: (self.picker.frame.height - rowHeight) / 2.0 )
            let userTappedOnSelectedRow = (selectedRowFrame.contains(sender.location(in: picker)))
            if (userTappedOnSelectedRow) {
                let selectedRow = self.picker.selectedRow(inComponent: 0)
                self.text = (self.titles[selectedRow] as? PickerData)?.name
                self.rowSelected?((self.titles[selectedRow] as? PickerData)?.code ?? "")
            }
        }
    }
}

