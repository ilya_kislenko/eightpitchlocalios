//
//  BirthdayTextField.swift
//  8Pitch
//
//  Created by 8pitch on 28.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class BirthdayTextField: PasswordTextField, UITextFieldDelegate {
    
    override var normalImage : UIImage {
        UIImage(.calendar)
    }
    
    override var selectedImage : UIImage {
        UIImage(.calendar)
    }
    
    override var action : Selector? {
        #selector(checkEditing)
    }
    
    @objc private func checkEditing() {
        if self.isEditing {
            self.endEditing(true)
        } else {
            self.becomeFirstResponder()
        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
            return false
    }
    
    override func setup() {
        super.setup()
        let datePicker = UIDatePicker()
        let calendar = Calendar(identifier: .gregorian)
        datePicker.date = calendar.date(byAdding: .year, value: -18, to: Date()) ?? Date()
        datePicker.maximumDate = Date()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .touchUpInside)
        self.inputView = datePicker
    }
    
    // Methods
    
    @objc private func datePickerValueChanged(sender: UIDatePicker) {
        let formatter = DateHelper.shared.uiFormatterShort
        self.text = formatter.string(from: sender.date)
        sender.setDate(sender.date, animated: true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        false
    }
    
}
