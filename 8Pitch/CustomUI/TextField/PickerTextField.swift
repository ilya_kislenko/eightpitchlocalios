//
//  PickerTextField.swift
//  8Pitch
//
//  Created by 8pitch on 27.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift

class PickerTextField : TapTextField {
    
    override var customRightView: UIView? {
        let imageView = UIImageView(image: UIImage(.select))
        let view = UIView()
        self.inputView = UIView()
        view.addSubview(imageView)
        view.snp.makeConstraints {
            $0.height.width.equalTo(Constants.viewSideSize)
        }
        imageView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        return view
    }

    override func addGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
      if gestureRecognizer.isKind(of: UILongPressGestureRecognizer.self) {
               gestureRecognizer.isEnabled = false
      }
     return super.addGestureRecognizer(gestureRecognizer)
    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
