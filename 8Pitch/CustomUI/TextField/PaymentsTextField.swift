//
//  PaymentsTextField.swift
//  8Pitch
//
//  Created by 8pitch on 07.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift

class PaymentsTextField: PasswordTextField {
    
    override func setup() {
        super.setup()
        self.underlineColor.onNext(.init(.gray))
    }

    override var normalImage : UIImage {
        UIImage(.copy)
    }
    
    override var selectedImage : UIImage {
        UIImage(.copy)
    }
    
    override var action : Selector? {
        #selector(copyText)
    }
    
    @objc private func copyText() {
        UIPasteboard.general.string = self.text
    }

}
