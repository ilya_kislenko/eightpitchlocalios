//
//  CircleButton.swift
//  8Pitch
//
//  Created by 8pitch on 8/27/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

final class CircleButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.clipsToBounds = true
        self.layer.cornerRadius = frame.height / 2
    }
}
