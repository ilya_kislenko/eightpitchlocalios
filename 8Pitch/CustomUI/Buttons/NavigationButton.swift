//
//  NavigationButton.swift
//  8Pitch
//
//  Created by 8pitch on 21.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class NavigationButton: Button {
    
    override func setup() {
        super.setup()
        self.setupStates()
        self.titleLabel?.font = UIFont.roboto.light.placeholderSize
    }
}

private extension NavigationButton {
    
    func setupStates() {
        self.tintColor = UIColor(.red)
        self.setTitle("navigationButton.next".localized, for: .normal)
        self.setTitleColor(UIColor(.red), for: .normal)
        self.contentHorizontalAlignment = .left
        self.contentVerticalAlignment = .center
        self.setBackgroundImage(UIImage(.normal), for: .normal)
        self.setBackgroundImage(UIImage(.disabled), for: .disabled)
        self.setBackgroundImage(UIImage(.pressed), for: .selected)
        self.setBackgroundImage(UIImage(.focused), for: .highlighted)
        self.setBackgroundImage(UIImage(.pressed), for: UIControl.State.selected.union(.highlighted))
    }
    
}
