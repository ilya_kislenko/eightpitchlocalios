//
//  PinButton.swift
//  8Pitch
//
//  Created by 8pitch on 1/24/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class PinButton: Button {
    
    override func setup() {
        super.setup()
        self.setAttributes()
    }
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
        self.setAttributes()
    }
    
    func setAttributes() {
        guard let title = self.title(for: .normal) else { return }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        paragraphStyle.alignment = self.titleLabel?.textAlignment ?? .center
        let attributedTitle = NSAttributedString(string: title,
                                                 attributes: [.kern: 0.3, .paragraphStyle : paragraphStyle, .font: self.titleLabel?.font,
                                                              .foregroundColor: self.titleColor(for: .normal)])
        self.setAttributedTitle(attributedTitle, for: .normal)
    }
}

