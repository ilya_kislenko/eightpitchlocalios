//
//  CheckmarkButton.swift
//  8Pitch
//
//  Created by 8pitch on 8/28/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class CheckmarkBlackButton: Button {

    override func setup() {
        self.backgroundColor = .clear
        self.tintColor = UIColor(.gray)
        self.setBackgroundImage(UIImage(.checkOff), for: .normal)
        self.setBackgroundImage(UIImage(.checkOn), for: .selected)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.snp.makeConstraints {
            $0.height.width.equalTo(Constants.size)
        }
    }
    
    private enum Constants {
        static let size: CGFloat = 16
    }

}
