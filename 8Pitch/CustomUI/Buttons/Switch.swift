//
//  Switch.swift
//  8Pitch
//
//  Created by 8pitch on 10/1/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class Switch: UISwitch {
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    func setup() {
        self.onTintColor = UIColor(.red)
        self.backgroundColor = UIColor(.switchGray)
        self.layer.cornerRadius = self.bounds.height / 2
        self.clipsToBounds = true
    }
    
}
