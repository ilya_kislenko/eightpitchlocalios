//
//  CheckmarkButton.swift
//  8Pitch
//
//  Created by 8pitch on 8/28/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class CheckmarkButton: Button {

    override func setup() {
        super.setup()
        self.backgroundColor = .clear
        self.tintColor = UIColor(.white)
        self.setBackgroundImage(UIImage(.checkOffWhite), for: .normal)
        self.setBackgroundImage(UIImage(.checkOnWhite), for: .selected)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.snp.makeConstraints {
            $0.height.width.equalTo(Constants.size)
        }
    }
    
    private enum Constants {
        static let size: CGFloat = 24
    }

}
