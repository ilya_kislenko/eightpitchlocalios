//
//  SuggestedAmountButton.swift
//  8Pitch
//
//  Created by 8pitch on 8/27/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class SuggestedAmountButton: UIButton {

    public override init(frame: CGRect) {
        super.init(frame: frame)

        self.setup()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)

        self.setup()
    }

    func setup() {
        self.titleLabel?.font = UIFont.roboto.regular.labelSize
        self.layer.cornerRadius = 4
        self.setTitleColor(UIColor(Color.white), for: .normal)
        self.backgroundColor = UIColor.init(white: 1, alpha: 0.1)
    }
}
