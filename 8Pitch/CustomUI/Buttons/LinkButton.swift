//
//  LinkButton.swift
//  8Pitch
//
//  Created by 8pitch on 9/3/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class LinkButton: Button {
    
    public var url: URL?
    
    override func setup() {
        self.titleLabel?.font = UIFont.barlow.regular.titleSize
        self.titleLabel?.textColor = UIColor(.gray)
        self.titleLabel?.textAlignment = .left
        self.backgroundColor = UIColor(.white)
    }

}
