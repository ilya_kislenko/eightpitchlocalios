//
//  RoundedButton.swift
//  8Pitch_LOG
//
//  Created by 8pitch on 14.07.2020.
//  Copyright © 2020 8pitch. All rights reserved.
//

import UIKit

class RoundedButton: Button {
    
    override func setup() {
        super.setup()
        self.titleLabel?.font = UIFont.barlow.medium.placeholderSize
        self.setAttributes()
        self.layer.cornerRadius = Constants.cornerRadius
        self.addShadow()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.snp.makeConstraints {
            $0.height.equalTo(Constants.height)
        }
    }
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
        self.setAttributes()
    }
    
    func setAttributes() {
        guard let title = self.title(for: .normal) else { return }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.13
        let attributedTitle = NSAttributedString(string: title, attributes: [.kern: 1.6, .paragraphStyle : paragraphStyle, .foregroundColor: self.titleColor(for: .normal)])
        self.setAttributedTitle(attributedTitle, for: .normal)
    }
}

private extension RoundedButton {
    
    func addShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: Constants.shadowWidth, height: Constants.shadowHeight)
        self.layer.shadowColor = Constants.shadowColor
        self.layer.shadowRadius = Constants.shadowRadius
        self.layer.shadowOpacity = Constants.shadowOpacity
    }
    
    enum Constants {
        static let cornerRadius: CGFloat = 4
        static let height: CGFloat = 56
        static let shadowWidth: CGFloat = 0
        static let shadowHeight: CGFloat = 6
        static let shadowRadius: CGFloat = 6
        static let shadowOpacity: Float = 1
        static let shadowColor = UIColor(hex: "222932", alpha: 0.15).cgColor
    }
    
}

