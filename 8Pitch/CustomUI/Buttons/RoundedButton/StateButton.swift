//
//  StateButton.swift
//  8Pitch
//
//  Created by 8pitch on 11.11.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation
import UIKit

class StateButton: RoundedButton {
    
    override var isEnabled: Bool {
        didSet {
            self.backgroundColor = isEnabled ? UIColor(.red) : UIColor(.stateButtonGrayColor)
        }
    }
    
    override func setup() {
        super.setup()
        self.isEnabled = false
        self.backgroundColor = isEnabled ? UIColor(.red) : UIColor(.stateButtonGrayColor)
    }
    
}
