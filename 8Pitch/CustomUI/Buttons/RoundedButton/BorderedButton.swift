//
//  RoundedShadowButton.swift
//  8Pitch_LOG
//
//  Created by 8pitch on 14.07.2020.
//  Copyright © 2020 8pitch. All rights reserved.
//

import UIKit

class BorderedButton: RoundedButton {
    
    override func setup() {
        super.setup()
        self.addBorder()
    }
    
}

private extension BorderedButton {
    
    func addBorder() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(.borderWhite).cgColor
    }
    
}
