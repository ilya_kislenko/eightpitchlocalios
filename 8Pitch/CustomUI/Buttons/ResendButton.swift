//
//  ResendButton.swift
//  8Pitch
//
//  Created by 8pitch on 19.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ResendButton: Button {
    
    enum ButtonType: String {
        case sms
        case email
    }
    
    // MARK: UI Components
    
    private var countLabel = ResendLabel()
    
    // MARK: Overrides
    
    override func setup() {
        self.setupContent()
    }
    
    // MARK: Methods
    
    func startCountdown() {
        (1...Constants.maxSecond).reversed().tick(by: 1) { [weak self] in
            guard let currentTick = $0 else {
                self?.isEnabled = true
                self?.countLabel.text = nil
                self?.countLabel.isHidden = true
                self?.imageView?.tintColor = UIColor(.gray)
                return
            }
            self?.countLabel.text = "\(currentTick)"
        }
        self.isEnabled = false
        self.countLabel.isHidden = false
        self.imageView?.tintColor = .clear
    }
    
    func setupForType(_ type: ButtonType, accConfirmation: Bool = false) {
        let titleCode = UIScreen.main.bounds.width < Constants.minimumWidth && accConfirmation ? "resendButton.title.\(type.rawValue).transfer" : "resendButton.title.\(type.rawValue)"
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        let attributedTitle = NSAttributedString(string: titleCode.localized, attributes: [.kern: 0.3, .paragraphStyle : paragraphStyle, .foregroundColor: self.titleColor(for: .normal)])
        self.setAttributedTitle(attributedTitle, for: .normal)
        self.countLabel.isHidden = true
        self.addSubview(self.countLabel)
        self.countLabel.snp.makeConstraints {
            $0.centerY.leading.equalToSuperview()
            $0.width.equalTo(Constants.countLabelInset)
            $0.height.equalTo(Constants.countLabelHeight)
        }
    }
    
}

// MARK: Private

private extension ResendButton {
    
    enum Constants {
        static let countLabelInset: CGFloat = 17
        static let countLabelHeight: CGFloat = 13
        static let maxSecond: Int = 31
        static let minimumWidth = CGFloat(376)
    }
    
    func setupContent() {
        self.titleLabel?.numberOfLines = 0
        self.setTitleColor(UIColor(.red), for: .normal)
        self.titleLabel?.font = UIFont.barlow.semibold.titleSize
        self.setImage(UIImage(.resend), for: .normal)
        self.imageView?.tintColor = UIColor(.gray)
        self.contentVerticalAlignment = .center
    }
    
}
