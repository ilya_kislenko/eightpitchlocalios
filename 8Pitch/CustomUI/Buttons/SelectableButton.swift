//
//  SelectableButton.swift
//  8Pitch
//
//  Created by 8pitch on 8/27/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class SelectableButton: UIButton {
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.backgroundColor = UIColor.clear
            } else {
                self.backgroundColor = UIColor(Color.backgroundDark)
            }
        }
    }
        
    // MARK: Init

    public override init(frame: CGRect) {
        super.init(frame: frame)

        self.setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        self.setup()
    }
    
    override func setAttributedTitle(_ title: NSAttributedString?, for state: UIControl.State) {
        let title = NSMutableAttributedString(attributedString: title ?? NSAttributedString())
        title.addAttributes([.foregroundColor: self.titleColor(for: state)],
                            range: NSMakeRange(0, title.length))
        super.setAttributedTitle(title, for: state)
    }
}

private extension SelectableButton {
    func setup() {
        self.backgroundColor = UIColor(Color.backgroundDark)
        self.tintColor = UIColor.clear
        self.setTitleColor(UIColor(Color.halfWhite), for: .normal)
        self.setTitleColor(UIColor(Color.white), for: .selected)
        self.titleLabel?.font = UIFont.barlow.semibold.selectableBarSize
    }
}
