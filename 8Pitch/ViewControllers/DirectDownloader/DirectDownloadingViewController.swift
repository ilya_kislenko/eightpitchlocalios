//
//  DirectDownloadingViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/28/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import ZIPFoundation

class DirectDownloadingViewController: DarkViewController {
    
    // MARK: Properties
    
    private lazy var viewModel : DirectDownloadingViewModel = {
        let retVal = DirectDownloadingViewModel(session: session)
        retVal.errorHandler = { [weak self] error in
            DispatchQueue.main.async {
                guard let error = error else { return }
                self?.presentAlert(message: error.localizedDescription)
            }
        }
        return retVal
    }()
    
    private var progress: Float = 0
    private var tasksInProgress: Int = 0 {
        didSet {
            self.setTitle()
        }
    }
    
    // MARK: UI Components:
    @IBOutlet var subtitleLabel: CustomSubtitleDarkLabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var confirmationView: ConfirmationView!
    @IBOutlet weak var successIndicator: UIImageView!
    private var browseButton: BorderedButton = {
        let button = BorderedButton()
        button.setTitle("investment.directDownloader.browse".localized, for: .normal)
        button.setTitleColor(UIColor(.white), for: .normal)
        return button
    }()
    
    @IBOutlet weak var confirmationViewOffset: NSLayoutConstraint!
    
    // MARK: Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.confirmationView.setupWith(self.session?.projectsProvider.expandedProject?.documents.map { $0?.originalFileName} ?? [])
        self.confirmationView.delegate = self
        self.loadDocuments()
        self.tasksInProgress = self.session?.projectsProvider.expandedProject?.documents.count ?? 0
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeButtonTapped))
        self.progressView.layoutIfNeeded()
        self.addBrowseButton()
        self.setupTextView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.progressView.layer.cornerRadius = self.progressView.bounds.height / 2
        self.progressView.clipsToBounds = true
    }
    
}

// MARK: InvestmentDocumentsDelegate

extension DirectDownloadingViewController: InvestmentDocumentsDelegate {
    
    func nextButtonAction() {
        _ = ViewControllerProvider.setupInvestmentConfirmationViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}

private extension DirectDownloadingViewController {
    
    func setTitle() {
        DispatchQueue.main.async { [weak self] in
            self?.subtitleLabel.text = String(format: "investment.directDownloader.title".localized, "\(self?.tasksInProgress ?? 0)")
        }
    }
    
    @objc func loadDocuments() {
        guard let session = self.session else {
            return
        }
        self.viewModel.loadDocuments(session: session, completionHandler: { [weak self] error in
            self?.handleSessionFinish(error)
        })
    }
    
    enum Constants {
        static let animateDuration: Double = 1
        static let buttonSideInset: CGFloat = 24
        static let buttonTopOffset: CGFloat = 20
        static let buttonBottomOffset: CGFloat = -30
    }
    
    @objc func closeButtonTapped() {
        self.popToPreviousController(animated: true)
    }
    
    @objc func browseButtonTapped() {
        self.viewModel.createArchive()
        guard let url = self.viewModel.zipURL else { return }
        let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        self.present(activityViewController, animated: true)
    }
    
    func downloadingFinished() {
        self.progressView.setProgress(1, animated: true)
        UIView.animate(withDuration: Constants.animateDuration, animations: {
            self.textView.isHidden = true
            self.successIndicator.isHidden = false
            self.browseButton.isHidden = false
            self.confirmationViewOffset.priority = .defaultLow
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
        })
    }
    
    func setupTextView() {
        let text = NSMutableAttributedString(string: "investment.directDownloader.info.1".localized, attributes: [.foregroundColor: UIColor(.white), .font : UIFont.roboto.regular.labelSize])
        let link = NSAttributedString(string: "investment.directDownloader.info.2".localized + "investment.directDownloader.info.3".localized, attributes: [.foregroundColor: UIColor(.red), .font : UIFont.roboto.regular.labelSize])
        text.append(link)
        self.textView.attributedText = text
        self.textView.textContainer.maximumNumberOfLines = 2
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapResponse(recognizer:)))
        self.textView.addGestureRecognizer(tap)
    }
    
    @objc func tapResponse(recognizer: UITapGestureRecognizer) {
        let location: CGPoint = recognizer.location(in: textView)
        let position: CGPoint = CGPoint(x: location.x, y: location.y)
        guard let tapPosition: UITextPosition = textView.closestPosition(to: position),
              let textRange: UITextRange = textView.tokenizer.rangeEnclosingPosition(tapPosition, with: UITextGranularity.word, inDirection: UITextDirection(rawValue: 1)) else {return}
        let tappedWord: String = textView.text(in: textRange) ?? ""
        if tappedWord == "investment.directDownloader.info.2".localized || tappedWord == "investment.directDownloader.info.3".localized {
            self.loadDocuments()
        }
    }
    
    func addBrowseButton() {
        self.browseButton.addTarget(self, action: #selector(self.browseButtonTapped), for: .touchUpInside)
        self.contentView.addSubview(self.browseButton)
        self.browseButton.translatesAutoresizingMaskIntoConstraints = false
        self.browseButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(Constants.buttonSideInset)
            $0.top.equalTo(self.progressView.snp.bottom).offset(Constants.buttonTopOffset)
            $0.bottom.equalTo(self.confirmationView.snp.top).offset(Constants.buttonBottomOffset)
        }
        self.browseButton.isHidden = true
    }
    
}

// MARK: Networking

private extension DirectDownloadingViewController {
    
    func handleSessionFinish(_ error: Error?) {
        guard let error = error else {
            self.tasksInProgress -= 1
            DispatchQueue.main.async { [weak self] in
                if self?.progressView.progress == 1 || self?.tasksInProgress == 0 {
                    self?.downloadingFinished()
                }
            }
            return
        }
        DispatchQueue.main.async {
            self.presentAlert(message: error.localizedDescription)
        }
    }
}
