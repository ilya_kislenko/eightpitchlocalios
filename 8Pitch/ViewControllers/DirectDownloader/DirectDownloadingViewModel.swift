//
//  DirectDownloadingViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 8/28/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Remote
import Input
import ZIPFoundation

class DirectDownloadingViewModel: GenericViewModel {
    
    private var filePathes: [URL] = []
    
    var errorHandler: ErrorClosure?
    
    var zipURL: URL?
    
    func loadDocuments(session: Session,
                       completionHandler: @escaping ErrorClosure) {
        self.remote.refreshToken(self.session)
            .compactMap { $0 }
            .subscribe(onNext: { [weak self] in
                switch $0 {
                case .success(_):
                    let ids = self?.session.projectsProvider.expandedProject?.documents.map { $0?.uniqueFileName }
                    ids?.forEach {
                        guard let id = $0 else {
                            completionHandler(GenericRequestError.invalidResponse(nil))
                            return
                        }
                        self?.document(id: id, completionHandler: completionHandler)
                    }
                case .failure(let error):
                    completionHandler(error)
                }
            }).disposed(by: self.disposables)
    }
    
    private func document(id: String, completionHandler: @escaping ErrorClosure) {
        self.remote.documents(session, id: id, completionHandler: { result in
            switch result {
            case .failure(let error):
                completionHandler(error)
            case .success(let data):
                self.saveFile(id: id, data: data, completionHandler: {_,_ in
                    guard let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
                          let fileName = self.fileName(id: id) else { return }
                    let source = documentsPath.appendingPathComponent(fileName)
                    self.filePathes.append(source)
                    completionHandler(nil)
                })
            }
        })
    }
    
    func createArchive() {
        //TODO: document directory would be a nice fit inside of a FileManager extension
        guard let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first, let id = self.session.projectsProvider.currentInvestmentID else { return }
        
        let zip = documentsPath.appendingPathComponent("8pitch-\(id).zip")
        try? FileManager.default.removeItem(at: zip)
        
        self.filePathes.forEach { path in
            do {
                if self.zipURL != nil, let archive = Archive(url: self.zipURL!, accessMode: .update) {
                    try archive.addEntry(with: path.lastPathComponent, relativeTo: path.deletingLastPathComponent(), compressionMethod: .deflate)
                } else {
                    try FileManager.default.zipItem(at: path, to: zip, shouldKeepParent: false, compressionMethod: .deflate)
                    self.zipURL = zip
                }
            } catch {
                self.errorHandler?(error)
            }
        }
    }
    
    private func fileName(id: String) -> String? {
        let files = self.session.projectsProvider.expandedProject?.documents
        let currentFile = files?.first(where: { $0?.uniqueFileName == id } )
        return currentFile??.originalFileName
    }
    
}
