//
//  InvestmentConfirmationViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/31/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input
import Remote

class InvestmentConfirmationViewController: SMSViewController {
    
    @IBOutlet var descriptionLabel: InfoCurrencyLabel!
    
    override var confirmationFooterViewType : PublishSubject<InputSMSView.ViewType?>? {
        didSet {
            guard let typeAction = self.confirmationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "investment.confirmation".localized
        self.customTitle = "investment.confirmation.subtitle".localized
        self.descriptionLabel.text = "investment.confirmation.description".localized
        self.navigationItem.rightBarButtonItem = nil
        self.sendCode()
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CodeTextCell.nibForRegistration, forCellWithReuseIdentifier: CodeTextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = InputSMSView.reuseIdentifier
        layout.register(InputSMSView.nibForRegistration, forDecorationViewOfKind: InputSMSView.reuseIdentifier)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.resendButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
    override func verifyToken() {
        super.verifyToken()
        guard let session = self.session, let model = try? self.model?.value() else {
            return
        }
        self.remote.verifyInvestmentToken(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                    case .success(_):
                        self?.nextButtonAction?.onNext(.next)
                    case .failure(let error):
                        if let error = error as? VerifyTokenError, error == VerifyTokenError.canceled {
                            self?.presentAlert(message: error.localizedDescription, completion: { [weak self] in
                                self?.navigationController?.popToRootViewController(animated: true)
                            })
                        }
                        self?.presentAlert(message: error.localizedDescription)
                }
            }).disposed(by: self.disposables)
    }
    
    override func nextButtonTapped() {
        let paymentMethodsViewController = ViewControllerProvider.setupPaymentMethodsViewController(rootViewController: self)
        paymentMethodsViewController?.hidesBottomBarWhenPushed = true
        self.nextController = paymentMethodsViewController
        self.pushNextController(animated: true)
    }
    
    override func sendCode() {
        super.sendCode()
        guard let session = self.session else {
            return
        }
        self.startProgress()
        self.remote.sendInvestmentCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] result in
                self?.stopProgress()
                switch result {
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                default:
                    return
                }
            }).disposed(by: self.disposables)
    }
    
    override func resendCode() {
        super.resendCode()
        guard let session = self.session else {
            return
        }
        self.remote.resendCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.stopProgress()
            }).disposed(by: self.disposables)
    }

}
