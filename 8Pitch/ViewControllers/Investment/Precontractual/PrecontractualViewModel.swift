//
//  PrecontractualViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 8/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import Remote

class PrecontractualViewModel: GenericViewModel {
    
    // Properties
    
    private var project: ExpandedProject?
    
    var needToOpenDocumentsScreen: Bool {
        !(self.session.projectsProvider.expandedProject?.documents.isEmpty ?? true)
    }
    
    let numberOfSections: Int = Constants.keys.count
    let headerTitles: [String] = Constants.headerTitles
    
    // MARK: Collection View Setup
    
    private var issuerValues: [String?] {
        if let project = self.project {
            return [project.companyName, project.financialInformation?.isin, project.financialInformation?.companyAddress]
        }
        return []
    }
    
    private var dsoValues: [String?] {
        if let project = self.project, let start = project.tokenParametersDocument?.dsoProjectStartDate, let finish = project.tokenParametersDocument?.dsoProjectFinishDate, let value = project.tokenParametersDocument?.nominalValue {
            let formatter = DateHelper.shared.uiFormatterShort
            return [project.financialInformation?.typeOfSecurity, project.financialInformation?.financingPurpose, "\(value.currency) EUR", formatter.string(from: start), formatter.string(from: finish)]
        }
        return []
    }
    
    private var values: [[String?]] {
        [issuerValues, dsoValues]
    }
    
    func doesRowHaveSeparator(_ indexPath: IndexPath) -> Bool {
        indexPath.row == self.numberOfRows(for: indexPath.section) - 1 ? false : true
    }
    
    func precontractualInfo(for indexPath: IndexPath) -> PrecontractualInfo {
        let keys = Constants.keys[indexPath.section]
        let values = self.values[indexPath.section]
        let text = indexPath.row < values.count ? values[indexPath.row] : ""
        return PrecontractualInfo(title: keys[indexPath.row], text: text)
    }
    
    func numberOfRows(for section: Int) -> Int {
        Constants.keys[section].count
    }
    
    // MARK: Networking
    
    func projectInfo(session: Session?, completionHandler: @escaping ProjectHandler) {
        //TODO: rename to projectID
        guard let session = session, let projectId = session.projectsProvider.currentProject?.identifier else {
            completionHandler(nil, nil)
            return
        }
        self.remote.project(session, projectId) { project, error in
            self.project = project
            completionHandler(project, error)
        }
    }
    
    func bookInvestment(session: Session, projectId: Int, completionHandler: @escaping BookHandler) {
        self.remote.bookInvestment(session, projectId) { id, error in
            DispatchQueue.main.async {
            completionHandler(id, error)
            }
        }
    }
    
}

// MARK: Private

private extension PrecontractualViewModel {
    
    enum Constants {
        static let issuerKeys = ["investment.preconstructual.name".localized,
                                 "investment.preconstructual.isin".localized,
                                 "investment.preconstructual.address".localized]
        static let dsoKeys = ["investment.preconstructual.security".localized,
                              "investment.preconstructual.purpose".localized,
                              "investment.preconstructual.token".localized,
                              "investment.preconstructual.start".localized,
                              "investment.preconstructual.end".localized]
        static let keys = [issuerKeys, dsoKeys]
        static let headerTitles = ["investment.preconstructual.info.issuer".localized,
                                   "investment.preconstructual.info.dso".localized]
        
    }
    
}
