//
//  PreconstructualViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Remote

class PrecontractualViewController: CustomTitleViewController {
    
    // MARK: Properties
    
    private lazy var viewModel : PrecontractualViewModel = {
        PrecontractualViewModel(session: session)
    }()
    
    private var height: CGFloat?
    
    // MARK: UI Components
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getProjectDetails()
        self.setupNavigationBar()
        self.setupCollectionView()
    }
    
}

// MARK: UICollectionViewDelegate, UICollectionViewDataSource

extension PrecontractualViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        self.viewModel.numberOfSections
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.viewModel.numberOfRows(for: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PrecontractualCell.reuseIdentifier, for: indexPath) as? PrecontractualCell else { return UICollectionViewCell() }
        cell.setupFor(info: self.viewModel.precontractualInfo(for: indexPath))
        if !self.viewModel.doesRowHaveSeparator(indexPath) {
            cell.clearSeparator()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: InvestmentHeaderView.reuseIdentifier, for: indexPath) as? InvestmentHeaderView else {
                return UICollectionReusableView()
            }
            headerView.setTitle(self.viewModel.headerTitles[indexPath.section])
            headerView.addSeparator(indexPath.section == 1)
            return headerView
        } else if kind == UICollectionView.elementKindSectionFooter && indexPath.section == 1 {
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: InvestmentFooterView.reuseIdentifier, for: indexPath) as? InvestmentFooterView else {
                return UICollectionReusableView()
            }
            footerView.confirmAction = { [weak self] in
                self?.bookInvestment()
            }
            footerView.setupWithFiles(self.session?.projectsProvider.expandedProject?.documents ?? [], delegate: self)
            self.height = footerView.contentHeight
            return footerView
        }
        return UICollectionReusableView()
    }
    
}

// MARK: UICollectionViewDelegateFlowLayout

extension PrecontractualViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        CGSize(width: self.collectionView.bounds.width, height: Constants.headerHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if section == 1 {
            return CGSize(width: collectionView.frame.width, height: self.height ?? 558)
        } else {
            return .zero
        }
    }
}

// MARK: Private

private extension PrecontractualViewController {
    
    enum Constants {
        static let headerHeight: CGFloat = 88
        static let footerHeight: CGFloat = 400
        static let path: String = WebURLConstants.filePath
    }
    
    func bookInvestment() {
        guard let session = self.session, let projectId = session.projectsProvider.currentProject?.identifier else { return }
        self.startProgress()
        self.viewModel.bookInvestment(session: session, projectId: projectId) { [weak self] id, error in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
                return
            }
            self?.goToNextPage()
        }
    }
    
    func goToNextPage() {
        if self.viewModel.needToOpenDocumentsScreen {
            self.openDocumentsScreen()
        } else {
            self.openConfirmationScreen()
        }
    }
    
    func openDocumentsScreen() {
        let _ = ViewControllerProvider.setupGetDocumentsViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openConfirmationScreen() {
        let _ = ViewControllerProvider.setupInvestmentConfirmationViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func getProjectDetails() {
        self.startProgress()
        self.viewModel.projectInfo(session: self.session) { [weak self] project, error in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.collectionView.reloadData()
            }
        }
    }
    
    func setupCollectionView() {
        self.collectionView.register(PrecontractualCell.nibForRegistration, forCellWithReuseIdentifier: PrecontractualCell.reuseIdentifier)
        self.collectionView.register(InvestmentHeaderView.nibForRegistration, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: InvestmentHeaderView.reuseIdentifier)
        self.collectionView.register(InvestmentFooterView.nibForRegistration, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: InvestmentFooterView.reuseIdentifier)
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.estimatedItemSize = CGSize(width: 1, height: 1)
        }
    }
    
    func setupNavigationBar() {
        self.customTitle = "investment.preconstructual.customTitle".localized
        self.title = "investment.preconstructual.title".localized
        self.navigationItem.rightBarButtonItem = nil
        if let navigationBar = self.navigationController?.navigationBar as? CustomNavigationBar {
            navigationBar.setupAppearance()
        }
    }
}

extension PrecontractualViewController: UITextViewDelegate {
    func textView(_ textView: UITextView,
                  shouldInteractWith URL: URL,
                  in characterRange: NSRange,
                  interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString.hasPrefix(Constants.path) {
            self.downloadFile(id: URL.lastPathComponent)
            return false
        } else {
            return true
        }
    }
    
    private func downloadFile(id: String?) {
        guard let id = id else { return }
        self.startProgress()
        self.viewModel.downloadFile(id: id) { [weak self] url, error in
            DispatchQueue.main.async {
                self?.stopProgress()
                guard let error = error else {
                    let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
                    self?.present(activityViewController, animated: true)
                    return
                }
                self?.presentAlert(message: error.localizedDescription)
            }
        }
    }
}

