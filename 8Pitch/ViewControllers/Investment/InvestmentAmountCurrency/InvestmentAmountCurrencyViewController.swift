//
//  InvestmentAmountCurrencyViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote

fileprivate struct Constants {
    static let cornerRadius = CGFloat(8)
    static let maxLangthOfText = 14
    static let eurSymbol = "currency.eur.symbol".localized
    static let tokenSymbol = "currency.token.symbol".localized
    static let loverLimitHint = "investment.amount.eurInvestmentAmountErrorLowerLimit".localized
    static let upperLimitHint = "investment.amount.eurInvestmentAmountErrorUpperLimit".localized
    static let notMultipleHint = "investment.amount.eurInvestmentAmountErrorText".localized
    static let amountImageSize: CGFloat = 12
}

class InvestmentAmountCurrencyViewController: CustomTitleViewController {
    @IBOutlet var subtitleLabel: CustomSubtitleLabel!
    @IBOutlet var continueButton: StateButton!
    
    //MARK: INPUT
    @IBOutlet var inpuContainerView: UIView!
    @IBOutlet var switchEuroButton: SelectableButton!
    @IBOutlet var switchTokenButton: SelectableButton!
    
    @IBOutlet var subtractStepButton: CircleButton!
    @IBOutlet var addStepButton: CircleButton!
    
    @IBOutlet var insertAmountTextField: CurrencyTextField!
    @IBOutlet var currencySymbolImageView: UIImageView!
    @IBOutlet var convertedInsertedValueLabel: ConvertedLabel!
    
    @IBOutlet var errorContainerStackView: UIStackView!
    
    @IBOutlet var errorLabel: InvestmentErrorLabel!
    @IBOutlet var errorInfoLabel: InvestmentErrorInfoLabel!
    
    @IBOutlet var suggestedInvestSingleContainer: UIView!
    @IBOutlet var suggestedInvestSingleButton: SuggestedAmountButton!
    
    @IBOutlet var suggestedAmountStackView: UIStackView!
    @IBOutlet var suggestedAmountLessButton: SuggestedAmountButton!
    @IBOutlet var suggestedAmountMoreButton: SuggestedAmountButton!
    
    //MARK: Description
    @IBOutlet var amountDescriptionTitle: TitleSumLabel!
    @IBOutlet var amountDescriptionSubtitle: SumLabel!
    @IBOutlet var minimalDescriptionTitle: TitleSumLabel!
    @IBOutlet var minimalDescriptionSubtitle: SumLabel!
    @IBOutlet var feeDescriptionTitle: TitleSumLabel!
    @IBOutlet var feeDescriptionSubtitle: SumLabel!
    @IBOutlet var classDescriptionTitle: TitleSumLabel!
    @IBOutlet var classDescriptionSubtitle: SumLabel!
    @IBOutlet weak var classDescriptionView: UIView!
    
    //MARK: Total
    @IBOutlet var totalTitleLabel: TotalSumLabel!
    @IBOutlet var totalAmountLabel: TotalSumLabel!
    @IBOutlet var totalAmountConvertedLabel: TitleSumLabel!
    
    private lazy var viewModel : InvestmentAmountCurrencyViewModel = {
        InvestmentAmountCurrencyViewModel(session: session)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.insertAmountTextField.delegate = self
        self.insertAmountTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.loadData()
        self.setupViews()
        self.setupCorners()
        self.setupKeyboardGesture()
        self.setupInitiallContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateNavigationBar()
        self.setupSelection(selectedCurrency: viewModel.selectedCurrency)
        self.setupTitles()
    }
    
    @objc func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func switchEuroAction(_ sender: Any) {
        self.handleEuroSelection()
    }
    
    @IBAction func switchTokenAction(_ sender: Any) {
        let selectedTabName = self.createAttributedTitle(for: self.viewModel.tokenName + "investment.amount.tokenTab".localized, currency: .bghToken, selected: true)
        self.switchTokenButton.setAttributedTitle(selectedTabName, for: .selected)
        self.handleTokenSelection()
    }
    
    @IBAction func subtractAction(_ sender: Any) {
        self.viewModel.stepDownCurrentAmount()
        self.insertAmountTextField.text = self.viewModel.currentAmount.decimalValue.currency
        self.textFieldDidChange(self.insertAmountTextField)
    }
    
    @IBAction func addAction(_ sender: Any) {
        self.viewModel.stepUpCurrentAmount()
        self.insertAmountTextField.text = self.viewModel.currentAmount.decimalValue.currency
        self.textFieldDidChange(self.insertAmountTextField)
    }
    
    @IBAction func suggestedInvesttSingleAction(_ sender: Any) {
        if let value = self.viewModel.validate().first {
            self.insertAmountTextField.text = value.decimalValue.currency
            self.textFieldDidChange(self.insertAmountTextField)
        }
    }
    
    @IBAction func suggestedAmountLessAction(_ sender: Any) {
        if let value = self.viewModel.validate().first {
            self.insertAmountTextField.text = value.decimalValue.currency
            self.textFieldDidChange(self.insertAmountTextField)
        }
    }
    
    @IBAction func suggestedAmountMoreAction(_ sender: Any) {
        if let value = self.viewModel.validate().last {
            self.insertAmountTextField.text = value.decimalValue.currency
            self.textFieldDidChange(self.insertAmountTextField)
        }
    }
    
    @IBAction func continueAction(_ sender: Any) {
        let validationResult = self.viewModel.validate()
        if validationResult.isEmpty {
            self.confirmInvestmentAmountAndContinue()
        } else {
            self.handleValidationResult(validationResult)
        }
    }
    
    private func confirmInvestmentAmountAndContinue() {
        self.viewModel.saveResult()
        self.openNextScreen()
    }
    
    private func loadData() {
        guard let model = try? self.session?.userProvider.value() else { return }
        self.startProgress()
        self.viewModel.user() { response in
            switch response {
            case .success( let result):
                if let prefix = result.userName.prefix,
                   let lastName = result.userName.lastName {
                    self.subtitleLabel.text = "\(prefix) \(lastName)\("investment.amount.subtitle".localized)"
                }
            case .failure(let error):
                self.presentAlert(message: error.localizedDescription)
                model.isValid.onNext(.failure(GenericRequestError.invalidResponse(error)))
            }
            self.stopProgress()
        }
    }
}

extension InvestmentAmountCurrencyViewController {
    private func setupKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
}

// MARK: setup Appearance
private extension InvestmentAmountCurrencyViewController {
    func updateNavigationBar() {
        self.title = "investment.amount.title".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.barlow.regular.titleSize, NSAttributedString.Key.foregroundColor: UIColor(.gray)]
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.arrowBack), style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.info), style: .plain, target: self, action: #selector(self.openInfo))
        self.navigationItem.customizeForWhite()
    }
    
    @objc func openInfo() {
        let _ = ViewControllerProvider.setupInfoViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func setupViews() {
        self.errorContainerStackView.isHidden = true
        self.continueButton.isEnabled = true
        self.errorLabel.isHidden = true
        self.errorInfoLabel.isHidden = true
        self.suggestedInvestSingleContainer.isHidden = true
        self.suggestedAmountStackView.isHidden = true
    }
    
    func setupCorners() {
        self.inpuContainerView.layer.cornerRadius = Constants.cornerRadius
        self.inpuContainerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    func setupTitles() {
        self.switchEuroButton.setTitle("investment.amount.euroTab".localized, for: .normal)
        let tabName = self.createAttributedTitle(for: self.viewModel.tokenName + "investment.amount.tokenTab".localized, currency: .bghToken)
        self.switchTokenButton.setAttributedTitle(tabName, for: .normal)
        self.errorLabel.text = "investment.amount.fieldIsRequired".localized
        self.amountDescriptionTitle.text = "investment.amount.step".localized
        self.amountDescriptionSubtitle.text = "\(self.viewModel.investmentStepEuro.decimalValue.currency) \("currency.eur".localized)"
        self.minimalDescriptionTitle.text = "investment.amount.minimalAmount".localized
        self.minimalDescriptionSubtitle.text = "\(self.viewModel.minimalValueEuro.decimalValue.currency) \("currency.eur".localized)"
        if let limit = self.viewModel.investorClassLimit?.decimalValue.currency {
            self.classDescriptionTitle.text = "investment.amount.classLimit".localized
            self.classDescriptionSubtitle.text = "\(limit) \("currency.eur".localized)"
        } else {
            self.classDescriptionView.isHidden = true
        }
        self.totalTitleLabel.text = "investment.amount.total".localized
    }
    
    @objc func handleEuroSelection() {
        self.setupSelection(selectedCurrency: .euro)
    }
    
    @objc func handleTokenSelection() {
        self.setupSelection(selectedCurrency: .bghToken)
    }
    
    func setupInitiallContent() {
        self.insertAmountTextField.text = self.viewModel.currentBGHAmount.decimalValue.currency
    }
    
    func setupSelection(selectedCurrency: CurrencyType) {
        viewModel.updateSelectedCurrency(selectedCurrency)
        switch selectedCurrency {
        case .bghToken:
            self.switchTokenButton.isSelected = true
            self.switchEuroButton.isSelected = false
            self.insertAmountTextField.text = self.viewModel.currentBGHAmount.decimalValue.currency
            
        case .euro:
            self.switchEuroButton.isSelected = true
            self.switchTokenButton.isSelected = false
            self.insertAmountTextField.text = self.viewModel.currentEurAmount.decimalValue.currency
            
        }
        
        self.textFieldDidChange(self.insertAmountTextField)
    }
    
    func convertValuesContent(selectedCurrency: CurrencyType) {
        let fee = self.viewModel.feeInEur
        self.feeDescriptionTitle.text = fee.isEqual(to: 0) ? "payment.transfer.nominal".localized : "\("investment.amount.fee".localized)\(self.viewModel.feeRateInPersants.decimalValue.currency)%:"
        self.feeDescriptionSubtitle.text = fee.isEqual(to: 0) ? "\(self.viewModel.tokenNominalValue.doubleValue.currency) \("currency.eur".localized)" : "\(fee.doubleValue.currency) \("currency.eur".localized)"
        
        switch selectedCurrency {
        case .bghToken:
            self.currencySymbolImageView.image = UIImage(.tokenNew)
            self.convertedInsertedValueLabel.text = "\(self.viewModel.currentEurAmount.decimalValue.currency) \(Constants.eurSymbol)"
        case .euro:
            self.currencySymbolImageView.image = UIImage(.amountInEuro)
            self.convertedInsertedValueLabel.text = "\(self.viewModel.currentBGHAmount.decimalValue.currency) \(self.viewModel.tokenName)"
        }
        
        self.totalAmountLabel.text = "\(viewModel.currentEurAmount.adding(viewModel.feeInEur).doubleValue.currency) \("currency.eur".localized)"
        self.totalAmountConvertedLabel.text = "\("investment.amount.totalShares".localized) \(viewModel.currentBGHAmount.decimalValue.currency) \(viewModel.tokenName)"
    }
    
    func handleValidationResult(_ result: [NSDecimalNumber]) {
        let currency = self.viewModel.selectedCurrency
        switch self.viewModel.validationError {
        case .min:
            self.handleOutOfRangeValue(result, currency)
        case .max:
            self.handleOutOfRangeValue(result, currency)
        case .classLimit:
            self.handleClassLimit(result, currency)
        case .step:
            self.handleNotAcceptableValue(result, currency)
        case .none:
            return
        }
    }
    
    private func handleClassLimit(_ result: [NSDecimalNumber],
                                  _ currensyCode: CurrencyType) {
        if let suggestedValue = result.first {
            self.errorContainerStackView.isHidden = false
            self.continueButton.isEnabled = false
            self.errorInfoLabel.text = "investment.amount.error.classLimit".localized
            self.errorInfoLabel.isHidden = false
            self.suggestedInvestSingleContainer.isHidden = false
            let text = self.createAttributedTitle(for: "\(suggestedValue.decimalValue.currency) ", currency: currensyCode)
            self.suggestedInvestSingleButton.setAttributedTitle(text, for: .normal)
        }
    }
    
    private func handleOutOfRangeValue(_ result: [NSDecimalNumber],
                                       _ currensyCode: CurrencyType) {
        if let suggestedValue = result.first {
            self.errorContainerStackView.isHidden = false
            self.continueButton.isEnabled = false
            if suggestedValue.compare(self.viewModel.minimalValueEuro) == .orderedSame {
                self.errorInfoLabel.text = Constants.loverLimitHint
            } else {
                self.errorInfoLabel.text = Constants.upperLimitHint
            }
            self.errorInfoLabel.isHidden = false
            self.suggestedInvestSingleContainer.isHidden = false
            let text = self.createAttributedTitle(for: "\(suggestedValue.decimalValue.currency) ", currency: currensyCode)
            self.suggestedInvestSingleButton.setAttributedTitle(text, for: .normal)
        }
    }
    
    private func handleNotAcceptableValue(_ result: [NSDecimalNumber],
                                          _ currensyCode: CurrencyType) {
        if let minSuggestedValue = result.first,
           let maxSuggestedValue = result.last {
            self.errorContainerStackView.isHidden = false
            self.continueButton.isEnabled = false
            self.errorInfoLabel.text = "\(Constants.notMultipleHint) \(viewModel.investmentStepEuro.decimalValue.currency) \("investment.amount.eurInvestmentAmountErrorValue".localized)"
            self.errorInfoLabel.isHidden = false
            self.suggestedAmountStackView.isHidden = false
            let lessText = self.createAttributedTitle(for: "\(minSuggestedValue.decimalValue.currency) ", currency: currensyCode)
            self.suggestedAmountLessButton.setAttributedTitle(lessText, for: .normal)
            let moreText = self.createAttributedTitle(for: "\(maxSuggestedValue.decimalValue.currency) ", currency: currensyCode)
            self.suggestedAmountMoreButton.setAttributedTitle(moreText, for: .normal)
        }
    }
    
    private func createAttributedTitle(for text: String, currency: CurrencyType, selected: Bool = false) -> NSMutableAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = currency == .euro ? UIImage(.amountInEuro) : UIImage(.tokenNew)
        attachment.bounds = CGRect(x: 0, y: 0, width: Constants.amountImageSize, height: Constants.amountImageSize)
        let attachmentString = NSAttributedString(attachment: attachment)
        let text = NSMutableAttributedString(string: text)
        text.append(attachmentString)
        if selected {
            text.addAttribute(.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: text.length))
        }
        return text
    }
    
    func hideErrorsViews() {
        self.errorContainerStackView.isHidden = true
        self.errorInfoLabel.isHidden = true
        self.suggestedInvestSingleContainer.isHidden = true
        self.suggestedAmountStackView.isHidden = true
        self.continueButton.isEnabled = true
    }
    
    func openNextScreen() {
        if self.viewModel.isGermanTaxesInfoFilled {
            self.nextController = ViewControllerProvider.setupPrecontractualViewController(rootViewController: self)
        } else {
            self.nextController = ViewControllerProvider.setupGermanTaxViewController(rootViewController: self)
        }
        self.pushNextController(animated: true)
    }
}

extension InvestmentAmountCurrencyViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.hideErrorsViews()
        if (self.insertAmountTextField.text?.count ?? 0) > Constants.maxLangthOfText {
            self.insertAmountTextField.text = self.viewModel.currentAmount.decimalValue.currency
        }
        
        var value: NSDecimalNumber
        if let decimalValue = self.insertAmountTextField.text?.numeric?.decimalValue {
            value = NSDecimalNumber(decimal: decimalValue)
        } else if let stringValue = self.insertAmountTextField.text?.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: ",", with: ""), !(NSDecimalNumber(string: stringValue) == .notANumber) {
            value = NSDecimalNumber(string: stringValue)
        } else {
            value = .zero
        }
        self.insertAmountTextField.text = value.decimalValue.currency
        
        self.viewModel.updateCurrentAmount(value)
        
        self.convertValuesContent(selectedCurrency: viewModel.selectedCurrency)
        
        if self.insertAmountTextField.text?.isEmpty == true || self.insertAmountTextField.text == "0" {
            self.continueButton.isEnabled = false
            self.errorLabel.isHidden = false
        } else {
            self.continueButton.isEnabled = true
            self.errorLabel.isHidden = true
        }
        if viewModel.isNewAmountLower {
            self.errorContainerStackView.isHidden = false
            self.subtractStepButton.isEnabled = false
            self.addStepButton.isEnabled = true
        } else if viewModel.isNewAmountHigher {
            self.errorContainerStackView.isHidden = false
            self.subtractStepButton.isEnabled = true
            self.addStepButton.isEnabled = false
        } else if viewModel.isNewAmountHigherThanClassLimit {
            self.errorContainerStackView.isHidden = false
            self.subtractStepButton.isEnabled = true
            self.addStepButton.isEnabled = false
        } else {
            self.errorContainerStackView.isHidden = true
            self.subtractStepButton.isEnabled = true
            self.addStepButton.isEnabled = true
        }
        self.handleValidationResult(viewModel.validate())
    }
}
