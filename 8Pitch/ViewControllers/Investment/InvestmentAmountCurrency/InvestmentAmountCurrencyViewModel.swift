//
//  InvestmentAmountCurrencyViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 8/24/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import Remote

enum CurrencyType {
    case euro
    case bghToken
}

enum ValidationError {
    case min
    case max
    case classLimit
    case step
}

class InvestmentAmountCurrencyViewModel: GenericViewModel {
    
    var project: Project?
    
    private (set) var selectedCurrency: CurrencyType {
        willSet {
            switch newValue {
            case .bghToken:
                self.currentAmount = self.currentBGHAmount
            case .euro:
                self.currentAmount = self.currentEurAmount
            }
        }
    }
    
    let investmentAmountValues: InvestmentAmountValues
    
    private (set) var currentAmount: NSDecimalNumber
    
    var validationError: ValidationError?
    
    var investorClassLimit: NSDecimalNumber? {
        self.investmentAmountValues.investorClassLimit
    }
    
    var investorClassLimitInCurrentCurrency: NSDecimalNumber {
        switch self.selectedCurrency {
        case .euro:
            return investorClassLimit ?? 0
        case .bghToken:
            return Self.calculateInBGHM(value: self.investorClassLimit ?? 0, nominalValue: investmentAmountValues.tokenNominalValue)
        }
    }
    
    var classLimitExist: Bool {
        investorClassLimit != nil
    }
    
    var maxInvestment: NSDecimalNumber {
        self.investmentAmountValues.maxValue
    }
    
    var currentEurAmount: NSDecimalNumber {
        return self.convertToEuro(value: self.currentAmount, nominalValue: investmentAmountValues.tokenNominalValue)
    }
    
    var tokenNominalValue: NSDecimalNumber {
        investmentAmountValues.tokenNominalValue
    }
    
    var tokenName: String {
        self.investmentAmountValues.shortCut
    }
    
    var currentBGHAmount: NSDecimalNumber {
        return self.convertToBGHM(value: self.currentAmount, nominalValue: investmentAmountValues.tokenNominalValue)
    }
    
    var feeInEur: NSDecimalNumber {
        return self.calculateFee(value: self.currentEurAmount, feeRate: investmentAmountValues.feeRate)
    }
    
    var feeRateInPersants: NSDecimalNumber {
        return self.investmentAmountValues.feeRate
    }
    
    
    var feeInBGH: NSDecimalNumber {
        return self.calculateFee(value: self.currentBGHAmount, feeRate: investmentAmountValues.feeRate)
    }
    
    var dynamicInvestmentStep: NSDecimalNumber {
        switch self.selectedCurrency {
        case .euro:
            return Self.calculateInEuro(value: investmentAmountValues.investmentStep, nominalValue: investmentAmountValues.tokenNominalValue)
        case .bghToken:
            return investmentAmountValues.investmentStep
        }
    }
    
    var investmentStepEuro: NSDecimalNumber {
        return Self.calculateInEuro(value: self.investmentAmountValues.investmentStep, nominalValue: investmentAmountValues.tokenNominalValue)
    }
    
    var minimalValueEuro: NSDecimalNumber {
        return Self.calculateInEuro(value: self.investmentAmountValues.minValue, nominalValue: investmentAmountValues.tokenNominalValue)
    }
    
    var isNewAmountLower: Bool {
        return currentBGHAmount.subtracting(investmentAmountValues.investmentStep).compare(investmentAmountValues.minValue) == .orderedAscending
    }
    
    var isNewAmountHigher: Bool {
        return currentBGHAmount.adding(investmentAmountValues.investmentStep).compare(investmentAmountValues.maxValue) == .orderedDescending
    }
    
    var isNewAmountHigherThanClassLimit: Bool {
        if let limit = investmentAmountValues.investorClassLimit {
            return currentBGHAmount.adding(investmentAmountValues.investmentStep).compare(limit) == .orderedDescending
        }
        return false
    }
    
    required init(session: Session?) {
        let session = session ?? Session(registrationType: .none)
        self.investmentAmountValues = session.projectsProvider.investmentAmountValues
        self.currentAmount = investmentAmountValues.minValueInEUR
        self.selectedCurrency = .euro
        super.init(session: session)
    }
    
    func updateCurrentAmount(_ newValue: NSDecimalNumber) {
        self.currentAmount = newValue
    }
    
    func stepUpCurrentAmount() {
        if self.currentAmount == .notANumber {
            self.currentAmount =  .zero
        }
        switch self.selectedCurrency {
        case .bghToken:
            self.currentAmount = self.currentAmount.adding(self.investmentAmountValues.investmentStep)
        case .euro:
            self.currentAmount = self.currentAmount.adding(self.investmentStepEuro)
        }
    }
    
    func stepDownCurrentAmount() {
        if self.currentAmount == .notANumber {
            self.currentAmount =  .zero
        }
        switch self.selectedCurrency {
        case .bghToken:
            self.currentAmount = self.currentAmount.subtracting(self.investmentAmountValues.investmentStep)
        case .euro:
            self.currentAmount = self.currentAmount.subtracting(self.investmentStepEuro)
        }
    }
    
    func updateSelectedCurrency(_ newValue: CurrencyType) {
        self.selectedCurrency = newValue
    }
    
    func validate() -> [NSDecimalNumber] {
        var result: [NSDecimalNumber] = []
        self.validationError = nil
        let handlerDown = NSDecimalNumberHandler(roundingMode: .down, scale: 0, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false)
        let handlerUp = NSDecimalNumberHandler(roundingMode: .up, scale: 0, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false)
        
        switch self.selectedCurrency {
        case .bghToken:
            if currentAmount.compare(investmentAmountValues.minValue) == .orderedAscending {
                result.append(investmentAmountValues.minValue)
                self.validationError = .min
                return result
            } else if currentAmount.compare(investmentAmountValues.maxValue) == .orderedDescending {
                result.append(investmentAmountValues.maxValue)
                self.validationError = .max
                return result
            } else if self.classLimitExist && currentAmount.compare(self.investorClassLimitInCurrentCurrency) == .orderedDescending {
                result.append(self.investorClassLimitInCurrentCurrency)
                self.validationError = .classLimit
                return result
            } else {
                let roundedDownValue = currentAmount.dividing(by: investmentAmountValues.investmentStep, withBehavior: handlerDown)
                let roundedUpValue = currentAmount.dividing(by: investmentAmountValues.investmentStep, withBehavior: handlerUp)
                if roundedDownValue.compare(roundedUpValue) != .orderedSame {
                    self.validationError = .step
                    result.append(investmentAmountValues.investmentStep.multiplying(by: roundedDownValue))
                    result.append(investmentAmountValues.investmentStep.multiplying(by: roundedUpValue))
                    return result
                }
            }
        case .euro:
            if currentAmount.compare(Self.calculateInEuro(value: investmentAmountValues.minValue, nominalValue: investmentAmountValues.tokenNominalValue)) == .orderedAscending {
                result.append(Self.calculateInEuro(value: investmentAmountValues.minValue, nominalValue: investmentAmountValues.tokenNominalValue))
                self.validationError = .min
                return result
            }  else if currentAmount.compare(Self.calculateInEuro(value: investmentAmountValues.maxValue, nominalValue: investmentAmountValues.tokenNominalValue)) == .orderedDescending {
                result.append(Self.calculateInEuro(value: investmentAmountValues.maxValue, nominalValue: investmentAmountValues.tokenNominalValue))
                self.validationError = .max
                return result
            } else if self.classLimitExist && currentAmount.compare(self.investorClassLimitInCurrentCurrency) == .orderedDescending {
                result.append(self.investorClassLimitInCurrentCurrency)
                self.validationError = .classLimit
                return result
            } else {
                let roundedDownValue = currentAmount.dividing(by: investmentStepEuro, withBehavior: handlerDown)
                let roundedUpValue = currentAmount.dividing(by: investmentStepEuro, withBehavior: handlerUp)
                if roundedDownValue.compare(roundedUpValue) != .orderedSame {
                    self.validationError = .step
                    result.append(investmentStepEuro.multiplying(by: roundedDownValue))
                    result.append(investmentStepEuro.multiplying(by: roundedUpValue))
                    return result
                }
            }
        }
        return result
    }
    
    func saveResult() {
        self.session.projectsProvider.currentInvestmentAmount = (inToken: currentBGHAmount, inEUR: currentEurAmount)
    }
}

extension InvestmentAmountCurrencyViewModel {
    
    private func convertToBGHM(value: NSDecimalNumber, nominalValue: NSDecimalNumber) -> NSDecimalNumber {
        if value == .notANumber || nominalValue == .notANumber {
            return .zero
        }
        switch self.selectedCurrency {
        case .bghToken:
            return value
        case .euro:
            return Self.calculateInBGHM(value: value, nominalValue: nominalValue)
        }
    }
    
    private func convertToEuro(value: NSDecimalNumber, nominalValue: NSDecimalNumber) -> NSDecimalNumber {
        if value == .notANumber || nominalValue == .notANumber {
            return .zero
        }
        switch self.selectedCurrency {
        case .bghToken:
            return Self.calculateInEuro(value: value, nominalValue: nominalValue)
        case .euro:
            return value
        }
    }
    
    static private func calculateInBGHM(value: NSDecimalNumber, nominalValue: NSDecimalNumber) -> NSDecimalNumber {
        if value == .notANumber || nominalValue == .notANumber {
            return .zero
        }
        return value.dividing(by: nominalValue)
    }
    
    static private func calculateInEuro(value: NSDecimalNumber, nominalValue: NSDecimalNumber) -> NSDecimalNumber {
        if value == .notANumber || nominalValue == .notANumber {
            return .zero
        }
        return value.multiplying(by: nominalValue)
    }
    
    private func calculateFee(value: NSDecimalNumber, feeRate: NSDecimalNumber) -> NSDecimalNumber {
        if value == .notANumber || feeRate == .notANumber {
            return .zero
        }
        return value.multiplying(by: feeRate.dividing(by: self.investmentAmountValues.feeMultiplier))
    }
}
