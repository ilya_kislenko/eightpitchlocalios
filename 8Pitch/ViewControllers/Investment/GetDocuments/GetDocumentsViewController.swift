//
//  GetDocumentsViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/28/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//
import UIKit
import Input
import Remote

fileprivate struct Constants {
    static let title = "investment.documents.title".localized
    static let subtitle = "investment.documents.subTitle".localized
    static let description = "investment.documents.description".localized
    static let confirmation = "investment.documents.confirmatoin".localized
    static let sendByEmail = "investment.documents.byEmail".localized
    static let directDownload = "investment.documents.directDownload".localized
    static let path: String = WebURLConstants.filePath
    static let textViewHeight: CGFloat = 32
}

class GetDocumentsViewController: GenericViewController {
    @IBOutlet var subtitleLabel: CustomSubtitleLabel!
    @IBOutlet var descriptionLabel: InfoCurrencyLabel!
    @IBOutlet weak var stackForDocuments: UIStackView!
    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    @IBOutlet var confirmationButton: UIButton!
    @IBOutlet var confirmationDescriptionLabel: ApproveLabel!
    @IBOutlet var sendByEmailButton: BorderedButton!
    @IBOutlet var directDownloadButton: BorderedButton!
    
    private lazy var viewModel : GetDocumentsViewModel = {
        GetDocumentsViewModel(session: session)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateNavigationBar()
    }
    
    func updateNavigationBar() {
        if let navigationBar = self.navigationController?.navigationBar as? CustomNavigationBar {
            navigationBar.setupForWhite()
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.barlow.regular.titleSize, NSAttributedString.Key.foregroundColor: UIColor(.gray)]
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.arrowBack), style: .plain, target: self, action: #selector(backTapped))
        self.navigationItem.customizeForWhite()
    }
    
    private func setupContent() {
        self.title = Constants.title
        self.subtitleLabel.text = Constants.subtitle
        self.descriptionLabel.text = Constants.description
        self.confirmationDescriptionLabel.text = Constants.confirmation
        self.sendByEmailButton.setTitle(Constants.sendByEmail, for: .normal)
        self.directDownloadButton.setTitle(Constants.directDownload, for: .normal)
        self.setupDocuments()
    }
    
    private func setupDocuments() {
        let files = self.session?.projectsProvider.expandedProject?.documents
        files?.forEach { self.createTextViewFor(document: $0) }
    }
    
    private func createTextViewFor(document: File?) {
        guard let document = document, let name = document.originalFileName?.uppercased().replacingOccurrences(of: ".", with: " ("),
              let id = document.uniqueFileName else { return }
        let textView = UITextView()
        textView.isEditable = false
        textView.isSelectable = true
        textView.delegate = self
        textView.textContainer.maximumNumberOfLines = 1
        textView.textContainer.lineBreakMode = .byTruncatingTail
        let attributes: [NSAttributedString.Key : Any] = [.link: Constants.path + id ]
        textView.attributedText = NSAttributedString(string: name + ")", attributes: attributes)
        textView.linkTextAttributes = [.foregroundColor: UIColor(.gray),
                                       .font: UIFont.barlow.semibold.titleSize]
        self.stackHeight.constant += Constants.textViewHeight
        self.stackForDocuments.addArrangedSubview(textView)
    }
    
    
    @IBAction func confirmAction(_ sender: UIButton) {
        sender.isSelected.toggle()
    }
    
    @IBAction func sendByEmailAction(_ sender: Any) {
        if confirmationButton.isSelected {
            self.startProgress()
            self.viewModel.sendDocumentsOnEmail(self.session) { (error) in
                self.stopProgress()
                if let error = error {
                    self.presentAlert(message: error.localizedDescription)
                } else {
                    let _ = ViewControllerProvider.setupSentOnEmailViewController(rootViewController:self)
                    self.pushNextController(animated: true)
                }
            }
        }
    }
    
    @IBAction func directDownloadAction(_ sender: Any) {
        if confirmationButton.isSelected {
            let _ = ViewControllerProvider.setupDirectDownloadedViewController(rootViewController: self)
            self.pushNextController(animated: true)
        }
    }
    
    @objc func backTapped() {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension GetDocumentsViewController: UITextViewDelegate {
    func textView(_ textView: UITextView,
                  shouldInteractWith URL: URL,
                  in characterRange: NSRange,
                  interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString.hasPrefix(Constants.path) {
            self.downloadFile(id: URL.lastPathComponent)
            return false
        } else {
            return true
        }
    }
    
    private func downloadFile(id: String?) {
        guard let id = id else { return }
        self.startProgress()
        self.viewModel.downloadFile(id: id) { [weak self] url, error in
            DispatchQueue.main.async {
                self?.stopProgress()
                guard let error = error else {
                    let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
                    self?.present(activityViewController, animated: true)
                    return
                }
                self?.presentAlert(message: error.localizedDescription)
            }
        }
    }
}
