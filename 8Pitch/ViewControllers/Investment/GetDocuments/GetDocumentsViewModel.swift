//
//  GetDocumentsViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 8/28/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import Remote

class GetDocumentsViewModel: GenericViewModel {
    
    func sendDocumentsOnEmail(_ session: Session?, completionHandler: @escaping (Error?) -> Void) {
        guard let session = session,
            let projectId = session.projectsProvider.currentProject?.identifier,
            let investmentID = session.projectsProvider.investmentId else {
                DispatchQueue.main.async {
                    completionHandler(GenericRequestError.invalidResponse(nil))
                }
                return
        }
        self.remote.documentsOnEmail(session, projectId: projectId, investmentID: investmentID) { (error) in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
    }
}
