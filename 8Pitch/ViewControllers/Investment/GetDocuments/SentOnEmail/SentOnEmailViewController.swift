//
//  SentOnEmailViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/31/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

fileprivate struct Constants {
    static let subtitle = "investment.onEmail.subtitle".localized
    static let preheader = "investment.onEmail.preheader".localized
    static let makeConfirm = "investment.onEmail.makeConfirm".localized
    static let informationComplete = "investment.onEmail.informationComplete".localized
    static let recieveConfirm = "investment.onEmail.recieveConfirm".localized
    static let contract = "investment.onEmail.contract".localized
    static let nextButton = "investment.onEmail.nextButton".localized
}

class SentOnEmailViewController: GenericViewController {

    @IBOutlet var subtitleLabel: CustomSubtitleDarkLabel!
    @IBOutlet weak var confirmationView: ConfirmationView!
    @IBOutlet weak var preheaderLabel: UppercasedWhiteLabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.confirmationView.setupWith(self.session?.projectsProvider.expandedProject?.documents.map { $0?.originalFileName} ?? [])
        self.confirmationView.delegate = self
        self.updateNavigationBar()
        self.setupContent()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.backgroundColor = UIColor(.backgroundDark)
        self.navigationController?.navigationBar.barTintColor = UIColor(.backgroundDark)
        self.navigationItem.customizeForColored()
        self.setNeedsStatusBarAppearanceUpdate()
    }


    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.backgroundColor = UIColor(.white)
        self.navigationController?.navigationBar.barTintColor = UIColor(.white)
        self.navigationItem.customizeForWhite()
    }

    func setupContent() {
        self.subtitleLabel.text = Constants.subtitle
        self.preheaderLabel.text = Constants.preheader
    }
    
    func updateNavigationBar() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.whiteClose), style: .plain, target: self, action: #selector(closeTapped))
    }

    @objc func closeTapped() {
        self.popToPreviousController(animated: true)
    }
}

extension SentOnEmailViewController: InvestmentDocumentsDelegate {
    
    func nextButtonAction() {
        _ = ViewControllerProvider.setupInvestmentConfirmationViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}
