//
//  GermanTaxViewController.swift
//  8Pitch
//
//  Created by 8pitch on 13.01.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class GermanTaxViewController: AbstractCollectionViewController<TaxInformation> {
    
    private lazy var viewModel : GermanTaxViewModel = {
        GermanTaxViewModel(session: session)
    }()
    
    @IBOutlet weak var infoLabel: InfoCurrencyLabel!
    
    override var applyChangesFooterViewType : PublishSubject<ApplyChangesView.ViewType?>? {
        didSet {
            guard let typeAction = self.applyChangesFooterViewType else { return }
            typeAction.onNext(.GermanTaxInfo)
        }
    }
    
    override var model: BehaviorSubject<TaxInformation>? {
        return self.session?.taxInformationProvider
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "investment.tax.title".localized
        self.customTitle = "investment.tax.customTitle".localized
        self.infoLabel.text = "investment.tax.info".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resetModel()
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(GermanTaxSwitcherCell.nibForRegistration, forCellWithReuseIdentifier: GermanTaxSwitcherCell.reuseIdentifier)
        self.collectionView.register(GermanTaxInputCell.nibForRegistration, forCellWithReuseIdentifier: GermanTaxInputCell.reuseIdentifier)
        self.collectionView.register(GermanTaxDropdownCell.nibForRegistration, forCellWithReuseIdentifier: GermanTaxDropdownCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = ApplyChangesView.reuseIdentifier
        layout.register(ApplyChangesView.nibForRegistration, forDecorationViewOfKind: ApplyChangesView.reuseIdentifier)
    }
    
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: { [weak self] in
                    self?.stopProgress()
                    switch $0 {
                    case .error: return
                    case .loading:
                        guard let isValid = try? self?.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self?.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            guard value else { return }
                            self?.startProgress()
                            self?.confirmTaxResidence()
                        }
                    case .next: self?.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        var sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: EmptyError.self, cellReuseIdentifier: GermanTaxSwitcherCell.reuseIdentifier)
        ]
        if self.viewModel.isGermanTaxResident {
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: EmptyError.self, cellReuseIdentifier: GermanTaxInputCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: 2, model: model, errorType: EmptyError.self, cellReuseIdentifier: GermanTaxInputCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: 3, model: model, errorType: EmptyError.self, cellReuseIdentifier: GermanTaxSwitcherCell.reuseIdentifier))
            if self.viewModel.isChurchTaxLability {
                sections.append(CollectionViewModel(collectionView: self.collectionView, section: 4, model: model, errorType: EmptyError.self, cellReuseIdentifier: GermanTaxDropdownCell.reuseIdentifier))
            }
        }
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        if let cell = cell as? GermanTaxSwitcherCell {
            cell.setup(for: self.viewModel.switcherCellType(indexPath.section))
            cell.switchHandler = { [weak self] in
                self?.setupLayout()
                self?.registerCells()
                self?.setupDataSource()
                self?.collectionView.reloadData()
            }
        } else if let cell = cell as? GermanTaxInputCell {
            cell.setup(for: self.viewModel.inputCellType(indexPath.section))
        }
        return cell
    }
    
    private func openNextScreen() {
        if self.viewModel.isGermanTaxResident {
            self.nextController = ViewControllerProvider.setupPrecontractualViewController(rootViewController: self)
            self.nextController?.previousController = self.previousController
            self.pushNextController(animated: true)
        } else {
            if let investScreen = self.navigationController?.viewControllers.first(where:
                                                                                    { $0 is ProjectDetailsRootViewController}) {
                self.navigationController?.popToViewController(investScreen, animated: true)
            }
        }
    }
    
    func confirmTaxResidence() {
        self.startProgress()
        self.viewModel.createTaxInformation(completionHandler: { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.openNextScreen()
                return
            }
            self?.presentAlert(message: error.localizedDescription)
        })
    }
}
