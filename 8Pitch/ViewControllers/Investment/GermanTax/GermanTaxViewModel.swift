//
//  GermanTaxViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 13.01.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//
import Input

class GermanTaxViewModel: GenericViewModel {
    
    var isChurchTaxLability: Bool {
        (try? self.session.taxInformationProvider.value().churchTaxLabilityProvider.value()) ?? false
    }
    
    func switcherCellType(_ index: Int) -> GermanTaxSwitcherCell.GermanTaxSwitcherCellType {
        index == Constants.taxResidenceCellIndex ? .tax : .churchTax
    }
    
    func inputCellType(_ index: Int) -> GermanTaxInputCell.GermanTaxInputCellType {
        index == Constants.taxIDCellIndex ? .id : .office
    }
    
    func createTaxInformation(completionHandler: @escaping ErrorClosure) {
        self.remote.taxInformation(self.session, completionHandler: { [weak self] error in
            guard let error = error else {
                self?.user(completionHandler: { response in
                    switch response {
                    case .failure(let error):
                        completionHandler(error)
                    case .success(_):
                        completionHandler(nil)
                    }
                })
                return
            }
            completionHandler(error)
        })
    }
    
    enum Constants {
        static let taxResidenceCellIndex: Int = 0
        static let taxIDCellIndex: Int = 1
    }
}
