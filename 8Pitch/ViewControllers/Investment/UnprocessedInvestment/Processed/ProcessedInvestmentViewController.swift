//
//  ProcessedInvestmentViewController.swift
//  8Pitch
//
//  Created by 8pitch on 01.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ProcessedInvestmentViewController: CustomTitleViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeTapped))
        self.navigationItem.rightBarButtonItem = nil
    }

    @objc private func closeTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func okTapped(_ sender: UIButton) {
        self.closeTapped()
    }
    
}
