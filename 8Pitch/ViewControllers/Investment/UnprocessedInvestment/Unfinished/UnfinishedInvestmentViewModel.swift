//
//  UnfinishedInvestmentViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/2/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Remote
import Input

class UnfinishedInvestmentViewModel: GenericViewModel {
        
    func cancelInvestment(_ session: Session, _ projectId: Int, _ investmentId: String, completionHandler: @escaping ErrorClosure) {
        self.remote.cancelInvestment(session, projectId, investmentId, completionHandler)
    }
    
    
    
}
