//
//  UnfinishedInvestmentViewController.swift
//  8Pitch
//
//  Created by 8pitch on 01.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class UnfinishedInvestmentViewController: RedViewController {
        
    private lazy var viewModel : UnfinishedInvestmentViewModel = {
        UnfinishedInvestmentViewModel(session: session)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeTapped))
        self.setupLabel()
    }
    
    @IBOutlet weak var infoLabel: InfoWhiteLabel!

    @IBAction func finalizePurchasesTapped(_ sender: Any) {
        let detailsViewController = self.previousController
        _ = ViewControllerProvider.setupGetDocumentsViewController(rootViewController: self)
        self.nextController?.previousController = detailsViewController
        self.pushNextController(animated: true)
    }
    
    @IBAction func cancelPurchasesTapped(_ sender: Any) {
        guard let session = session, let projectId = session.projectsProvider.currentProject?.identifier, let investmentId = session.projectsProvider.projectInvestmentInfo?.unprocessedInvestment?.investmentId else { return }
        self.startProgress()
        self.viewModel.cancelInvestment(session, projectId, investmentId) { error in
            self.stopProgress()
            if let error = error {
                self.presentAlert(message: error.localizedDescription)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @objc private func closeTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setupLabel() {
        guard let tokens = self.session?.projectsProvider.projectInvestmentInfo?.unprocessedInvestment?.amount, let value = self.session?.projectsProvider.projectInvestmentInfo?.nominalValue, let tokenName = self.session?.projectsProvider.projectInvestmentInfo?.shortCut else { return }
        self.infoLabel.text = String(format: "investment.unfinished.info".localized, "\(Int(tokens)) \(tokenName)", "\(Int(tokens * value))")
    }
    
}
