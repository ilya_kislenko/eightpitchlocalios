//
//  NotificationListViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import Remote

class NotificationListViewModel: GenericViewModel {
    
    var notifications: [ProjectNotification] = []
    
    func loadNotifications(_ session: Session?, completionHandler: @escaping ErrorClosure) {
        guard let session = session else { return }
        self.session = session
        self.remote.notifications(session) { response, error in
            self.notifications = session.notificationsProvider.notifications.filter {
                $0.parseToMutableString(baseUrl: Remote().webURL) != nil
            }
            completionHandler(error)
        }
    }
    
    func readNotification(_ session: Session?, _ notificationId: Int, completionHandler: @escaping ErrorClosure) {
        guard let session = session else { return }
        self.session = session
        self.remote.readNotification(session, notificationId) { (error) in
            completionHandler(error)
        }
    }
    
    func voting(id: Int, completionHandler: @escaping ErrorClosure) {
        self.remote.voting(session, id: id, completionHandler: { error in
            completionHandler(error)
        })
    }
    
    func canBePassed(_ voting: Voting?) -> Bool {
        guard let voting = voting else { return false }
        let isInvestor = self.session.accountGroup == .privateInvestor || self.session.accountGroup == .institutionalInvestor
        let isRunning = voting.status == .running
        let isAvailable = voting.usersVotingStatus == .available
        return isInvestor && isRunning && isAvailable
    }
    
    func needTrusteeDecision(_ voting: Voting?) -> Bool {
        guard let voting = voting else { return false }
        return voting.investorTrusteeDecision == nil &&
            voting.startDate ?? Date () >= Date()
    }
    
    func canBeApproved(_ voting: Voting?) -> Bool {
        guard let voting = voting else { return false }
        let isInitiator = self.session.accountGroup == .initiator
        let isWaitingForApproval = voting.status == .waitingForApproval
        return isWaitingForApproval && isInitiator
    }
    
    func joinStream(id: Int, completionHandler: @escaping ErrorClosure) {
        self.remote.joinMeeting(session, id: id, completionHandler: { error in
            completionHandler(error)
        })
    }
}
