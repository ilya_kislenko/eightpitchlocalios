//
//  NotificationListViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote

protocol OpenVotingPageDelegate: class {
    func passVoting(with id: String)
    func approveVoting(with id: String)
    func votingsList(with id: String)
    func getVoting(with id: String)
    func joinToStream(with id: Int)
    func votingSurvey()
    func trustee(with id: String)
}

class NotificationListViewController: CustomTitleViewController {
    
    // MARK: Properties
    
    private lazy var viewModel : NotificationListViewModel = {
        NotificationListViewModel(session: session)
    }()
    
    // MARK: UI Components
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyLabel: UILabel!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.setupTableView()
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadNotifications()
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func pushNextController(animated: Bool) {
        self.nextController?.hidesBottomBarWhenPushed = true
        super.pushNextController(animated: true)
    }
    
    private func registerCells() {
        self.tableView.register(NotificationTableViewCell.nibForRegistration, forCellReuseIdentifier: NotificationTableViewCell.reuseIdentifier)
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.topItem?.title = "notifications.title".localized
        customTitle = "notifications.subtitle".localized
        navigationItem.leftBarButtonItem = nil
        navigationItem.rightBarButtonItem = nil
    }
    
    private func setupTableView() {
        emptyLabel.text = "notification.empty.list.label".localized
        tableView.estimatedRowHeight = Constants.estimatedRowHeight
    }
    
}

extension NotificationListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTableViewCell.reuseIdentifier, for: indexPath)
        if let cell = cell as? NotificationTableViewCell {
            cell.delegate = self
            cell.setup(viewModel.notifications[indexPath.row])
            cell.onExpand = { [weak self] in
                self?.tableView.beginUpdates()
                self?.viewModel.notifications[indexPath.row].changeExpandedState()
                cell.changeButtonState(isExpanded: self?.viewModel.notifications[indexPath.row].isExpanded)
                self?.tableView.endUpdates()
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if viewModel.notifications[indexPath.row].status != .read {
            let closeAction = UIContextualAction(style: .normal, title:  "notifications.cell.mark.as.read".localized, handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                guard let notificationId = self.viewModel.notifications[indexPath.row].identifier else { return }
                self.readNotification(notificationId: notificationId, with: indexPath)
                success(true)
            })
            closeAction.backgroundColor = UIColor(.red)
            return UISwipeActionsConfiguration(actions: [closeAction])
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
}

private extension NotificationListViewController {
    enum Constants {
        static let estimatedRowHeight = CGFloat(93)
    }
    
    func loadNotifications() {
        self.startProgress()
        self.viewModel.loadNotifications(self.session) { [weak self] error in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.tableView.isHidden = self?.viewModel.notifications.isEmpty ?? true
                self?.setupUnreadNotificationIndicator()
                self?.tableView.reloadData()
            }
        }
    }
    
    func readNotification(notificationId: Int, with indexPath: IndexPath) {
        self.startProgress()
        self.viewModel.readNotification(self.session, notificationId) { [weak self] error in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.viewModel.notifications[indexPath.row].status = .read
                self?.setupUnreadNotificationIndicator()
                self?.tableView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
    
    func setupUnreadNotificationIndicator() {
        if self.viewModel.notifications.contains(where: { $0.status != .read }) {
            self.tabBarItem.badgeValue = "●"
            self.tabBarItem.badgeColor = .clear
            self.tabBarItem.setBadgeTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(Color.red)], for: .normal)
        } else {
            self.tabBarItem.badgeValue = ""
            self.tabBarItem.badgeColor = .clear
        }
    }
    
    func openVotings() {
        _ = ViewControllerProvider.setupVotingsForProjectsViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openVotingApprove() {
        _ = ViewControllerProvider.setupApproveVotingViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openVotingPass() {
        _ = ViewControllerProvider.setupPassVotingViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openStream() {
        _ = ViewControllerProvider.setupStreamViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openTrusteeDecision() {
        _ = ViewControllerProvider.setupTrusteeDecisionViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openTrusteeError() {
        _ = ViewControllerProvider.setupTrusteeErrorViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openStreamError() {
        _ = ViewControllerProvider.setupStreamErrorViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func handleError(error: Error) {
        switch error {
        case VotingRequestError.prohibited:
            self.openVotings()
        default:
            self.presentAlert(message: error.localizedDescription)
        }
    }
}

extension NotificationListViewController: OpenVotingPageDelegate {
    func trustee(with id: String) {
        guard let id = Int(id) else  { return }
        self.startProgress()
        self.viewModel.voting(id: id, completionHandler: { [weak self] error in
            guard let self = self else { return }
            self.stopProgress()
            if let error = error {
                self.handleError(error: error)
            } else {
                if self.viewModel.needTrusteeDecision(self.session?.votingProvider.currentVoting) {
                    self.openTrusteeDecision()
                } else {
                    self.openTrusteeError()
                }
            }
        })
    }
    
    func getVoting(with id: String) {
        guard let id = Int(id) else { return }
        self.startProgress()
        self.viewModel.voting(id: id, completionHandler: { [weak self] error in
            if let error = error {
                self?.stopProgress()
                switch error {
                case VotingRequestError.prohibited:
                    self?.session?.votingProvider.currentVoting = nil
                    self?.openStreamError()
                default:
                    self?.presentAlert(message: error.localizedDescription)
                }
            } else {
                self?.joinToStream(with: id)
            }
        })
    }
    
    func joinToStream(with id: Int) {
        self.viewModel.joinStream(id: id, completionHandler: { [weak self] error in
            if let _ = error {
                self?.stopProgress()
                self?.openStreamError()
            } else {
                self?.openStream()
            }
        })
    }
    
    func votingSurvey() {
        _ = ViewControllerProvider.setupVotingPlugViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func passVoting(with id: String) {
        guard let id = Int(id) else  { return }
        self.startProgress()
        self.viewModel.voting(id: id, completionHandler: { [weak self] error in
            guard let self = self else { return }
            self.stopProgress()
            if let error = error {
                self.handleError(error: error)
            } else {
                if self.viewModel.canBePassed(self.session?.votingProvider.currentVoting) {
                    self.openVotingPass()
                } else {
                    self.openVotings()
                }
            }
        })
    }
    
    func approveVoting(with id: String) {
        guard let id = Int(id) else  { return }
        self.startProgress()
        self.viewModel.voting(id: id, completionHandler: { [weak self] error in
            guard let self = self else { return }
            self.stopProgress()
            if let error = error {
                self.handleError(error: error)
            } else {
                if self.viewModel.canBeApproved(self.session?.votingProvider.currentVoting) {
                    self.openVotingApprove()
                } else {
                    self.openVotings()
                }
            }
        })
    }
    
    func votingsList(with id: String) {
        self.startProgress()
        self.viewModel.votings(completionHandler: { [weak self] error in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.openVotings()
            }
        })
    }
}
