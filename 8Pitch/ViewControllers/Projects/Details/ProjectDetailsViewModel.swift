//
//  ProjectDetailsViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/1/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Remote
import Input

class ProjectDetailsViewModel: GenericViewModel {
    func requestProjectDetailsVideoURL(itemID: String, _ completion: @escaping ResponseLinkClosure) {
        self.remote.vimeoVideoURL(itemID) { (result) in
            switch result {
            case .success(let link):
                completion(.success(link))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
