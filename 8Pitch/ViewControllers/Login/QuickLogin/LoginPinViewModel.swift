//
//  LoginPinViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 15.12.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class LoginPinViewModel: GenericViewModel {
    
    func checkBiometry(completion: @escaping ErrorClosure) {
        BiometryManager.shared.checkBiometry(completion: { error in
            completion(error)
        })
    }

}
