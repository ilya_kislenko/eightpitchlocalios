//
//  SetupQuickLoginViewController.swift
//  8Pitch
//
//  Created by 8pitch on 26.11.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class SetupQuickLoginViewController: GenericViewController, SheetDetentPresentationSupport {
    
    var contentHeight: CGFloat {
        self.laterButton.frame.maxY + Constants.inset * 2 + self.view.safeAreaInsets.bottom
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: InfoTitleSemiboldLabel!
    @IBOutlet weak var infoLabel: InfoLabel!
    @IBOutlet weak var useButton: RoundedButton!
    @IBOutlet weak var pinButton: BorderedButton!
    @IBOutlet weak var laterButton: UIButton!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.modalPresentationStyle = .custom
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        let isBiometryEnrolled = BiometryManager.shared.isBiometryEnrolled
        if isBiometryEnrolled {
            self.useButton.addTarget(self, action: #selector(useBiometry), for: .touchUpInside)
        } else {
            self.useButton.addTarget(self, action: #selector(setPin), for: .touchUpInside)
        }
        self.pinButton.addTarget(self, action: #selector(setPin), for: .touchUpInside)
        self.laterButton.addTarget(self, action: #selector(setLater), for: .touchUpInside)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layer.cornerRadius = Constants.cornerRadius
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (self.transitioningDelegate as? CustomTitleViewController)?.removeBlurEffect()
    }
    
    @objc func close() {
        self.setLater()
    }
    
    @objc private func useBiometry() {
        guard let delegate = self.transitioningDelegate as? CustomTitleViewController else {
            return
        }
        self.dismiss(animated: true, completion: {
            delegate.useBiometry(fromLogin: true)
        })
    }
    
    @objc private func setPin() {
        guard let delegate = self.transitioningDelegate as? CustomTitleViewController else {
            return
        }
        self.dismiss(animated: true, completion: {
            delegate.usePin(fromLogin: true)
        })
    }
    
    @objc private func setLater() {
        try? self.session?.loginProvider.value().rememberMeSelected = false
        guard let delegate = self.transitioningDelegate as? CustomTitleViewController else {
            return
        }
        self.dismiss(animated: true, completion: {
            delegate.openMainApp()
        })
    }
    
    private func setupUI() {
        self.view.layer.cornerRadius = Constants.cornerRadius
        let isBiometryEnrolled = BiometryManager.shared.isBiometryEnrolled
        let isTouchID = BiometryManager.shared.biometryType == .touchID
        self.pinButton.isHidden = !isBiometryEnrolled
        let image = isBiometryEnrolled ? (isTouchID ? UIImage(.touchIDLogin) : UIImage(.faceIDLogin)) : UIImage(.pinCode)
        self.imageView.image = image
        let title = isBiometryEnrolled ? (isTouchID ? "login.setupQuick.touchID.title".localized : "login.setupQuick.faceID.title".localized) : "login.setupQuick.pinCode.title".localized
        self.titleLabel.text = title
        self.titleLabel.sizeThatFits(self.titleLabel.intrinsicContentSize)
        let text = isBiometryEnrolled ? (isTouchID ? "login.setupQuick.touchID.subtitle".localized : "login.setupQuick.faceID.subtitle".localized) : "login.setupQuick.pinCode.subtitle".localized
        self.infoLabel.text = text
        self.infoLabel.sizeThatFits(self.infoLabel.intrinsicContentSize)
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.close))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        self.pinButton.layer.borderColor = UIColor(.gray).cgColor
        let useText = isBiometryEnrolled ? (isTouchID ? "login.setupQuick.touchID.useButton".localized : "login.setupQuick.faceID.useButton".localized) : "login.setupQuick.pinCode.useButton".localized
        let laterText = isBiometryEnrolled ? (isTouchID ? "login.setupQuick.touchID.laterButton".localized : "login.setupQuick.faceID.laterButton".localized) : "login.setupQuick.pinCode.laterButton".localized
        self.useButton.setTitle(useText, for: .normal)
        self.laterButton.setTitle(laterText, for: .normal)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    
    private enum Constants {
        static let cornerRadius: CGFloat = 10
        static let inset: CGFloat = 24
    }
    
}
