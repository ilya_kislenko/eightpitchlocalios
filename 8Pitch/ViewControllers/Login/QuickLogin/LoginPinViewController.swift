//
//  LoginPinViewController.swift
//  8Pitch
//
//  Created by 8pitch on 12/6/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class LoginPinViewController: PinViewController {
    
    private lazy var viewModel : LoginPinViewModel = {
        LoginPinViewModel(session: session)
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.nextController is SplashViewController {
            self.pushNextController(animated: false)
        } else if BiometryManager.shared.isBiometryEnabled {
            self.checkBiometry()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.session = .init(registrationType: .none)
        self.titleLabel.text = "quicklLogin.pin.enter.customTitle".localized
        let image = BiometryManager.shared.biometryType  == .touchID ? UIImage(.touchIDButton) : UIImage(.faceIDButton)
        self.keyboardView.funcButton.setImage(BiometryManager.shared.isBiometryEnabled ? image : nil, for: .normal)
        self.keyboardView.checkBiometry = { self.checkBiometry() }
    }
    
    override func checkPin(_ pin: String) {
        super.checkPin(pin)
        guard let session = self.session else { return }
        session.quickLoginProvider.createPin = pin
        if pin.count == 4 {
            guard session.quickLoginProvider.pinsMatched() else {
                self.clearPin()
                self.presentAlert(title: "quicklLogin.pin.error.title".localized, message: "quicklLogin.pin.error.text".localized)
                return
            }
            self.loadUser()
        }
    }
    
    override func openNext() {
        super.openNext()
        self.session?.quickLoginProvider.cleanUp()
        self.openMainApp()
    }
    
    override func backTapped() {
        _ = ViewControllerProvider.createLoginViewController(rootViewController: self)
        super.backTapped()
    }
    
    @IBAction func useLoginAndPassword(_ sender: UIButton) {
        self.backTapped()
    }
    
    @objc private func checkBiometry() {
        self.viewModel.checkBiometry(completion: { [weak self] error in
            if error == nil {
                self?.loadUser()
            }
        })
    }
}

private extension LoginPinViewController {
    
    func loadUser() {
        self.startProgress()
        self.viewModel.user(completionHandler: { [weak self] response in
            switch response {
            case .success(_):
                self?.loadInvestedProjects()
            case .failure(let error):
                self?.stopProgress()
                self?.presentAlert(message: error.localizedDescription)
            }
        })
    }
    
    func loadInvestedProjects() {
        self.viewModel.loadInvestedProjects(completionHandler: { [weak self] error in
            if let error = error {
                self?.stopProgress()
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.loadNotifications()
            }
        })
    }
    
    func loadNotifications() {
        self.viewModel.loadNotifications(completionHandler: { [weak self] error in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.openNext()
            }
        })
    }
}
