//
//  ConfirmForgotPasswordViewController.swift
//  8Pitch
//
//  Created by 8pitch on 10/19/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class ConfirmForgotPasswordViewController: SMSViewController {
    
    private lazy var viewModel : ConfirmForgotPasswordViewModel = {
        ConfirmForgotPasswordViewModel(session: session)
    }()
    
    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.resendSMS)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.title = "restorePassword.enterEmail.title".localized
        self.customTitle = "restorePassword.confirm.customTitle".localized
    }
    
    override func nextButtonTapped() {
        super.nextButtonTapped()
        self.presentAlert(title: "", message: "restorePassword.info.linkSent".localized, completion:  { [weak self] in
            guard let login = self?.previousController?.previousController else { return }
            self?.previousController = login
            self?.popToPreviousController(animated: true)
        })
    }
    
    override func verifyToken() {
        super.verifyToken()
        guard let _ = try? self.model?.value(), let session = self.session else {
            return
        }
        self.viewModel.verifyToken(session) { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.nextButtonAction?.onNext(.next)
                return
            }
            self?.presentAlert(message: error.localizedDescription)
            self?.nextButtonAction?.onNext(.error)
        }
    }
    
    override func resendCode() {
        super.resendCode()
        guard let session = self.session else {
            return
        }
        self.remote.resendCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.stopProgress()
            }).disposed(by: self.disposables)
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CodeTextCell.nibForRegistration, forCellWithReuseIdentifier: CodeTextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.resendButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
}
