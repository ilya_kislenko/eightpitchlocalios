//
//  ConfirmForgotPasswordViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 10/19/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class ConfirmForgotPasswordViewModel: GenericViewModel {
    
    func verifyToken(_ session: Session, completionHandler: @escaping ErrorClosure) {
        self.remote.passwordRecoveryLink(session, completionHandler: completionHandler)
    }
    
    

}
