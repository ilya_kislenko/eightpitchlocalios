//
//  ForgotPasswordViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 10/19/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class ForgotPasswordViewModel: GenericViewModel {
    
    override func verifyEmail(_ session: Session, completionHandler: @escaping ErrorClosure) {
        super.verifyEmail(session, completionHandler: completionHandler)
        self.remote.passwordRecoveryInitial(session, completionHandler: completionHandler)
    }
    
}
