//
//  ForgotPasswordViewController.swift
//  8Pitch
//
//  Created by 8pitch on 10/19/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift

class ForgotPasswordViewController: EmailViewController {
    
    private lazy var viewModel : ForgotPasswordViewModel = {
        ForgotPasswordViewModel(session: session)
    }()
    
    override var navigationFooterViewType: PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "restorePassword.enterEmail.title".localized
        self.customTitle = "restorePassword.enterEmail.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func setupProgressView() {}
    
    override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupConfirmForgotPasswordViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func resetModel() {
        super.resetModel()
    }
    
    override func verifyEmail() {
     super.verifyEmail()
         guard let model = try? self.model?.value(), let session = self.session else {
             return
         }
         self.viewModel.verifyEmail(session) { [weak self] error in
             self?.stopProgress()
             guard let error = error else {
                 self?.nextButtonAction?.onNext(.next)
                 return
             }
             self?.presentAlert(message: error.localizedDescription)
             self?.nextButtonAction?.onNext(.error)
         }
     }

}
