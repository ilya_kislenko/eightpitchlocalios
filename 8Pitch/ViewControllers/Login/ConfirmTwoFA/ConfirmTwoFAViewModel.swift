//
//  ConfirmTwoFAViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 05.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class ConfirmTwoFAViewModel: GenericViewModel {
    
    func verifyToken(_ session: Session, completionHandler: @escaping ErrorClosure) {
        if session.updatePasswordNeeded {
            self.verifyUpdatePasswordTwoFACode(completionHandler: completionHandler)
        } else {
            self.verifyTwoFACode(completionHandler: completionHandler)
        }
    }
    
    func verifyTwoFACode(completionHandler: @escaping ErrorClosure) {
        self.remote.verifyTwoFACode(session) { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
    }
    
    func verifyUpdatePasswordTwoFACode(completionHandler: @escaping ErrorClosure) {
        self.remote.verifyTwoFAUpdatePasswordCode(session) { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
    }
}
