//
//  ConfirmTwoFAViewController.swift
//  8Pitch
//
//  Created by 8pitch on 05.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class ConfirmTwoFAViewController: AbstractCollectionViewController<Code> {
    
    @IBOutlet weak var infoLabel: InfoCurrencyLabel!
    
    private lazy var viewModel : ConfirmTwoFAViewModel = {
        ConfirmTwoFAViewModel(session: session)
    }()
    
    override var model: BehaviorSubject<Code>? {
        return self.session?.codeProvider
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            guard value else { return }
                            self.startProgress()
                            self.verifyToken()
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var applyChangesFooterViewType : PublishSubject<ApplyChangesView.ViewType?>? {
        didSet {
            guard let typeAction = self.applyChangesFooterViewType else { return }
            typeAction.onNext(.logIn)
        }
    }
    
    // MARK: Overrides
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.collectionView.reloadData()
        self.disposables = DisposeBag()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        try? self.session?.codeProvider.value().cleanUp()
        self.title = "login.title".localized
        self.customTitle = "confirm.SMS.title".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.info), style: .plain, target: self, action: #selector(self.showInfo))
        try? self.model?.value().isValid.subscribe(onNext: {
            [weak self] in
            switch $0 {
            case .success(let value):
                if value {
                    self?.startProgress()
                    self?.verifyToken()
                }
            default: return
            }
        }).disposed(by: disposables)
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CodeTextCell.nibForRegistration, forCellWithReuseIdentifier: CodeTextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = ApplyChangesView.reuseIdentifier
        layout.register(ApplyChangesView.nibForRegistration, forDecorationViewOfKind: ApplyChangesView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: CodeError.self, cellReuseIdentifier: CodeTextCell.reuseIdentifier),
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func showInfo() {
        let _ = ViewControllerProvider.setupTwoFAInfoViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    // MARK: Actions
    
    override func nextButtonTapped() {
        super.nextButtonTapped()
        guard let session = self.session else { return }
        if session.rememberMeSelected || session.updatePasswordNeeded {
            self.openSetupQuickLoginOrUpdatePassword()
        } else {
            self.loadUser()
        }
    }
    
    private func openSetupQuickLoginOrUpdatePassword() {
        guard let session = self.session else { return }
        if session.updatePasswordNeeded {
            self.updatePassword()
        } else {
            self.openSetupQuickLogin()
        }
    }
    
    private func updatePassword() {
        _ = ViewControllerProvider.setupSetNewPasswordViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func verifyToken() {
        guard let session = self.session else {
            return
        }
        self.viewModel.verifyToken(session) { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.nextButtonTapped()
                return
            }
            self?.presentAlert(message: error.localizedDescription)
        }
    }
    
    func loadUser() {
        guard let session = self.session else {
            return
        }
        self.startProgress()
        self.remote.user(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    self?.loadInvestedProjects()
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                    self?.nextButtonAction?.onNext(.error)
                }
            }).disposed(by: self.disposables)
    }
    
    func loadInvestedProjects() {
        guard let session = self.session, let model = try? self.model?.value() else {
            return
        }
        self.loadInvestedProjects(session) { (error) in
            if let error = error {
                self.presentAlert(message: error.localizedDescription)
                model.isValid.onNext(.failure(error))
                self.nextButtonAction?.onNext(.error)
            } else {
                self.openMainApp()
            }
        }
    }
    
    func loadInvestedProjects(_ session: Session?, completionHandler: @escaping ErrorClosure) {
        guard let session = session else {
            return
        }
        self.session = session
        self.remote.investedProjects(session) { response, error in
            completionHandler(error)
        }
    }
    
}
