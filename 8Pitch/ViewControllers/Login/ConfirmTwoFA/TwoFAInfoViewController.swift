//
//  TwoFAInfoViewController.swift
//  8Pitch
//
//  Created by 8pitch on 06.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class TwoFAInfoViewController: GenericViewController, SheetDetentPresentationSupport {
    
    @IBOutlet weak var infoLabel: InfoLabel!
    
    var contentHeight: CGFloat {
        let size = self.infoLabel.sizeThatFits(self.infoLabel.frame.size)
        return self.infoLabel.frame.origin.y + size.height + Constants.inset
    }
        
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.modalPresentationStyle = .custom
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = Constants.cornerRadius
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.close))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layer.cornerRadius = Constants.cornerRadius
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (self.transitioningDelegate as? CustomTitleViewController)?.removeBlurEffect()
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private enum Constants {
        static let cornerRadius: CGFloat = 10
        static let inset: CGFloat = 24
    }
    
    
}
