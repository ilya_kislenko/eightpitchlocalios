//
//  ConfirmSMSViewController.swift
//  8Pitch
//
//  Created by 8pitch on 24.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift
import Remote

class ConfirmSMSViewController: SMSViewController {
    
    @IBOutlet weak var infoLabel: InfoCurrencyLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.title = "login.title".localized
        self.customTitle = "confirm.SMS.title".localized
    }
    
    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.resendSMS)
        }
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CodeTextCell.nibForRegistration, forCellWithReuseIdentifier: CodeTextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.resendButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: CodeError.self, cellReuseIdentifier: CodeTextCell.reuseIdentifier),
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func nextButtonTapped() {
        super.nextButtonTapped()
        if (try? self.session?.loginProvider.value().rememberMeSelected) ?? false {
            self.openSetupQuickLogin()
        } else {
            self.loadUser()
        }
    }
    
    override func verifyToken() {
        super.verifyToken()
        guard let session = self.session else { return }
        if session.updatePasswordNeeded {
            self.verifyUpdatePasswordToken()
        } else {
            self.verifyLoginToken()
        }
    }
    
    private func verifyLoginToken() {
        guard let session = self.session else { return }
        self.remote.verifyLoginToken(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(let value):
                    if case .hasMetadata = value.loginState {
                        self?.nextButtonTapped()
                    } else {
                        self?.confirmTwoFAorUpdatePassword()
                    }
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                }
                
            }).disposed(by: self.disposables)
    }
    
    private func verifyUpdatePasswordToken() {
        guard let session = self.session else { return }
        self.remote.verifyUpdatePasswordToken(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(let value):
                    if case .hasMetadata = value.loginState {
                        self?.loadUser()
                    } else {
                        self?.confirmTwoFAorUpdatePassword()
                    }
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                }
                
            }).disposed(by: self.disposables)
    }
    
    override func resendCode() {
        super.resendCode()
        self.resendLimitCode()
    }
}

private extension ConfirmSMSViewController {
    
    func resendLimitCode() {
        guard let session = self.session else { return }
        self.remote.resendLimitCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    return
                case .failure(let error):
                    let limitReached = error as? SendCodeError == SendCodeError.limit
                    self?.presentAlert(message: error.localizedDescription,
                                       completion: limitReached ?
                                        { [weak self] in self?.popToPreviousController(animated: true)} : {} )
                }
            }).disposed(by: self.disposables)
    }
    
    func resendUpdatePasswordCode() {
        guard let session = self.session else { return }
        self.remote.resendUpdatePasswordCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    return
                case .failure(let error):
                    let limitReached = error as? SendCodeError == SendCodeError.limit
                    self?.presentAlert(message: error.localizedDescription,
                                       completion: limitReached ?
                                        { [weak self] in self?.popToPreviousController(animated: true)} : {} )
                }
            }).disposed(by: self.disposables)
    }
    
    func confirmTwoFAorUpdatePassword() {
        guard let session = self.session else { return }
        if session.updatePasswordNeeded && !session.verificationTypes.contains(.totp) {
            self.updatePassword()
        } else {
            self.confirmTwoFA()
        }
    }
    
    func updatePassword() {
        _ = ViewControllerProvider.setupSetNewPasswordViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func confirmTwoFA() {
        _ = ViewControllerProvider.setupConfirmTwoFAViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func loadUser() {
        guard let session = self.session else {
            return
        }
        self.startProgress()
        self.remote.user(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { response in
                self.stopProgress()
                switch response {
                case .success(_):
                    self.loadNotifications()
                case .failure(let error):
                    self.stopProgress()
                    self.presentAlert(message: error.localizedDescription)
                    self.nextButtonAction?.onNext(.error)
                }
            }).disposed(by: self.disposables)
    }
    
    func loadInvestedProjects() {
        guard let session = self.session, let model = try? self.model?.value() else {
            return
        }
        self.loadInvestedProjects(session) { [weak self] (error) in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            } else {
                self?.openMainApp()
            }
        }
    }
    
    func loadInvestedProjects(_ session: Session?, completionHandler: @escaping ErrorClosure) {
        guard let session = session else {
            return
        }
        self.session = session
        self.startProgress()
        self.remote.investedProjects(session) { [weak self] response, error in
            self?.stopProgress()
            completionHandler(error)
        }
    }
    
    func loadNotifications() {
        guard let session = self.session else {
            return
        }
        self.loadNotifications(session) { [weak self] error in
            if let error = error {
                self?.stopProgress()
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            } else {
                self?.loadInvestedProjects()
            }
        }
    }
    
    func loadNotifications(_ session: Session?, completionHandler: @escaping ErrorClosure) {
        guard let session = session else { return }
        self.session = session
        self.remote.notifications(session) { response, error in
            _ = !session.notificationsProvider.notifications.filter({ $0.status != .read }).isEmpty
            completionHandler(error)
        }
    }
    
}
