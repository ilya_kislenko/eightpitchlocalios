//
//  LoginViewController.swift
//  8Pitch_LOG
//
//  Created by 8pitch on 14.07.2020.
//  Copyright © 2020 8pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class LoginViewController: AbstractCollectionViewController<Login> {
    
    private lazy var viewModel : LoginViewModel = {
        LoginViewModel(session: session)
    }()
    
    override var model: BehaviorSubject<Login>? {
        self.session?.loginProvider
    }
    
    // TODO: workaround for navigation issue
    var hasMetadata : Bool = false
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        try? self.model?.value().validate()
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            guard value else { return }
                            self.startProgress()
                            self.login()
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    private var forgotButtonAction : BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.forgotButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: { [weak self] in
                    self?.stopProgress()
                    switch $0 {
                    case .error: return
                    case .loading:
                        self?.openForgotPasswordFlow()
                    case .next: return
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    private var rememberButtonAction : BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.rememberButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: { [weak self] in
                    switch $0 {
                    case .error: try? self?.model?.value().rememberMeSelected = false
                    case .loading: return
                    case .next: try? self?.model?.value().rememberMeSelected = true
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.password)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "login.title".localized
        self.customTitle = "login.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.reloadData()
    }
    
    override func backTapped() {
        _ = ViewControllerProvider.createStartViewController(rootViewController: self)
        super.backTapped()
    }
    
    override func setupModel() {
        super.setupModel()
        self.session = .init(registrationType: .none)
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(LoginInputPhoneCell.nibForRegistration, forCellWithReuseIdentifier: LoginInputPhoneCell.reuseIdentifier)
        self.collectionView.register(InputPasswordCell.nibForRegistration, forCellWithReuseIdentifier: InputPasswordCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
        layout.forgotButtonAction.subscribe(onNext: { [weak self] in
            self?.forgotButtonAction = $0
        }).disposed(by: self.disposables)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.rememberButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: PhoneError.self, cellReuseIdentifier: LoginInputPhoneCell.reuseIdentifier),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: PasswordError.self, cellReuseIdentifier: InputPasswordCell.reuseIdentifier)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        if let cell = cell as? LoginInputPhoneCell {
            cell.delegate = self
        }
        return cell
    }
    
    override func nextButtonTapped() {
        super.nextButtonTapped()
        guard let session = session else { return }
        if session.updatePasswordNeeded && session.verificationTypes.isEmpty {
            self.updatePassword()
        } else if session.verificationTypes.first == .totp {
            self.confirmTwoFA()
        } else {
            self.confirmSMS()
        }
    }
    
    override func popToPreviousController(animated: Bool) {
        
        guard let model = try? self.model?.value(), let isValid = try? model.isValid.value() else {
            fatalError("misconfigured controller!")
        }
        switch isValid {
        case .success(let value) where value:
            
            if self.hasMetadata {
                self.popToMockViewController()
                self.resetModel()
                return
            }
            // TODO: don't validate code in Login screen. Fix navigation issue.
            guard let isValdCode = try? self.session?.codeProvider.value().isValid.value(),
                  case .success(let value) = isValdCode, value else {
                super.popToPreviousController(animated: animated)
                self.resetModel()
                return
            }
            self.popToMockViewController()
            
        default: super.popToPreviousController(animated: animated)
        }
        self.resetModel()
    }
    
    override func verifyModelAndNextButtonActionIfPossible() {
        guard let model = try? self.model?.value(),
              let isValid = try? model.isValid.value() else { return }
        switch isValid {
        case .success(let entered) where !entered: self.nextButtonTapped()
        case .success(let entered) where entered: self.openMainAppOrQuickLogin()
        case .failure(let error as ModelError): self.presentAlert(message: error.localizedDescription)
        default: return
        }
    }
}

extension LoginViewController: CodeListDelegate {
    func openCodeList() {
        let _ = ViewControllerProvider.setupCodesListViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}

private extension LoginViewController {
    func popToMockViewController() {
        self.navigationController?.popToRootViewController(animated: false)
        guard let nextController = self.nextController else {
            print("invalid self.nextController setup!")
            return
        }
        self.previousController?.navigationController?.pushViewController(nextController, animated: true)
    }
    
    func confirmSMS() {
        let _ = ViewControllerProvider.setupConfirmSMSViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func confirmTwoFA() {
        let _ = ViewControllerProvider.setupConfirmTwoFAViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openMainAppOrQuickLogin() {
        if (try? self.session?.loginProvider.value().rememberMeSelected) ?? false {
            self.openSetupQuickLogin()
        } else {
            self.openMainApp()
        }
    }
    
    func updatePassword() {
        _ = ViewControllerProvider.setupSetNewPasswordViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openForgotPasswordFlow() {
        _ = ViewControllerProvider.setupForgotPasswordViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func login() {
        guard let model = try? self.model?.value() else { return }
        self.startProgress()
        self.viewModel.login(completionHandler: { [weak self] response in
            switch response {
            case .success(let value):
                model.isValid.onNext(.success(true))
                if case .hasMetadata = value.loginState {
                    self?.loadUser()
                } else {
                    self?.stopProgress()
                    self?.hasMetadata = false
                    // TODO: hack due to the lack of the extended Login state
                    // i.e. since user can go back and should be able to go forward w/o editing phone
                    self?.nextButtonTapped()
                }
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            }
        })
    }
    
    func loadUser() {
        self.startProgress()
        self.viewModel.user(completionHandler: { [weak self] response in
            switch response {
            case .success(_):
                self?.loadInvestedProjects()
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            }
        })
    }
    
    func loadInvestedProjects() {
        self.viewModel.loadInvestedProjects(completionHandler: { [weak self] error in
            if let error = error {
                self?.stopProgress()
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            } else {
                self?.loadNotifications()
            }
        })
    }
    
    func loadNotifications() {
        self.viewModel.loadNotifications(completionHandler: { [weak self] error in
            if let error = error {
                self?.stopProgress()
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            } else {
                self?.hasMetadata = true
                self?.nextButtonAction?.onNext(.next)
            }
        })
    }
    
}
