//
//  VotingOnlyViewController.swift
//  8Pitch
//
//  Created by 8pitch on 12.04.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Remote

class VotingOnlyViewController: CustomTitleViewController {
    
    @IBOutlet weak var titleLabel: ProjectsTitleLabel!
    @IBOutlet weak var infoLabel: InfoLabel!
    @IBOutlet weak var loginButton: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(close))
        self.navigationItem.rightBarButtonItem = nil
        self.titleLabel.text = "voting.error.noLogin.title".localized
        self.infoLabel.text = "voting.error.noLogin.text".localized
        self.loginButton.setTitle("voting.error.noLogin.button".localized.uppercased(), for: .normal)
    }
    
    @objc private func close() {
        self.logout()
    }
    
    @IBAction func login(_ sender: RoundedButton) {
        guard let url = URL(string: WebURLConstants.login) else { return }
        UIApplication.shared.open(url)
    }
}
