//
//  EditViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 12/30/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class EditViewModel: GenericViewModel {
    
    struct EditProfileSection {
        let headerTitle: String?
        let title: String?
        let type: EditCellType?
    }

    var editSectionsContent: [EditProfileSection] = []
    
    override var info: KYC? {
        let _ = super.info
        return try? self.session.KYCProvider.value()
    }

    required init(session: Session?) {
        super.init(session: session)
        let editSections = self.sectionsContent.filter {
            $0.type != .upgrade && $0.type != .contact && $0.type != .company
        }
        for section in editSections {
            let header = EditProfileSection(headerTitle: section.headerTitle, title: nil, type: nil)

            self.editSectionsContent.append(header)
            section.rowsContent.forEach {
                if $0.imageName != .KYCStreet {
                self.editSectionsContent.append(EditProfileSection(headerTitle: nil, title: $0.title, type: $0.type))
                }
            }
        }
    }

}
