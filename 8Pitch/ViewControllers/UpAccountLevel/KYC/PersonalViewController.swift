//
//  PersonalViewController.swift
//  8Pitch
//
//  Created by 8pitch on 29.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxCocoa
import RxSwift

class PersonalViewController: NameViewController {
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: move to better error handling
                    case .loading:
                        try? self.model?.value().validate()
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            print(value)
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var model: BehaviorSubject<NameAndLastName>? {
        try? self.session?.KYCProvider.value().nameAndLastNameProvider
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customTitle = "kyc.1.customTitle".localized
        self.title = "kyc.1.title".localized
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeKYCFlow))
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = 3
    }
    
    override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupBirthViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}


