//
//  SummarizeViewController.swift
//  8Pitch
//
//  Created by 8pitch on 09.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import RxSwift

class SummarizeViewController: CustomTitleViewController {
    
    // MARK: UI Components
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Properties
    
    var kycStepsCount: Int {
        switch self.session?.accountGroup {
        case .initiator:
            return 4
        default:
            return 5
        }
    }
    
    private var dataSource: [[SummaryData]] = []
    private var savedData: SavedData?

    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDataSource()
        self.title = "summarize.title".localized
        self.customTitle = "summarize.customTitle".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.edit), style: .plain, target: self, action: #selector(self.openPageEditing))
        self.navigationItem.customizeForWhite()
        let text = NSMutableAttributedString()
        text.append(NSAttributedString(string: "summarize.info.1".localized, attributes: [.foregroundColor : UIColor(.gray), .font : UIFont.roboto.regular.labelSize]))
        let attributedText = NSAttributedString(string: "summarize.info.2".localized, attributes: [.foregroundColor: UIColor(.red), .strokeColor: UIColor(.red), .font : UIFont.roboto.regular.labelSize])
        text.append(attributedText)
        self.tableView.register(SummarizeCell.nibForRegistration, forCellReuseIdentifier: SummarizeCell.reuseIdentifier)
        self.tableView.register(KYCSummaryHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: KYCSummaryHeaderView.reuseIdentifier)
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateSession()
        self.tableView.reloadData()
    }
    
    public func setupDataSource() {
        guard let kyc = try? self.session?.KYCProvider.value() else { return }
        self.savedData = SavedData(title: kyc.title.rawValue.localized, firstName: kyc.firstName, lastName: kyc.lastName, date: kyc.dateOfBirth, place: kyc.placeOfBirth, nationality: kyc.nationality, country: kyc.country, city: kyc.city, zip: kyc.zip, street: kyc.street, number: kyc.streetNumber, accountOwner: kyc.accountOwner, iban: kyc.iban, pep: kyc.politicallyExposedPerson, ustl: kyc.usTaxLiability)
        let infoTitle : [SummaryData] = []
        let personalInfo = [
            SummaryData(image: UIImage(.KYCuser), key: "summarize.header.personal.name".localized, value: "\(kyc.firstName) \(kyc.lastName)"),
            SummaryData(image: UIImage(.KYCcalendar), key: "summarize.header.personal.birth".localized, value: kyc.dateOfBirth),
            SummaryData(image: UIImage(.KYCnationality), key: "summarize.header.personal.nationality".localized, value: kyc.nationality)
        ]
        let address = [
            SummaryData(image: UIImage(.KYCflag), key: "summarize.header.address.country".localized, value: kyc.country),
            SummaryData(image: UIImage(.KYCpin), key: "summarize.header.address.city".localized, value: kyc.city),
            SummaryData(image: UIImage(.KYCzip), key: "summarize.header.address.zip".localized, value: kyc.zip),
            SummaryData(image: UIImage(.KYCStreet), key: "summarize.header.address.street".localized, value: kyc.street),
            SummaryData(image: UIImage(.KYCnumber), key: "summarize.header.address.number".localized, value: kyc.streetNumber)
        ]
        let financialInfo = [
            SummaryData(image: UIImage(.KYCowner), key: "summarize.header.financial.owner".localized, value: kyc.accountOwner),
            SummaryData(image: UIImage(.KYCiban), key: "summarize.header.financial.iban".localized, value: kyc.iban)
        ]
        
        let otherInfo = [
            SummaryData(image: UIImage(.profileLaw), key: "info.details.title".localized, value: (kyc.politicallyExposedPerson ?? false) ? "profile.personal.yes".localized : "profile.personal.no".localized),
            SummaryData(image: UIImage(.profileTaxes), key: "profile.personal.ustl".localized, value: (kyc.usTaxLiability ?? false) ? "profile.personal.yes".localized : "profile.personal.no".localized)
        ]
        self.dataSource = [infoTitle, personalInfo, address, financialInfo, otherInfo]
    }
    
}

// MARK: UITableViewDelegate, UITableViewDataSource

extension SummarizeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SummarizeCell", for: indexPath) as? SummarizeCell else {
            fatalError("SummarizeCell is not exist")
        }
        cell.setupWith(dataSource[indexPath.section][indexPath.row])
        if indexPath.row == dataSource[indexPath.section].count - 1 {
            cell.hideSeparator()
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        dataSource.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UITableViewHeaderFooterView()
        view.tintColor = UIColor(.white)
        view.contentView.backgroundColor = UIColor(.white)
        self.makeSeparatorFor(view, ofHeaderType: false)
        if section == dataSource.count - 1 {
            let button = RoundedButton()
            button.backgroundColor = UIColor(.red)
            button.titleLabel?.textColor = UIColor(.white)
            button.setTitle("summarize.button.title".localized, for: .normal)
            button.addTarget(self, action: #selector(confirmButtonTapped), for: .touchUpInside)
            view.addSubview(button)
            button.snp.makeConstraints {
                $0.top.bottom.equalToSuperview().inset(Constants.offset)
                $0.trailing.leading.equalToSuperview().inset(Constants.sideOffset)
            }
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: KYCSummaryHeaderView.reuseIdentifier) as? KYCSummaryHeaderView
            view?.setup(stepsCount: kycStepsCount)
            return view
        } else {
            let view = UITableViewHeaderFooterView()
            view.tintColor = UIColor(.white)
            view.contentView.backgroundColor = UIColor(.white)
            let label = UILabel()
            label.textColor = UIColor(.gray)
            label.font = UIFont.barlow.regular.labelSize
            view.addSubview(label)
            label.snp.makeConstraints {
                $0.trailing.leading.equalToSuperview().inset(Constants.sideOffset)
                $0.bottom.equalToSuperview().inset(Constants.labelOffset)
            }
            label.text = Constants.sections[section]
            self.makeSeparatorFor(view, ofHeaderType: true)
            if section != 1 {
                self.makeSeparatorFor(view, ofHeaderType: false)
            }
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == dataSource.count - 1 {
            return Constants.footerHeight
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        section == 0 ? Constants.titleHeaderHeight : Constants.headerHeight
    }
    
}

// MARK: Private

private extension SummarizeViewController {
    
    enum Constants {
        static let numberOfSections: Int = 3
        static let titleHeaderHeight: CGFloat = 160
        static let headerHeight: CGFloat = UIScreen.main.bounds.width > 320 ? 88 : 44
        static let footerHeight: CGFloat = UIScreen.main.bounds.width > 320 ? 120 : 100
        static let sideOffset: CGFloat = 24
        static let offset: CGFloat = 32
        static let labelOffset: CGFloat = 15
        static let sections: [String] = ["summarize.header.title".localized,
                                         "summarize.header.personal".localized,
                                         "summarize.header.address".localized,
                                         "summarize.header.financial".localized,
                                         "profile.personal.edit.other".localized]
    }
    
    @objc func openPageEditing() {
        _ = ViewControllerProvider.setupEditViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    @objc func confirmButtonTapped() {
        self.requestAction()
    }
    
    func openSuccessScreen() {
        let _ = ViewControllerProvider.setupKYCSuccessViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func makeSeparatorFor(_ superview: UIView, ofHeaderType: Bool) {
        let view = UIView()
        view.backgroundColor = UIColor(.separatorGray)
        superview.addSubview(view)
        view.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(1)
            if ofHeaderType {
                $0.bottom.equalToSuperview()
            } else {
                $0.top.equalToSuperview()
            }
        }
    }
}

// MARK: Networking

extension SummarizeViewController {
    
    func updateSession() {
        guard let kyc = try? self.session?.KYCProvider.value() else { return }
        try? kyc.nameAndLastNameProvider.value().titleProvider.onNext(self.savedData?.title)
        try? kyc.nameAndLastNameProvider.value().firstNameProvider.onNext(self.savedData?.firstName)
        try? kyc.nameAndLastNameProvider.value().lastNameProvider.onNext(self.savedData?.lastName)
        try? kyc.birthDayAndPlaceProvider.value().dateProvider.onNext(self.savedData?.date)
        try? kyc.birthDayAndPlaceProvider.value().placeProvider.onNext(self.savedData?.place)
        try? kyc.personalDetailsProvider.value().nationalityProvider.onNext(self.savedData?.nationality)
        try? kyc.localityProvider.value().countryProvider.onNext(self.savedData?.country)
        try? kyc.localityProvider.value().cityProvider.onNext(self.savedData?.city)
        try? kyc.localityProvider.value().zipProvider.onNext(self.savedData?.zip)
        try? kyc.addressProvider.value().streetProvider.onNext(self.savedData?.street)
        try? kyc.addressProvider.value().streetNoProvider.onNext(self.savedData?.number)
        try? kyc.financialInfoProvider.value().accountOwnerProvider.onNext(self.savedData?.accountOwner)
        try? kyc.financialInfoProvider.value().ibanProvider.onNext(self.savedData?.iban)
        try? kyc.personalDetailsProvider.value().usTaxLiabilityProvider.onNext(self.savedData?.ustl)
        try? kyc.personalDetailsProvider.value().politicallyExposedPersonProvider.onNext(self.savedData?.pep)
    }
    
    func requestAction() {
        guard let session = self.session else { return }
        self.startProgress()
        self.remote.webID(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {
                self.stopProgress()
            switch $0 {
                case .success(_):
                    self.openSuccessScreen()
                case .failure(let error):
                    self.presentAlert(message: error.localizedDescription)
                }
            }).disposed(by: self.disposables)
    }
    
}

struct SavedData {
    let title: String
    let firstName: String
    let lastName: String
    let date: String?
    let place: String?
    let nationality: String?
    let country: String?
    let city: String?
    let zip: String?
    let street: String?
    let number: String?
    let accountOwner: String?
    let iban: String?
    let pep: Bool?
    let ustl: Bool?
}
