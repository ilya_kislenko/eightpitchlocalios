//
//  FinancialViewController.swift
//  8Pitch
//
//  Created by 8pitch on 01.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class FinancialViewController: AbstractCollectionViewController<FinancialInfo> {
    
    // MARK: Properties
    
    override var model: BehaviorSubject<FinancialInfo>? {
        return try? self.session?.KYCProvider.value().financialInfoProvider
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        try? self.model?.value().validate()
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            guard value else { return }
                            self.verifyModelAndNextButtonActionIfPossible()
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var navigationFooterViewType: PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    // MARK: Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "financialInfo.title".localized
        self.customTitle = "financialInfo.customTitle".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.info), style: .plain, target: self, action: #selector(self.showInfo))
        self.navigationItem.customizeForWhite()
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = 3
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(AccountOwnerCell.nibForRegistration, forCellWithReuseIdentifier: AccountOwnerCell.reuseIdentifier)
        self.collectionView.register(IBANCell.nibForRegistration, forCellWithReuseIdentifier: IBANCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: AccountOwnerError.self, cellReuseIdentifier: AccountOwnerCell.reuseIdentifier),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: IBANError.self, cellReuseIdentifier: IBANCell.reuseIdentifier)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    @objc override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupSummarizeViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func showInfo() {
        let _ = ViewControllerProvider.setupInfoViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}
