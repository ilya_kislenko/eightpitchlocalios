//
//  EditViewController.swift
//  8Pitch
//
//  Created by 8pitch on 11.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class EditViewController: AbstractCollectionViewController<KYC> {
    
    // MARK: Properties
    
    override var model: BehaviorSubject<KYC>? {
        self.session?.KYCProvider
    }
    
    private lazy var viewModel : EditViewModel = {
        EditViewModel(session: session)
    }()
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        try? self.model?.value().validate(forSecondLevel: true, forInstitutionalInvestor: (self.session?.user?.isInstitutionalInvestor) ?? false, forKYC: true)
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            guard value else { return }
                            self.verifyModelAndNextButtonActionIfPossible()
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var applyChangesFooterViewType : PublishSubject<ApplyChangesView.ViewType?>? {
        didSet {
            guard let typeAction = self.applyChangesFooterViewType else { return }
            typeAction.onNext(.applyChanges)
        }
    }
    
    // MARK: Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "KYC.edit.title".localized
        self.customTitle = "KYC.edit.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.reloadData()
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(EditCell.nibForRegistration, forCellWithReuseIdentifier: EditCell.reuseIdentifier)
        self.collectionView.register(EditHeaderCell.nibForRegistration, forCellWithReuseIdentifier: EditHeaderCell.reuseIdentifier)
        self.collectionView.register(PoliticallyExposedPersonCell.nibForRegistration, forCellWithReuseIdentifier: PoliticallyExposedPersonCell.reuseIdentifier)
        self.collectionView.register(TaxCell.nibForRegistration, forCellWithReuseIdentifier: TaxCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = ApplyChangesView.reuseIdentifier
        layout.register(ApplyChangesView.nibForRegistration, forDecorationViewOfKind: ApplyChangesView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        var sections: [CollectionViewModel<KYC>] = []
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: TitleError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: FirstNameError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: LastNameError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: BirthDayError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: PersonalDetailsError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: BirthPlaceError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: CountryError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: LocalityError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: ZipError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: AddressError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: AccountOwnerError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: IBANError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: PoliticallyExposedPersonCell.reuseIdentifier))
        sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: TaxCell.reuseIdentifier))
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    @objc override func nextButtonTapped() {
        self.saveEditing()
        self.popToPreviousController(animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        if let headerCell = cell as? EditHeaderCell {
            headerCell.setTitle(self.viewModel.editSectionsContent[indexPath.section].headerTitle)
        } else if let inputCell = cell as? EditCell {
            inputCell.setup(for: self.viewModel.editSectionsContent[indexPath.section].type ?? .firstName)
        } else if let switchCell = cell as? CellModel<PersonalDetails> {
            switchCell.model = try? self.model?.value().personalDetailsProvider
        }
        return cell
    }
    
    private func saveEditing() {
        (self.previousController as? SummarizeViewController)?.setupDataSource()
    }
    
    enum Constants {
        static let sectionHeader: [Int: String] = [0: "summarize.header.personal".localized,
                                                   5: "summarize.header.address".localized]
        static let cellTypes: [Int: EditCellType] = [1: .firstName,
                                                     2: .lastName,
                                                     3: .birth,
                                                     4: .nationality,
                                                     6: .country,
                                                     7: .city,
                                                     8: .zip,
                                                     9: .address]
    }
    
}

