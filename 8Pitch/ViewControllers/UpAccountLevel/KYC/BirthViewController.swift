//
//  BirthViewController.swift
//  8Pitch
//
//  Created by 8pitch on 28.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxCocoa
import RxSwift

class BirthViewController: AbstractCollectionViewController<BirthDayAndPlace> {
    
    // MARK: Properties
    
    override var model: BehaviorSubject<BirthDayAndPlace>? {
        try? self.session?.KYCProvider.value().birthDayAndPlaceProvider
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        try? self.model?.value().validate()
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            guard value else { return }
                            self.verifyModelAndNextButtonActionIfPossible()
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var navigationFooterViewType: PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    // MARK: Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customTitle = "kyc.2.customTitle".localized
        self.title = "kyc.2.title".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = 3
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(BirthdayCell.nibForRegistration, forCellWithReuseIdentifier: BirthdayCell.reuseIdentifier)
        self.collectionView.register(PlaceOfBirthCell.nibForRegistration, forCellWithReuseIdentifier: PlaceOfBirthCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: BirthDayError.self, cellReuseIdentifier: BirthdayCell.reuseIdentifier),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: BirthPlaceError.self, cellReuseIdentifier: PlaceOfBirthCell.reuseIdentifier)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupDetailsViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}

