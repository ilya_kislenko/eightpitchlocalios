//
//  VerifyEmailViewController.swift
//  8Pitch
//
//  Created by 8pitch on 29.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class VerifyEmailViewController: SMSViewController {
        
    @IBOutlet weak var infoLabel: InfoLabel!
        
    override var emailConfirmationFooterViewType : PublishSubject<VerifyEmailView.ViewType?>? {
        didSet {
            guard let typeAction = self.emailConfirmationFooterViewType else { return }
            typeAction.onNext(.confirm)
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupContent()
        self.sendCode()
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = 1
    }
        
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CodeTextCell.nibForRegistration, forCellWithReuseIdentifier: CodeTextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = VerifyEmailView.reuseIdentifier
        layout.register(VerifyEmailView.nibForRegistration, forDecorationViewOfKind: VerifyEmailView.reuseIdentifier)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.resendButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
    override func nextButtonTapped() {
        super.nextButtonTapped()
        let _ = ViewControllerProvider.setupEmailVerifiedViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func resendCode() {
        super.resendCode()
        guard let session = self.session else {
            return
        }
        self.remote.resendEmailCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.stopProgress()
            }).disposed(by: self.disposables)
    }
    
    override func verifyToken() {
        super.verifyToken()
        guard let session = self.session, let model = try? self.model?.value() else {
            return
        }
        self.remote.verifyEmailToken(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    self?.nextButtonAction?.onNext(.next)
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                }
            }).disposed(by: self.disposables)
    }
    
    override func sendCode() {
        super.sendCode()
        guard let _ = try? self.model?.value(), let session = self.session else {
            return
        }
        self.remote.sendEmailCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    return
                case .failure(_):
                    self?.nextButtonAction?.onNext(.error)
                }
            }).disposed(by: self.disposables)
    }
        
    private func setupContent() {
        self.customTitle = "verifyEmail.customTitle".localized
        self.title = "verifyEmail.title".localized
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeKYCFlow))
        self.navigationItem.customizeForWhite()
        let attribitedText = NSAttributedString(string: (self.session?.email ?? ""), attributes: [.font: UIFont.boldSystemFont(ofSize: 15), .foregroundColor: UIColor(.gray)])
        let text = NSMutableAttributedString()
        text.append(NSAttributedString(string: "verifyEmail.info".localized, attributes: [.font: UIFont.roboto.regular.labelSize, .foregroundColor: UIColor(.gray)]))
        text.append(attribitedText)
        self.infoLabel.attributedText = text
    }
}
