//
//  EmailVerifiedViewController.swift
//  8Pitch
//
//  Created by 8pitch on 30.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class EmailVerifiedViewController: RedViewController {
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.whiteClose), style: .plain, target: self, action: #selector(closeKYCFlow))
    }
    
    // MARK: Overrides
    
    override func popToPreviousController(animated: Bool) {
        guard let controller = self.previousController else {
            print("self.previousController == nil")
            return
        }
        UIApplication.shared.keyWindow?.rootViewController = controller
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
    // MARK: Actions
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        guard let session = self.session else { return }
        let _ = session.twoFAEnabled ? ViewControllerProvider.setupPersonalViewController(rootViewController: self) : ViewControllerProvider.setupAddSecurityViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}
