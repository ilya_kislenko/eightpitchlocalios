//
//  LiveViewController.swift
//  8Pitch
//
//  Created by 8pitch on 01.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class LocalityViewController: AbstractCollectionViewController<Locality> {
    
    // MARK: Properties
    
    override var model: BehaviorSubject<Locality>? {
        try? self.session?.KYCProvider.value().localityProvider
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        try? self.model?.value().validate()
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            guard value else {
                                try? self.model?.value().countryProvider.onNext("")
                                self.verifyModelAndNextButtonActionIfPossible()
                                return
                            }
                            self.verifyModelAndNextButtonActionIfPossible()
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var navigationFooterViewType: PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    // MARK: Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "locality.title".localized
        self.customTitle = "locality.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = 3
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CountryCell.nibForRegistration, forCellWithReuseIdentifier: CountryCell.reuseIdentifier)
        self.collectionView.register(CityAndZipCell.nibForRegistration, forCellWithReuseIdentifier: CityAndZipCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: CountryError.self, cellReuseIdentifier: CountryCell.reuseIdentifier),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: LocalityError.self, cellReuseIdentifier: CityAndZipCell.reuseIdentifier)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    @objc override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupAddressViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}
