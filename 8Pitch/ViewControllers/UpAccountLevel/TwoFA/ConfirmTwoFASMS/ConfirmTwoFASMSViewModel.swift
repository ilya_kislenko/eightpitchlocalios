//
//  ConfirmTwoFASMSViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 04.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class ConfirmTwoFASMSViewModel: GenericViewModel {
    
    var infoText: String {
        self.twoFAEnabled ?
            "TwoFA.sms.confirm.info.disable".localized : "TwoFA.sms.confirm.info.enable".localized
    }
    
    func enable2FA(_ session: Session, completionHandler: @escaping ErrorClosure) {
        self.remote.enableSMSAuth(session) { error in
            guard let error = error else {
                self.user() { response in
                    DispatchQueue.main.async {
                        switch response {
                        case .failure(let error):
                            completionHandler(error)
                        case .success(_):
                            completionHandler(nil)
                        }
                    }
                }
                return
            }
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
    }
    
    func disable2FA(_ session: Session, completionHandler: @escaping ErrorClosure) {
        self.remote.disableSMSAuth(session) { error in
            guard let error = error else {
                self.user() { response in
                    DispatchQueue.main.async {
                        switch response {
                        case .failure(let error):
                            completionHandler(error)
                        case .success(_):
                            completionHandler(nil)
                        }
                    }
                }
                return
            }
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
    }
    
    func toggleTwoFA(_ session: Session, completionHandler: @escaping ErrorClosure) {
        if self.twoFAEnabled {
            self.disable2FA(session, completionHandler: completionHandler)
        } else {
            self.enable2FA(session, completionHandler: completionHandler)
        }
    }
    
    func loadInvestedProjects(_ session: Session, completionHandler: @escaping ErrorClosure) {
        self.remote.investedProjects(session) { response, error in
            completionHandler(error)
        }
    }
    
    func loadNotifications(_ session: Session, completionHandler: @escaping ErrorClosure) {
        self.remote.notifications(session) { response, error in
            _ = !session.notificationsProvider.notifications.filter({ $0.status != .read }).isEmpty
            completionHandler(error)
        }
    }
    
}
