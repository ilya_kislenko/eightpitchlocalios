//
//  ConfirmTwoFASMSViewController.swift
//  8Pitch
//
//  Created by 8pitch on 04.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input
import Remote

class ConfirmTwoFASMSViewController: SMSViewController {
    
    private lazy var viewModel : ConfirmTwoFASMSViewModel = {
        ConfirmTwoFASMSViewModel(session: session)
    }()
    
    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.resendSMS)
        }
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CodeTextCell.nibForRegistration, forCellWithReuseIdentifier: CodeTextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.resendButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: CodeError.self, cellReuseIdentifier: CodeTextCell.reuseIdentifier),
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func nextButtonTapped() {
        super.nextButtonTapped()
        _ = ViewControllerProvider.setupSuccessTwoFAViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func resendCode() {
        super.resendCode()
        guard let session = self.session else {
            return
        }
        self.remote.resendLimitCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    return
                case .failure(let error):
                    let limitReached = error as? SendCodeError == SendCodeError.limit
                    self?.presentAlert(message: error.localizedDescription,
                                       completion: limitReached ?
                                        { [weak self] in self?.popToPreviousController(animated: true)} : {} )
                }
            }).disposed(by: self.disposables)
    }
 
    @IBOutlet weak var infoLabel: InfoCurrencyLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.title = "TwoFA.title".localized
        self.customTitle = "TwoFA.sms.confirm.customTitle".localized
        self.infoLabel.text = self.viewModel.infoText
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let accessToken = self.session?.accessToken,
              !accessToken.isEmpty,
              (try? self.session?.loginProvider.value().rememberMeSelected) ?? false else {
            return
        }
        self.openMainApp()
    }
    
    override func verifyToken() {
        super.verifyToken()
        guard let session = self.session else { return }
        self.viewModel.toggleTwoFA(session) { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.nextButtonTapped()
                return
            }
            self?.presentAlert(message: error.localizedDescription)
        }
    }
}

private extension ConfirmTwoFASMSViewController {
    
    func confirmTwoFA() {
        _ = ViewControllerProvider.setupConfirmTwoFAViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func loadUser() {
        self.startProgress()
        self.viewModel.user(completionHandler: { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success(_):
                self?.loadNotifications()
            case .failure(let error):
                self?.stopProgress()
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            }
        })
    }
    
    func loadInvestedProjects() {
        guard let session = self.session, let _ = try? self.model?.value() else {
            return
        }
        self.loadInvestedProjects(session) { [weak self] (error) in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            } else {
                self?.nextButtonAction?.onNext(.next)
            }
        }
    }
    
    func loadInvestedProjects(_ session: Session?, completionHandler: @escaping ErrorClosure) {
        guard let session = session else { return }
        self.startProgress()
        self.viewModel.loadInvestedProjects(session) { [weak self] error in
            self?.stopProgress()
            completionHandler(error)
        }
    }
    
    func loadNotifications() {
        guard let session = self.session else {
            return
        }
        self.loadNotifications(session) { [weak self] error in
            if let error = error {
                self?.stopProgress()
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            } else {
                self?.loadInvestedProjects()
            }
        }
    }
    
    func loadNotifications(_ session: Session?, completionHandler: @escaping ErrorClosure) {
        guard let session = session else { return }
        self.viewModel.loadNotifications(session) { error in
            _ = !session.notificationsProvider.notifications.filter({ $0.status != .read }).isEmpty
            completionHandler(error)
        }
    }
    
}

