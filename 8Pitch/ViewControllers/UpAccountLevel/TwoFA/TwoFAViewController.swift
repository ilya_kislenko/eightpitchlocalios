//
//  TwoFAViewController.swift
//  8Pitch
//
//  Created by 8pitch on 10/1/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class TwoFAViewController: AbstractCollectionViewController<TwoFAProvider> {
    
    override var model: BehaviorSubject<TwoFAProvider>? {
        .init(value: self.session?.twoFAProvider ?? TwoFAProvider())
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .subscribe(onNext: {
                    switch $0 {
                    case .loading:
                        self.nextButtonTapped()
                    default:
                        return
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.title = "TwoFA.title".localized
        self.progressView?.isHidden = self.cameFromProfile
    }
    
    @objc func skipTwoFA() {
        _ = ViewControllerProvider.setupPersonalViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openGAinAppStore(completionHandler: VoidClosure? = nil) {
        guard let url  = URL(string: Constants.GAURL) else { return }
        UIApplication.shared.open(url) { opened in
            if opened {
                completionHandler?()
            }
        }
    }
    
    private enum Constants {
        static let GAURL: String = "itms-apps://itunes.apple.com/app/google-authenticator/id388497605"
    }
    
}
