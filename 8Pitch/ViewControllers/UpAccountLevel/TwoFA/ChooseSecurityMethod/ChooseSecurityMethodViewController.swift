//
//  ChooseSecurityMethodViewController.swift
//  8Pitch
//
//  Created by 8pitch on 10/1/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class ChooseSecurityMethodViewController: TwoFAViewController {
    
    override var numberOfLines: Int { Constants.titleNumberOfLines }
    
    private lazy var viewModel : ChooseSecurityMethodViewModel = {
        ChooseSecurityMethodViewModel(session: session)
    }()
    
    override func setupLayout() {
        super.setupLayout()
        guard let model = self.model else {
            fatalError("invalid controller setup!")
        }
        let layout = DecoratableCollectionViewLayout(model: model)
        layout.contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.collectionView.collectionViewLayout = layout
        self.collectionViewLayout = layout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customTitle = "TwoFA.choose.customTitle".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.reloadData()
    }
    
    override func registerCells() {
        super.registerCells()
        self.collectionView.register(SecurityMethodCell.nibForRegistration, forCellWithReuseIdentifier: SecurityMethodCell.reuseIdentifier)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        if let cell = cell as? SecurityMethodCell {
            cell.model = self.model
            cell.setupForType((indexPath.section == 0) ? .sms : .totp)
            cell.switchHandler = { [weak self] in
                self?.switchToggled(forSMS: indexPath.section == 0)
            }
        }
            return cell
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: EmptyError.self, cellReuseIdentifier: SecurityMethodCell.reuseIdentifier),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: EmptyError.self, cellReuseIdentifier: SecurityMethodCell.reuseIdentifier)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
}

private extension ChooseSecurityMethodViewController {
    
    func switchToggled(forSMS: Bool) {
        let action = self.viewModel.switchAction(smsSwitchToggled: forSMS)
        switch action {
        case .dialog:
            self.showDialog()
        case .disableSMS:
            self.disableSMSTwoFA()
        case .enableSMS:
            self.enableSMS()
        case .disableTOTP:
            self.disableTOTPTwoFA()
        case .enableTOTP:
            self.enableTOTP()
        }
    }
    
    func showDialog() {
        self.presentAlert(title: "", message: "TwoFA.optionDialog.text".localized)
        self.collectionView.reloadData()
    }
    
    func disableTOTPTwoFA() {
        _ = ViewControllerProvider.setupEnterTwoFACodeViewController(rootViewController: self)
        self.nextController?.cameFromProfile = true
        self.nextController?.hidesBottomBarWhenPushed = true
        self.pushNextController(animated: true)
    }
    
    func disableSMSTwoFA() {
        guard let session = self.session else { return }
        self.startProgress()
        self.viewModel.sendCode(session) { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success(_):
                self?.openSMSConfirmation()
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
                self?.collectionView.reloadData()
            }
        }
    }
    
    func checkOTPApp() {
        guard let urlString = self.session?.twoFAURL, let url = URL(string: urlString) else { return }
        if UIApplication.shared.canOpenURL(url) {
            self.openTOTP()
        } else {
            self.openTOTPGuide()
        }
    }
    
    func openTOTP() {
        _ = ViewControllerProvider.setupAddTwoFACodeViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openTOTPGuide() {
        _ = ViewControllerProvider.setupAddExtraSecurityViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openSMSConfirmation() {
        _ = ViewControllerProvider.setupConfirmTwoFASMSViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func enableSMS() {
        self.sendCode()
    }
    
    func enableTOTP() {
        guard let session = self.session else { return }
        self.startProgress()
        self.viewModel.twoFAURL(session) { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.checkOTPApp()
                return
            }
            self?.presentAlert(message: error.localizedDescription)
            self?.collectionView.reloadData()
        }
    }
    
    func sendCode() {
        guard let session = self.session else {
            return
        }
        self.startProgress()
        self.remote.sendCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    self?.openSMSConfirmation()
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                    self?.collectionView.reloadData()
                    
                }
            }).disposed(by: self.disposables)
    }
    
    enum Constants {
        static let titleNumberOfLines: Int = 3
    }
    
}
