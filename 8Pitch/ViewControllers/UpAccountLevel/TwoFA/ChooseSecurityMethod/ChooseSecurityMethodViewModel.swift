//
//  ChooseSecurityMethodViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 10/5/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import RxSwift

class ChooseSecurityMethodViewModel: GenericViewModel {
    
    enum SwitchAction {
        case enableSMS
        case enableTOTP
        case disableSMS
        case disableTOTP
        case dialog
    }
    
    var smsTwoFAEnabled: Bool {
        self.session.twoFAProvider.smsAuth
    }
    
    func switchAction(smsSwitchToggled: Bool) -> SwitchAction {
        if !self.twoFAEnabled && smsSwitchToggled {
            return .enableSMS
        } else if !self.twoFAEnabled && !smsSwitchToggled {
            return .enableTOTP
        } else if self.smsTwoFAEnabled && smsSwitchToggled {
            return .disableSMS
        } else if !self.smsTwoFAEnabled && !smsSwitchToggled {
            return .disableTOTP
        } else if (self.smsTwoFAEnabled && !smsSwitchToggled) || (!self.smsTwoFAEnabled && smsSwitchToggled) {
            return .dialog
        }
        return .dialog
    }
    
    func twoFAURL(_ session: Session, completionHandler: @escaping ErrorClosure) {
        self.remote.QRCode(session) { error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
    }
    
    func sendCode(_ session: Session, completionHandler: @escaping ResponseClosure) {
        self.remote.sendCode(self.session)
        .compactMap { $0 }
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { response in
            completionHandler(response)
        }).disposed(by: self.disposables)
    }
    
}
