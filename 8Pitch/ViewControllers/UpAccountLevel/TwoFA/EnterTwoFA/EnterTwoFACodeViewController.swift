//
//  EnterTwoFACodeViewController.swift
//  8Pitch
//
//  Created by 8pitch on 03.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class EnterTwoFACodeViewController: InputSMSViewController {
    
    private lazy var viewModel : EnterTwoFACodeViewModel = {
        EnterTwoFACodeViewModel(session: session)
    }()
    
    override var confirmationFooterViewType : PublishSubject<InputSMSView.ViewType?>? {
        didSet {
            guard let typeAction = self.confirmationFooterViewType else { return }
            typeAction.onNext(.twoFA)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "TwoFA.title".localized
        self.customTitle = "TwoFA.enterCode.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func nextButtonTapped() {
        self.showSuccess()
    }
    
    private func showSuccess() {
        _ = ViewControllerProvider.setupSuccessTwoFAViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func verifyToken() {
        guard let session = self.session else {
            return
        }
        self.startProgress()
        self.viewModel.toggleTwoFA(session) { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.showSuccess()
                return
            }
            self?.presentAlert(message: error.localizedDescription)
        }
    }
    
}
