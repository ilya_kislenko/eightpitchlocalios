//
//  EnterTwoFACodeViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 10/5/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class EnterTwoFACodeViewModel: GenericViewModel {
    
    func toggleTwoFA(_ session: Session, completionHandler: @escaping ErrorClosure) {
        if self.twoFAEnabled {
            self.disableTwoFA(session, completionHandler: completionHandler)
        } else {
            self.enableTwoFA(session, completionHandler: completionHandler)
        }
    }
    
    func enableTwoFA(_ session: Session, completionHandler: @escaping ErrorClosure) {
        self.remote.enableTwoFA(session) { error in
            guard let error = error else {
                self.user() { response in
                    DispatchQueue.main.async {
                        switch response {
                        case .failure(let error):
                            completionHandler(error)
                        case .success(_):
                            completionHandler(nil)
                        }
                    }
                }
                return
            }
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
    }
    
    func disableTwoFA(_ session: Session, completionHandler: @escaping ErrorClosure) {
        self.remote.disableTwoFA(session) { error in
            guard let error = error else {
                self.user() { response in
                    DispatchQueue.main.async {
                        switch response {
                        case .failure(let error):
                            completionHandler(error)
                        case .success(_):
                            completionHandler(nil)
                        }
                    }
                }
                return
            }
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
    }
    
}
