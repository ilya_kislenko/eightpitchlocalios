//
//  AddExtraSecurityViewController.swift
//  8Pitch
//
//  Created by 8pitch on 03.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift

class AddExtraSecurityViewController: AddSecurityViewController {
    
    override var applyChangesFooterViewType : PublishSubject<ApplyChangesView.ViewType?>? {
        didSet {
            guard let typeAction = self.applyChangesFooterViewType else { return }
            typeAction.onNext(.installApp)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.arrowBack), style: .plain, target: self, action: #selector(backTapped))
        self.customTitle = "TwoFA.add.customTitle".localized
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        if let cell = cell as? TextCell {
            cell.setupForType(.installApp)
        }
        return cell
    }
    
    override func nextButtonTapped() {
        self.openGAinAppStore() { [weak self] in
            self?.openGetYourCode()
        }
    }
    
    func openGetYourCode() {
        _ = ViewControllerProvider.setupAddTwoFACodeViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}
