//
//  SuccessTwoFAViewController.swift
//  8Pitch
//
//  Created by 8pitch on 03.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class SuccessTwoFAViewController: DarkViewController {
    
    private lazy var viewModel : SuccessTwoFAViewModel = {
        SuccessTwoFAViewModel(session: session)
    }()
    
    @IBOutlet weak var resultLabel: CustomSubtitleDarkLabel!
    @IBOutlet weak var nextLabel: HintLabel!
    @IBOutlet weak var nextButton: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.whiteClose), style: .plain, target: self, action: #selector(closeKYCFlow))
        self.navigationItem.rightBarButtonItem = nil
        self.resultLabel.text = self.viewModel.resultText
        self.nextLabel.isHidden = self.cameFromProfile
        self.nextButton.isHidden = self.cameFromProfile
    }
    
    override func closeKYCFlow() {
        if self.cameFromProfile && !self.viewModel.twoFAEnabled {
            self.popToChooseMethod()
        } else {
            super.closeKYCFlow()
        }
    }
    
    private func popToChooseMethod() {
        guard let chooseMethodViewController = self.previousController?.previousController else { return }
        self.previousController = chooseMethodViewController
        self.popToPreviousController(animated: true)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        let _ = ViewControllerProvider.setupPersonalViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}
