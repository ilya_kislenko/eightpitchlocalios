//
//  SuccessTwoFAViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 10/5/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class SuccessTwoFAViewModel: GenericViewModel {
    
    var resultText: String {
        self.twoFAEnabled ? "TwoFA.success.result.enabled".localized : "TwoFA.success.result.disabled".localized
    }
    
}
