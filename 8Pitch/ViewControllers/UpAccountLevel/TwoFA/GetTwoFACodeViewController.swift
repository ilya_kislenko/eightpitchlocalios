//
//  AddTwoFACodeViewController.swift
//  8Pitch
//
//  Created by 8pitch on 03.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class GetTwoFACodeViewController: TwoFAViewController {
    
    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customTitle = "TwoFA.getCode.customTitle".localized
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = 2
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(TextCell.nibForRegistration, forCellWithReuseIdentifier: TextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        if let cell = cell as? TextCell {
            cell.setupForType(.getCode)
        }
        return cell
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: EmptyError.self, cellReuseIdentifier: TextCell.reuseIdentifier),
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    
    override func nextButtonTapped() {
        guard let urlString = self.session?.twoFAURL, let url = URL(string: urlString) else { return }
        UIApplication.shared.open(url) { [weak self] opened in
            if opened {
                self?.openEnterCode()
            } else {
                self?.openGAinAppStore()
            }
        }
    }
    
    private func openEnterCode() {
        _ = ViewControllerProvider.setupEnterTwoFACodeViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}
