//
//  AddSecurityViewController.swift
//  8Pitch
//
//  Created by 8pitch on 10/1/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class AddSecurityViewController: TwoFAViewController {
    
    var skipButtonAction : BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.skipButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: { [weak self] in
                    switch $0 {
                    case .error: return
                    case .loading:
                        self?.skipTwoFA()
                    case .next: return
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var applyChangesFooterViewType : PublishSubject<ApplyChangesView.ViewType?>? {
        didSet {
            guard let typeAction = self.applyChangesFooterViewType else { return }
            typeAction.onNext(self.cameFromProfile ? .getStartedFromProfile : .getStarted)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "TwoFA.title".localized
        self.customTitle = "TwoFA.add.customTitle".localized
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeKYCFlow))
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = 2
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(TextCell.nibForRegistration, forCellWithReuseIdentifier: TextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = ApplyChangesView.reuseIdentifier
        layout.register(ApplyChangesView.nibForRegistration, forDecorationViewOfKind: ApplyChangesView.reuseIdentifier)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.skipButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        if let cell = cell as? TextCell {
            cell.setupForType(.addSecurity)
        }
        return cell
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: EmptyError.self, cellReuseIdentifier: TextCell.reuseIdentifier),
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func nextButtonTapped() {
        _ = ViewControllerProvider.setupChooseSecurityMethodViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}
