//
//  AssetClassesViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class AssetClassesViewController: AbstractCollectionViewController<Questionnaire> {
    struct Constants {
        static let tokenizedSecurities = "questionnaire.assetClasses.tokenizedSecurities".localized
        static let stocks = "questionnaire.assetClasses.stocks".localized
        static let hedgeFunds = "questionnaire.assetClasses.hedgeFunds".localized
        static let warrants = "questionnaire.assetClasses.warrants".localized
        static let bonds = "questionnaire.assetClasses.bonds".localized
        static let closedFunds = "questionnaire.assetClasses.closedFunds".localized
        static let investmentCertificates = "questionnaire.assetClasses.investmentCertificates".localized

    }
    
    @IBOutlet var subtitleLabel: CustomSubtitleLabel!

    override var model: BehaviorSubject<Questionnaire>? {
        self.session?.questionnaireProvider
    }

    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading,
                         .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "\("questionnaire.title".localized) 2/10"
        self.subtitleLabel.text = "questionnaire.assetClasses.title".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func setupProgressView() {
        self.progressView.numberOfItems = 5
        self.progressView.currentIndex = 5
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        try? model?.value().currentStep = .assetClasses
    }

    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }

    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(SegmentedQuestionnaireCell.nibForRegistration, forCellWithReuseIdentifier: SegmentedQuestionnaireCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }

    override func nextButtonTapped() {
        _ = ViewControllerProvider.setupInvestingExperienceViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }

    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: SegmentedQuestionnaireCell.reuseIdentifier, content: Constants.tokenizedSecurities),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: SegmentedQuestionnaireCell.reuseIdentifier, content: Constants.stocks),
            CollectionViewModel(collectionView: self.collectionView, section: 2, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: SegmentedQuestionnaireCell.reuseIdentifier, content: Constants.hedgeFunds),
            CollectionViewModel(collectionView: self.collectionView, section: 3, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: SegmentedQuestionnaireCell.reuseIdentifier, content: Constants.warrants),
            CollectionViewModel(collectionView: self.collectionView, section: 4, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: SegmentedQuestionnaireCell.reuseIdentifier, content: Constants.bonds),
            CollectionViewModel(collectionView: self.collectionView, section: 5, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: SegmentedQuestionnaireCell.reuseIdentifier, content: Constants.closedFunds),
            CollectionViewModel(collectionView: self.collectionView, section: 6, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: SegmentedQuestionnaireCell.reuseIdentifier, content: Constants.investmentCertificates)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
}
