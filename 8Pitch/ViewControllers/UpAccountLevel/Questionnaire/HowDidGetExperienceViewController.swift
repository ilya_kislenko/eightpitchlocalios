//
//  HowDidGetExperienceViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class HowDidGetExperienceViewController: AbstractCollectionViewController<Questionnaire> {
    struct Constants {
        static let profession = "questionnaire.howDidGetExperience.profession".localized
        static let studies = "questionnaire.howDidGetExperience.studies".localized
    }

    @IBOutlet var subtitleLabel: CustomSubtitleLabel!

    override var model: BehaviorSubject<Questionnaire>? {
        self.session?.questionnaireProvider
    }

    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        guard let _ = try? self.model?.value().howDidGetExperienceProvider,
                              !(self.collectionView.indexPathsForSelectedItems?.isEmpty ?? true) else { return }
                        self.startProgress()
                        self.sendRequest()
                    case .next:
                        self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }

    func sendRequest() {
        guard let model = try? self.model?.value(), let session = self.session,
              model.howDidGetExperienceProvider != nil else {
            return
        }
        if session.questionaryPassed {
            self.remote.updateQuestionnaire(session)
                .compactMap { $0 }
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [weak self] response in
                    self?.stopProgress()
                    switch response {
                    case .success( _):
                        model.isValid.onNext(.success(true))
                        self?.nextButtonAction?.onNext(.next)
                    case .failure(let error):
                        self?.presentAlert(message: error.localizedDescription)
                        self?.nextButtonAction?.onNext(.error)
                    }
                }).disposed(by: self.disposables)
        } else {
            self.remote.sendQuestionnaire(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success( _):
                        model.isValid.onNext(.success(true))
                    self?.nextButtonAction?.onNext(.next)
                    case .failure(let error):
                        self?.presentAlert(message: error.localizedDescription)
                        self?.nextButtonAction?.onNext(.error)
                }
            }).disposed(by: self.disposables)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "\("questionnaire.title".localized) 10/10"
        self.subtitleLabel.text = "questionnaire.howDidGetExperience.title".localized
        self.navigationItem.rightBarButtonItem = nil
        self.collectionView.allowsMultipleSelection = true
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = 5
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        try? model?.value().currentStep = .howDidGetExperience
        self.setupSelectionSubscription()
    }

    private func setupSelectionSubscription() {
        self.collectionView.rx.itemSelected.subscribe(onNext: { (indexPath) in
            self.setupSelectedState(indexPath: indexPath)
        }).disposed(by: self.disposables)
        self.collectionView.rx.itemDeselected.subscribe(onNext: { (indexPath) in
            self.setupDeselectedState(indexPath: indexPath)
        }).disposed(by: self.disposables)
    }

    private func setupSelectedState(indexPath: IndexPath) {
        guard let model = try? self.model?.value() else { return }
        switch indexPath.section {
        case 0:
            if model.howDidGetExperienceProvider == ProfessionalExperienceSource.studies.rawValue {
                model.howDidGetExperienceProvider = ProfessionalExperienceSource.professionStudies.rawValue
            } else {
                model.howDidGetExperienceProvider = ProfessionalExperienceSource.profession.rawValue
            }
        case 1:
            if model.howDidGetExperienceProvider == ProfessionalExperienceSource.profession.rawValue {
                model.howDidGetExperienceProvider = ProfessionalExperienceSource.professionStudies.rawValue
            } else {
                model.howDidGetExperienceProvider = ProfessionalExperienceSource.studies.rawValue
            }
        default:
            break
        }
    }

    private func setupDeselectedState(indexPath: IndexPath) {
        guard let model = try? self.model?.value() else { return }
        switch indexPath.section {
        case 0:
            if model.howDidGetExperienceProvider == ProfessionalExperienceSource.professionStudies.rawValue {
                model.howDidGetExperienceProvider = ProfessionalExperienceSource.studies.rawValue
            } else {
                model.howDidGetExperienceProvider = nil
            }
        case 1:
            if model.howDidGetExperienceProvider == ProfessionalExperienceSource.professionStudies.rawValue {
                model.howDidGetExperienceProvider = ProfessionalExperienceSource.profession.rawValue
            } else {
                model.howDidGetExperienceProvider = nil
            }
        default:
            break
        }
    }

    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }

    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(SquareCheckMarkCell.nibForRegistration, forCellWithReuseIdentifier: SquareCheckMarkCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }

    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: SquareCheckMarkCell.reuseIdentifier, content: Constants.profession),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: SquareCheckMarkCell.reuseIdentifier, content: Constants.studies)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }

    override func nextButtonTapped() {
        _ = ViewControllerProvider.setupQuestionnaireSuccessViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}
