//
//  QuestionnaireExplanationViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class QuestionnaireExplanationViewController: CustomTitleViewController {
    @IBOutlet var customSubtitle: CustomSubtitleLabel!
    @IBOutlet var textLabel: InfoLabel!
    @IBOutlet var confirmationButton: BorderedButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.customSubtitle.text = "questionnaire.explanation.title".localized
        self.textLabel.text = "questionnaire.explanation.text".localized
        self.confirmationButton.setTitle("questionnaire.explanation.understandBtn".localized, for: .normal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(backTapped))
        self.navigationItem.rightBarButtonItem = nil
    }
    
    @IBAction func confirmationButtonAction(_ sender: Any) {
        _ = ViewControllerProvider.setupAssetExperienceViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }

    @objc override func backTapped() {
        self.closeKYCFlow()
    }
}
