//
//  ExperienceDurationViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class ExperienceDurationViewController: CheckMarkQuestionnaireViewController<Questionnaire> {
    struct Constants {
       static let moreThan = "questionnaire.moreThan".localized
        static let lessThan = "questionnaire.lessThan".localized
        static let year = "questionnaire.year".localized
        static let years = "questionnaire.years".localized
    }
    
    @IBOutlet var subtitleLabel: CustomSubtitleLabel!

    override var model: BehaviorSubject<Questionnaire>? {
        self.session?.questionnaireProvider
    }

    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                        case .error: return // TODO: show error
                        case .loading:
                            self.verifyModelAndNextButtonActionIfPossible()
                        case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "\("questionnaire.title".localized) 4/10"
        self.subtitleLabel.text = "questionnaire.experienceTerms.title".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        try? model?.value().currentStep = .investmentDuration
        self.setupSelectionSubscription()
    }
    
    private func setupSelectionSubscription() {
        collectionView.rx.itemSelected.subscribe(onNext: { (indexPath) in
            self.setupSelectedState(indexPath: indexPath)
        }).disposed(by: self.disposables)
    }
    
    private func setupSelectedState(indexPath: IndexPath) {
        guard let model = try? self.model?.value() else { return }
        switch indexPath.section {
        case 0:
            model.experienceDurationProvider = ExperienceDuration.lessThanOne.rawValue
        case 1:
            model.experienceDurationProvider = ExperienceDuration.fromOneToThree.rawValue
        case 2:
            model.experienceDurationProvider = ExperienceDuration.moreThanThree.rawValue
        default:
            break
        }
    }

    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }

    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CheckMarkCell.nibForRegistration, forCellWithReuseIdentifier: CheckMarkCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }

    override func nextButtonTapped() {
        _ = ViewControllerProvider.setupInvestmentAmountViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }

    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: CheckMarkCell.reuseIdentifier, content: "\(Constants.lessThan) 1 \(Constants.year)"),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: CheckMarkCell.reuseIdentifier, content: "1-3 \(Constants.years)"),
            CollectionViewModel(collectionView: self.collectionView, section: 2, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: CheckMarkCell.reuseIdentifier, content: "\(Constants.moreThan) 3 \(Constants.years)")
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
}
