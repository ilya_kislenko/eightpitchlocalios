//
//  QuestionnaireSuccessViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class QuestionnaireSuccessViewController: RedViewController {
    @IBOutlet var subtitleLabel: CustomSubtitleDarkLabel!
    @IBOutlet var textLabel: HintLabel!
    @IBOutlet var goToHomePageButton: BorderedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(goToHomePageAction(_:)))
        self.navigationController?.navigationBar.barTintColor = UIColor(Color.red)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor(Color.white)
        self.subtitleLabel.text = "questionnaire.success.thankYou".localized
        self.textLabel.text = "questionnaire.success.explanation".localized
        self.goToHomePageButton.setTitle("questionnaire.success.goToHomepageBtn".localized, for: .normal)
    }
    
    @objc @IBAction func goToHomePageAction(_ sender: Any) {
        if self.kycInitialController is DoneViewController {
            let tabBarController = ViewControllerProvider.setupTabBarController(rootViewController: self)
            self.navigationController?.viewControllers = [tabBarController]
        } else {
            self.closeKYCFlow()
        }
    }
}

