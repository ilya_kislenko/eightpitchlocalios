//
//  CheckMarkQuestionnaireViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/7/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class CheckMarkQuestionnaireViewController <T: Questionnaire>: AbstractCollectionViewController<T> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func setupProgressView() {
        self.progressView.numberOfItems = 5
        self.progressView.currentIndex = 5
    }
    
    override func verifyModelAndNextButtonActionIfPossible() {
        guard !(self.collectionView.indexPathsForSelectedItems?.isEmpty ?? true) else { return }
        super.verifyModelAndNextButtonActionIfPossible()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let model = try? self.session?.questionnaireProvider.value(),
              (self.session?.questionaryPassed ?? true) else { return }
        switch self {
        case is AssetExperienceViewController:
            experienceSelectionSetup(model)
        case is InvestingExperienceViewController:
            investingExperienceSetup(model)
        case is ExperienceDurationViewController:
            experienceDurationSetup(model)
        case is InvestmentAmountViewController:
            investmentAmountSetup(model)
        case is InvestingFrequencyViewController:
            investingFrequencySetup(model)
        case is VirtualCurrencyViewController:
            virtualCurrencySetup(model)
        case is VirtualCurrencyShareViewController:
            virtualCurrencyShareSetup(model)
        case is CapitalMarketExperienceViewController:
            capitalMarketExperienceSetup(model)
        default:
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
        }
    }
    
    private func experienceSelectionSetup(_ model: Questionnaire) {
        switch model.assetExperienceProvider {
        case AssetExperience.TRUE.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
        case AssetExperience.notProvided.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 2), animated: false, scrollPosition: .left)
        default:
            collectionView.selectItem(at: IndexPath(item: 0, section: 1), animated: false, scrollPosition: .left)
        }
    }
    
    private func investingExperienceSetup(_ model: Questionnaire) {
        switch model.investingExperienceProvider {
        case true:
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
        case false:
            collectionView.selectItem(at: IndexPath(item: 0, section: 1), animated: false, scrollPosition: .left)
        case .none,
             .some(_):
            return
        }
    }
    
    private func experienceDurationSetup(_ model: Questionnaire) {
        switch model.experienceDurationProvider {
        case ExperienceDuration.lessThanOne.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
        case ExperienceDuration.fromOneToThree.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 1), animated: false, scrollPosition: .left)
        case ExperienceDuration.moreThanThree.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 2), animated: false, scrollPosition: .left)
        default:
            break
        }
    }
    
    private func investmentAmountSetup(_ model: Questionnaire) {
        switch model.investmentAmountProvider {
        case InvestmentAmount.lessThanFive.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 1), animated: false, scrollPosition: .left)
        case InvestmentAmount.fromFiveToTwentyfive.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 2), animated: false, scrollPosition: .left)
        case InvestmentAmount.moreThanTwentyfive.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 3), animated: false, scrollPosition: .left)
        case .some(_):
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
        case .none:
            return
        }
    }
    
    private func investingFrequencySetup(_ model: Questionnaire) {
        switch model.investingFrequencyProvider {
        case InvestingFrequency.lessThanFive.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
        case InvestingFrequency.moreThanFive.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 1), animated: false, scrollPosition: .left)
        default:
            break
        }
    }
    
    private func virtualCurrencySetup(_ model: Questionnaire) {
        switch model.virtualCurrencyProvider {
        case true:
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
        case false:
            collectionView.selectItem(at: IndexPath(item: 0, section: 1), animated: false, scrollPosition: .left)
        case .none, .some(_):
            return
        }
    }
    
    private func virtualCurrencyShareSetup(_ model: Questionnaire) {
        switch model.virtualCurrencyShareProvider {
        case VirtualCurrencyShare.lessThanFifty.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
        case VirtualCurrencyShare.moreThanFifty.rawValue:
            collectionView.selectItem(at: IndexPath(item: 0, section: 1), animated: false, scrollPosition: .left)
        default:
            break
        }
    }
    
    private func capitalMarketExperienceSetup(_ model: Questionnaire) {
        switch model.capitalMarketExperienceProvider {
        case true:
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
        case false:
            collectionView.selectItem(at: IndexPath(item: 0, section: 1), animated: false, scrollPosition: .left)
        case .none, .some(_):
            return
        }
    }
}
