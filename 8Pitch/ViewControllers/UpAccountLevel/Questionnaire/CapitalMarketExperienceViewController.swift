//
//  CapitalMarketExperienceViewController.swift
//  8Pitch
//
//  Created by 8pitch on 8/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class CapitalMarketExperienceViewController: CheckMarkQuestionnaireViewController<Questionnaire> {
    struct Constants {
        static let yes = "questionnaire.yes".localized
        static let no = "questionnaire.no".localized

    }
    
    @IBOutlet var subtitleLabel: CustomSubtitleLabel!
    
    override var model: BehaviorSubject<Questionnaire>? {
        self.session?.questionnaireProvider
    }

    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        if let haveExperience = try? self.model?.value().capitalMarketExperienceProvider,
                            haveExperience {
                            fallthrough
                        } else {
                            guard !(self.collectionView.indexPathsForSelectedItems?.isEmpty ?? true) else { return }
                            self.startProgress()
                            self.sendRequest()
                        }
                    case .next:
                        self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }

    func sendRequest() {
        guard let model = try? self.model?.value(), let session = self.session else {
            return
        }
        if session.questionaryPassed {
            self.remote.updateQuestionnaire(session)
                .compactMap { $0 }
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [weak self] response in
                    self?.stopProgress()
                    switch response {
                    case .success( _):
                        model.isValid.onNext(.success(true))
                        self?.nextButtonAction?.onNext(.next)
                    case .failure(let error):
                        self?.presentAlert(message: error.localizedDescription)
                        self?.nextButtonAction?.onNext(.error)
                    }
                }).disposed(by: self.disposables)
        } else {
            self.remote.sendQuestionnaire(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success( _):
                        model.isValid.onNext(.success(true))
                    self?.nextButtonAction?.onNext(.next)
                    case .failure(let error):
                        self?.presentAlert(message: error.localizedDescription)
                        self?.nextButtonAction?.onNext(.error)
                }
            }).disposed(by: self.disposables)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "\("questionnaire.title".localized) 9/10"
        self.subtitleLabel.text = "questionnaire.capitalMarketExperience.title".localized
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        try? model?.value().currentStep = .capitalMarketExperience
        self.setupSelectionSubscription()
    }

    private func setupSelectionSubscription() {
        collectionView.rx.itemSelected.subscribe(onNext: { (indexPath) in
            self.setupSelectedState(indexPath: indexPath)
        }).disposed(by: self.disposables)
    }

    private func setupSelectedState(indexPath: IndexPath) {
        guard let model = try? self.model?.value() else { return }
        switch indexPath.section {
        case 0:
            model.capitalMarketExperienceProvider = true
        case 1:
            model.capitalMarketExperienceProvider = false
        default:
            break
        }
    }

    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }

    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CheckMarkCell.nibForRegistration, forCellWithReuseIdentifier: CheckMarkCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }

    override func nextButtonTapped() {
        if let haveExperience = try? self.model?.value().capitalMarketExperienceProvider,
            haveExperience {
            _ = ViewControllerProvider.setupHowDidGetExperienceViewController(rootViewController: self)
        } else {
            _ = ViewControllerProvider.setupQuestionnaireSuccessViewController(rootViewController: self)
        }
        self.pushNextController(animated: true)
    }

    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: CheckMarkCell.reuseIdentifier, content: Constants.yes),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: QuestionnaireError.self, cellReuseIdentifier: CheckMarkCell.reuseIdentifier, content: Constants.no)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
}

