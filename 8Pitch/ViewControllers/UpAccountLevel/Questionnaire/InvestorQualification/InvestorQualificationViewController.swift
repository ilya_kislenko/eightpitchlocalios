//
//  InvestorQualificationViewController.swift
//  8Pitch
//
//  Created by 8pitch on 6/1/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import RxSwift
import MobileCoreServices

class InvestorQualificationViewController: CustomTitleViewController {
    
    private lazy var viewModel : InvestorQualificationViewModel = {
        InvestorQualificationViewModel(session: session)
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subtitleLabel: CustomSubtitleLabel!
    
    private var cellItems: [CellItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.barlow.semibold.titleSize, NSAttributedString.Key.foregroundColor: UIColor(.gray)]
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeTapped))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.info), style: .plain, target: self, action: #selector(openInfo))
        self.title = "\("questionnaire.title".localized) 1/11"
        self.subtitleLabel.text = "investor.qualification.subtitle".localized
        self.navigationItem.customizeForWhite()
        setupTableView()
        self.setupTapGesture()
        setupTableData()
    }
    
    func setupTableData() {
        cellItems = [CellItem(.descriptionTitle),  CellItem(.qualificationPicker)]
        if let qualificationType = viewModel.qualificationType, qualificationType != .unqualified {
            cellItems +=  [CellItem.init(.descriptionInput), CellItem.init(.filelistTitle)]
            for file in viewModel.filesNeedToSend {
                cellItems.append(CellItem(.filelisItem, fileNeedToSend: file))
            }
            cellItems.append(CellItem(.appendFileHeader))
            viewModel.attachedFiles.forEach { (file) in
                cellItems.append(CellItem.init(.appendFileInfo, file: file))
            }
            cellItems.append(CellItem.init(.appendFileButton))
        }
        cellItems += [CellItem.init(.checkmarkButton), CellItem.init(.nextButton)]
        tableView.reloadData()
    }
    
    func setupTableView() {
        self.tableView.register(QualificationDescriptionTitleCell.nibForRegistration, forCellReuseIdentifier: QualificationDescriptionTitleCell.reuseIdentifier)
        self.tableView.register(PickQualificationCell.nibForRegistration, forCellReuseIdentifier: PickQualificationCell.reuseIdentifier)
        self.tableView.register(QualificationDescriptionCell.nibForRegistration, forCellReuseIdentifier: QualificationDescriptionCell.reuseIdentifier)
        self.tableView.register(FileToSendDescriptionCell.nibForRegistration, forCellReuseIdentifier: FileToSendDescriptionCell.reuseIdentifier)
        self.tableView.register(AttachedFileHeaderCellCell.nibForRegistration, forCellReuseIdentifier: AttachedFileHeaderCellCell.reuseIdentifier)
        self.tableView.register(AttachedFileDescriptionCell.nibForRegistration, forCellReuseIdentifier: AttachedFileDescriptionCell.reuseIdentifier)
        self.tableView.register(AttachedFileButtonCell.nibForRegistration, forCellReuseIdentifier: AttachedFileButtonCell.reuseIdentifier)
        self.tableView.register(QualificatonCheckMarkCell.nibForRegistration, forCellReuseIdentifier: QualificatonCheckMarkCell.reuseIdentifier)
        self.tableView.register(NextButtonCellCell.nibForRegistration, forCellReuseIdentifier: NextButtonCellCell.reuseIdentifier)
        tableView.separatorStyle = .none
    }

    @objc func closeTapped() {
        closeKYCFlow()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.endEditing()
    }
    
    @objc func endEditing() {
        self.tableView.endEditing(true)
        self.view.endEditing(true)
    }
    
    func setupTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.endEditing))
        tap.cancelsTouchesInView = false
        self.tableView.addGestureRecognizer(tap)
        self.tableView.delaysContentTouches = false
    }
    
    func setupKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
}

extension InvestorQualificationViewController: DocumentPickerPresentable {
    func selectedFile(file: AttachedFile?, errorMessage: String?) {
        guard let file = file else { return }
        viewModel.appendFile(attachedFile: file)
        setupTableData()
    }
}

extension InvestorQualificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cellItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch cellItems[indexPath.row].type {
        case .descriptionTitle:
            let cell = tableView.dequeueReusableCell(withIdentifier: QualificationDescriptionTitleCell.reuseIdentifier, for: indexPath) as! QualificationDescriptionTitleCell
            cell.selectionStyle = .none
            return cell
        case .qualificationPicker:
            let cell = tableView.dequeueReusableCell(withIdentifier: PickQualificationCell.reuseIdentifier, for: indexPath) as! PickQualificationCell
            cell.qualificationTypeChoosed = { [weak self] (qualificationType) in
                if self?.viewModel.qualificationType != qualificationType {
                    self?.viewModel.setQualificationType(qualificationType)
                    self?.setupTableData()
                }
            }
            cell.selectionStyle = .none
            return cell
        case .descriptionInput:
            let cell = tableView.dequeueReusableCell(withIdentifier: QualificationDescriptionCell.reuseIdentifier, for: indexPath) as! QualificationDescriptionCell
            cell.textFieldText = { [weak self] (text) in
                self?.viewModel.setDescription(description: text)
            }
            cell.selectionStyle = .none
            return cell
        case .filelistTitle:
            let cell = tableView.dequeueReusableCell(withIdentifier: FileToSendDescriptionCell.reuseIdentifier, for: indexPath) as! FileToSendDescriptionCell
            cell.selectionStyle = .none
            cell.setupForTitlePreview(title: viewModel.qualificationType?.rawValue)
            return cell
        case .filelisItem:
            let cell = tableView.dequeueReusableCell(withIdentifier: FileToSendDescriptionCell.reuseIdentifier, for: indexPath) as! FileToSendDescriptionCell
            cell.selectionStyle = .none
            cell.setupForDocument(documentName: cellItems[indexPath.row].fileNeedToSend)
            return cell
        case .appendFileHeader:
            let cell = tableView.dequeueReusableCell(withIdentifier: AttachedFileHeaderCellCell.reuseIdentifier, for: indexPath) as! AttachedFileHeaderCellCell
            cell.selectionStyle = .none
            return cell
        case .appendFileInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: AttachedFileDescriptionCell.reuseIdentifier, for: indexPath) as! AttachedFileDescriptionCell
            cell.setup(attachedFileInfo: cellItems[indexPath.row].file)
            cell.removeButtonAction = { [weak self] in
                self?.viewModel.removeFile(attachedFile: self?.cellItems[indexPath.row].file)
                self?.setupTableData()
            }
            cell.selectionStyle = .none
            return cell
        case .appendFileButton:
            let cell = tableView.dequeueReusableCell(withIdentifier: AttachedFileButtonCell.reuseIdentifier, for: indexPath) as! AttachedFileButtonCell
            cell.addFileButtonAction = { [weak self] in
                self?.showDocumentPicker()
            }
            cell.selectionStyle = .none
            return cell
        case .checkmarkButton:
            let cell = tableView.dequeueReusableCell(withIdentifier: QualificatonCheckMarkCell.reuseIdentifier, for: indexPath) as! QualificatonCheckMarkCell
            cell.selectionStyle = .none
            cell.toggleChanged = { [weak self] (isSelected) in
                self?.viewModel.setAcceptRules(isSelected)
            }
            return cell
        case .nextButton:
            let cell = tableView.dequeueReusableCell(withIdentifier: NextButtonCellCell.reuseIdentifier, for: indexPath) as! NextButtonCellCell
            cell.nextButtonAction = { [weak self] in
                self?.sendInvestorQualification()
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    
    private func sendInvestorQualification() {
        if self.viewModel.isValid() {
            self.startProgress()
            self.viewModel.sendInvestorQualification(self.session) { [weak self] (error) in
                guard let self = self else { return }
                self.stopProgress()
                if let error = error {
                    self.presentAlert(message: error.localizedDescription)
                } else {
                    _ = ViewControllerProvider.setupAssetExperienceViewController(rootViewController: self)
                    self.pushNextController(animated: true)
                }
            }
        } else {
            self.presentAlert(message: "investor.qualification.error.message".localized)
        }
    }
    
    @objc private func openInfo() {
        let _ = ViewControllerProvider.setupInfoViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}


fileprivate enum ItemType {
    
    case descriptionTitle
    case qualificationPicker
    case descriptionInput
    case filelistTitle
    case filelisItem
    case appendFileHeader
    case appendFileInfo
    case appendFileButton
    case checkmarkButton
    case nextButton
    
}

fileprivate class CellItem {
    var type:       ItemType
    var file:      AttachedFile?
    var fileNeedToSend: String?
    
    init(_ type: ItemType, fileNeedToSend: String? = nil , file: AttachedFile? = nil) {
        self.type = type
        self.fileNeedToSend = fileNeedToSend
        self.file = file
    }
}
