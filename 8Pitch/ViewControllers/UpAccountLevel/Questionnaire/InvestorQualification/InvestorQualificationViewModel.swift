//
//  InvestorQualificationViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 13.01.21.
//  Copyright © 2021 8pitch. All rights reserved.
//

import Input
import Remote

class InvestorQualificationViewModel: GenericViewModel {
    
    var description: String = ""
    var qualificationType: QualificationType?
    var attachedFiles: [AttachedFile] = []
    var filesNeedToSend: [String] = ["First", "Second", "Third"]
    var acceptRules: Bool = false
    
    func setDescription(description: String?) {
        if let description = description {
            self.description = description
        }
    }
    
    func setQualificationType(_ type: QualificationType) {
        qualificationType = type
        if type == .unqualified {
            attachedFiles = []
            description = ""
        }
    }
    
    func setAcceptRules(_ isAccepted: Bool) {
        acceptRules = isAccepted
    }
    
    func appendFile(attachedFile: AttachedFile) {
        if !attachedFiles.contains(where: { $0.fileData == attachedFile.fileData && $0.fileExtension == attachedFile.fileExtension && $0.name == attachedFile.name && $0.size == attachedFile.size }) {
            attachedFiles.append(attachedFile)
        }
    }
    
    func removeFile(attachedFile: AttachedFile?) {
        guard let attachedFile = attachedFile, let index = attachedFiles.firstIndex(where: { $0.fileData == attachedFile.fileData && $0.fileExtension == attachedFile.fileExtension && $0.name == attachedFile.name && $0.size == attachedFile.size }) else { return }
        attachedFiles.remove(at: index)
    }
    
    func sendInvestorQualification(_ session: Session?, completionHandler: @escaping ErrorClosure) {
        guard let session = session, let qualificationType = qualificationType else { return }
        self.session = session
        let group = DispatchGroup()
        
        self.session.uploadFileProvider.uploadFileIDs = []
        
        for attachedFile in attachedFiles {
            group.enter()
            self.remote.sendFile(session, attachedFile) { (error) in
                completionHandler(error)
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            self.remote.sendQualificationForm(session, self.description, qualificationType, self.session.uploadFileProvider.uploadFileIDs) { (error) in
                completionHandler(error)
            }
        }
    }
    
    func isValid() -> Bool {
        if let qualificationType = qualificationType, acceptRules {
            switch qualificationType {
            case .unqualified:
                return true
            case .firstClass, .secondClass, .thirdClass, .fourthClass, .fifthClass:
                return !description.trimmingCharacters(in: .whitespaces).isEmpty && !attachedFiles.isEmpty
            }
        } else {
            return false
        }
    }
}


