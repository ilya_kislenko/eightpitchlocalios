//
//  WebIDDoneViewController.swift
//  8Pitch
//
//  Created by 8pitch on 13.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class WebIDDoneViewController : DarkViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.whiteClose), style: .plain, target: self, action: #selector(closeKYCFlow))
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.customizeForColored()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor(.backgroundDark)
    }

    @IBAction func nextButtonTapped(_ sender: UIButton) {
        _ = ViewControllerProvider.setupQuestionnaireExplanationViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }

    override func popToPreviousController(animated: Bool) {
        guard let controller = self.previousController else {
            print("self.previousController == nil")
            return
        }
        UIApplication.shared.keyWindow?.rootViewController = controller
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
}
