//
//  WebIDTanInputView.swift
//  8Pitch
//
//  Created by 8pitch on 06.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class WebIDTanInputView {
    
    let tanProvider : BehaviorSubject<String?> = .init(value: nil)
    var disposables = DisposeBag()
    
    func present(in viewController: GenericViewController) {
        let alert = UIAlertController(title: "webid.text.tan.input.title".localized, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "webid.text.tan.input.button.ok".localized, style: .default)
        alert.addTextField {
            $0.placeholder = "webid.text.tan.input.placeholder".localized
            $0.rx.controlEvent(.editingChanged)
            .withLatestFrom($0.rx.text.orEmpty)
            .subscribe(onNext: {
                self.tanProvider.onNext($0)
            }).disposed(by: self.disposables)
        }
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
    
}
