//
//  WebIDViewController.swift
//  8Pitch
//
//  Created by 8pitch on 05.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import WebIdIosSdk
import RxSwift
import WebRTC
import AhoyKit
import Remote

class WebIDViewController : CustomTitleViewController {
    
    enum SDKMetadata {
        static let mobileAPIKey = "8c126ebcdf70beb61a1c366814afd25a"
        static let mobileUserName = "000417"
        static var host: String {
            Enviroment.current == .prod ? "https://webid-gateway.de" : "https://test.webid-solutions.de"
        }
        static var shaKey: String {
            Enviroment.current == .prod ? "sha256/6GueQbEhY5S5tNIZMyij+oGsuki38Z8ao6t3gZkDix4=" : "sha256/0m47tebFsqJd2lwhVjDBNox+egJZoa/1c7tc+I5MXvI="
        }
    }
    
    private var sdk : IWebIdMobileAppSdk?
    private var actionIDResult: VerifyActionIdResult?
    private var webIDStateHandler = WebIDStateHandler()
    private var call : IVideoCallStateDelegate?
    private var cameraAccess : AudioVideoAccessProvider = .init()
    private var tan : String?
    private var tanInput : WebIDTanInputView = WebIDTanInputView()
    
    @IBOutlet weak var agentPlaceholderImage : UIImageView!
    
    @IBOutlet weak var remoteVideoView: RTCEAGLVideoView!
    @IBOutlet weak var selfVideoView: RTCEAGLVideoView!
    
    @IBOutlet weak var statusView : UIView!
    @IBOutlet weak var statusLabel : UILabel!
    
    @IBOutlet weak var insertTanView : UIView!
    @IBOutlet weak var insertTanTextField : TapTextField!
    
    @IBOutlet weak var customerIDLabel : UILabel!
    
    private var isCameraAuthorized = AudioVideoAccessProvider.Access.notDetermined
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initializeSession()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor(.white)
        self.startProgress()
        self.cameraAccess.accessProvider
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.startProgress()
                self?.isCameraAuthorized = $0
                switch $0 {
                case .authorized:
                    self?.stopProgress()
                    self?.getActionId()
                    self?.setup()
                case .denied:
                    self?.startProgress()
                    self?.openSettings()
                case .notDetermined:
                    self?.startProgress()
                    return
                }
            }).disposed(by: self.disposables)
        self.cameraAccess.requestAccessIfNeeded()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.endCall()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customTitle = "webid.customTitle".localized
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeKYCFlow))
        self.navigationItem.rightBarButtonItem = nil
        self.navigationController?.navigationBar.tintColor = UIColor(.gray)
        
    }
    
    private func getActionId() {
        guard let session = self.session else { return }
        self.startProgress()
        self.remote.webID(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.stopProgress()
                switch $0 {
                case .success(_):
                    self?.verifyUserActions()
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                }
            }).disposed(by: self.disposables)
    }
    
    private func setup() {
        self.title = "webid.title".localized
        self.setupAgentPlaceholderImage(isHidden: false)
        self.setupCameraBarButton()
        self.setupTanInputView()
        self.setupTanInputField()
        self.setupStatusView()
        self.setupYourIDLabel()
        self.setupWebIDStateHandler()
    }
    
    private func setupWebIDStateHandler() {
        self.webIDStateHandler.statusText
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {
                self.updateStatusLabel(text: $0)
            })
            .disposed(by: self.disposables)
        
        self.webIDStateHandler.stateProvider
            .compactMap { $0 }
            .subscribe(onNext: {
                switch $0 {
                case .CALL_DISCONNECTED:
                    self.setupAgentPlaceholderImage(isHidden: false)
                    self.endCall()
                case .CALL_GET_ANSWERED:
                    self.setupAgentPlaceholderImage(isHidden: true)
                case .FAILED_CALL_ADDRESS_NOT_FOUND:
                    let message = "webid.error.call.addressNotFound".localized
                    self.presentInternalAlertAndDismiss(message)
                    self.endCall()
                case .FAILED_HOLD_THE_LINE_TIMEOUT_REACHED:
                    let message = "webid.error.call.info.timeout".localized
                    self.presentInternalAlertAndDismiss(message)
                    self.endCall()
                case .FAILED_INIT:
                    let message = "webid.error.call.internal".localized
                    self.presentInternalAlertAndDismiss(message)
                    self.endCall()
                case .FAILED_INTERNAL_ERROR:
                    let message = "webid.error.call.internal".localized
                    self.presentInternalAlertAndDismiss(message)
                    self.endCall()
                case .FAILED_MEDIA_STREAM_LOCAL_CREATION_UNKNOWN_ERROR:
                    let message = "webid.error.call.internal".localized
                    self.presentInternalAlertAndDismiss(message)
                    self.endCall()
                case .FAILED_UNKNOWN_ERROR:
                    let message = "webid.error.call.internal".localized
                    self.presentInternalAlertAndDismiss(message)
                    self.endCall()
                case .HUNG_UP_BY_AGENT: fallthrough
                case .HUNG_UP_BY_USER:
                    let message = "webid.error.call.hungup".localized
                    self.updateStatusLabel(text: message)
                    self.setupAgentPlaceholderImage(isHidden: false)
                    self.endCall()
                case .IN_CALL_WITH_AGENT:
                    self.setupAgentPlaceholderImage(isHidden: false)
                case .MEDIA_STREAM_LOCAL_CREATED: fallthrough
                case .MEDIA_STREAM_REMOTE_CREATED: fallthrough
                case .NEW:
                    return
                case .IN_QUEUE:
                    return
                default: return
                }
            }).disposed(by: self.disposables)
    }
    
    private func setupStatusView() {
        self.updateStatusLabel()
        self.statusView.isHidden = false
    }
    
    func updateStatusLabel(bold: String = "webid.text.status.wait.bold".localized, text: String = "webid.text.status.wait.plain".localized) {
        let plainText = NSAttributedString(string: text, attributes: [ .font : UIFont.roboto.regular.labelSize ])
        let boldText = NSAttributedString(string: bold, attributes:
                                            [ .font : UIFont.roboto.medium.labelSize,
                                              .strokeWidth : -3.0 ]
        )
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.28
        let text = NSMutableAttributedString()
        text.append(boldText)
        text.append(plainText)
        text.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, text.length))
        self.statusLabel.attributedText = text
    }
    
    private func setupYourIDLabel() {
        self.customerIDLabel.font = UIFont.barlow.bold.textSize
        self.customerIDLabel.textColor = UIColor(.gray)
        self.customerIDLabel.text = ""
    }
    
    private func setupTanInputView() {
        self.tanInput.tanProvider
            .compactMap { $0 }
            .subscribe(onNext: {
                self.tan = $0
                self.insertTanTextField.text = $0
            }).disposed(by: self.disposables)
        self.insertTanView.isHidden = true
    }
    
    private func setupTanInputField() {
        self.insertTanTextField.tapProvider
            .filter { $0 }
            .subscribe(onNext: { _ in
                self.tanInput.disposables = DisposeBag()
                self.tanInput.present(in: self)
            }).disposed(by: self.disposables)
        self.insertTanTextField.placeholder = "webid.text.tan.input.placeholder".localized
    }
    
    private func setupCameraBarButton() {
        let item = UIBarButtonItem(image: UIImage(.camera), landscapeImagePhone: nil, style: .plain, target: self, action: #selector(rotateCamera))
        self.navigationItem.rightBarButtonItem = item
        self.navigationItem.customizeForWhite()
    }
    
    func setupAgentPlaceholderImage(isHidden: Bool) {
        self.agentPlaceholderImage.image = UIImage(.waitingForAgent)
        self.agentPlaceholderImage.isHidden = isHidden
        self.remoteVideoView.isHidden = !isHidden
        
        self.insertTanView.isHidden = !isHidden
        self.statusView.isHidden = isHidden
    }
    
    @objc func rotateCamera() {
        try? self.call?.switchCamera()
    }
    
    @IBAction func startCall() {
        guard self.call == nil else {
            return
        }
        if self.isCameraAuthorized == .authorized {
            self.startCallSetup()
            return
        }
    }
    
    private func cleanState() {
        self.insertTanTextField.text = nil
    }
    
    @IBAction func endCall() {
        self.call?.hangUp()
        self.call?.remove(stateChangedListener: self.webIDStateHandler )
        self.call?.remove(holdTheLineProgressChangedListener: self.webIDStateHandler )
        self.call = nil
    }
    
}

extension WebIDViewController {
    
    func startCallSetup() {
        guard let userAction = self.actionIDResult?.userAction,
              let videoCallInfo = userAction.userActionStatus.videoCallInfo else
        {
            return
        }
        var call : IVideoCallStateDelegate?
        do {
            call = try self.sdk?.createCall(for: userAction, with: videoCallInfo)
            call?.add(stateChangedListener: self.webIDStateHandler)
            call?.add(holdTheLineProgressChangedListener: self.webIDStateHandler)
            try call?.startCall(localVideoView: self.selfVideoView, remoteVideoView: self.remoteVideoView)
            self.call = call
        } catch VideoCallError.AlreadyInCall { // call was already started
            print("XXX error: already started")
        } catch VideoCallError.CallcenterIsClosed {
            self.presentNextOpeningHoursDialog()
        } catch VideoCallError.VideoCallInfoExpired {
            let message = "webid.error.call.info.timeout".localized
            self.presentAlert(message: message)
        } catch VideoCallError.CameraPermissionRequired {
            let message = "webid.error.camera.permission".localized
            self.presentAlert(message: message)
        } catch VideoCallError.MicrophonePermissionRequired {
            let message = "webid.error.microphone.permission".localized
            self.presentAlert(message: message)
        } catch {
            print("XXX error: unknown \(error)")
            // configuration error
        }
    }
    
    private func presentNextOpeningHoursDialog() {
        guard let date = self.actionIDResult?.userAction.userActionStatus.videoCallInfo?.nextCallcenterOpening else {
            print("unable to get opening date!")
            return
        }
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        let dateStr = formatter.string(from: date.withCurrentTimeZoneOffset())
        let message = "webid.error.callcentre.closed".localized + dateStr
        self.presentAlert(message: message)
    }
    
}

class WebIDStateHandler : IVideoCallStateChangedDelegate, IHoldTheLineProgressChangedDelegate {
    
    var statusText : BehaviorSubject<String?> = .init(value: nil)
    var stateProvider : BehaviorSubject<EVideoCallState?> = .init(value: nil)
    
    func stateChanged(newValue: EVideoCallState, oldValue: EVideoCallState?) {
        self.stateProvider.onNext(newValue)
    }
    
    func progressChanged(newValue: HoldTheLineProgress, oldValue: HoldTheLineProgress?) {
        let position = newValue.positionInQueue
        let averageWaitingMinutes = newValue.averageWaitingTimeOfLastHourInSeconds
        let averageText = "\("webid.text.status.wait.plain.averageWaiting".localized)\(averageWaitingMinutes)"
        let positionText = "\("webid.text.status.wait.plain.position".localized)\(position)"
        let final = averageText + "\n" + positionText
        self.statusText.onNext(final)
    }
    
}

extension WebIDViewController {
    
    var certificates : [SecCertificate] {
        guard let fileURL = Bundle.main.url(forResource: "test.webid-solutions.de", withExtension: "cer"),
              let data = try? Data(contentsOf: fileURL) else
        {
            fatalError("unable to find certificate: test.webid-solutions.de.cer")
        }
        
        guard let certificate = SecCertificateCreateWithData(kCFAllocatorDefault, data as CFData) else {
            fatalError("unable to load: \(fileURL)")
        }
        return [certificate]
    }
    
    func initializeSession() {
        let certificates = self.certificates
        let envToUse = try! WebIdSdkEnvironment(url: URLComponents(string: SDKMetadata.host)!, pinningCertificates:
                                                    certificates)
        let webIdMobileAppSdkFactory: IWebIdMobileAppSdkFactory = WebIdMobileAppSdkFactory()
        let sdkInstance: IWebIdMobileAppSdk = webIdMobileAppSdkFactory.create(
            environment: envToUse, username: SDKMetadata.mobileUserName, apiKey: SDKMetadata.mobileAPIKey)
        self.sdk = sdkInstance
    }
    
    func verifyUserActions() {
        guard let session = self.session else {
            fatalError("user-action was not defined!")
        }
        self.sdk?.verify(actionId: session.webIDUserAction) { result in
            switch result {
            case .failure(let error): self.handleError(error)
            case .success(let actionIDResult):
                self.actionIDResult = actionIDResult
                self.setup(with: actionIDResult)
                self.startCall()
                return
            }
        }
    }
    
    func setup(with actionID: VerifyActionIdResult) {
        self.setupCustomerIDText(actionID: actionID)
        self.setupTestTANEnvironment()
    }
    
    func setupTestTANEnvironment() {
        if !(Enviroment.current == .prod) {
            self.setupAgentPlaceholderImage(isHidden: true)
        }
    }
    
    func setupCustomerIDText(actionID: VerifyActionIdResult) {
        let idString = actionID.userAction.actionId
        let idParts = [Character](idString)
        let combined = idParts.reduce(into: [[Character]]()) {
            if let currentIdx = $0.firstIndex(where: { $0.count < 3 }) {
                var current = $0[currentIdx]
                current.append($1)
                $0[currentIdx] = current
            } else {
                $0.append([$1])
            }
        }.map { String($0) }
        let final = combined.joined(separator: "-")
        self.customerIDLabel.text = "webid.label.yourid".localized + final
    }
    
    func handleError(_ error: Error) {
        switch error {
        case let error as WebIdMobileAppSdkError:
            self.handleWebIDSDKError(error)
        case let error as UserActionError:
            self.handleUserActionError(error)
        default: return
        }
    }
    
    func handleWebIDSDKError(_ error: WebIdMobileAppSdkError) {
        switch(error) {
        case .AccessDenied:
            self.presentInternalAlertAndDismiss("wrong API credentials: \(error)")
        case .AuthenticationFailed:
            self.presentInternalAlertAndDismiss("wrong API credentials: \(error)")
        case .IllegalArgument:
            self.presentInternalAlertAndDismiss("Exception during getting user action status: \(error)")
        case let .NSURLErrorDomain(nwError):
            if nwError.domain == NSURLErrorDomain && nwError.code == NSURLErrorCancelled {
                self.presentInternalAlertAndDismiss("Operation was cancelled: \(error)")
            } else {
                self.presentInternalAlertAndDismiss("API connection failed: \(error)")
            }
        case let .HttpError(statusCode, _):
            self.presentInternalAlertAndDismiss("Network failure \(statusCode)")
        default:
            self.presentInternalAlertAndDismiss("API connection failed: \(error)")
        }
    }
    
    func handleUserActionError(_ error: UserActionError) {
        switch error {
        case .IllegalState:
            // user-action found, but state denies usage case .NotFound:
            // unknown user-action }
            return
        case .ECheckRequired( _): return
        case .PreCheckRequired( _): return
        case .LegalTermsConfirmationMissing( _): return
        case .AutoIdentRequired( _):
            // user-action required a process, which is not supported by the SDK // Use getEntryLink() of exception to redirect the user in a browser // to WebId to finish the process
            return
        default: return
        }
    }
    
    private func presentInternalAlertAndDismiss(_ message: String) {
        self.presentAlert(message: message, completion: {
            self.popToPreviousController(animated: true)
        })
    }
    
    private func openSettings() {
        self.presentAlert(title: "webid.permission.title".localized,
                          message: "webid.permission.text".localized,
                          actionMessage: "webid.permission.settings".localized,
                          addAction: self.openSettingsAction,
                          okTitle: "webid.permission.cancel".localized) { [weak self] in
            self?.popToPreviousController(animated: true)
        }
    }
    
}

extension WebIDViewController {
    
    @IBAction func applyTan() {
        guard let tan = self.tan, let userAction = self.actionIDResult?.userAction else {
            print("empty self.tan!")
            return
        }
        self.startProgress()
        self.sdk?.verify(tan: tan, of: userAction) { result in
            self.stopProgress()
            switch result {
            case .failure(let error as TanError) where error == .IncorrectTan:
                let message = "webid.error.call.tan.incorrect".localized
                self.presentAlert(message: message)
            case .failure(let error as TanError) where error == .IllegalState:
                let message = "webid.error.call.tan.wrongState".localized
                self.presentAlert(message: message)
            case .failure(let error):
                self.presentInternalAlertAndDismiss("Unknown error occured: \(error)")
            case .success(_):
                let message = "webid.text.status.checking".localized
                self.updateStatusLabel(text: message)
                self.startProgress()
                self.checkUserState()
            }
        }
    }
    
    private func checkUserState() {
        guard let userAction = self.actionIDResult?.userAction else {
            print("empty self.actionIDResult?.userAction!")
            return
        }
        self.sdk?.getStatus(of: userAction) { [weak self] in
            switch $0 {
            case .failure(let error):
                let message = "webid.error.call.internal".localized + " \(error)"
                self?.presentInternalAlertAndDismiss(message)
            case .success(let status):
                switch status.identState {
                case .successful:
                    self?.startProgress()
                    self?.finishCall({ [weak self] (error) in
                        if let error = error {
                            self?.stopProgress()
                            self?.presentAlert(message: error.localizedDescription)
                        } else {
                            self?.sendQualification({ [weak self] (error) in
                                self?.stopProgress()
                                if let error = error {
                                    self?.presentAlert(message: error.localizedDescription)
                                } else {
                                    self?.presentSuccessViewController()
                                }
                            })
                        }
                    })
                    return
                case .unsuccessful:
                    self?.startProgress()
                    self?.finishCall({ [weak self] (error) in
                        self?.stopProgress()
                        if let error = error {
                            self?.presentAlert(message: error.localizedDescription)
                        } else {
                            self?.presentFailureViewController()
                        }
                    })
                    return
                default:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
                        self?.checkUserState()
                    }
                }
            }
        }
    }
    
    private func presentSuccessViewController() {
        _ = ViewControllerProvider.setupWebIDDoneViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    private func presentFailureViewController() {
        _ = ViewControllerProvider.setupWebIDFailViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    private func finishCall(_ completionHandler: @escaping (Error?) -> Void) {
        guard let session = self.session else { return }
        self.remote.finishCall(session) { error in
            if let error = error {
                completionHandler(error)
            } else {
                completionHandler(nil)
            }
        }
    }
    
    private func sendQualification(_ completionHandler: @escaping (Error?) -> Void) {
        guard let session = self.session else { return }
        self.remote.sendQualificationForm(session, "", .unqualified, []) { error in
            if let error = error {
                completionHandler(error)
            } else {
                completionHandler(nil)
            }
        }
    }
    
}
