//
//  AudioVideoAccessProvider.swift
//  8Pitch
//
//  Created by 8pitch on 04.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import AVFoundation
import RxSwift

class AudioVideoAccessProvider {
    
    enum Access {
        case authorized
        case denied
        case notDetermined
    }
    
    var accessProvider : BehaviorSubject<Access?> = .init(value: nil)
    private var audioAccess : Access = .notDetermined {
        didSet {
            self.accessProvider.onNext(self.isAuthorized)
        }
    }
    private var videoAccess : Access = .notDetermined {
        didSet {
            self.accessProvider.onNext(self.isAuthorized)
        }
    }
    
    private var isAuthorized : Access {
        if self.audioAccess == .authorized && self.videoAccess == .authorized {
            return .authorized
        }
        if self.audioAccess == .denied || self.videoAccess == .denied {
            return .denied
        }
        return .notDetermined
    }
    
    func requestAccessIfNeeded() {
        self.requestVideoAccessIfNeeded()
        self.requestAudioAccessIfNeeded()
    }
    
    private func requestVideoAccessIfNeeded() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized: // The user has previously granted access to the camera.
                self.videoAccess = .authorized
            case .notDetermined: // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        self.videoAccess = .authorized
                    } else {
                        self.videoAccess = .denied
                    }
                }
            case .denied: // The user has previously denied access.
                self.videoAccess = .denied
            case .restricted: // The user can't grant access due to restrictions.
                self.videoAccess = .denied
            default: self.videoAccess = .notDetermined
        }
    }
    
    private func requestAudioAccessIfNeeded() {
        switch AVAudioSession.sharedInstance().recordPermission {
            case .denied:
                self.audioAccess = .denied
            case .granted:
                self.audioAccess = .authorized
            case .undetermined:
                AVAudioSession.sharedInstance().requestRecordPermission { granted in
                    if granted {
                        self.audioAccess = .authorized
                    } else {
                        self.audioAccess = .denied
                    }
                }
            default: self.audioAccess = .notDetermined
        }
    }
    
}
