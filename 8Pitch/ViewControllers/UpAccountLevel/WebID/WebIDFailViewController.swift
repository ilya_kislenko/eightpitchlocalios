//
//  WebIDFailViewController.swift
//  8Pitch
//
//  Created by 8pitch on 13.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class WebIDFailViewController : GenericViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.whiteClose), style: .plain, target: self, action: #selector(closeKYCFlow))
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.customizeForColored()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor(.backgroundDark)
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        guard let accountGroup = self.session?.accountGroup else {
            print("self.session?.accountGroup == nil")
            return
        }
        let path = accountGroup == .initiator ? Constants.faqPathPI : Constants.faqPathInvestor
        self.openWebScreenWithPath(path)
    }

    override func popToPreviousController(animated: Bool) {
        guard let controller = self.previousController else {
            print("self.previousController == nil")
            return
        }
        UIApplication.shared.keyWindow?.rootViewController = controller
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
}

private extension WebIDFailViewController {
    enum Constants {
        static let faqPathPI: String = "faq/issuers"
        static let faqPathInvestor: String = "faq/investors"
    }
}
