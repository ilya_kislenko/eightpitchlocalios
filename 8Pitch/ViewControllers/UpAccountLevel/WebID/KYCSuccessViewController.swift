//
//  KYCSuccessViewController.swift
//  8Pitch
//
//  Created by 8pitch on 05.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class KYCSuccessViewController : RedViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.whiteClose), style: .plain, target: self, action: #selector(closeKYCFlow))
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        let _ = ViewControllerProvider.setupWebIDViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func popToPreviousController(animated: Bool) {
        guard let controller = self.previousController else {
            print("self.previousController == nil")
            return
        }
        UIApplication.shared.keyWindow?.rootViewController = controller
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
}
