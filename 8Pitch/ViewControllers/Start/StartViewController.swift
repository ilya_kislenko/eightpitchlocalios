//
//  StartViewController.swift
//  8Pitch
//
//  Created by 8pitch on 14.07.2020.
//  Copyright © 2020 8pitch. All rights reserved.
//

import UIKit
import Input

class StartViewController: GenericViewController {
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.nextController is SplashViewController {
            self.pushNextController(animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
        
    @IBAction func registrationSelected(_ sender: UIButton) {
        let _ = ViewControllerProvider.setupTypesViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    @IBAction func loginSelected(_ sender: UIButton) {
        self.login()
    }
    
    private func login() {
        let _ = ViewControllerProvider.setupLoginViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}
