//
//  SplashViewController.swift
//  8Pitch
//
//  Created by 8pitch on 10.07.2020.
//  Copyright © 2020 8pitch. All rights reserved.
//

import UIKit

class SplashViewController: GenericViewController {
    
    private let counter = CounterView()

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.modalPresentationStyle = .fullScreen
        self.modalTransitionStyle = .crossDissolve
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.counter)
        self.counter.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.trailing.leading.equalToSuperview().inset(Constants.counterSideInset)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.counter.startAnimating() {
            self.popToPreviousController(animated: false)
        }
    }
    
    override func popToPreviousController(animated: Bool) {
        self.dismiss(animated: animated, completion: nil)
        self.previousController?.nextController = nil
    }

}

private extension SplashViewController {
    
    enum Constants {
        static let counterSideInset: CGFloat = 40
    }
}

