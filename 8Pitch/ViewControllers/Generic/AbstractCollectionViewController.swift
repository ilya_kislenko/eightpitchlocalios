//
//  AbstractCollectionViewController.swift
//  8Pitch
//
//  Created by 8pitch on 26.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import RxSwift

struct AbstractCollectionViewDataSourceV2<T : Model> {
    
    var models : [CollectionViewModel<T>]
    
}

class AbstractCollectionViewController<T : Model> : CustomTitleViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var registration : UIStoryboard {
        UIStoryboard(name: "Registration", bundle: nil)
    }
    
    var nextButtonActionDisposeBag = DisposeBag()
    var nextButtonAction: BehaviorSubject<NextState?>? {
        willSet {
            self.nextButtonActionDisposeBag = DisposeBag()
        }
    }

    var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>?
    var confirmationFooterViewType : PublishSubject<InputSMSView.ViewType?>?
    var emailConfirmationFooterViewType : PublishSubject<VerifyEmailView.ViewType?>?
    var applyChangesFooterViewType : PublishSubject<ApplyChangesView.ViewType?>?
    
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var progressView : ProgressView!
    
    var collectionViewLayout : DecoratableCollectionViewLayout<T>?

    var dataSource : AbstractCollectionViewDataSourceV2<T>?
    var model : BehaviorSubject<T>? {
        fatalError("this needs to be overridden!")
    }
    
    var registrationStepsCount: Int {
        switch self.session?.registrationType {
            case .some(.individualInvestor): return 4
            default: return 5
        }
    }
    
    var kycStepsCount: Int {
        switch self.session?.accountGroup {
        case .initiator:
            return 4
        default:
            return 5
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    /// MARK: Methods to be overridden
    
    func setup() {
        self.setupProgressView()
        self.setupModel()
        self.setupLayout()
        self.registerCells()
        self.setupDataSource()
        self.setupKeyboardGesture()
        self.setupTapGesture()
    }
    
    func setupProgressView() {
        if self.progressView != nil {
            self.progressView.numberOfItems = self.session?.user != nil ? self.kycStepsCount : self.registrationStepsCount
        }
    }
    
    func setupLayout() {
        guard let model = self.model else {
            fatalError("invalid controller setup!")
        }
        let layout = DecoratableCollectionViewLayout(model: model)
        layout.contentInsets = UIEdgeInsets(top: 0, left: AbstractCollectionViewConstants.edgeInset, bottom: 0, right: AbstractCollectionViewConstants.edgeInset)
        self.collectionView.collectionViewLayout = layout
        self.collectionViewLayout = layout
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setupTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.endEditing))
        tap.cancelsTouchesInView = false
        self.collectionView.addGestureRecognizer(tap)
        self.collectionView.delaysContentTouches = false
    }
    
    private func setupKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    private func setupKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardAppeared), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDismissed), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc private func keyboardAppeared(_ notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let frame = keyboardFrame.cgRectValue
        let fixedFrame = self.view.convert(frame, to: self.view.window)
        let bottomInset = fixedFrame.height - self.view.safeAreaInsets.bottom
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: bottomInset, right: 0)
    }
    
    @objc private func keyboardDismissed(_ notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            self.collectionView.contentInset = .zero
        }
    }
    
    func setupDataSource() {
    }
    
    func registerCells() {
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(ErrorCell.nibForRegistration, forCellWithReuseIdentifier: ErrorCell.reuseIdentifier)
        layout.nextButtonAction.subscribe(onNext: {
            self.nextButtonAction = $0
        }).disposed(by: self.disposables)
        layout.confirmationFooterViewType.subscribe(onNext: {
            self.confirmationFooterViewType = $0
        }).disposed(by: self.disposables)
        layout.emailConfirmationFooterViewType.subscribe(onNext: {
            self.emailConfirmationFooterViewType = $0
        }).disposed(by: self.disposables)
        layout.navigationFooterViewType.subscribe(onNext: {
            self.navigationFooterViewType = $0
        }).disposed(by: self.disposables)
        layout.editFooterViewType.subscribe(onNext: {
            self.applyChangesFooterViewType = $0
        }).disposed(by: self.disposables)
    }
    
    func setupModel() {}
    
    func verifyModelAndNextButtonActionIfPossible() {
        guard let model = try? self.model?.value(),
            let isValid = try? model.isValid.value() else { return }
        switch isValid {
            case .success(let entered) where entered: self.nextButtonTapped()
            case .failure(let error as ModelError): self.presentAlert(message: error.localizedDescription)
            default: return
        }
    }
    
    func nextButtonTapped() {}
    
    @objc private func endEditing() {
        self.collectionView.endEditing(true)
        self.view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let model = self.dataSource?.models[indexPath.section] else {
            fatalError("invalid self.dataSource setup!")
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: model.reuseIdentifiers[indexPath.row], for: indexPath)

        if let modelCell = cell as? CellModel<T> {
            modelCell.content = model.content
            modelCell.indexPath = indexPath
            modelCell.model = model.model
        } else if let modelCell = cell as? ErrorCell {
            modelCell.textLabel.text = model.errorMessage
            modelCell.textAlignment = model.errorTextAlignment
        }
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.dataSource?.models.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource?.models[section].reuseIdentifiers.count ?? 0
    }
    
    override func popToPreviousController(animated: Bool) {
        super.popToPreviousController(animated: animated)
        self.resetModel()
    }
    
    func resetModel() {
        try? self.model?.value().cleanUp()
    }
    
}

struct AbstractCollectionViewConstants {
    static let edgeInset : CGFloat = 24.0
}
