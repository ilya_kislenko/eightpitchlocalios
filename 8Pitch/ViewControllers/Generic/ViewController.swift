//
//  ViewController.swift
//  8Pitch
//
//  Created by 8pitch on 26.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let activityIndicator: UIImageView = {
        UIImageView(image: UIImage(.progress))
    }()
    
    private let blurView: BlurView = {
        BlurView()
    }()
    
    func startProgress() {
        self.view.endEditing(true)
        self.view.addSubview(self.blurView)
        self.blurView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        self.blurView.contentView.addSubview(self.activityIndicator)
        self.activityIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        self.animateView()
    }
    
    func stopProgress() {
        self.blurView.removeFromSuperview()
    }
    
    private func animateView() {
        let rotationAnimation = CABasicAnimation()
        rotationAnimation.keyPath="transform.rotation"
        rotationAnimation.fromValue = 0
        rotationAnimation.toValue = .pi * 2.0
        rotationAnimation.duration = 1
        rotationAnimation.repeatCount = Float.greatestFiniteMagnitude
        self.activityIndicator.layer.add(rotationAnimation, forKey: "rotationAnimation")
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            viewControllerToPresent.popoverPresentationController?.sourceView = self.view
            viewControllerToPresent.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: self.view.bounds.width, height: self.view.bounds.height/2)
        }
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}
