//
//  CollectionViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 01.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

// CollectionViewModel is a representation of dynamic section in UICollectionView
class CollectionViewModel<T : Model> {
    
    var model : BehaviorSubject<T>
    
    // Rows for collection view, indexed by IndexPath.item
    var reuseIdentifiers : [String] {
        guard let retVal = try? self.reuseIdentifiersProvider.value() else {
            fatalError("invalid model setup!")
        }
        return retVal
    }
    
    var errorTextAlignment : NSTextAlignment = .center
    var errorMessage : String?
    var content: Any?
    
    private var collectionView : UICollectionView
    private var section : Int
    
    let reuseIdentifiersProvider : BehaviorSubject<[String]?> = .init(value: nil)
    private var disposables : DisposeBag = .init()
    private var errorIndexPath : IndexPath {
        IndexPath(item: 1, section: self.section)
    }
    private var errorType : Error.Type
    
    init(collectionView: UICollectionView,
         section: Int,
         model: BehaviorSubject<T>,
         errorType: Error.Type,
         cellReuseIdentifier: String,
         content: Any? = nil)
    {
        self.errorType = errorType
        self.collectionView = collectionView
        self.model = model
        self.reuseIdentifiersProvider.onNext([cellReuseIdentifier])
        self.section = section
        self.content = content
        self.setup()
    }
    
    private func setup() {
        guard let model = try? self.model.value() else {
            fatalError("model is nil")
        }
        model.isValid
            .compactMap { $0 }
            // in order to make UI more stable, we need to throttle on user input events
            .delay(.milliseconds(100), scheduler: MainScheduler.instance)
            .subscribe(onNext: { result in
                switch result {
                    case .failure(let error) where type(of: error) == self.errorType:
                        self.errorTextAlignment = model.errorTextAlignment
                        self.errorMessage = error.localizedDescription
                        self.insertErrorRow()
                    default:
                        self.errorMessage = nil
                        self.removeErrorRow()
                }
            }).disposed(by: self.disposables)
    }
    
    private func insertErrorRow() {
        guard self.reuseIdentifiers.count < 2 else {
            self.reloadRow()
            return
        }
        var newValue = self.reuseIdentifiers
        newValue.append(ErrorCell.reuseIdentifier)
        self.reuseIdentifiersProvider.onNext(newValue)
        self.insertRow()
    }
    
    private func removeErrorRow() {
        guard self.reuseIdentifiers.count == 2 else {
            return
        }
        var newValue = self.reuseIdentifiers
        newValue.removeLast()
        self.reuseIdentifiersProvider.onNext(newValue)
        self.deleteRow()
    }
    
    private func reloadRow() {
        self.collectionView.reloadItems(at: [self.errorIndexPath])
    }
    
    private func insertRow() {
        self.collectionView.insertItems(at: [self.errorIndexPath])
    }
    
    private func deleteRow() {
        self.collectionView.deleteItems(at: [self.errorIndexPath])
    }
    
}
