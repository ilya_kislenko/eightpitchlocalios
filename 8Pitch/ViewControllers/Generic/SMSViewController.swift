//
//  SMSViewController.swift
//  8Pitch
//
//  Created by 8pitch on 27.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class SMSViewController: AbstractCollectionViewController<Code> {
    
    private var screenLeft: Bool = false
    
    override var model: BehaviorSubject<Code>? {
        return self.session?.codeProvider
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.screenLeft {
            self.resendButtonAction?.onNext(.loading)
        }
        try? self.model?.value().isValid.subscribe(onNext: {
            [weak self] in
            switch $0 {
            case .success(let value):
                if value {
                    self?.startProgress()
                    self?.verifyToken()
                }
            default: return
            }
        }).disposed(by: disposables)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.screenLeft = true
        (self.collectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? CodeTextCell)?.input.clearField()
        self.disposables = DisposeBag()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        try? self.session?.codeProvider.value().cleanUp()
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: { [weak self] in
                    self?.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        guard let isValid = try? self?.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self?.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            guard value else { return }
                            self?.startProgress()
                            self?.verifyToken()
                        }
                    case .next: self?.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    var resendButtonAction : BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.resendButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: { [weak self] in
                    self?.stopProgress()
                    switch $0 {
                    case .error: return
                    case .loading:
                        self?.startProgress()
                        self?.resendCode()
                    case .next: return
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: CodeError.self, cellReuseIdentifier: CodeTextCell.reuseIdentifier),
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    func verifyToken() {}
    
    func resendCode() {}
    
    func sendCode() {}
    
}
