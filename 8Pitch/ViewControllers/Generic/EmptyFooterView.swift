//
//  EmptyFooterView.swift
//  8Pitch
//
//  Created by 8pitch on 02.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class EmptyFooterView : UICollectionReusableView {
    
    class var reuseIdentifier: String {
        "EmptyFooterView"
    }
    
    class var nibForRegistration : UINib {
        UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
}
