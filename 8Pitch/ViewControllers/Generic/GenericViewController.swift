//
//  GenericViewController.swift
//  8Pitch
//
//  Created by 8pitch on 14.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import RxSwift

class GenericViewController : ViewController, Presentatable, PresentationStrategy {
    typealias AddAction = ((UIAlertController, String) -> Void)?
    
    // TODO: this needs to be moved out of ViewController, use dataProviders in Model instead
    var session : Session? {
        didSet {
            guard let session = self.session else {
                return
            }
            // session refreshes only on the last existing controller
            // if there's an error for refresh token - logout, pop to the roots
            session.shouldRefreshTokens = { [weak self] _ in
                self?.presentExpiredTokenDialog()
            }
        }
    }
    
    var remote = Remote()
    var disposables : DisposeBag = .init()
    
    var splashController: GenericViewController?
    weak var nextController: GenericViewController?
    weak var previousController: GenericViewController?
    weak var kycInitialController: GenericViewController?
    
    var cameFromProfile: Bool = false
    
    private var blurView: UIVisualEffectView = {
        BlurView(effect: UIBlurEffect(style: .dark))
    }()
    
    private var navigationBlurView: UIVisualEffectView = {
        BlurView(effect: UIBlurEffect(style: .dark))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.setupUIForIpad()
        }
    }
    
    func setupUIForIpad() {}
    
    func pushNextController(animated: Bool) {
        guard let controller = self.nextController else {
            print("self.nextController == nil")
            return
        }
        switch controller {
        case is SheetDetentPresentationSupport:
            addBlurEffect()
            controller.transitioningDelegate = self as? UIViewControllerTransitioningDelegate
            self.present(controller, animated: animated, completion: nil)
        case is SplashViewController:
            self.present(controller, animated: animated, completion: nil)
        default:
            self.navigationController?.pushViewController(controller, animated: animated)
        }
    }
    
    func addBlurEffect() {
        self.blurView.frame = self.view.bounds
        self.view.addSubview(self.blurView)
        guard let navigationBar = self.navigationController?.navigationBar else { return }
        self.navigationBlurView.frame = navigationBar.bounds
        navigationBar.addSubview(self.navigationBlurView)
    }
    
    func removeBlurEffect() {
        self.blurView.removeFromSuperview()
        self.navigationBlurView.removeFromSuperview()
    }
    
    func popToPreviousController(animated: Bool) {
        guard let controller = self.previousController else {
            print("self.previousController == nil")
            return
        }
        self.navigationController?.popToViewController(controller, animated: animated)
    }
    
    @objc func closeKYCFlow() {
        guard let vc = self.kycInitialController else {
            fatalError("kycInitialController is not exist")
        }
        self.navigationController?.popToViewController(vc, animated: true)
    }
    
    func presentAlert(title: String = "error.dialog.title".localized,
                      message: String,
                      actionMessage: String = "",
                      addAction: AddAction = nil,
                      okTitle: String = "error.dialog.button.ok.title".localized,
                      completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        addAction?(alert, actionMessage)
        alert.addAction(UIAlertAction(title: okTitle, style: .default) { _ in
            alert.dismiss(animated: true)
            completion?()
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func addCopyAction(_ alert: UIAlertController, _ message: String) {
        let action = UIAlertAction(title: message, style: .default) { _ in
            let pasteboard = UIPasteboard.general
            pasteboard.string = message
            alert.dismiss(animated: true)
        }
        alert.addAction(action)
    }
    
    func openSettingsAction(_ alert: UIAlertController, _ message: String) {
        let action = UIAlertAction(title: message, style: .default) { _ in
            if UIApplication.shared.canOpenURL(URL(string: UIApplication.openSettingsURLString)!) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                self.popToPreviousController(animated: true)
            }
        }
        alert.addAction(action)
    }
    
    func cancelAction(_ alert: UIAlertController, _ message: String) {
        let action = UIAlertAction(title: message, style: .default) { _ in
            alert.dismiss(animated: true)
        }
        alert.addAction(action)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func upgradeAccountTapped() {
        self.checkAccountLevel()
    }
    
    func checkAccountLevel() {
        switch self.session?.level {
        case .first:
            _ = ViewControllerProvider.setupVerifyEmailViewController(rootViewController: self)
        case .emailConfirmed:
            _ = ViewControllerProvider.setupAddSecurityViewController(rootViewController: self)
        case .twoFAEnabled:
            _ = ViewControllerProvider.setupPersonalViewController(rootViewController: self)
        case .KYCPassed:
            _ = ViewControllerProvider.setupWebIDViewController(rootViewController: self)
        case .WEBIDPassed,
             .webIDInProgress:
            _ = ViewControllerProvider.setupQuestionnaireExplanationViewController(rootViewController: self)
        case .second,
             .questionaryPassed:
            self.closeCurrentFlow()
            return
        case .none:
            return
        }
        self.nextController?.hidesBottomBarWhenPushed = true
        self.pushNextController(animated: true)
    }
    
    func passQuestionaryTapped() {
        _ = ViewControllerProvider.setupQuestionnaireExplanationViewController(rootViewController: self)
        self.nextController?.hidesBottomBarWhenPushed = true
        self.pushNextController(animated: true)
    }
    
    private func closeCurrentFlow() {
        guard let _ = self.kycInitialController else {
            self.openMainApp()
            return
        }
        self.closeKYCFlow()
    }
    
    func openWebScreenWithPath(_ path: String) {
        guard let url = self.remote.webURL?.absoluteString else { return }
        self.nextController = ViewControllerProvider.setupWebViewController(rootViewController: self)
        (self.nextController as? WebViewController)?.url = url + path
        self.nextController?.hidesBottomBarWhenPushed = true
        self.pushNextController(animated: true)
    }
    
    func openWebScreenWithAbsolutPath(_ path: String) {
        self.nextController = ViewControllerProvider.setupWebViewController(rootViewController: self)
        (self.nextController as? WebViewController)?.url = path
        self.nextController?.hidesBottomBarWhenPushed = true
        self.pushNextController(animated: true)
    }
    
    func logout() {
        guard let session = session else { return }
        self.startProgress()
        self.remote.logout(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.stopProgress()
                switch $0 {
                case .success(_): fallthrough
                case .failure(_):
                    self?.session?.resetTokens()
                    ViewControllerProvider.startOverAgain()
                }
            }).disposed(by: self.disposables)
    }
    
    func presentExpiredTokenDialog() {
        self.presentAlert(message: "remote.error.token.expired".localized, completion: {
            ViewControllerProvider.startOverAgain()
        })
    }
    
    private func showNoMobileLogin() {
        _ = ViewControllerProvider.setupVotingOnlyViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openMainApp() {
        if self.session?.isVotingOnlyAccount ?? false {
            self.showNoMobileLogin()
            return
        }
        try? self.session?.loginProvider.value().rememberMeSelected = false
        let tabBarController = ViewControllerProvider.setupTabBarController(rootViewController: self)
        self.navigationController?.viewControllers = [tabBarController]
    }
    
    func openTwoFAFlow() {
        _ = ViewControllerProvider.setupAddSecurityViewController(rootViewController: self)
        self.nextController?.hidesBottomBarWhenPushed = true
        self.nextController?.cameFromProfile = true
        self.pushNextController(animated: true)
    }
    
}
