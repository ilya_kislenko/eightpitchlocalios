//
//  RedViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class RedViewController: CustomTitleViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    override func setupNavigationBarColor() {
        if let navigationBar = self.navigationController?.navigationBar as? CustomNavigationBar {
            navigationBar.setupForRed()
        }
        self.navigationItem.customizeForColored()
        self.navigationController?.navigationBar.barStyle = .black
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationItem.rightBarButtonItem = nil
    }

}
