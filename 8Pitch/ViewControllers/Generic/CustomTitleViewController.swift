//
//  RegistrationViewController.swift
//  8Pitch
//
//  Created by 8pitch on 15.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Remote
import RxSwift

class CustomTitleViewController: GenericViewController {
    
    // MARK: Properties
    
    var customTitle: String?
    var numberOfLines: Int { Constants.numberOfLines }
    
    // MARK: UI Components
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    
    @IBOutlet private var customTitleLabel: CustomLabel?
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBarColor()
        self.setupCustomTitle()
    }
    
    // MARK: Action for info/faq button, override in child classes
    
    func goToProfile() {
        if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController,
           let tabBarController = navigationController.viewControllers.first as? UITabBarController {
            tabBarController.selectedIndex = 3
            ((tabBarController.viewControllers?.last as? UINavigationController)?.viewControllers.first as? ProfileViewController)?.openPersonalScreen()
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func setupNavigationBarColor() {
        if let navigationBar = self.navigationController?.navigationBar as? CustomNavigationBar {
            navigationBar.setupAppearance()
            navigationBar.setupForWhite()
        }
        self.navigationItem.customizeForWhite()
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    @objc func showInfo() {
        self.openWebScreenWithPath(Constants.faqPath)
    }
    
    @objc func backTapped() {
        self.previousController?.nextController = nil
        self.popToPreviousController(animated: true)
    }
    
    func openSetupQuickLogin() {
        let _ = ViewControllerProvider.setupSetupQuickLoginViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func useBiometry(fromLogin: Bool = false) {
        self.usePin(withBiometry: true, fromLogin: fromLogin)
    }
    
    func usePin(withBiometry: Bool = false, fromLogin: Bool = false) {
        _ = ViewControllerProvider.setupCreatePinViewController(rootViewController: self, withBiometry: withBiometry, fromLogin: fromLogin)
        self.pushNextController(animated: true)
    }
    
    func setupCustomTitle() {
        self.customTitleLabel?.textAlignment = .center
        self.customTitleLabel?.numberOfLines = self.numberOfLines
        self.customTitleLabel?.text = self.customTitle
    }
}

// MARK: Private

private extension CustomTitleViewController {
    
    @objc func infoTapped() {
        self.showInfo()
    }
    
    func setupNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.arrowBack), style: .plain, target: self, action: #selector(backTapped))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "navigationBar.faqButton".localized, style: .plain, target: self, action: #selector(self.infoTapped))
    }
    
    enum Constants {
        static let faqPath: String = "/faq/investors"
        static let numberOfLines: Int = 0
    }
}

// MARK: UIViewControllerTransitioningDelegate

extension CustomTitleViewController: UIViewControllerTransitioningDelegate {
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return SheetDetentPresentationController(presentedViewController: presented, presenting: presenting)
    }
}
