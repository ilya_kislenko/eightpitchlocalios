//
//  DarkViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class DarkViewController: CustomTitleViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func setupNavigationBarColor() {
        if let navigationBar = self.navigationController?.navigationBar as? CustomNavigationBar {
            navigationBar.setupForDark()
            navigationBar.titleTextAttributes = [
                .font: UIFont.barlow.semibold.titleSize,
                .foregroundColor: UIColor(.white),
                .kern: 0.3]
        }
        self.navigationItem.customizeForColored()
        self.navigationController?.navigationBar.barStyle = .black
        self.setNeedsStatusBarAppearanceUpdate()
    }

}
