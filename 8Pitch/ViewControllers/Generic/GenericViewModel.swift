//
//  GenericViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 8/26/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import RxSwift
import Remote
import UIKit

class GenericViewModel {
    
    private enum Constants {
        static let title = "profile.personal.personal.title".localized
        static let firstName = "profile.personal.edit.firstName".localized
        static let lastName = "profile.personal.edit.lastName".localized
        static let name = "profile.personal.personal.name".localized
        static let birth = "profile.personal.personal.birth".localized
        static let place = "profile.personal.personal.place".localized
        static let nationality = "profile.personal.personal.nationality".localized
        static let companyName = "profile.personal.company.name".localized
        static let companyType = "profile.personal.company.type".localized
        static let country = "profile.personal.address.country".localized
        static let city = "profile.personal.address.city".localized
        static let zip = "profile.personal.address.zip".localized
        static let street = "profile.personal.address.street".localized
        static let number = "profile.personal.address.number".localized
        static let email = "profile.personal.contact.email".localized
        static let phone = "profile.personal.contact.phone".localized
        static let owner = "profile.personal.financial.owner".localized
        static let iban = "profile.personal.financial.iban".localized
        static let detailsTitle = "info.details.title".localized
        static let yes = "profile.personal.yes".localized
        static let no = "profile.personal.no".localized
        static let ustl = "profile.personal.ustl".localized
        static let germanResident = "profile.personal.tex.germanResident".localized
        static let id = "investment.tax.key.number".localized
        static let office = "investment.tax.key.office".localized
        static let churchTax = "investment.tax.key.churchTax".localized
        static let churchAttribute = "profile.personal.tex.churchAttribute".localized
        static let companyHeader = "profile.personal.header.company".localized
        static let personal = "profile.personal.header.personal".localized
        static let address = "profile.personal.header.address".localized
        static let contact = "profile.personal.header.contact".localized
        static let financial = "profile.personal.header.financial".localized
        static let other = "profile.personal.edit.other".localized
        static let regNumber = "profile.personal.company.number".localized
    }
    
    struct Section {
        let type: PersonalSectionType
        let headerTitle: String?
        let rowsContent: [Row]
    }
    
    struct Row {
        let imageName: Image?
        let title: String
        let value: String?
        let type: EditCellType?
    }
    
    let remote = Remote()
    var session: Session
    
    required init(session: Session?) {
        self.session = session ?? Session(registrationType: .none)
    }
    
    var disposables : DisposeBag = .init()
    
    var isSecondLevelAccount: Bool {
        self.session.level == .second
    }
    
    var isFirstLevelAccount: Bool {
        self.session.level == .first
    }
    
    var isQuestionaryPassed: Bool {
        self.session.level == .questionaryPassed || self.session.level == .second
    }
    
    var isInitiator: Bool {
        self.session.accountGroup == .initiator
    }
    
    var isInstitutionalInvestor: Bool {
        self.session.accountGroup == .institutionalInvestor
    }
    
    var isInvestor: Bool {
        !(self.session.accountGroup == .initiator)
    }
    
    var payments: [UserPayment] {
        self.session.user?.paymentsHistory ?? []
    }
    
    var transfers: [UserTransfer] {
        self.session.user?.transfersHistory ?? []
    }
    
    var twoFAEnabled: Bool {
        self.session.twoFAEnabled
    }
    
    var isGermanTaxesInfoFilled: Bool {
        self.isSecondLevelAccount &&
        (try? self.session.user?.taxInformationProvider.value()) != nil
    }
    
    var isGermanTaxResident: Bool {
        self.isSecondLevelAccount &&
        (try? self.session.taxInformationProvider.value().taxResidentProvider.value()) ?? true
    }
    
    var info: KYC? {
        try? self.session.personalProfileInfoProvider.value()
    }
    
    var isTwoTransactionHistoryTabAvailable: Bool {
        !(self.session.user?.transfersHistory.isEmpty ?? true)
    }
    
    var sectionsContent: [Section] {
        guard let info = self.info else { return [] }
        let titleRow = Row(imageName: .prefix, title: Constants.title, value: info.title.rawValue.localized, type: .title)
        let firstNameRow = Row(imageName: nil, title: Constants.firstName, value: info.firstName, type: .firstName)
        let lastNameRow = Row(imageName: nil, title: Constants.lastName, value: info.lastName, type: .lastName)
        let fullName = Row(imageName: .KYCuser, title: Constants.name, value: try? self.session.userProvider.value().fullName, type: nil)
        let nameRowsContent = (self is EditProfileViewModel) || (self is EditViewModel) ? [titleRow, firstNameRow, lastNameRow] : [titleRow, fullName]
        var fullPersonalRowsContent: [Row] = []
        fullPersonalRowsContent.append(contentsOf: nameRowsContent)
        fullPersonalRowsContent += [Row(imageName: .KYCcalendar, title: Constants.birth, value: info.dateOfBirth, type: .birth),
                                    Row(imageName: .KYCnationality, title: Constants.nationality, value: info.nationality, type: .nationality),
                                    Row(imageName: .profilePin, title: Constants.place, value: info.placeOfBirth, type: .place)]
        let shortPersonalRowsContent = nameRowsContent
        let companyRowsContent = [Row(imageName: .company, title: Constants.companyName, value: info.companyName, type: .companyName),
                                  Row(imageName: .companyType, title: Constants.companyType, value: info.companyType?.rawValue.localized, type: .companyType),
                                  Row(imageName: .registrationNumber, title: Constants.regNumber, value: info.companyNumber, type: .companyNumber)]
        let addressRowsContent = [Row(imageName: .KYCflag, title: Constants.country, value: info.country, type: .country),
                                  Row(imageName: .KYCpin, title: Constants.city, value: info.city, type: .city),
                                  Row(imageName: .KYCzip, title: Constants.zip, value: info.zip, type: .zip),
                                  Row(imageName: .KYCStreet, title: Constants.street, value: info.street, type: .address),
                                  Row(imageName: .KYCnumber, title: Constants.number, value: info.streetNumber, type: .address)]
        let contactRowsContent = [Row(imageName: .profileMail, title: Constants.email, value: info.email, type: .email),
                                  Row(imageName: .profilePhone, title: Constants.phone, value: "+ \(info.phone)", type: .phone)]
        let finacialRowsContent = [Row(imageName: .KYCowner, title: Constants.owner, value: info.accountOwner, type: .accountOwner),
                                   Row(imageName: .KYCiban, title: Constants.iban, value: info.iban, type: .iban)]
        var otherRowsContent = [Row(imageName: .profileLaw, title: Constants.detailsTitle, value: (info.politicallyExposedPerson ?? false) ? Constants.yes : Constants.no, type: .PEP),
                                Row(imageName: .profileTaxes, title: Constants.ustl, value: (info.usTaxLiability ?? false) ? Constants.yes : Constants.no, type: .USTL)]
        otherRowsContent.append(contentsOf: self.createTaxInfo(info: info))
        
        let upgradeSection = self.isQuestionaryPassed || self.isInitiator ? nil : Section(type: .upgrade, headerTitle: nil, rowsContent: [])
        
        let companySection = self.isInstitutionalInvestor ? Section(type: .company,
                                                                    headerTitle: Constants.companyHeader,
                                                                    rowsContent: companyRowsContent) : nil
        let personalSection = Section(type: .personal,
                                      headerTitle: Constants.personal,
                                      rowsContent: isSecondLevelAccount || self is EditViewModel ? fullPersonalRowsContent : shortPersonalRowsContent)
        let addressSection = Section(type: .address,
                                     headerTitle: Constants.address,
                                     rowsContent: addressRowsContent)
        let contactSection = Section(type: .contact,
                                     headerTitle: Constants.contact,
                                     rowsContent: contactRowsContent)
        let financialSection = Section(type: .financial,
                                       headerTitle: Constants.financial,
                                       rowsContent: finacialRowsContent)
        let otherSection = Section(type: .other, headerTitle: Constants.other, rowsContent: otherRowsContent)
        
        let sectionsContent: [Section?] = [upgradeSection, companySection, personalSection, addressSection, contactSection, financialSection, otherSection]
        return sectionsContent.compactMap { $0 }
    }
    
    private func createTaxInfo(info: KYC) -> [Row] {
        var germanTaxContent: [Row] = []
        if let germanTaxInfo = try? info.germanTaxesProvider?.value(),
           let germanResident = try? germanTaxInfo.taxResidentProvider.value() {
            germanTaxContent = [Row(imageName: .profileTaxes, title: Constants.germanResident, value: germanResident ? Constants.yes : Constants.no, type: .germanTaxesResident)]
            if germanResident,
               let churchTaxLability = germanTaxInfo.churchTaxLabilityValue {
                germanTaxContent.append(contentsOf: [
                    Row(imageName: .taxID, title: Constants.number, value: germanTaxInfo.taxIDValue, type: .germanTaxesid),
                    Row(imageName: .taxOffice, title: Constants.office, value: germanTaxInfo.taxOfficeValue, type: .germanTaxesOffice),
                    Row(imageName: .churchTax, title: Constants.churchTax, value: churchTaxLability ? Constants.yes : Constants.no, type: .churchTaxLability)
                ])
                if churchTaxLability {
                    germanTaxContent.append(Row(imageName: .church, title: Constants.churchAttribute, value: germanTaxInfo.churchAttributeValue, type: .churchTaxAttribute))
                }
            }
        }
        return germanTaxContent
    }
    
    func loadProjectDetailsImageData(_ ID: String, _ imageView: UIImageView, completionHandler: VoidClosure? = nil) {
        imageView.sd_setImage(with: ID, session: self.session, placeholderImage: nil, completed: { _,_,_,_ in
            imageView.layoutSubviews()
            imageView.layoutIfNeeded()
            completionHandler?()
        })
    }
    
    func verifyEmail(_ session: Session, completionHandler: @escaping ErrorClosure) {}
    
    func saveFile(id: String, data: Data, completionHandler: @escaping URLHandler) {
        guard let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
              let fileName = self.fileName(id: id) else { return }
        let source = documentsPath.appendingPathComponent(fileName)
        do {
            try? FileManager.default.removeItem(at: source)
            try data.write(to: source, options: .atomic)
        } catch {
            completionHandler(nil, error)
            return
        }
        completionHandler(source, nil)
    }
}

// MARK: Network

extension GenericViewModel {
    func sendDeletionRequest(completionHandler: @escaping ResponseClosure) {
        guard let reason = self.session.deletionReason else {
            return
        }
        self.remote.deleteAccount(session, deletionReason: reason)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { response in
                completionHandler(response)
            }).disposed(by: self.disposables)
    }
    
    func getHistory(_ completionHandler: @escaping ErrorClosure) {
        self.remote.payments(self.session, completionHandler: { [weak self] error in
            guard let error = error else {
                self?.getTransfersHistory(completionHandler)
                return
            }
            completionHandler(error)
        })
    }
    
    func getTransfersHistory(_ completionHandler: @escaping ErrorClosure) {
        self.remote.transfersHistory(self.session, completionHandler: completionHandler)
    }
    
    func loadProjectData(_ completionHandler: @escaping (Result<InvestmentStatus?, Error>) -> Void) {
        self.projectInvestmentInfo() { (result) in
            switch result {
            case .failure(let error):
                completionHandler(.failure(error))
            case .success(let session):
                completionHandler(.success(session.projectsProvider.projectInvestmentInfo?.unprocessedInvestment?.investmentStatus))
            }
        }
    }
    
    func loadProjectInvestmentInfo(id: Int, _ completionHandler: @escaping (Result<InvestmentStatus?, Error>) -> Void) {
        self.remote.isInvestmentAllowed(self.session, projectID: id, completionHandler: { [weak self] error in
            guard let error = error else {
                self?.loadProjectInvestmentData(completionHandler)
                return
            }
            completionHandler(.failure(error))
        })
    }
    
    func loadProjectInvestmentData(_ completionHandler: @escaping (Result<InvestmentStatus?, Error>) -> Void) {
        self.projectInvestmentInfo() { (result) in
            DispatchQueue.main.async {
                switch result {
                case .failure(let error):
                    completionHandler(.failure(error))
                case .success(let session):
                    completionHandler(.success(session.projectsProvider.projectInvestmentInfo?.unprocessedInvestment?.investmentStatus))
                }
            }
        }
    }
    
    func downloadFile(id: String, completionHandler: @escaping URLHandler) {
        self.remote.document(self.session, id: id, completionHandler: { [weak self] result in
            switch result {
            case .failure(let error):
                completionHandler(nil, error)
            case .success(let data):
                self?.saveFile(id: id, data: data, completionHandler: completionHandler)
            }
        })
    }
    
    private func fileName(id: String) -> String? {
        let file = self.session.projectsProvider.expandedProject?.documents.first(where: {
            $0?.id == id
        })
        return file??.originalFileName
    }
    
    private func projectInvestmentInfo(completionHandler: @escaping ResponseClosure) {
        guard let projectID = self.session.projectsProvider.currentProject?.identifier else {
            completionHandler(.failure(GenericRequestError.invalidResponse(nil)))
            return
        }
        self.remote.projectInvestmentInfo(session, projectId: projectID) { (result) in
            switch result {
            case .failure(let error):
                completionHandler(.failure(error))
            case .success(let session):
                completionHandler(.success(session))
            }
        }
    }
    
    func user(completionHandler: @escaping ResponseClosure) {
        self.remote.user(session)
            .compactMap { $0 }
            .subscribe(onNext: { response in
                DispatchQueue.main.async {
                    completionHandler(response)
                }
            }).disposed(by: self.disposables)
    }
    
    func login(completionHandler: @escaping ResponseClosure) {
        self.remote.login(session)
            .compactMap { $0 }
            .subscribe(onNext: { response in
                DispatchQueue.main.async {
                    completionHandler(response)
                }
            }).disposed(by: self.disposables)
    }
    
    func loadInvestedProjects(completionHandler: @escaping ErrorClosure) {
        self.remote.investedProjects(session) { response, error in
            DispatchQueue.main.async {
                completionHandler(error)
            }
        }
    }
    
    func loadNotifications(completionHandler: @escaping ErrorClosure) {
        self.remote.notifications(session) { [weak self] response, error in
            guard let self = self else { return }
            let isHaveUnreadNotifications = !self.session.notificationsProvider.notifications.filter({ $0.status != .read }).isEmpty
            completionHandler(error)
        }
    }
    
    func votings(completionHandler: @escaping ErrorClosure) {
        switch session.accountGroup {
        case .initiator:
            self.remote.PIvotings(session, completionHandler: completionHandler)
        case .institutionalInvestor, .privateInvestor:
            self.remote.investorsVotings(session, completionHandler: completionHandler)
        case .none:
            return
        }
    }
}
