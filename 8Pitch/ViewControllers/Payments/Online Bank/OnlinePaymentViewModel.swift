//
//  OnlinePaymentViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/8/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import Remote

class OnlinePaymentViewModel: GenericViewModel {

    func onlinePaymentsSubmit(_ session: Session?, completionHandler: @escaping (Result<OnlinePaymentSubmitInfo?, Error>) -> Void) {
        guard let investmentID = self.session.projectsProvider.currentInvestmentID else {
                completionHandler(.failure(GenericRequestError.invalidResponse(nil)))
                return
        }
        self.remote.onlinePaymentsSubmitRequest(self.session, currentInvestmentID: investmentID) { (result) in
            switch result {
            case .failure(let error):
                completionHandler(.failure(error))
            case .success(let session):
                completionHandler(.success(session.paymentProvider.onlinePaymentSubmitInfo))
            }
        }
    }

    func klarnaPaymentsSubmit(_ session: Session?, completionHandler: @escaping (Result<OnlinePaymentSubmitInfo?, Error>) -> Void) {
        guard let session = session,
            let investmentID = session.projectsProvider.currentInvestmentID else {
                completionHandler(.failure(GenericRequestError.invalidResponse(nil)))
                return
        }
        self.remote.klarnaPaymentsSubmitRequest(session, currentInvestmentID: investmentID) { (result) in
            switch result {
            case .failure(let error):
                completionHandler(.failure(error))
            case .success(let session):
                completionHandler(.success(session.paymentProvider.onlinePaymentSubmitInfo))
            }
        }
    }
}
