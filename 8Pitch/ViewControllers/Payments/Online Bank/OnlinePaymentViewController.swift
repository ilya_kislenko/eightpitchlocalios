//
//  OnlinePaymentViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Remote

class OnlinePaymentViewController: CustomTitleViewController {
    @IBOutlet var subtitleLabel: InfoCurrencyLabel!
    @IBOutlet var typeNameLabel: InfoTitleSemiboldLabel!
    @IBOutlet var paymentSystemLogo: UIImageView!

    private lazy var viewModel : OnlinePaymentViewModel = {
        OnlinePaymentViewModel(session: session)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.customTitle = "payment.softcap.online.subTitle".localized
        self.subtitleLabel.text = "payment.softcap.online.description".localized
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.rightBarButtonItem = nil
        switch self.session?.paymentProvider.selectedPaymentMethodType {
        case .klarna:
            self.paymentSystemLogo.image = UIImage(Image.klarna)
            self.typeNameLabel.text = "payment.softcap.online.method".localized
            self.title = "payment.softcap.online.title.klarna".localized
        case .onlineUberweisen:
            self.paymentSystemLogo.image = UIImage(Image.onlinePayment)
            self.typeNameLabel.text = "payment.softcap.online.method.klarna".localized
            self.title = "payment.softcap.online.title".localized
        default:
            self.presentAlert(message: GenericRequestError.invalidResponse(nil).localizedDescription)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startOnlinePayment()
    }

    private func startOnlinePayment() {
        switch self.session?.paymentProvider.selectedPaymentMethodType {
        case .klarna:
            self.klarnaOnlinePayment()
        case .onlineUberweisen:
            self.onlineUberweisenPayment()
        default:
            self.presentAlert(message: GenericRequestError.invalidResponse(nil).localizedDescription)
        }
    }

    private func onlineUberweisenPayment() {
        self.viewModel.onlinePaymentsSubmit(self.session, completionHandler: {[weak self] (result) in
            switch result {
            case .failure(_):
                self?.showPaymentErrorController()
            case .success(let submiInfo):
                guard let _ = submiInfo?.paymentUrl,
                      let previousController = self?.previousController else {
                    self?.showPaymentErrorController()
                        break
                }
                self?.nextController = ViewControllerProvider.setupOnlinePaymentWebViewController(rootViewController: previousController)
                self?.pushNextController(animated: true)
            }
        })
    }

    private func klarnaOnlinePayment() {
        self.viewModel.klarnaPaymentsSubmit(self.session, completionHandler: {[weak self] (result) in
            switch result {
            case .failure(_):
                self?.showPaymentErrorController()
            case .success(let submiInfo):
                guard let _ = submiInfo?.paymentUrl,
                    let previousController = self?.previousController else {
                    self?.showPaymentErrorController()
                        break
                }
                self?.nextController = ViewControllerProvider.setupOnlinePaymentWebViewController(rootViewController: previousController)
                self?.pushNextController(animated: true)
            }
        })
    }

    private func showPaymentErrorController() {
        _ = ViewControllerProvider.setupPaymentProviderErrorViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}
