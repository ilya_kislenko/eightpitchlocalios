//
//  OnlinePaymentWebViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import WebKit
import Remote

class OnlinePaymentWebViewController: CustomTitleViewController {
    @IBOutlet var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.webView.allowsBackForwardNavigationGestures = true
        self.webView.navigationDelegate = self
        guard let urlString = self.session?.paymentProvider.onlinePaymentSubmitInfo?.paymentUrl,
        let url = URL(string: urlString) else {
            return
        }
        self.webView.load(URLRequest(url: url))
    }

}

extension OnlinePaymentWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let redirectURL = navigationAction.request.url {
            if redirectURL.absoluteString.contains(OnlinePaymentsSubmitRedirectionURL.abort.rawValue) {
                decisionHandler(.cancel)
                _ = ViewControllerProvider.setupPaymentProviderErrorViewController(rootViewController: self)
                self.pushNextController(animated: true)
            } else if redirectURL.absoluteString.contains(OnlinePaymentsSubmitRedirectionURL.confirm.rawValue) {
                decisionHandler(.cancel)
                let _ = ViewControllerProvider.setupPaymentProviderSuccessViewController(rootViewController: self)
                self.pushNextController(animated: true)
            } else {
                decisionHandler(.allow)
            }
        } else {
            decisionHandler(.allow)
        }
    }
}
