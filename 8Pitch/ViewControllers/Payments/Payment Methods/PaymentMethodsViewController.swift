//
//  PaymentMethodsViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/4/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let cellHeight = CGFloat(192)
}

class PaymentMethodsViewController: GenericViewController {

    @IBOutlet var titleLabel: CustomSubtitleLabel!
    @IBOutlet var collectionView: UICollectionView!

    fileprivate lazy var viewModel : PaymentMethodsViewModel = {
        PaymentMethodsViewModel(session: session)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(PaymentMethodCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: PaymentMethodCollectionViewCell.reuseIdentifier)
        self.titleLabel.text = "payment.softcap.methods.subTitle".localized
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateNavigationBar()
        self.loadInvestmentInfo()
    }

    @objc func backTapped() {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: setup Appearance
private extension PaymentMethodsViewController {
    func loadInvestmentInfo() {
        self.startProgress()
        self.viewModel.loadProjectInvestmentData() { [weak self] (result) in
            self?.stopProgress()
            switch result {
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
            case .success(_):
                return
            }
        }
    }
    
    func updateNavigationBar() {
        self.title = "payment.softcap.methods.title".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.barlow.regular.titleSize, NSAttributedString.Key.foregroundColor: UIColor(.gray)]
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.arrowBack), style: .plain, target: self, action: #selector(backTapped))
        self.setupNavigationBarColor()
    }

    func setupNavigationBarColor() {
        if let navigationBar = self.navigationController?.navigationBar as? CustomNavigationBar {
            navigationBar.setupForWhite()
        }
        self.navigationItem.customizeForWhite()
        self.setNeedsStatusBarAppearanceUpdate()
    }

    private func redirectToPaymentScreen(type: PaymentMethodType?) {
        guard let type = type else { return }
        self.session?.paymentProvider.selectedPaymentMethodType = type
        switch type {
        case .onlineUberweisen,
             .klarna:
            _ = ViewControllerProvider.setupOnlinePaymentViewController(rootViewController: self)
        case .classicBankTransfer:
            self.loadPaymentInfo()
            return
        case .secupayDirectDebit:
            _ = ViewControllerProvider.setupDirectDebitViewController(rootViewController: self)
        }
        self.pushNextController(animated: true)
    }
    
    func loadPaymentInfo() {
        guard let session = session,
            let investmentID = session.projectsProvider.currentInvestmentID else { return }
        self.startProgress()
        self.viewModel.paymentInfo(session: session, investmentID: investmentID) { [weak self] error in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.redirectToBankTransfer()
            }
        }
    }
    
    func redirectToBankTransfer() {
        _ = ViewControllerProvider.setupBankTransferViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}

extension PaymentMethodsViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.availablePaymentMethods.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PaymentMethodCollectionViewCell.reuseIdentifier, for: indexPath)

        if let cell = cell as? PaymentMethodCollectionViewCell {
            let cellModel = PaymentMethodCollectionViewCellModel(type: viewModel.availablePaymentMethods[indexPath.row], isHardCapOnly: viewModel.isHardCapOnly)
            cell.setup(model: cellModel)
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.redirectToPaymentScreen(type: viewModel.availablePaymentMethods[indexPath.row])
    }
}

extension PaymentMethodsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.bounds.width, height: Constants.cellHeight)
    }
}

