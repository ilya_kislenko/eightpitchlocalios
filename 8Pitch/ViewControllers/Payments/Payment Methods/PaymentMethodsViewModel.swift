//
//  PaymentMethodsViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/8/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class PaymentMethodsViewModel: GenericViewModel {

    var availablePaymentMethods: [PaymentMethodType] {
        switch isHardCapOnly {
        case true:
            return [.onlineUberweisen, .klarna, .classicBankTransfer]
        case false:
            return [.onlineUberweisen, .classicBankTransfer, .secupayDirectDebit]
        }
    }
    
    var isHardCapOnly: Bool {
        let softCap = self.session.projectsProvider.expandedProject?.tokenParametersDocument?.softCap
        return softCap == 0 || softCap == nil
    }
    
    public func paymentInfo(session: Session,
                               investmentID: String,
                               completionHandler: @escaping ErrorClosure) {
        if isHardCapOnly {
            self.remote.manualBankTransferInfo(session, investmentID, completionHandler)
        } else {
            self.remote.secupayBankTransferInfo(session, investmentID, completionHandler)
        }
    }
    
}
