//
//  BankTransferViewController.swift
//  8Pitch
//
//  Created by 8pitch on 07.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class BankTransferViewController: CustomTitleViewController {
    
    private lazy var viewModel : BankTransferViewModel = {
        BankTransferViewModel(session: session)
    }()
    
    @IBOutlet weak var infoLabel: ProjectDescriptionLabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "payment.title".localized
        self.customTitle = "payment.transfer.customTitle".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.load), style: .plain, target: self, action: #selector(self.loadButtonTapped))
        self.setupInfoLabel()
        self.setupCollectionView()
    }
    
    override func backTapped() {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: UICollectionViewDelegate, UICollectionViewDataSource

extension BankTransferViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Constants.numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BankTransferCell.reuseIdentifier, for: indexPath) as? BankTransferCell else { return UICollectionViewCell() }
            cell.setText(forTitle: Constants.singleCellTitle, forValue: self.viewModel.iban)
            return cell
        case 1:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DoubleBankTransferCell.reuseIdentifier, for: indexPath) as? DoubleBankTransferCell else { return UICollectionViewCell() }
            cell.setText(forTitles: Constants.doubleCellTitles, forValues: self.viewModel.paymentInfo)
            return cell
        default:
            fatalError("\(self.debugDescription): wrong number of items in collection view")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: BankTransferView.reuseIdentifier, for: indexPath) as? BankTransferView else {
                return UICollectionViewCell()
            }
            footerView.setupWith(values: self.viewModel.values, sum: self.viewModel.sum)
            footerView.buttonAction = viewModel.paymentConfirmed ?
                { [weak self] in self?.exitTapped() } : { [weak self] in self?.checkOutTapped() }
            footerView.setButtonTitle(self.viewModel.buttonTitle)
            return footerView
        }
        return UICollectionReusableView()
    }
    
}

// MARK: UICollectionViewDelegateFlowLayout

extension BankTransferViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.frame.width, height: Constants.cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: Constants.footerHeight)
    }
}

// MARK: Private

private extension BankTransferViewController {
    
    @objc func loadButtonTapped() {
        guard let session = self.session,
              let investmentID = session.projectsProvider.currentInvestmentID else { return }
        self.startProgress()
        self.viewModel.downloadInvoiceFile(session: session, investmentID: investmentID) { [weak self] url, error in
            DispatchQueue.main.async {
                self?.stopProgress()
                if let error = error {
                    self?.presentAlert(message: error.localizedDescription)
                } else if let url = url {
                    let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
                    self?.present(activityViewController, animated: true)
                }
            }
        }
    }
    
    func checkOutTapped() {
        guard let session = session,
              let investmentID = session.projectsProvider.currentInvestmentID else { return }
        self.startProgress()
        self.viewModel.confirmBankTransfer(session: session, investmentID: investmentID) { [weak self] error in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.viewModel.paymentConfirmed = true
                self?.collectionView.reloadData()
            }
        }
    }
    
    func exitTapped() {
        let _ = ViewControllerProvider.setupPaymentProviderSuccessViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func setupInfoLabel() {
        self.infoLabel.text = String(format: "payment.transfer.info".localized, String(self.session?.projectsProvider.currentInvestmentAmount?.inEUR.doubleValue ?? 0))
    }
    
    enum Constants {
        static let numberOfItems: Int = 2
        static let cellHeight: CGFloat = 80
        static let footerHeight: CGFloat = 364
        static let singleCellTitle: String = "payment.transfer.iban".localized
        static let doubleCellTitles: [String] = ["payment.transfer.bic".localized, "payment.transfer.ref".localized]
    }
    
    func setupCollectionView() {
        self.collectionView.register(BankTransferCell.nibForRegistration, forCellWithReuseIdentifier: BankTransferCell.reuseIdentifier)
        self.collectionView.register(DoubleBankTransferCell.nibForRegistration, forCellWithReuseIdentifier: DoubleBankTransferCell.reuseIdentifier)
        self.collectionView.register(BankTransferView.nibForRegistration, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: BankTransferView.reuseIdentifier)
    }
    
}

