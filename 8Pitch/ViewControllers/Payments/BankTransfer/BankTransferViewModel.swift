//
//  BankTransferViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 07.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import PDFKit

class BankTransferViewModel: GenericViewModel {
    
    public var iban: String? {
        self.session.paymentProvider.bankTransferPaymentInfo?.iban
    }
    
    public var paymentInfo: [String?] {
        let info = self.session.paymentProvider.bankTransferPaymentInfo
        return [info?.bic, info?.transactionInfo?.referenceNumber]
    }
    
    public var values: InvestmentAmountValues? {
        self.session.projectsProvider.investmentAmountValues
    }
    
    public var sum: InvestmentSum? {
        self.session.projectsProvider.currentInvestmentSum
    }
    
    public var paymentConfirmed: Bool = false
    
    public var buttonTitle: String {
        paymentConfirmed ? "payment.transfer.exit".localized : "payment.transfer.check".localized
    }
    
    public func confirmBankTransfer(session: Session,
                                    investmentID: String,
                                    completionHandler: @escaping ErrorClosure) {
        self.remote.confirmBankTransfer(session, investmentID, completionHandler)
    }
    
    public func downloadInvoiceFile(session: Session,
                                    investmentID: String,
                                    completionHandler: @escaping URLHandler) {
        self.remote.downloadInvoiceFile(session, investmentID) { [weak self] result in
            switch result {
            case .failure(let error):
                completionHandler(nil, error)
            case .success(let data):
                self?.saveFile(data: data, completionHandler: completionHandler)
            }
        }
    }
    
    public func saveFile(data: Data, completionHandler: @escaping URLHandler) {
        guard let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let source = documentsPath.appendingPathComponent("Invoice.pdf")
        do {
            try? FileManager.default.removeItem(at: source)
            try data.write(to: source, options: .atomic)
        } catch {
            completionHandler(nil, error)
            return
        }
        completionHandler(source, nil)
    }
}
