//
//  DirectDebitViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/8/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Remote
import Input

class DirectDebitViewModel: GenericViewModel {
    
    public var values: InvestmentAmountValues? {
        self.session.projectsProvider.investmentAmountValues
    }
    
    public var sum: InvestmentSum? {
        self.session.projectsProvider.currentInvestmentSum
    }
        
    public func submitPayment(session: Session,
                              investmentID: String,
                              bankAccount: BankAccount,
                              completionHandler: @escaping ErrorClosure) {
        self.remote.secupayPaymentSubmit(session, investmentID, bankAccount, completionHandler)
    }
    
}


