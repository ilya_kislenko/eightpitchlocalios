//
//  DirectDebitViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/8/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class DirectDebitViewController: CustomTitleViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private lazy var viewModel : DirectDebitViewModel = {
        DirectDebitViewModel(session: session)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "payment.title".localized
        self.customTitle = "payment.direct.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
        let name = try? self.session?.user?.accountOwnerProvider.value()
        let acc = BankAccount(owner: name)
        self.session?.paymentProvider.bankAccount = acc
        self.setupCollectionView()
        self.setupTapGesture()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing()
    }
    
}

// MARK: UICollectionViewDelegate, UICollectionViewDataSource

extension DirectDebitViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Constants.numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DirectDebitNameCell.reuseIdentifier, for: indexPath) as? DirectDebitNameCell else { return UICollectionViewCell() }
            cell.model = self.session?.paymentProvider.bankAccount
            return cell
        default:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DirectDebitDataCell.reuseIdentifier, for: indexPath) as? DirectDebitDataCell else { return UICollectionViewCell() }
            switch indexPath.row {
            case 1:
                cell.setType(.iban)
            default:
                cell.setType(.bic)
            }
            cell.model = self.session?.paymentProvider.bankAccount
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: BankTransferView.reuseIdentifier, for: indexPath) as? BankTransferView else {
                return UICollectionReusableView()
            }
            footerView.setupWith(values: self.viewModel.values, sum: self.viewModel.sum)
            footerView.buttonAction = { [weak self] in
                self?.checkOutTapped()
            }
            return footerView
        }
        return UICollectionReusableView()
    }
    
}

// MARK: UICollectionViewDelegateFlowLayout

extension DirectDebitViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.frame.width, height: Constants.cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: Constants.footerHeight)
    }
}

// MARK: Private

private extension DirectDebitViewController {
    
    enum Constants {
        static let numberOfItems: Int = 3
        static let footerHeight: CGFloat = 364
        static let cellHeight: CGFloat = 80
    }
    
    @objc func endEditing() {
        self.collectionView.endEditing(true)
        self.view.endEditing(true)
    }
    
    func setupTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.endEditing))
        tap.cancelsTouchesInView = false
        self.collectionView.addGestureRecognizer(tap)
        self.collectionView.delaysContentTouches = false
    }
    
    func setupKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    func setupKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardAppeared), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDismissed), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func keyboardAppeared(_ notification: NSNotification) {
        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue else {
            return
        }
        let frame = keyboardFrame.cgRectValue
        let fixedFrame = self.view.convert(frame, to: self.view.window)
        let bottomInset = fixedFrame.height - self.view.safeAreaInsets.bottom
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: bottomInset + 100, right: 0)
    }
    
    @objc func keyboardDismissed(_ notification: NSNotification) {
        self.collectionView.contentInset = .zero
    }
    
    func checkOutTapped() {
        guard let session = session,
            let investmentID = session.projectsProvider.currentInvestmentID,
        let bankAccount = session.paymentProvider.bankAccount else { return }
        if let error = bankAccount.validationError() {
            self.presentAlert(message: (error as? BankAccountError)?.errorDescription ?? "")
            return
        }
        self.startProgress()
        self.viewModel.submitPayment(session: session, investmentID: investmentID, bankAccount: bankAccount) { [weak self] error in
            self?.stopProgress()
            if let _ = error {
            self?.showDeclinedPaymentScreen()
            } else {
                self?.showSuccessScreen()
            }
        }
    }
    
    func showDeclinedPaymentScreen() {
        _ = ViewControllerProvider.setupPaymentProviderErrorViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func showSuccessScreen() {
        let _ = ViewControllerProvider.setupPaymentProviderSuccessViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func setupCollectionView() {
        self.collectionView.register(DirectDebitNameCell.nibForRegistration, forCellWithReuseIdentifier: DirectDebitNameCell.reuseIdentifier)
        self.collectionView.register(DirectDebitDataCell.nibForRegistration, forCellWithReuseIdentifier: DirectDebitDataCell.reuseIdentifier)
        self.collectionView.register(BankTransferView.nibForRegistration, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: BankTransferView.reuseIdentifier)
    }
    
}
