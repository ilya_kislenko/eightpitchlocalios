//
//  PaymentProviderErrorViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/10/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PaymentProviderErrorViewController: RedViewController {

    @IBOutlet var infoLabel: CustomSubtitleDarkLabel!
    @IBOutlet var hintLabel: HintLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeTapped))
        self.setupLabels()
    }
    
    @objc private func closeTapped() {
        self.previousController = self.previousController?.previousController
        self.popToPreviousController(animated: true)
    }
    
    @IBAction func okTapped(_ sender: UIButton) {
        self.closeTapped()
    }
    

    private func setupLabels() {
        self.infoLabel.text = "payment.error.subtitle".localized
        self.hintLabel.text = "payment.error.tryAgain".localized
    }
}
