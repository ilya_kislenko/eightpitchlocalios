//
//  PaymentProviderSuccessViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/10/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PaymentProviderSuccessViewController: RedViewController {
    @IBOutlet var subTitleLabel: CustomSubtitleDarkLabel!
    @IBOutlet var projectNameLabel: HintLabel!
    @IBOutlet var descriptionLabel: InfoWhiteLabel!
    @IBOutlet var continueButton: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeTapped))
        self.setupLabels()
    }

    @objc private func closeTapped() {
        self.redirectAction()
    }

    @IBAction func continueAction(_ sender: Any) {
        self.redirectAction()
    }

    private func setupLabels() {
        self.subTitleLabel.text = "payment.success.subtitle".localized
        self.projectNameLabel.text = String(format: "payment.success.projectName".localized, self.session?.projectsProvider.currentProject?.companyName ?? "")
        self.descriptionLabel.text = "payment.success.description".localized
        self.continueButton.setTitle("payment.success.continueButton".localized, for: .normal)
    }

    private func redirectAction() {
        if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController,
           let tabBarController = navigationController.viewControllers.first as? UITabBarController {
            tabBarController.selectedIndex = 1
            tabBarController.tabBar.isHidden = false
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
