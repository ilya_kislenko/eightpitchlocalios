//
//  PresentationStrategy.swift
//  8Pitch
//
//  Created by 8pitch on 14.07.2020.
//

import Foundation

protocol Presentatable {
    
    associatedtype Controller
    
    var nextController : Controller? { get set }
    var previousController : Controller? { get set }
    
}

protocol PresentationStrategy {
    
    func pushNextController(animated: Bool)
    func popToPreviousController(animated: Bool)
    
}
