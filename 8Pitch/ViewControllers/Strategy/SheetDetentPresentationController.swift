//
//  CustomSizePresentationController.swift
//  8Pitch
//
//  Created by 8pitch on 16.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class SheetDetentPresentationController: UIPresentationController {
    
    override var frameOfPresentedViewInContainerView: CGRect {
        if let infoViewController = self.presentedViewController as? SheetDetentPresentationSupport, let containerView = self.containerView {
            let height = min(containerView.bounds.height, infoViewController.contentHeight)
            let frame = CGRect(x: 0, y: containerView.bounds.height - height, width: containerView.bounds.width, height: height)
            return frame
        }
        return CGRect.zero
    }
    
    override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()
        guard let containerView = self.containerView,
            let presentedView = self.presentedView else { return }
        let tapView = UIView(frame: containerView.bounds)
        tapView.backgroundColor = .clear
        containerView.addSubview(tapView)
        tapView.addSubview(presentedView)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPresentedController(_:)))
        tapView.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissPresentedController(_ sender: UITapGestureRecognizer) {
        let presentedController = self.presentedViewController
        (presentedController as? SheetDetentPresentationSupport)?.close()
    }
    
}
