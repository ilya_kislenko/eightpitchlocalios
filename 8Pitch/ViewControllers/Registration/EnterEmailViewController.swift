//
//  EnterEmailViewController.swift
//  8Pitch
//
//  Created by 8pitch on 22.10.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class EnterEmailViewController: EmailViewController {
    
    lazy var viewModel : EmailViewModel = {
        EmailViewModel(session: session)
    }()
    
    override var navigationFooterViewType: PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.infoEmail)
        }
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = self.registrationStepsCount - 2
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "registration.title".localized
        self.customTitle = "createEmail.customTitle".localized
    }
    
    override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupInputPhoneViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
   override func verifyEmail() {
    super.verifyEmail()
        guard let model = try? self.model?.value(), let session = self.session else {
            return
        }
        self.viewModel.verifyEmail(session) { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.nextButtonAction?.onNext(.next)
                return
            }
            self?.presentAlert(message: error.localizedDescription)
            self?.nextButtonAction?.onNext(.error)
        }
    }

}
