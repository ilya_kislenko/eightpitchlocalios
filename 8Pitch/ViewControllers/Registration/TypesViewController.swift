//
//  SelectITypeViewController.swift
//  8Pitch
//
//  Created by 8pitch on 16.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class TypesViewController: CustomTitleViewController {
    
    // MARK: Overrides
    
    override var numberOfLines: Int { 1 }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // TODO: move this out of ViewController, use properties for title and customtitle with override
        self.title = "registration.type.title".localized
        self.customTitle = "accountTypes.customeTitle".localized
        self.navigationItem.rightBarButtonItem?.image = UIImage(.info)
        setupCustomTitle()
    }
    
    override func showInfo() {
        let _ = ViewControllerProvider.setupInfoViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    // MARK: Actions
    
    @IBAction func privateSelected(_ sender: UIView) {
        self.session = Session(registrationType: .individualInvestor)
        let _ = ViewControllerProvider.setupNameViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    
    @IBAction func institutionalSelected(_ sender: UIView) {
        self.session = Session(registrationType: .institutionalInvestor)
        _ = ViewControllerProvider.setupCompanyViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}
