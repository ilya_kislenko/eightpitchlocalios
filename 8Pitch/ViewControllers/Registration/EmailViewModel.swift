//
//  EmailViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 10/21/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import RxSwift

class EmailViewModel: GenericViewModel {
    
    override func verifyEmail(_ session: Session, completionHandler: @escaping ErrorClosure) {
        super.verifyEmail(session, completionHandler: completionHandler)
        self.remote.emailVerify(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { response in
                switch response {
                case .success(_):
                    completionHandler(nil)
                case .failure(let error):
                    completionHandler(error)
                }
            }).disposed(by: self.disposables)
    }
    
}
