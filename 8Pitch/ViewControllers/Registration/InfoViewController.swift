//
//  InfoViewController.swift
//  8Pitch
//
//  Created by 8pitch on 16.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import TTTAttributedLabel
import Remote

protocol SheetDetentPresentationSupport {
    
    var contentHeight: CGFloat { get }
    func close()
    
}

class InfoViewController: GenericViewController, SheetDetentPresentationSupport {
    
    enum ContentType {
        case accountType
        case personalInfo
        case IBAN
        case dashboard
        case investmentAmount
        case investorQualification
    }
    
    // MARK: UI Components
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var sideInset: NSLayoutConstraint!
    @IBOutlet weak var panView: UIView!
    
    // MARK: Properties
    
    private var dashboardContentInset: CGFloat = 0
    var contentHeight: CGFloat {
        self.stackView.frame.height + self.panView.frame.height * Constants.numberOfOffsets * 2
    }
    
    var contentType: ContentType {
        switch transitioningDelegate {
        case is DetailsViewController:
            return .personalInfo
        case is DashboardViewController:
            return .dashboard
        case is FinancialViewController:
            return .IBAN
        case is InvestmentAmountCurrencyViewController:
            return .investmentAmount
        case is InvestorQualificationViewController:
            return .investorQualification
        case is TypesViewController,
             .some(_),
             .none:
            return .accountType
        }
    }
    
    // MARK: Init
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.modalPresentationStyle = .custom
    }
    
    // MARK: Actions
    
    @IBAction func closeTapped(_ sender: UIButton) {
        self.close()
    }
    
    @IBAction func panDown(_ sender: UIPanGestureRecognizer) {
        self.close()
    }
    
    
    
    @objc func close() {
        (self.transitioningDelegate as? CustomTitleViewController)?.removeBlurEffect()
        self.dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = Constants.cornerRadius
        self.createContent()
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.close))
            swipeDown.direction = .down
            self.view.addGestureRecognizer(swipeDown)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layer.cornerRadius = Constants.cornerRadius
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (self.transitioningDelegate as? CustomTitleViewController)?.removeBlurEffect()
    }
    
    override func setupUIForIpad() {
        super.setupUIForIpad()
        if contentType == .dashboard {
            self.stackView.axis = .horizontal
            self.dashboardContentInset = UIScreen.main.bounds.width * Constants.shareOfInsetInWidth - self.sideInset.constant
        }
    }
}

// MARK: Private

extension InfoViewController: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        if url.absoluteString == Constants.profileLink {
            self.goToProfile()
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}

private extension InfoViewController {
    
    func goToProfile() {
        (self.transitioningDelegate as? CustomTitleViewController)?.removeBlurEffect()
        self.dismiss(animated: true, completion: { [weak self] in
            (self?.transitioningDelegate as? CustomTitleViewController)?.goToProfile()
        })
    }
    
    func createStackView(number: Int) -> UIView {
        let view = UIView()
        let titleLabel = InfoPageTitleSemiboldLabel()
        titleLabel.textAlignment = .left
        titleLabel.numberOfLines = 0
        titleLabel.text = self.title(number: number)
        titleLabel.isHidden = self.contentType == .investmentAmount
        let label = InfoAttributedLabel()
        label.textAlignment = .left
        label.delegate = self
        if contentType == .accountType && number == Constants.attributedStep || contentType == .investmentAmount {
            label.setText(contentType == .investmentAmount ? Constants.investmentAmountText : Constants.attributedText)
        } else if contentType == .investorQualification {
            label.setText(Constants.qualificationText(type: self.session?.accountGroup ?? .initiator))
        } else {
            label.setText(self.text(number: number))
        }
        let activeLinkAttributes = NSMutableDictionary(dictionary: label.activeLinkAttributes)
        activeLinkAttributes[NSAttributedString.Key.foregroundColor] = UIColor(.redText)
        activeLinkAttributes[NSAttributedString.Key.font] = UIFont.roboto.regular.labelSize
        label.linkAttributes = activeLinkAttributes as NSDictionary as? [AnyHashable: Any]
        label.activeLinkAttributes = activeLinkAttributes as NSDictionary as? [AnyHashable: Any]
        view.addSubview(titleLabel)
        view.addSubview(label)
        titleLabel.snp.makeConstraints {
            $0.top.leading.equalToSuperview()
            $0.trailing.equalToSuperview().inset(self.dashboardContentInset)
            $0.bottom.equalTo(label.snp.top).inset(Constants.titleLabelInset)
        }
        label.snp.makeConstraints {
            $0.bottom.trailing.leading.equalToSuperview()
            $0.trailing.equalToSuperview().inset(self.dashboardContentInset)
        }
        return view
    }
    
    func title(number: Int) -> String {
        switch self.contentType {
        case .accountType:
            return Constants.typeTitles[number]
        case .personalInfo:
            return Constants.detailsTitle
        case .IBAN:
            return Constants.ibanTitles[number]
        case .dashboard:
            return self.session?.accountGroup == .initiator ? Constants.dashboardTitleInitiator : Constants.dashboardTitleInvestor
        case .investmentAmount:
            return ""
        case .investorQualification:
            return Constants.qualificationTitle
        }
    }
    
    func text(number: Int) -> String {
        switch self.contentType {
        case .accountType:
            return Constants.typeText[number]
        case .personalInfo:
            return Constants.detailsText
        case .IBAN:
            return Constants.ibanText[number]
        case .dashboard:
            return self.session?.accountGroup == .initiator ? Constants.dashboardTextInitiator : Constants.dashboardTextInvestor
        case .investmentAmount,
             .investorQualification:
            return ""
        }
    }
    
    func addTopIcon() {
        let icon = UIImageView(frame: CGRect(x: 0, y: 0, width: Constants.iconDimension, height: Constants.iconDimension))
        icon.contentMode = .scaleAspectFit
        switch self.contentType {
        case .dashboard:
            icon.image = UIImage(.dashboardInfo)
        case .investmentAmount:
            icon.image = UIImage(.invest)
        default: break
        }
        self.stackView.addArrangedSubview(icon)
    }
    
    func createContent() {
        var numberOfSections = 0
        switch self.contentType {
        case .IBAN:
            numberOfSections = 6
        case .accountType:
            numberOfSections = 3
        case .personalInfo:
            numberOfSections = 1
        case .dashboard,
             .investmentAmount:
            numberOfSections = 1
            self.addTopIcon()
        case .investorQualification:
            numberOfSections = 1
        }
        for i in 0 ..< numberOfSections {
            let stack = createStackView(number: i)
            self.stackView.addArrangedSubview(stack)
            if self.contentType == .dashboard {
                stack.snp.makeConstraints {
                    $0.trailing.equalToSuperview().inset(self.dashboardContentInset)
                }
            }
        }
        stackView.setNeedsLayout()
        stackView.layoutIfNeeded()
    }
    
    enum Constants {
        static let typeTitles: [String] = ["info.type.title.private".localized,
                                           "info.type.title.institutional".localized,
                                           "info.type.title.initiator".localized]
        static let typeText: [String] = ["info.type.text.private".localized,
                                         "info.type.text.institutional".localized,
                                         "info.type.text.initiator".localized]
        static let detailsTitle: String = "info.details.title".localized
        static let detailsText: String = "info.details.text".localized
        static var attributedText: NSAttributedString {
            let link = NSMutableAttributedString(string: "registration.info.initiator.2".localized, attributes:[NSAttributedString.Key.foregroundColor: UIColor(.red)])
            link.addAttribute(.link, value: eightPitchLink, range: NSMakeRange(0, link.length))
            let endText = NSMutableAttributedString(string: "registration.info.initiator.3".localized, attributes: nil)
            let text = NSMutableAttributedString(string: "registration.info.initiator.1".localized, attributes: nil)
            text.append(link)
            text.append(endText)
            let range = NSRange(location: 0, length: text.length)
            text.addAttribute(.font, value: UIFont.roboto.regular.labelSize, range: NSRange(location: 0, length: text.length))
            text.addAttribute(.backgroundColor, value: UIColor.clear, range: range)
            text.addAttribute(.foregroundColor, value: UIColor(.placeholderGray), range: range)
            return text
        }
        static let ibanTitles = ["info.iban.title.p1".localized, "info.iban.title.p2".localized, "info.iban.title.p3".localized, "info.iban.title.p4".localized, "info.iban.title.p5".localized, "info.iban.title.p6".localized,]
        static let ibanText = ["info.iban.text.p1".localized, "info.iban.text.p2".localized, "info.iban.text.p3".localized, "info.iban.text.p4".localized, "info.iban.text.p5".localized, "info.iban.text.p6".localized]
        static let numberOfOffsets: CGFloat = 2
        static let topInset: CGFloat = 59
        static let cornerRadius: CGFloat = 10
        static let attributedStep: Int = 2
        static let titleLabelInset: Int = -16
        static let iconDimension: CGFloat = 80
        static let dashboardTitleInitiator = "info.dashboard.title.initiator".localized
        static let dashboardTitleInvestor = "info.dashboard.title.investor".localized
        static let dashboardTextInitiator = "info.dashboard.text.initiator".localized
        static let dashboardTextInvestor = "info.dashboard.text.investor".localized
        static let eightPitchLink = "https://www.8pitch.com"
        static let profileLink = "8pitch://profile.personal"
        static var investmentAmountText: NSAttributedString {
            let link = "investment.amount.error.classLimit.popup.link".localized
            let text = NSString(format: "investment.amount.error.classLimit.popup".localized as NSString, link)
            let attributedText = NSMutableAttributedString(string: text as String)
            let linkRange = text.range(of: link)
            let range = NSMakeRange(0, attributedText.length)
            attributedText.addAttribute(.link, value: profileLink, range: linkRange)
            attributedText.addAttribute(.font, value: UIFont.roboto.regular.labelSize, range: range)
            attributedText.addAttribute(.backgroundColor, value: UIColor.clear, range: range)
            attributedText.addAttribute(.foregroundColor, value: UIColor(.placeholderGray), range: range)
            return attributedText
        }
        static let shareOfInsetInWidth: CGFloat = 0.15
        static let dashboardPadTextInset: CGFloat = 20
        static let qualificationTitle = "investor.qualification.subtitle".localized
        static let faqPILink: String = WebURLConstants.faqPathPI
        static let faqInvestorLink: String = WebURLConstants.faqPathInvestors
        static func qualificationText(type: AccountGroup) -> NSAttributedString {
            let link = "investor.qualification.info.text.2".localized
            let text = ("investor.qualification.info.text.1".localized + link) as NSString
            let attributedText = NSMutableAttributedString(string: text as String)
            let linkRange = text.range(of: link)
            let range = NSMakeRange(0, attributedText.length)
            attributedText.addAttribute(.link, value: type == .initiator ? faqPILink : faqInvestorLink, range: linkRange)
            attributedText.addAttribute(.font, value: UIFont.roboto.regular.labelSize, range: range)
            attributedText.addAttribute(.backgroundColor, value: UIColor.clear, range: range)
            attributedText.addAttribute(.foregroundColor, value: UIColor(.placeholderGray), range: range)
            return attributedText
        }
    }
    
}
