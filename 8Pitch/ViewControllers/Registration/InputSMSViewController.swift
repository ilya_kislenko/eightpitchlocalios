//
//  InputSMSViewController.swift
//  8Pitch
//
//  Created by 8pitch on 19.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input
import Remote

class InputSMSViewController: SMSViewController {
    
    override var confirmationFooterViewType : PublishSubject<InputSMSView.ViewType?>? {
        didSet {
            guard let typeAction = self.confirmationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem?.title = "navigationBar.faqButton".localized
        self.title = "registration.title".localized
        self.customTitle = "inputSMS.customTitle".localized
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CodeTextCell.nibForRegistration, forCellWithReuseIdentifier: CodeTextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = InputSMSView.reuseIdentifier
        layout.register(InputSMSView.nibForRegistration, forDecorationViewOfKind: InputSMSView.reuseIdentifier)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.resendButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
    override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupCreatePasswordViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func verifyToken() {
        super.verifyToken()
        guard let session = self.session, let model = try? self.model?.value() else {
            return
        }
        self.remote.verifyToken(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    self?.nextButtonAction?.onNext(.next)
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                    self?.nextButtonAction?.onNext(.error)
                }
            }).disposed(by: self.disposables)
    }
    
    override func resendCode() {
        super.resendCode()
        guard let session = self.session else {
            return
        }
        self.remote.resendLimitCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    return
                case .failure(let error):
                    let limitReached = error as? SendCodeError == SendCodeError.limit
                    self?.presentAlert(message: error.localizedDescription,
                                       completion: limitReached ?
                                        { [weak self] in self?.popToPreviousController(animated: true)} : {} )
                }
            }).disposed(by: self.disposables)
    }
    
}
