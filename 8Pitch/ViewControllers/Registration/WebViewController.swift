// BE71096123456769
//  WebViewController.swift
//  8Pitch
//
//  Created by 8pitch on 20.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: CustomTitleViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    var url: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.configuration.mediaTypesRequiringUserActionForPlayback = .all
        self.webView.configuration.allowsInlineMediaPlayback = true
        self.navigationItem.rightBarButtonItem = nil
        guard let url = URL(string: self.url) else {
            return
        }
        self.webView.load(URLRequest(url: url))
    }

}
