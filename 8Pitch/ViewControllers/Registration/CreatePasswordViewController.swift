//
//  PasswordViewController.swift
//  8Pitch
//
//  Created by 8pitch on 22.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class CreatePasswordViewController: AbstractCollectionViewController<Password> {
        
    override var model: BehaviorSubject<Password>? {
        self.session?.passwordProvider
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                        case .error: return // TODO: show error
                        case .loading:
                            guard let isValid = try? self.model?.value().isValid.value() else { return }
                            guard let registrationEnd = try? self.registrationEnd?.value()?.isValid.value() else { return }
                            self.validate(model: isValid, registrationEnd: registrationEnd)
                        case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    private func validate(model: Result<Bool, Error>, registrationEnd: Result<Bool, Error>) {
        switch (model, registrationEnd) {
            case (.failure(let error), _): fallthrough
            case (_, .failure(let error)):
                self.presentAlert(message: error.localizedDescription)
            case (.success(let result), .success(let endResult)):
                guard result, endResult else { return }
                self.startProgress()
                self.register()
            default: return
        }
    }
    
    var registrationEnd : BehaviorSubject<Model?>? {
        didSet {
            guard let model = self.registrationEnd else { return }
            model
                .compactMap { $0 }
                .subscribe(onNext: {
                    print("XXX : \($0)")
//                    guard model = $0 as? RegistrationEnd else { return }
                    
                    
                    
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "registration.title".localized
        self.customTitle = "createPassword.customTitle".localized
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = self.registrationStepsCount
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CreatePasswordCell.nibForRegistration, forCellWithReuseIdentifier: CreatePasswordCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = CreatePasswordView.reuseIdentifier
        layout.register(CreatePasswordView.nibForRegistration, forDecorationViewOfKind: CreatePasswordView.reuseIdentifier)
        layout.metadataProvider.subscribe(onNext: {
            self.registrationEnd = $0
        }).disposed(by: self.disposables)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: PasswordError.self, cellReuseIdentifier: CreatePasswordCell.reuseIdentifier),
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func nextButtonTapped() {
        _ = ViewControllerProvider.setupDoneViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func pushNextController(animated: Bool) {
        guard let nextViewController = self.nextController else {
            print("self.nextController == nil")
            return
        }
        self.navigationController?.pushViewController(nextViewController, animated: animated)
    }

}

extension CreatePasswordViewController {
    
    func register() {
        guard let session = self.session, let model = try? self.model?.value() else {
            return
        }
        session.registrationEndProvider.onNext(try? self.registrationEnd?.value() as? RegistrationEnd)
        self.remote.register(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
            switch response {
                case .success(_):
                    model.isValid.onNext(.success(true))
                    self?.setLanguage()
                case .failure(let error):
                    self?.stopProgress()
                    self?.presentAlert(message: error.localizedDescription)
            }
        }).disposed(by: self.disposables)
    }
    
    func setLanguage() {
        guard let session = self.session else { return }
        self.remote.updateLanguage(session) { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.nextButtonAction?.onNext(.next)
                return
            }
            self?.presentAlert(message: error.localizedDescription)
        }
    }
    
}

