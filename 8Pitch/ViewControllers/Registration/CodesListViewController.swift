//
//  CodesListViewController.swift
//  8Pitch
//
//  Created by 8pitch on 20.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import CoreTelephony

struct CodesListControllerDataSource {
    
    // MARK: Properties
    
    var list: [PickerData]
    var currentList: [PickerData]
    var numberOfItemsInSections : Int {
        currentList.count
    }
    
    init() {
        self.list = DataSource.countries
        self.currentList = DataSource.countries
    }
    
}

class CodesListViewController: CustomTitleViewController {
    
    // MARK: Properties
    
    private var dataSource = CodesListControllerDataSource()
    
    // MARK: UI Components
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.reuseIdentifier)
        self.navigationItem.rightBarButtonItem = nil
    }
    
}

extension CodesListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.dataSource.numberOfItemsInSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.reuseIdentifier) else {
            return UITableViewCell()
        }
        let country = dataSource.currentList[indexPath.row]
        if let flag = country.iso?.flag() {
        cell.textLabel?.text = "\(flag) +\(country.code) \(country.name)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let country = self.dataSource.currentList[indexPath.row]
        try? self.session?.loginProvider.value().phoneProvider.value()?.codeProvider.onNext(country)
        try? self.session?.phoneProvider.value().codeProvider.onNext(country)
        self.popToPreviousController(animated: true)
    }
    
}

// MARK: - UISearchResultsUpdating

extension CodesListViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.filterLanguages(query: self.searchBar.text)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.filterLanguages(query: self.searchBar.text)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterLanguages(query: self.searchBar.text)
    }
    
}

// MARK: Private

extension CodesListViewController {
    
    func filterLanguages(query: String?) {
        guard let query = query,
            !query.isEmpty else {
                self.dataSource.currentList = self.dataSource.list
                return
        }
        self.dataSource.currentList = self.dataSource.list.filter { $0.name.lowercased().contains(query.lowercased()) }
        tableView.reloadData()
    }
}
