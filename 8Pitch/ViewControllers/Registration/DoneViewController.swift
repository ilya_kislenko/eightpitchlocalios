//
//  DoneViewController.swift
//  8Pitch
//
//  Created by 8pitch on 26.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Remote
import RxSwift
import RxCocoa

class DoneViewController : CustomTitleViewController {
    
    var nextButtonAction : BehaviorSubject<NextState?>?
    private var isIndividualAccount: Bool {
        self.session?.registrationType == .some(.individualInvestor)
    }
    
    @IBOutlet weak var titleLabel: CustomSubtitleDarkLabel!
    @IBOutlet weak var subtitleLabel: HintDarkLargeLabel!
    
    @IBOutlet weak var approvalButton: RoundedButton!
    @IBOutlet weak var browseButton: BorderedButton!
    
    @objc private func close() {
        self.openMainApp()
    }
    
    @IBAction func getApprovalTapped(_ sender: UIButton) {
        self.checkAccountLevel()
    }
    @IBAction func browseProjectTapped(_ sender: Any) {
        self.openMainApp()
    }
    
    override func popToPreviousController(animated: Bool) {
        self.dismiss(animated: animated, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let accountType = self.isIndividualAccount ? "done.subtitle.private".localized : "done.subtitle.institutional".localized
        self.subtitleLabel.text = String(format: "done.subtitle".localized, accountType)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(close))
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getUser()
    }
    
    private func getUser() {
        guard let session = self.session, let model = try? self.session?.userProvider.value() else {
            return
        }
        self.startProgress()
        self.remote.user(session)
        .compactMap { $0 }
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { response in
            self.stopProgress()
            switch response {
                case .success(_):
                 if session.level == .second || session.level == .questionaryPassed {
                     self.approvalButton.isHidden = true
                 }
                return
                case .failure(let error):
                    self.presentAlert(message: error.localizedDescription)
                    self.nextButtonAction?.onNext(.error)
                return
                
            }
        }).disposed(by: self.disposables)
    }
    
}

