//
//  CompanyViewController.swift
//  8Pitch
//
//  Created by 8pitch on 26.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class CompanyViewController : AbstractCollectionViewController<Company> {
    
    override var model: BehaviorSubject<Company>? {
        self.session?.companyProvider
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: { [weak self] in
                    self?.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        try? self?.model?.value().validate()
                        guard let isValid = try? self?.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .success(let value) where value: self?.verifyModelAndNextButtonActionIfPossible()
                        case .failure(let error):
                            self?.presentAlert(message: error.localizedDescription)
                        default: return
                        }
                    case .next: self?.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    override func setupModel() {
        try? self.model?.value().valueProviders[\.companyTypeProvider] = self.companyTypeProvider
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "registration.title".localized
        self.customTitle = "createCompany.customTitle".localized
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = self.registrationStepsCount - 4
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CompanyNameCell.nibForRegistration, forCellWithReuseIdentifier: CompanyNameCell.reuseIdentifier)
        self.collectionView.register(CompanyTypeCell.nibForRegistration, forCellWithReuseIdentifier: CompanyTypeCell.reuseIdentifier)
        self.collectionView.register(RegistrationNumberCell.nibForRegistration, forCellWithReuseIdentifier: RegistrationNumberCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: CompanyError.self, cellReuseIdentifier: CompanyNameCell.reuseIdentifier),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: CompanyTypeError.self, cellReuseIdentifier: CompanyTypeCell.reuseIdentifier),
            CollectionViewModel(collectionView: self.collectionView, section: 2, model: model, errorType: CompanyNumberError.self, cellReuseIdentifier: RegistrationNumberCell.reuseIdentifier)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func nextButtonTapped() {
        _ = ViewControllerProvider.setupNameViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}

extension CompanyViewController {
    var companyTypeProvider : (() -> Void)? {
        return { [weak self] in
            /* to avoid  focus is redirected to the "Company Name" field after the user selects the type of company.
             STR 1 fill Company Name field 2 Tap on Company type field 3 Tap on Select button. (Expected result:
             The virtual keyboard is hidden)*/
            self?.view.endEditing(true)
            if let pickerVC = self?.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as? PickerViewController {
                guard let model = try? self?.model?.value() else {
                    return
                }
                pickerVC.selection.subscribe(onNext: {
                    model.companyTypeProvider.onNext($0)
                }).disposed(by: self?.disposables)
                pickerVC.dataSource = CompanyType.allCases.map { $0.rawValue.localized }
                self?.present(pickerVC, animated: true, completion: nil)
            }
        }
    }
}


