//
//  EmailViewController.swift
//  8Pitch
//
//  Created by 8pitch on 27.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class EmailViewController : AbstractCollectionViewController<Email> {
    
    override var model: BehaviorSubject<Email>? {
        self.session?.emailProvider
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        try? self.model?.value().validate()
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            if value {
                                self.startProgress()
                                self.verifyEmail()
                            }
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(EmailCell.nibForRegistration, forCellWithReuseIdentifier: EmailCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: EmailError.self, cellReuseIdentifier: EmailCell.reuseIdentifier),
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func resetModel() {
        super.resetModel()
    }
    
    func verifyEmail() {}
    
}
