//
//  PVINameViewController.swift
//  8Pitch
//
//  Created by 8pitch on 16.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class NameViewController : AbstractCollectionViewController<NameAndLastName> {
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        try? self.model?.value().validate()
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .success(let value) where value: self.verifyModelAndNextButtonActionIfPossible()
                        case .failure(let error):
                            self.presentAlert(message: error.localizedDescription)
                        default: return
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    override var model: BehaviorSubject<NameAndLastName>? {
        self.session?.nameAndLastNameProvider
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customTitle = "PVI.register.name.customTitle".localized
        self.title = "registration.title".localized
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = self.registrationStepsCount - 3
    }

    override func setupModel() {
        guard let model = try? self.model?.value() else {
            return
        }
        model.valueProviders[\.titleProvider] = self.titleProvider
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(TitleAndFirstNameCell.nibForRegistration, forCellWithReuseIdentifier: TitleAndFirstNameCell.reuseIdentifier)
        self.collectionView.register(LastNameCell.nibForRegistration, forCellWithReuseIdentifier: LastNameCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: FirstNameError.self, cellReuseIdentifier: TitleAndFirstNameCell.reuseIdentifier),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: LastNameError.self, cellReuseIdentifier: LastNameCell.reuseIdentifier)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupEmailViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}

extension NameViewController {
    
    var titleProvider : (() -> Void)? {
        return { [weak self] in
            if let pickerVC = self?.registration.instantiateViewController(withIdentifier: "PickerViewController") as? PickerViewController {
                guard let model = try? self?.model?.value() else {
                    return
                }
                pickerVC.selection.subscribe(onNext: {
                    model.titleProvider.onNext($0)
                }).disposed(by: self?.disposables)
                pickerVC.dataSource = Title.allCases.map { $0.rawValue.localized }
                self?.present(pickerVC, animated: true, completion: nil)
            }
        }
    }
}
