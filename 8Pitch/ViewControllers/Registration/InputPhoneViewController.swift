//
//  InputPhoneViewController.swift
//  8Pitch
//
//  Created by 8pitch on 17.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class InputPhoneViewController: AbstractCollectionViewController<Phone> {
    
    // MARK: Overrides
    
    override var model: BehaviorSubject<Phone>? {
        self.session?.phoneProvider
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                        case .error: return // TODO: show error
                        case .loading:
                            try? self.model?.value().validate()
                            guard let isValid = try? self.model?.value().isValid.value() else { return }
                            switch isValid {
                                case .failure(let error): self.presentAlert(message: error.localizedDescription)
                                case .success(let value):
                                    guard value else { return }
                                    self.startProgress()
                                    self.sendRequests()
                            }
                        case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var navigationFooterViewType: PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.info)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "registration.title".localized
        self.customTitle = "inputPhone.customeTitle".localized
    }
    
    override func setupProgressView() {
        super.setupProgressView()
        self.progressView.currentIndex = self.registrationStepsCount - 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.reloadData()
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(InputPhoneCell.nibForRegistration, forCellWithReuseIdentifier: InputPhoneCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: PhoneError.self, cellReuseIdentifier: InputPhoneCell.reuseIdentifier),
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        if let cell = cell as? InputPhoneCell {
        cell.delegate = self
        }
        return cell
    }
    
    // MARK: Actions
    
    // TODO: add valueProvider for codeButton event via model
    @objc func codeButtonTapped() {
        let _ = ViewControllerProvider.setupCodesListViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupInputSMSViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}

extension InputPhoneViewController {
    
    func sendRequests() {
        self.phoneCheck {
            if $0 {
                self.sendCode()
            }
        }
    }
    
    func phoneCheck(completion: @escaping (Bool) -> Void) {
        guard let session = self.session, let model = try? self.model?.value() else {
            return
        }
        self.remote.phoneCheck(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { response in
                switch response {
                    case .success(_):
                        if session.isPhoneValid {
                            model.isValid.onNext(.success(true))
                        } else {
                            model.isValid.onNext(.failure(PhoneError.phoneDuplicate))
                            self.nextButtonAction?.onNext(.error)
                        }
                        completion(session.isPhoneValid)
                    case .failure(let error):
                        self.presentAlert(message: error.localizedDescription)
                        model.isValid.onNext(.failure(PhoneError.phoneDuplicate))
                        self.nextButtonAction?.onNext(.error)
                    completion(false)
                }
            }).disposed(by: self.disposables)
    }
    
    func sendCode() {
        guard let model = try? self.model?.value(), let session = self.session else {
            return
        }
        self.remote.sendLimitCode(session)
        .compactMap { $0 }
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { [weak self] response in
            self?.stopProgress()
            switch response {
                case .success(_):
                    model.isValid.onNext(.success(true))
                    self?.nextButtonAction?.onNext(.next)
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                    self?.nextButtonAction?.onNext(.error)
            }
        }).disposed(by: self.disposables)
    }
    
}

extension InputPhoneViewController: CodeListDelegate {
    
    func openCodeList() {
        let _ = ViewControllerProvider.setupCodesListViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}
