//
//  ViewControllerProvider.swift
//  8Pitch
//
//  Created by 8pitch on 15.07.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class ViewControllerProvider {
    
    enum AuthFlow {
        case registration
        case login
        case questionnaire
    }
    
    private static var main : UIStoryboard {
        UIStoryboard(name: "Main", bundle: nil)
    }
    
    private static var registration : UIStoryboard {
        UIStoryboard(name: "Registration", bundle: nil)
    }
    
    private static var login : UIStoryboard {
        UIStoryboard(name: "Login", bundle: nil)
    }
    
    private static var questionnaire : UIStoryboard {
        UIStoryboard(name: "Questionnaire", bundle: nil)
    }
    
    private static var kyc : UIStoryboard {
        UIStoryboard(name: "KYC", bundle: nil)
    }
    
    private static var webID : UIStoryboard {
        UIStoryboard(name: "WebID", bundle: nil)
    }
    
    private static var investment : UIStoryboard {
        UIStoryboard(name: "Investment", bundle: nil)
    }
    
    private static var projects : UIStoryboard {
        UIStoryboard(name: "Projects", bundle: nil)
    }
    
    private static var dashboard : UIStoryboard {
        UIStoryboard(name: "Dashboard", bundle: nil)
    }
    
    private static var outOfDelivery: UIStoryboard {
        UIStoryboard(name: "OutOfDelivery", bundle: nil)
    }
    
    private static var payments: UIStoryboard {
        UIStoryboard(name: "Payments", bundle: nil)
    }
    
    private static var profile: UIStoryboard {
        UIStoryboard(name: "Profile", bundle: nil)
    }
    
    private static var notification: UIStoryboard {
        UIStoryboard(name: "Notification", bundle: nil)
    }
    
    private static var transfer: UIStoryboard {
        UIStoryboard(name: "Transfer", bundle: nil)
    }
    
    private static var settings: UIStoryboard {
        UIStoryboard(name: "Settings", bundle: nil)
    }
    
    private static var deletion: UIStoryboard {
        UIStoryboard(name: "Deletion", bundle: nil)
    }
    
    private static var twoFA: UIStoryboard {
        UIStoryboard(name: "TwoFA", bundle: nil)
    }
    
    private static var paymentsHistory: UIStoryboard {
        UIStoryboard(name: "PaymentsHistory", bundle: nil)
    }
    
    private static var changeEmail: UIStoryboard {
        UIStoryboard(name: "ChangeEmail", bundle: nil)
    }
    
    private static var security: UIStoryboard {
        UIStoryboard(name: "Security", bundle: nil)
    }
    
    private static var approveVoting: UIStoryboard {
        UIStoryboard(name: "VotingApprove", bundle: nil)
    }
    
    private static var votingsList: UIStoryboard {
        UIStoryboard(name: "VotingsList", bundle: nil)
    }
    
    private static var passVoting: UIStoryboard {
        UIStoryboard(name: "PassVoting", bundle: nil)
    }
    
    private static var stream: UIStoryboard {
        UIStoryboard(name: "Stream", bundle: nil)
    }
    
    private static var trustee: UIStoryboard {
        UIStoryboard(name: "Trustee", bundle: nil)
    }
    
    static func setupStartViewController() -> UINavigationController? {
        self.main.instantiateInitialViewController() as? UINavigationController
    }
    
    static func startOverAgain() {
        let appDelegate = UIApplication.shared.delegate
        appDelegate?.window??.rootViewController = ViewControllerProvider.setupStartViewController()
        appDelegate?.window??.makeKeyAndVisible()
    }
    
    static func setupNameViewController(rootViewController: GenericViewController) -> NameViewController? {
        guard let nameViewController = self.registration.instantiateViewController(withIdentifier: "NameViewController") as? NameViewController else {
            return nil
        }
        nameViewController.previousController = rootViewController
        rootViewController.nextController = nameViewController
        nameViewController.session = rootViewController.session
        return nameViewController
    }
    
    static func setupSplashViewController(rootViewController: GenericViewController) -> SplashViewController? {
        guard let splashViewController = rootViewController.storyboard?.instantiateViewController(withIdentifier: "SplashViewController") as? SplashViewController else {
            return nil
        }
        splashViewController.previousController = rootViewController
        rootViewController.nextController = splashViewController
        return splashViewController
    }
    
    static func setupTypesViewController(rootViewController: GenericViewController) -> TypesViewController? {
        guard let typesViewController = self.registration.instantiateViewController(withIdentifier: "TypesViewController") as? TypesViewController  else {
            return nil
        }
        typesViewController.previousController = rootViewController
        rootViewController.nextController = typesViewController
        return typesViewController
    }
    
    static func createLoginViewController(rootViewController: GenericViewController) -> LoginViewController? {
        guard let viewController = self.login.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
            return nil
        }
        viewController.session = Session(registrationType: .none)
        rootViewController.previousController = viewController
        rootViewController.navigationController?.viewControllers.append(viewController)
        return viewController
    }
    
    static func createStartViewController(rootViewController: GenericViewController) -> StartViewController? {
        guard let viewController = self.main.instantiateViewController(withIdentifier: "StartViewController") as? StartViewController else {
            return nil
        }
        rootViewController.previousController = viewController
        rootViewController.navigationController?.viewControllers.append(viewController)
        return viewController
    }
    
    static func setupLoginViewController(rootViewController: GenericViewController) -> LoginViewController? {
        guard let loginViewController = self.login.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
            return nil
        }
        loginViewController.previousController = rootViewController
        rootViewController.nextController = loginViewController
        return loginViewController
    }
    
    static func setupInfoViewController(rootViewController: GenericViewController) -> InfoViewController? {
        guard let infoViewController = self.registration.instantiateViewController(withIdentifier: "InfoViewController") as? InfoViewController else {
            return nil
        }
        infoViewController.previousController = rootViewController
        rootViewController.nextController = infoViewController
        infoViewController.session = rootViewController.session
        return infoViewController
    }
    
    static func setupInputPhoneViewController(rootViewController: GenericViewController) -> InputPhoneViewController? {
        guard let inputViewController = self.registration.instantiateViewController(withIdentifier: "InputPhoneViewController") as? InputPhoneViewController else {
            return nil
        }
        inputViewController.previousController = rootViewController
        rootViewController.nextController = inputViewController
        inputViewController.session = rootViewController.session
        return inputViewController
    }
    
    static func setupEmailViewController(rootViewController: GenericViewController) -> EnterEmailViewController? {
        guard let emailViewController = self.registration.instantiateViewController(withIdentifier: "EnterEmailViewController") as? EnterEmailViewController else {
            return nil
        }
        emailViewController.previousController = rootViewController
        rootViewController.nextController = emailViewController
        emailViewController.session = rootViewController.session
        return emailViewController
    }
    
    static func setupWebViewController(rootViewController: GenericViewController) -> WebViewController? {
        guard let webViewController = self.registration.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController else {
            return nil
        }
        webViewController.previousController = rootViewController
        rootViewController.nextController = webViewController
        return webViewController
    }
    
    static func setupCodesListViewController(rootViewController: GenericViewController) -> CodesListViewController? {
        guard let listViewController = self.registration.instantiateViewController(withIdentifier: "CodesListViewController") as? CodesListViewController else {
            return nil
        }
        listViewController.previousController = rootViewController
        rootViewController.nextController = listViewController
        listViewController.session = rootViewController.session
        return listViewController
    }
    
    static func setupInputSMSViewController(rootViewController: GenericViewController) -> InputSMSViewController? {
        guard let inputViewController = self.registration.instantiateViewController(withIdentifier: "InputSMSViewController") as? InputSMSViewController else {
            return nil
        }
        inputViewController.previousController = rootViewController
        rootViewController.nextController = inputViewController
        inputViewController.session = rootViewController.session
        return inputViewController
    }
    
    static func setupCreatePasswordViewController(rootViewController: GenericViewController) -> CreatePasswordViewController? {
        guard let passwordViewController = self.registration.instantiateViewController(withIdentifier: "CreatePasswordViewController") as? CreatePasswordViewController else {
            return nil
        }
        passwordViewController.previousController = rootViewController
        rootViewController.nextController = passwordViewController
        passwordViewController.session = rootViewController.session
        return passwordViewController
    }
    
    static func setupConfirmSMSViewController(rootViewController: GenericViewController) -> ConfirmSMSViewController? {
        guard let confirmViewController = self.login.instantiateViewController(withIdentifier: "ConfirmSMSViewController") as? ConfirmSMSViewController else {
            return nil
        }
        confirmViewController.previousController = rootViewController
        rootViewController.nextController = confirmViewController
        confirmViewController.session = rootViewController.session
        return confirmViewController
    }
    
    static func setupConfirmTwoFAViewController(rootViewController: GenericViewController) -> ConfirmTwoFAViewController? {
        guard let confirmViewController = self.login.instantiateViewController(withIdentifier: "ConfirmTwoFAViewController") as? ConfirmTwoFAViewController else {
            return nil
        }
        confirmViewController.previousController = rootViewController
        rootViewController.nextController = confirmViewController
        confirmViewController.session = rootViewController.session
        return confirmViewController
    }
    
    static func setupDoneViewController(rootViewController: GenericViewController) -> DoneViewController? {
        guard let doneViewController = self.registration.instantiateViewController(withIdentifier: "DoneViewController") as? DoneViewController else {
            return nil
        }
        doneViewController.previousController = nil
        rootViewController.nextController = doneViewController
        doneViewController.session = rootViewController.session
        return doneViewController
    }
    
    static func setupCompanyViewController(rootViewController: GenericViewController) -> CompanyViewController? {
        guard let companyViewController = self.registration.instantiateViewController(withIdentifier: "CompanyViewController") as? CompanyViewController else {
            return nil
        }
        companyViewController.previousController = rootViewController
        rootViewController.nextController = companyViewController
        companyViewController.session = rootViewController.session
        return companyViewController
    }
    
    static func setupQuestionnaireExplanationViewController(rootViewController: GenericViewController) -> QuestionnaireExplanationViewController? {
        guard let questionnaireExplanationViewController = self.questionnaire.instantiateViewController(withIdentifier: "QuestionnaireExplanationViewController") as? QuestionnaireExplanationViewController else {
            return nil
        }
        questionnaireExplanationViewController.previousController = rootViewController
        rootViewController.nextController = questionnaireExplanationViewController
        questionnaireExplanationViewController.session = rootViewController.session
        questionnaireExplanationViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return questionnaireExplanationViewController
    }
    
    static func setupInvestorQualificationViewController(rootViewController: GenericViewController) -> InvestorQualificationViewController? {
        guard let investorQualificationViewController = self.questionnaire.instantiateViewController(withIdentifier: "InvestorQualificationViewController") as? InvestorQualificationViewController else {
            return nil
        }
        investorQualificationViewController.previousController = rootViewController
        rootViewController.nextController = investorQualificationViewController
        investorQualificationViewController.session = rootViewController.session
        investorQualificationViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return investorQualificationViewController
    }
    
    static func setupAssetExperienceViewController(rootViewController: GenericViewController) -> AssetExperienceViewController? {
        guard let assetExperienceViewController = self.questionnaire.instantiateViewController(withIdentifier: "AssetExperienceViewController") as? AssetExperienceViewController else {
            return nil
        }
        assetExperienceViewController.previousController = rootViewController
        assetExperienceViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        rootViewController.nextController = assetExperienceViewController
        assetExperienceViewController.session = rootViewController.session
        return assetExperienceViewController
    }
    
    static func setupAssetClassesViewController(rootViewController: GenericViewController) -> AssetClassesViewController? {
        guard let assetClassesViewController = self.questionnaire.instantiateViewController(withIdentifier: "AssetClassesViewController") as? AssetClassesViewController else {
            return nil
        }
        assetClassesViewController.previousController = rootViewController
        rootViewController.nextController = assetClassesViewController
        assetClassesViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        assetClassesViewController.session = rootViewController.session
        return assetClassesViewController
    }
    
    static func setupInvestingExperienceViewController(rootViewController: GenericViewController) -> InvestingExperienceViewController? {
        guard let investingExperienceViewController = self.questionnaire.instantiateViewController(withIdentifier: "InvestingExperienceViewController") as? InvestingExperienceViewController else {
            return nil
        }
        investingExperienceViewController.previousController = rootViewController
        rootViewController.nextController = investingExperienceViewController
        investingExperienceViewController.session = rootViewController.session
        investingExperienceViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return investingExperienceViewController
    }
    
    static func setupExperienceTermsViewController(rootViewController: GenericViewController) -> ExperienceDurationViewController? {
        guard let experienceTermsViewController = self.questionnaire.instantiateViewController(withIdentifier: "ExperienceTermsViewController") as? ExperienceDurationViewController else {
            return nil
        }
        experienceTermsViewController.previousController = rootViewController
        rootViewController.nextController = experienceTermsViewController
        experienceTermsViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        experienceTermsViewController.session = rootViewController.session
        return experienceTermsViewController
    }
    
    static func setupInvestmentAmountViewController(rootViewController: GenericViewController) -> InvestmentAmountViewController? {
        guard let investmentAmountViewController = self.questionnaire.instantiateViewController(withIdentifier: "InvestmentAmountViewController") as? InvestmentAmountViewController else {
            return nil
        }
        investmentAmountViewController.previousController = rootViewController
        rootViewController.nextController = investmentAmountViewController
        investmentAmountViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        investmentAmountViewController.session = rootViewController.session
        return investmentAmountViewController
    }
    
    static func setupInvestingPeriodicityViewController(rootViewController: GenericViewController) -> InvestingFrequencyViewController? {
        guard let investingPeriodicityViewController = self.questionnaire.instantiateViewController(withIdentifier: "InvestingPeriodicityViewController") as? InvestingFrequencyViewController else {
            return nil
        }
        investingPeriodicityViewController.previousController = rootViewController
        rootViewController.nextController = investingPeriodicityViewController
        investingPeriodicityViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        investingPeriodicityViewController.session = rootViewController.session
        return investingPeriodicityViewController
    }
    
    static func setupVirtualCurrencyViewController(rootViewController: GenericViewController) -> VirtualCurrencyViewController? {
        guard let virtualCurrencyViewController = self.questionnaire.instantiateViewController(withIdentifier: "VirtualCurrencyViewController") as? VirtualCurrencyViewController else {
            return nil
        }
        virtualCurrencyViewController.previousController = rootViewController
        rootViewController.nextController = virtualCurrencyViewController
        virtualCurrencyViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        virtualCurrencyViewController.session = rootViewController.session
        return virtualCurrencyViewController
    }
    
    static func setupVirtualCurrencyShareViewController(rootViewController: GenericViewController) -> VirtualCurrencyShareViewController? {
        guard let virtualCurrencyShareViewController = self.questionnaire.instantiateViewController(withIdentifier: "VirtualCurrencyShareViewController") as? VirtualCurrencyShareViewController else {
            return nil
        }
        virtualCurrencyShareViewController.previousController = rootViewController
        rootViewController.nextController = virtualCurrencyShareViewController
        virtualCurrencyShareViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        virtualCurrencyShareViewController.session = rootViewController.session
        return virtualCurrencyShareViewController
    }
    
    static func setupCapitalMarketExperienceViewController(rootViewController: GenericViewController) -> CapitalMarketExperienceViewController? {
        guard let capitalMarketExperienceViewController = self.questionnaire.instantiateViewController(withIdentifier: "CapitalMarketExperienceViewController") as? CapitalMarketExperienceViewController else {
            return nil
        }
        capitalMarketExperienceViewController.previousController = rootViewController
        rootViewController.nextController = capitalMarketExperienceViewController
        capitalMarketExperienceViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        capitalMarketExperienceViewController.session = rootViewController.session
        return capitalMarketExperienceViewController
    }
    
    static func setupHowDidGetExperienceViewController(rootViewController: GenericViewController) -> HowDidGetExperienceViewController? {
        guard let howDidGetExperienceViewController = self.questionnaire.instantiateViewController(withIdentifier: "HowDidGetExperienceViewController") as? HowDidGetExperienceViewController else {
            return nil
        }
        howDidGetExperienceViewController.previousController = rootViewController
        rootViewController.nextController = howDidGetExperienceViewController
        howDidGetExperienceViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        howDidGetExperienceViewController.session = rootViewController.session
        return howDidGetExperienceViewController
    }
    
    static func setupQuestionnaireSuccessViewController(rootViewController: GenericViewController) -> QuestionnaireSuccessViewController? {
        guard let questionnaireSuccessViewController = self.questionnaire.instantiateViewController(withIdentifier: "QuestionnaireSuccessViewController") as? QuestionnaireSuccessViewController else {
            return nil
        }
        questionnaireSuccessViewController.previousController = rootViewController
        rootViewController.nextController = questionnaireSuccessViewController
        questionnaireSuccessViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        questionnaireSuccessViewController.session = rootViewController.session
        return questionnaireSuccessViewController
    }
    
    static func setupBirthViewController(rootViewController: GenericViewController) -> BirthViewController? {
        guard let birthViewController = self.kyc.instantiateViewController(withIdentifier: "BirthViewController") as? BirthViewController else {
            return nil
        }
        birthViewController.previousController = rootViewController
        rootViewController.nextController = birthViewController
        birthViewController.session = rootViewController.session
        birthViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return birthViewController
    }
    
    static func setupVerifyEmailViewController(rootViewController: GenericViewController) -> VerifyEmailViewController? {
        guard let verifyViewController = self.kyc.instantiateViewController(withIdentifier: "VerifyEmailViewController") as? VerifyEmailViewController else {
            return nil
        }
        verifyViewController.previousController = rootViewController
        rootViewController.nextController = verifyViewController
        verifyViewController.session = rootViewController.session
        verifyViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return verifyViewController
    }
    
    static func setupPersonalViewController(rootViewController: GenericViewController) -> PersonalViewController? {
        guard let personalViewController = self.kyc.instantiateViewController(withIdentifier: "PersonalViewController") as? PersonalViewController else {
            return nil
        }
        personalViewController.previousController = rootViewController
        rootViewController.nextController = personalViewController
        personalViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        personalViewController.session = rootViewController.session
        return personalViewController
    }
    
    static func setupEmailVerifiedViewController(rootViewController: GenericViewController) -> EmailVerifiedViewController? {
        guard let verifiedViewController = self.kyc.instantiateViewController(withIdentifier: "EmailVerifiedViewController") as? EmailVerifiedViewController else {
            return nil
        }
        rootViewController.nextController = verifiedViewController
        verifiedViewController.session = rootViewController.session
        verifiedViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return verifiedViewController
    }
    
    static func setupDetailsViewController(rootViewController: GenericViewController) -> DetailsViewController? {
        guard let detailsViewController = self.kyc.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController else {
            return nil
        }
        detailsViewController.previousController = rootViewController
        rootViewController.nextController = detailsViewController
        detailsViewController.session = rootViewController.session
        detailsViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return detailsViewController
    }
    
    static func setupLocalityViewController(rootViewController: GenericViewController) -> LocalityViewController? {
        guard let localityViewController = self.kyc.instantiateViewController(withIdentifier: "LocalityViewController") as? LocalityViewController else {
            return nil
        }
        localityViewController.previousController = rootViewController
        rootViewController.nextController = localityViewController
        localityViewController.session = rootViewController.session
        localityViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return localityViewController
    }
    
    static func setupAddressViewController(rootViewController: GenericViewController) -> AddressViewController? {
        guard let addressViewController = self.kyc.instantiateViewController(withIdentifier: "AddressViewController") as? AddressViewController else {
            return nil
        }
        addressViewController.previousController = rootViewController
        rootViewController.nextController = addressViewController
        addressViewController.session = rootViewController.session
        addressViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return addressViewController
    }
    
    static func setupFinancialViewController(rootViewController: GenericViewController) -> FinancialViewController? {
        guard let financialViewController = self.kyc.instantiateViewController(withIdentifier: "FinancialViewController") as? FinancialViewController else {
            return nil
        }
        financialViewController.previousController = rootViewController
        rootViewController.nextController = financialViewController
        financialViewController.session = rootViewController.session
        financialViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return financialViewController
    }
    
    static func setupSummarizeViewController(rootViewController: GenericViewController) -> SummarizeViewController? {
        guard let summarizeViewController = self.kyc.instantiateViewController(withIdentifier: "SummarizeViewController") as? SummarizeViewController else {
            return nil
        }
        summarizeViewController.previousController = rootViewController
        rootViewController.nextController = summarizeViewController
        summarizeViewController.session = rootViewController.session
        summarizeViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return summarizeViewController
    }
    
    static func setupEditViewController(rootViewController: GenericViewController) -> EditViewController? {
        guard let editViewController = self.kyc.instantiateViewController(withIdentifier: "EditViewController") as? EditViewController else {
            return nil
        }
        editViewController.previousController = rootViewController
        rootViewController.nextController = editViewController
        editViewController.session = rootViewController.session
        editViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return editViewController
    }
    
    static func setupWebIDViewController(rootViewController: GenericViewController) -> WebIDViewController? {
        // TODO: move identifier into class, take it from there
        guard let webIDViewController = self.webID.instantiateViewController(withIdentifier: "WebIDViewController") as? WebIDViewController else {
            return nil
        }
        webIDViewController.previousController = rootViewController
        rootViewController.nextController = webIDViewController
        webIDViewController.session = rootViewController.session
        webIDViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return webIDViewController
    }
    
    static func setupWebIDDoneViewController(rootViewController: GenericViewController) -> WebIDDoneViewController? {
        // TODO: move identifier into class, take it from there
        guard let webIDDoneViewController = self.webID.instantiateViewController(withIdentifier: "WebIDDoneViewController") as? WebIDDoneViewController else {
            return nil
        }
        webIDDoneViewController.previousController = rootViewController
        rootViewController.nextController = webIDDoneViewController
        webIDDoneViewController.session = rootViewController.session
        webIDDoneViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return webIDDoneViewController
    }
    
    static func setupWebIDFailViewController(rootViewController: GenericViewController) -> WebIDFailViewController? {
        // TODO: move identifier into class, take it from there
        guard let webIDFailViewController = self.webID.instantiateViewController(withIdentifier: "WebIDFailViewController") as? WebIDFailViewController else {
            return nil
        }
        webIDFailViewController.previousController = rootViewController
        rootViewController.nextController = webIDFailViewController
        webIDFailViewController.session = rootViewController.session
        webIDFailViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return webIDFailViewController
    }
    
    static func setupKYCSuccessViewController(rootViewController: GenericViewController) -> KYCSuccessViewController? {
        guard let KYCSuccessViewController = self.webID.instantiateViewController(withIdentifier: "KYCSuccessViewController") as? KYCSuccessViewController else {
            return nil
        }
        KYCSuccessViewController.previousController = rootViewController
        rootViewController.nextController = KYCSuccessViewController
        KYCSuccessViewController.session = rootViewController.session
        KYCSuccessViewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return KYCSuccessViewController
    }
    
    static func setupPrecontractualViewController(rootViewController: GenericViewController) -> PrecontractualViewController? {
        guard let viewController = self.projects.instantiateViewController(withIdentifier: "PrecontractualViewController") as? PrecontractualViewController else {
            return nil
        }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupProjectsViewController(rootViewController: TabBarController) -> ProjectsViewController? {
        guard let projectsViewController = self.projects.instantiateViewController(withIdentifier: "ProjectsViewController") as? ProjectsViewController else {
            return nil
        }
        projectsViewController.session = rootViewController.session
        return projectsViewController
    }
    
    static func setupProjectViewController(rootViewController: TabBarController) -> ProjectsTabViewController? {
        guard let projectsViewController = self.projects.instantiateViewController(withIdentifier: "ProjectsTabViewController") as? ProjectsTabViewController else {
            return nil
        }
        projectsViewController.session = rootViewController.session
        return projectsViewController
    }
    
    static func setupProjectsTabViewController(rootViewController: GenericViewController, type: ProjectsTabViewModel.TabType) -> ProjectsTabViewController? {
        guard let projectsTabViewController = self.projects.instantiateViewController(withIdentifier: "ProjectsTabViewController") as? ProjectsTabViewController else {
            return nil
        }
        projectsTabViewController.session = rootViewController.session
        projectsTabViewController.type = type
        return projectsTabViewController
    }
    
    static func setupProjectDetailsViewController(rootViewController: GenericViewController) -> ProjectDetailsViewController? {
        guard let detailsViewController = self.projects.instantiateViewController(withIdentifier: "ProjectDetailsViewController") as? ProjectDetailsViewController else {
            return nil
        }
        detailsViewController.previousController = rootViewController
        rootViewController.nextController = detailsViewController
        detailsViewController.session = rootViewController.session
        return detailsViewController
    }
    
    static func setupProjectDetailsConstructorViewController(rootViewController: GenericViewController) -> ProjectDetailsConstructorViewController? {
        guard let detailsViewController = self.projects.instantiateViewController(withIdentifier: "ProjectDetailsConstructorViewController") as? ProjectDetailsConstructorViewController else {
            return nil
        }
        detailsViewController.previousController = rootViewController
        rootViewController.nextController = detailsViewController
        detailsViewController.session = rootViewController.session
        return detailsViewController
    }
    
    static func setupGetDocumentsViewController(rootViewController: GenericViewController) -> GetDocumentsViewController? {
        guard let getDocumentsViewController = self.investment.instantiateViewController(withIdentifier: "GetDocumentsViewController") as? GetDocumentsViewController else {
            return nil
        }
        getDocumentsViewController.previousController = rootViewController
        rootViewController.nextController = getDocumentsViewController
        getDocumentsViewController.session = rootViewController.session
        return getDocumentsViewController
    }
    
    static func setupOutOfDeliveryController(rootViewController: TabBarController) -> GenericViewController? {
        guard let outOfDeliveryController = self.outOfDelivery.instantiateViewController(withIdentifier: "OutOfDelivery") as? GenericViewController else {
            return nil
        }
        return outOfDeliveryController
    }
    
    static func setupInvestmentAmountCurrencyViewController(rootViewController: GenericViewController) -> InvestmentAmountCurrencyViewController? {
        guard let investmentAmountCurrencyViewController = self.investment.instantiateViewController(withIdentifier: "InvestmentAmountCurrencyViewController") as? InvestmentAmountCurrencyViewController else {
            return nil
        }
        investmentAmountCurrencyViewController.hidesBottomBarWhenPushed = true
        investmentAmountCurrencyViewController.previousController = rootViewController
        rootViewController.nextController = investmentAmountCurrencyViewController
        investmentAmountCurrencyViewController.session = rootViewController.session
        return investmentAmountCurrencyViewController
    }
    
    static func setupSentOnEmailViewController(rootViewController: GenericViewController) -> SentOnEmailViewController? {
        guard let sentOnEmailViewController = self.investment.instantiateViewController(withIdentifier: "SentOnEmailViewController") as? SentOnEmailViewController else {
            return nil
        }
        sentOnEmailViewController.previousController = rootViewController
        rootViewController.nextController = sentOnEmailViewController
        sentOnEmailViewController.session = rootViewController.session
        return sentOnEmailViewController
    }
    
    static func setupInvestmentConfirmationViewController(rootViewController: GenericViewController) -> InvestmentConfirmationViewController? {
        guard let investmentConfirmationViewController = self.investment.instantiateViewController(withIdentifier: "InvestmentConfirmationViewController") as? InvestmentConfirmationViewController else {
            return nil
        }
        investmentConfirmationViewController.previousController = rootViewController
        rootViewController.nextController = investmentConfirmationViewController
        investmentConfirmationViewController.session = rootViewController.session
        return investmentConfirmationViewController
    }
    
    static func setupTabBarController(rootViewController: GenericViewController) -> TabBarController {
        let tabBarController = TabBarController(rootViewController.session ?? Session(registrationType: .none))
        return tabBarController
    }
    
    static func setupDirectDownloadedViewController(rootViewController: GenericViewController) -> DirectDownloadingViewController? {
        guard let viewController = self.investment.instantiateViewController(withIdentifier: "DirectDownloadingViewController") as? DirectDownloadingViewController else {
            return nil
        }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupProfileViewController(rootViewController: TabBarController) -> ProfileViewController? {
        guard let viewController = self.profile.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController else {
            return nil
        }
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupUnfinishedInvestmentViewController(rootViewController: GenericViewController) -> UnfinishedInvestmentViewController? {
        guard let viewController = self.investment.instantiateViewController(withIdentifier: "UnfinishedInvestmentViewController") as? UnfinishedInvestmentViewController else {
            return nil
        }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupProcessedInvestmentViewController(rootViewController: GenericViewController) -> ProcessedInvestmentViewController? {
        guard let viewController = self.investment.instantiateViewController(withIdentifier: "ProcessedInvestmentViewController") as? ProcessedInvestmentViewController else {
            return nil }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupPaymentMethodsViewController(rootViewController: GenericViewController) -> PaymentMethodsViewController? {
        guard let viewController = self.payments.instantiateViewController(withIdentifier: "PaymentMethodsViewController") as? PaymentMethodsViewController else {
            return nil
        }
        viewController.hidesBottomBarWhenPushed = true
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupBankTransferViewController(rootViewController: GenericViewController) -> BankTransferViewController? {
        guard let viewController = self.transfer.instantiateViewController(withIdentifier: "BankTransferViewController") as? BankTransferViewController else {
            return nil
        }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupOnlinePaymentViewController(rootViewController: GenericViewController) -> OnlinePaymentViewController? {
        guard let viewController = self.payments.instantiateViewController(withIdentifier: "OnlinePaymentViewController") as? OnlinePaymentViewController else {
            return nil }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupOnlinePaymentWebViewController(rootViewController: GenericViewController) -> OnlinePaymentWebViewController? {
        guard let viewController = self.payments.instantiateViewController(withIdentifier: "OnlinePaymentWebViewController") as? OnlinePaymentWebViewController else {
            return nil }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupDirectDebitViewController(rootViewController: GenericViewController) -> DirectDebitViewController? {
        guard let viewController = self.transfer.instantiateViewController(withIdentifier: "DirectDebitViewController") as? DirectDebitViewController else { return nil }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupPaymentProviderErrorViewController(rootViewController: GenericViewController) -> PaymentProviderErrorViewController? {
        guard let viewController = self.payments.instantiateViewController(withIdentifier: "PaymentProviderErrorViewController") as? PaymentProviderErrorViewController else {
            return nil }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupPaymentProviderSuccessViewController(rootViewController: GenericViewController) -> PaymentProviderSuccessViewController? {
        guard let viewController = self.payments.instantiateViewController(withIdentifier: "PaymentProviderSuccessViewController") as? PaymentProviderSuccessViewController else {
            return nil }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupSettingsViewController(rootViewController: GenericViewController) -> SettingsViewController? {
        guard let viewController = self.settings.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {
            return nil }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupPersonalProfileViewController(rootViewController: GenericViewController) -> PersonalProfileViewController? {
        guard let viewController = self.profile.instantiateViewController(withIdentifier: "PersonalProfileViewController") as? PersonalProfileViewController else {
            return nil }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupEditProfileViewController(rootViewController: GenericViewController) -> EditProfileViewController? {
        guard let viewController = self.profile.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController else {
            return nil }
        viewController.hidesBottomBarWhenPushed = true
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupDeletionViewController(rootViewController: GenericViewController) -> DeletionViewController? {
        guard let viewController = self.deletion.instantiateViewController(withIdentifier: "DeletionViewController") as? DeletionViewController else {
            return nil }
        viewController.hidesBottomBarWhenPushed = true
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupDeletedViewController(rootViewController: GenericViewController) -> DeletedViewController? {
        guard let viewController = self.deletion.instantiateViewController(withIdentifier: "DeletedViewController") as? DeletedViewController else {
            return nil }
        viewController.previousController = rootViewController.previousController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupDeletionConfirmationViewController(rootViewController: GenericViewController) -> DeletionConfirmationViewController? {
        guard let viewController = self.deletion.instantiateViewController(withIdentifier: "DeletionConfirmationViewController") as? DeletionConfirmationViewController else {
            return nil }
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupNotificationListViewController(rootViewController: TabBarController) -> NotificationListViewController? {
        guard let notificationsViewController = self.notification.instantiateViewController(withIdentifier: "NotificationListViewController") as? NotificationListViewController else { return nil }
        notificationsViewController.session = rootViewController.session
        return notificationsViewController
    }
    
    static func setupProjectDetailsRootViewController(rootViewController: GenericViewController) -> ProjectDetailsRootViewController? {
        guard let viewController = self.projects.instantiateViewController(withIdentifier: "ProjectDetailsRootViewController") as? ProjectDetailsRootViewController else {
            return nil }
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupSettingsInfoViewController(rootViewController: GenericViewController) -> SettingsInfoViewController? {
        guard let viewController = self.settings.instantiateViewController(withIdentifier: "SettingsInfoViewController") as? SettingsInfoViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupAddSecurityViewController(rootViewController: GenericViewController) -> AddSecurityViewController? {
        guard let viewController = self.twoFA.instantiateViewController(withIdentifier: "AddSecurityViewController") as? AddSecurityViewController else {
            return nil
        }
        viewController.cameFromProfile = rootViewController.cameFromProfile
        viewController.previousController = rootViewController
        rootViewController.nextController = viewController
        viewController.session = rootViewController.session
        viewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return viewController
    }
    
    static func setupDashboardViewController(rootViewController: TabBarController) -> DashboardViewController? {
        guard let viewController = self.dashboard.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController else {
            return nil }
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupHistoryTabViewController(rootViewController: GenericViewController, tabType: HistoryTabViewController.HistoryTabType) -> HistoryTabViewController? {
        guard let viewController = self.paymentsHistory.instantiateViewController(withIdentifier: "HistoryTabViewController") as? HistoryTabViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.tabType = tabType
        return viewController
    }
    
    static func setupHistoryViewController(rootViewController: GenericViewController) -> HistoryViewController? {
        guard let viewController = self.paymentsHistory.instantiateViewController(withIdentifier: "HistoryViewController") as? HistoryViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupChangeEmailViewController(rootViewController: GenericViewController) -> ChangeEmailViewController? {
        guard let viewController = self.changeEmail.instantiateViewController(withIdentifier: "ChangeEmailViewController") as? ChangeEmailViewController else {
            return nil }
        viewController.hidesBottomBarWhenPushed = true
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupTransactionDetailsViewController(rootViewController: GenericViewController, transaction: UserTransaction) -> TransactionDetailsViewController? {
        guard let viewController = self.paymentsHistory.instantiateViewController(withIdentifier: "PaymentDetailsViewController") as? TransactionDetailsViewController else {
            return nil }
        viewController.hidesBottomBarWhenPushed = true
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.transaction = transaction
        return viewController
    }
    static func setupVerifyChangeEmailViewController(rootViewController: GenericViewController) -> VerifyChangeEmailViewController? {
        guard let viewController = self.changeEmail.instantiateViewController(withIdentifier: "VerifyChangeEmailViewController") as? VerifyChangeEmailViewController else {
            return nil }
        viewController.hidesBottomBarWhenPushed = true
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupSuccessEmailChangeViewController(rootViewController: GenericViewController) -> SuccessEmailChangeViewController? {
        guard let viewController = self.changeEmail.instantiateViewController(withIdentifier: "SuccessEmailChangeViewController") as? SuccessEmailChangeViewController else {
            return nil }
        viewController.hidesBottomBarWhenPushed = true
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupSecurityViewController(rootViewController: GenericViewController) -> SecurityViewController? {
        guard let viewController = self.security.instantiateViewController(withIdentifier: "SecurityViewController") as? SecurityViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupChooseSecurityMethodViewController(rootViewController: GenericViewController) -> ChooseSecurityMethodViewController? {
        guard let viewController = self.twoFA.instantiateViewController(withIdentifier: "ChooseSecurityMethodViewController") as? ChooseSecurityMethodViewController else {
            return nil }
        viewController.cameFromProfile = rootViewController.cameFromProfile
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return viewController
    }
    
    static func setupAddExtraSecurityViewController(rootViewController: GenericViewController) -> AddExtraSecurityViewController? {
        guard let viewController = self.twoFA.instantiateViewController(withIdentifier: "AddExtraSecurityViewController") as? AddExtraSecurityViewController else {
            return nil }
        viewController.cameFromProfile = rootViewController.cameFromProfile
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return viewController
    }
    
    static func setupAddTwoFACodeViewController(rootViewController: GenericViewController) -> GetTwoFACodeViewController? {
        guard let viewController = self.twoFA.instantiateViewController(withIdentifier: "GetTwoFACodeViewController") as? GetTwoFACodeViewController else {
            return nil }
        viewController.cameFromProfile = rootViewController.cameFromProfile
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return viewController
    }
    
    static func setupEnterTwoFACodeViewController(rootViewController: GenericViewController) -> EnterTwoFACodeViewController? {
        guard let viewController = self.twoFA.instantiateViewController(withIdentifier: "EnterTwoFACodeViewController") as? EnterTwoFACodeViewController else {
            return nil }
        viewController.cameFromProfile = rootViewController.cameFromProfile
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return viewController
    }
    
    static func setupSuccessTwoFAViewController(rootViewController: GenericViewController) -> SuccessTwoFAViewController? {
        guard let viewController = self.twoFA.instantiateViewController(withIdentifier: "SuccessTwoFAViewController") as? SuccessTwoFAViewController else {
            return nil }
        viewController.cameFromProfile = rootViewController.cameFromProfile
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return viewController
    }
    
    static func setupConfirmTwoFASMSViewController(rootViewController: GenericViewController) -> ConfirmTwoFASMSViewController? {
        guard let viewController = self.twoFA.instantiateViewController(withIdentifier: "ConfirmTwoFASMSViewController") as? ConfirmTwoFASMSViewController else {
            return nil }
        viewController.cameFromProfile = rootViewController.cameFromProfile
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.kycInitialController = rootViewController.kycInitialController ?? rootViewController
        return viewController
    }
    
    static func setupTwoFAInfoViewController(rootViewController: GenericViewController) -> TwoFAInfoViewController? {
        guard let viewController = self.login.instantiateViewController(withIdentifier: "TwoFAInfoViewController") as? TwoFAInfoViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        return viewController
    }
    
    static func setupDashboardInitiatorProjecctDetailsViewController(rootViewController: GenericViewController) -> DashboardInitiatorProjecctDetailsViewController? {
        guard let viewController = self.dashboard.instantiateViewController(withIdentifier: "DashboardInitiatorProjecctDetailsViewController") as? DashboardInitiatorProjecctDetailsViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupConfirmPasswordViewController(rootViewController: GenericViewController) -> ConfirmPasswordViewController? {
        guard let viewController = self.security.instantiateViewController(withIdentifier: "ConfirmPasswordViewController") as? ConfirmPasswordViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.session?.createChangePasswordProviders()
        return viewController
    }
    
    static func setupSetNewPasswordViewController(rootViewController: GenericViewController) -> SetNewPasswordViewController? {
        guard let viewController = self.security.instantiateViewController(withIdentifier: "SetNewPasswordViewController") as? SetNewPasswordViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupPasswordChangedViewController(rootViewController: GenericViewController) -> PasswordChangedViewController? {
        guard let viewController = self.security.instantiateViewController(withIdentifier: "PasswordChangedViewController") as? PasswordChangedViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController.previousController?.previousController
        return viewController
    }
    
    static func setupForgotPasswordViewController(rootViewController: GenericViewController) -> ForgotPasswordViewController? {
        guard let viewController = self.login.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as? ForgotPasswordViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupConfirmForgotPasswordViewController(rootViewController: GenericViewController) -> ConfirmForgotPasswordViewController? {
        guard let viewController = self.login.instantiateViewController(withIdentifier: "ConfirmForgotPasswordViewController") as? ConfirmForgotPasswordViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupChangePhoneViewController(rootViewController: GenericViewController) -> ChangePhoneViewController? {
        guard let viewController = self.profile.instantiateViewController(withIdentifier: "ChangePhoneViewController") as? ChangePhoneViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupConfirmChangeNumberViewController(rootViewController: GenericViewController) -> ConfirmChangeNumberViewController? {
        guard let viewController = self.profile.instantiateViewController(withIdentifier: "ConfirmChangeNumberViewController") as? ConfirmChangeNumberViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupSuccessPhoneChangeViewController(rootViewController: GenericViewController) -> SuccessPhoneChangeViewController? {
        guard let viewController = self.profile.instantiateViewController(withIdentifier: "SuccessPhoneChangeViewController") as? SuccessPhoneChangeViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupQuickLoginViewController(rootViewController: GenericViewController) -> QuickLoginViewController? {
        guard let viewController = self.security.instantiateViewController(withIdentifier: "QuickLoginViewController") as? QuickLoginViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupCreatePinViewController(rootViewController: GenericViewController, withBiometry: Bool = false, fromLogin: Bool = false) -> CreatePinViewController? {
        guard let viewController = self.security.instantiateViewController(withIdentifier: "CreatePinViewController") as? CreatePinViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.hidesBottomBarWhenPushed = true
        viewController.withBiometry = withBiometry
        viewController.fromLogin = fromLogin
        return viewController
    }
    
    static func setupRepeatPinViewController(rootViewController: GenericViewController, withBiometry: Bool = false) -> RepeatPinViewController? {
        guard let viewController = self.security.instantiateViewController(withIdentifier: "RepeatPinViewController") as? RepeatPinViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.hidesBottomBarWhenPushed = true
        viewController.withBiometry = withBiometry
        viewController.fromLogin = (rootViewController as? PinViewController)?.fromLogin ?? false
        return viewController
    }
    
    static func setupCheckPinViewController(rootViewController: GenericViewController) -> CheckPinViewController? {
        guard let viewController = self.security.instantiateViewController(withIdentifier: "CheckPinViewController") as? CheckPinViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.hidesBottomBarWhenPushed = true
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupSuccessQuickLoginViewController(rootViewController: GenericViewController, for type: SuccessQuickLoginViewController.LoginType) -> SuccessQuickLoginViewController? {
        guard let viewController = self.security.instantiateViewController(withIdentifier: "SuccessQuickLoginViewController") as? SuccessQuickLoginViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.hidesBottomBarWhenPushed = true
        viewController.session = rootViewController.session
        viewController.type = type
        viewController.fromLogin = (rootViewController as? PinViewController)?.fromLogin ?? false
        return viewController
    }
    
    static func setupSetupQuickLoginViewController(rootViewController: GenericViewController) -> SetupQuickLoginViewController? {
        guard let viewController = self.login.instantiateViewController(withIdentifier: "SetupQuickLoginViewController") as? SetupQuickLoginViewController else {
            return nil }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupLoginPinViewController() -> UINavigationController? {
        guard let viewController = self.main.instantiateViewController(withIdentifier: "NavigationPinViewController") as? UINavigationController else {
            return nil
        }
        let appDelegate = UIApplication.shared.delegate
        appDelegate?.window??.rootViewController = viewController
        appDelegate?.window??.makeKeyAndVisible()
        return viewController
    }
    
    static func setupGermanTaxViewController(rootViewController: GenericViewController) -> GermanTaxViewController? {
        guard let viewController = self.investment.instantiateViewController(withIdentifier: "GermanTaxViewController") as? GermanTaxViewController else {
            return nil
        }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupProjectFilterSummaryViewController(rootViewController: GenericViewController) -> ProjectFilterSummaryViewController? {
        guard let viewController = self.projects.instantiateViewController(withIdentifier: "ProjectFilterSummaryViewController") as? ProjectFilterSummaryViewController else {
            return nil
        }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        return viewController
    }
    
    static func setupProjectFilterViewController(rootViewController: GenericViewController, type: ProjectFilterViewModel.TabType) -> ProjectFilterViewController? {
        guard let viewController = self.projects.instantiateViewController(withIdentifier: "ProjectFilterViewController") as? ProjectFilterViewController else {
            return nil
        }
        rootViewController.nextController = viewController
        viewController.previousController = rootViewController
        viewController.session = rootViewController.session
        viewController.type = type
        return viewController
    }
    
    static func setupProjectsSearchViewController(rootViewController: GenericViewController) -> ProjectsTabViewController? {
        guard let projectsTabViewController = self.projects.instantiateViewController(withIdentifier: "ProjectsTabViewController") as? ProjectsTabViewController else {
            return nil
        }
        rootViewController.nextController = projectsTabViewController
        projectsTabViewController.previousController = rootViewController
        projectsTabViewController.session = rootViewController.session
        projectsTabViewController.type = .searched
        return projectsTabViewController
    }
    
    static func setupVotingsForProjectsViewController(rootViewController: GenericViewController) -> VotingsForProjectsViewController? {
        guard let votingsForProjectsVC = self.votingsList.instantiateViewController(withIdentifier: "VotingsForProjectsViewController") as? VotingsForProjectsViewController else {
            return nil
        }
        rootViewController.nextController = votingsForProjectsVC
        votingsForProjectsVC.previousController = rootViewController
        votingsForProjectsVC.session = rootViewController.session
        return votingsForProjectsVC
    }
    
    static func setupProjectVotingsViewController(rootViewController: GenericViewController) -> ProjectVotingsViewController? {
        guard let projectVotingsVC = self.votingsList.instantiateViewController(withIdentifier: "ProjectVotingsViewController") as? ProjectVotingsViewController else {
            return nil
        }
        rootViewController.nextController = projectVotingsVC
        projectVotingsVC.previousController = rootViewController
        projectVotingsVC.session = rootViewController.session
        let presenter = ProjectVotingsPresenter(view: projectVotingsVC)
        projectVotingsVC.presenter = presenter
        return projectVotingsVC
    }
    
    static func setupApproveVotingViewController(rootViewController: GenericViewController) -> ApproveVotingViewController? {
        guard let approveVotingVC = self.approveVoting.instantiateViewController(withIdentifier: "ApproveVotingViewController") as? ApproveVotingViewController else {
            return nil
        }
        rootViewController.nextController = approveVotingVC
        approveVotingVC.previousController = rootViewController
        approveVotingVC.session = rootViewController.session
        let presenter = ApproveVotingPresenter(view: approveVotingVC)
        approveVotingVC.presenter = presenter
        return approveVotingVC
    }
    
    static func setupConfirmRejectViewController(rootViewController: GenericViewController) -> ConfirmRejectViewController? {
        guard let vc = self.approveVoting.instantiateViewController(withIdentifier: "ConfirmRejectViewController") as? ConfirmRejectViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupConfirmApproveViewController(rootViewController: GenericViewController) -> ConfirmApproveViewController? {
        guard let vc = self.approveVoting.instantiateViewController(withIdentifier: "ConfirmApproveViewController") as? ConfirmApproveViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupMockViewController(rootViewController: GenericViewController) -> MockViewController? {
        guard let vc = self.approveVoting.instantiateViewController(withIdentifier: "MockViewController") as? MockViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupVotingApprovedViewController(rootViewController: GenericViewController) -> VotingApprovedViewController? {
        guard let vc = self.approveVoting.instantiateViewController(withIdentifier: "VotingApprovedViewController") as? VotingApprovedViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupPassVotingInfoViewController(rootViewController: GenericViewController) -> PassVotingInfoViewController? {
        guard let vc = self.passVoting.instantiateViewController(withIdentifier: "PassVotingInfoViewController") as? PassVotingInfoViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupPassVotingViewController(rootViewController: GenericViewController) -> PassVotingViewController? {
        guard let vc = self.passVoting.instantiateViewController(withIdentifier: "PassVotingViewController") as? PassVotingViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        let presenter = PassVotingPresenter(view: vc)
        vc.presenter = presenter
        return vc
    }
    
    static func setupConfirmPassVotingViewController(rootViewController: GenericViewController) -> ConfirmPassVotingViewController? {
        guard let vc = self.passVoting.instantiateViewController(withIdentifier: "ConfirmPassVotingViewController") as? ConfirmPassVotingViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupVotingPassedViewController(rootViewController: GenericViewController) -> VotingPassedViewController? {
        guard let vc = self.passVoting.instantiateViewController(withIdentifier: "VotingPassedViewController") as? VotingPassedViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupYourAnswersViewController(rootViewController: GenericViewController) -> YourAnswersViewController? {
        guard let vc = self.passVoting.instantiateViewController(withIdentifier: "YourAnswersViewController") as? YourAnswersViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        let presenter = YourAnswersPresenter(view: vc)
        vc.presenter = presenter
        return vc
    }
    
    static func setupVotingResultsViewController(rootViewController: GenericViewController) -> VotingResultsViewController? {
        guard let vc = self.passVoting.instantiateViewController(withIdentifier: "VotingResultsViewController") as? VotingResultsViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        let presenter = VotingResultsPresenter(view: vc)
        vc.presenter = presenter
        return vc
    }
    
    static func setupVotingPlugViewController(rootViewController: GenericViewController) -> VotingPlugViewController? {
        guard let vc = self.votingsList.instantiateViewController(withIdentifier: "VotingPlugViewController") as? VotingPlugViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupTrusteeErrorViewController(rootViewController: GenericViewController) -> TrusteeErrorViewController? {
        guard let vc = self.trustee.instantiateViewController(withIdentifier: "TrusteeErrorViewController") as? TrusteeErrorViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupStreamErrorViewController(rootViewController: GenericViewController) -> StreamErrorViewController? {
        guard let vc = self.stream.instantiateViewController(withIdentifier: "StreamErrorViewController") as? StreamErrorViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupStreamViewController(rootViewController: GenericViewController) -> StreamViewController? {
        guard let vc = self.stream.instantiateViewController(withIdentifier: "StreamViewController") as? StreamViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    static func setupTrusteeDecisionViewController(rootViewController: GenericViewController) -> TrusteeDecisionViewController? {
        guard let vc = self.trustee.instantiateViewController(withIdentifier: "TrusteeDecisionViewController") as? TrusteeDecisionViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupConfirmTrusteeDecisionViewController(rootViewController: GenericViewController) -> ConfirmTrusteeDecisionViewController? {
        guard let vc = self.trustee.instantiateViewController(withIdentifier: "ConfirmTrusteeDecisionViewController") as? ConfirmTrusteeDecisionViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupPassTrusteeViewController(rootViewController: GenericViewController) -> PassTrusteeViewController? {
        guard let vc = self.trustee.instantiateViewController(withIdentifier: "PassTrusteeViewController") as? PassTrusteeViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
    
    static func setupVotingOnlyViewController(rootViewController: GenericViewController) -> VotingOnlyViewController? {
        guard let vc = self.login.instantiateViewController(withIdentifier: "VotingOnlyViewController") as? VotingOnlyViewController else {
            return nil
        }
        rootViewController.nextController = vc
        vc.previousController = rootViewController
        vc.session = rootViewController.session
        return vc
    }
}


