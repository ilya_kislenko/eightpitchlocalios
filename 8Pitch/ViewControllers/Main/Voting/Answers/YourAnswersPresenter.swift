//
//  YourAnswersPresenter.swift
//  8Pitch
//
//  Created by 8pitch on 3/16/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input
import Remote

class YourAnswersPresenter: YourAnswersPresenterProtocol {
    weak var view: YourAnswersViewProtocol?
    private var remote: Remote = Remote()
    private var session: Session? {
        self.view?.session
    }
    var voting: Voting?
    
    var answers: [Answer] {
        self.session?.votingProvider.answers ?? []
    }
    
    var project: VotingProjectInfo? {
        self.session?.votingProvider.currentProject
    }
    
    required init(view: YourAnswersViewProtocol) {
        self.view = view
        self.loadAnswers()
    }
    
    func loadFile() {
        guard let session = self.session,
              let id = self.session?.votingProvider.currentVoting?.id else {
            return
        }
        self.view?.startProgress()
        self.remote.votingAnswersFile(session, id, { [weak self] result in
            switch result {
            case .success(let data):
                self?.saveFile(id: String(id), data: data)
            case .failure(let error):
                self?.view?.completeRequest(with: error, url: nil)
            }
        })
    }
    
    private func saveFile(id: String, data: Data) {
        guard let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
              let name = self.voting?.type else { return }
        let source = documentsPath.appendingPathComponent(String(format: "voting.answers.fileName".localized, name))
        do {
            try? FileManager.default.removeItem(at: source)
            try data.write(to: source, options: .atomic)
        } catch {
            self.view?.completeRequest(with: error, url: nil)
            return
        }
        self.view?.completeRequest(with: nil, url: source)
    }
    
    private func loadAnswers() {
        guard let session = self.session,
              let id = self.session?.votingProvider.currentVoting?.id else {
            return
        }
        self.view?.startProgress()
        self.remote.answers(session, id: id, completionHandler: { [weak self] error in
            self?.setVoting()
            self?.session?.votingProvider.currentProject = self?.session?.votingProvider.currentVoting?.projectInfo
            self?.view?.completeRequest(with: error, url: nil)
        })
    }
    
    private func setVoting() {
        let initialVoting = self.session?.votingProvider.currentVoting
        initialVoting?.questions.forEach { question in
            let answeredQuestion = self.answers.first(where: { answer in answer.question.id == question.id })
            answeredQuestion?.answers.forEach { answer in
                question.options.first(where: { $0.id == answer.id})?.selected = true
            }
        }
        self.voting = initialVoting
    }
}
