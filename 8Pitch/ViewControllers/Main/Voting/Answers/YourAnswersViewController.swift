//
//  YourAnswersViewController.swift
//  8Pitch
//
//  Created by 8pitch on 3/16/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class YourAnswersViewController: GenericVotingViewController, YourAnswersViewProtocol, LoadFileDelegate {
    
    var presenter: YourAnswersPresenterProtocol!
    @IBOutlet weak var periodButton: BorderedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "voting.answers.title".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.load), style: .plain, target: self, action: #selector(self.loadFile))
        self.tableView.register(DownloadPDFFooter.nibForRegistration, forHeaderFooterViewReuseIdentifier: DownloadPDFFooter.reuseIdentifier)
        self.headers[Constants.numberOfSections - 1] = "voting.answers.header".localized
        self.footerHeight = 150
    }
    
    override func headerTapped(_ sender: UITapGestureRecognizer?) {
        super.headerTapped(sender)
        guard let section = sender?.view?.tag else { return }
        expandedSections[section] = !expandedSections[section]
        tableView.reloadSections([section], with: .automatic)
    }
    
    @objc func loadFile() {
        self.presenter.loadFile()
    }
    
    func completeRequest(with error: Error?, url: URL?) {
        self.stopProgress()
        if error == nil, url == nil {
            self.setupUI()
        } else if error == nil, let url = url {
            self.openFile(with: url)
        } else if let error = error {
            self.presentAlert(message: error.localizedDescription)
        }
    }
    
    private func setupUI() {
        guard let voting = self.presenter.voting,
              let project = self.presenter.project else { return }
        self.customTitle = voting.type
        self.setupCustomTitle()
        self.periodButton.layer.borderColor = UIColor(.red).cgColor
        let title = NSMutableAttributedString(string: "voting.answers.customTitle".localized, attributes: [.foregroundColor: UIColor(.gray), .font: UIFont.barlow.semibold.selectableBarSize])
        let date = NSAttributedString(string: voting.conductedDate, attributes: [.foregroundColor: UIColor(.red), .font: UIFont.barlow.semibold.selectableBarSize])
        title.append(date)
        self.periodButton.setAttributedTitle(title, for: .normal)
        self.votingData = VotingData(name: "\(project.legalFormType ?? "") \(project.companyName ?? "")", date: voting.votingPeriod, description: voting.description, agenda: voting.agenda, questions: voting.questions, editable: false)
        self.tableView.reloadData()
    }
    
    private func openFile(with url: URL) {
        let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        self.present(activityViewController, animated: true)
    }
}

extension YourAnswersViewController {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == Constants.numberOfSections - 1 {
            let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: DownloadPDFFooter.reuseIdentifier) as? DownloadPDFFooter
            view?.delegate = self
            view?.contentView.backgroundColor = .white
            return view
        } else {
            return nil
        }
    }
}
