//
//  GenericVotingViewController.swift
//  8Pitch
//
//  Created by 8pitch on 3/16/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

struct VotingData {
    var name: String
    var date: String
    var description: String
    var agenda: String
    var questions: [Question]
    var editable: Bool
}

class GenericVotingViewController: CustomTitleViewController {
    enum Constants {
        static let headers = ["voting.passing.header.main".localized,
                              "voting.approval.header.agenda".localized,
                              "voting.approval.header.questions".localized]
        static let numberOfSections: Int = 3
        static let headerHeight: CGFloat = 48
        static let footerHeight: CGFloat = 230
        static let trusteeHeaderHeight = CGFloat(185)
    }
    
    var votingData: VotingData?
    var headers = Constants.headers
    var footerHeight = Constants.footerHeight
    var expandedSections = [false, false, false, false]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
    }
    
    @objc func headerTapped(_ sender: UITapGestureRecognizer?) {}
}

private extension GenericVotingViewController {
    func registerCells() {
        self.tableView.register(ExpandableHeader.nibForRegistration, forHeaderFooterViewReuseIdentifier: ExpandableHeader.reuseIdentifier)
        self.tableView.register(PassVotingFooter.nibForRegistration, forHeaderFooterViewReuseIdentifier: PassVotingFooter.reuseIdentifier)
        self.tableView.register(ApproveVotingAgendaCell.nibForRegistration, forCellReuseIdentifier: ApproveVotingAgendaCell.reuseIdentifier)
        self.tableView.register(VotingQuestionsContainerCell.nibForRegistration, forCellReuseIdentifier: VotingQuestionsContainerCell.reuseIdentifier)
        self.tableView.register(VotingInfoCell.nibForRegistration, forCellReuseIdentifier: VotingInfoCell.reuseIdentifier)
        self.tableView.register(ApproveVotingMainCell.nibForRegistration, forCellReuseIdentifier: ApproveVotingMainCell.reuseIdentifier)
    }
}

extension GenericVotingViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        Constants.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if expandedSections[section] {
            return section == 0 ? 3 : 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingInfoCell.reuseIdentifier, for: indexPath) as! VotingInfoCell
                cell.setup(with: self.votingData?.name, type: .name)
                return cell
            case 1:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingInfoCell.reuseIdentifier, for: indexPath) as! VotingInfoCell
                cell.setup(with: self.votingData?.date, type: .date)
                return cell
            case 2:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: ApproveVotingMainCell.reuseIdentifier, for: indexPath) as! ApproveVotingMainCell
                cell.setup(description: self.votingData?.description)
                return cell
            default:
                let cell = UITableViewCell()
                return cell
            }
        case 1:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: ApproveVotingAgendaCell.reuseIdentifier, for: indexPath) as! ApproveVotingAgendaCell
            cell.setText(self.votingData?.agenda)
            return cell
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingQuestionsContainerCell.reuseIdentifier, for: indexPath) as! VotingQuestionsContainerCell
            cell.setup(with: self.votingData?.questions ?? [], editable: self.votingData?.editable ?? false)
            cell.delegate = self as? PassVotingViewProtocol
            return cell
        default:
            let cell = UITableViewCell()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: ExpandableHeader.reuseIdentifier) as? ExpandableHeader
        let text = self.headers[section]
        view?.setText(text, expanded: self.expandedSections[section])
        view?.contentView.backgroundColor = .white
        let tapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                          action: #selector(headerTapped(_:)))
        view?.tag = section
        view?.addGestureRecognizer(tapGestureRecognizer)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        Constants.headerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        section == Constants.numberOfSections - 1 ? self.footerHeight : 0
    }
}
