//
//  YourAnswersProtocol.swift
//  8Pitch
//
//  Created by 8pitch on 3/16/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input

protocol YourAnswersPresenterProtocol {
    var voting: Voting? { get set }
    var project: VotingProjectInfo? { get }
    func loadFile()
}

protocol YourAnswersViewProtocol: class {
    var session: Session? { get set }
    func startProgress()
    func completeRequest(with error: Error?, url: URL?)
    func loadFile()
}

protocol LoadFileDelegate: class {
    func loadFile()
}

