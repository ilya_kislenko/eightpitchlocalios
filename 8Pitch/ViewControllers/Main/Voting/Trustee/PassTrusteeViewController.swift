//
//  PassTrusteeViewController.swift
//  8Pitch
//
//  Created by 8pitch on 4/6/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class PassTrusteeViewController: GenericVotingViewController {
    
    private lazy var viewModel : PassTrusteeViewModel = {
        PassTrusteeViewModel(session: session)
    }()
    
    @IBOutlet weak var hintLabel: DualTextLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "voting.trustee.title".localized.uppercased()
        self.navigationItem.rightBarButtonItem = nil
        self.customTitle = "voting.trustee.customTitle".localized
        self.hintLabel.text = "voting.trustee.hint".localized
        self.expandedSections = [true, true, true, true]
        self.footerHeight = Constants.trusteeHeaderHeight
        if let voting = self.viewModel.voting,
           let project = self.viewModel.project {
            self.votingData = VotingData(name: "\(project.legalFormType ?? "") \(project.companyName ?? "")", date: voting.votingPeriod, description: voting.description, agenda: voting.agenda, questions: voting.questions, editable: true)
        }
    }
}

extension PassTrusteeViewController {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == Constants.numberOfSections - 1 {
            let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: PassVotingFooter.reuseIdentifier) as? PassVotingFooter
            view?.delegate = self
            view?.voting = self.viewModel.voting
            view?.setupForTrustee()
            view?.contentView.backgroundColor = .white
            return view
        } else {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = super.tableView(tableView, viewForHeaderInSection: section) as? ExpandableHeader
        view?.setupForTrustee()
        return view
    }
}

extension PassTrusteeViewController: PassVotingViewProtocol {
    func completeRequest(with error: Error?) {}
    
    func submit() {
        self.startProgress()
        self.viewModel.sendCodeRequest(completionHandler: { [weak self] response in
            self?.stopProgress()
            switch response {
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
            case .success(_):
                self?.confirmSubmit()
            }
        })
    }
    
    func confirmSubmit() {
        self.session?.votingProvider.trusteeDecision = .withObligation
        _ = ViewControllerProvider.setupConfirmTrusteeDecisionViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func reloadFooter() {
        self.tableView.reloadSections(IndexSet(integer: Constants.numberOfSections - 1), with: .fade)
    }
}
