//
//  ConfirmTrusteeDecisionViewController.swift
//  8Pitch
//
//  Created by 8pitch on 4/5/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import RxSwift

class ConfirmTrusteeDecisionViewController: SMSViewController {
    
    @IBOutlet weak var infoLabel: InfoCurrencyLabel!
    
    override var confirmationFooterViewType : PublishSubject<InputSMSView.ViewType?>? {
        didSet {
            guard let typeAction = self.confirmationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.title = "voting.trustee.title".localized.uppercased()
        self.customTitle = "voting.approval.confirm.customTitle".localized
        self.infoLabel.text = "voting.approval.confirm.info".localized
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CodeTextCell.nibForRegistration, forCellWithReuseIdentifier: CodeTextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = InputSMSView.reuseIdentifier
        layout.register(InputSMSView.nibForRegistration, forDecorationViewOfKind: InputSMSView.reuseIdentifier)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.resendButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
    override func verifyToken() {
        super.verifyToken()
        guard let session = self.session, let id = session.votingProvider.currentVoting?.id else {
            return
        }
        self.remote.trusteeDecision(session, id: id, completionHandler: { [weak self] error in
            self?.stopProgress()
            if let error = error {
                self?.handleTrusteeError(error)
            } else {
                self?.complited()
            }
        })
    }
    
    override func resendCode() {
        super.resendCode()
        guard let session = self.session else {
            return
        }
        self.remote.resendCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    return
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                }
            }).disposed(by: self.disposables)
    }
    
    private func complited() {
        _ = ViewControllerProvider.setupVotingsForProjectsViewController(rootViewController: self)
        self.nextController?.previousController = navigationController?.viewControllers.first(where: { $0 is NotificationListViewController }) as? GenericViewController
        self.pushNextController(animated: true)
    }
    
    private func handleTrusteeError(_ error: Error) {
        if self.session?.votingProvider.currentVoting?.startDate ?? Date() < Date() {
            self.presentAlert(message: "voting.trustee.error.started".localized, completion: {
                self.complited()
            })
        } else {
            self.presentAlert(message: error.localizedDescription)
        }
    }
}
