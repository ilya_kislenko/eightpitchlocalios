//
//  TrusteeErrorViewController.swift
//  8Pitch
//
//  Created by 8pitch on 4/5/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class TrusteeErrorViewController: CustomTitleViewController {
    
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var textLabel: InfoCurrencyLabel!
    @IBOutlet weak var button: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = "voting.trustee.error.term.title".localized
        self.button.setTitle("voting.trustee.error.repeat.button".localized.uppercased(), for: .normal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(close))
        self.navigationItem.rightBarButtonItem = nil
        self.setup()
    }
    
    func setup() {
        guard let voting = self.session?.votingProvider.currentVoting else { return }
        self.picture.image = UIImage(.errorRepeate)
        switch voting.investorTrusteeDecision {
        case .voteByHimself:
            self.textLabel.text = "voting.trustee.error.repeat.yourself.text".localized
        case .withObligation:
            self.textLabel.text = "voting.trustee.error.repeat.transfer.text".localized
        case .withoutObligation:
            self.textLabel.text = "voting.trustee.error.repeat.trustee.text".localized
        case .none:
            self.textLabel.text = String(format: "voting.trustee.error.term.text".localized, voting.type ?? "")
            self.picture.image = UIImage(.errorTerm)
        }
    }
    
    @IBAction func openVotingList(_ sender: UIButton) {
        _ = ViewControllerProvider.setupVotingsForProjectsViewController(rootViewController: self)
        self.nextController?.previousController = self.previousController
        self.pushNextController(animated: true)
    }
    
    @objc private func close() {
        self.popToPreviousController(animated: true)
    }
}
