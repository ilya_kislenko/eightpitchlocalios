//
//  PassTrusteeViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 4/6/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import RxSwift
import Input

class PassTrusteeViewModel: GenericViewModel {
    
    var project: VotingProjectInfo? {
        self.session.votingProvider.currentProject
    }
    
    var voting: Voting? {
        self.session.votingProvider.currentVoting
    }
    
    func sendCodeRequest(completionHandler: @escaping ResponseClosure) {
        self.remote.sendCode(self.session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { response in
                completionHandler(response)
            }).disposed(by: self.disposables)
    }
}
