//
//  TrusteeDecisionViewController.swift
//  8Pitch
//
//  Created by 8pitch on 4/5/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

protocol TrusteeDecisionDelegate: class {
    func noInstructionSelected()
    func instructionSelected()
    func vote()
}

class TrusteeDecisionViewController: CustomTitleViewController {
    
    private lazy var viewModel : TrusteeDecisionViewModel = {
        TrusteeDecisionViewModel(session: session)
    }()
    
    @IBOutlet weak var hintLabel: DualTextLabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.title = "voting.trustee.title".localized.uppercased()
        self.customTitle = "voting.trustee.customTitle".localized
        self.hintLabel.text = "voting.trustee.hint".localized
        self.registerCells()
        self.session?.votingProvider.currentProject = self.session?.votingProvider.currentVoting?.projectInfo
    }
}

private extension TrusteeDecisionViewController {
    enum Constants {
        static let headers = [
            "voting.approval.header.settings".localized,
            "voting.approval.header.agenda".localized,
            "voting.approval.header.questions".localized
        ]
        static let headerHeight: CGFloat = 38
        static let footerHeight: CGFloat = 312
        static let estimatedRowHeight: CGFloat = 200
        static let numberOfSections: Int = 4
    }
    
    func registerCells() {
        self.tableView.register(ApproveVotingHeader.nibForRegistration, forHeaderFooterViewReuseIdentifier: ApproveVotingHeader.reuseIdentifier)
        self.tableView.register(ApproveVotingMainCell.nibForRegistration, forCellReuseIdentifier: ApproveVotingMainCell.reuseIdentifier)
        self.tableView.register(VotingTrusteeCell.nibForRegistration, forCellReuseIdentifier: VotingTrusteeCell.reuseIdentifier)
        self.tableView.register(ApproveVotingAgendaCell.nibForRegistration, forCellReuseIdentifier: ApproveVotingAgendaCell.reuseIdentifier)
        self.tableView.register(VotingQuestionsContainerCell.nibForRegistration, forCellReuseIdentifier: VotingQuestionsContainerCell.reuseIdentifier)
        self.tableView.register(TrusteeDecisionFooter.nibForRegistration, forHeaderFooterViewReuseIdentifier: TrusteeDecisionFooter.reuseIdentifier)
        self.tableView.register(VotingInfoCell.nibForRegistration, forCellReuseIdentifier: VotingInfoCell.reuseIdentifier)
        self.tableView.register(VotingSurveyCell.nibForRegistration, forCellReuseIdentifier: VotingSurveyCell.reuseIdentifier)
        self.tableView.register(VotingStreamCell.nibForRegistration, forCellReuseIdentifier: VotingStreamCell.reuseIdentifier)
    }
    
    func confirm() {
        _ = ViewControllerProvider.setupConfirmTrusteeDecisionViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func sendCode() {
        self.startProgress()
        self.viewModel.sendCodeRequest(completionHandler: { [weak self] response in
            self?.stopProgress()
            switch response {
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
            case .success(_):
                self?.confirm()
            }
        })
    }
}

extension TrusteeDecisionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        Constants.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let voting = self.viewModel.voting else { return 0 }
        if section == 0 {
            return 2
        } else if section == 1 {
            let surveyCell = voting.includeSurvey ? 1 : 0
            let streamCell = voting.includeStream ? 1 : 0
            return 1 + surveyCell + streamCell
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let voting = self.viewModel.voting
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingInfoCell.reuseIdentifier, for: indexPath) as! VotingInfoCell
                let name = "\(self.viewModel.project?.legalFormType ?? "") \(self.viewModel.project?.companyName ?? "")"
                cell.setup(with: name, type: .name)
                return cell
            default:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: ApproveVotingMainCell.reuseIdentifier, for: indexPath) as! ApproveVotingMainCell
                cell.setup(description: self.viewModel.voting?.description)
                return cell
            }
        case 1:
            if (voting?.includeSurvey ?? false) && indexPath.row == 0 {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingSurveyCell.reuseIdentifier, for: indexPath) as! VotingSurveyCell
                cell.setup(for: self.viewModel.voting)
                return cell
            } else if (voting?.includeStream ?? false) && indexPath.row == 1 {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingStreamCell.reuseIdentifier, for: indexPath) as! VotingStreamCell
                cell.setup(for: self.viewModel.voting)
                return cell
            } else {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingTrusteeCell.reuseIdentifier, for: indexPath) as! VotingTrusteeCell
                cell.setup(with: self.viewModel.voting)
                return cell
            }
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: ApproveVotingAgendaCell.reuseIdentifier, for: indexPath) as! ApproveVotingAgendaCell
            cell.setText(self.viewModel.voting?.agenda)
            return cell
        case 3:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingQuestionsContainerCell.reuseIdentifier, for: indexPath) as! VotingQuestionsContainerCell
            cell.setup(with: self.viewModel.voting?.questions ?? [], editable: false)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: ApproveVotingHeader.reuseIdentifier) as? ApproveVotingHeader
        let text: String?
        switch section {
        case 0:
            text = self.viewModel.voting?.type
        default:
            text = Constants.headers[section - 1]
        }
        view?.setText(text ?? "")
        view?.contentView.backgroundColor = .white
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 3 {
            let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: TrusteeDecisionFooter.reuseIdentifier) as? TrusteeDecisionFooter
            view?.delegate = self
            view?.contentView.backgroundColor = .white
            return view
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        section == 3 ? Constants.footerHeight : 0
    }
}

extension TrusteeDecisionViewController: TrusteeDecisionDelegate {
    func noInstructionSelected() {
        self.session?.votingProvider.trusteeDecision = .withoutObligation
        self.sendCode()
    }
    
    func instructionSelected() {
        _ = ViewControllerProvider.setupPassTrusteeViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func vote() {
        self.session?.votingProvider.trusteeDecision = .voteByHimself
        self.sendCode()
    }
}

