//
//  PassVotingPresenter.swift
//  8Pitch
//
//  Created by 8pitch on 3/15/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input
import Remote
import RxSwift

class PassVotingPresenter: PassVotingPresenterProtocol {    
    weak var view: PassVotingViewProtocol?
    var voting: Voting? {
        self.session?.votingProvider.currentVoting
    }
    var project: VotingProjectInfo? {
        self.session?.votingProvider.currentProject
    }
    private var remote: Remote = Remote()
    private var disposables : DisposeBag = .init()
    private var session: Session? {
        self.view?.session
    }
    
    required init(view: PassVotingViewProtocol) {
        self.view = view
        self.session?.votingProvider.currentProject = self.session?.votingProvider.currentVoting?.projectInfo
    }
    
    func submit() {
        guard let session = self.session else { return }
        self.view?.startProgress()
        self.remote.sendCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                switch response {
                case .success(_):
                    self?.view?.confirmSubmit()
                case .failure(let error):
                    self?.view?.completeRequest(with: error)
                }
            }).disposed(by: self.disposables)
    }
}
