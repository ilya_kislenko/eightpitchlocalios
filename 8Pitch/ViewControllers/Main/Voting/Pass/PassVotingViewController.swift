//
//  PassVotingViewController.swift
//  8Pitch
//
//  Created by 8pitch on 12.03.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class PassVotingViewController: GenericVotingViewController {
    
    var presenter: PassVotingPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "voting.passing.title".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.info), style: .plain, target: self, action: #selector(self.openInfo))
        self.setupUI()
    }
    override func headerTapped(_ sender: UITapGestureRecognizer?) {
        super.headerTapped(sender)
        guard let section = sender?.view?.tag else { return }
        expandedSections[section] = !expandedSections[section]
        tableView.reloadSections([section], with: .automatic)
    }
}

private extension PassVotingViewController {
    @objc func openInfo() {
        self.nextController = ViewControllerProvider.setupPassVotingInfoViewController(rootViewController: self)
        (self.nextController as? PassVotingInfoViewController)?.project = self.presenter.project
        self.pushNextController(animated: true)
    }
    
    func setupUI() {
        guard let voting = self.presenter.voting,
              let project = self.presenter.project else { return }
        self.customTitle = voting.type
        self.setupCustomTitle()
        self.votingData = VotingData(name: "\(project.legalFormType) \(project.companyName)", date: voting.votingPeriod, description: voting.description, agenda: voting.agenda, questions: voting.questions, editable: true)
        self.tableView.reloadData()
    }
}

extension PassVotingViewController {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == Constants.numberOfSections - 1 {
            let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: PassVotingFooter.reuseIdentifier) as? PassVotingFooter
            view?.delegate = self
            view?.voting = self.presenter.voting
            view?.contentView.backgroundColor = .white
            return view
        } else {
            return nil
        }
    }
}

extension PassVotingViewController: PassVotingViewProtocol {
    func reloadFooter() {
        self.tableView.reloadSections(IndexSet(integer: Constants.numberOfSections - 1), with: .fade)
    }
    
    func completeRequest(with error: Error?) {
        self.stopProgress()
        guard let error = error else {
            self.setupUI()
            return
        }
        self.presentAlert(message: error.localizedDescription)
    }
    
    func submit() {
        self.presenter.submit()
    }
    
    func confirmSubmit() {
        self.stopProgress()
        _ = ViewControllerProvider.setupConfirmPassVotingViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}
