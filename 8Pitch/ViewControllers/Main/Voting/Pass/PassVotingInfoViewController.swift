//
//  PassVotingInfoViewController.swift
//  8Pitch
//
//  Created by 8pitch on 3/15/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class PassVotingInfoViewController: GenericViewController, SheetDetentPresentationSupport {
    
    public var project: VotingProjectInfo?
    
    var contentHeight: CGFloat {
        let size = self.infoLabel.sizeThatFits(self.infoLabel.frame.size)
        return self.infoLabel.frame.origin.y + size.height + 2 * Constants.inset * 2
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: InfoTitleSemiboldLabel!
    @IBOutlet weak var infoLabel: InfoLabel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.modalPresentationStyle = .custom
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layer.cornerRadius = Constants.cornerRadius
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (self.transitioningDelegate as? CustomTitleViewController)?.removeBlurEffect()
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func reject() {
        guard let delegate = self.transitioningDelegate as? ApproveVotingViewProtocol else {
            return
        }
        self.dismiss(animated: true, completion: {
            delegate.reject()
        })
    }
    
    private func setupUI() {
        self.view.layer.cornerRadius = Constants.cornerRadius
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.close))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        self.titleLabel.text = "voting.passing.info.title".localized
        let text = NSMutableAttributedString(string: "voting.passing.info.text".localized)
        let projectName = "\(self.project?.legalFormType ?? "") \(self.project?.companyName ?? "")"
        let bye = NSAttributedString(string: String(format: "voting.passing.info.finish".localized, projectName),
                                     attributes: [.foregroundColor: UIColor(.red)])
        text.append(bye)
        self.infoLabel.attributedText = text
    }
    
    private enum Constants {
        static let cornerRadius: CGFloat = 10
        static let inset: CGFloat = 24
    }
}

