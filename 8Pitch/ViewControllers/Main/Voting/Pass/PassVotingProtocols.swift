//
//  PassVotingProtocols.swift
//  8Pitch
//
//  Created by 8pitch on 12.03.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input

protocol PassVotingPresenterProtocol {
    var voting: Voting? { get }
    var project: VotingProjectInfo? { get }
    func submit()
}

protocol PassVotingViewProtocol: class {
    var session: Session? { get set }
    func startProgress()
    func completeRequest(with error: Error?)
    func submit()
    func confirmSubmit()
    func reloadFooter()
}

