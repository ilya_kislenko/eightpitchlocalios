//
//  VotingPassedViewController.swift
//  8Pitch
//
//  Created by 8pitch on 3/15/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class VotingPassedViewController: RedViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeTapped))
        self.setupContent()
    }
    
    @IBOutlet weak var titleLabel: CustomSubtitleDarkLabel!
    @IBOutlet weak var infoLabel: InfoWhiteLabel!
    @IBOutlet weak var overviewButton: RoundedButton!
    
    @IBAction func overview(_ sender: UIButton) {
        self.closeTapped()
    }
    
    private func setupContent() {
        self.titleLabel.text = "voting.passing.success.title".localized
        self.infoLabel.text = "voting.passing.success.text".localized
        self.overviewButton.setTitle("voting.passing.success.button".localized, for: .normal)
    }
    
    @objc private func closeTapped() {
        if let votingList = navigationController?.viewControllers.first(where: { $0 is VotingsForProjectsViewController }) as? GenericViewController {
            self.previousController = votingList
            self.popToPreviousController(animated: true)
        } else {
            _ = ViewControllerProvider.setupVotingsForProjectsViewController(rootViewController: self)
            self.nextController?.previousController = navigationController?.viewControllers.first(where: { $0 is NotificationListViewController }) as? GenericViewController
            self.pushNextController(animated: true)
        }
    }
}

