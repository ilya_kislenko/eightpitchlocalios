//
//  ProjectVotingsViewController.swift
//  8Pitch
//
//  Created by 8pitch on 15.03.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Foundation
import UIKit

class ProjectVotingsViewController: CustomTitleViewController, ProjectVotingsViewProtocol {
    
    enum VotingCell {
        case openApprove
        case openPass
        case loadResults
        case loadAnswers
        case plug
        case stream
        case popUp
    }
    
    enum Constants {
        static let separatorInset = CGFloat(16)
        static let openRowHeight = CGFloat(48)
        static let rowHeight = CGFloat(128)
        static let headerHeight = CGFloat(48)
        static let numberWithOpenRow = 2
        static let numberOfRow = 1
    }
    
    var presenter: ProjectVotingsPresenterProtocol!
    
    @IBOutlet weak var tableView: SelfSizingTableView!
    
    lazy var expandedSections = Array(repeating: false, count: presenter.votings?.count ?? 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTitles()
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: VotingProjectDetailsCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: VotingProjectDetailsCell.reuseIdentifier)
        tableView.register(UINib(nibName: "VotingProjectDetailsHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "VotingProjectDetailsHeaderView")
        tableView.register(UINib(nibName: OpenVotingDetailsCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: OpenVotingDetailsCell.reuseIdentifier)
        tableView.register(UINib(nibName: OpenVotingCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: OpenVotingCell.reuseIdentifier)
        self.navigationItem.rightBarButtonItem = nil
    }
    
    private func loadStream(id: Int) {
        self.startProgress()
        self.presenter.joinStream(id: id, completionHandler: { [weak self] error in
            self?.stopProgress()
            if let _ = error {
                self?.streamError()
            } else {
                self?.openStream()
            }
        })
    }
    
    private func openCellNeeded(indexPath: IndexPath) -> VotingCell? {
        if indexPath.row != 0 {
            return nil
        } else {
            self.session?.votingProvider.currentVoting = presenter.votings?[indexPath.section]
            let voting = self.session?.votingProvider.currentVoting
            let isInitiator = self.session?.accountGroup == .initiator
            switch self.session?.votingProvider.currentVoting?.status {
            case .completed:
                return isInitiator ? .loadResults : .loadAnswers
            case .waitingForApproval:
                return (voting?.createdBy != .PI) && isInitiator ? .openApprove : nil
            case .running,
                 .pending:
                if isInitiator {
                    return nil
                } else if voting?.usersVotingStatus == .finished {
                    return .loadAnswers
                } else if voting?.usersVotingStatus == .available {
                    return .openPass
                } else if voting?.usersVotingStatus == .inProgress {
                    return .popUp
                }
            case .infoCollectionRunning:
                return isInitiator ? nil : .plug
            case .streamRunning,
                 .stream:
                return isInitiator ? nil : .stream
            default:
                return nil
            }
            return nil
        }
    }
    
    private func setupTitles() {
        let isInitiator = self.session?.accountGroup == .initiator
        if let projectName = session?.votingProvider.currentProject?.companyName {
            self.title = projectName.uppercased()
        }
        
        if let votingStatus = presenter.votings?.first?.status {
            switch votingStatus {
            case .waitingForApproval:
                self.customTitle = "voting.list.status.waitingForApproval".localized
            case .running,
                 .pending:
                let title = isInitiator ? "voting.list.status.running" : "voting.list.status.pending"
                self.customTitle = title.localized
            case.completed :
                self.customTitle = "voting.list.status.completed".localized
            case .infoCollection,
                 .infoCollectionRunning:
                self.customTitle = "voting.list.status.survey".localized
            case .stream,
                 .streamRunning:
                self.customTitle = "voting.list.status.stream".localized
            case .waitTrustee:
                self.customTitle = "voting.list.status.trustee".localized
            default:
                break
            }
        }
    }
    
    func completeRequest(with error: Error?) {
        stopProgress()
        guard let error = error else {
            return
        }
        presentAlert(message: error.localizedDescription)
    }
    
    func updateContent() {
        tableView.reloadData()
    }
    
    func openStream() {
        _ = ViewControllerProvider.setupStreamViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func streamError() {
        _ = ViewControllerProvider.setupStreamErrorViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}

extension ProjectVotingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.votings?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if expandedSections[section] {
            let openCellNeeded = self.openCellNeeded(indexPath: IndexPath(row: 0, section: section)) != nil
            return openCellNeeded ? Constants.numberWithOpenRow : Constants.numberOfRow
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "VotingProjectDetailsHeaderView") as! VotingProjectDetailsHeaderView
        if expandedSections[section] {
            view.image.image = UIImage(named: "arrowUp")
        } else {
            view.image.image = UIImage(named: "arrowDown")
        }
        let voting = presenter.votings![section]
        view.name.text = voting.type
        let tapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(headerTapped(_:))
        )
        view.tag = section
        view.addGestureRecognizer(tapGestureRecognizer)
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let openCellType = self.openCellNeeded(indexPath: indexPath) {
            switch openCellType {
            case .loadAnswers, .loadResults, .stream, .popUp:
                let cell = tableView.dequeueReusableCell(withIdentifier: OpenVotingCell.reuseIdentifier) as! OpenVotingCell
                cell.setup(for: openCellType)
                return cell
            case .openPass, .openApprove, .plug:
                let cell = tableView.dequeueReusableCell(withIdentifier: OpenVotingDetailsCell.reuseIdentifier) as! OpenVotingDetailsCell
                cell.setup(for: openCellType)
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: VotingProjectDetailsCell.reuseIdentifier) as! VotingProjectDetailsCell
            let voting = presenter.votings![indexPath.section]
            cell.setupWith(voting: voting, for: self.session?.accountGroup ?? .initiator)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return expandedSections[section] ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        
        let separator = UIView()
        separator.backgroundColor = UIColor(.grayCurrecy)
        view.addSubview(separator)
        
        let isLastSection = section == (presenter.votings?.count ?? 0) - 1
        let inset = isLastSection ? 0 : Constants.separatorInset
        
        separator.snp.makeConstraints {
            $0.height.equalTo(1)
            $0.leading.trailing.equalToSuperview().inset(inset)
            $0.centerY.equalToSuperview()
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let openCellType = self.openCellNeeded(indexPath: indexPath) {
            switch openCellType {
            case .loadAnswers:
                _ = ViewControllerProvider.setupYourAnswersViewController(rootViewController: self)
            case .loadResults:
                _ = ViewControllerProvider.setupVotingResultsViewController(rootViewController: self)
            case .openApprove:
                _ = ViewControllerProvider.setupApproveVotingViewController(rootViewController: self)
            case .openPass:
                _ = ViewControllerProvider.setupPassVotingViewController(rootViewController: self)
            case .plug:
                _ = ViewControllerProvider.setupVotingPlugViewController(rootViewController: self)
            case .stream:
                self.loadStream(id: presenter.votings?[indexPath.section].id ?? 0)
                return
            case .popUp:
                self.presentAlert(message: "voting.pending.error.inProgress".localized)
                return
            }
            self.pushNextController(animated: true)
        }
    }
    
    @objc func headerTapped(_ sender: UITapGestureRecognizer?) {
        guard let section = sender?.view?.tag else { return }
        expandedSections[section] = !expandedSections[section]
        tableView.reloadSections([section], with: .fade)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let _ = self.openCellNeeded(indexPath: indexPath) {
            return UITableView.automaticDimension
        } else {
            return Constants.rowHeight
        }
    }
}
