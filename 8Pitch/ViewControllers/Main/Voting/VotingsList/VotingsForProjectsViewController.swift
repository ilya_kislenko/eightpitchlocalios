//
//  VotingsListViewController.swift
//  8Pitch
//
//  Created by 8pitch on 15.03.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Foundation
import UIKit

class VotingsForProjectsViewController: CustomTitleViewController, VotingsForProjectsViewProtocol {
    
    fileprivate enum Constants {
        static let indicatorOffset: CGFloat = -indicatorHeight
        static let indicatorHeight: CGFloat = 2
        static let verticalOffsetsImageLink: CGFloat = 71
        static let verticalOffsetsTiledImage: CGFloat = 55
        static let horizontalOffsets: CGFloat = 32
        static let horizontalOffsetsForPad: CGFloat = 40
        static let expectedImageWidth: CGFloat = 343
        static let expectedImageHeight: CGFloat = 224
        static let iconsHeight: CGFloat = 39
    }
    
    lazy var presenter: VotingsForProjectsPresenterProtocol = {
        VotingsForProjectsPresenter(view: self)
    }()
    
    @IBOutlet weak var noResultsView: UIView!
    @IBOutlet weak var noResultsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filtersBar: ProjectsSegmentedControl!
    @IBOutlet weak var separator: UIView!
    
    private let indicator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(.red)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "voting.approval.approved.button".localized
        self.customTitle = "dashboard.voting.title.initiator".localized
        self.filtersBar.apportionsSegmentWidthsByContent = true
        self.filtersBar.addSubview(indicator)
        self.setupSegmentedControl(segmentedControl: filtersBar)
        self.setDefaultConstraints(for: self.indicator, dependsOn: filtersBar)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 48
        tableView.register(UINib(nibName: VotingProjectsCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: VotingProjectsCell.reuseIdentifier)
        self.navigationItem.rightBarButtonItem = nil
        self.hidesBottomBarWhenPushed = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.handleSegmentSelection(control: self.filtersBar, indicator: self.indicator)
    }
    
    override func setupUIForIpad() {
        super.setupUIForIpad()
        self.filtersBar.customizeForPad()
    }
    
    func reloadTable() {
        tableView.reloadData()
    }
    
    func completeRequest(with error: Error?) {
        self.stopProgress()
        guard let error = error else {
            tableView.reloadData()
            return
        }
        presentAlert(message: error.localizedDescription)
    }
    
}

extension VotingsForProjectsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch presenter.selectedFilter?.status.first {
        case .completed:
            self.noResultsLabel.text = self.session?.accountGroup == .initiator ? "votings.noVoting.initiator.completed".localized : "votings.noVoting.investor.completed".localized
        default:
            self.noResultsLabel.text = self.session?.accountGroup == .initiator ? "votings.noVoting.initiator.active".localized : "votings.noVoting.investor.active".localized
        }
        if presenter.projectsForVotings.count == 0 {
            noResultsView.isHidden = false
            separator.isHidden = true
        } else {
            noResultsView.isHidden = true
            separator.isHidden = false
        }
        return presenter.projectsForVotings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VotingProjectsCell.reuseIdentifier) as! VotingProjectsCell
        let project = presenter.projectsForVotings[indexPath.row]
        cell.name.text = project.companyName
        cell.separator.isHidden = indexPath.row == presenter.projectsForVotings.count - 1
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor(.grayCurrecy)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let project = presenter.projectsForVotings[indexPath.row]
        self.session?.votingProvider.currentProject = project
        _ = ViewControllerProvider.setupProjectVotingsViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}

//MARK:/ SegmentedControl setup
extension VotingsForProjectsViewController {
    func setupSegmentedControl(segmentedControl: ProjectsSegmentedControl) {
        filtersBar.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
        }
        filtersBar.removeAllSegments()
        for filter in presenter.votingFilters {
            filtersBar.insertSegment(withTitle: filter.title, at: filter.index!, animated: false)
        }
        self.fixBackgroundSegmentControl(segmentedControl)
        self.filtersBar.selectedSegmentIndex = 0
    }
    
    func fixBackgroundSegmentControl( _ segmentedControl: UISegmentedControl) {
        if #available(iOS 13.0, *) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                for i in 0...(segmentedControl.numberOfSegments - 1)  {
                    let backgroundSegmentView = segmentedControl.subviews[i]
                    backgroundSegmentView.isHidden = true
                }
            }
        }
    }
    
    func setDefaultConstraints(for indicator: UIView, dependsOn segmentedControl: UISegmentedControl) {
        let width = self.filtersBar.segmentWidthByContent(for: segmentedControl.selectedSegmentIndex)
        indicator.snp.makeConstraints {
            $0.leading.equalTo(segmentedControl.snp.leading)
            $0.top.equalTo(segmentedControl.snp.bottom).offset(Constants.indicatorOffset)
            $0.height.equalTo(Constants.indicatorHeight)
            $0.width.equalTo(width)
        }
    }
    
    @IBAction func tap(_ sender: UISegmentedControl) {
        presenter.didSelectFilter(index: sender.selectedSegmentIndex)
        self.handleSegmentSelection(control: self.filtersBar, indicator: self.indicator)
        UIView.animate(withDuration: 0.3, animations: {
                        self.view.layoutIfNeeded() })
    }
    
    func handleSegmentSelection(control: UISegmentedControl, indicator: UIView) {
        let width = self.filtersBar.segmentWidthByContent(for: filtersBar.selectedSegmentIndex)
        let offset = self.filtersBar.segmentOffsetByContent(for: filtersBar.selectedSegmentIndex)
        indicator.snp.remakeConstraints {
            $0.leading.equalTo(filtersBar.snp.leading).offset(offset)
            $0.top.equalTo(filtersBar.snp.bottom).offset(Constants.indicatorOffset)
            $0.height.equalTo(Constants.indicatorHeight)
            $0.width.equalTo(width)
        }
    }
}
