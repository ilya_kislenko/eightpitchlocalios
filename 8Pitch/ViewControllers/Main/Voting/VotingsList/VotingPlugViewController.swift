//
//  VotingPlugViewController.swift
//  8Pitch
//
//  Created by 8pitch on 3/31/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Remote

class VotingPlugViewController: CustomTitleViewController {
    
    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var textLabel: InfoLabel!
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerLabel.text = "voting.plug.title".localized
        self.textLabel.text = "voting.plug.text".localized
        self.button.setTitle("voting.plug.button".localized, for: .normal)
        self.navigationItem.rightBarButtonItem = nil
    }
    
    @IBAction func login(_ sender: UIButton) {
        guard let url = URL(string: WebURLConstants.login) else { return }
        UIApplication.shared.open(url)
    }
}
