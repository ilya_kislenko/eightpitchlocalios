//
//  VotingsListProtocols.swift
//  8Pitch
//
//  Created by 8pitch on 17.03.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input
import Remote

struct VotingStatusFilterModel {
    let title: String?
    let index: Int?
    let status: [Voting.VotingStatus]
    var userVotingStatus: Voting.UserVotingsStatus?
}

protocol VotingsForProjectsPresenterProtocol {
    var selectedFilter: VotingStatusFilterModel? { get set }
    var votings: [Voting?]? { get }
    var projectsForVotings: [VotingProjectInfo] { get }
    var votingFilters: [VotingStatusFilterModel] { get }
    func didSelectFilter(index: Int)
    func viewWillAppear()
}

protocol VotingsForProjectsViewProtocol: class {
    var session: Session? { get set }
    func startProgress()
    func completeRequest(with error: Error?)
}


protocol ProjectVotingsPresenterProtocol {
    var votings: [Voting]? { get set }
    func joinStream(id: Int, completionHandler: @escaping ErrorClosure)
}

protocol ProjectVotingsViewProtocol: class {
    var session: Session? { get set }
    func startProgress()
    func completeRequest(with error: Error?)
    func updateContent()
}
