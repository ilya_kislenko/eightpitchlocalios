//
//  VotingsForProjectsPresenter.swift
//  8Pitch
//
//  Created by 8pitch on 17.03.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input
import Remote
import RxSwift

class VotingsForProjectsPresenter: VotingsForProjectsPresenterProtocol {
    
    weak var view: VotingsForProjectsViewProtocol?
    
    private var disposables : DisposeBag = .init()
    private var remote: Remote = Remote()
    private var session: Session? {
        self.view?.session
    }
    
    var votings: [Voting?]? {
        self.session?.votingProvider.votings
    }
    var selectedFilter: VotingStatusFilterModel?
    
    var votingFilters: [VotingStatusFilterModel] {
        session?.accountGroup == .initiator ?
            [VotingStatusFilterModel(title: "voting.list.status.waitingForApproval".localized.uppercased(), index: 0, status: [.waitingForApproval]),
             VotingStatusFilterModel(title: "voting.list.status.survey".localized.uppercased(), index: 1, status: [.infoCollection, .infoCollectionRunning]),
             VotingStatusFilterModel(title: "voting.list.status.stream".localized.uppercased(), index: 2, status: [.stream, .streamRunning]),
             VotingStatusFilterModel(title: "voting.list.status.running".localized.uppercased(), index: 3, status: [.running, .pending]),
             VotingStatusFilterModel(title: "voting.list.status.trustee".localized.uppercased(), index: 4, status: [.waitTrustee]),
             VotingStatusFilterModel(title: "voting.list.status.completed".localized.uppercased(), index: 5, status: [.completed])]
            :
            [VotingStatusFilterModel(title: "voting.list.status.survey".localized.uppercased(), index: 0, status: [.infoCollection, .infoCollectionRunning]),
             VotingStatusFilterModel(title: "voting.list.status.stream".localized.uppercased(), index: 1, status: [.stream, .streamRunning]),
             VotingStatusFilterModel(title: "voting.list.status.pending".localized.uppercased(), index: 2, status: [.running]),
             VotingStatusFilterModel(title: "voting.list.status.completed".localized.uppercased(), index: 3, status: [.completed], userVotingStatus: .finished)]
    }
    
    var projectsForVotings = [VotingProjectInfo]()
    
    required init(view: VotingsForProjectsViewProtocol) {
        self.view = view
        let isInitiator = session?.accountGroup == .initiator
        self.selectedFilter = isInitiator ?
            VotingStatusFilterModel(title: "voting.list.status.waitingForApproval".localized.uppercased(),
                                    index: 0,
                                    status: [.waitingForApproval]) :
            VotingStatusFilterModel(title: "voting.list.status.pending".localized.uppercased(),
                                    index: 0,
                                    status: [.running])
        self.session?.votingProvider.currentFilter = VotingFilter(statuses: self.selectedFilter?.status ?? [], userStatus: self.selectedFilter?.userVotingStatus)
    }
    
    func viewWillAppear() {
        self.votings(completionHandler: { [weak self] error in
            if let error = error {
                self?.view?.completeRequest(with: error)
            } else {
                self?.didSelectFilter(index: self?.selectedFilter?.index ?? 0)
            }
        })
    }
    
    func votings(completionHandler: @escaping ErrorClosure) {
        guard let session = session else { return }
        switch session.accountGroup {
        case .initiator:
            self.view?.startProgress()
            self.remote.PIvotings(session) { (error) in
                completionHandler(error)
            }
        case .institutionalInvestor, .privateInvestor:
            self.view?.startProgress()
            self.remote.investorsVotings(session) { (error) in
                completionHandler(error)
            }
        case .none:
            return
        }
    }
    
    func didSelectFilter(index: Int) {
        projectsForVotings.removeAll()
        self.selectedFilter = votingFilters.first { (filter) -> Bool in
            filter.index == index
        }
        self.session?.votingProvider.currentFilter = VotingFilter(statuses: self.selectedFilter?.status ?? [], userStatus: self.selectedFilter?.userVotingStatus)
        if let status = selectedFilter?.status {
            filterProjectsWith(status: status, userStatus: selectedFilter?.userVotingStatus)
            self.view?.completeRequest(with: nil)
        }
    }
    
    private func filterProjectsWith(status: [Voting.VotingStatus], userStatus: Voting.UserVotingsStatus?) {
        guard let safeVotings = votings else { return }
        for vote in safeVotings {
            guard let vote = vote else { continue }
            if (status.contains(where: { $0 == vote.status }) && userStatus == nil) ||
                (status.contains(where: { $0 == vote.status }) && vote.usersVotingStatus == userStatus ) {
                self.projectsForVotings.append(vote.projectInfo)
            }
        }
        self.projectsForVotings = Array(Set(projectsForVotings))
    }
}

