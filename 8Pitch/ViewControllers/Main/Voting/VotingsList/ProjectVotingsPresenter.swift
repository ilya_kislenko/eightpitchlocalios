//
//  ProjectVotingsPresenter.swift
//  8Pitch
//
//  Created by 8pitch on 17.03.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input
import Remote
import RxSwift

class ProjectVotingsPresenter: ProjectVotingsPresenterProtocol {
    
    weak var view: ProjectVotingsViewProtocol?
    
    var votings: [Voting]?
    
    private var disposables : DisposeBag = .init()
    private var remote: Remote = Remote()
    private var session: Session? {
        self.view?.session
    }
    
    required init(view: ProjectVotingsViewProtocol) {
        self.view = view
        self.filterVotingsByProjectId()
        self.sortVotings()
    }
    
    func joinStream(id: Int, completionHandler: @escaping ErrorClosure) {
        guard let session = self.session else { return }
        self.remote.joinMeeting(session, id: id, completionHandler: { error in
            completionHandler(error)
        })
    }
    
    private func filterVotingsByProjectId() {
        guard let filter = self.session?.votingProvider.currentFilter,
              let project = session?.votingProvider.currentProject else { return }
        let filtered = session?.votingProvider.votings.filter { voting in
            (voting.projectId == project.projectId)
                && (filter.statuses.contains(where: { voting.status == $0 }))
                && ((filter.userStatus == nil) || (filter.userStatus == voting.usersVotingStatus))
        }
        votings = filtered
        self.view?.completeRequest(with: nil)
    }
    
    private func sortVotings() {
        switch self.votings?.first?.status {
        case .running,
             .pending:
            self.votings?.sort { $0.startDate ?? Date() > $1.startDate ?? Date() }
        case .completed:
            self.votings?.sort { $0.endDate ?? Date() > $1.endDate ?? Date() }
        case .waitingForApproval:
            self.votings?.sort { $0.createDateTime ?? Date() > $1.createDateTime ?? Date() }
        case .infoCollection,
             .infoCollectionRunning:
            self.votings?.sort { $0.informationCollectionPeriodStartDate ?? Date() > $1.informationCollectionPeriodStartDate ?? Date() }
        case .stream,
             .streamRunning:
            self.votings?.sort { $0.streamStartDate ?? Date() > $1.streamStartDate ?? Date() }
        case .waitTrustee:
            self.votings?.sort { $0.endDate ?? Date() > $1.endDate ?? Date() }
        default:
            return
        }
    }
}
