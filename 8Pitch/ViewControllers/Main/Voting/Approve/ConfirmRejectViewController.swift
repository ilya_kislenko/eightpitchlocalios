//
//  ConfirmRejectViewController.swift
//  8Pitch
//
//  Created by 8pitch on 3/10/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class ConfirmRejectViewController: GenericViewController, SheetDetentPresentationSupport {
    
    var contentHeight: CGFloat {
        self.cancelButton.frame.maxY + Constants.inset * 2 + self.view.safeAreaInsets.bottom
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: InfoTitleSemiboldLabel!
    @IBOutlet weak var infoLabel: InfoLabel!
    @IBOutlet weak var rejectButton: RoundedButton!
    @IBOutlet weak var cancelButton: BorderedButton!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.modalPresentationStyle = .custom
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.rejectButton.addTarget(self, action: #selector(reject), for: .touchUpInside)
        self.cancelButton.addTarget(self, action: #selector(close), for: .touchUpInside)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layer.cornerRadius = Constants.cornerRadius
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (self.transitioningDelegate as? CustomTitleViewController)?.removeBlurEffect()
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func reject() {
        guard let delegate = self.transitioningDelegate as? ApproveVotingViewProtocol else {
            return
        }
        self.dismiss(animated: true, completion: {
            delegate.reject()
        })
    }
    
    private func setupUI() {
        self.view.layer.cornerRadius = Constants.cornerRadius
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.close))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        self.cancelButton.layer.borderColor = UIColor(.gray).cgColor
        self.rejectButton.setTitle("voting.approval.button.reject.yes".localized, for: .normal)
        self.cancelButton.setTitle("voting.approval.button.cancel".localized, for: .normal)
        self.titleLabel.text = "voting.approval.reject.title".localized
        self.infoLabel.text = "voting.approval.reject.info".localized
    }
    
    private enum Constants {
        static let cornerRadius: CGFloat = 10
        static let inset: CGFloat = 24
    }
    
}
