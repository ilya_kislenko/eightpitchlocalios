//
//  VotingApprovedViewController.swift
//  8Pitch
//
//  Created by 8pitch on 3/10/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class VotingApprovedViewController: RedViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeTapped))
        self.setupContent()
    }
    
    @IBOutlet weak var titleLabel: CustomSubtitleDarkLabel!
    @IBOutlet weak var infoLabel: InfoWhiteLabel!
    @IBOutlet weak var overviewButton: RoundedButton!
    
    @IBAction func overview(_ sender: UIButton) {
        self.closeTapped()
    }
    
    private func setupContent() {
        self.titleLabel.text = "voting.approval.approved.title".localized
        self.infoLabel.text = "voting.approval.approved.info".localized
        self.overviewButton.setTitle("voting.approval.approved.button".localized, for: .normal)
    }
    
    @objc private func closeTapped() {
        if let votingList = navigationController?.viewControllers.first(where: { $0 is VotingsForProjectsViewController }) as? GenericViewController {
            self.previousController = votingList
            self.popToPreviousController(animated: true)
        } else {
            _ = ViewControllerProvider.setupVotingsForProjectsViewController(rootViewController: self)
            self.nextController?.previousController = navigationController?.viewControllers.first(where: { $0 is NotificationListViewController }) as? GenericViewController
            self.pushNextController(animated: true)
        }
    }
}
