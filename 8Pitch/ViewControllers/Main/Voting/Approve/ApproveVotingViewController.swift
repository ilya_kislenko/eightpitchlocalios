//
//  ApproveVotingViewController.swift
//  8Pitch
//
//  Created by 8pitch on 3/9/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class ApproveVotingViewController: CustomTitleViewController {
    
    var presenter: ApproveVotingPresenterProtocol!
    
    @IBOutlet weak var tableView: UITableView!
    var expandedSections = [true, true, true, true]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "voting.approval.title".localized
        self.customTitle = "voting.approval.customTitle".localized
        self.registerCell()
        self.navigationItem.rightBarButtonItem = nil
    }
}

extension ApproveVotingViewController:  ApproveVotingViewProtocol {
    func approve() {
        self.presenter.approve()
    }
    
    func reject() {
        self.presenter.reject()
    }
    
    func confirmReject() {
        _ = ViewControllerProvider.setupConfirmRejectViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func confirmApprove() {
        self.stopProgress()
        _ = ViewControllerProvider.setupConfirmApproveViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func completeRequest(with error: Error?) {
        self.stopProgress()
        guard let error = error else {
            self.tableView.reloadData()
            return
        }
        self.presentAlert(message: error.localizedDescription)
    }
    
    func openVotings() {
        if let votingList = navigationController?.viewControllers.first(where: { $0 is VotingsForProjectsViewController }) as? GenericViewController {
            self.previousController = votingList
            self.popToPreviousController(animated: true)
        } else {
            _ = ViewControllerProvider.setupVotingsForProjectsViewController(rootViewController: self)
            self.nextController?.previousController = navigationController?.viewControllers.first(where: { $0 is NotificationListViewController }) as? GenericViewController
            self.pushNextController(animated: true)
        }
    }
}

private extension ApproveVotingViewController {
    enum Constants {
        static let headers = [
            "voting.approval.header.settings".localized,
            "voting.approval.header.agenda".localized,
            "voting.approval.header.questions".localized
        ]
        static let headerHeight: CGFloat = 38
        static let footerHeight: CGFloat = 230
        static let estimatedRowHeight: CGFloat = 200
    }
    
    func registerCell() {
        self.tableView.register(ApproveVotingHeader.nibForRegistration, forHeaderFooterViewReuseIdentifier: ApproveVotingHeader.reuseIdentifier)
        self.tableView.register(ApproveVotingMainCell.nibForRegistration, forCellReuseIdentifier: ApproveVotingMainCell.reuseIdentifier)
        self.tableView.register(VotingSettingsCell.nibForRegistration, forCellReuseIdentifier: VotingSettingsCell.reuseIdentifier)
        self.tableView.register(ApproveVotingAgendaCell.nibForRegistration, forCellReuseIdentifier: ApproveVotingAgendaCell.reuseIdentifier)
        self.tableView.register(VotingQuestionsContainerCell.nibForRegistration, forCellReuseIdentifier: VotingQuestionsContainerCell.reuseIdentifier)
        self.tableView.register(ApproveVotingFooter.nibForRegistration, forHeaderFooterViewReuseIdentifier: ApproveVotingFooter.reuseIdentifier)
        self.tableView.register(VotingInfoCell.nibForRegistration, forCellReuseIdentifier: VotingInfoCell.reuseIdentifier)
        self.tableView.register(VotingSurveyCell.nibForRegistration, forCellReuseIdentifier: VotingSurveyCell.reuseIdentifier)
        self.tableView.register(VotingStreamCell.nibForRegistration, forCellReuseIdentifier: VotingStreamCell.reuseIdentifier)
    }
}

extension ApproveVotingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let voting = self.presenter.voting else { return 0 }
        if section == 0 {
            return 2
        } else if section == 1 {
            let surveyCell = voting.includeSurvey ? 1 : 0
            let streamCell = voting.includeStream ? 1 : 0
            return 1 + surveyCell + streamCell
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let voting = self.presenter.voting
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingInfoCell.reuseIdentifier, for: indexPath) as! VotingInfoCell
                let name = "\(self.presenter.project?.legalFormType ?? "") \(self.presenter.project?.companyName ?? "")"
                cell.setup(with: name, type: .name)
                return cell
            default:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: ApproveVotingMainCell.reuseIdentifier, for: indexPath) as! ApproveVotingMainCell
                cell.setup(description: self.presenter.voting?.description)
                return cell
            }
        case 1:
            if (voting?.includeSurvey ?? false) && indexPath.row == 0 {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingSurveyCell.reuseIdentifier, for: indexPath) as! VotingSurveyCell
                cell.setup(for: self.presenter.voting)
                return cell
            } else if (voting?.includeStream ?? false) && indexPath.row == 1 {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingStreamCell.reuseIdentifier, for: indexPath) as! VotingStreamCell
                cell.setup(for: self.presenter.voting)
                return cell
            } else {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingSettingsCell.reuseIdentifier, for: indexPath) as! VotingSettingsCell
                cell.setup(with: self.presenter.voting)
                return cell
            }
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: ApproveVotingAgendaCell.reuseIdentifier, for: indexPath) as! ApproveVotingAgendaCell
            cell.setText(self.presenter.voting?.agenda)
            return cell
        case 3:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingQuestionsContainerCell.reuseIdentifier, for: indexPath) as! VotingQuestionsContainerCell
            cell.setup(with: self.presenter.voting?.questions ?? [], editable: false)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: ApproveVotingHeader.reuseIdentifier) as? ApproveVotingHeader
        let text: String?
        switch section {
        case 0:
            text = self.presenter.voting?.type
        default:
            text = Constants.headers[section - 1]
        }
        view?.setText(text ?? "")
        view?.contentView.backgroundColor = .white
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 3 {
            let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: ApproveVotingFooter.reuseIdentifier) as? ApproveVotingFooter
            view?.delegate = self
            view?.contentView.backgroundColor = .white
            return view
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        section == 3 ? Constants.footerHeight : 0
    }
}
