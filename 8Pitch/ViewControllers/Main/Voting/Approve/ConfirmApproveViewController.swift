//
//  ConfirmApproveViewController.swift
//  8Pitch
//
//  Created by 8pitch on 3/10/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import RxSwift

class ConfirmApproveViewController: SMSViewController {
    
    @IBOutlet weak var infoLabel: InfoCurrencyLabel!
    
    override var confirmationFooterViewType : PublishSubject<InputSMSView.ViewType?>? {
        didSet {
            guard let typeAction = self.confirmationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.title = "voting.approval.title".localized
        self.customTitle = "voting.approval.confirm.customTitle".localized
        self.infoLabel.text = "voting.approval.confirm.info".localized
    }

    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CodeTextCell.nibForRegistration, forCellWithReuseIdentifier: CodeTextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = InputSMSView.reuseIdentifier
        layout.register(InputSMSView.nibForRegistration, forDecorationViewOfKind: InputSMSView.reuseIdentifier)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.resendButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
    override func verifyToken() {
        super.verifyToken()
        guard let session = self.session, let id = session.votingProvider.currentVoting?.id else {
            return
        }
        self.remote.approveVoting(session, id: id, completionHandler: { [weak self] error in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.openSuccessScreen()
            }
        })
    }
    
    override func resendCode() {
        super.resendCode()
        guard let session = self.session else {
            return
        }
        self.remote.resendCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    return
                case .failure(let error):
                    self?.presentAlert(message: error.localizedDescription)
                }
            }).disposed(by: self.disposables)
    }
    
    private func openSuccessScreen() {
        _ = ViewControllerProvider.setupVotingApprovedViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}
