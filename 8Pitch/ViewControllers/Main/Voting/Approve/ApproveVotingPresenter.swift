//
//  ApproveVotingPresenter.swift
//  8Pitch
//
//  Created by 8pitch on 3/9/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input
import Remote
import RxSwift

class ApproveVotingPresenter: ApproveVotingPresenterProtocol {
    weak var view: ApproveVotingViewProtocol?
    
    private var disposables : DisposeBag = .init()
    private var remote: Remote = Remote()
    private var session: Session? {
        self.view?.session
    }
    
    var project: VotingProjectInfo? {
        self.session?.votingProvider.currentProject
    }
    
    var voting: Voting? {
        self.session?.votingProvider.currentVoting
    }
    
    required init(view: ApproveVotingViewProtocol) {
        self.view = view
        self.session?.votingProvider.currentProject = self.session?.votingProvider.currentVoting?.projectInfo
    }
    
    func reject() {
        guard let session = self.session,
              let id = self.voting?.id else { return }
        self.view?.startProgress()
        self.remote.rejectVoting(session, id: id, completionHandler: { [weak self] error in
            if let error = error {
                self?.view?.completeRequest(with: error)
            } else {
                self?.view?.openVotings()
            }
        })
    }
    
    func approve() {
        guard let session = self.session else { return }
        self.view?.startProgress()
        self.remote.sendCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                switch response {
                case .success(_):
                    self?.view?.confirmApprove()
                case .failure(let error):
                    self?.view?.completeRequest(with: error)
                }
            }).disposed(by: self.disposables)
    }
}
