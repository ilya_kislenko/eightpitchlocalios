//
//  ApproveVotingProtocols.swift
//  8Pitch
//
//  Created by 8pitch on 3/9/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input
import Remote

protocol ApproveVotingPresenterProtocol {
    var voting: Voting? { get }
    var project: VotingProjectInfo? { get }
    func reject()
    func approve()
}

protocol ApproveVotingViewProtocol: class {
    var session: Session? { get set }
    func startProgress()
    func completeRequest(with error: Error?)
    func approve()
    func confirmReject()
    func confirmApprove()
    func reject()
    func openVotings()
}
