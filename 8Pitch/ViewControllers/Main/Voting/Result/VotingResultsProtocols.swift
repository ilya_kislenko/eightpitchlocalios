//
//  VotingResultsProtocols.swift
//  8Pitch
//
//  Created by 8pitch on 3/17/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input

protocol VotingResultsPresenterProtocol {
    var voting: Voting? { get set }
    var project: VotingProjectInfo? { get }
    var votingResult: VotingResult? { get }
    func loadFile()
}

protocol VotingResultsViewProtocol: class {
    var session: Session? { get set }
    func startProgress()
    func completeRequest(with error: Error?, url: URL?)
}
