//
//  VotingResultsPresenter.swift
//  8Pitch
//
//  Created by 8pitch on 3/17/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input
import Remote

class VotingResultsPresenter: VotingResultsPresenterProtocol {
    var voting: Voting?
    var project: VotingProjectInfo? {
        self.session?.votingProvider.currentProject
    }
    var votingResult: VotingResult? {
        self.session?.votingProvider.votingResult
    }
    weak var view: VotingResultsViewProtocol?
    private var remote: Remote = Remote()
    private var session: Session? {
        self.view?.session
    }
    
    required init(view: VotingResultsViewProtocol) {
        self.view = view
        self.voting = self.session?.votingProvider.currentVoting
        self.loadResult()
    }
    
    private func loadResult() {
        guard let session = self.session,
              let id = self.session?.votingProvider.currentVoting?.id else {
            return
        }
        self.view?.startProgress()
        self.remote.votingResult(session, id: id, completionHandler: { [weak self] error in
            self?.session?.votingProvider.currentProject = self?.session?.votingProvider.currentVoting?.projectInfo
            self?.view?.completeRequest(with: nil, url: nil)
        })
    }
    
    func loadFile() {
        guard let session = self.session,
              let id = self.session?.votingProvider.currentVoting?.id else {
            return
        }
        self.view?.startProgress()
        self.remote.votingResultFile(session, id, { [weak self] result in
            switch result {
            case .success(let data):
                self?.saveFile(id: String(id), data: data)
            case .failure(let error):
                self?.view?.completeRequest(with: error, url: nil)
            }
        })
    }
    
    private func saveFile(id: String, data: Data) {
        guard let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
              let name = self.voting?.type else { return }
        let source = documentsPath.appendingPathComponent(String(format: "voting.result.fileName".localized, name))
        do {
            try? FileManager.default.removeItem(at: source)
            try data.write(to: source, options: .atomic)
        } catch {
            self.view?.completeRequest(with: error, url: nil)
            return
        }
        self.view?.completeRequest(with: nil, url: source)
    }
}
