//
//  VotingResultsViewController.swift
//  8Pitch
//
//  Created by 8pitch on 3/17/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit

class VotingResultsViewController: CustomTitleViewController, LoadFileDelegate, VotingResultsViewProtocol {
    var presenter: VotingResultsPresenterProtocol!
    
    enum Constants {
        static let headers = ["voting.passing.header.main".localized,
                              "voting.result.header.rate".localized,
                              "voting.approval.header.agenda".localized,
                              "voting.result.header.answers".localized]
        static let numberOfSections: Int = 4
        static let headerHeight: CGFloat = 48
        static let footerHeight: CGFloat = 150
    }
    
    private var expandedSections = [false, false, false, false]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "voting.result.title".localized
        self.registerCells()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.load), style: .plain, target: self, action: #selector(self.loadFile))
    }
    
    @objc func loadFile() {
        self.presenter.loadFile()
    }
    
    func completeRequest(with error: Error?, url: URL?) {
        self.stopProgress()
        if error == nil, url == nil {
            self.setupUI()
        } else if error == nil, let url = url {
            self.openFile(with: url)
        } else if let error = error {
            self.presentAlert(message: error.localizedDescription)
        }
    }
}

private extension VotingResultsViewController {
    func setupUI() {
        guard let voting = self.presenter.voting else { return }
        self.customTitle = voting.type
        self.setupCustomTitle()
        self.tableView.reloadData()
    }
    
    func registerCells() {
        self.tableView.register(ExpandableHeader.nibForRegistration, forHeaderFooterViewReuseIdentifier: ExpandableHeader.reuseIdentifier)
        self.tableView.register(ApproveVotingAgendaCell.nibForRegistration, forCellReuseIdentifier: ApproveVotingAgendaCell.reuseIdentifier)
        self.tableView.register(ApproveVotingMainCell.nibForRegistration, forCellReuseIdentifier: ApproveVotingMainCell.reuseIdentifier)
        self.tableView.register(DownloadPDFFooter.nibForRegistration, forHeaderFooterViewReuseIdentifier: DownloadPDFFooter.reuseIdentifier)
        self.tableView.register(VotingInfoCell.nibForRegistration, forCellReuseIdentifier: VotingInfoCell.reuseIdentifier)
        self.tableView.register(VotingRateCell.nibForRegistration, forCellReuseIdentifier: VotingRateCell.reuseIdentifier)
        self.tableView.register(VotingAnswersContainerCell.nibForRegistration, forCellReuseIdentifier: VotingAnswersContainerCell.reuseIdentifier)
    }
    
    @objc func headerTapped(_ sender: UITapGestureRecognizer?) {
        guard let section = sender?.view?.tag else { return }
        expandedSections[section] = !expandedSections[section]
        tableView.reloadSections([section], with: .fade)
    }
    
    func openFile(with url: URL) {
        let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        self.present(activityViewController, animated: true)
    }
    
}

extension VotingResultsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        Constants.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if expandedSections[section] {
            return section == 0 ? 3 : 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingInfoCell.reuseIdentifier, for: indexPath) as! VotingInfoCell
                let name = "\(self.presenter.project?.legalFormType ?? "") \(self.presenter.project?.companyName ?? "")"
                cell.setup(with: name, type: .name)
                return cell
            case 1:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingInfoCell.reuseIdentifier, for: indexPath) as! VotingInfoCell
                cell.setup(with: self.presenter.voting?.votingPeriod, type: .date)
                return cell
            case 2:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: ApproveVotingMainCell.reuseIdentifier, for: indexPath) as! ApproveVotingMainCell
                cell.setup(description: self.presenter.voting?.description)
                return cell
            default:
                let cell = UITableViewCell()
                return cell
            }
        case 1:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingRateCell.reuseIdentifier, for: indexPath) as! VotingRateCell
            cell.setValues(for: self.presenter.votingResult?.coveragePerInvestors ?? 0,
                           token: self.presenter.votingResult?.coveragePerTokens ?? 0)
            return cell
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: ApproveVotingAgendaCell.reuseIdentifier, for: indexPath) as! ApproveVotingAgendaCell
            cell.setText(self.presenter.voting?.agenda)
            return cell
        case 3:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: VotingAnswersContainerCell.reuseIdentifier, for: indexPath) as! VotingAnswersContainerCell
            cell.setup(with: self.presenter.votingResult?.questions ?? [], questions: self.presenter.voting?.questions ?? [])
            return cell
        default:
            let cell = UITableViewCell()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: ExpandableHeader.reuseIdentifier) as? ExpandableHeader
        let text = Constants.headers[section]
        view?.setText(text, expanded: self.expandedSections[section])
        view?.contentView.backgroundColor = .white
        let tapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                          action: #selector(headerTapped(_:)))
        view?.tag = section
        view?.addGestureRecognizer(tapGestureRecognizer)
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == Constants.numberOfSections - 1 {
            let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: DownloadPDFFooter.reuseIdentifier) as? DownloadPDFFooter
            view?.delegate = self
            view?.contentView.backgroundColor = .white
            return view
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        Constants.headerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        section == Constants.numberOfSections - 1 ? Constants.footerHeight : 0
    }
    
}
