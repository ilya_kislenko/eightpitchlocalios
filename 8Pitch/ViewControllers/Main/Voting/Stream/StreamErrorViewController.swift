//
//  StreamErrorViewController.swift
//  8Pitch
//
//  Created by 8pitch on 07.04.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class StreamErrorViewController: CustomTitleViewController {
    
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var titleLabel: CustomLabel!
    @IBOutlet weak var infoLabel: InfoLabel!
    @IBOutlet weak var button: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupContent()
    }
    
    private func setupContent() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeTapped))
        self.navigationItem.rightBarButtonItem = nil
        self.button.setTitle("voting.stream.error.button".localized.uppercased(), for: .normal)
        let formatter = DateHelper.shared.uiFormatterFull
        if let voting = self.session?.votingProvider.currentVoting,
           voting.streamStartDate ?? Date() > Date() {
            self.picture.image = UIImage(.streamErrorTime)
            self.titleLabel.text = "voting.stream.wait.title".localized
            self.infoLabel.text = String(format: "voting.stream.wait.text".localized,
                                         voting.type ?? "",
                                         formatter.string(from: voting.streamStartDate ?? Date()))
        } else if let voting = self.session?.votingProvider.currentVoting,
                  voting.streamEndDate ?? Date() < Date() {
            self.picture.image = UIImage(.streamErrorState)
            self.titleLabel.text = "voting.stream.completed.title".localized
            self.infoLabel.text = String(format: "voting.stream.completed.text".localized,
                                         voting.type ?? "",
                                         formatter.string(from: voting.streamStartDate ?? Date()))
        } else if let _ = self.session?.votingProvider.currentVoting {
            self.picture.image = UIImage(.streamErrorTime)
            self.titleLabel.text = "voting.stream.notStarted.title".localized
            self.infoLabel.text = "voting.stream.notStarted.text".localized
        } else {
            self.picture.image = UIImage(.streamErrorState)
            self.titleLabel.text = "voting.stream.error.403.title".localized
            self.infoLabel.text = "voting.stream.error.403.text".localized
        }
    }
    
    @objc private func closeTapped() {
        self.popToPreviousController(animated: true)
    }
    
    @IBAction func goToVotings(_ sender: UIButton) {
        if let votingList = navigationController?.viewControllers.first(where: { $0 is VotingsForProjectsViewController }) as? GenericViewController {
            self.previousController = votingList
            self.popToPreviousController(animated: true)
        } else {
            _ = ViewControllerProvider.setupVotingsForProjectsViewController(rootViewController: self)
            self.nextController?.previousController = navigationController?.viewControllers.first(where: { $0 is NotificationListViewController }) as? GenericViewController
            self.pushNextController(animated: true)
        }
    }
}
