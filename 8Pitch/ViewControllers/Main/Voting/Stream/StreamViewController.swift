//
//  StreamViewController.swift
//  8Pitch
//
//  Created by 8pitch on 07.04.2021.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import MobileRTC
import MobileCoreServices

class StreamViewController: DarkViewController, MobileRTCMeetingServiceDelegate {
    
    private var leaved: Bool = false
    private var joined: Bool = false
    
    @IBOutlet weak var goButton: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationItem.rightBarButtonItem = nil
        self.title = "voting.stream.title".localized.uppercased()
        self.view.backgroundColor = UIColor(.backgroundDark)
        self.goButton.setTitle("voting.stream.error.button".localized.uppercased(), for: .normal)
        self.joinMeeting()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.leaveMeeting()
    }
    
    private func joinMeeting() {
        self.startProgress()
        guard let creds = self.session?.votingProvider.streamCreds else { return }
        if let meetingService = MobileRTC.shared().getMeetingService() {
            let joinMeetingParameters = MobileRTCMeetingJoinParam()
            meetingService.delegate = self
            MobileRTC.shared().getMeetingSettings()?.waitingHUDHidden = true
            joinMeetingParameters.meetingNumber = String(creds.meetingNumber)
            joinMeetingParameters.password = creds.meetingPassword
            joinMeetingParameters.userName = self.session?.userName.fullName
            meetingService.joinMeeting(with: joinMeetingParameters)
        }
    }
                        
    @IBAction func goToVotings(_ sender: UIButton) {
        self.leaveMeeting()
        if let votingList = navigationController?.viewControllers.first(where: { $0 is VotingsForProjectsViewController }) as? GenericViewController {
            self.previousController = votingList
            self.popToPreviousController(animated: true)
        } else {
            _ = ViewControllerProvider.setupVotingsForProjectsViewController(rootViewController: self)
            self.nextController?.previousController = navigationController?.viewControllers.first(where: { $0 is NotificationListViewController }) as? GenericViewController
            self.pushNextController(animated: true)
        }
    }
}

extension StreamViewController {
    
    func onJBHWaiting(with cmd: JBHCmd) {
        switch cmd {
        case .show:
            self.leaveMeeting()
            self.openError()
        default:
            return
        }
    }
    
    func onMeetingStateChange(_ state: MobileRTCMeetingState) {
        switch state {
        case .waitingForHost,
             .failed,
             .ended:
            self.leaveMeeting()
            self.openError()
        case .inMeeting:
            self.joined = true
            self.stopProgress()
        default:
            return
        }
    }
    
    private func leaveMeeting() {
        if let meetingService = MobileRTC.shared().getMeetingService() {
            meetingService.delegate = nil
            meetingService.leaveMeeting(with: .leave)
            self.leaved = true
        }
    }
    
    private func openError() {
        guard self.leaved, !self.joined else { return }
        _ = ViewControllerProvider.setupStreamErrorViewController(rootViewController: self)
        self.nextController?.previousController = self.previousController
        self.pushNextController(animated: true)
    }
}

extension StreamViewController: MobileRTCWebinarServiceDelegate {
    func onSinkJoinWebinarNeedUserNameAndEmail(completion: @escaping (String, String, Bool) -> Bool) {
        let name = self.session?.userName.fullName ?? ""
        let email = self.session?.email ?? ""
        _ = completion(name, email, false)
    }
    
    func onSinkQAConnectStarted() {}
    
    func onSinkQAConnected(_ connected: Bool) {}
    
    func onSinkQAOpenQuestionChanged(_ count: Int) {}
    
    func onSinkQAAddQuestion(_ questionID: String, success: Bool) {}
    
    func onSinkQAAddAnswer(_ answerID: String, success: Bool) {}
    
    func onSinkQuestionMarked(asDismissed questionID: String) {}
    
    func onSinkReopenQuestion(_ questionID: String) {}
    
    func onSinkReceiveQuestion(_ questionID: String) {}
    
    func onSinkReceiveAnswer(_ answerID: String) {}
    
    func onSinkUserLivingReply(_ questionID: String) {}
    
    func onSinkUserEndLiving(_ questionID: String) {}
    
    func onSinkVoteupQuestion(_ questionID: String, orderChanged: Bool) {}
    
    func onSinkRevokeVoteupQuestion(_ questionID: String, orderChanged: Bool) {}
    
    func onSinkDeleteQuestion(_ questionIDArray: [Any]) {}
    
    func onSinkDeleteAnswer(_ answerIDArray: [Any]) {}
    
    func onSinkQAAllowAskQuestionAnonymouslyNotification(_ beAllowed: Bool) {}
    
    func onSinkQAAllowAttendeeViewAllQuestionNotification(_ beAllowed: Bool) {}
    
    func onSinkQAAllowAttendeeUpVoteQuestionNotification(_ beAllowed: Bool) {}
    
    func onSinkQAAllowAttendeeAnswerQuestionNotification(_ beAllowed: Bool) {}
    
    func onSinkWebinarNeedRegister(_ registerURL: String) {}
    
    func onSinkPanelistCapacityExceed() {}
    
    func onSinkPromptAttendee2PanelistResult(_ errorCode: MobileRTCWebinarPromoteorDepromoteError) {}
    
    func onSinkDePromptPanelist2AttendeeResult(_ errorCode: MobileRTCWebinarPromoteorDepromoteError) {}
    
    func onSinkAllowAttendeeChatNotification(_ currentPrivilege: MobileRTCChatAllowAttendeeChat) {}
    
    func onSinkSelfAllowTalkNotification() {}
    
    func onSinkSelfDisallowTalkNotification() {}
}



