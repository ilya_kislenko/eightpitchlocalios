//
//  SettingsInfoViewController.swift
//  8Pitch
//
//  Created by 8pitch on 27.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class SettingsInfoViewModel: GenericViewModel {}

class SettingsInfoViewController: GenericViewController, SheetDetentPresentationSupport {
        
    var contentHeight: CGFloat {
        let size = self.infoLabel.sizeThatFits(self.infoLabel.frame.size)
        return self.infoLabel.frame.origin.y + size.height + Constants.inset
    }
    
    private lazy var viewModel : SettingsInfoViewModel = {
        SettingsInfoViewModel(session: session)
    }()

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: InfoTitleSemiboldLabel!
    @IBOutlet weak var infoLabel: InfoLabel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.modalPresentationStyle = .custom
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = Constants.cornerRadius
        self.imageView.image = self.viewModel.isSecondLevelAccount || self.viewModel.isQuestionaryPassed ? UIImage(.secondLevel) : UIImage(.firstLevel)
        self.titleLabel.text = self.viewModel.isSecondLevelAccount ? "profile.settings.levelTitle.second".localized : self.viewModel.isQuestionaryPassed ? "profile.settings.levelTitle.second.in.progress".localized : "profile.settings.levelTitle.first".localized
        self.infoLabel.text = self.viewModel.isSecondLevelAccount ? "profile.settings.levelInfo.second".localized : self.viewModel.isQuestionaryPassed ? "profile.settings.levelInfo.second.in.progress".localized : "profile.settings.levelInfo.first".localized
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.close))
            swipeDown.direction = .down
            self.view.addGestureRecognizer(swipeDown)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layer.cornerRadius = Constants.cornerRadius
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (self.transitioningDelegate as? CustomTitleViewController)?.removeBlurEffect()
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private enum Constants {
        static let cornerRadius: CGFloat = 10
        static let inset: CGFloat = 100
    }
}
