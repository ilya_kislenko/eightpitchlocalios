//
//  ProfileViewController.swift
//  8Pitch
//
//  Created by 8pitch on 01.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class ProfileViewController: GenericViewController {
        
    private lazy var viewModel : ProfileViewModel = {
        guard let session = self.session else {
            fatalError("wrong init sequence!")
        }
        let retVal = ProfileViewModel(session: self.session)
        return retVal
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUserData()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

}

// MARK: Private

private extension ProfileViewController {
    
    func logoutButtonTapped() {
        let appDelegate = UIApplication.shared.delegate
        appDelegate?.window??.rootViewController = ViewControllerProvider.setupStartViewController()
        appDelegate?.window??.makeKeyAndVisible()
    }
    
    func registerCells() {
        self.tableView.register(ProfileCell.nibForRegistration, forCellReuseIdentifier: ProfileCell.reuseIdentifier)
        self.tableView.register(ProfileNameCell.nibForRegistration, forCellReuseIdentifier: ProfileNameCell.reuseIdentifier)
        self.tableView.register(ProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: ProfileHeaderView.reuseIdentifier)
    }
    
    enum Constants {
        static let headerHeight: CGFloat = 141
        static let emptyHeaderHeight: CGFloat = 40
        static let cellHeight: CGFloat = 48
    }
    
    func upgradeAccountTapped() {
        self.checkAccountLevel()
    }
    
    func checkAccountLevel() {
        switch self.session?.level {
        case .first:
            _ = ViewControllerProvider.setupVerifyEmailViewController(rootViewController: self)
        case .emailConfirmed:
            _ = ViewControllerProvider.setupPersonalViewController(rootViewController: self)
        case .KYCPassed:
            _ = ViewControllerProvider.setupWebIDViewController(rootViewController: self)
        case .WEBIDPassed:
            _ = ViewControllerProvider.setupQuestionnaireExplanationViewController(rootViewController: self)
        default:
            return
        }
        self.tabBarController?.hidesBottomBarWhenPushed = true
        self.pushNextController(animated: true)
    }
    
    func updateUserData() {
        guard let session = self.session else { return }
        self.startProgress()
        self.viewModel.user(session) { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success( _):
                self?.tableView.reloadData()
                return
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
                return
            default:
                return
            }
        }
    }
    
    func openSettingsScreen() {
        _ = ViewControllerProvider.setupSettingsViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}

// MARK: UITableViewDelegate, UITableViewDataSource

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileNameCell.reuseIdentifier, for: indexPath) as? ProfileNameCell else { fatalError("\(ProfileNameCell.reuseIdentifier) is not exist") }
            cell.setupFor(name: viewModel.userName)
            cell.selectionStyle = .none
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCell.reuseIdentifier, for: indexPath) as? ProfileCell else { fatalError("\(ProfileCell.reuseIdentifier) is not exist") }
            cell.setupFor(parameter: viewModel.parameterFor(indexPath))
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 && !self.viewModel.isSecondAccountLevel {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: ProfileHeaderView.reuseIdentifier) as? ProfileHeaderView
            view?.upgradeAction = { [weak self] in
                self?.upgradeAccountTapped()
            }
            view?.contentView.backgroundColor = UIColor(.white)
            return view
        } else {
            let view = UITableViewHeaderFooterView()
            view.tintColor = UIColor(.white)
            let topSeparator = SeparatorView()
            view.addSubview(topSeparator)
            topSeparator.snp.makeConstraints {
                $0.top.trailing.leading.equalToSuperview()
            }
            let bottomSeparator = SeparatorView()
            view.addSubview(bottomSeparator)
            bottomSeparator.snp.makeConstraints {
                $0.bottom.trailing.leading.equalToSuperview()
            }
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 && !self.viewModel.isSecondAccountLevel {
            return Constants.headerHeight
        } else if section == 0 {
            return 0
        } else {
            return Constants.emptyHeaderHeight
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let parameter = viewModel.parameterFor(indexPath)
        switch parameter {
        case .logout:
            self.logoutButtonTapped()
        case .settings:
            self.openSettingsScreen()
        default:
            return
        }
    }
    
}
