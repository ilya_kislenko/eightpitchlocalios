//
//  ProfileViewModel.swift
//  8Pitch
//
//  Created by Volha Bychok on 15.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation
import Input
import Remote

class ProfileViewModel: GenericViewModel {
    
    public var userName: String? {
        self.session?.userName.fullName
    }
    
    let numberOfSections: Int = Constants.numberOfSections
    
    func parameterFor(_ indexPath: IndexPath) -> ProfileCell.Parameter {
        switch indexPath.section {
        case 1:
            switch indexPath.row {
            case 0:
                return self.isSecondAccountLevel ? .payments : .security
            case 1:
                return self.isSecondAccountLevel ? .security : .settings
            default:
                return .settings
            }
        default:
            switch indexPath.row {
            case 0:
                return .help
            case 1:
                return .legal
            default:
                return .logout
            }
        }
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 2:
            return 3
        default:
            return self.isSecondAccountLevel ? 3 : 2
        }
    }
    
    
    
}

private extension ProfileViewModel {
    
    enum Constants {
        static let numberOfSections: Int = 3
    }
}
