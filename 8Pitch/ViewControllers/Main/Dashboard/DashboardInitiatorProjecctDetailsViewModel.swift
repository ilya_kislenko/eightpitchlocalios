//
//  DashboardInitiatorProjecctDetailsViewModel.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import UIKit

fileprivate struct Constants {
    static let numberOfRowsInInitiatorScreen = 6
    static let numberOfRowsInInitiatorScreenForPad = 3
}

class DashboardInitiatorProjecctDetailsViewModel: GenericViewModel {
    func numberOfRowsInSection(section: Int) -> Int {
        UIDevice.current.userInterfaceIdiom == .pad ? Constants.numberOfRowsInInitiatorScreenForPad : Constants.numberOfRowsInInitiatorScreen
    }

    var projectTitle: String? {
        return self.session.projectsProvider.currentInitiatorsProject?.companyName
    }

    var project: InitiatorsProject? {
        return self.session.projectsProvider.currentInitiatorsProject
    }
}
