//
//  DashboardViewModel.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

fileprivate struct Constants {
    static let greetingText = "dashboard.greeting".localized
    static let runningProjectsTypes = Set([ProjectStatus.notStarted, ProjectStatus.comingSoon, ProjectStatus.active, ProjectStatus.manualRelease, ProjectStatus.finished, ProjectStatus.waitRelease])

    static let finishedProjectsTypes = Set([ProjectStatus.refunded, ProjectStatus.tokensTransferred, ProjectStatus.waitBlockchain])
}

class DashboardViewModel: GenericViewModel {
    var greeting: String {
        return "\(Constants.greetingText) \(self.session.userName.prefix ?? "") \(self.session.userName.lastName ?? "")!"
    }

    //TODO: generalize runningProjecs and finishedProjecs
    var runningProjects: [ProjectInfo?] {
        if self.isInitiator {
            return self.session.projectsProvider.initiatorsProjects.filter { (project) -> Bool in
                guard let status = project.projectStatus else { return false }
                return Constants.runningProjectsTypes.contains(status)}
        }
        return self.session.projectsProvider.investorsProjects.filter { (project) -> Bool in
            guard let status = project.projectStatus else { return false }
            return Constants.runningProjectsTypes.contains(status)}
    }

    var finishedProjects: [ProjectInfo?] {
        if self.isInitiator {
            return self.session.projectsProvider.initiatorsProjects.filter { (project) -> Bool in
                guard let status = project.projectStatus else { return false }
                return Constants.finishedProjectsTypes.contains(status)}
        }
        return self.session.projectsProvider.investorsProjects.filter { (project) -> Bool in
            guard let status = project.projectStatus else { return false }
            return Constants.finishedProjectsTypes.contains(status)}
    }
    
    var noInvestments: Bool {
        self.session.projectsProvider.investorsPaymentsGraph?.totalMonetaryAmount == 0
    }
    
    func loadSliderData(completionHandler: @escaping ErrorClosure) {
        self.remote.linkSlider(session) { error in
                completionHandler(error)
        }
    }

    func loadProjectData(projectID: Int?, completionHandler: @escaping (Error?) -> Void) {
        guard let projectID = projectID else {
            completionHandler(nil)
            return
        }
        self.remote.project(self.session, projectID) {[weak self] project, error in
            self?.session.projectsProvider.expandedProject = project
            completionHandler(error)
        }
    }

    func loadData(completion: @escaping (Result<Bool, Error>) -> Void) {
        self.user { (_) in
            if self.isInitiator {
                self.initiatorProjects(completion: completion)
            } else {
                self.investorsGraph(completion: completion)
            }
        }
    }

    private func initiatorProjects(completion: @escaping (Result<Bool, Error>) -> Void) {
        self.remote.initiatorsProjects(self.session) { (error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.success(self.session.projectsProvider.initiatorsProjects.count != 0))
                }
            }
        }
    }
    
    private func investorsGraph(completion: @escaping (Result<Bool, Error>) -> Void) {
        self.remote.investorsGraph(session, completionHandler: {error in
            guard let error = error else {
                self.investorsProjects(completion: completion)
                return
            }
            completion(.failure(error))
        })
    }

    private func investorsProjects(completion: @escaping (Result<Bool, Error>) -> Void) {
        self.remote.investorsProjects(self.session) { (error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.success(self.session.projectsProvider.investorsProjects.count != 0))
                }
            }
        }
    }

    func openDetailsFor(projectID: String?) {
        self.session.projectsProvider.currentInitiatorsProject = self.session.projectsProvider.initiatorsProjects.first(where: {$0.identifier == projectID})
    }

    func numberOfSections() -> Int {
        if self.runningProjects.isEmpty && self.finishedProjects.isEmpty {
            return 0
        }
        return 4
    }

    func numberOfRowsInSection(_ sectionNumber: Int) -> Int {
        return 1
    }
}
