//
//  DashboardViewController.swift
//  8Pitch
//
//  Created by 8Pitch on 10/1/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

enum ProjectStatusType: Int {
    case running
    case finished
}

class DashboardViewController: CustomTitleViewController {
    @IBOutlet var greetingLabel: CustomSubtitleDarkLabel!
    @IBOutlet var infoLabel: HintLargeLabel!
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var actionButton: RoundedButton!
    @IBOutlet var descriptionAsterix: UILabel!
    @IBOutlet var descriptionInfo: InvestmentErrorInfoLabel!
    @IBOutlet var contentTableView: UITableView!
    
    private lazy var viewModel: DashboardViewModel = {
        DashboardViewModel(session: session)
    }()
    
    private var statusBarStyle: UIStatusBarStyle = .lightContent
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        self.statusBarStyle
    }

    private var projectStatusType: ProjectStatusType = .running {
        didSet {
            self.startProgress()
            CATransaction.begin()
            CATransaction.setCompletionBlock({
                self.stopProgress()
            })
            self.contentTableView.reloadRows(at: [IndexPath(row: 0, section: 3)], with: .none)
            CATransaction.commit()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initiallSetup()
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideViews()
        self.setupNavigationBarColor()
        self.loadDashboardData()
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func setupUIForIpad() {
        super.setupUIForIpad()
        let width = UIScreen.main.bounds.width
        self.actionButton.snp.makeConstraints {
            $0.width.equalTo(width * Constants.buttonWidthShare)
        }
    }
    
    override func setupNavigationBarColor() {
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor(.white)
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.barStyle = .black
        self.setNeedsStatusBarAppearanceUpdate()
        let style = NSMutableParagraphStyle()
        style.lineHeightMultiple = 1
        self.navigationController?.navigationBar.titleTextAttributes = [.font: UIFont.barlow.semibold.titleSize,
                                                                        .foregroundColor: UIColor(.white),
                                                                        .paragraphStyle: style,
                                                                        .kern: 0.3]
    }
    
    @objc override func showInfo() {
        let _ = ViewControllerProvider.setupInfoViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    @IBAction func actionHandle(_ sender: Any) {
        if self.viewModel.isInitiator {
            self.openWebScreenWithPath(Constants.loginPath)
        } else if self.viewModel.isSecondLevelAccount {
            self.tabBarController?.selectedIndex = 0
        } else {
            self.upgradeAccountTapped()
        }
    }
    
    private func loadData() {
        self.viewModel.loadData { [weak self] (response) in
            guard let self = self else { return }
            switch response {
            case .success(let hasProjects):
                if hasProjects {
                    self.contentTableView.reloadData()
                    self.contentTableView.isHidden = false
                    self.stopProgress()
                } else {
                    self.showViews()
                    self.setupEmptyProjectsContent()
                    self.stopProgress()
                }
                break
            case.failure(let error):
                self.stopProgress()
                self.presentAlert(message: error.localizedDescription)
            }
        }
    }
    
    private func loadDashboardData() {
        self.startProgress()
        self.viewModel.loadSliderData { [weak self] error in
            guard let error = error else {
                self?.loadData()
                return
            }
            self?.stopProgress()
            self?.presentAlert(message: error.localizedDescription)
        }
    }
}

extension DashboardViewController {
    private func registerCells() {
        self.contentTableView.register(GreetingTableViewCell.nibForRegistration, forCellReuseIdentifier: GreetingTableViewCell.reuseIdentifier)
        self.contentTableView.register(InitiatorProjectsTitleTableViewCell.nibForRegistration, forCellReuseIdentifier: InitiatorProjectsTitleTableViewCell.reuseIdentifier)
        self.contentTableView.register(InvestmentStatisticOverviewTableViewCell.nibForRegistration, forCellReuseIdentifier: InvestmentStatisticOverviewTableViewCell.reuseIdentifier)
        self.contentTableView.register(ProjectsListTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectsListTableViewCell.reuseIdentifier)
        self.contentTableView.register(VotingButtonCell.nibForRegistration, forCellReuseIdentifier: VotingButtonCell.reuseIdentifier)
    }

    private func initiallSetup() {
        self.hideViews()
        self.setupNavigationBar()
        self.contentTableView.contentInsetAdjustmentBehavior = .never
        self.greetingLabel.text = self.viewModel.greeting
        self.setupEmptyProjectsContent()
    }
    
    private func hideViews() {
        self.infoLabel.isHidden = true
        self.actionButton.isHidden = true
        self.descriptionAsterix.isHidden = true
        self.descriptionInfo.isHidden = true
        self.contentTableView.isHidden = true
    }
    
    private func setupEmptyProjectsContent() {
        self.contentTableView.isHidden = true
        self.backgroundImageView.image = self.viewModel.isInitiator ? UIImage(.dashboardInitiatorBackground) : UIImage(.dashboardInvestorBackground)
        if self.viewModel.isInitiator {
            self.descriptionAsterix.isHidden = false
            self.descriptionInfo.isHidden = false
            self.infoLabel.text = Constants.infoTextInitiator
            self.actionButton.setTitle(Constants.actionButtonInitiator, for: .normal)
            self.descriptionInfo.text = Constants.infoTextInitiatorDescription
        } else {
            self.infoLabel.text = Constants.infoTextInvestor
            var title: String?
            switch self.session?.level {
            case .questionaryPassed:
                self.actionButton.isHidden = true
            case .second:
                title = Constants.actionButtonTextLVLTwo
            default:
                title = Constants.actionButtonTextLVLOne
            }
            self.actionButton.setTitle(title, for: .normal)
        }
    }

    private func showViews() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.info), style: .plain, target: self, action: #selector(self.showInfo))
        self.infoLabel.isHidden = false
        self.actionButton.isHidden = false
        if self.viewModel.isInitiator {
            self.descriptionAsterix.isHidden = false
            self.descriptionInfo.isHidden = false
        }
    }
    
    private func setupNavigationBar() {
        self.navigationItem.title = self.viewModel.isInitiator ? Constants.navigationTitleInitiator : Constants.navigationTitleInvestor
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
    }
}

extension DashboardViewController: UITableViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let greetingCellHidden = scrollView.contentOffset.y > Constants.greetingHeight
        self.navigationController?.setNavigationBarHidden(scrollView.contentOffset.y > 40, animated: true)
        self.statusBarStyle = greetingCellHidden ? .default : .lightContent
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return Constants.greetingHeight
        case 1:
            return Constants.votingCellHeight
        default:
            return UITableView.automaticDimension
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: .zero, y: .zero, width: self.view.frame.width, height: Constants.firstHeaderHeight))
        headerView.backgroundColor  = UIColor(.white)
        if self.viewModel.isInitiator {
            switch section {
            case 3:
                return self.segmentedHeaderSetup(headerView: headerView)
            default:
                return nil
            }
        } else {
            switch section {
            case 2:
                return headerView
            case 3:
                return self.segmentedHeaderSetup(headerView: headerView)
            default:
                return nil
            }
        }
    }

    private func segmentedHeaderSetup(headerView: UIView) -> UIView {
        if self.viewModel.runningProjects.isEmpty || self.viewModel.finishedProjects.isEmpty {
            return headerView
        } else {
            let headerView: ProjectsListSectionHeader = .fromNib()
            headerView.delegate = self
            headerView.segmentedControl.selectedSegmentIndex = self.projectStatusType.rawValue
            return headerView
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.viewModel.isInitiator {
            switch section {
            case 3:
                return self.segmentedHeaderHeight()
            default:
                return .zero
            }
        } else {
            switch section {
            case 2:
                return Constants.firstHeaderHeight
            case 3:
                return self.segmentedHeaderHeight()
            default:
                return .zero
            }
        }
    }

    private func segmentedHeaderHeight() -> CGFloat {
        if self.viewModel.runningProjects.isEmpty || self.viewModel.finishedProjects.isEmpty {
            return Constants.firstHeaderHeight
        } else {
            return self.topbarHeight() + Constants.segmentedControllHeight
        }
    }
}

extension DashboardViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsInSection(section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section) {
        case 0:
            let cell = self.contentTableView.dequeueReusableCell(withIdentifier: GreetingTableViewCell.reuseIdentifier, for: indexPath) as! GreetingTableViewCell
            cell.delegate = self
            cell.setup(sliderContent: self.session?.projectsProvider.sliderContent ?? [], isInitiator: self.viewModel.isInitiator, greeting: self.viewModel.greeting)
            return cell
        case 1:
            let cell = self.contentTableView.dequeueReusableCell(withIdentifier: VotingButtonCell.reuseIdentifier, for: indexPath) as! VotingButtonCell
            cell.setup(for: self.session?.accountGroup ?? .privateInvestor, delegate: self)
            return cell
        case 2:
            if self.viewModel.isInitiator {
                let cell = self.contentTableView.dequeueReusableCell(withIdentifier: InitiatorProjectsTitleTableViewCell.reuseIdentifier, for: indexPath) as! InitiatorProjectsTitleTableViewCell
                cell.titleTextLabel.text = Constants.initiatorTitleTextLabel
                return cell
            } else {
                let cell = self.contentTableView.dequeueReusableCell(withIdentifier: InvestmentStatisticOverviewTableViewCell.reuseIdentifier, for: indexPath) as! InvestmentStatisticOverviewTableViewCell
                cell.setup(paymentGraph: self.session?.projectsProvider.investorsPaymentsGraph)
                return cell
            }
        case 3:
            return self.setupProjectsListTableViewCell(indexPath: indexPath)
        default:
            return UITableViewCell()
        }
    }

    private func setupProjectsListTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = self.contentTableView.dequeueReusableCell(withIdentifier: ProjectsListTableViewCell.reuseIdentifier, for: indexPath) as! ProjectsListTableViewCell
        if self.viewModel.runningProjects.isEmpty || self.viewModel.finishedProjects.isEmpty {
            self.projectStatusType = self.viewModel.runningProjects.isEmpty ? .finished : .running
        }
        switch self.projectStatusType {
        case .running:
            cell.setup(isInitiator: self.viewModel.isInitiator, projects: self.viewModel.runningProjects)
        case .finished:
            cell.setup(isInitiator: self.viewModel.isInitiator, projects: self.viewModel.finishedProjects)
        }
        cell.delegate = self
        return cell
    }

    func loadProjectDetails(identifier: Int?) {
        self.startProgress()
        self.viewModel.loadProjectData(projectID: identifier) {[weak self] (error) in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                if let project = self?.session?.projectsProvider.currentProjects.first(where: { $0.identifier == identifier } ) {
                    self?.session?.projectsProvider.currentProject = project
                } else if let project = self?.session?.projectsProvider.completedProjects.first(where: { $0.identifier == identifier } ) {
                    self?.session?.projectsProvider.currentProject = project
                }
                self?.openDetails()
            }
        }
    }

    func openDetails() {
        if let projectDetailsRootViewController = ViewControllerProvider.setupProjectDetailsRootViewController(rootViewController: self) {
            self.navigationController?.pushViewController(projectDetailsRootViewController, animated: true)
        }
    }
    
    private func openVotingsList() {
        _ = ViewControllerProvider.setupVotingsForProjectsViewController(rootViewController: self)
        self.nextController?.hidesBottomBarWhenPushed = true
        self.pushNextController(animated: true)
    }
}

extension DashboardViewController: ProjectsListSectionHeaderDelegate {
    func didSelect(type: ProjectStatusType) {
        self.projectStatusType = type
    }
}

extension DashboardViewController: ProjectCollectionViewCellDelegate {
    func didSelectProject(identifier: Int?) {
        self.loadProjectDetails(identifier: identifier)
    }

    func showMoreInfo(identifier: String?) {
        self.viewModel.openDetailsFor(projectID: identifier)
        let _ = ViewControllerProvider.setupDashboardInitiatorProjecctDetailsViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}

extension DashboardViewController: LoadProjectDetailsImageDelegate {
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView, completionHandler: VoidClosure?) {
        self.viewModel.loadProjectDetailsImageData(ID, imageView)
    }
    
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView) {
        self.viewModel.loadProjectDetailsImageData(ID, imageView)
    }
}

extension DashboardViewController: OpenVotingFlowDelegate {
    func openVoting() {
        self.startProgress()
        self.viewModel.votings(completionHandler: { [weak self] error in
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.openVotingsList()
            }
        })
    }
}

fileprivate struct Constants {
    static let navigationTitleInvestor = "dashboard.title.investor".localized
    static let navigationTitleInitiator = "dashboard.title.initiator".localized
    static let infoTextInvestor = "dashboard.info.investor".localized
    static let infoTextInitiator = "dashboard.info.initiator".localized
    static let infoTextInitiatorDescription = "dashboard.info.initiatorDescription".localized
    static let actionButtonTextLVLOne = "dashboard.actionUpggradeProfile".localized
    static let actionButtonTextLVLTwo = "dashboard.actionOpportunities".localized
    static let actionButtonInitiator = "dashboard.actionCreateProject".localized
    static let initiatorTitleTextLabel = "dashboard.title.initiatorSecction".localized
    static let loginPath = "/login"
    static let segmentedControllHeight = CGFloat(15)
    static let buttonWidthShare = CGFloat(0.7)
    static let greetingHeight = CGFloat(241)
    static let firstHeaderHeight = CGFloat(27)
    static let votingCellHeight = CGFloat(48)
}

