//
//  DashboardInitiatorProjecctDetailsViewController.swift
//  8Pitch
//
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

fileprivate struct Constants {
    static let title = "dashboard.initiators.statistics.navigationtitle".localized
    static let eurSymbol = "currency.eur.symbol".localized
    static let capitalInvestment = "dashboard.initiators.statistics.capitalInvestment".localized
    static let timeleft = "dashboard.initiators.statistics.timeleft".localized
    static let averageInvestment = "dashboard.initiators.statistics.averageInvestment".localized
    static let investors = "dashboard.initiators.statistics.investors".localized
    static let highest = "dashboard.initiators.statistics.highest".localized
    static let lowest = "dashboard.initiators.statistics.lowest".localized
    static let headerHeight = CGFloat(186)
    static let topPadInset = CGFloat(31)
    static let sidePadInset = CGFloat(20)
}

class DashboardInitiatorProjecctDetailsViewController: CustomTitleViewController {
    @IBOutlet weak var topInset: NSLayoutConstraint!
    @IBOutlet weak var leftInset: NSLayoutConstraint!
    @IBOutlet weak var rightInset: NSLayoutConstraint!
    @IBOutlet var contentTableView: UITableView!
    
    private lazy var viewModel: DashboardInitiatorProjecctDetailsViewModel = {
        DashboardInitiatorProjecctDetailsViewModel(session: session)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [.font: UIFont.barlow.regular.titleSize, .foregroundColor: UIColor(.gray)]
        self.title = viewModel.projectTitle
        self.customTitle = Constants.title
        self.navigationItem.rightBarButtonItem = nil
        self.registerCells()
    }
    
    override func setupUIForIpad() {
        super.setupUIForIpad()
        self.topInset.constant = Constants.topPadInset
        self.leftInset.constant = Constants.sidePadInset
        self.rightInset.constant = Constants.sidePadInset
    }
    
    private func registerCells() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.contentTableView.register(InitiatorStatisticsPadTableViewCell.nibForRegistration, forCellReuseIdentifier: InitiatorStatisticsPadTableViewCell.reuseIdentifier)
            self.contentTableView.separatorStyle = .none
        } else {
            self.contentTableView.register(InitiatorStatisticsTableViewCell.nibForRegistration, forCellReuseIdentifier: InitiatorStatisticsTableViewCell.reuseIdentifier)
        }
    }
}

extension DashboardInitiatorProjecctDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: InitiatorStatisticsSectionHeader = .fromNib()
        headerView.setup(project: self.viewModel.project)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.headerHeight
    }
}

extension DashboardInitiatorProjecctDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: InitiatorStatisticsTableViewCell = UIDevice.current.userInterfaceIdiom == .pad ?
            self.contentTableView.dequeueReusableCell(withIdentifier: InitiatorStatisticsPadTableViewCell.reuseIdentifier, for: indexPath) as! InitiatorStatisticsPadTableViewCell :
            self.contentTableView.dequeueReusableCell(withIdentifier: InitiatorStatisticsTableViewCell.reuseIdentifier, for: indexPath) as! InitiatorStatisticsTableViewCell
        let images = [UIImage(.euro), UIImage(.clock), UIImage(.averageInvestment), UIImage(.numberOfInvestors), UIImage(.hight), UIImage(.low)]
        let titles = [Constants.capitalInvestment, Constants.timeleft, Constants.averageInvestment, Constants.investors, Constants.highest, Constants.lowest]
        let content = ["\(Constants.eurSymbol)\(self.viewModel.project?.projectBI?.capitalInvested?.currency ?? "0")",
                       self.viewModel.project?.tokenParametersDocument?.dsoProjectFinishDate?.endDateString(),
                       "\(Constants.eurSymbol)\(self.viewModel.project?.projectBI?.averageInvestment?.currency ?? "0")",
                       "\(self.viewModel.project?.projectBI?.investors?.totalCount ?? 0)",
                       "\(Constants.eurSymbol)\(self.viewModel.project?.projectBI?.maxInvestment?.currency ?? "0")",
                       "\(Constants.eurSymbol)\(self.viewModel.project?.projectBI?.minInvestment?.currency ?? "0")"]
        let rowsCount = self.viewModel.numberOfRowsInSection(section: indexPath.section)
        if UIDevice.current.userInterfaceIdiom == .pad {
            cell.setupForPad(icons: [images[indexPath.row], images[indexPath.row + rowsCount]],
                             titles: [titles[indexPath.row], titles[indexPath.row + rowsCount]],
                             content: [content[indexPath.row], content[indexPath.row + rowsCount]])
        } else {
            cell.setup(icon: images[indexPath.row], title: titles[indexPath.row], content: content[indexPath.row])
        }
        return cell
    }
}
