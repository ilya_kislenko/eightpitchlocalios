//
//  QuickLoginViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 11/18/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class QuickLoginViewModel: GenericViewModel {
    
    var numberOfSections: Int {
        1
    }
    
    var isBiometryEnabled: Bool {
        BiometryManager.shared.isBiometryEnabled
    }
    
    var isPINEnabled: Bool {
        BiometryManager.shared.isPINEnabled
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        BiometryManager.shared.isBiometryEnrolled ? 2 : 1
    }
    
    func parameter(_ indexPath: IndexPath) -> ProfileSwitchCell.CellType {
        var parameters: [ProfileSwitchCell.CellType] = [.pinCode]
        let biometryType = BiometryManager.shared.biometryType
        switch biometryType {
        case .faceID:
            parameters.append(.faceID)
        case .touchID:
            parameters.append(.touchID)
        default:
            break
        }
        return parameters[indexPath.row]
    }
    
    func parameterEnabled(_ indexPath: IndexPath) -> Bool {
        let parameter = self.parameter(indexPath)
        switch parameter {
        case .pinCode:
            return isPINEnabled
        default:
            return isBiometryEnabled
        }
    }
}
