//
//  DeletedViewController.swift
//  8Pitch
//
//  Created by 8pitch on 20.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class DeletedViewModel: GenericViewModel {}

class DeletedViewController: CustomTitleViewController {
    
    private lazy var viewModel : DeletedViewModel = {
        DeletedViewModel(session: session)
    }()
    
    @IBOutlet weak var titleLabel: CustomSubtitleLabel!
    @IBOutlet weak var buyLabel: HintDarkLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = nil
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeTapped))
        self.navigationItem.rightBarButtonItem = nil
        self.titleLabel.text = self.viewModel.isSecondLevelAccount ? "profile.deletion.done.second".localized : "profile.deletion.done.first".localized
        self.buyLabel.text = self.viewModel.isSecondLevelAccount ? "profile.deletion.bye.second".localized : "profile.deletion.bye.first".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !self.viewModel.isSecondLevelAccount {
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) { [weak self] in
                self?.logout()
            }
        }
    }
    
    private func close() {
        if !self.viewModel.isSecondLevelAccount {
            self.logout()
        } else {
            self.popToPreviousController(animated: true)
        }
    }
    
    @objc private func closeTapped() {
        self.close()
    }
    
    @IBAction func okTapped(_ sender: UIButton) {
        self.close()
    }
    
}
