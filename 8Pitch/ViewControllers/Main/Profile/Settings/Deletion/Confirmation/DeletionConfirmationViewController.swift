//
//  DeletionConfirmationViewController.swift
//  8Pitch
//
//  Created by 8pitch on 20.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Remote
import RxSwift

class DeletionConfirmationViewModel: GenericViewModel {}

class DeletionConfirmationViewController: InputSMSViewController {
    
    private lazy var viewModel : DeletionConfirmationViewModel = {
        DeletionConfirmationViewModel(session: session)
    }()
    
    override var confirmationFooterViewType : PublishSubject<InputSMSView.ViewType?>? {
        didSet {
            guard let typeAction = self.confirmationFooterViewType else { return }
            typeAction.onNext(self.viewModel.isSecondLevelAccount ? .deletionSecond : .deletionFirst)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "profile.deletion.title".localized
        self.customTitle = "profile.deletion.confirm.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
    }

    override func nextButtonTapped() {
        self.deleteAccount()
    }
    
    private func deleteAccount() {
        guard let _ = session else { return }
        self.startProgress()
        self.viewModel.sendDeletionRequest { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success(_):
                self?.openSuccessScreen()
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
            }
        }
    }
    
    private func openSuccessScreen() {
        _ = ViewControllerProvider.setupDeletedViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}
