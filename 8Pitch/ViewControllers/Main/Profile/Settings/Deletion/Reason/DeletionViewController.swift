//
//  DeletionViewController.swift
//  8Pitch
//
//  Created by 8pitch on 19.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Remote

class DeletionViewController: CustomTitleViewController {
    
    @IBOutlet weak var sendRequestButton: StateButton!
    @IBOutlet weak var infoLabel: InfoLabel!
    
    private lazy var viewModel : DeletionViewModel = {
        DeletionViewModel(session: session)
    }()
    
    private var inputLabel: TextFieldHeaderLabel = {
        let label = TextFieldHeaderLabel()
        label.text = "profile.deletion.placeholder.second".localized
        return label
    }()
    
    private var pickerTextField: DeletionTextField = {
        DeletionTextField()
    }()
    
    private var reasonTextField: TextField = {
        let field = TextField()
        field.placeholder = "profile.deletion.placeholder.first".localized
        return field
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "profile.deletion.title".localized
        self.customTitle = "profile.deletion.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
        self.setupContent()
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @IBAction func sendRequestTapped(_ sender: UIButton) {
        guard self.isReasonSaved(), let _ = session else { return }
        self.startProgress()
        self.viewModel.sendRequest() { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success(_):
                self?.openNextScreen()
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
            }
        }
    }
    
}

private extension DeletionViewController {
    
    enum Constants {
        static let infoLabelTopOffset: CGFloat = 15
        static let sideInset: CGFloat = 24
        static let buttonButtomInset: CGFloat = -112
        static let textFieldTopOffset: CGFloat = 28
        static let textFieldBottomInset: CGFloat = -40
        static let textFieldHeight: CGFloat = 44
        static let reasonFieldTopOffset: CGFloat = 35
        static let reasonFieldBottomInset: CGFloat = -32
    }
    
    private func openNextScreen() {
        _ = self.viewModel.isSecondLevelAccount ? ViewControllerProvider.setupDeletedViewController(rootViewController: self) : ViewControllerProvider.setupDeletionConfirmationViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    @objc private func reasonChosen() {
        if self.pickerTextField.text == "profile.deletion.reason.5".localized {
            self.addTextField()
            self.sendRequestButton.isEnabled = false
        } else {
            self.sendRequestButton.isEnabled = !(self.pickerTextField.text?.isEmpty ?? true)
            self.removeTextField()
        }
    }
    
    @objc private func reasonEntered() {
        let reason = reasonTextField.superview == nil ? pickerTextField.text : reasonTextField.text
        self.sendRequestButton.isEnabled = !(reason?.isEmpty ?? true)
    }
    
    private func setupContent() {
        self.view.addSubview(self.inputLabel)
        self.view.addSubview(self.pickerTextField)
        self.pickerTextField.addTarget(self, action: #selector(reasonChosen), for: .allEditingEvents)
        self.inputLabel.snp.makeConstraints {
            $0.top.equalTo(self.infoLabel.snp.bottom).offset(Constants.infoLabelTopOffset)
            $0.leading.equalToSuperview().inset(Constants.sideInset)
        }
        self.pickerTextField.snp.makeConstraints {
            $0.top.equalTo(self.inputLabel.snp.bottom)
            $0.trailing.leading.equalToSuperview().inset(Constants.sideInset)
            $0.height.equalTo(Constants.textFieldHeight)
            $0.bottom.equalTo(sendRequestButton.snp.top).inset(Constants.buttonButtomInset).priority(.medium)
        }
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
        self.infoLabel.text = self.viewModel.isSecondLevelAccount ? "profile.deletion.info.second".localized : "profile.deletion.info.first".localized
    }
    
    private func addTextField() {
        self.view.addSubview(self.reasonTextField)
        self.reasonTextField.addTarget(self, action: #selector(reasonEntered), for: .allEvents)
        self.reasonTextField.snp.makeConstraints {
            $0.top.equalTo(self.pickerTextField.snp.bottom).offset(Constants.reasonFieldTopOffset)
            $0.trailing.leading.equalToSuperview().inset(Constants.sideInset)
            $0.height.equalTo(Constants.textFieldHeight)
            $0.bottom.equalTo(self.sendRequestButton.snp.top).inset(Constants.reasonFieldBottomInset).priority(.high)
        }
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
    }
    
    private func removeTextField() {
        UIView.animate(withDuration: 0.3) {
            self.reasonTextField.removeFromSuperview()
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        }
    }
    
    private func isReasonSaved() -> Bool {
        let reason = reasonTextField.superview == nil ? pickerTextField.text : reasonTextField.text
        if reason?.isEmpty ?? true {
            self.presentAlert(message: "profile.deletion.alert".localized)
            return false
        }
        self.session?.deletionReason = reason
        return true
    }
    
}
