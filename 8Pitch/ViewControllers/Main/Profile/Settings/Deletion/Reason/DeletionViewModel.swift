//
//  DeletionViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 19.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import RxSwift

class DeletionViewModel: GenericViewModel {
    
    func sendRequest(completionHandler: @escaping ResponseClosure) {
        if isSecondLevelAccount {
            self.sendDeletionRequest(completionHandler: completionHandler)
        } else {
            self.sendCodeRequest(completionHandler: completionHandler)
        }
        
    }
    
    private func sendCodeRequest(completionHandler: @escaping ResponseClosure) {
        self.remote.sendCode(self.session)
        .compactMap { $0 }
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { response in
            completionHandler(response)
        }).disposed(by: self.disposables)
    }

}
