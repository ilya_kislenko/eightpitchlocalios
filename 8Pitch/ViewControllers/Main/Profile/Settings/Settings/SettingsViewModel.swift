//
//  SettingsViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/18/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import UIKit

class SettingsViewModel: GenericViewModel {
    
    var numberOfSections: Int {
        self.isQuestionaryPassed ? Constants.sectionsForSecond : Constants.sectionsForFirst
    }
    
    var isUserSubscribed: Bool {
        self.session.subscribedToEmail
    }
    
    var userStatus: String {
        self.isSecondLevelAccount ? "profile.settings.level.second".localized : self.isQuestionaryPassed ? "profile.settings.level.second.in.progress".localized : "profile.settings.level.first".localized
    }
    
    var subscriptionText: String? {
        isUserSubscribed ? "profile.settings.subscription.info".localized : nil
    }
    
    var subscriptionUpdated: Bool {
        self.isUserSubscribed != (try? self.session.subscribeToEmailsProvider.value())
    }
    
    var userClass: String? {
        switch self.session.investorClass {
        case .unqualified:
            return "profile.settings.investorClass.unqualified".localized
        case .class1:
            return "profile.settings.investorClass.1-2".localized
        case .class2:
            return "profile.settings.investorClass.1-2".localized
        case .class3:
            return "profile.settings.investorClass.3".localized
        case .class4:
            return "profile.settings.investorClass.4".localized
        case .class5:
            return "profile.settings.investorClass.5".localized
        case .none:
            return nil
        }
    }
    
    func isUpgradeViewNeeded(for section: Int) -> Bool {
        return !self.isQuestionaryPassed && !self.isInitiator && section == 0
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        if (self.isQuestionaryPassed && section == 0 || !self.isQuestionaryPassed && section == 1) && self.userClass == nil {
            return Constants.rowsForSettings - 1
        }
        if self.isQuestionaryPassed && section == 0 || !self.isQuestionaryPassed && section == 1 {
            return Constants.rowsForSettings
        }
        if self.isQuestionaryPassed && section == 1 || !self.isQuestionaryPassed && section == 2  {
            return Constants.rowForDeletion
        }
        return 0
    }

    func subscribe(_ state: Bool, completionHandler: @escaping ErrorClosure) {
        self.session.subscribeToEmailsProvider.onNext(state)
        self.remote.updateUserSettingsData(session, completionHandler: completionHandler)
    }
    
    func isUserClassCell(_ indexPath: IndexPath) -> Bool {
        self.userClass != nil && indexPath.row == 1
    }
    
    func isProfileCell(_ indexPath: IndexPath) -> Bool {
        indexPath.section == self.numberOfSections - 1
    }
    
    func isSubscriptionCell(_ indexPath: IndexPath) -> Bool {
        indexPath.row == self.numberOfRowsInSection(indexPath.section) - 1
    }
    
    func isProfileHeaderView(_ section: Int) -> Bool {
        !self.isQuestionaryPassed && section == 0
    }
    
    func isPersonalProfileHeaderView(_ section: Int) -> Bool {
        (self.isQuestionaryPassed && section == 0) || (!self.isQuestionaryPassed && section == 1)
    }

}

private extension SettingsViewModel {
    
    enum Constants {
        static let sectionsForFirst: Int = 3
        static let sectionsForSecond: Int = 2
        static let rowsForSettings: Int = 3
        static let rowForDeletion: Int = 1
    }
}
