//
//  SettingsViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/18/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class SettingsViewController: CustomTitleViewController {
    
    private lazy var viewModel : SettingsViewModel = {
        SettingsViewModel(session: session)
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "profile.settings.title".localized
        self.customTitle = "profile.settings.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateContent(isToggleSwitched: false)
    }
    
    override func showInfo() {
        let _ = ViewControllerProvider.setupSettingsInfoViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.viewModel.isProfileCell(indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCell.reuseIdentifier, for: indexPath) as? ProfileCell {
            cell.setupFor(parameter: .delete)
            return cell
        } else if self.viewModel.isSubscriptionCell(indexPath),
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileSwitchCell.reuseIdentifier, for: indexPath) as? ProfileSwitchCell {
            cell.setupFor(.newsletters)
            cell.setState(self.viewModel.isUserSubscribed)
            cell.switchToggled = { [weak self] subscribed in
                self?.updateSubscription(subscribed)
            }
            return cell
        } else if let cell = tableView.dequeueReusableCell(withIdentifier: SummarizeCell.reuseIdentifier, for: indexPath) as? SummarizeCell {
            if indexPath.row == 0 {
            let data = SummaryData(image: UIImage(.profileSettings), key: "profile.settings.status".localized, value: self.viewModel.userStatus)
            cell.setupWith(data)
            } else {
                let data = SummaryData(image: UIImage(.briefcase), key: "profile.settings.title.investorClass".localized, value: self.viewModel.userClass)
                cell.setupWith(data)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.viewModel.isProfileHeaderView(section) {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: ProfileHeaderView.reuseIdentifier) as? ProfileHeaderView
            view?.upgradeAction = { [weak self] in
                self?.upgradeAccountTapped()
            }
            view?.hideSeparator()
            return view
        }
        if self.viewModel.isPersonalProfileHeaderView(section) {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: PersonalProfileHeaderView.reuseIdentifier) as? PersonalProfileHeaderView
            view?.setTitle("profile.settings.header".localized)
            return view
        }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: SettingsHeaderView.reuseIdentifier) as? SettingsHeaderView
        view?.setText(self.viewModel.subscriptionText ?? "")
        return view
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.viewModel.isUpgradeViewNeeded(for: section) {
            return Constants.upgradeHeaderHeight
        } else {
            return Constants.headerHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == self.viewModel.numberOfSections - 1 {
            self.openDeletionScreen()
        } else if indexPath.row == 0 {
            self.showInfo()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        self.viewModel.isUserClassCell(indexPath) ? Constants.cellHeight * 2 : Constants.cellHeight
    }
    
}

private extension SettingsViewController {
    
    func updateContent(isToggleSwitched: Bool) {
        self.startProgress()
        self.viewModel.user() { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success( _):
                if self?.viewModel.subscriptionUpdated ?? true && isToggleSwitched {
                    self?.presentAlert(title: "", message: "settings.subscription.alert.message".localized)
                }
                self?.tableView.reloadData()
                return
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
                return
            }
        }
    }
    
    enum Constants {
        static let headerHeight: CGFloat = 88
        static let upgradeHeaderHeight: CGFloat = 141
        static let cellHeight: CGFloat = 48
    }
    
    func updateSubscription(_ subscribed: Bool) {
        guard let _ = self.session else { return }
        self.startProgress()
        self.viewModel.subscribe(subscribed) { [weak self] error in
            // todo: move to remote
            DispatchQueue.main.async {
                self?.stopProgress()
                guard let error = error else {
                    self?.updateContent(isToggleSwitched: true)
                    return
                }
                self?.presentAlert(message: error.localizedDescription)
            }
        }
    }
    
    func registerCells() {
        self.tableView.register(ProfileCell.nibForRegistration, forCellReuseIdentifier: ProfileCell.reuseIdentifier)
        self.tableView.register(SummarizeCell.nibForRegistration, forCellReuseIdentifier: SummarizeCell.reuseIdentifier)
        self.tableView.register(ProfileSwitchCell.nibForRegistration, forCellReuseIdentifier: ProfileSwitchCell.reuseIdentifier)
        self.tableView.register(ProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: ProfileHeaderView.reuseIdentifier)
        self.tableView.register(SettingsHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: SettingsHeaderView.reuseIdentifier)
        self.tableView.register(PersonalProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: PersonalProfileHeaderView.reuseIdentifier)
    }
    
    func openDeletionScreen() {
        _ = ViewControllerProvider.setupDeletionViewController(rootViewController: self)
        self.nextController?.hidesBottomBarWhenPushed = true
        self.pushNextController(animated: true)
    }
}
