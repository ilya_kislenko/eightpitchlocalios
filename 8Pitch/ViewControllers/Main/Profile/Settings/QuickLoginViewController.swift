//
//  QuickLoginViewController.swift
//  8Pitch
//
//  Created by 8pitch on 11/18/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class QuickLoginViewController: CustomTitleViewController {
    
    private lazy var viewModel : QuickLoginViewModel = {
        QuickLoginViewModel(session: session)
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "security.quicklLogin.title".localized
        self.customTitle = "security.quicklLogin.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateContent()
    }
    
}

extension QuickLoginViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileSwitchCell.reuseIdentifier, for: indexPath) as? ProfileSwitchCell else {
            fatalError("TableView setup is impossible")
        }
        let parameter = self.viewModel.parameter(indexPath)
        if self.viewModel.numberOfRowsInSection(indexPath.section) == 1 {
            cell.hideSeparator()
        }
        cell.setupFor(parameter)
        cell.setState(self.viewModel.parameterEnabled(indexPath))
        cell.switchToggled = parameter == .pinCode ? {[weak self] enabled in self?.togglePIN(enabled)} : {[weak self] enabled in self?.toggleBiometry(enabled)}
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: SettingsHeaderView.reuseIdentifier) as? SettingsHeaderView
        view?.setText("security.quicklLogin.info".localized)
        view?.hideSeparator()
        let separator = SeparatorView()
        view?.addSubview(separator)
        separator.snp.makeConstraints {
            $0.top.trailing.leading.equalToSuperview()
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        Constants.headerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cellHeight
    }
}

private extension QuickLoginViewController {
    
    func updateContent() {
        self.startProgress()
        self.viewModel.user() { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success( _):
                self?.tableView.reloadData()
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
            }
        }
    }
    
    enum Constants {
        static let headerHeight: CGFloat = 88
        static let cellHeight: CGFloat = 48
    }
    
    func registerCells() {
        self.tableView.register(ProfileSwitchCell.nibForRegistration, forCellReuseIdentifier: ProfileSwitchCell.reuseIdentifier)
        self.tableView.register(SettingsHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: SettingsHeaderView.reuseIdentifier)
    }
    
    func togglePIN(_ enabled: Bool) {
        if enabled {
            self.openEnablePin()
        } else {
            self.openDisablePin()
        }
    }
    
    func toggleBiometry(_ enabled: Bool) {
        if enabled && !self.viewModel.isPINEnabled {
            self.openEnablePin(withBiometry: true)
        } else if enabled && self.viewModel.isPINEnabled {
            self.enableBiometry()
        } else {
            self.disableBiometry()
        }
    }
    
    func openEnablePin(withBiometry: Bool = false) {
        if withBiometry && !BiometryManager.shared.isBiometryEnrolled {
            self.presentAlert(title: "quickLogin.biometry.error.notEnrolled.title".localized,
                              message: "quickLogin.biometry.error.notEnrolled.text".localized,
                              actionMessage: "quickLogin.biometry.settings".localized,
                              addAction: self.openSettingsAction)
            self.tableView.reloadData()
            return
        }
        self.usePin(withBiometry: withBiometry)
    }
    
    func openDisablePin() {
        _ = ViewControllerProvider.setupCheckPinViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func disableBiometry() {
        BiometryManager.shared.isBiometryEnabled = false
        let title = BiometryManager.shared.biometryType == .touchID ? "security.quicklLogin.touchID.disabled.title".localized : "security.quicklLogin.faceID.disabled.title".localized
        let text = BiometryManager.shared.biometryType == .touchID ? "security.quicklLogin.touchID.disabled.text".localized : "security.quicklLogin.faceID.disabled.text".localized
        self.presentAlert(title: title, message: text)
    }
    
    func enableBiometry() {
        if !BiometryManager.shared.isBiometryEnrolled {
            self.presentAlert(title: "quickLogin.biometry.error.notEnrolled.title".localized,
                              message: "quickLogin.biometry.error.notEnrolled.text".localized,
                              actionMessage: "quickLogin.biometry.settings".localized,
                              addAction: self.openSettingsAction)
            self.tableView.reloadData()
            return
        }
        BiometryManager.shared.enableBiometry(completion: { [weak self] error in
            guard let error = error else {
                return
            }
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.presentAlert(message: error.localizedDescription)
            }
        })
    }
    
}
