//
//  EditProfileViewController.swift
//  8Pitch
//
//  Created by Volha Bychok on 9/16/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class EditProfileViewController: EditViewController {
    
    private lazy var viewModel : EditProfileViewModel = {
        guard let session = self.session else {
            fatalError("wrong init sequence!")
        }
        let retVal = EditProfileViewModel(session: self.session)
        return retVal
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.customTitle = "profile.personal.edit.customTitle".localized
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        var sections: [CollectionViewModel<KYC>] = []
        if self.viewModel.isInstitutionalInvestor {
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: CompanyError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: CompanyError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        }
        if self.viewModel.isSecondLevelAccount {
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: NameAndLastNameError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: LastNameError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: BirthDayError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: PersonalDetailsError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: CountryError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: LocalityError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: LocalityError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: AddressError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        } else {
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: NameAndLastNameError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: NameAndLastNameError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        }
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let model = self.dataSource?.models[indexPath.section] else {
            fatalError("invalid self.dataSource setup!")
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: model.reuseIdentifiers[indexPath.row], for: indexPath)

        if let modelCell = cell as? CellModel<KYC> {
            modelCell.content = model.content
            modelCell.indexPath = indexPath
            modelCell.model = model.model
        } else if let modelCell = cell as? ErrorCell {
            modelCell.textLabel.text = model.errorMessage
        }
        
        if let headerCell = cell as? EditHeaderCell {
            headerCell.setTitle(self.viewModel.editSectionsContent[indexPath.section].headerTitle)
        } else if let inputCell = cell as? EditCell {
            inputCell.setup(for: self.viewModel.editSectionsContent[indexPath.section].type ?? .name)
        }
        return cell
    }
    
    @objc override func nextButtonTapped() {
        // request { self.popToPreviousController(animated: true) }
    }

}
