//
//  EditProfileViewModel.swift
//  8Pitch
//
//  Created by Volha Bychok on 9/16/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class EditProfileViewModel: GenericViewModel {
    
    struct EditProfileSection {
        let headerTitle: String?
        let title: String?
        let type: EditCellType?
    }
    
    var editSectionsContent: [EditProfileSection] = []
    
    required init(session: Session?) {
        super.init(session: session)
        let editSections = super.sectionsContent.filter {
            $0.type != .upgrade && $0.type != .contact
        }
        for section in editSections {
            let header = EditProfileSection(headerTitle: section.headerTitle, title: nil, type: nil)
            self.editSectionsContent.append(header)
            section.rowsContent.forEach { self.editSectionsContent.append(EditProfileSection(headerTitle: nil, title: $0.title, type: $0.type))}
        }
    }
    
}
