//
//  ProfileViewController.swift
//  8Pitch
//
//  Created by 8pitch on 01.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Remote

class ProfileViewController: GenericViewController {
    
    private lazy var viewModel : ProfileViewModel = {
        ProfileViewModel(session: session)
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUserData()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func openPersonalScreen() {
        _ = ViewControllerProvider.setupPersonalProfileViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}

// MARK: Private

private extension ProfileViewController {
    
    func logoutButtonTapped() {
        self.logout()
    }
    
    func registerCells() {
        self.tableView.register(ProfileCell.nibForRegistration, forCellReuseIdentifier: ProfileCell.reuseIdentifier)
        self.tableView.register(ProfileNameCell.nibForRegistration, forCellReuseIdentifier: ProfileNameCell.reuseIdentifier)
        self.tableView.register(ProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: ProfileHeaderView.reuseIdentifier)
    }
    
    enum Constants {
        static let headerHeight: CGFloat = 141
        static let emptyHeaderHeight: CGFloat = 40
        static let cellHeight: CGFloat = 48
        static let faqPathPI: String = "/faq/issuers"
        static let faqPathInvestor: String = "/faq/investors"
        static let languageCodeDE = "de"
    }
    
    func openFAQScreen() {
        let path = self.viewModel.isInitiator ? Constants.faqPathPI : Constants.faqPathInvestor
        self.openWebScreenWithPath(path)
    }
    
    func openLegalScreen() {
        let link = Locale.current.languageCode == Constants.languageCodeDE ? WebURLConstants.policiesDE : WebURLConstants.policiesEN
        self.openWebScreenWithAbsolutPath(link)
    }
    
    func updateUserData() {
        self.startProgress()
        self.viewModel.user() { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success( _):
                self?.tableView.reloadData()
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
            }
        }
    }
    
    func loadPayments() {
        self.startProgress()
        self.viewModel.getHistory( { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.openPaymentsScreen()
                return
            }
            self?.presentAlert(message: error.localizedDescription)
        })
    }
    
    func openSettingsScreen() {
        _ = ViewControllerProvider.setupSettingsViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openPaymentsScreen() {
        _ = ViewControllerProvider.setupHistoryViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openSecurityScreen() {
        _ = ViewControllerProvider.setupSecurityViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}

// MARK: UITableViewDelegate, UITableViewDataSource

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileNameCell.reuseIdentifier, for: indexPath) as? ProfileNameCell else { return UITableViewCell()
            }
            cell.setupFor(name: viewModel.userName)
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCell.reuseIdentifier, for: indexPath) as? ProfileCell else {
                return UITableViewCell()
            }
            cell.setupFor(parameter: viewModel.parameterFor(indexPath))
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.viewModel.isUpgradeViewNeeded(for: section) {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: ProfileHeaderView.reuseIdentifier) as? ProfileHeaderView
            view?.upgradeAction = { [weak self] in
                self?.upgradeAccountTapped()
            }
            return view
        } else {
            let view = UITableViewHeaderFooterView()
            view.contentView.backgroundColor = UIColor(.white)
            if section != 1 {
                let topSeparator = SeparatorView()
                view.addSubview(topSeparator)
                topSeparator.snp.makeConstraints {
                    $0.top.trailing.leading.equalToSuperview()
                }
            }
            let bottomSeparator = SeparatorView()
            view.addSubview(bottomSeparator)
            bottomSeparator.snp.makeConstraints {
                $0.bottom.trailing.leading.equalToSuperview()
            }
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.viewModel.isUpgradeViewNeeded(for: section) {
            return Constants.headerHeight
        } else if section == 0 {
            return 0
        } else {
            return Constants.emptyHeaderHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let parameter = viewModel.parameterFor(indexPath)
        switch parameter {
        case .logout:
            self.logoutButtonTapped()
        case .personal:
            self.openPersonalScreen()
        case .settings:
            self.openSettingsScreen()
        case .help:
            self.openFAQScreen()
        case .legal:
            self.openLegalScreen()
        case .payments:
            self.loadPayments()
        case .security:
            self.openSecurityScreen()
        default:
            return
        }
    }
    
}
