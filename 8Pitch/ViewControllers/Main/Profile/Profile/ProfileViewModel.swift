//
//  ProfileViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 15.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Foundation
import Input
import Remote

class ProfileViewModel: GenericViewModel {
    
    var userName: String? {
        self.session.userName.fullName
    }
    
    var emptyPaymentsMessage: String? {
        self.payments.isEmpty && self.transfers.isEmpty ? "profile.payments.list.empty".localized : nil
    }
    
    let numberOfSections: Int = Constants.numberOfSections
    
    func parameterFor(_ indexPath: IndexPath) -> ProfileCell.Parameter {
        switch indexPath.section {
        case 0:
            return .personal
        case 1:
            switch indexPath.row {
            case 0:
                return self.isSecondLevelAccount ? .payments : .security
            case 1:
                return self.isSecondLevelAccount ? .security : .settings
            default:
                return .settings
            }
        default:
            switch indexPath.row {
            case 0:
                return .help
            case 1:
                return .legal
            default:
                return .logout
            }
        }
    }
    
    func isUpgradeViewNeeded(for section: Int) -> Bool {
        section == 1 && !self.isQuestionaryPassed && !self.isInitiator
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 2:
            return 3
        default:
            return self.isSecondLevelAccount ? 3 : 2
        }
    }
}

private extension ProfileViewModel {
    
    enum Constants {
        static let numberOfSections: Int = 3
    }
}
