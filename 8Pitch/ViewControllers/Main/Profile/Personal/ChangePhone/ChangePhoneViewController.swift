//
//  ChangePhoneViewController.swift
//  8Pitch
//
//  Created by 8pitch on 16.11.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input

class ChangePhoneViewController: InputPhoneViewController {
    
    override var navigationFooterViewType: PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupConfirmChangeNumberViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "profile.changePhone.title".localized
        self.customTitle = "profile.changePhone.customTitle".localized
    }
    
    override func setupProgressView() {}
    
}

