//
//  ConfirmChangeNumberViewController.swift
//  8Pitch
//
//  Created by 8pitch on 16.11.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift
import Input
import Remote

class ConfirmChangeNumberViewController: SMSViewController {
    
    override var confirmationFooterViewType : PublishSubject<InputSMSView.ViewType?>? {
        didSet {
            guard let typeAction = self.confirmationFooterViewType else { return }
            typeAction.onNext(.changePhone)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.title = "profile.changePhone.title".localized
        self.customTitle = "profile.changePhone.confirm".localized
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(CodeTextCell.nibForRegistration, forCellWithReuseIdentifier: CodeTextCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = InputSMSView.reuseIdentifier
        layout.register(InputSMSView.nibForRegistration, forDecorationViewOfKind: InputSMSView.reuseIdentifier)
        layout.additionalButtonAction.subscribe(onNext: { [weak self] in
            self?.resendButtonAction = $0
        }).disposed(by: self.disposables)
    }
    
    override func nextButtonTapped() {
        let _ = ViewControllerProvider.setupSuccessPhoneChangeViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func verifyToken() {
        super.verifyToken()
        guard let session = self.session else {
            return
        }
        self.startProgress()
        self.remote.changePhone(session) { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.nextButtonTapped()
                return
            }
            self?.presentAlert(message: error.localizedDescription)
        }
    }
    
    override func resendCode() {
        super.resendCode()
        guard let session = self.session else {
            return
        }
        self.remote.resendLimitCode(session)
            .compactMap { $0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] response in
                self?.stopProgress()
                switch response {
                case .success(_):
                    return
                case .failure(let error):
                    let limitReached = error as? SendCodeError == SendCodeError.limit
                    self?.presentAlert(message: error.localizedDescription,
                                       completion: limitReached ?
                                        { [weak self] in self?.popToPreviousController(animated: true)} : {} )
                }
            }).disposed(by: self.disposables)
    }
    
}
