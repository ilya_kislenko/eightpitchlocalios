//
//  SuccessPhoneChangeViewController.swift
//  8Pitch
//
//  Created by 8pitch on 16.11.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class SuccessPhoneChangeViewController: CustomTitleViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeChangePhoneFlow))
    }
    
    @IBAction func okTapped(_ sender: UIButton) {
        self.closeChangePhoneFlow()
    }
    
    @objc private func closeChangePhoneFlow() {
        self.previousController = self.previousController?.previousController?.previousController
        self.popToPreviousController(animated: true)
    }
    
    override func popToPreviousController(animated: Bool) {
        super.popToPreviousController(animated: true)
        self.session?.codeProvider.onNext(Code())
        self.session?.phoneProvider.onNext(Phone())
    }

}
