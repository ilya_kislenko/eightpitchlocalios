//
//  SuccessEmailUpdateViewController.swift
//  8Pitch
//
//  Created by 8pitch on 30.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class SuccessEmailChangeViewController: CustomTitleViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeChangeEmailFlow))
    }
    
    @IBAction func okTapped(_ sender: UIButton) {
        self.closeChangeEmailFlow()
    }
    
    @objc private func closeChangeEmailFlow() {
        self.previousController = self.previousController?.previousController?.previousController
        self.popToPreviousController(animated: true)
    }
    
    override func popToPreviousController(animated: Bool) {
        super.popToPreviousController(animated: true)
        self.session?.codeProvider.onNext(Code())
        self.session?.emailProvider.onNext(Email())
    }

}
