//
//  UpdateEmailViewController.swift
//  8Pitch
//
//  Created by 8pitch on 30.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import RxSwift

class ChangeEmailViewController: EnterEmailViewController {
    
    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        if let cell = cell as? EmailCell {
            cell.textField.placeholder = "cell.email.placeholder".localized
        }
        return cell
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "profile.updateEmail.title".localized
        self.customTitle = "profile.updateEmail.type.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func nextButtonTapped() {
        _ = ViewControllerProvider.setupVerifyChangeEmailViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    override func setupProgressView() {}
    
}
