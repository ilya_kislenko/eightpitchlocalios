//
//  EditProfileViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/16/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift
import RxCocoa

class EditProfileViewController: AbstractCollectionViewController<KYC> {
    
    // MARK: Properties
    
    override var model: BehaviorSubject<KYC>? {
        self.session?.personalProfileInfoProvider
    }
    
    override func setupModel() {
        guard let model = try? self.model?.value() else {
            return
        }
        try? model.nameAndLastNameProvider.value().valueProviders[\.titleProvider] = self.titleProvider
        try? model.companyProvider.value().valueProviders[\.companyTypeProvider] = self.companyTypeProvider
    }
    
    private lazy var viewModel : EditProfileViewModel = {
        EditProfileViewModel(session: session)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customTitle = "profile.personal.edit.customTitle".localized
        self.title = "KYC.edit.title".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        try? self.model?.value().validate(forSecondLevel: self.viewModel.isSecondLevelAccount,
                                          forInstitutionalInvestor: self.viewModel.isInstitutionalInvestor)
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return
                    case .loading:
                        try? self.model?.value().validate(forSecondLevel: self.viewModel.isSecondLevelAccount,
                                                          forInstitutionalInvestor: self.viewModel.isInstitutionalInvestor)
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            guard value else { return }
                            self.verifyModelAndNextButtonActionIfPossible()
                        }
                    case .next: self.popToPreviousController(animated: true)
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var applyChangesFooterViewType : PublishSubject<ApplyChangesView.ViewType?>? {
        didSet {
            guard let typeAction = self.applyChangesFooterViewType else { return }
            typeAction.onNext(viewModel.session.level == .second || viewModel.session.level == .questionaryPassed ? .sendRequest : .applyChanges)
        }
    }
    
    // MARK: Methods
    
    private func presentUpdateRule() {
        self.presentAlert(title: "deletion.request.notification.title".localized,
                          message: "deletion.request.notification.text".localized) {
            self.nextButtonAction?.onNext(.next)
        }
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(EditCell.nibForRegistration, forCellWithReuseIdentifier: EditCell.reuseIdentifier)
        self.collectionView.register(EditHeaderCell.nibForRegistration, forCellWithReuseIdentifier: EditHeaderCell.reuseIdentifier)
        self.collectionView.register(PoliticallyExposedPersonCell.nibForRegistration, forCellWithReuseIdentifier: PoliticallyExposedPersonCell.reuseIdentifier)
        self.collectionView.register(TaxCell.nibForRegistration, forCellWithReuseIdentifier: TaxCell.reuseIdentifier)
        self.collectionView.register(GermanTaxDropdownCell.nibForRegistration, forCellWithReuseIdentifier: GermanTaxDropdownCell.reuseIdentifier)
        self.collectionView.register(GermanTaxInputCell.nibForRegistration, forCellWithReuseIdentifier: GermanTaxInputCell.reuseIdentifier)
        self.collectionView.register(GermanTaxSwitcherCell.nibForRegistration, forCellWithReuseIdentifier: GermanTaxSwitcherCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = ApplyChangesView.reuseIdentifier
        layout.register(ApplyChangesView.nibForRegistration, forDecorationViewOfKind: ApplyChangesView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        var sections: [CollectionViewModel<KYC>] = []
        if self.viewModel.isInstitutionalInvestor {
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: CompanyError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: CompanyTypeError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: CompanyNumberError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        }
        if self.viewModel.isSecondLevelAccount {
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: TitleError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: FirstNameError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: LastNameError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: BirthDayError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: PersonalDetailsError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: BirthPlaceError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: CountryError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: LocalityError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: ZipError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: AddressError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: AccountOwnerError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: IBANError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: PoliticallyExposedPersonCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: TaxCell.reuseIdentifier))
        } else {
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: EditHeaderCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: TitleError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: FirstNameError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: LastNameError.self, cellReuseIdentifier: EditCell.reuseIdentifier))
        }
        if self.viewModel.isGermanTaxesInfoFilled {
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: GermanTaxSwitcherCell.reuseIdentifier))
        }
        if self.viewModel.isGermanTaxResident {
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: GermanTaxInputCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: GermanTaxInputCell.reuseIdentifier))
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: GermanTaxSwitcherCell.reuseIdentifier))
        }
        if self.viewModel.isChurchTaxLability {
            sections.append(CollectionViewModel(collectionView: self.collectionView, section: sections.count, model: model, errorType: EmptyError.self, cellReuseIdentifier: GermanTaxDropdownCell.reuseIdentifier))
        }
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
    }
    
    @objc override func nextButtonTapped() {
        guard let session = self.session else { return }
        self.startProgress()
        self.viewModel.updateUserData(session) { response in
            self.stopProgress()
            switch response {
            case .success(_):
                guard self.viewModel.isFirstLevelAccount else {
                    self.presentUpdateRule()
                    return
                }
                self.nextButtonAction?.onNext(.next)
            case .failure(let error):
                self.presentAlert(message: error.localizedDescription)
                self.nextButtonAction?.onNext(.error)
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath)
        if let headerCell = cell as? EditHeaderCell {
            headerCell.setTitle(self.viewModel.editSectionsContent[indexPath.section].headerTitle)
        } else if let inputCell = cell as? EditCell {
            inputCell.setup(for: self.viewModel.editSectionsContent[indexPath.section].type ?? .firstName)
        } else if let switchCell = cell as? CellModel<PersonalDetails> {
            switchCell.model = try? self.model?.value().personalDetailsProvider
        } else if let germanTaxSwitchCell = cell as? GermanTaxSwitcherCell {
            germanTaxSwitchCell.model = try? self.model?.value().germanTaxesProvider
            germanTaxSwitchCell.setup(for: self.viewModel.germanTaxSwitchCellType(indexPath.section))
            germanTaxSwitchCell.switchHandler = { [weak self] in
                self?.viewModel.updateSectionsContent()
                self?.setupLayout()
                self?.registerCells()
                self?.setupDataSource()
                self?.collectionView.reloadData()
                self?.collectionView.layoutIfNeeded()
                self?.collectionView.scrollToItem(at: indexPath, at: .top, animated: false)
            }
        } else if let germanTaxInputCell = cell as? GermanTaxInputCell {
            germanTaxInputCell.model = try? self.model?.value().germanTaxesProvider
            germanTaxInputCell.setup(for: self.viewModel.germanTaxInputCellType(indexPath.section))
            germanTaxInputCell.validate()
        } else if let germanTaxDropdownCell = cell as? GermanTaxDropdownCell {
            germanTaxDropdownCell.model = try? self.model?.value().germanTaxesProvider
            germanTaxDropdownCell.validate()
        }
        return cell
    }
}

extension EditProfileViewController {
    
    var companyTypeProvider : (() -> Void)? {
        return { [weak self] in
            if let pickerVC = self?.registration.instantiateViewController(withIdentifier: "PickerViewController") as? PickerViewController {
                guard let model = try? self?.model?.value() else {
                    return
                }
                pickerVC.selection.subscribe(onNext: {
                    try? model.companyProvider.value().companyTypeProvider.onNext($0)
                }).disposed(by: self?.disposables)
                pickerVC.dataSource = CompanyType.allCases.map { $0.rawValue.localized }
                self?.present(pickerVC, animated: true, completion: nil)
            }
        }
    }
    
}

extension EditProfileViewController {
    
    var titleProvider : (() -> Void)? {
        return { [weak self] in
            if let pickerVC = self?.registration.instantiateViewController(withIdentifier: "PickerViewController") as? PickerViewController {
                guard let model = try? self?.model?.value() else {
                    return
                }
                pickerVC.selection.subscribe(onNext: {
                    try? model.nameAndLastNameProvider.value().titleProvider.onNext($0)
                }).disposed(by: self?.disposables)
                pickerVC.dataSource = Title.allCases.map { $0.rawValue.localized }
                self?.present(pickerVC, animated: true, completion: nil)
            }
        }
    }
}
