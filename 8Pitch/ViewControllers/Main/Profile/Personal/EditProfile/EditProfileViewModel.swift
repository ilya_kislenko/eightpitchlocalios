//
//  EditProfileViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/16/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class EditProfileViewModel: GenericViewModel {

    struct EditProfileSection {
        let headerTitle: String?
        let title: String?
        let type: EditCellType?
    }
    
    override var isGermanTaxResident: Bool {
        self.isGermanTaxesInfoFilled &&
            (try? self.session.personalProfileInfoProvider.value().isGermanTaxResident) ?? false
    }
    
    var isChurchTaxLability: Bool {
        isGermanTaxResident &&
            (try? self.session.personalProfileInfoProvider.value().taxInfo?.churchTaxLabilityValue) ?? false
    }
    
    var editSectionsContent: [EditProfileSection] = []

    required init(session: Session?) {
        super.init(session: session)
        var editSections = self.sectionsContent.filter {
            $0.type != .upgrade && $0.type != .contact
        }
        if !self.isSecondLevelAccount {
            editSections = editSections.filter {
                $0.type != .address && $0.type != .other && $0.type != .financial
            }
        }
        for section in editSections {
            let header = EditProfileSection(headerTitle: section.headerTitle, title: nil, type: nil)

            self.editSectionsContent.append(header)
            section.rowsContent.forEach {
                if $0.imageName != .KYCStreet {
                self.editSectionsContent.append(EditProfileSection(headerTitle: nil, title: $0.title, type: $0.type))
                }
            }
        }
    }
    
    func updateSectionsContent() {
        self.editSectionsContent = []
        var editSections = self.sectionsContent.filter {
            $0.type != .upgrade && $0.type != .contact
        }
        if !self.isSecondLevelAccount {
            editSections = editSections.filter {
                $0.type != .address && $0.type != .other && $0.type != .financial
            }
        }
        for section in editSections {
            let header = EditProfileSection(headerTitle: section.headerTitle, title: nil, type: nil)

            self.editSectionsContent.append(header)
            section.rowsContent.forEach {
                if $0.imageName != .KYCStreet {
                self.editSectionsContent.append(EditProfileSection(headerTitle: nil, title: $0.title, type: $0.type))
                }
            }
        }
    }
    
    func germanTaxInputCellType(_ section: Int) -> GermanTaxInputCell.GermanTaxInputCellType {
        editSectionsContent[section].type == .germanTaxesid ? .id : .office
    }
    
    func germanTaxSwitchCellType(_ section: Int) -> GermanTaxSwitcherCell.GermanTaxSwitcherCellType {
        editSectionsContent[section].type == .germanTaxesResident ? .tax : .churchTax
    }

    func updateUserData(_ session: Session, completionHandler: @escaping ResponseClosure) {
        self.remote.updateUserData(session)
        .compactMap { $0 }
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { response in
            completionHandler(response)
        }).disposed(by: self.disposables)
    }

}
