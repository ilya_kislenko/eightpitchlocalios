//
//  PersonalProfileViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/15/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import UIKit

class PersonalProfileViewModel: GenericViewModel {
    
    override var sectionsContent: [Section] {
        if !self.isSecondLevelAccount {
            return super.sectionsContent.filter {
                    $0.type != .address && $0.type != .other && $0.type != .financial
                }
            }
        return super.sectionsContent
    }
    
    var numberOfSections: Int {
        self.sectionsContent.count
    }
    
    func isUpgradeViewNeeded(for section: Int) -> Bool {
        return !self.isInitiator && section == 0
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        self.sectionsContent[section].rowsContent.count
    }
    
    func summarizeData(for indexPath: IndexPath) -> SummaryData {
        let content = self.sectionsContent[indexPath.section].rowsContent[indexPath.row]
        let image = content.imageName == nil ? nil : UIImage(content.imageName!)
        return SummaryData(image: image, key: content.title, value: content.value)
        
    }
    
    func contentTypeToOpen(indexPath: IndexPath) -> EditCellType? {
        let content = self.sectionsContent[indexPath.section].rowsContent[indexPath.row]
        return content.type
    }
    
    func isTopSeparatorNeeded(for section: Int) -> Bool {
        return section != 0 || (section != 1 && (self.isSecondLevelAccount || self.isInvestor))
    }
    
    func isLast(_ indexPath: IndexPath) -> Bool {
        let section = self.sectionsContent[indexPath.section]
        return indexPath.row == section.rowsContent.count - 1
    }

}
