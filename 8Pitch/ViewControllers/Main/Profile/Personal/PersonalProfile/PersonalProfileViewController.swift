//
//  PersonalProfileViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/15/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class PersonalProfileViewController: CustomTitleViewController {
    
    private lazy var viewModel : PersonalProfileViewModel = {
        PersonalProfileViewModel(session: session)
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "profile.personal.title".localized
        self.customTitle = "profile.personal.customTitle".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.edit), style: .plain, target: self, action: #selector(self.openPageEditing))
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateContent()
    }
    
}

// MARK: Private

private extension PersonalProfileViewController {
    
    func openEmailChangingScreen() {
        _ = ViewControllerProvider.setupChangeEmailViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openPhoneChangingScreen() {
        _ = ViewControllerProvider.setupChangePhoneViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    @objc func openPageEditing() {
        _ = ViewControllerProvider.setupEditProfileViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    enum Constants {
        static let headerHeight: CGFloat = 88
        static let upgradeHeaderHeight: CGFloat = 141
        static let secondUpgradeHeaderHeight: CGFloat = 125
        static let cellHeight: CGFloat = 48
    }
    
    func registerCells() {
        self.tableView.register(SummarizeCell.nibForRegistration, forCellReuseIdentifier: SummarizeCell.reuseIdentifier)
        self.tableView.register(UpdateQuestionaryHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: UpdateQuestionaryHeaderView.reuseIdentifier)
        self.tableView.register(ProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: ProfileHeaderView.reuseIdentifier)
        self.tableView.register(PersonalProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: PersonalProfileHeaderView.reuseIdentifier)
    }
    
    func updateContent() {
        self.startProgress()
        self.viewModel.user() { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success( _):
                self?.session?.updateModelForPersonalProfile()
                self?.tableView.reloadData()
                return
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
                return
            }
        }
    }
    
}

extension PersonalProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SummarizeCell.reuseIdentifier, for: indexPath) as? SummarizeCell else {
            return UITableViewCell()
        }
        cell.setupWith(self.viewModel.summarizeData(for: indexPath))
        cell.updateSeparatorForLastRow(self.viewModel.isLast(indexPath))
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.viewModel.isUpgradeViewNeeded(for: section) {
            if viewModel.isQuestionaryPassed {
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: UpdateQuestionaryHeaderView.reuseIdentifier) as? UpdateQuestionaryHeaderView
                view?.upgradeAction = { [weak self] in
                    self?.passQuestionaryTapped()
                }
                view?.hideSeparator()
                return view
            } else {
                let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: ProfileHeaderView.reuseIdentifier) as? ProfileHeaderView
                view?.upgradeAction = { [weak self] in
                    self?.upgradeAccountTapped()
                }
                view?.hideSeparator()
                return view
            }
        } else {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: PersonalProfileHeaderView.reuseIdentifier) as? PersonalProfileHeaderView
            view?.setTitle(self.viewModel.sectionsContent[section].headerTitle)
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.viewModel.isUpgradeViewNeeded(for: section) {
            return viewModel.isQuestionaryPassed ? Constants.secondUpgradeHeaderHeight : Constants.upgradeHeaderHeight
        } else {
            return Constants.headerHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type = self.viewModel.contentTypeToOpen(indexPath: indexPath)
        switch type {
        case .email:
            self.openEmailChangingScreen()
        case .phone:
            self.openPhoneChangingScreen()
        default:
            return
        }
    }
    
}
