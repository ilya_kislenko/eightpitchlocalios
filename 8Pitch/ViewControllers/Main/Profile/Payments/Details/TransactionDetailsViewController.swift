//
//  PaymentDetailsViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/30/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class TransactionDetailsViewController: CustomTitleViewController {
    
    private lazy var viewModel : TransactionDetailsViewModel = {
        let model = TransactionDetailsViewModel(session: session)
        model.transaction = self.transaction
        return model
    }()
    
    @IBOutlet weak var tableView: UITableView!
    public var transaction: UserTransaction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.viewModel.title.localized
        self.customTitle = self.viewModel.customTitle.localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.load), style: .plain, target: self, action: #selector(self.loadButtonTapped))
        self.registerCells()
    }
    
}

extension TransactionDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SummarizeCell.reuseIdentifier, for: indexPath) as? SummarizeCell else {
            fatalError("\(SummarizeCell.reuseIdentifier) not exists")
        }
        cell.updateSeparatorForLastRow(self.viewModel.isLast(indexPath))
        cell.setupWith(self.viewModel.details[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: PersonalProfileHeaderView.reuseIdentifier) as? PersonalProfileHeaderView
        view?.setTitle(self.viewModel.header.localized)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        Constants.headerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constants.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let parameter = self.viewModel.details[indexPath.row].value ?? ""
        self.presentAlert(title: "", message: parameter, actionMessage: "profile.payments.detail.copy".localized, addAction: self.addCopyAction)
    }
}

private extension TransactionDetailsViewController {
    
    @objc func loadButtonTapped() {
        self.startProgress()
        self.viewModel.downloadFile { [weak self] error, url in
            DispatchQueue.main.async {
                self?.stopProgress()
                if let error = error {
                    self?.presentAlert(message: error.localizedDescription)
                } else if let url = url {
                    let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
                    self?.present(activityViewController, animated: true)
                }
            }
        }
    }
    
    enum Constants {
        static let headerHeight: CGFloat = 35
        static let cellHeight: CGFloat = 48
    }
    
    func registerCells() {
        self.tableView.register(SummarizeCell.nibForRegistration, forCellReuseIdentifier: SummarizeCell.reuseIdentifier)
        self.tableView.register(PersonalProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: PersonalProfileHeaderView.reuseIdentifier)
    }
}
