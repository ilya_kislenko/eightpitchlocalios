//
//  PaymentDetailsViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/30/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import UIKit
import Remote

class TransactionDetailsViewModel: GenericViewModel {
    
    enum PaymentMethods: String {
        case klarna = "Klarna"
        case secupay = "Secupay"
        case fintecsystems = "Fintecsystems"
        case classicBankTransfer = "ClassicBankTransfer"
    }
    
    public var transaction: UserTransaction?
    
    var currentPayment: UserPayment? {
        self.transaction as? UserPayment
    }
    
    var currentTransfer: UserTransfer? {
        self.transaction as? UserTransfer
    }
    
    var transactionType: HistoryTabViewController.HistoryTabType {
        switch transaction {
        case is UserPayment:
            return .payments
        case is UserTransfer:
            return .transfers
        default:
            return .payments
        }
    }
    
    var details: [SummaryData] {
        switch transactionType {
        case .payments:
            return paymentDetails
        case .transfers:
            return transferDetails
        }
    }
    
    var title: String {
        switch transactionType {
        case .payments:
            return "profile.payments.details.title"
        case .transfers:
            return "profile.transfer.details.title"
        }
    }
    
    var customTitle: String {
        switch transactionType {
        case .payments:
            return "profile.payments.details.customTitle"
        case .transfers:
            return "profile.transfer.details.customTitle"
        }
    }
    
    var header: String {
        switch transactionType {
        case .payments:
            return "profile.payments.details.header"
        case .transfers:
            return "profile.transfer.details.header"
        }
    }
    
    func isLast(_ indexPath: IndexPath) -> Bool {
        indexPath.row == self.paymentDetails.count - 1
    }
    
    lazy var paymentDetails: [SummaryData] = {
        guard let payment = currentPayment,
              let monetaryAmount = payment.monetaryAmount,
              let amount = payment.amount,
              let transactionDate = payment.transactionDate
        else { return []}
        let date = DateHelper.shared.uiFormatterFullSeconds.string(from: transactionDate)
        let name = SummaryData(image: UIImage(.company), key: "profile.payments.details.name".localized, value: payment.companyName ?? "")
        let amountInvested = SummaryData(image: UIImage(.boldEuro), key: "profile.payments.details.amount".localized, value: "\(monetaryAmount.doubleValue.currency) " + "projects.nominal.currency.euro".localized)
        let paymentProvider = SummaryData(image: UIImage(.profilePayments), key: "profile.payments.details.provider".localized, value: self.paymentProviderName(providerName: payment.paymentSystem))
        let tokenAmount = SummaryData(image: UIImage(.amountOfTokens), key: "profile.payments.details.token".localized, value: "\(amount.doubleValue.currency) \(payment.shortCut)")
        let isin = SummaryData(image: UIImage(.isin), key: "profile.payments.details.isin".localized, value: payment.isin ?? "")
        let address = SummaryData(image: UIImage(.KYCpin), key: "profile.payments.details.address".localized, value: payment.companyAddress ?? "")
        let id = SummaryData(image: UIImage(.transactionID), key: "profile.payments.details.id".localized, value: payment.transactionID ?? "")
        let dateTime = SummaryData(image: UIImage(.KYCcalendar), key: "profile.payments.details.date".localized, value: date)
        return [name, address, amountInvested, paymentProvider, tokenAmount, isin, id, dateTime]
    }()
    
    lazy var transferDetails: [SummaryData] = {
        guard let transfer = currentTransfer,
              let monetaryAmount = transfer.amount,
              let transactionDate = transfer.transactionDate
        else { return []}
        let date = DateHelper.shared.uiFormatterFullSeconds.string(from: transactionDate)
        let dateTime = SummaryData(image: UIImage(.KYCcalendar), key: "profile.payments.details.date".localized, value: date)
        let name = SummaryData(image: UIImage(.company), key: "profile.payments.details.name".localized, value: transfer.companyName ?? "")
        let address = SummaryData(image: UIImage(.KYCpin), key: "profile.payments.details.address".localized, value: transfer.companyAddress ?? "")
        let isin = SummaryData(image: UIImage(.isin), key: "investment.preconstructual.isin".localized, value: transfer.isin ?? "")
        let tokenAmount = SummaryData(image: UIImage(.amountOfTokens), key: "profile.payments.details.token".localized, value: "\(monetaryAmount.doubleValue.currency) \(transfer.shortCut)")
        let from = SummaryData(image: UIImage(.userSent), key: "profile.transfer.details.from".localized, value: transfer.from)
        let to = SummaryData(image: UIImage(.userRecieved), key: "profile.transfer.details.to".localized, value: transfer.to)
        return [dateTime, name, address, isin, tokenAmount, from, to]
    }()
    
    private func paymentProviderName(providerName: String?) -> String {
        let method = PaymentMethods(rawValue: providerName ?? "")
        switch method {
        case .klarna:
            return "payment.softcap.methods.online.klarna".localized
        case .secupay:
            return "payment.softcap.methods.directDebit".localized
        case .fintecsystems:
            return "payment.softcap.methods.online".localized
        case .classicBankTransfer:
            return "payment.softcap.methods.classicBank".localized
        case .none:
            return ""
        }
    }
    
    func downloadFile(_ completionHandler: @escaping DownloadFileHandler) {
        guard let id = self.transaction?.receiptID else {
            completionHandler(GenericRequestError.invalidResponse(nil), nil)
            return
        }
        switch self.transactionType {
        case .payments:
            self.downloadPaymentFile(id: id, completionHandler)
        case .transfers:
            self.downloadTransferFile(id: id, completionHandler)
        }
    }
    
    private func downloadTransferFile(id: String, _ completionHandler: @escaping DownloadFileHandler) {
        self.remote.downloadTransferFile(session, id) { [weak self] result in
            switch result {
            case .failure(let error):
                completionHandler(error, nil)
            case .success(let data):
                self?.saveFile(data: data, completionHandler: completionHandler)
            }
        }
    }
    
    private func downloadPaymentFile(id: String, _ completionHandler: @escaping DownloadFileHandler) {
        self.remote.downloadFile(session, id) { [weak self] result in
            switch result {
            case .failure(let error):
                completionHandler(error, nil)
            case .success(let data):
                self?.saveFile(data: data, completionHandler: completionHandler)
            }
        }
    }
    
    public func saveFile(data: Data, completionHandler: @escaping DownloadFileHandler) {
        guard let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileName = "Invoice-\(self.transaction?.receiptID ?? "").pdf"
        let source = documentsPath.appendingPathComponent(fileName)
        do {
            try? FileManager.default.removeItem(at: source)
            try data.write(to: source, options: .atomic)
        } catch {
            completionHandler(error, nil)
            return
        }
        completionHandler(nil, source)
    }
}
