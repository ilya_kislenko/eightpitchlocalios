//
//  PaymentsHistoryViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/30/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PaymentsHistoryViewController: CustomTitleViewController {
    
    private lazy var viewModel : PaymentsHistoryViewModel = {
        PaymentsHistoryViewModel(session: session)
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "profile.payments.list.title".localized
        self.customTitle = "profile.payments.list.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
        self.registerCells()
    }
    
}

extension PaymentsHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.payments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentHistoryCell.reuseIdentifier, for: indexPath) as? PaymentHistoryCell else {
            fatalError("\(PaymentHistoryCell.nibForRegistration) doesn't exists")
        }
        cell.setupFor(payment: self.viewModel.payments[indexPath.row])
        cell.setupLastSeparator(self.viewModel.isLast(indexPath))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.viewModel.selectPayment(for: indexPath)
        self.openDetailsScreen()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: PersonalProfileHeaderView.reuseIdentifier) as? PersonalProfileHeaderView
        view?.setTitle("profile.payments.list.header".localized)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        Constants.headerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constants.cellHeight
    }
    
}

private extension PaymentsHistoryViewController {
    
    enum Constants {
        static let headerHeight: CGFloat = 35
        static let cellHeight: CGFloat = 48
    }
    
    func registerCells() {
        self.tableView.register(PaymentHistoryCell.nibForRegistration, forCellReuseIdentifier: PaymentHistoryCell.reuseIdentifier)
        self.tableView.register(PersonalProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: PersonalProfileHeaderView.reuseIdentifier)
    }
    
    func openDetailsScreen() {
        _ = ViewControllerProvider.setupPaymentDetailsViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
}
