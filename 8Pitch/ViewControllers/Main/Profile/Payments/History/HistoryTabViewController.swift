//
//  HistoryTabViewController.swift
//  8Pitch
//
//  Created by 8pitch on 1/11/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class HistoryTabViewController: CustomTitleViewController {
    
    enum HistoryTabType {
        case payments
        case transfers
    }
    
    private lazy var viewModel : HistoryTabViewModel = {
        HistoryTabViewModel(session: session)
    }()
    
    public var tabType: HistoryTabType = .payments
    public var openDetailsScreen: TransactionHandler?
    public var investNow: VoidClosure?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noPaymentsView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isHidden = self.viewModel.transactions(self.tabType).isEmpty
        self.noPaymentsView.isHidden = !self.viewModel.transactions(self.tabType).isEmpty
        self.registerCells()
    }
    
    @IBAction func investNowTapped(_ sender: RoundedButton) {
        self.investNow?()
    }
    
}

extension HistoryTabViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.transactions(self.tabType).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentHistoryCell.reuseIdentifier, for: indexPath) as? PaymentHistoryCell else {
            fatalError("\(PaymentHistoryCell.nibForRegistration) doesn't exists")
        }
        cell.setupFor(payment: self.viewModel.transactions(self.tabType)[indexPath.row])
        cell.setupLastSeparator(self.viewModel.isLast(indexPath))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.openDetailsScreen(for: self.viewModel.transactions(self.tabType)[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constants.cellHeight
    }
}

private extension HistoryTabViewController {
    
    enum Constants {
        static let cellHeight: CGFloat = 48
    }
    
    func registerCells() {
        self.tableView.register(PaymentHistoryCell.nibForRegistration, forCellReuseIdentifier: PaymentHistoryCell.reuseIdentifier)
    }
    
    func openDetailsScreen(for transaction: UserTransaction) {
        self.openDetailsScreen?(transaction)
    }
}
