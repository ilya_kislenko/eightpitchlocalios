//
//  HistoryTabViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 1/11/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input
import UIKit

class HistoryTabViewModel: GenericViewModel {
    
    func isLast(_ indexPath: IndexPath) -> Bool {
        indexPath.row == self.payments.count - 1
    }
    
    func transactions(_ type: HistoryTabViewController.HistoryTabType) -> [UserTransaction] {
        switch type {
        case .payments:
            return self.payments
        case .transfers:
            return self.transfers
        }
    }
}
