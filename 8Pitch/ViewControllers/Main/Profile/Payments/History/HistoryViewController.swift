//
//  HistoryViewController.swift
//  8Pitch
//
//  Created by 8pitch on 1/11/21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input

class HistoryViewController: CustomTitleViewController {
    
    private lazy var viewModel : HistoryViewModel = {
        HistoryViewModel(session: session)
    }()
    
    // MARK: UI Components
    
    @IBOutlet weak var noPaymentsView: UIView!
    @IBOutlet weak var segmentedControl: ProjectsSegmentedControl!
    @IBOutlet weak var segmentedControlHeight: NSLayoutConstraint!
    
    private let indicator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(.red)
        return view
    }()
    
    private lazy var paymentsScreen: HistoryTabViewController = {
        (ViewControllerProvider.setupHistoryTabViewController(rootViewController: self, tabType: .payments) ?? HistoryTabViewController())
    }()
    private lazy var transfersScreen: HistoryTabViewController = {
        (ViewControllerProvider.setupHistoryTabViewController(rootViewController: self, tabType: .transfers) ?? HistoryTabViewController())
    }()
    
    private let contentView = UIView()
    
    // MARK: Actions
    
    @IBAction func tap(_ sender: UISegmentedControl) {
        self.handleAppearanceOnTouch(sender, indicator: indicator)
        UIView.animate(withDuration: 0.3, animations: {
                        self.view.layoutIfNeeded() })
        self.paymentsScreen.view.isHidden = sender.selectedSegmentIndex != 0
        self.transfersScreen.view.isHidden = sender.selectedSegmentIndex != 1
    }
    
    @IBAction func investNowTapped(_ sender: RoundedButton) {
        self.goToProjectsPage()
    }
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "profile.payments.list.title".localized
        self.customTitle = "profile.payments.list.customTitle".localized
        self.setupContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBar()
        self.navigationController?.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.handleAppearanceOnTouch(self.segmentedControl, indicator: self.indicator)
    }
}

// MARK: Private

private extension HistoryViewController {
    
    func goToProjectsPage() {
        self.navigationController?.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setupContent() {
        self.segmentedControlHeight.constant = self.viewModel.isTwoTransactionHistoryTabAvailable ? self.segmentedControlHeight.constant : 0
        self.segmentedControl.isHidden = !self.viewModel.isTwoTransactionHistoryTabAvailable
        self.indicator.isHidden = !self.viewModel.isTwoTransactionHistoryTabAvailable
        self.setupSegmentedControl(segmentedControl: segmentedControl, with: indicator)
        self.noPaymentsView.isHidden = !(self.viewModel.payments.isEmpty && self.viewModel.transfers.isEmpty)
        self.addContent(payment: !self.viewModel.payments.isEmpty || !self.viewModel.transfers.isEmpty, transfer: !self.viewModel.transfers.isEmpty)
    }
    
    func setupSegmentedControl(segmentedControl: ProjectsSegmentedControl, with indicator: UIView) {
        self.fixBackgroundSegmentControl(segmentedControl)
        let separator = SeparatorView()
        self.view.addSubview(separator)
        separator.isHidden = !self.viewModel.isTwoTransactionHistoryTabAvailable
        separator.snp.makeConstraints {
            $0.trailing.leading.equalToSuperview()
            $0.top.equalTo(segmentedControl.snp.bottom).offset(Constants.indicatorOffset + 1)
        }
        self.view.addSubview(self.indicator)
        self.setDefaultConstraints(for: indicator, dependsOn: segmentedControl)
        indicator.snp.makeConstraints {
            $0.leading.equalTo(segmentedControl.snp.leading)
        }
    }
    
    func handleAppearanceOnTouch(_ sender: UISegmentedControl, indicator: UIView) {
        indicator.snp.remakeConstraints {
            if sender.selectedSegmentIndex == sender.numberOfSegments - 1 {
                $0.trailing.equalTo(sender.snp.trailing)
            } else {
                $0.leading.equalTo(sender.snp.leading)
            }
        }
        self.setDefaultConstraints(for: indicator, dependsOn: sender)
    }
    
    func setDefaultConstraints(for indicator: UIView, dependsOn segmentedControl: UISegmentedControl) {
        indicator.snp.makeConstraints {
            $0.top.equalTo(segmentedControl.snp.bottom).offset(Constants.indicatorOffset)
            $0.height.equalTo(Constants.indicatorHeight)
            $0.width.equalTo(segmentedControl.frame.width/CGFloat(segmentedControl.numberOfSegments))
        }
    }
    
    func fixBackgroundSegmentControl( _ segmentedControl: UISegmentedControl) {
        if #available(iOS 13.0, *) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                for i in 0...(segmentedControl.numberOfSegments - 1)  {
                    let backgroundSegmentView = segmentedControl.subviews[i]
                    backgroundSegmentView.isHidden = true
                }
            }
        }
    }
    
    enum Constants {
        static let indicatorOffset: CGFloat = 3
        static let indicatorHeight: CGFloat = 2
    }
    
    func add(_ child: HistoryTabViewController) {
        addChild(child)
        self.contentView.addSubview(child.view)
        child.didMove(toParent: self)
        child.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        child.openDetailsScreen = { [weak self] transaction in
            self?.openDetails(for: transaction)
        }
        child.investNow = { [weak self] in
            self?.goToProjectsPage()
        }
    }
    
    func openDetails(for transaction: UserTransaction) {
        _ = ViewControllerProvider.setupTransactionDetailsViewController(rootViewController: self, transaction: transaction)
        self.pushNextController(animated: true)
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.rightBarButtonItem = nil
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationItem.customizeForWhite()
    }
    
    func addContent(payment: Bool, transfer: Bool) {
        if !payment && !transfer {
            return
        }
        self.view.addSubview(self.contentView)
        self.contentView.snp.makeConstraints {
            $0.trailing.leading.bottom.equalToSuperview()
            $0.top.equalTo(self.indicator.snp.bottom)
        }
        if payment {
            self.add(self.paymentsScreen)
            self.paymentsScreen.view.isHidden = false
        }
        if transfer {
            self.add(self.transfersScreen)
            self.transfersScreen.view.isHidden = payment
        }
    }
}
