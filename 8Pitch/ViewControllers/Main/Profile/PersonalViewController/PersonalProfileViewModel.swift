//
//  PersonalProfileViewModel.swift
//  8Pitch
//
//  Created by Volha Bychok on 9/15/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import UIKit

class PersonalProfileViewModel: GenericViewModel {
    
    var numberOfSections: Int {
        self.sectionsContent.count
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        self.sectionsContent[section].rowsContent.count
    }
    
    func summarizeData(for indexPath: IndexPath) -> SummarizeData {
        let content = self.sectionsContent[indexPath.section].rowsContent[indexPath.row]
        let image = content.imageName == nil ? nil : UIImage(content.imageName!)
        return SummarizeData(image: image, key: content.title, value: content.value)
        
    }
    
    func isUpgradeViewNeeded(for section: Int) -> Bool {
        return !self.isSecondLevelAccount && section == 0
    }
    
    func isTopSeparatorNeeded(for section: Int) -> Bool {
        return section != 0 || (section != 1 && self.isSecondLevelAccount)
    }
    
    func islastRow(_ indexPath: IndexPath) -> Bool {
        let section = self.sectionsContent[indexPath.section]
        return indexPath.row == section.rowsContent.count - 1
    }

}
