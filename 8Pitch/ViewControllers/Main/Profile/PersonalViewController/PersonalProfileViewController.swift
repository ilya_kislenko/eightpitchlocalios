//
//  PersonalProfileViewController.swift
//  8Pitch
//
//  Created by Volha Bychok on 9/15/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PersonalProfileViewController: CustomTitleViewController {
    
    private lazy var viewModel : PersonalProfileViewModel = {
        guard let session = self.session else {
            fatalError("wrong init sequence!")
        }
        let retVal = PersonalProfileViewModel(session: self.session)
        return retVal
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "profile.personal.title".localized
        self.customTitle = "profile.personal.customTitle".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(.edit), style: .plain, target: self, action: #selector(self.openPageEditing))
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateContent()
    }
    
}

// MARK: Private

private extension PersonalProfileViewController {
    
    @objc func openPageEditing() {
        _ = ViewControllerProvider.setupEditProfileViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    enum Constants {
        static let headerHeight: CGFloat = 88
        static let upgradeHeaderHeight: CGFloat = 141
    }
    
    func registerCells() {
        self.tableView.register(SummarizeCell.nibForRegistration, forCellReuseIdentifier: SummarizeCell.reuseIdentifier)
        self.tableView.register(ProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: ProfileHeaderView.reuseIdentifier)
        self.tableView.register(PersonalProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: PersonalProfileHeaderView.reuseIdentifier)
    }
    
    func updateContent() {
        guard let session = self.session else { return }
        self.startProgress()
        self.viewModel.user(session) { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success( _):
                self?.tableView.reloadData()
                return
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
                return
            default:
                return
            }
        }
    }
    
}

extension PersonalProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SummarizeCell.reuseIdentifier, for: indexPath) as? SummarizeCell else {
            fatalError("\(SummarizeCell.reuseIdentifier) is not exist")
        }
        cell.setupWith(self.viewModel.summarizeData(for: indexPath))
        if self.viewModel.islastRow(indexPath) {
            cell.updateSeparatorForLastRow()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.viewModel.isUpgradeViewNeeded(for: section) {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: ProfileHeaderView.reuseIdentifier) as? ProfileHeaderView
            view?.upgradeAction = { [weak self] in
                self?.upgradeAccountTapped()
            }
            view?.contentView.backgroundColor = UIColor(.white)
            view?.removeSeparator()
            return view
        }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: PersonalProfileHeaderView.reuseIdentifier) as? PersonalProfileHeaderView
        view?.contentView.backgroundColor = UIColor(.white)
        view?.setTitle(self.viewModel.sectionsContent[section].headerTitle)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.viewModel.isUpgradeViewNeeded(for: section) {
            return Constants.upgradeHeaderHeight
        }
        return Constants.headerHeight
    }
    
}
