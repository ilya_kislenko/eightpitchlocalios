//
//  SecurityViewController.swift
//  8Pitch
//
//  Created by 8pitch on 29.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class SecurityViewController: CustomTitleViewController {
    
    private lazy var viewModel : SecurityViewModel = {
        SecurityViewModel(session: session)
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "profile.security.title".localized
        self.customTitle = "profile.security.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateContent()
    }
    
}

extension SecurityViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.viewModel.isProfileCell(indexPath),
           let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCell.reuseIdentifier, for: indexPath) as? ProfileCell {
            cell.setupFor(parameter: self.viewModel.parameter(indexPath))
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileSwitchCell.reuseIdentifier, for: indexPath) as? ProfileSwitchCell else {
            fatalError("TableView setup is impossible")
        }
        cell.setupFor(.twoFA)
        cell.setState(self.viewModel.is2FAEnabled)
        cell.switchToggled = { [weak self] enabled in
            self?.toggle2FA(enabled)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.viewModel.isProfileHeaderView(section) {
            let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: ProfileHeaderView.reuseIdentifier) as? ProfileHeaderView
            view?.upgradeAction = { [weak self] in
                self?.upgradeAccountTapped()
            }
            view?.hideSeparator()
            return view
        }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: PersonalProfileHeaderView.reuseIdentifier) as? PersonalProfileHeaderView
        view?.setTitle(self.viewModel.titleForHeader(section))
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: SettingsHeaderView.reuseIdentifier) as? SettingsHeaderView
        view?.setText("profile.security.footer.2fa".localized)
        view?.hideSeparator()
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        self.viewModel.isLast(section) ? Constants.headerHeight : 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.viewModel.isProfileHeaderView(section) {
            return Constants.upgradeHeaderHeight
        } else if self.viewModel.isFirst(section) {
            return Constants.topHeaderHeight
        }
        return Constants.headerHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let cellType = SecurityCellType(rawValue: self.viewModel.isQuestionaryPassed ? indexPath.section : indexPath.section - 1)
        switch cellType {
        case .password:
            self.openChangePasswordScreen()
        case .login:
            openQuickLogin()
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cellHeight
    }
    
}

private extension SecurityViewController {
    
    func updateContent() {
        self.startProgress()
        self.viewModel.user() { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success( _):
                self?.tableView.reloadData()
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
            }
        }
    }
    
    func toggle2FA(_ enabled: Bool) {
        if enabled {
            self.openTwoFAFlow()
        } else {
            self.openChooseTwoFAMethodScreen()
        }
    }
    
    func openChooseTwoFAMethodScreen() {
        _ = ViewControllerProvider.setupChooseSecurityMethodViewController(rootViewController: self)
        self.nextController?.cameFromProfile = true
        self.nextController?.hidesBottomBarWhenPushed = true
        self.pushNextController(animated: true)
    }
    
    enum Constants {
        static let topHeaderHeight: CGFloat = 40
        static let headerHeight: CGFloat = 88
        static let upgradeHeaderHeight: CGFloat = 141
        static let cellHeight: CGFloat = 48
    }
    
    func registerCells() {
        self.tableView.register(ProfileCell.nibForRegistration, forCellReuseIdentifier: ProfileCell.reuseIdentifier)
        self.tableView.register(ProfileSwitchCell.nibForRegistration, forCellReuseIdentifier: ProfileSwitchCell.reuseIdentifier)
        self.tableView.register(ProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: ProfileHeaderView.reuseIdentifier)
        self.tableView.register(SettingsHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: SettingsHeaderView.reuseIdentifier)
        self.tableView.register(PersonalProfileHeaderView.nibForRegistration, forHeaderFooterViewReuseIdentifier: PersonalProfileHeaderView.reuseIdentifier)
    }
    
    func openChangePasswordScreen() {
        _ = ViewControllerProvider.setupConfirmPasswordViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
    
    func openQuickLogin() {
        _ = ViewControllerProvider.setupQuickLoginViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}
