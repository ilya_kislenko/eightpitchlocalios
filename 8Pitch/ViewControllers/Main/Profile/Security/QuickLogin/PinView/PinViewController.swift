//
//  PinViewController.swift
//  8Pitch
//
//  Created by 8pitch on 11/25/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PinViewController: RedViewController {
    
    @IBOutlet weak var titleLabel: CustomSubtitleDarkLabel!
    @IBOutlet weak var pinView: PinView!
    @IBOutlet weak var keyboardView: KeyboardView!
    
    public var withBiometry: Bool = false
    public var fromLogin: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.keyboardView.savePin = { [weak self] pin in
            self?.setupPinView(for: pin)
        }
    }
    
    func setupPinView(for pin: String) {
        self.pinView.setupFor(pin.count)
        self.checkPin(pin)
    }
    
    func clearPin() {
        self.keyboardView.clearPin()
    }
    
    func checkPin(_ pin: String) {}
    
    func openNext() {}
}
