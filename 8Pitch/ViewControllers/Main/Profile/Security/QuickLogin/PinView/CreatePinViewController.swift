//
//  CreatePinViewController.swift
//  8Pitch
//
//  Created by 8pitch on 11/25/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class CreatePinViewController: PinViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text =  "quicklLogin.pin.create.customTitle".localized
    }
    
    override func checkPin(_ pin: String) {
        super.checkPin(pin)
        self.session?.quickLoginProvider.createPin = pin
        if pin.count == 4 {
            self.openNext()
        }
    }
    
    override func openNext() {
        _ = ViewControllerProvider.setupRepeatPinViewController(rootViewController: self, withBiometry: self.withBiometry)
        self.pushNextController(animated: true)
    }
    
}
