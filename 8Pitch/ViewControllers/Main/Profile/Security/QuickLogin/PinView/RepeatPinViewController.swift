//
//  RepeatPinViewController.swift
//  8Pitch
//
//  Created by 8pitch on 11/26/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class RepeatPinViewController: PinViewController {
    
    private lazy var viewModel : PinViewModel = {
        PinViewModel(session: session)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text =  "quicklLogin.pin.repeat.customTitle".localized
    }
    
    override func checkPin(_ pin: String) {
        super.checkPin(pin)
        guard let session = self.session else { return }
        session.quickLoginProvider.repeatPin = pin
        if pin.count == 4 {
            self.savePin()
        }
    }
    
    override func openNext() {
        super.openNext()
        let biometryLoginType: SuccessQuickLoginViewController.LoginType = BiometryManager.shared.biometryType == .touchID ? .touchID : .faceID
        _ = ViewControllerProvider.setupSuccessQuickLoginViewController(rootViewController: self, for: self.withBiometry ? biometryLoginType : .pin)
        self.pushNextController(animated: true)
    }
    
    private func savePin() {
        guard let session = self.session,
              session.quickLoginProvider.pinsMatched() else {
            self.clearPin()
            self.presentAlert(message: "quicklLogin.pin.error.match".localized)
            return
        }
        if self.withBiometry {
            self.enableBiometry()
        } else {
            self.openNext()
        }
    }
    
    private func enableBiometry() {
        self.viewModel.enableBiometry(completion: { [weak self] error in
            guard let _ = error else {
                self?.openNext()
                return
            }
            self?.previousController = self?.previousController?.previousController
            self?.popToPreviousController(animated: true)
        })
    }
    
}
