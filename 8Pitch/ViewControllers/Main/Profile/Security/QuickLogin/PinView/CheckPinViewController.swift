//
//  CheckPinViewController.swift
//  8Pitch
//
//  Created by 8pitch on 11/26/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class CheckPinViewController: PinViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = "quicklLogin.pin.checkPin.customTitle".localized
    }
    
    override func checkPin(_ pin: String) {
        super.checkPin(pin)
        guard let session = self.session else { return }
        session.quickLoginProvider.createPin = pin
        if pin.count == 4 {
            guard session.quickLoginProvider.pinsMatched() else {
                self.clearPin()
                self.presentAlert(message: "quicklLogin.pin.error.invalid".localized)
                return
            }
            session.quickLoginProvider.isPinEnabled = false
            self.openNext()
        }
    }
    
    override func openNext() {
        super.openNext()
        self.session?.quickLoginProvider.cleanUp()
        self.popToPreviousController(animated: true)
    }
}
