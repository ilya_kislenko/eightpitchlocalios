//
//  PinViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 15.12.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class PinViewModel: GenericViewModel {
    
    func enableBiometry(completion: @escaping ErrorClosure) {
        BiometryManager.shared.enableBiometry(completion: { error in
            DispatchQueue.main.async {
                completion(error)
            }
        })
    }
}
