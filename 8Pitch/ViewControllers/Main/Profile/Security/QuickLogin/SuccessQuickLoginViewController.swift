//
//  SuccessQuickLoginViewController.swift
//  8Pitch
//
//  Created by 8pitch on 11/25/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class SuccessQuickLoginViewController: CustomTitleViewController {
    
    enum LoginType: String {
        case pin = "pinCode"
        case touchID = "touchID"
        case faceID = "faceID"
    }
    
    enum Constants {
        static let loginType: String = "profile.switchCell."
        static let faceIDTitle: String = "security.quicklLogin.faceID.enable.title".localized
        static let touchIDTitle: String = "security.quicklLogin.touchID.enable.title".localized
        static let pinCodeTitle: String = "security.quicklLogin.pinCode.enable.title".localized
        static let subtitle: String = "security.quicklLogin.enable.subtitle".localized
        static let image: String = "Success"
    }
    
    var type: LoginType = .pin
    public var fromLogin: Bool = false
    
    @IBOutlet weak var screenImageView: UIImageView!
    @IBOutlet weak var titleLabel: CustomSubtitleLabel!
    @IBOutlet weak var subtitleLabel: InfoCurrencyLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.setupContent()
        self.saveState()
    }
    
    private func setupNavigationBar() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(self.closeFlow))
        self.navigationItem.rightBarButtonItem = nil
    }
    
    @IBAction func okTapped(_ sender: UIButton) {
        self.closeFlow()
    }
    
    private func setupContent() {
        self.screenImageView.image = UIImage(named: self.type.rawValue + Constants.image)
        var text: String
        switch type {
        case .faceID:
            text = Constants.faceIDTitle
        case .touchID:
            text = Constants.touchIDTitle
        case .pin:
            text = Constants.pinCodeTitle
        }
        self.titleLabel.text = text
        self.subtitleLabel.text = Constants.subtitle
    }
    
    private func saveState() {
        self.session?.quickLoginProvider.isPinEnabled = true
        self.session?.saveTokens()
        BiometryManager.shared.isBiometryEnabled = self.type == .pin ? false : true
    }
    
    @objc private func closeFlow() {
        self.session?.quickLoginProvider.cleanUp()
        if self.fromLogin {
            self.openMainApp()
        } else {
            self.previousController = self.previousController?.previousController?.previousController
            self.popToPreviousController(animated: true)
        }
    }
}
