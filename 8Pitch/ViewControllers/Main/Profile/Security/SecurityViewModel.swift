//
//  SecurityViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 29.09.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

public enum SecurityCellType: Int {
    case password
    case login
    case twoFA
}

class SecurityViewModel: GenericViewModel {
    
    var smsTwoFAEnabled: Bool {
        !self.session.verificationTypes.contains(.totp) && self.session.twoFAEnabled
    }
    
    var numberOfSections: Int {
        self.isQuestionaryPassed ? 3 : 4
    }
    
    var is2FAEnabled: Bool {
        self.session.user?.twoFAEnabled ?? false
    }
    
    func isLast(_ section: Int) -> Bool {
        section == self.numberOfSections - 1
    }
    
    func isFirst(_ section: Int) -> Bool {
        (section == 1 && !self.isQuestionaryPassed)
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        (!self.isQuestionaryPassed && section == 0) ? 0 : 1
    }
    
    func isProfileCell(_ indexPath: IndexPath) -> Bool {
        self.isQuestionaryPassed ? (indexPath.section <= 1) : (indexPath.section <= 2)
    }
    
    func isProfileHeaderView(_ section: Int) -> Bool {
        !self.isQuestionaryPassed && (section == 0)
    }
    
    func titleForHeader(_ section: Int) -> String {
        let headers = ["profile.security.header.personal", "profile.security.header.login", "profile.security.header.2FA"]
        let index = self.isQuestionaryPassed ? section : section - 1
        return headers[index].localized
    }
    
    func parameter(_ indexPath: IndexPath) -> ProfileCell.Parameter {
        let parameters: [ProfileCell.Parameter] = [.password, .login]
        return parameters[indexPath.section - (self.isQuestionaryPassed ? 0 : 1)]
    }

}
