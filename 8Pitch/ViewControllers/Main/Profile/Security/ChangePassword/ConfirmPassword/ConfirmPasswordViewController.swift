//
//  ConfirmPasswordViewController.swift
//  8Pitch
//
//  Created by 8pitch on 10/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class ConfirmPasswordViewController: AbstractCollectionViewController<Login> {
    private lazy var viewModel : ConfirmPasswordViewModel = {
        ConfirmPasswordViewModel(session: session)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "changePassword.confirm.title".localized
        self.customTitle = "changePassword.confirm.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override var model: BehaviorSubject<Login>? {
        self.session?.verifyPasswordProvider
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        try? self.model?.value().passwordProvider.value()?.validateSingleMode()
                        guard let isValid = try? self.model?.value().passwordProvider.value()?.isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            self.confirmPassword()
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var navigationFooterViewType : PublishSubject<NavigationView.ViewType?>? {
        didSet {
            guard let typeAction = self.navigationFooterViewType else { return }
            typeAction.onNext(.standart)
        }
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(InputCurrentPasswordCell.nibForRegistration, forCellWithReuseIdentifier: InputCurrentPasswordCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = NavigationView.reuseIdentifier
        layout.register(NavigationView.nibForRegistration, forDecorationViewOfKind: NavigationView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: PasswordError.self, cellReuseIdentifier: InputCurrentPasswordCell.reuseIdentifier)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
        
    override func nextButtonTapped() {
        _ = ViewControllerProvider.setupSetNewPasswordViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}

private extension ConfirmPasswordViewController {
    func confirmPassword() {
        self.startProgress()
        self.viewModel.confirmPassword() { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.nextButtonTapped()
                return
            }
            self?.presentAlert(message: error.localizedDescription)
        }
    }
}

