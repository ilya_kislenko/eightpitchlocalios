//
//  ConfirmPasswordViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 10/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class ConfirmPasswordViewModel: GenericViewModel {
    func confirmPassword(completionHandler: @escaping ErrorClosure) {
        self.remote.verifyPassword(self.session, completionHandler: completionHandler)
    }
}
