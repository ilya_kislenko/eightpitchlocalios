//
//  PasswordChangedViewController.swift
//  8Pitch
//
//  Created by 8pitch on 10/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit

class PasswordChangedViewController: CustomTitleViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.close), style: .plain, target: self, action: #selector(closeButtonTapped))
        self.navigationItem.rightBarButtonItem = nil
    }
    
    @IBAction func okTapped(_ sender: UIButton) {
        self.popToPreviousController(animated: true)
    }
}

private extension PasswordChangedViewController {
    @objc func closeButtonTapped() {
        self.popToPreviousController(animated: true)
    }
}
