//
//  SetNewPasswordViewController.swift
//  8Pitch
//
//  Created by 8pitch on 10/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class SetNewPasswordViewController: AbstractCollectionViewController<Password> {
    private lazy var viewModel : SetNewPasswordViewModel = {
        SetNewPasswordViewModel(session: session)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "changePassword.confirm.title".localized
        self.customTitle = "changePassword.setNew.customTitle".localized
        self.navigationItem.rightBarButtonItem = nil
    }
    
    override var model: BehaviorSubject<Password>? {
        self.session?.changePasswordProvider
    }
    
    override var nextButtonAction: BehaviorSubject<NextState?>? {
        didSet {
            guard let action = self.nextButtonAction else { return }
            action
                .compactMap { $0 }
                .subscribe(onNext: {
                    self.stopProgress()
                    switch $0 {
                    case .error: return // TODO: show error
                    case .loading:
                        guard let isValid = try? self.model?.value().isValid.value() else { return }
                        switch isValid {
                        case .failure(let error): self.presentAlert(message: error.localizedDescription)
                        case .success(let value):
                            guard value else { return }
                            self.changeOrUpdatePassword()
                        }
                    case .next: self.verifyModelAndNextButtonActionIfPossible()
                    }
                }).disposed(by: self.nextButtonActionDisposeBag)
        }
    }
    
    override var applyChangesFooterViewType : PublishSubject<ApplyChangesView.ViewType?>? {
        didSet {
            guard let typeAction = self.applyChangesFooterViewType else { return }
            typeAction.onNext(.changePassword)
        }
    }
    
    override func registerCells() {
        super.registerCells()
        guard let layout = self.collectionViewLayout else {
            fatalError("invalid controller setup!")
        }
        self.collectionView.register(ChangePasswordCell.nibForRegistration, forCellWithReuseIdentifier: ChangePasswordCell.reuseIdentifier)
        self.collectionView.register(ConfirmPasswordCell.nibForRegistration, forCellWithReuseIdentifier: ConfirmPasswordCell.reuseIdentifier)
        layout.bottomFooterReuseIdentifier = ApplyChangesView.reuseIdentifier
        layout.register(ApplyChangesView.nibForRegistration, forDecorationViewOfKind: ApplyChangesView.reuseIdentifier)
    }
    
    override func setupDataSource() {
        guard let model = self.model else { return }
        let sections = [
            CollectionViewModel(collectionView: self.collectionView, section: 0, model: model, errorType: PasswordError.self, cellReuseIdentifier: ChangePasswordCell.reuseIdentifier),
            CollectionViewModel(collectionView: self.collectionView, section: 1, model: model, errorType: ConfirmPasswordError.self, cellReuseIdentifier: ConfirmPasswordCell.reuseIdentifier)
        ]
        let dataSource = AbstractCollectionViewDataSourceV2(models: sections)
        self.dataSource = dataSource
        super.setupDataSource()
    }
    
    override func nextButtonTapped() {
        _ = ViewControllerProvider.setupPasswordChangedViewController(rootViewController: self)
        self.pushNextController(animated: true)
    }
}

private extension SetNewPasswordViewController {
    
    func changeOrUpdatePassword() {
        guard let session = session else { return }
        if session.updatePasswordNeeded {
            self.updatePassword()
        } else {
            self.changePassword()
        }
    }
    
    func updatePassword() {
        self.startProgress()
        self.viewModel.updatePassword() { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.loadUser()
                return
            }
            self?.presentAlert(message: error.localizedDescription)
        }
    }
    
    func openMainAppOrQuickLogin() {
        if (try? self.session?.loginProvider.value().rememberMeSelected) ?? false {
            self.openSetupQuickLogin()
        } else {
            self.openMainApp()
        }
    }
    
    func loadUser() {
        self.startProgress()
        self.viewModel.user(completionHandler: { [weak self] response in
            switch response {
            case .success(_):
                self?.loadInvestedProjects()
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            }
        })
    }
    
    func loadInvestedProjects() {
        self.viewModel.loadInvestedProjects(completionHandler: { [weak self] error in
            if let error = error {
                self?.stopProgress()
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            } else {
                self?.loadNotifications()
            }
        })
    }
    
    func loadNotifications() {
        self.viewModel.loadNotifications(completionHandler: { [weak self] error in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
                self?.nextButtonAction?.onNext(.error)
            } else {
                self?.openMainAppOrQuickLogin()
            }
        })
    }
    
    func changePassword() {
        self.startProgress()
        self.viewModel.changePassword() { [weak self] error in
            self?.stopProgress()
            guard let error = error else {
                self?.nextButtonTapped()
                return
            }
            self?.presentAlert(message: error.localizedDescription)
        }
    }
    
}
