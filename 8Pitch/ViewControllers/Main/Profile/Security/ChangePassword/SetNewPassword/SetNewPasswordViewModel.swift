//
//  SetNewPasswordViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 10/9/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input

class SetNewPasswordViewModel: GenericViewModel {
    func changePassword(completionHandler: @escaping ErrorClosure) {
        self.remote.changePassword(self.session, completionHandler: completionHandler)
    }
    
    func updatePassword(completionHandler: @escaping ErrorClosure) {
        self.remote.updatePassword(self.session, completionHandler: completionHandler)
    }
}
