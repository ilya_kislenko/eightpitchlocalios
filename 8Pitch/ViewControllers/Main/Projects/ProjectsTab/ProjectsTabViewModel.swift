//
//  ProjectsTabViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 20.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import Remote

class ProjectsTabViewModel: GenericViewModel {
    
    enum TabType {
        case current
        case completed
        case filtered
        case searched
    }
    
    var timer: Timer?
    var type: TabType = .current
    var searchText: String?  {
        didSet {
            filterProjects()
        }
    }
    
    var projects: [Project] = []
    var filteredProjects: [Project] = []
    
    private func filterProjects() {
        guard let searchText = searchText else { return }
        filteredProjects = projects.filter( {($0.companyName?.contains(searchText) ?? false)} )
    }
    
    private func loadProjects(completionHandler: @escaping ErrorClosure) {
        switch type {
        case .completed:
            self.loadCompletedProjects(completionHandler: completionHandler)
        case .current:
            self.loadCurrentProjects(completionHandler: completionHandler)
        case .filtered:
            loadFilteredProjects(completionHandler: completionHandler)
        case .searched:
            loadSearchedProjects(completionHandler: completionHandler)
        }
    }
    
    private func loadSearchedProjects(completionHandler: @escaping ErrorClosure) {
        self.projects = []
        completionHandler(nil)
    }
    
    private func loadFilteredProjects(completionHandler: @escaping ErrorClosure) {
        self.projects = session.projectsFilterProvider.filteredProjects
        completionHandler(nil)
    }
    
    private func loadCompletedProjects(completionHandler: @escaping ErrorClosure) {
        self.remote.projectsWithStatus(session, type: .completed, completionHandler: { error in
            self.projects = self.session.projectsProvider.completedProjects
            completionHandler(error)
        })
    }
    
    private func loadCurrentProjects(completionHandler: @escaping ErrorClosure) {
        self.remote.projectsWithStatus(session, type: .current, completionHandler: { error in
            self.projects = self.session.projectsProvider.currentProjects
            completionHandler(error)
        })
    }
    
    func loadInvestedProjects(_ session: Session?, completionHandler: @escaping ErrorClosure) {
        guard let session = session else { return }
        self.session = session
        self.remote.investedProjects(session) { response, error in
            self.projects = session.projectsProvider.investedProjects
            completionHandler(error)
        }
    }
    
    func openDetailsFor(projectNumber: Int) {
        if !session.projectsSearchProvider.searchedText.isEmpty {
            self.session.projectsProvider.currentProject = self.filteredProjects[projectNumber]
        } else {
            self.session.projectsProvider.currentProject = self.projects[projectNumber]
        }
    }
    
    func loadProjectsFilters(_ session: Session?, completionHandler: @escaping ErrorClosure) {
        guard let session = session else { return }
        self.remote.getSecurityTypes(session) { (error) in
            if let error = error {
                completionHandler(error)
            } else {
                self.remote.getMinimalInvestment(session) { (error) in
                    if let error = error {
                        completionHandler(error)
                    } else {
                        self.loadProjects(completionHandler: completionHandler)
                    }
                }
            }
        }
    }
    
    func getSearchedProjects(_ session: Session?, completionHandler: @escaping ErrorClosure) {
        guard let session = session else { return }
        self.remote.getProjectsSearched(session, queryItems: self.session.projectsSearchProvider.getQueryItems()) { (error) in
            self.filteredProjects = session.projectsSearchProvider.searchedProjects
            completionHandler(error)
        }
    }
    
    func setupFilteredData() {
        if session.projectsFilterProvider.filterWasApplied {
            projects = session.projectsFilterProvider.filteredProjects
            filterProjects()
        }
    }
}
