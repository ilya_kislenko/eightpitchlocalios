//
//  InvestedViewController.swift
//  8Pitch
//
//  Created by 8pitch on 20.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import RxSwift

class ProjectsTabViewController: CustomTitleViewController {
    
    // MARK: Properties
    
    private lazy var viewModel : ProjectsTabViewModel = {
        ProjectsTabViewModel(session: session)
    }()
    
    public var type: ProjectsTabViewModel.TabType {
        get { self.viewModel.type }
        set { self.viewModel.type = newValue }
    }
    
    public var openDetails: VoidClosure?
    private var cellHeight: CGFloat = Constants.cellHeight
    
    // MARK: UI Components
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchResultsTitle: UILabel!
    @IBOutlet weak var searchResultsView: UIView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptySubtitleLabel: UILabel!
    @IBOutlet weak var emtptyViewImage: UIImageView!
    @IBOutlet weak var searchBarView: UIView!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(ProjectCell.nibForRegistration, forCellWithReuseIdentifier: ProjectCell.reuseIdentifier)
        self.searchBar.placeholder = "projects.page.search.bar.placeholder".localized
        self.emptySubtitleLabel.text = "projects.page.empty.results.subtitle".localized
        self.hideKeyboardWhenTappedAround()
        self.collectionView.isHidden = viewModel.type == .searched
        self.emptyView.isHidden = viewModel.type != .searched
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadProjects()
        self.searchBar.showsCancelButton = true
        self.searchBar.tintColor = UIColor(.red)
        self.searchBar.enableCancelButton(in: searchBar)
        self.searchBarView.isHidden = viewModel.type != .searched
        self.searchResultsView.isHidden = viewModel.type != .searched
        self.navigationController?.setNavigationBarHidden(viewModel.type == .searched, animated: true)
        collectionView.reloadData()
    }
    
    override func setupUIForIpad() {
        super.setupUIForIpad()
        self.collectionView.register(ProjectPadCell.nibForRegistration, forCellWithReuseIdentifier: ProjectPadCell.reuseIdentifier)
        self.cellHeight = Constants.padCellHeight
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.leftBarButtonItem = nil
        let font = UIDevice.current.userInterfaceIdiom == .pad ? UIFont.barlow.semibold.largeTitleSize : UIFont.barlow.semibold.currencyInputSize
        self.navigationController?.navigationBar.titleTextAttributes = [.font: font,
                                                                        .foregroundColor: UIColor(.gray),
                                                                        .kern: -0.5]
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationItem.customizeForWhite()
    }
}

private extension ProjectsTabViewController {
    func loadProjectDetails() {
        self.startProgress()
        self.viewModel.loadProjectInvestmentData() { (result) in
            self.stopProgress()
            switch result {
            case .failure(let error):
                self.presentAlert(message: error.localizedDescription)
            case .success(_):
                self.openProjectDetails()
            }
        }
    }
    
    func openProjectDetails() {
        if let projectDetailsRootViewController = ViewControllerProvider.setupProjectDetailsRootViewController(rootViewController: self) {
            self.navigationController?.pushViewController(projectDetailsRootViewController, animated: true)
        }
    }
}

// MARK: UICollectionViewDelegate, UICollectionViewDataSource

extension ProjectsTabViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch viewModel.type {
        case .searched:
            if let searchText = session?.projectsSearchProvider.searchedText {
                searchResultsView.isHidden = searchText.isEmpty
                searchResultsTitle.text = String(format: "projects.page.search.results.string".localized, self.viewModel.filteredProjects.count, searchText)
                self.emptySubtitleLabel.text = searchText.isEmpty ? "filter.projects.type.chars".localized : "projects.page.empty.results.subtitle".localized
                self.emtptyViewImage.isHidden = searchText.isEmpty
            }
            self.collectionView.isHidden = self.viewModel.filteredProjects.isEmpty
            self.emptyView.isHidden = !self.viewModel.filteredProjects.isEmpty
            
            return self.viewModel.filteredProjects.count
        case .current, .completed, .filtered:
            if let isFilterSelected = self.session?.projectsFilterProvider.isFiltersSelected() {
                self.collectionView.isHidden = self.viewModel.projects.isEmpty && isFilterSelected
                self.emptyView.isHidden = !(self.viewModel.projects.isEmpty && isFilterSelected)
            }
            searchResultsView.isHidden = true
            return self.viewModel.projects.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var projectCell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectCell.reuseIdentifier, for: indexPath) as? ProjectCell
        if UIDevice.current.userInterfaceIdiom == .pad {
            projectCell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectPadCell.reuseIdentifier, for: indexPath) as? ProjectPadCell
        }
        guard let cell = projectCell else {
            fatalError("ProjectCell is not exist")
        }
        cell.delegate = self
        switch viewModel.type {
        case .searched:
            cell.setupFor(self.viewModel.filteredProjects[indexPath.row])
        case .current, .completed, .filtered:
            cell.setupFor(self.viewModel.projects[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel.openDetailsFor(projectNumber: indexPath.row)
        switch viewModel.type {
        case .searched:
            self.loadProjectDetails()
        case .current, .completed, .filtered:
            self.openDetails?()
        }
    }
    
}

// MARK: UICollectionViewDelegateFlowLayout

extension ProjectsTabViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.frame.width - Constants.cellWidthInsetSum, height: self.cellHeight)
    }
    
}

// MARK: Private

private extension ProjectsTabViewController {
    
    enum Constants {
        static let cellHeight: CGFloat = 528
        static let padCellHeight: CGFloat = 408
        static let cellWidthInsetSum: CGFloat = 32
    }
    
    func loadProjects() {
        self.startProgress()
        self.viewModel.loadProjectsFilters(self.session){ [weak self] (error) in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.collectionView.reloadData()
            }
        }
    }
}

extension ProjectsTabViewController: LoadProjectDetailsImageDelegate {
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView, completionHandler: VoidClosure?) {
        self.viewModel.loadProjectDetailsImageData(ID, imageView)
    }
    
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView) {
        self.viewModel.loadProjectDetailsImageData(ID, imageView)
    }
}

extension ProjectsTabViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.session?.projectsSearchProvider.searchedText = searchText
        self.viewModel.timer?.invalidate()
        if searchText.isEmpty {
            self.viewModel.filteredProjects.removeAll()
            collectionView.reloadData()
        } else {
            self.viewModel.timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.getSearchedProjects), userInfo: nil, repeats: false)
        }
    }
    
    @objc func getSearchedProjects() {
        self.startProgress()
        self.viewModel.getSearchedProjects(session) { [weak self] (error) in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.collectionView.reloadData()
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        session?.projectsSearchProvider.resetFilter()
        backTapped()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.enableCancelButton(in: searchBar)
    }
}
