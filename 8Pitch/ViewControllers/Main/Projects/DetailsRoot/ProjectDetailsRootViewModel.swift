//
//  ProjectDetailsRootViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/17/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

class ProjectDetailsRootViewModel: GenericViewModel {
    var hasConstructorInfo: Bool {
        return self.session.projectsProvider.expandedProject?.projectPage?.blockConstructor?.tabs?.compactMap({$0}).first != nil
    }
}
