//
//  ProjectDetailsRootViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/17/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import UIKit

class ProjectDetailsRootViewController: UIPageViewController, Presentatable {
    var session : Session?
    
    private lazy var viewModel : ProjectDetailsRootViewModel = {
        ProjectDetailsRootViewModel(session: session)
    }()
    
    var nextController: GenericViewController?
    var previousController: GenericViewController?
    
    fileprivate var availableViewControllers: [UIViewController]?
    
    private var statusBarStyle: UIStatusBarStyle = .lightContent
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        self.statusBarStyle
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.viewModel.hasConstructorInfo {
            self.delegate = self
            self.dataSource = self
        }
        self.setupPages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBar()
        if let availableViewControllers = self.availableViewControllers,
           availableViewControllers.count > 0,
           let _ = self.viewControllers?.first as? ProjectDetailsConstructorViewController {
            self.navigationItem.customizeForWhite()
            self.statusBarStyle = .default
            self.setNeedsStatusBarAppearanceUpdate()
        } else {
            self.navigationItem.customizeForColored()
            self.statusBarStyle = .lightContent
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.barStyle = .default
    }
    
    private func setupPages() {
        guard let previousController = self.previousController,
              let detailsViewController = ViewControllerProvider.setupProjectDetailsViewController(rootViewController: previousController) else { return }
        self.setViewControllers([detailsViewController], direction: .forward, animated: true, completion: nil)
        
        if self.viewModel.hasConstructorInfo,
           let detailsConstructorViewController = ViewControllerProvider.setupProjectDetailsConstructorViewController(rootViewController: previousController) {
            self.availableViewControllers = [detailsViewController, detailsConstructorViewController]
        }
    }
}

private extension ProjectDetailsRootViewController {
    enum Constants {
        static let logoWidth: CGFloat = 70
        static let logoHeight: CGFloat = 27
    }
    
    func setupNavigationBar() {
        if let imageIdentifier = self.session?.projectsProvider.currentProject?.companyLogo {
            let companyLogoImageView = UIImageView(image: UIImage())
            companyLogoImageView.contentMode = .scaleAspectFit
            let view = UIView(frame: CGRect(x: 0, y: 0, width: Constants.logoWidth, height: Constants.logoHeight))
            companyLogoImageView.frame = view.bounds
            view.addSubview(companyLogoImageView)
            self.navigationItem.titleView = view
            self.viewModel.loadProjectDetailsImageData(imageIdentifier, companyLogoImageView)
        }
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.arrowBack), style: .plain, target: self, action: #selector(backTapped))
        self.navigationController?.navigationBar.titleTextAttributes = [.font: UIFont.barlow.regular.titleSize, .foregroundColor: UIColor(.gray)]
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.barStyle = .black
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationItem.customizeForColored()
    }
    
    @objc func backTapped() {
        self.popToPreviousController(animated: true)
    }
}

extension ProjectDetailsRootViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            if let availableViewControllers = self.availableViewControllers,
               availableViewControllers.count > 0,
               let _ = previousViewControllers.firstIndex(of: availableViewControllers[0]) {
                self.navigationItem.customizeForWhite()
                self.statusBarStyle = .default
                self.setNeedsStatusBarAppearanceUpdate()
            } else {
                self.navigationItem.customizeForColored()
                self.statusBarStyle = .lightContent
                self.setNeedsStatusBarAppearanceUpdate()
            }
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let availableViewControllers = self.availableViewControllers,
              availableViewControllers.count > 0,
              let _ = viewController as? ProjectDetailsConstructorViewController else { return nil }
        return availableViewControllers[0]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let availableViewControllers = self.availableViewControllers,
              availableViewControllers.count > 1,
              let _ = viewController as? ProjectDetailsViewController else { return nil }
        return availableViewControllers[1]
    }
}

extension ProjectDetailsRootViewController: PresentationStrategy {
    func pushNextController(animated: Bool) {
        guard let controller = self.nextController else {
            print("self.nextController == nil")
            return
        }
        self.navigationController?.pushViewController(controller, animated: animated)
    }
    
    func popToPreviousController(animated: Bool) {
        guard let controller = self.previousController else {
            print("self.previousController == nil")
            return
        }
        self.navigationController?.popToViewController(controller, animated: animated)
    }
}
