//
//  ProjectFilterViewController.swift
//  8Pitch
//
//  Created by 8Pitch on 19.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import RxSwift
import MobileCoreServices

class ProjectFilterViewController: CustomTitleViewController {
    
    private lazy var viewModel : ProjectFilterViewModel = {
        ProjectFilterViewModel(session: session)
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var acceptButton: RoundedButton!
    
    public var type: ProjectFilterViewModel.TabType {
        get { self.viewModel.type }
        set { self.viewModel.type = newValue }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.barlow.semibold.titleSize, NSAttributedString.Key.foregroundColor: UIColor(.gray)]
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.arrowBack), style: .plain, target: self, action: #selector(backTapped))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "project.filter.summary.reset.button".localized.uppercased(), style: .plain, target: self, action: #selector(resetFilter))
        switch viewModel.type {
        case .status:
            self.title = "project.status.title".localized.uppercased()
        case .investmentAmount:
            self.title = "project.invest.amount.title".localized.uppercased()
            viewModel.availableKeys = session?.projectsFilterProvider.availableInvestmentAmount?.sorted(by:  < ) ?? []
        case .typesOfSecurity:
            self.title = "project.financial.instrument.title".localized.uppercased()
            viewModel.availableKeys = session?.projectsFilterProvider.availableSecurityTypes?.sorted(by: <) ?? []
        }
        acceptButton.setTitle("project.filter.apply.button".localized, for: .normal)
        setupTableView()
        setupTableData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.customizeForFilters()
    }
    
    @IBAction func acceptButtonClick(_ sender: Any) {
        viewModel.acceptFilter()
        backTapped()
    }
    
    @objc private func resetFilter() {
        viewModel.resetFilter()
        tableView.reloadData()
    }
    
    func setupTableData() {
        viewModel.setupSelectedFilter()
        tableView.reloadData()
    }

    func setupTableView() {
        self.tableView.register(ProjectFilterElementCell.nibForRegistration, forCellReuseIdentifier: ProjectFilterElementCell.reuseIdentifier)
        tableView.separatorStyle = .none
    }
    
}

extension ProjectFilterViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch viewModel.type {
            case .status:
                return viewModel.availableStatuses.count
            case .investmentAmount, .typesOfSecurity:
                return viewModel.availableKeys.count
                
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel.type {
        case .status:
            let cell = tableView.dequeueReusableCell(withIdentifier: ProjectFilterElementCell.reuseIdentifier, for: indexPath) as! ProjectFilterElementCell
            DispatchQueue.main.async {
                cell.setup(title: self.viewModel.availableStatuses[indexPath.row].rawValue, type: self.viewModel.type, isItemSelected: self.viewModel.selectedStatuses.contains(self.viewModel.availableStatuses[indexPath.row]))
            }
            cell.selectionStyle = .none
            return cell
        case .investmentAmount, .typesOfSecurity:
            let cell = tableView.dequeueReusableCell(withIdentifier: ProjectFilterElementCell.reuseIdentifier, for: indexPath) as! ProjectFilterElementCell
            DispatchQueue.main.async {
                cell.setup(title: self.viewModel.availableKeys[indexPath.row], type: self.viewModel.type, isItemSelected: self.viewModel.selectedKeys.contains(self.viewModel.availableKeys[indexPath.row]))
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch viewModel.type {
        case .status:
            viewModel.selectFilter(status: viewModel.availableStatuses[indexPath.row])
        case .investmentAmount, .typesOfSecurity:
            viewModel.selectFilter(key: viewModel.availableKeys[indexPath.row])
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }

}
