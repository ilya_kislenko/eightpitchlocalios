//
//  ProjectFilterViewModel.swift
//  8Pitch
//
//  Created by 8Pitch  on 19.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import Input

class ProjectFilterViewModel: GenericViewModel {
    var availableKeys: [String] = []
    var availableStatuses: [ProjectStatus] = [.comingSoon, .active, .finished]
    
    var selectedKeys: [String] = []
    var selectedStatuses: [ProjectStatus] = []
    
    enum TabType {
        case status
        case investmentAmount
        case typesOfSecurity
    }
    
    var type: TabType = .status
    
    func setupSelectedFilter() {
        switch type {
        case .status:
            selectedStatuses = session.projectsFilterProvider.selectedStatuses
        case .investmentAmount:
            selectedKeys = session.projectsFilterProvider.selectedInvesmentAmount
        case .typesOfSecurity:
            selectedKeys = session.projectsFilterProvider.selectedSecurityTypes
        }
    }
    
    func resetFilter() {
        switch type {
        case .status:
            selectedStatuses = []
        case .investmentAmount, .typesOfSecurity:
            selectedKeys = []
        }
    }
    
    func acceptFilter() {
        switch type {
        case .status:
            session.projectsFilterProvider.selectedStatuses = selectedStatuses.sorted(by: { $0.rawValue < $1.rawValue })
        case .investmentAmount:
            session.projectsFilterProvider.selectedInvesmentAmount = selectedKeys.sorted(by: <)
        case .typesOfSecurity:
            session.projectsFilterProvider.selectedSecurityTypes = selectedKeys.sorted(by: <)
        }
    }
    
    func selectFilter(key: String) {
        if let index = selectedKeys.firstIndex(of: key) {
            selectedKeys.remove(at: index)
        } else {
            selectedKeys.append(key)
        }
    }
    
    func selectFilter(status: ProjectStatus) {
        if let index = selectedStatuses.firstIndex(of: status) {
            selectedStatuses.remove(at: index)
        } else {
            selectedStatuses.append(status)
        }
    }
}
