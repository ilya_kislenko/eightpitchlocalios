//
//  ProjectDetailsConstructorViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 9/17/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import Input
import Remote
import UIKit

public enum LoadedDataConversionError : Error {

    case InvalidDataFormat
    case InvalidImageID
    case InvalidVideoID
    case InvalidVideoURL

    public var errorDescription : String? {
        switch self {
        case .InvalidDataFormat:
            return "Invalid Image Format"
        case .InvalidImageID:
            return "Invalid Image identifier"
        case .InvalidVideoID:
            return "Invalid Video identifier"
        case .InvalidVideoURL:
            return "Invalid Video URL"
        }
    }
}

class ProjectDetailsConstructorViewModel: GenericViewModel {
    var blockConstructor: BlockConstructor? {
        return self.session.projectsProvider.expandedProject?.projectPage?.blockConstructor
    }

    var tabs: [ProjectPageTab?] {
        return self.blockConstructor?.tabs?.filter() {($0?.blocks.count ?? 0) > 0} ?? []
    }

    func block(indexPath: IndexPath) -> ProjectPageBlock? {
        guard self.tabs.indices.contains(indexPath.section),
              ((self.tabs[indexPath.section]?.blocks.indices.contains(indexPath.row)) != nil) else {
            return nil
        }
        return self.tabs[indexPath.section]?.blocks[indexPath.row]
    }

    func requestProjectDetailsVideoURL (itemID: String, _ completion: @escaping ResponseLinkClosure) {
        self.remote.vimeoVideoURL(itemID) { (result) in
            switch result {
            case .success(let link):
                completion(.success(link))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func checkInvestButton() -> Bool {
        
        guard let project = session.projectsProvider.expandedProject, let statusString = project.projectStatus, let status = ProjectStatus(rawValue: statusString), let pageStatusString = project.projectPage?.projectPageStatus, let pageStatus = ProjectPageStatus(rawValue: pageStatusString) else { return true }
        switch pageStatus {
        case .published:
            switch status {
            case .active,
                 .comingSoon:
                switch session.level {
                case .second:
                    return false
                case .KYCPassed,
                     .WEBIDPassed,
                     .emailConfirmed,
                     .first,
                     .questionaryPassed,
                     .twoFAEnabled,
                     .webIDInProgress:
                    return true
                }
            case .finished,
                 .manualRelease,
                 .notStarted,
                 .refunded,
                 .tokensTransferred,
                 .waitBlockchain,
                 .waitRelease:
                return true
            }
        case .approved,
             .draft,
             .rejected,
             .waiting:
            return true
        }
    }
}
