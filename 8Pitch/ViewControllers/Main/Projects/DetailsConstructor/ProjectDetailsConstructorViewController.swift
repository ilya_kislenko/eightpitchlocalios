//
//  ProjectDetailsConstructorViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/17/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import AVKit
import AVFoundation

protocol LoadProjectDetailsImageDelegate: class {
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView, completionHandler: VoidClosure?)
}

extension LoadProjectDetailsImageDelegate {
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView) {
        loadProjectDetailsImage(ID, imageView, completionHandler: nil)
    }
}

fileprivate enum Constants {
    static let indicatorOffset: CGFloat = -indicatorHeight
    static let indicatorHeight: CGFloat = 2
    static let verticalOffsetsImageLink: CGFloat = 71
    static let verticalOffsetsTiledImage: CGFloat = 55
    static let horizontalOffsets: CGFloat = 32
    static let horizontalOffsetsForPad: CGFloat = 40
    static let expectedImageWidth: CGFloat = 343
    static let expectedImageHeight: CGFloat = 224
    static let iconsHeight: CGFloat = 39
}

class ProjectDetailsConstructorViewController: GenericViewController {
    @IBOutlet var segmentedControl: ProjectsSegmentedControl!
    
    @IBOutlet var constructorTableView: UITableView!
    @IBOutlet weak var investNowButton: UIButton!
    
    @IBOutlet var segmentScrollView: UIScrollView!
    private lazy var viewModel : ProjectDetailsConstructorViewModel = {
        ProjectDetailsConstructorViewModel(session: session)
    }()
    
    private let indicator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(.red)
        return view
    }()
    
    fileprivate var segmentWasSelectedManually: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.segmentedControl.apportionsSegmentWidthsByContent = true
        self.registerCells()
        self.view.addSubview(indicator)
        self.setupSegmentedControl(segmentedControl: segmentedControl)
        self.setDefaultConstraints(for: self.indicator, dependsOn: self.segmentedControl)
        self.setupInvestNowButton()
    }
    
    override func setupUIForIpad() {
        super.setupUIForIpad()
        self.segmentedControl.customizeForPad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.handleSegmentSelection(control: self.segmentedControl, indicator: self.indicator)
    }
    
    private func registerCells() {
        self.constructorTableView.register(ProjectDetailsMainFactsTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsMainFactsTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsTextTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsTextTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsImageTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsImageTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsTextAndImageTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsTextAndImageTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsTextAndImagePadTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsTextAndImagePadTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsListTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsListTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsFAQTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsFAQTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsTableTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsTableTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsIconsTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsIconsTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsIconsPadTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsIconsPadTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsVideoTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsVideoTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsDividerTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsDividerTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsFactsTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsFactsTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsTilesImagesTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsTilesImagesTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsDualColumnImagesTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsDualColumnImagesTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsImageLinkTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsImageLinkTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsHeaderTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsHeaderTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsDualColumnTextTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsDualColumnTextTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsDualColumnTextPadTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsDualColumnTextPadTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsButtonTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsButtonTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsDisclaimerTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsDisclaimerTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsDepartmentTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsDepartmentTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsDepartmentPadTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsDepartmentPadTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsLinkedTextTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsLinkedTextTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsTiledImagesTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsTiledImagesTableViewCell.reuseIdentifier)
        self.constructorTableView.register(ProjectDetailsChartTableViewCell.nibForRegistration, forCellReuseIdentifier: ProjectDetailsChartTableViewCell.reuseIdentifier)
    }
    
    
    @IBAction func investButtonClick(_ sender: Any) {
        self.investButtonAction()
    }
    
    private func investButtonAction() {
        self.startProgress()
        self.viewModel.loadProjectInvestmentData() { [weak self] (result) in
            guard let self = self else { return }
            self.stopProgress()
            switch result {
            case .failure(let error):
                self.presentAlert(message: error.localizedDescription)
            case .success(let status):
                switch status {
                case .canceldInProgress,
                     .new,
                     .pending:
                    self.investButtonAction()
                    return
                case .paid:
                    let _ = ViewControllerProvider.setupProcessedInvestmentViewController(rootViewController: self)
                case .booked:
                    let _ = ViewControllerProvider.setupUnfinishedInvestmentViewController(rootViewController: self)
                case .none:
                    let investmentAmountCurrencyViewController = ViewControllerProvider.setupInvestmentAmountCurrencyViewController(rootViewController: self)
                    investmentAmountCurrencyViewController?.hidesBottomBarWhenPushed = true
                case .confirmed:
                    let paymentMethodsViewController = ViewControllerProvider.setupPaymentMethodsViewController(rootViewController: self)
                    paymentMethodsViewController?.hidesBottomBarWhenPushed = true
                case .canceled,
                     .received,
                     .refundFailed,
                     .refunded,
                     .refundedInProgress:
                    return
                }
                self.pushNextController(animated: true)
            }
        }
    }
    
    private func setupInvestNowButton() {
        investNowButton.isHidden = viewModel.checkInvestButton()
    }
}

extension ProjectDetailsConstructorViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let block = self.viewModel.block(indexPath: indexPath) else { return UITableView.automaticDimension }
        switch block.type {
        case .table:
            let payload = block.payload as? TableBlockPayload
            return ProjectDetailsTableTableViewCell.cellHeight(numberOfRows: payload?.rowsNumber ?? 0)
        case .imageLink:
            let payload = block.payload as? ImageLinkBlockPayload
            let offset = UIDevice.current.userInterfaceIdiom == .pad ? Constants.horizontalOffsetsForPad : Constants.horizontalOffsets
            let itemWidth = UIScreen.main.bounds.width - offset
            let width = UIDevice.current.userInterfaceIdiom == .pad ? itemWidth/2 : itemWidth
            let maxText = payload?.items.map { $0?.description }.sorted { $0?.count ?? 0 > $1?.count ?? 0}.first
            let maxHeight = (maxText ?? "")?.height(withConstrainedWidth: width, attributes: [.font: UIFont.roboto.regular.placeholderSize]) ?? 0
            return ProjectDetailsImageLinkCollectionViewCell.height(with: maxHeight) + ProjectDetailsImageLinkTableViewCell.Constants.swipeViewHeight
        case .divider:
            return CGFloat((block.payload as? DividerBlockPayload)?.height ?? Int(UITableView.automaticDimension))
        case .delimiter:
            return (block.payload as? LineDelimiterBlockPayload)?.height ?? UITableView.automaticDimension
        default:
            break
        }
        return UITableView.automaticDimension
    }
}

extension ProjectDetailsConstructorViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        self.viewModel.tabs.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.tabs[section]?.blocks.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let block = self.viewModel.block(indexPath: indexPath) else { return UITableViewCell() }
        switch block.type {
        case .mainFacts:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsMainFactsTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsMainFactsTableViewCell
            cell.setup(project: self.session?.projectsProvider.currentProject ,projectInfo: self.session?.projectsProvider.expandedProject)
            cell.delegate = self
            return cell
        case .text:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsTextTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsTextTableViewCell
            cell.setup(block.payload as? TextBlockPayload)
            return cell
        case .image:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsImageTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsImageTableViewCell
            cell.delegate = self
            cell.setup(block.payload as? ImageBlockPayload)
            return cell
        case .textAndImage:
            let cell = UIDevice.current.userInterfaceIdiom == .pad ? self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsTextAndImagePadTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsTextAndImagePadTableViewCell : self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsTextAndImageTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsTextAndImageTableViewCell
            cell.delegate = self
            cell.setup(block.payload as? TextAndImageBlockPayload)
            return cell
        case .list:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsListTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsListTableViewCell
            cell.setup(block.payload as? ListBlockPayload)
            return cell
        case .FAQ:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsFAQTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsFAQTableViewCell
            cell.delegate = self
            cell.setup(block.payload as? FAQBlockPayload)
            return cell
        case .table:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsTableTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsTableTableViewCell
            cell.setup(block.payload as? TableBlockPayload)
            return cell
        case .icons:
            let cell = UIDevice.current.userInterfaceIdiom == .pad ? self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsIconsPadTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsIconsPadTableViewCell :  self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsIconsTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsIconsTableViewCell
            cell.delegate = self
            cell.setup(block.payload as? IconsBlockPayload)
            return cell
        case .video:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsVideoTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsVideoTableViewCell
            cell.delegate = self
            cell.setup(block.payload as? VideoBlockPayload)
            return cell
        case .divider:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsDividerTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsDividerTableViewCell
            cell.setup()
            return cell
        case .facts:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsFactsTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsFactsTableViewCell
            cell.delegate = self
            cell.setup(block.payload as? FactsBlockPayload)
            return cell
        case .dualColumnImages:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsDualColumnImagesTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsDualColumnImagesTableViewCell
            cell.delegate = self
            cell.setup(block.payload as? DualColumnImagesBlockPayload)
            return cell
        case .imageLink:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsImageLinkTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsImageLinkTableViewCell
            cell.delegate = self
            cell.setup(block.payload as? ImageLinkBlockPayload)
            return cell
        case .header:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsHeaderTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsHeaderTableViewCell
            cell.setup(block.payload as? HeaderBlockPayload)
            return cell
        case .button:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsButtonTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsButtonTableViewCell
            cell.setup(block.payload as? ButtonBlockPayload)
            return cell
        case .department:
            let cell = UIDevice.current.userInterfaceIdiom == .pad ?
                self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsDepartmentPadTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsDepartmentPadTableViewCell :
                self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsDepartmentTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsDepartmentTableViewCell
            cell.delegate = self
            if UIDevice.current.userInterfaceIdiom == .pad {
                cell.setupForPad(block.payload as? DepartmentBlockPayload)
            } else {
                cell.setup(block.payload as? DepartmentBlockPayload)
            }
            return cell
        case .dualColumnText:
            let cell = UIDevice.current.userInterfaceIdiom == .pad ? self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsDualColumnTextPadTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsDualColumnTextPadTableViewCell : self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsDualColumnTextPadTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsDualColumnTextPadTableViewCell
            cell.setup(block.payload as? DualColumnTextBlockPayload)
            return cell
        case .disclaimer:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsDisclaimerTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsDisclaimerTableViewCell
            cell.delegate = self
            cell.setup(block.payload as? DisclaimerBlockPayload)
            return cell
        case .linkedText:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsLinkedTextTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsLinkedTextTableViewCell
            cell.setup(block.payload as? LinkedTextBlockPayload)
            return cell
        case .tilesImages:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsTiledImagesTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsTiledImagesTableViewCell
            cell.delegate = self
            cell.setup(block.payload as? TilesImagesBlockPayload)
            return cell
        case .chart:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsChartTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsChartTableViewCell
            guard let item = (block.payload as? ChartBlockPayload) else { return UITableViewCell() }
            cell.setup(item)
            return cell
        case .delimiter:
            let cell = self.constructorTableView.dequeueReusableCell(withIdentifier: ProjectDetailsDividerTableViewCell.reuseIdentifier, for: indexPath) as! ProjectDetailsDividerTableViewCell
            let payload = block.payload as? LineDelimiterBlockPayload
            cell.setup(payload?.above, payload?.below)
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension ProjectDetailsConstructorViewController: LoadProjectDetailsImageDelegate {
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView, completionHandler: VoidClosure?) {
        self.viewModel.loadProjectDetailsImageData(ID, imageView, completionHandler: completionHandler)
    }
}

extension ProjectDetailsConstructorViewController: ProjectDetailsVideoDelegate {
    func playVideoWithID(_ videoID: String) {
        self.viewModel.requestProjectDetailsVideoURL(itemID: videoID) { (result) in
            switch result {
            case .failure(let error):
                self.presentAlert(message: error.localizedDescription)
                break
            case .success(let videoURL):
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }
    }
}

extension ProjectDetailsConstructorViewController: ProjectDetailsExpandableCellDelegate {
    func updateExpandableCell() {
        self.constructorTableView.beginUpdates()
        self.constructorTableView.endUpdates()
    }
    
    func scrollToTop() {
        self.constructorTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    }
}


//MARK:/ SegmentedControl setup
extension ProjectDetailsConstructorViewController {
    func setupSegmentedControl(segmentedControl: ProjectsSegmentedControl) {
        self.viewModel.tabs.forEach { (item) in
            if let index = self.viewModel.tabs.firstIndex(where: {$0?.ID == item?.ID}) {
                if self.segmentedControl.numberOfSegments > index {
                    self.segmentedControl.setTitle(item?.name?.uppercased(), forSegmentAt: index)
                } else {
                    self.segmentedControl.insertSegment(withTitle: item?.name?.uppercased(), at: index, animated: false)
                }
            }
        }
        self.fixBackgroundSegmentControl(segmentedControl)
        self.segmentedControl.selectedSegmentIndex = 0
    }
    
    func fixBackgroundSegmentControl( _ segmentedControl: UISegmentedControl) {
        if #available(iOS 13.0, *) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                for i in 0...(segmentedControl.numberOfSegments - 1)  {
                    let backgroundSegmentView = segmentedControl.subviews[i]
                    backgroundSegmentView.isHidden = true
                }
            }
        }
    }
    
    func setDefaultConstraints(for indicator: UIView, dependsOn segmentedControl: UISegmentedControl) {
        let width = self.segmentedControl.segmentWidthByContent(for: segmentedControl.selectedSegmentIndex)
        indicator.snp.makeConstraints {
            $0.leading.equalTo(segmentedControl.snp.leading)
            $0.top.equalTo(segmentedControl.snp.bottom).offset(Constants.indicatorOffset)
            $0.height.equalTo(Constants.indicatorHeight)
            $0.width.equalTo(width)
        }
    }
    
    @IBAction func tap(_ sender: UISegmentedControl) {
        guard self.constructorTableView.numberOfSections > sender.selectedSegmentIndex,
              self.constructorTableView.numberOfRows(inSection: sender.selectedSegmentIndex) > 0 else {
            return
        }
        self.segmentWasSelectedManually = true
        self.handleSegmentSelection(control: sender, indicator: self.indicator)
        self.constructorTableView.scrollToRow(at: IndexPath(row: 0, section: sender.selectedSegmentIndex), at: .top, animated: false)
        UIView.animate(withDuration: 0.3, animations: {
                        self.view.layoutIfNeeded() })
    }
    
    func handleSegmentSelection(control: UISegmentedControl, indicator: UIView) {
        let width = self.segmentedControl.segmentWidthByContent(for: segmentedControl.selectedSegmentIndex)
        let offset = self.segmentedControl.segmentOffsetByContent(for: segmentedControl.selectedSegmentIndex)
        indicator.snp.remakeConstraints {
            $0.leading.equalTo(segmentedControl.snp.leading).offset(offset)
            $0.top.equalTo(segmentedControl.snp.bottom).offset(Constants.indicatorOffset)
            $0.height.equalTo(Constants.indicatorHeight)
            $0.width.equalTo(width)
        }
    }
}

extension ProjectDetailsConstructorViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !self.segmentWasSelectedManually {
            if let visibleRows = constructorTableView.indexPathsForVisibleRows,
               let sectionIndex = visibleRows.map({$0.section}).sorted(by: > ).last {
                self.segmentedControl.selectedSegmentIndex = sectionIndex
            }
            self.handleSegmentSelection(control: self.segmentedControl, indicator: self.indicator)
        }
        self.segmentWasSelectedManually = false
    }
}
