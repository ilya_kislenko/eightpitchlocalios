//
//  ProjectDetailsViewController.swift
//  8Pitch
//
//  Created by 8pitch on 9/27/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Input

class ProjectDetailsViewController: GenericViewController {
    
    private lazy var viewModel : ProjectDetailsViewModel = {
        ProjectDetailsViewModel(session: session)
    }()

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(ProjectDetailsOverViewCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectDetailsOverViewCollectionViewCell.reuseIdentifier)
        self.collectionView.register(ProjectDetailsInvestReasonsCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectDetailsInvestReasonsCollectionViewCell.reuseIdentifier)
        self.collectionView.register(ProjectDetailsWarningCollectionViewCell.nibForRegistration, forCellWithReuseIdentifier: ProjectDetailsWarningCollectionViewCell.reuseIdentifier)
    }
    
    private func openPromoVideo() {
        self.viewModel.requestProjectDetailsVideoURL(itemID: self.session?.projectsProvider.expandedProject?.projectPage?.promoVideo ?? "") { (result) in
            switch result {
            case .failure(let error):
                self.presentAlert(message: error.localizedDescription)
            case .success(let videoURL):
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }
        }
    }
    
    private func investButtonAction() {
        guard let id = self.session?.projectsProvider.currentProject?.identifier else {
            return
        }
        self.startProgress()
        self.viewModel.loadProjectInvestmentInfo(id: id) { [weak self] (result) in
            guard let self = self else { return }
            self.stopProgress()
            switch result {
            case .failure(let error):
                self.presentAlert(message: error.localizedDescription)
            case .success(let status):
                switch status {
                case .canceldInProgress,
                     .new:
                    self.investButtonAction()
                    return
                case .pending:
                    self.presentAlert(message: "error.remote.investment.pending".localized)
                    return
                case .paid:
                    let _ = ViewControllerProvider.setupProcessedInvestmentViewController(rootViewController: self)
                    break
                case .booked:
                    let _ = ViewControllerProvider.setupUnfinishedInvestmentViewController(rootViewController: self)
                    break
                case .none:
                    let investmentAmountCurrencyViewController = ViewControllerProvider.setupInvestmentAmountCurrencyViewController(rootViewController: self)
                    investmentAmountCurrencyViewController?.hidesBottomBarWhenPushed = true
                    break
                case .confirmed:
                    let paymentMethodsViewController = ViewControllerProvider.setupPaymentMethodsViewController(rootViewController: self)
                    paymentMethodsViewController?.hidesBottomBarWhenPushed = true
                    break
                default: return
                }
                self.pushNextController(animated: true)
            }
        }

    }

}

extension ProjectDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Constants.screenSectionsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case Constants.projectDetailsIndex:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectDetailsOverViewCollectionViewCell.reuseIdentifier, for: indexPath) as? ProjectDetailsOverViewCollectionViewCell else {
                fatalError("ProjectDetailsOverViewCollectionViewCell is not exist")
            }
            cell.delegate = self
            cell.setupFor(project: session?.projectsProvider.expandedProject, for: session?.level)
            cell.playButtonAction = {
                self.openPromoVideo()
            }
            cell.buttonAction = {
                self.investButtonAction()
            }
            return cell
        case Constants.investmentReasonsIndex:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectDetailsInvestReasonsCollectionViewCell.reuseIdentifier, for: indexPath) as? ProjectDetailsInvestReasonsCollectionViewCell else {
                fatalError("ProjectDetailsInvestReasonsCollectionViewCell is not exist")
            }
            cell.setup(firstArgument: session?.projectsProvider.expandedProject?.projectPage?.firstInvestmentArguments, secondArgument: session?.projectsProvider.expandedProject?.projectPage?.secondInvestmentArguments, thirdArgument: session?.projectsProvider.expandedProject?.projectPage?.thirdInvestmentArguments)
            return cell
        case Constants.projectWarningIndex:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectDetailsWarningCollectionViewCell.reuseIdentifier, for: indexPath) as? ProjectDetailsWarningCollectionViewCell else {
                fatalError("ProjectDetailsWarningCollectionViewCell is not exist")
            }
            return cell
        default:
            fatalError("\(self.debugDescription): wrong number of items in collection view")
        }
    }
    
}

// MARK: UICollectionViewDelegateFlowLayout

extension ProjectDetailsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == Constants.projectDetailsIndex {
            return CGSize(width: UIScreen.main.bounds.width, height: 700)
        }
        return .zero
    }
}

extension ProjectDetailsViewController {
    private enum Constants {
        static let screenSectionsCount = 3
        static let projectDetailsIndex = 0
        static let investmentReasonsIndex = 1
        static let projectWarningIndex = 2
    }
}

extension ProjectDetailsViewController: LoadProjectDetailsImageDelegate {
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView, completionHandler: VoidClosure?) {
        self.viewModel.loadProjectDetailsImageData(ID, imageView, completionHandler: completionHandler)
    }
    
    func loadProjectDetailsImage(_ ID: String, _ imageView: UIImageView) {
        self.viewModel.loadProjectDetailsImageData(ID, imageView)
    }
}
