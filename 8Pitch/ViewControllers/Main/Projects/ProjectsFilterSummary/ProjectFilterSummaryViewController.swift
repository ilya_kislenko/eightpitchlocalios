//
//  ProjectFilterSummaryViewController.swift
//  8Pitch
//
//  Created by 8Pitch on 19.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import RxSwift
import MobileCoreServices

class ProjectFilterSummaryViewController: CustomTitleViewController {
    
    private lazy var viewModel : ProjectFilterSummaryViewModel = {
        ProjectFilterSummaryViewModel(session: session)
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subtitleLabel: CustomSubtitleLabel!
    @IBOutlet weak var acceptButton: StateButton!
    
    private var cellItems: [CellItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.barlow.semibold.titleSize, NSAttributedString.Key.foregroundColor: UIColor(.gray)]
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(.arrowBack), style: .plain, target: self, action: #selector(backTapped))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "project.filter.summary.reset.button".localized.uppercased(), style: .plain, target: self, action: #selector(resetFilter))
        self.title = "project.filter.summary.main.title".localized.uppercased()
        self.subtitleLabel.text = "project.filter.summary.subtitle".localized
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navigationBar = self.navigationController?.navigationBar as? CustomNavigationBar {
            navigationBar.setupAppearance()
        }
        self.navigationItem.customizeForFilters()
        setupTableData()
    }
    
    @IBAction func acceptButtonClick(_ sender: Any) {
        self.session?.projectsFilterProvider.setupAppliedFilters()
        backTapped()
    }
    
    func setupTableData() {
        guard let session = session else { return }
        cellItems = [CellItem(.projectStatusTitle)]
        for (index, status) in session.projectsFilterProvider.selectedStatuses.enumerated() {
            cellItems.append(.init(.projectStatusItem, filterStatus: status, isLastItem: index == session.projectsFilterProvider.selectedStatuses.endIndex - 1))
        }
        cellItems.append( CellItem(.investAmountTitle))
        for (index, investAmount) in session.projectsFilterProvider.selectedInvesmentAmount.enumerated() {
            cellItems.append(.init(.investAmountItem, filterName: investAmount, isLastItem: index == session.projectsFilterProvider.selectedInvesmentAmount.endIndex - 1))
        }
        cellItems.append(CellItem(.securityTypesTitle))
        for (index, securityType) in session.projectsFilterProvider.selectedSecurityTypes.enumerated() {
            cellItems.append(.init(.securityTypesItem, filterName: securityType, isLastItem: index == session.projectsFilterProvider.selectedSecurityTypes.endIndex - 1))
        }
        getFilteredProjects()
    }
    
    func getFilteredProjects() {
        guard let session = session else { return }
        self.startProgress()
        self.remote.getProjectsFiltered(session, queryItems: self.session?.projectsFilterProvider.getQueryItems()) { [weak self] (error) in
            self?.stopProgress()
            if let error = error {
                self?.presentAlert(message: error.localizedDescription)
            } else {
                self?.tableView.reloadData()
                self?.acceptButton.setTitle(session.projectsFilterProvider.isFiltersSelected() ? session.projectsFilterProvider.filteredProjects.isEmpty ? "project.filter.summary.no.projects.button".localized : String(format: "project.filter.summary.show.part.of.projects.button".localized, session.projectsFilterProvider.filteredProjects.count) : "project.filter.summary.show.all.projects.button".localized, for: .normal)
                self?.acceptButton.isEnabled = session.projectsFilterProvider.isFiltersSelected() ? !session.projectsFilterProvider.filteredProjects.isEmpty : true
            }
        }
    }

    func setupTableView() {
        self.tableView.register(ProjectFilterSelectedStateCell.nibForRegistration, forCellReuseIdentifier: ProjectFilterSelectedStateCell.reuseIdentifier)
        self.tableView.register(ProjectFilterHeaderCell.nibForRegistration, forCellReuseIdentifier: ProjectFilterHeaderCell.reuseIdentifier)
        tableView.separatorStyle = .none
    }
    
    @objc private func resetFilter() {
        self.session?.projectsFilterProvider.resetFilter()
        setupTableData()
    }
    
}

extension ProjectFilterSummaryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cellItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch cellItems[indexPath.row].type {
            case .projectStatusTitle:
                let cell = tableView.dequeueReusableCell(withIdentifier: ProjectFilterHeaderCell.reuseIdentifier, for: indexPath) as! ProjectFilterHeaderCell
                cell.setup(type: .status, hideSeparator: !cellItems.contains(where: {$0.type == .projectStatusItem}))
                cell.selectionStyle = .none
                return cell
            case .investAmountTitle:
                let cell = tableView.dequeueReusableCell(withIdentifier: ProjectFilterHeaderCell.reuseIdentifier, for: indexPath) as! ProjectFilterHeaderCell
                cell.setup(type: .investmentAmount, hideSeparator: !cellItems.contains(where: {$0.type == .investAmountItem}))
                cell.selectionStyle = .none
                return cell
            case .securityTypesTitle:
                let cell = tableView.dequeueReusableCell(withIdentifier: ProjectFilterHeaderCell.reuseIdentifier, for: indexPath) as! ProjectFilterHeaderCell
                cell.setup(type: .typesOfSecurity, hideSeparator: !cellItems.contains(where: {$0.type == .securityTypesItem}))
                cell.selectionStyle = .none
                return cell
            case .projectStatusItem:
                let cell = tableView.dequeueReusableCell(withIdentifier: ProjectFilterSelectedStateCell.reuseIdentifier, for: indexPath) as! ProjectFilterSelectedStateCell
                if let item = cellItems[indexPath.row].filterStatus, let isLastItem = cellItems[indexPath.row].isLastItem {
                    cell.setup(item: item, isLastItem: isLastItem)
                    cell.closeButtonAction = { [weak self] in
                        self?.session?.projectsFilterProvider.getStatus(status: item)
                        self?.setupTableData()
                    }
                }
                cell.selectionStyle = .none
                return cell
            case .investAmountItem:
                let cell = tableView.dequeueReusableCell(withIdentifier: ProjectFilterSelectedStateCell.reuseIdentifier, for: indexPath) as! ProjectFilterSelectedStateCell
                if let item = cellItems[indexPath.row].filterName, let isLastItem = cellItems[indexPath.row].isLastItem {
                    cell.setup(item: item, type: .investmentAmount, isLastItem: isLastItem)
                    cell.closeButtonAction = { [weak self] in
                        self?.session?.projectsFilterProvider.getInvestmentAmount(investmentAmount: item)
                        self?.setupTableData()
                    }
                }
                cell.selectionStyle = .none
                return cell
            case .securityTypesItem:
                let cell = tableView.dequeueReusableCell(withIdentifier: ProjectFilterSelectedStateCell.reuseIdentifier, for: indexPath) as! ProjectFilterSelectedStateCell
                if let item = cellItems[indexPath.row].filterName, let isLastItem = cellItems[indexPath.row].isLastItem {
                    cell.setup(item: item, type: .typesOfSecurity, isLastItem: isLastItem)
                    cell.closeButtonAction = { [weak self] in
                        self?.session?.projectsFilterProvider.getSecurityType(securityType: item)
                        self?.setupTableData()
                    }
                }
                cell.selectionStyle = .none
                return cell
        }
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch cellItems[indexPath.row].type {
            case .projectStatusTitle:
                let _ = ViewControllerProvider.setupProjectFilterViewController(rootViewController: self, type: .status)
                self.pushNextController(animated: true)
            case .investAmountTitle:
                let _ = ViewControllerProvider.setupProjectFilterViewController(rootViewController: self, type: .investmentAmount)
                self.pushNextController(animated: true)
            case .securityTypesTitle:
                let _ = ViewControllerProvider.setupProjectFilterViewController(rootViewController: self, type: .typesOfSecurity)
                self.pushNextController(animated: true)
            case .projectStatusItem, .investAmountItem, .securityTypesItem:
                break
        }
    }


}


fileprivate enum ItemType {

    case projectStatusTitle
    case projectStatusItem
    case investAmountTitle
    case investAmountItem
    case securityTypesTitle
    case securityTypesItem

}

fileprivate class CellItem {
    var type:       ItemType
    var filterName: String?
    var filterStatus: ProjectStatus?
    var isLastItem: Bool?

    init(_ type: ItemType, filterName: String? = nil, filterStatus: ProjectStatus? = nil, isLastItem: Bool? = nil) {
        self.type = type
        self.filterName = filterName
        self.filterStatus = filterStatus
        self.isLastItem = isLastItem
    }
}
