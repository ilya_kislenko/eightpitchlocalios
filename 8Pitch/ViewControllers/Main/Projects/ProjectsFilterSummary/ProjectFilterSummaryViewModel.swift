//
//  ProjectFilterSummaryViewModel.swift
//  8Pitch
//
//  Created by 8Pitch on 19.01.21.
//  Copyright © 2021 8Pitch. All rights reserved.
//
import Input

class ProjectFilterSummaryViewModel: GenericViewModel {
    var selectedInvestedAmount: [String] = []
    var selectedSecurityTypes: [String] = []
    var selectedStatuses: [ProjectStatus] = []
    
    func setupSelectedFilters() {
        selectedStatuses = session.projectsFilterProvider.selectedStatuses
        selectedInvestedAmount = session.projectsFilterProvider.selectedInvesmentAmount
        selectedSecurityTypes = session.projectsFilterProvider.selectedSecurityTypes
    }
}
