//
//  ProjectsViewController.swift
//  8Pitch
//
//  Created by 8pitch on 17.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input
import Remote
import RxSwift

class ProjectsViewController: CustomTitleViewController {
    
    // MARK: Properties
    
    private lazy var viewModel : ProjectsViewModel = {
        ProjectsViewModel(session: session)
    }()
    
    // MARK: UI Components
    
    @IBOutlet weak var segmentedControl: ProjectsSegmentedControl!
    
    private let indicator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(.red)
        return view
    }()
    
    private lazy var currentProjectsScreen: ProjectsTabViewController = {
        (ViewControllerProvider.setupProjectsTabViewController(rootViewController: self, type: .current) ?? ProjectsTabViewController())
    }()
    private lazy var completedProjectsScreen: ProjectsTabViewController = {
        ViewControllerProvider.setupProjectsTabViewController(rootViewController: self, type: .completed) ?? ProjectsTabViewController()
    }()
    private lazy var filteredProjectsScreen: ProjectsTabViewController = {
        ViewControllerProvider.setupProjectsTabViewController(rootViewController: self, type: .filtered) ?? ProjectsTabViewController()
    }()
    
    private let contentView = UIView()
    private let filteredView = UIView()
    
    // MARK: Actions
    
    @IBAction func tap(_ sender: UISegmentedControl) {
        self.handleAppearanceOnTouch(sender, indicator: indicator)
        UIView.animate(withDuration: 0.3, animations: {
                        self.view.layoutIfNeeded() })
        self.currentProjectsScreen.view.isHidden = sender.selectedSegmentIndex != 0
        self.completedProjectsScreen.view.isHidden = sender.selectedSegmentIndex != 1
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getUser()
        self.setupSegmentedControl(segmentedControl: segmentedControl, with: indicator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.contentView.isHidden = viewModel.isFilterWasApplied()
        self.filteredView.isHidden = !viewModel.isFilterWasApplied()
        segmentedControl.isHidden = viewModel.isFilterWasApplied()
        indicator.isHidden = viewModel.isFilterWasApplied()
        self.setupNavigationBar()
        self.navigationController?.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.handleAppearanceOnTouch(self.segmentedControl, indicator: self.indicator)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.subviews.forEach {
            $0.removeFromSuperview()
        }
    }
    
}

// MARK: Private

private extension ProjectsViewController {
    
    func setupSegmentedControl(segmentedControl: ProjectsSegmentedControl, with indicator: UIView) {
        segmentedControl.setupFor(titles: ["projects.tabs.explore".localized, "projects.tab.invested".localized])
        self.fixBackgroundSegmentControl(segmentedControl)
        let separator = SeparatorView()
        self.view.addSubview(separator)
        separator.snp.makeConstraints {
            $0.trailing.leading.equalToSuperview()
            $0.top.equalTo(segmentedControl.snp.bottom).offset(Constants.indicatorOffset + 1)
        }
        self.view.addSubview(self.indicator)
        self.setDefaultConstraints(for: indicator, dependsOn: segmentedControl)
        indicator.snp.makeConstraints {
            $0.leading.equalTo(segmentedControl.snp.leading)
        }
    }
    
    func handleAppearanceOnTouch(_ sender: UISegmentedControl, indicator: UIView) {
        indicator.snp.remakeConstraints {
            if sender.selectedSegmentIndex == sender.numberOfSegments - 1 {
                $0.trailing.equalTo(sender.snp.trailing)
            } else {
                $0.leading.equalTo(sender.snp.leading)
            }
        }
        self.setDefaultConstraints(for: indicator, dependsOn: sender)
    }
    
    func setDefaultConstraints(for indicator: UIView, dependsOn segmentedControl: UISegmentedControl) {
        indicator.snp.makeConstraints {
            $0.top.equalTo(segmentedControl.snp.bottom).offset(Constants.indicatorOffset)
            $0.height.equalTo(Constants.indicatorHeight)
            $0.width.equalTo(segmentedControl.frame.width/CGFloat(segmentedControl.numberOfSegments))
        }
    }
    
    func fixBackgroundSegmentControl( _ segmentedControl: UISegmentedControl) {
        if #available(iOS 13.0, *) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                for i in 0...(segmentedControl.numberOfSegments - 1)  {
                    let backgroundSegmentView = segmentedControl.subviews[i]
                    backgroundSegmentView.isHidden = true
                }
            }
        }
    }
    
    enum Constants {
        static let indicatorOffset: CGFloat = 3
        static let indicatorHeight: CGFloat = 2
    }
    
    func add(_ child: ProjectsTabViewController) {
        addChild(child)
        self.contentView.addSubview(child.view)
        child.didMove(toParent: self)
        child.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        child.openDetails = {
            self.loadProjectDetails()
        }
    }
    
    func addFilter(_ child: ProjectsTabViewController) {
        addChild(child)
        self.filteredView.addSubview(child.view)
        child.didMove(toParent: self)
        child.view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        child.openDetails = {
            self.loadProjectDetails()
        }
    }
    
    func loadProjectDetails() {
        self.startProgress()
        self.viewModel.loadProjectInvestmentData() { (result) in
            self.stopProgress()
            switch result {
            case .failure(let error):
                self.presentAlert(message: error.localizedDescription)
                break
            case .success(_):
                self.openDetails()
            }
        }
    }
    
    func openDetails() {
        if let projectDetailsRootViewController = ViewControllerProvider.setupProjectDetailsRootViewController(rootViewController: self) {
            self.navigationController?.pushViewController(projectDetailsRootViewController, animated: true)
        }
    }
    
    func setTitle() {
        if let navigationBar = self.navigationController?.navigationBar {
            let titleLabel = UILabel()
            let attributes: [NSAttributedString.Key : Any] = [.font: UIFont.barlow.semibold.currencyInputSize,
                              .foregroundColor: UIColor(.gray), .kern: -0.5]
            titleLabel.attributedText = NSAttributedString(string: "projects.title".localized, attributes: attributes)
            
            navigationBar.addSubview(titleLabel)
            titleLabel.snp.makeConstraints {
                $0.center.equalToSuperview()
            }
        }
    }
    
    func setBarButtons() {
        self.navigationItem.rightBarButtonItem = nil
        let customView = UIButton()
        let filterButton = UIButton(type: .custom)
        filterButton.frame = CGRect(x: 0, y: 0, width: 28, height: 28)
        filterButton.setImage(viewModel.isFilterWasApplied() ? UIImage(.glyphFilled) : UIImage(.glyph), for: .normal)
        filterButton.addTarget(self, action: #selector(self.showFilterSummaryPage), for: .touchUpInside)
        let searchButton = UIButton(frame: CGRect(x: 0, y: 0, width: 28, height: 28))
        searchButton.setImage(UIImage(.searchIcon), for: .normal)
        searchButton.addTarget(self, action: #selector(self.showProjectsFiltersPage), for: .touchUpInside)
        customView.addSubview(filterButton)
        customView.addSubview(searchButton)
        searchButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview()
        }
        filterButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.leading.equalTo(searchButton.snp.trailing).offset(8)
        }
        filterButton.tintColor = viewModel.isFilterWasApplied() ? .red : .gray
        searchButton.tintColor = .gray
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: customView)
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.customizeForWhite()
        self.navigationItem.leftBarButtonItem = nil
        self.setTitle()
        self.setBarButtons()
    }
    
    @objc func showProjectsFiltersPage() {
        let view = ViewControllerProvider.setupProjectsSearchViewController(rootViewController: self)
        view?.openDetails = {
            self.loadProjectDetails()
        }
        self.pushNextController(animated: true)
    }
    
    @objc func showFilterSummaryPage() {
        let _ = ViewControllerProvider.setupProjectFilterSummaryViewController(rootViewController: self)
        session?.projectsFilterProvider.setupSelectedFilters()
        self.pushNextController(animated: true)
    }
    
    func getUser() {
        guard let model = try? self.session?.userProvider.value() else { return }
        self.startProgress()
        self.viewModel.user() { [weak self] response in
            self?.stopProgress()
            switch response {
            case .success( _):
                self?.addContent()
                return
            case .failure(let error):
                self?.presentAlert(message: error.localizedDescription)
                model.isValid.onNext(.failure(error))
                return
            }
        }
    }
    
    func addContent() {
        self.view.addSubview(self.contentView)
        self.view.addSubview(self.filteredView)
        self.filteredView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        self.addFilter(filteredProjectsScreen)
        self.contentView.snp.makeConstraints {
            $0.trailing.leading.bottom.equalToSuperview()
            $0.top.equalTo(self.indicator.snp.bottom)
        }
        self.add(self.currentProjectsScreen)
        self.add(self.completedProjectsScreen)
        self.currentProjectsScreen.view.isHidden = false
        self.completedProjectsScreen.view.isHidden = true
        self.filteredView.isHidden = true
        self.filteredProjectsScreen.view.isHidden = false
    }
    
}
