//
//  ProjectsViewModel.swift
//  8Pitch
//
//  Created by 8pitch on 20.08.2020.
//  Copyright © 2020 8Pitch. All rights reserved.
//

class ProjectsViewModel: GenericViewModel {
    func  isFilterWasApplied() -> Bool {
        return session.projectsFilterProvider.filterWasApplied 
    }
}

