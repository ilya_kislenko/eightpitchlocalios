//
//  TabBarController.swift
//  8Pitch
//
//  Created by 8pitch on 8/21/20.
//  Copyright © 2020 8Pitch. All rights reserved.
//

import UIKit
import Input

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    private struct Constants {
        static let selectionIndicatorHeight : CGFloat = 2
    }

    var session: Session
    
    init(_ session: Session) {
        self.session = session
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private var shouldSelectIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        self.setupTabBarContent()
        self.setupTabBarAppearance()
        self.navigationItem.setHidesBackButton(true, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    private func setupTabBarAppearance() {
        self.tabBar.isTranslucent = false
        self.tabBar.barTintColor = UIColor(Color.backgroundDark)
        let selectionIndicatorSize = self.getSelectionIndicatorSize()
        self.tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: UIColor(Color.red), size: selectionIndicatorSize, lineWidth: Constants.selectionIndicatorHeight)
    }

    private func getSelectionIndicatorSize() -> CGSize {
        let width = self.tabBar.frame.width/CGFloat(tabBar.items!.count)
        let height = self.tabBar.frame.height + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? .zero)
        let size = CGSize(width: width, height: height)
        return size
    }

    private func setupTabBarContent() {
        guard let projectsController = ViewControllerProvider.setupProjectsViewController(rootViewController: self),
            let dashboardController = ViewControllerProvider.setupDashboardViewController(rootViewController: self),
            let notificationsController = ViewControllerProvider.setupNotificationListViewController(rootViewController: self),
            let profileController = ViewControllerProvider.setupProfileViewController(rootViewController: self) else { return }
        let navigationViewControllers = [self.createNavigationController(root: projectsController),
                                         self.createNavigationController(root: dashboardController),
                                         self.createNavigationController(root: notificationsController),
                                         self.createNavigationController(root: profileController)]
        let projectsBarItem = UITabBarItem(title: "tabbar.projects".localized, image: UIImage(Image.projects), selectedImage: UIImage(Image.projectsSelected))
        projectsBarItem.setupAppearance()
        let dashboardBarItem = UITabBarItem(title: "tabbar.dashboard".localized, image: UIImage(Image.dashboard), selectedImage: UIImage(Image.dashboardSelected))
        dashboardBarItem.setupAppearance()
        let nottificatitonsBarItem = UITabBarItem(title: "tabbar.notifications".localized, image: UIImage(Image.notifications), selectedImage: UIImage(Image.notificationsSelected))
        if session.notificationsProvider.notifications.contains(where: { $0.status != .read }) {
            nottificatitonsBarItem.badgeValue = "●"
            nottificatitonsBarItem.badgeColor = .clear
            nottificatitonsBarItem.setBadgeTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(Color.red)], for: .normal)
        } else {
            nottificatitonsBarItem.badgeValue = ""
            nottificatitonsBarItem.badgeColor = .clear
        }
        nottificatitonsBarItem.setupAppearance()
        let profileBarItem = UITabBarItem(title: "tabbar.profile".localized, image: UIImage(Image.profile), selectedImage: UIImage(Image.profileSelected))
        profileBarItem.setupAppearance()
        
        projectsController.tabBarItem = projectsBarItem
        dashboardController.tabBarItem = dashboardBarItem
        notificationsController.tabBarItem = nottificatitonsBarItem
        profileController.tabBarItem = profileBarItem
        self.tabBar.unselectedItemTintColor = UIColor(.tabBarWhite)
        self.setViewControllers(navigationViewControllers, animated: false)
    }
    
   private func createNavigationController(root: UIViewController) -> UINavigationController {
        let controller = UINavigationController(navigationBarClass: CustomNavigationBar.self, toolbarClass: nil)
        controller.setViewControllers([root], animated: true)
        return controller
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        shouldSelectIndex = tabBarController.selectedIndex
        return true
    }

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if shouldSelectIndex == tabBarController.selectedIndex {
            if let splitViewController = viewController as? UINavigationController {
                if let navController = splitViewController.viewControllers[0] as? ProjectsViewController {
                    navController.view.scrollToTop(in: navController.view)
                }
            }
        }
    }
}
