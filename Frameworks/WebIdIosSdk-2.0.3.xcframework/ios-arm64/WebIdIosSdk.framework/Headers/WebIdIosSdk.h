//
//  WebIdIosSdk.h
//  WebIdIosSdk
//
//  Created by Florian Pavkovic on 05.12.18.
//  Copyright © 2018 WebID Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WebIdIosSdk.
FOUNDATION_EXPORT double WebIdIosSdkVersionNumber;

//! Project version string for WebIdIosSdk.
FOUNDATION_EXPORT const unsigned char WebIdIosSdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WebIdIosSdk/PublicHeader.h>


